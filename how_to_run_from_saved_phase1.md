# How to run Phase II and III from a saved Phase I

- Phase I terminated and you have an output directory that should be named something like: `../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/1500_1500_2000_200_200_0.005`.
- What I do is that I copy-paste the entire folder and its content in its parent folder and rename it by changing the "first number" to let's say a million: `1000000`, so now you have a copy of your Phase I run in `../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/1000000_1500_2000_200_200_0.005`.
- Then you simply have to modify in the config file: `../examples/config_files/config_GW170817_IMRPhenomD_NRTidal.ini` the parameter `n_traj_hmc_tot`. Normally it is set equal to `1500` and you set it to this number we chose, i.e. `1000000`. It represents the maximum number of trajectories which will be run by the code (Phase I included) if it does not converge to the asked number of SISs first (defined by `n_sis` in the config file). So here a maximum of `998500` will be run in Phase III which is clearly enough as it should converge after `~1e5`.
- Then you simply have to re-run the `main.py` script from the command line with:
```sh
$ python main.py --event=GW170817 --config_file=../examples/config_files/config_GW170817_IMRPhenomD_NRTidal.ini --trigger_file=../examples/trigger_files/GW170817_IMRPhenomD_NRTidal.ini --fit_method=dnn
```
and it will load Phase I data to run Phase II directly.
- Of course you could simply directly rename the original folder, or we could have directly set the run to not stop at the end of Phase I, but it's very useful to keep a copy of Phase I results only in a folder so that multiple different Phase II or III runs can be carried on from it using this copy-paste procedure.
- Note that in Phase II the result of the fit is saved (DNN or cubic-OLUTs) but I did not have the time to save the result of the optimization of the stepsize, the latter are of course used in Phase III but simply written in the logs if we want to see what happened and retrieve the derived value of the optimal stepsize `eps_opt`.
- Finally do not be surprised if you see in the logs that OLUTs are created and saved even though you are using the DNN, it is indeed done but OLUTs are not used, that should be removed at some point but requires some refactoring of the code.
