# Can we use forward differencing only, for spins and tides?

## Comparing acceptance rates

| | Central | Forward |
|:-|-:|-:|
| Acc phase1 | 92.5% | 92.8% |
| Acc phase3 - 11300  | 49.8% | 54.2% |

- Surprisingly the "forward" runs seems to do a bit better but I think this is just up to statistical fluctuations and to the little differences when resetting the scales at the beginning of phase3.

- Let's do a full run but starting phase3 with the same scales for both:

| | Central with same scales | Forward |
|:-|-:|-:|
| N_traj = 11300 - Acc  | 52.8% | 54.2% |
| End of run - Acc | 51.5% | 52.4% |
| End of run - N_traj | 140 000 | 170 000 |
| ACL_max | d_L: 27 | d_L: 34 |
| End of run - N_SIS | 5 185 | 5 000 |
| Phase1 run time | 24.6h | 18.5h |
| Phase3 run time | 7.1h | 7.4h |
| Total run time | 31.8h | 26.0h |

**Caveat: the above run time figures concern a run starting at 40Hz**


## Comparing fitting regressions
- However on phase2 the dnn seemed to have more problem with the forward differencing than with the central

### Validation set = LAST 10% of phase1 trajectories

#### Regression fit LAST 10%
![](dnn_reg_compare_central_vs_forward.png)

#### dlogL vs qpos LAST 10%

![](dlogL_vs_qpos_central_vs_forward.png)

<!-- #### Central differencing LAST 10%

![](216540x_train_vs_24060x_val_central.png)

#### Forward differencing LAST 10%

![](217080x_train_vs_24120x_val_forward.png) -->


### Validation set = FIRST 10% of phase1 trajectories

#### Regression fit FIRST 10%
![](dnn_reg_compare_central_vs_forward_first10pct.png)

#### dlogL vs qpos FIRST 10%

![](dlogL_vs_qpos_central_vs_forward_first10pct.png)
