# Comparing at the end of the two runs the corner plots of SIS

![](corner_central_vs_forward/corner_central_vs_forward_5186_theta_jn-psi-luminosity_distance.png)

![](corner_central_vs_forward/corner_central_vs_forward_5186_chirp_mass-reduced_mass-mass_1-mass_2.png)

![](corner_central_vs_forward/corner_central_vs_forward_5186_mass_1-mass_2-chi_1-chi_2-lambda_1-lambda_2.png)

![](corner_central_vs_forward/corner_central_vs_forward_5186_ra-dec-geocent_time.png)

![](corner_central_vs_forward/corner_central_vs_forward_5186_theta_jn-psi-luminosity_distance-mass_1-mass_2-dec-ra-geocent_time-chi_1-chi_2-lambda_1-lambda_2.png)
