# Tuning stepsize with Newton and bisection methods

## Starting stepsize: `epsilon = 0.005`
- Running 800 trajectories for each stepsize tested
```
20:10 gwhmc INFO    : Summary of the stepsize optimization:
                          5.00e-03    1.02e-02    2.14e-02    4.85e-02    3.22e-02    2.63e-02    2.37e-02
Acc                       0.779040    0.701266    0.528517    0.205056    0.372836    0.454429    0.519846
C = 1/(a*(eps-8*eps^2)         267         152         107         164         112         106         100
18:32 gwhmc INFO    : Setting the stepsize for Phase III to epsilon = 2.37e-02.
18:32 gwhmc INFO    : Stepsize optimization lasted 73min.
```
![costadhoc_vs_eps_800_eps0_0.005.png](costadhoc_vs_eps_800_eps0_0.005.png)

## Starting stepsize: `epsilon = 0.03`
- Running 800 trajectories for each stepsize tested
```
20:10 gwhmc INFO    : Summary of the stepsize optimization:
                         3.00e-02  6.00e-02    4.24e-02   3.57e-02    3.27e-02
Acc                      0.459770      -1.0    0.319629   0.420849    0.375489
C = 1/(a*(eps-8*eps^2)         95       inf         111         93         110
20:10 gwhmc INFO    : Setting the stepsize for Phase III to epsilon = 3.57e-02.
20:10 gwhmc INFO    : Stepsize optimization lasted 35min.
```
![costadhoc_vs_eps_800_eps0_0.005.png](costadhoc_vs_eps_800_eps0_0.06.png)

- Running 1600 trajectories for each stepsize tested
```
13:32 gwhmc INFO    : Summary of the stepsize optimization:
                          3.00e-02  6.00e-02    4.24e-02   3.57e-02    3.27e-02
Acc                       0.400788      -1.0    0.314851   0.394412    0.356863
C = 1/(a*(eps-8*eps^2)         109       inf         113         99         116
13:32 gwhmc INFO    : Setting the stepsize for Phase III to epsilon = 3.00e-02.
13:32 gwhmc INFO    : Stepsize optimization lasted 78min.
```

<!-- ```py
locs_eps = [5.00e-03, 1.02e-02, 2.14e-02, 4.85e-02, 3.22e-02, 2.63e-02, 2.37e-02]
accs_eps = [0.779040, 0.701266, 0.528517, 0.205056, 0.372836, 0.454429, 0.519846]
costs_adhoc_eps = [267.423015, 151.988673, 106.547705, 164.349085, 112.135342, 106.020284, 100.045867]

locs_eps = [3.00e-02, 6.00e-02, 4.24e-02, 3.57e-02, 3.27e-02]
accs_eps = [0.459770, 0.1, 0.319629, 0.420849, 0.375489]
costs_adhoc_eps = [95.394737, 320, 111.631531, 93.204611, 110.263776]

delta = 5
plt.plot(locs_eps, costs_adhoc_eps, marker='+', markersize=10, linestyle = 'None')
for i in range(len(locs_eps)):
  txt = f'{i+1}: ({locs_eps[i]:.2e}, {accs_eps[i]:.1%})'
  # plt.annotate(txt, (locs_eps[i], costs_adhoc_eps[i] + delta))
  plt.annotate(i+1, (locs_eps[i], costs_adhoc_eps[i] + delta))
plt.xscale('log')
plt.xlabel(r'$\epsilon$')
plt.ylabel(r'$C^{adhoc}$')
# plt.legend()
plt.savefig('/Users/marcarene/projects/python/gwhmc/Technical_notes/tuning_epsilon_flowchart/costadhoc_vs_eps_800_eps0_0.06.png')
plt.close()
``` -->

## Explaining the new condition in bisection method
- How to decide whether to set the mid point as the new lower or upper boundary ?
- If `r_acc(eps_mid) < 40%`, set `eps_mid` as the new upper boundary: it simply replaces the upper boundary derived from Newton's method.
- If `r_acc(eps_mid) >= 40%`: given the two boundaries `[eps_low, eps_high]` and their costs, there are two possibilities:
  1. Either `C(eps_low) <= C(eps_high)`
  2. Or `C(eps_low) > C(eps_high)`
- If we are in the case `C(eps_low) < C(eps_high)`, whichever will `C(eps_mid)` evaluate to, we shall set `eps_mid` as the new upper boundary:
![cost_vs_eps_Cepslow_lt_Cepshigh.png](cost_vs_eps_Cepslow_lt_Cepshigh.png)
- If we are in the other case, `C(eps_low) > C(eps_high)`
  - If the `C(eps_mid) > max(C(eps_low), C(eps_high))`: this case should not happen theoretically as we expect a well-typed parabolla.  Uncertainty on the estimation of `r_acc` might however lead to such a case. If so, favor small stepsizes by setting also `eps_mid` as the new upper boundary:
  ![cost_vs_eps_Cepslow_gt_Cepshigh_0.png](cost_vs_eps_Cepslow_gt_Cepshigh_0.png)
  - If `C(eps_mid) < max(C(eps_low), C(eps_high))`, set `eps_mid` as the new lower boundary
  ![cost_vs_eps_Cepslow_gt_Cepshigh_1.png](cost_vs_eps_Cepslow_gt_Cepshigh_1.png)
<!-- ```py
eps_bound = [2.14e-02, 4.85e-02]
accs_eps_bound = [0.528517, 0.205056]
costs_eps_bound = [106.547705, 164.349085]
eps_mid = [3.22e-02] * 3
costs_eps_mid = [90, 120, 190]

eps_bound = [1.02e-02, 2.14e-02]
accs_eps_bound = [0.701266, 0.528517]
costs_eps_bound = [151.988673, 106.547705]
eps_mid = [1.48e-02]
costs_eps_mid = [180]
# eps_mid = [1.48e-02] * 2
# costs_eps_mid = [120, 90]


plt.plot(eps_bound, costs_eps_bound, marker='+', markersize=10, linestyle = 'None')
plt.plot(eps_mid, costs_eps_mid, marker='*', markersize=10, linestyle = 'None', color='red')
plt.ylim([70, 190])
plt.xscale('log')
plt.xlabel(r'$\epsilon$')
plt.ylabel(r'$C^{adhoc}$')
# plt.legend()
plt.savefig('/Users/marcarene/projects/python/gwhmc/Technical_notes/tuning_epsilon_flowchart/cost_vs_eps_Cepslow_gt_Cepshigh_0.png')
plt.close()
``` -->
