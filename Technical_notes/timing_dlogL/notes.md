# Understanding how long it takes to compute 1 dlogL

## Timing one logL computation

```py
logL = <s|h> - 0.5 * <h|h>
h = h_ifo = h_source * np.exp(i*2*pi*f*dt)
```
```py
dlogL = <s|dh> - <h|dh>
dlogL_forward = <s|h+> - <s|h> - [<h|h+> - <h|h>]
dlogL_central = <s|h+> - <s|h-> - <h|h+ - h->
```
| Code chunk | h_s | F+h+ + Fxhx| exp(i * 2pi * f * dt)| s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|----|:----:|:----:|:---:|:----:|:----:|
| nb calls for 1 `logL` | 1 | n_ifos | n_ifos | n_ifos | 2 x n_ifos |
| nb calls `dlogL_forward` | 1 | n_ifos | n_ifos | n_ifos | 2 x n_ifos |
| nb calls `dlogL_central` | 2 | 2 x n_ifos| 2 x n_ifos | 2 x n_ifos | 3 x n_ifos |

| Code chunk | h_source | F+h+ + Fxhx| exp(i * 2pi * f * dt)| s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|----|:----:|:----:|:---:|:----:|:----:|
| IMRPhenomD, 30Hz | **12.3 ms** | 1ms | 3.5 ms | 1.6 ms | 0.9 ms |
| IMRPhenomD_NRTidal, 30Hz | **25.4 ms** | 1ms | 3.5 ms | 1.6 ms | 0.9 ms |
| IMRPhenomD_NRTidal, 23Hz | **?? ms** | ?? ms | ?? ms | ?? ms | ?? ms |
| IMRPhenomPv2_NRTidal, 30Hz | **57.9 ms** | 1ms | 3.5 ms | 1.6 ms | 0.9 ms |
| IMRPhenomPv2_NRTidal, 23Hz | **?? ms** | ?? ms | ?? ms | ?? ms | ?? ms |


## Analysis IMRPhenomD with 8 parameters

### Caracteristics
- IMRPhenomD
- 8 parameters: aligned spins fixed and phase marginalization
- flow = 30 Hz => duration = 59 sec
```py
len(frequency_domain_strain) = 120833
ifo.frequency_mask.sum() = 119063
```

### Timing at the end of phase1

```
11:20 gwhmc INFO    : After 1300 trajectories of length 200
11:01 gwhmc INFO    : Total time for phase1: 27.0 hours
11:01 gwhmc INFO    : Average time to compute 1 numerical trajectory: 7.5e+01 sec
11:01 gwhmc INFO    : Average time to compute one numerical gradient dlogL = 373ms
```

### Timing of functions directly
#### num gradients non optimized
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  407 ms ± 10.7 ms ms
```

#### num gradients optimized
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  280 ms ± 3.85 ms ms
```

#### other
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  407 ms ± 10.7 ms ms
%timeit waveform_generator.frequency_domain_strain(self.likelihood.parameters):
  11.7 ms ± 576 µs
%timeit ifo.get_fd_time_translation(self.likelihood.parameters):
  3.5 ms ± 180 µs
%timeit signal_ifo[self.strain_data.frequency_mask] * exp_two_pi_f_dt:
  1.59 ms ± 108 µs
%timeit nwip(ifo.fd_strain, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration):
  902 µs ± 21.3 µs
```

### Timing analysis WITHOUT optimized computation
- Parameters contributions to `dlogL`

  |  | Diff method | h_source | F+h+ + Fxhx | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
  |:----|:----|:----:|:----:|:----:|:----:|:----:|
  | At point | - | 1 | 3 | 3 | 3 | 6 |
  | cos(iota) | Forward | 1 | 3 | 3 | 3 | 6 |
  | psi | Forward | 1 | 3 | 3 | 3 | 6 |
  | ln(D) | Forward | 0 | 3 | 3 | 3 | 6 |
  | ln(Mc) | Central | 2 | 6 | 6 | 6 | 9 |
  | ln(mu) | Central | 2 | 6 | 6 | 6 | 9 |
  | sin(dec) | Forward | 1 | 3 | 3 | 3 | 6 |
  | ra | Forward | 1 | 3 | 3 | 3 | 6 |
  | ln(tc) | Central | 2 | 6 | 6 | 6 | 9 |
  | **TOTAL** | - | 11 | 36 | 36| 36  | 63 |
  | **IMRPhenomD, 30Hz** | **369ms** | 11x12.3ms | 36x1ms | 36x3.5 ms | 36x1.6ms | 63x0.9ms |
  | **IMRPhenomD, 23Hz** | **???ms** | 11x??ms | ?x?ms | 36x??ms | 36x??ms | 63x??ms |


```py
swg = 11.7
fh = 1
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 11
n_fh = 36
n_exp2pifdt = 36
n_stimesexp = 36
n_nwip = 63

dlogL_timing = n_swg*swg + n_fh*fh + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
traj_timing = dlogL_timing * 200 / 1000
phase1_timing = traj_timing * 1500 / 3600
```
- Phase1 theoretical timing = 31.7h
- Phase1 measured timing after 100 traj:
  - total phase1: 2.2h
  - traj = 82.5 sec
  - dlogL = 412ms
- Phase1 projected after 1500: 34.4h


### Timing analysis WITH optimized computation
- Parameters contributions to `dlogL`

|  | Diff method | h_source | F+h+ + Fxhx | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|:----|:----|:----:|:----:|:----:|:----:|:----:|
| At point | - | 1 | 3 | 3 | 3 | 6 |
| cos(iota) | Forward | 1 | 3 | 0 | 3 | 6 |
| psi | Forward | 0 | 3 | 0 | 3 | 6 |
| ln(D) | Forward | 0 | 3 | 0 | 3 | 6 |
| ln(Mc) | Central | 2 | 6 | 0 | 6 | 9 |
| ln(mu) | Central | 2 | 6 | 0 | 6 | 9 |
| sin(dec) | Forward | 0 | 3 | 3 | 3 | 6 |
| ra | Forward | 0 | 3 | 3 | 3 | 6 |
| ln(tc) | Central | 0 | 6 | 6 | 6 | 9 |
| **TOTAL** | - | 6 | 36 | 15 | 36| 63  |
| **IMRPhenomD, 30Hz** | **273ms** | 6x12.3ms | 36x1ms | 15x3.5 ms | 36x1.6ms | 63x0.9ms |
| **IMRPhenomD, 23Hz** | **???ms** | 11x??ms | 36x?ms | 15x??ms | 36x??ms | 63x??ms |

```py
swg = 11.7
fh = 1
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 6
n_fh = 36
n_exp2pifdt = 15
n_stimesexp = 36
n_nwip = 63

dlogL_timing = n_swg*swg + n_fh*fh + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
traj_timing = dlogL_timing * 200 / 1000
phase1_timing = traj_timing * 1500 / 3600
```
- Phase1 theoretical timing = 19.8h
- Phase1 measured timing after 700 traj:
  - total phase1: 10h22miin
  - traj = 53 sec
  - dlogL = 267ms
- Phase1 measure projected after 1500: 22.2h


## Analysis IMRPhenomD with 10 parameters

### Caracteristics
- IMRPhenomD
- 10 parameters: aligned spins but phase marginalization
- flow = 30 Hz => duration = 59 sec
```py
len(frequency_domain_strain) = 120833
ifo.frequency_mask.sum() = 119063
```

### Timing at the end of phase1

```
11:20 gwhmc INFO    : After 1300 trajectories of length 200
11:01 gwhmc INFO    : Total time for phase1: 27.0 hours
11:01 gwhmc INFO    : Average time to compute 1 numerical trajectory: 7.5e+01 sec
11:01 gwhmc INFO    : Average time to compute one numerical gradient dlogL = 373ms
```

### Timing of functions directly
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  393 ms ± 3.5 ms ms
%timeit waveform_generator.frequency_domain_strain(self.likelihood.parameters):
  12.3 ms ± 74.1 µs
%timeit ifo.get_fd_time_translation(self.likelihood.parameters):
  3.5 ms ± 180 µs
%timeit signal_ifo[self.strain_data.frequency_mask] * exp_two_pi_f_dt:
  1.59 ms ± 108 µs
%timeit nwip(ifo.fd_strain, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration):
  902 µs ± 21.3 µs
```

### Timing analysis
- Parameters contributions to `dlogL`

  |  | Diff method | h_source | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
  |:----|:----|:----:|:----:|:----:|:----:|
  | At point | - | 1 | 3 | 3 | 6 |
  | cos(iota) | Forward | 1 | 0 | 3 | 6 |
  | psi | Forward | 0 | 0 | 3 | 6 |
  | ln(D) | Forward | 1 | 0 | 3 | 6 |
  | ln(Mc) | Central | 2 | 0 | 6 | 9 |
  | ln(mu) | Central | 2 | 0 | 6 | 9 |
  | sin(dec) | Forward | 0 | 3 | 3 | 6 |
  | ra | Forward | 0 | 3 | 3 | 6 |
  | ln(tc) | Central | 0 | 6 | 6 | 9 |
  | chi_1 | Central | 2 | 0 | 6 | 9 |
  | chi_2 | Central | 2 | 0 | 6 | 9 |
  | **TOTAL** | - | 11 | 15 | 48| 81  |
  | **IMRPhenomD, 30Hz** | **338ms** | 11x12.3ms | 15x3.5 ms | 48x1.6ms | 81x0.9ms |
  | **IMRPhenomD, 23Hz** | **???ms** | 11x??ms | 15x??ms | 48x??ms | 81x??ms |

```py
swg = 12.3
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 11
n_exp2pifdt = 15
n_stimesexp = 48
n_nwip = 81

timing = n_swg*swg + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
```



## Analysis for IMRPhenomD_NRTidal with 12 parameters - 59sec

### Caracteristics
- IMRPhenomD_NRTidal
- 12 parameters: aligned spins, tides but phase marginalization
- flow = 30 Hz => duration = 59 sec
```py
len(frequency_domain_strain) = 120833
ifo.frequency_mask.sum() = 119063
```

### Timing at the end of phase1

```
11:20 gwhmc INFO    : After 670 trajectories of length 200
11:20 gwhmc INFO    : Total time for phase1: 24.7 hours
11:20 gwhmc INFO    : Average time to compute 1 numerical trajectory: 132.6 sec
11:20 gwhmc INFO    : Average time to compute one numerical gradient dlogL = 662ms
```

### Timing of functions directly
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  672 ms ± 14.5 ms
%timeit waveform_generator.frequency_domain_strain(self.likelihood.parameters):
  25.4 ms ± 177
%timeit ifo.get_fd_time_translation(self.likelihood.parameters):
  3.5 ms ± 180 µs
%timeit signal_ifo[self.strain_data.frequency_mask] * exp_two_pi_f_dt:
  1.59 ms ± 108 µs
%timeit nwip(ifo.fd_strain, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration):
  902 µs ± 21.3 µs
```

### Timing analysis
- Parameters contributions to `dlogL`

|  | Diff method | h_source | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|:----|:----|:----:|:----:|:----:|:----:|
| At point | - | 1 | 3 | 3 | 6 |
| cos(iota) | Forward | 1 | 0 | 3 | 6 |
| psi | Forward | 0 | 0 | 3 | 6 |
| ln(D) | Forward | 1 | 0 | 3 | 6 |
| ln(Mc) | Central | 2 | 0 | 6 | 9 |
| ln(mu) | Central | 2 | 0 | 6 | 9 |
| sin(dec) | Forward | 0 | 3 | 3 | 6 |
| ra | Forward | 0 | 3 | 3 | 6 |
| ln(tc) | Central | 0 | 6 | 6 | 9 |
| chi_1 | Central | 2 | 0 | 6 | 9 |
| chi_2 | Central | 2 | 0 | 6 | 9 |
| lambda_1 | Central | 2 | 0 | 6 | 9 |
| lambda_2 | Central | 2 | 0 | 6 | 9 |
| **TOTAL** | - | 15 | 15 | 60 | 99 |
| **IMRPhenomD_NRTidal, 30Hz** | **619ms** | 15x25.4ms | 15x3.5ms | 60x1.6ms | 99x0.9ms |
| **IMRPhenomD_NRTidal, 23Hz** | **???ms** | 15x??ms | 15x??ms | 60x??ms | 99x??ms |

```py
swg = 25.4
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 15
n_exp2pifdt = 15
n_stimesexp = 60
n_nwip = 99

timing = n_swg*swg + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
```

- Parameters contributions to `dlogL` if forward on spins and tides

|  | Diff method | h_source | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|:----|:----|:----:|:----:|:----:|:----:|
| At point | - | 1 | 3 | 3 | 6 |
| cos(iota) | Forward | 1 | 0 | 3 | 6 |
| psi | Forward | 0 | 0 | 3 | 6 |
| ln(D) | Forward | 1 | 0 | 3 | 6 |
| ln(Mc) | Central | 2 | 0 | 6 | 9 |
| ln(mu) | Central | 2 | 0 | 6 | 9 |
| sin(dec) | Forward | 0 | 3 | 3 | 6 |
| ra | Forward | 0 | 3 | 3 | 6 |
| ln(tc) | Central | 0 | 6 | 6 | 9 |
| chi_1 | Forward | 1 | 0 | 3 | 6 |
| chi_2 | Forward | 1 | 0 | 3 | 6 |
| lambda_1 | Forward | 1 | 0 | 3 | 6 |
| lambda_2 | Forward | 1 | 0 | 3 | 6 |
| TOTAL | - | 11 | 15 | 48 | 87 |
| **IMRPhenomD_NRTidal, 40Hz** | **255ms** | 11x12.6ms | 15x1.5ms | 48x1.6ms | 87x0.9ms |
| **IMRPhenomD_NRTidal, 30Hz** | **487ms** | 11x25.4ms | 15x3.5ms | 48x1.6ms | 87x0.9ms |
| **IMRPhenomD_NRTidal, 23Hz** | **???ms** | 11x??.?ms | 15x?.?ms | 48x?.?ms | 87x?.?ms |

```py
swg = 25.4
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 11
n_exp2pifdt = 15
n_stimesexp = 48
n_nwip = 87

timing = n_swg*swg + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
```



```py
n_ifos = 3
swg = 25.4
fh = 1
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 12
n_fh = 18 * n_ifos
n_exp2pifdt = 5 * n_ifos
n_stimesexp = 18 * n_ifos
n_nwip = 31 * n_ifos

dlogL_timing = n_swg*swg + n_fh*fh + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
traj_timing = dlogL_timing * 200 / 1000
phase1_timing = traj_timing * 1500 / 3600
```
- Phase1 theoretical timing = 19.8h
- Phase1 measured timing after 700 traj:
  - total phase1: 10h22miin
  - traj = 53 sec
  - dlogL = 267ms
- Phase1 measure projected after 1500: 22.2h






## Analysis for IMRPhenomD_NRTidal with 12 parameters - 64sec

### Caracteristics
- IMRPhenomD_NRTidal
- 12 parameters: aligned spins, tides but phase marginalization
- flow = 30 Hz => duration = 64 sec
```py
# 64 sec
len(frequency_domain_strain) = 120833
ifo.frequency_mask.sum() = 119063

# 128 sec
len(frequency_domain_strain) = 120833
ifo.frequency_mask.sum() = 119063
```

### Timing at the end of phase1

### Timing of functions directly
- 64 sec
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  900 ms ± 14.5 ms ??
  1.05 s ± 53.1 ms
  843 ms ± 15.8 ms # (with inner_product_mask_fixed)
  945 ms ± 16.3 ms # (with inner_product_mask_fixed)
  1.05 s ± 74 ms # (with inner_product_mask_fixed)
%timeit waveform_generator.frequency_domain_strain(self.likelihood.parameters):
  31.3 ms ± 283 µs
%timeit ifo.get_detector_response_geocent(waveform_polarizations, parameters):
  1.08 ms ± 5.15 µs
  1.4 ms ± 40.9 µs
%timeit ifo.get_fd_time_translation(self.likelihood.parameters):
  4.05 ms ± 29.1 µs
%timeit signal_ifo[self.strain_data.frequency_mask] * exp_two_pi_f_dt:
  650 µs ± 10.7 µs
%timeit ifo.get_detector_response(waveform_polarizations, self.likelihood.parameters, exp_two_pi_f_dt=ifo.exp_two_pi_f_dt_at_point):
  2.19 ms ± 34.1 µs
%timeit ifo.inner_product(template_plus):
  2.4 ms ± 34.3 µs
%timeit nwip(ifo.fd_strain, template_plus, ifo.psd_array, ifo.strain_data.duration):
  1.15 ms ± 93.9 µs
```
- 128 sec
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  1.88 s ± 18.9 ms # (with inner_product_mask_fixed)
%timeit waveform_generator.frequency_domain_strain(self.likelihood.parameters):
  71.8 ms ± 298 µs
%timeit ifo.get_detector_response_geocent(waveform_polarizations, parameters):
  2.83 ms ± 39 µs
%timeit ifo.get_fd_time_translation(self.likelihood.parameters):
  9.66 ms ± 402 µs
%timeit signal_ifo[self.strain_data.frequency_mask] * exp_two_pi_f_dt:
  1.31 ms ± 6.4 µs
%timeit ifo.get_detector_response(waveform_polarizations, self.likelihood.parameters, exp_two_pi_f_dt=ifo.exp_two_pi_f_dt_at_point):
  4.83 ms ± 140 µs
%timeit ifo.inner_product_mask_fixed(ifo.template_at_point):
  3.47 ms ± 36.9 µs
```

### Why `ifo.inner_product()` takes longer than `nwip()`

```py
def inner_product(self, signal):
return gwutils.noise_weighted_inner_product(
    aa=signal[self.strain_data.frequency_mask],
    bb=self.strain_data.frequency_domain_strain[self.strain_data.frequency_mask],
    power_spectral_density=self.power_spectral_density_array[self.strain_data.frequency_mask],
    duration=self.strain_data.duration)

%timeit signal[self.strain_data.frequency_mask]:
  209 µs ± 27.5 µs
%timeit self.strain_data.frequency_domain_strain[self.strain_data.frequency_mask]:
  504 µs ± 1.62 µs
%timeit self.power_spectral_density_array[self.strain_data.frequency_mask]:
  305 µs ± 3.52 µs
```

- So "masking" an array from 131072 elements to 129153 takes 209 µs.
- Calling the `ifo.strain_data.frequency_domain_strain` is longer because the following is done internally each time:
```py
ifo.strain_data._frequency_domain_strain * ifo.strain_data.frequency_mask
```
- Calling the psd is longer because when `ifo.power_spectral_density_array` is called, the psd array is multiplied each time by the `ifo.strain_data.window_factor`.

I could create a new nwip function for my Interferometer Class which would be like:
```py
def inner_product_mask_fixed(self, signal_masked):
  return gwutilsnwip(
      aa=signal[self.strain_data.frequency_mask],
      bb=self.frequency_domain_strain_masked,
      power_spectral_density=self.power_spectral_density_array_masked_windowed,
      duration=self.strain_data.duration)
```

### Timing analysis
- Parameters contributions to `dlogL`


|  | Diff method | h_source | F+h+ + Fxhx | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|:----|:----|:----:|:----:|:----:|:----:|:----:|
| At point | - | 1 | 3 | 3 | 3 | 6 |
| cos(iota) | Forward | 1 | 3 | 0 | 3 | 6 |
| psi | Forward | 0 | 3 | 0 | 3 | 6 |
| ln(D) | Forward | 0 | 3 | 0 | 3 | 6 |
| ln(Mc) | Central | 2 | 6 | 0 | 6 | 9 |
| ln(mu) | Central | 2 | 6 | 0 | 6 | 9 |
| sin(dec) | Forward | 0 | 3 | 3 | 3 | 6 |
| ra | Forward | 0 | 3 | 3 | 3 | 6 |
| ln(tc) | Central | 0 | 6 | 6 | 6 | 9 |
| chi_1 | Central | 2 | 6 | 0 | 6 | 9 |
| chi_2 | Central | 2 | 6 | 0 | 6 | 9 |
| lambda_1 | Forward | 1 | 3 | 0 | 3 | 6 |
| lambda_2 | Forward | 1 | 3 | 0 | 3 | 6 |
| **TOTAL** | - | 12 | 54 | 15 | 54 | 93 |
| **IMRPhenomD_NRTidal, 30Hz 64sec** | **642ms** | 12x31.3ms | 54x1.24ms | 15x4.05ms | 54x0.65ms | 93x1.2ms |
| **IMRPhenomD_NRTidal, 23Hz 128sec** | **1.5sec** | 12x71.8ms | 54x2.8ms | 15x9.7ms | 54x1.3ms | 93x3.5ms |

```py
# 64 sec
swg = 31.3
fphpfchc = 1.1
exp2pifdt = 4.05
stimesexp = 0.65
nwiprd = 1.2

# 128 sec
# swg =71.8
# fphpfchc =2.8
# exp2pifdt = 9.7
# stimesexp = 1.3
# nwiprd = 3.5

n_swg = 12
n_fphpfchc = 54
n_exp2pifdt = 15
n_stimesexp = 54
n_nwiprd = 93

timing = n_swg*swg + n_fphpfchc*fphpfchc + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwiprd*nwiprd
```















## Analysis forecasts IMRPhenomPv2_NRTidal with 16 parameters

|  | Diff method | h_source | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|:----|:----|:----:|:----:|:----:|:----:|
| At point | - | 1 | 3 | 3 | 6 |
| cos(iota) | Forward | 1 | 0 | 3 | 6 |
| psi | Forward | 0 | 0 | 3 | 6 |
| ln(D) | Forward | 1 | 0 | 3 | 6 |
| ln(Mc) | Central | 2 | 0 | 6 | 9 |
| ln(mu) | Central | 2 | 0 | 6 | 9 |
| sin(dec) | Forward | 0 | 3 | 3 | 6 |
| ra | Forward | 0 | 3 | 3 | 6 |
| ln(tc) | Central | 0 | 6 | 6 | 9 |
| a_1 | Central | 2 | 0 | 6 | 9 |
| a_2 | Central | 2 | 0 | 6 | 9 |
| tilt_1 | Central | 2 | 0 | 6 | 9 |
| tilt_2 | Central | 2 | 0 | 6 | 9 |
| phi_12 | Central | 2 | 0 | 6 | 9 |
| phi_jl | Central | 2 | 0 | 6 | 9 |
| lambda_1 | Central | 2 | 0 | 6 | 9 |
| lambda_2 | Central | 2 | 0 | 6 | 9 |
| TOTAL | - | 23 | 15 | 84 | 135 |
| **IMRPhenomPv2_NRTidal, 30Hz** | **1.640sec** | 23x57.9ms | 15x3.5ms | 84x1.6ms | 135x0.9ms |
| **IMRPhenomPv2_NRTidal, 23Hz** | **3.284sec?** | 23x??ms | 15x??ms | 84x??ms | 135x??ms |

```py
swg = 57.9
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 23
n_exp2pifdt = 15
n_stimesexp = 84
n_nwip = 135

timing = n_swg*swg + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
```

```py
n_ifos = 3
swg = 57.9
fh = 1
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 20
n_fh = 26 * n_ifos
n_exp2pifdt = 5 * n_ifos
n_stimesexp = 26 * n_ifos
n_nwip = 43 * n_ifos

dlogL_timing = n_swg*swg + n_fh*fh + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
traj_timing = dlogL_timing * 200 / 1000
phase1_timing = traj_timing * 1500 / 3600
```
