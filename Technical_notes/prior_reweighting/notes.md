# Reweighting samples with new priors

- The HMC samples the target distribution with implicit priors defined by the _trajectory functions_ used for each parameter. Let's name them the **trajectory-priors: $\pi_{traj}$**.
- Since (for the moment) we equate the gradient of the posterior distribution to that of the likelihood function, it means that **the trajectory-priors are uniform in the trajectory functions**.
- To compare the raw HMC's results with LALInference's, we need to reweight the samples with LALInference priors using the following formula for the weights:

$$ p_{\pi_{LI}}(\theta) = \frac{\pi_{LI}(\theta)}{\pi_{traj}(\theta)} p_{\pi_{traj}}(\theta)$$
$$ w = \frac{\pi_{LI}(\theta)}{\pi_{traj}(\theta)}$$

- Where $\theta$ refers to the set of parameters used for the analysis, $p_{\pi_{LI}}$ and $p_{\pi_{traj}}$ are the posterior distributions of samples produced by the HMC using respectively LALInference priors and trajectory-priors.

## Comparisions of priors used between $\pi_{traj}$  and $\pi_{LI}$

|Parameter|$\pi_{traj}$ is uniform in...|$\pi_{traj}(\theta)$|$\pi_{LI}$ is uniform in...|$\pi_{LI}(\theta)$|
|:-|:-|:-|:-|:-|
|$\theta_{JN}$|Uniform in $cos(\theta_{JN})$|$sin(\theta_{JN})/2$|Uniform in $cos(\theta_{JN})$|$sin(\theta_{JN})/2$|
|$\psi$|Uniform in $\psi$|$1/\pi$|Uniform in $\psi$|$1/\pi$|
|$d_L$|Uniform in $ln(d_L)$|$\frac{1}{ln(d_L^{max}/d_L^{min})}\frac{1}{d_L}$|Uniform in comoving volume and source frame time| ~$\frac{3}{d_{L_{max}}^3} \frac{d_L^2}{1+z}$ ? |
|$\mathcal{M}$|Uniform in $ln(\mathcal{M})$|$\frac{1}{ln(\mathcal{M}^{max}/\mathcal{M}^{min})}\frac{1}{\mathcal{M}}$|Constrained between $[\mathcal{M}_{min}, \mathcal{M}_{max}]$||
|$\mu$|Uniform in $ln(\mu)$|$\frac{1}{ln(\mu^{max}/\mu^{min})}\frac{1}{\mu}$|||
|$m_1$|Constrained between $[m_1^{min}, m_1^{max}]$||Uniform in $m_1$|$\frac{1}{m_1^{max} - m_1^{min}}$|
|$m_2$|Constrained between $[m_2^{min}, m_2^{max}]$||Uniform in $m_2$|$\frac{1}{m_2^{max} - m_2^{min}}$|
|$RA$|Uniform in $RA$|$\frac{1}{2\pi}$|Uniform in $RA$|$\frac{1}{2\pi}$|
|$DEC$|Uniform in $sin(DEC)$|$cos(DEC)/2$|Uniform in $sin(DEC)$|$cos(DEC)/2$|
|$t_c$|Uniform in $ln(\delta t_c)$|$\frac{1}{ln(\delta t_c^{max}/\delta t_c^{min})}\frac{1}{\delta t_c}$|Uniform in $[ t_c - 0.05, t_c + 0.05]$|$1/0.1$|
|$\chi_1$|Uniform in $\chi_1$|$1/0.1$|So called "zprior"|$\frac{-1}{2\chi_{max}} ln(\frac{|\chi_{i,z}|}{\chi_{max}})$|
|$\chi_2$|Uniform in $\chi_2$|$1/0.1$|So called "zprior"|$\frac{-1}{2\chi_{max}} ln(\frac{|\chi_{i,z}|}{\chi_{max}})$|


## Jacobian for masses

- When reweighting for mass parameters, one must account for the jacobian if priors have been set in different coordinate systems, here $(\mathcal{M}, \mu) \rightarrow (m_1, m_2)$, and the formula of the weights for masses parameters becomes:

$$ w_{masses} = \frac{\pi_{LI}(m_1)\pi_{LI}(m_2)}{\pi_{traj}(\mathcal{M})\pi_{traj}(\mu)} \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}$$
where $\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}$ is the determinant of the jacobian matrix defined for the transformation $(\mathcal{M}, \mu) \rightarrow (m_1, m_2)$.

- We will compute the jacobian for the inverse transformation, easier to derive, and then use the property:
$$\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)} = \mathcal{J}_{(m_1, m_2) \rightarrow (\mathcal{M}, \mu) }^{-1}$$
- The jacobian matrix when going from $(m_1, m_2) \rightarrow  (\mathcal{M}, \mu)$ is:

$$
J_{(m_1, m_2) \rightarrow  (\mathcal{M}, \mu)}
=  
\begin{bmatrix}
    \frac{\partial{\mathcal{M}}}{\partial{m_1}} & \frac{\partial{\mathcal{M}}}{\partial{m_2}} \\
    \frac{\partial{\mathcal{\mu}}}{\partial{m_1}} & \frac{\partial{\mathcal{\mu}}}{\partial{m_2}}
\end{bmatrix}
$$

- With:
$$ \mathcal{M} = \frac{(m_1 m_2)^{3/5}}{(m_1+m_2)^{1/5}} $$
$$ \mu = \frac{(m_1 m_2)}{(m_1+m_2)} $$

- Leading to:
$$ \frac{\partial \mathcal{M}}{\partial m_1} = \frac{\mathcal{M}}{5} \frac{2m_1+3m_2}{m_1(m_1+m_2)} $$
$$ \frac{\partial \mathcal{M}}{\partial m_2} = \frac{\mathcal{M}}{5} \frac{3m_1+2m_1}{m_2(m_1+m_2)} $$
$$ \frac{\partial \mu}{\partial m_1} = \frac{m_2^2}{(m_1+m_2)^2} $$
$$ \frac{\partial \mu}{\partial m_2} = \frac{m_1^2}{(m_1+m_2)^2} $$

- We can compute the determinant of the jacobian:
$$
|J_{(m_1, m_2) \rightarrow  (\mathcal{M}, \mu)}| = \mathcal{J}_{(m_1, m_2) \rightarrow  (\mathcal{M}, \mu)} = \frac25 \mathcal{M} \frac{m_1 - m_2}{(m_1+m_2)^2} $$

- Finally we can write:
$$ \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)} = \frac52 \frac{1}{\mathcal{M}} \frac{(m_1+m_2)^2}{m_1 - m_2}$$

- Note: I checked that, using these formula for $\frac{\partial \mathcal{M}}{\partial m_i}$, I recover:
$$\mathcal{J}_{(\mathcal{M}, q) \rightarrow (m_1, m_2)} = \frac{m_1^2}{\mathcal{M}}$$
as stated in eq. (21) of [_J. Veitch et al_](https://link.aps.org/doi/10.1103/PhysRevD.91.042003) PE paper.

## Drawback of the reweighting procedure
- Once we have computed the weights of all the $N_{traj}$ raw-samples, we will use a standart python method which will draw $N_{LI}$ samples according to the weights computed, leading a set of $N_{LI}$ reweighted samples which follow $\pi_{LI}$ priors.
- It is hence obvious that $N_{LI} < N_{traj}$ and **reweighting samples results in a net loss on the samples gathered**.
- The maximum number of reweighted samples we can extract without being biased is given by:
$$ N_{LI}^{max} = \frac{(\sum_i w_i)^2}{\sum_i w_i^2} $$
- At the beginning I was only reweighting SIS samples, to be certain that the reweighted samples are independent as well, meaning in the end I only got a small fraction of the total number of correlated samples I had gathered, in my case only 7% of them...
- I was doing the following:
  1. Compute integrated autocorrelation length on $N_{traj}$ correlated-samples: typically L = 20
  2. Thin the chains to get SIS samples. With L=20, N_sis = 5% N_corr
  3. Compute weights on the N_sis samples.
  4. Draw the max fraction (here 7%) from N_sis according to the weights
  5. In the end I am left with only 0.35% of the correlated samples I had gathered...!
- Now I am doing this, which I think is legid as well and produces 4 times more reweighted sis:
  1. Compute weights on $N_{traj}$ correlated-samples
  2. Draw the max fraction from N_corr according to the weights, here 7%, but keep those 7% in the ordered in the chain as they were before the draw.
  3. Compute integrated autocorrelation length on the reweighted ordered samples: now L = 5.
  4. Thin the chain to get reweighted SIS samples.
- Hence with the last procedure I have 4 times more SIS reweighted samples compared with before. **I am still losing 75% of my SIS samples...**

## Notes on prior definitions

### Change of variables
#### Link between "uniform in function $f$" and "$\pi_f$ = ..."
- Let $\theta$ be any of the former parameters. Defining a prior $\pi_{f}$ that is uniform over $[\theta_{min}, \theta_{max}]$ in $f(\theta)$ for any integrable function $f$, we can write the prior probability for $f(\theta)$ to be part of $[f(a), f(b)]$ as:
$$ p(f(\theta) \in [f(a), f(b)]) = \int_{f(a)}^{f(b)} \pi_{f-uniform}(f(\theta)) df(\theta) $$

$$ p(f(\theta) \in [f(a), f(b)]) = \frac{1}{f(\theta_{max})-f(\theta_{min})} \int_{f(a)}^{f(b)} df(\theta) $$

$$ p(f(\theta) \in [f(a), f(b)]) = \frac{1}{f(\theta_{max})-f(\theta_{min})} \int_{f(a)}^{f(b)} \frac{df(\theta)}{d\theta} d\theta$$

$$ p(\theta \in [a, b]) = \frac{1}{f(\theta_{max})-f(\theta_{min})} \int_{a}^{b} f'(\theta) d\theta$$

- Hence meaning that:
$$ \pi_{f-uniform}(\theta) = \frac{|f'(\theta)|}{f(\theta_{max})-f(\theta_{min})} $$

- The absolute value accounts for the fact that if $f'$ is negative over $[a, b]$, then actually it means that $b<a$ and we swap $a$ with $b$ and get rid of the negative sine.
- For instance if $f(\theta) = cos(\theta)$ we retrieve that a uniform distribution in $cos(\theta)$ corresponds to a prior
$$ \pi_{cos-uniform}(\theta) = \frac{sin(\theta)}{2} $$

#### Link with the jacobian
- Let's say we have sampled in parameter $\phi = \phi(\theta)$
$$ p(\phi \in [a, b]) =  \int_{a}^{b} \pi^{\phi}(\phi) d\phi$$

$$\pi^{\theta}(\theta) = \pi^{\phi}(\phi(\theta)) \mathcal{J}_{\theta \rightarrow \phi}(\theta)$$

- Multiple change of variables
$$ p((m_1, m_2) \in ...) =  \int \int \pi^{m_1}(m_1) \pi^{m_2}(m_2) dm_1 dm_2$$
$$ p((m_1, m_2) \in ...) =  \int \int \pi^{m_1}(m_1(\mathcal{M}, \mu))\; \pi^{m_2}(m_2(\mathcal{M}, \mu))\; \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}(\mathcal{M}, \mu)\;  d\mathcal{M} d\mu$$

- Hence from this I understand that:
$$\pi^{(\mathcal{M}, \mu)}(\mathcal{M}, \mu) =  \pi^{m_1}(m_1(\mathcal{M}, \mu))\; \pi^{m_2}(m_2(\mathcal{M}, \mu))\; \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}(\mathcal{M}, \mu)$$

- But if the right hand side is not separable in $(\mathcal{M}, \mu)$, I don't see how we can get an analytical expression for $\pi^{\mathcal{M}}$ and $\pi^{\mu}$...

- The fact that the left hand side is not separable in variable: ie $\pi^{(\mathcal{M}, \mu)}((\mathcal{M}, \mu)) \neq \pi^{\mathcal{M}}(\mathcal{M}) \pi^{\mu}(\mu)$, is not such a problem. Indeed for the HMC computing the gradient of the log-posterior requires the gradient of the log-likelihood and the gradient of the log-prior:
$$\frac{\partial ln(\mathcal{L}(q^\mu)\pi(q^\mu))}{\partial q^\mu} = \frac{\partial ln(\mathcal{L}(q^\mu))}{\partial q^\mu} + \frac{\partial ln(\pi(q^\mu)))}{\partial q^\mu}$$

- And we could express:
$$\frac{\partial ln\pi(q^\mu))}{\partial \mathcal{M}} = \frac{\partial ln\pi^{(\mathcal{M}, \mu)}(\mathcal{M}, \mu)}{\partial \mathcal{M}}$$

$$\frac{\partial ln\pi(q^\mu))}{\partial \mathcal{M}} = \frac{\partial ln\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}(\mathcal{M}, \mu)}{\partial \mathcal{M}}$$

### Geometric motivations for priors on $d_L$ $RA$ and $DEC$

#### Uniform in luminosity volume
- The natural choice is a uniform prior on the volume in space the source belongs to; hence:
$$ p(source \in dV) = \frac{dV}{V_{tot}}$$

- Since the total volume considered $V_{tot} = \frac{4\pi}{3} d_{L_{max}}^3$.

$$  p(cbc \in dV) = \frac{d_L^2 cos(DEC)\; dd_L\; dRA\; dDEC}{\frac{4\pi}{3} d_{L_{max}}^3} $$
$$  p(cbc \in dV) = \frac{d_L^2}{d_{L_{max}}^3 / 3}dd_L \; \frac{1}{2\pi}dRA\; \frac{cos(DEC)}{2}dDEC $$

- Hence leading to:
$$ \pi(d_L) = \frac{d_L^2}{d_{L_{max}}^3 / 3} $$
$$ \pi(RA) = \frac{1}{2\pi} $$
$$ \pi(DEC) = \frac{cos(DEC)}{2} $$

#### Uniform in comoving volume
- The former prior on $d_L$ "would  distribute mergers uniformly throughout a Euclidean universe, allowing us to be agnostic towards any cosmology; however, beyond a red-shift  of ~1, the  difference  between  a prior which is uniform in the comoving (source) frame and uniform in luminosity volume is large." [Bilby-GWTC-1 paper]

$$ \pi(z) \propto \frac{dV_c}{dz} $$


#### Uniform in comoving source frame volume
$$ \pi(z) \propto \frac{1}{1+z}  \frac{dV_c}{dz} $$
- "The additional factor of $(1+z)^{-1}$ accounts for time dilation" [Bilby-GWTC-1 paper].
- "the extra 1+z is due to doppler shifting of the source frame time." [bilby.gw.prior.UniformSourceFrame() doc]
- Me: does the $(1+z)^{-1}$ factor renders the geometrical situtation at merger time, ie the fact that the source was closer to us at that time and hence belonged to a smaller _shell volume_ around the earth ?
