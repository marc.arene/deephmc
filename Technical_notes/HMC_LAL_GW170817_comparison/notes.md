# GW170817 HMC - LALInference-MCMC comparison

- HMC: 5000 SIS
- LAL: 5008 SIS
- IMRPhenomD_NRTidal, 12 dimensional analysis.
- LALInference-MCMC is run locally on my laptop
- No marginalization over calibration, marginalizing over phase at coalescence, same PSDs and segment files.


## 64 sec for HMC

### SNR - logL
- Raw plot

![corner_HMC5001_vs_LI5008_snr-logL.png](64sec/corner_HMC5001_vs_LI5008_snr-logL.png)

- Arranged plot
  - Removing artifically 12 points from HMC chain for which mf_snr < 31
  - Removed the first samples of LAL chain where logL = 376

![corner_HMC5309_vs_LI5008_snr-logL.png](64sec/corner_HMC5309_vs_LI5008_snr-logL.png)


### Frame independent
![corner_HMC5001_vs_LI5008_theta_jn-psi-luminosity_distance.png](64sec/corner_HMC5001_vs_LI5008_theta_jn-psi-luminosity_distance.png)

![corner_HMC5001_vs_LI5008_ra-dec-geocent_time.png](64sec/corner_HMC5001_vs_LI5008_ra-dec-geocent_time.png)

![corner_HMC5001_vs_LI5008_chi_1-chi_2-chi_eff.png](64sec/corner_HMC5001_vs_LI5008_chi_1-chi_2-chi_eff.png)

![corner_HMC5001_vs_LI5008_lambda_1-lambda_2-lambda_tilde-delta_lambda_tilde.png](64sec/corner_HMC5001_vs_LI5008_lambda_1-lambda_2-lambda_tilde-delta_lambda_tilde.png)


### Detector frame
![corner_HMC5001_vs_LI5008_mass_1-mass_2-chirp_mass-mass_ratio.png](64sec/corner_HMC5001_vs_LI5008_mass_1-mass_2-chirp_mass-mass_ratio.png)

### Source frame
![corner_HMC5001_vs_LI5008_mass_1_source-mass_2_source-chirp_mass_source-mass_ratio.png](64sec/corner_HMC5001_vs_LI5008_mass_1_source-mass_2_source-chirp_mass_source-mass_ratio.png)


## 59 sec for HMC
### Frame independent
![corner_HMC5001_vs_LI5008_theta_jn-psi-luminosity_distance.png](59sec/corner_HMC5001_vs_LI5008_theta_jn-psi-luminosity_distance.png)

![corner_HMC5001_vs_LI5008_ra-dec-geocent_time.png](59sec/corner_HMC5001_vs_LI5008_ra-dec-geocent_time.png)

![corner_HMC5001_vs_LI5008_chi_1-chi_2-chi_eff.png](59sec/corner_HMC5001_vs_LI5008_chi_1-chi_2-chi_eff.png)

![corner_HMC5001_vs_LI5008_lambda_1-lambda_2-lambda_tilde-delta_lambda_tilde.png](59sec/corner_HMC5001_vs_LI5008_lambda_1-lambda_2-lambda_tilde-delta_lambda_tilde.png)


### Detector frame
![corner_HMC5001_vs_LI5008_mass_1-mass_2-chirp_mass-mass_ratio.png](59sec/corner_HMC5001_vs_LI5008_mass_1-mass_2-chirp_mass-mass_ratio.png)

### Source frame
![corner_HMC5001_vs_LI5008_mass_1_source-mass_2_source-chirp_mass_source-mass_ratio.png](59sec/corner_HMC5001_vs_LI5008_mass_1_source-mass_2_source-chirp_mass_source-mass_ratio.png)
