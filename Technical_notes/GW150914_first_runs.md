## Scales settings
- using `offset = 1e-7 ` for `\ln\delta tc` is to small since the duration of the signal is 3sec, thus a round off occurs and the numerical gradient of `\ln\delta tc` is effectively zero
  ```
  09:35 gwhmc INFO    : log_likelihood_gradient:
     cos_theta_jn           : 274.7804855560364
     psi                    : -23.116969208091632
     log_luminosity_distance: 212.3448788699559
     log_chirp_mass         : 8556.259069434169
     log_reduced_mass       : -5878.789813384107
     sin_dec                : -6950.947926638933
     ra                     : 375.7374001364876
     log_geocent_duration   : 0.0
     chi_1                  : -1264.7249012844263
     chi_2                  : -318.14139883272
  ```
  => using an offset of `1e-6`: `log_geocent_duration   : 73853.34`
  => using an offset of `1e-5`: `log_geocent_duration   : 88623.84`
- The FIM is singular with a condition number `~ 1e17`, hence scales derived don't make sens.
```
From                          FIM with spins  From FIM without spins  Scale Chosen
cos_theta_jn                     6055.493484             5835.085203   5835.085203
psi                              4432.292780             4270.991996   4270.991996
log_luminosity_distance         10207.214330             9835.694885   9835.694885
log_chirp_mass                      0.054518                0.016412      0.016412
log_reduced_mass                    0.058629                0.033898      0.033898
sin_dec                           706.484026              680.770352    680.770352
ra                               4518.402114             4353.946594   4353.946594
log_geocent_duration               97.666800               94.131390     94.131390
chi_1                               7.782736                     NaN      7.782736
chi_2                               9.436821                     NaN      9.436821
```
The smallest value in the FIM is the coefficient linking `\ln\delta tc` with `\ln d_L`, `~1e-2`, which makes sens since these two parameters should not be correlated. Hence I decided to separate the FIM in two blocks to compute scales: first block containing only coefficients relative to `\costheta, \psi, \ln d_L` and the other block with all other coeffs. Scales computed look much better.
```
From                          FIM with spins  From FIM without spins  Scale Chosen
cos_theta_jn                        0.566637                0.566637      0.566637
psi                                 1.105260                1.105260      1.105260
log_luminosity_distance             0.746369                0.746369      0.746369
log_chirp_mass                      0.053614                0.014504      0.014504
log_reduced_mass                    0.050682                0.028726      0.028726
sin_dec                             0.018552                0.013352      0.013352
ra                                  0.111246                0.077821      0.077821
log_geocent_duration                0.083936                0.002164      0.002164
chi_1                               7.556313                     NaN      7.556313
chi_2                               9.159481                     NaN      9.159481
```
Thus finally only the two aligned-spin scales are being restricted to half-parameter range.
- Phase 1: acceptance rate after 30 trajectories is only around 73%. Plotting trajectories shows that quite a lot of bounces on the equal mass line take place, but the trajectories has time to get close to it and the gradients of the log priors in chirp-mass and reduced-mass diverges enough to distabilize the trajectory.
