# How to set the offset when computing derivatives w.r.t. `geocent_time`

- For this matter `geocent_time` (or the log of it) stands out w.r.t. the other parameters. Indeed since lalsimulation uses a GPS time as input, we have to deal with a number having 10 digits before the decimal place while we measure it with a precision at the 4th decimal place. Typically for GW170817:
`tc = 1187008882.4312 +0.0005 -0.0006`
This leads to two numerical issues

## Can't use a small offset for the numerical derivative
- Since the scale on which tc varies is ~1e-4 sec, we want to use an offset ~1e-7 when computing the its derivative. However that would imply using numbers with a 17 digits precision, which exceeds the standard python float precision. The `Decimal` library can handle this but since it's not used amongst the different packages used by the hmc (bilby, lalsimulation...) our Decimal number would be casted into python float and our trick would be useless.
```py
ipdb> type(gps_tc)
<class 'float'>
ipdb> gps_tc
1126259462.4161134
ipdb> gps_tc + 1e-7
1126259462.4161134
ipdb> decimal.Decimal(gps_tc)
Decimal('1126259462.416113376617431640625')
ipdb> decimal.Decimal(gps_tc) + decimal.Decimal(1e-7)
Decimal('1126259462.416113476617431641')
ipdb> float(decimal.Decimal(gps_tc) + decimal.Decimal(1e-7))
1126259462.4161134
# Same with np.float128
ipdb> np.float128(gps_tc)
1126259462.4161133766
ipdb> np.float128(gps_tc) + 1e-7
1126259462.4161134766
ipdb> float(np.float128(gps_tc) + 1e-7)
1126259462.4161134
```

## Can't take the derivative w.r.t. log(gps time)
- What about considering `ln(tc_gps)` then ?
```py
tc_gps_plus = np.exp(np.log(tc_gps) + 1e-7)
            = tc_gps * np.exp(1e-7)
            = tc_gps * (1 + 1e-7 + epsilon)
tc_gps_plus = tc_gps + tc_gps * 1e-7
tc_gps_plus = tc_gps + ~1000 sec
```
So because the gps time is ~1e10, we would try to compute a central derivative with points offseted by about 1000 sec which makes no sense.

## Using the duration of the signal
Using the duration of the signal and not the gps time appears both more sensible and accurate. For BNS, the signal will last some tens of seconds when for BBHs it would stay some tenth of a second in the ifo band. However we still need to provided lalsimulation with a gps_time_plus and gps_time_minus.
```py
tc_gps_plus = np.exp(np.log(duration) + 1e-7) + start_time
            = duration * np.exp(1e-7) + start_time
            = duration * (1 + 1e-7 + epsilon) + start_time
            ~ tc_gps + duration * 1e-7
```
So now instead of offsetting the gps time by `tc_gps * 1e-7 ~ 1000 sec`, we offset it by `duration * 1e-7`

For BNS signal where duration ranges from 30 sec to 90 sec, this leads to an offset ~ 1e-6 which the computer can handle on the gps time.

However for BBH signal where duration ~ 0.2 sec, this leads to an offset ~ 1e-8 which is too much precision to ask for, the result being that the numerical derivative is always computed equal to zero.

## Why not just using tc instead of log(tc) with offset = 1e-6 ?
- I'm still getting non-sense scales:
- offset_geocent_time = 1e-6
```bash
                        cos_theta_jn           psi        ln(D)    ln(Mc)    ln(mu)    sin(dec)           ra  geocent_time      chi_1      chi_2
From FIM with spins     11689.326912  10111.576820  9589.894251  0.085517  0.089648  562.126581  6447.607557     49.707834  12.803633  14.850939
From FIM without spins  11762.633458  10174.939002  9650.018740  0.023241  0.046499  565.652465  6488.049363     50.029324        NaN        NaN
Scale Chosen            11689.326912  10111.576820  9589.894251  0.023241  0.046499  562.126581  6447.607557     49.707834  12.803633  14.850939
```
- Surprisingly when increasing the offset on geocent_time, the scales seem to behave better:
- offset_geocent_time = 1e-4
``` bash
                        cos_theta_jn        psi      ln(D)    ln(Mc)    ln(mu)  sin(dec)         ra  geocent_time      chi_1     chi_2
From FIM with spins        41.145150  38.894301  33.924438  0.160724  0.120102  1.973155  22.639440      0.194359  15.475191  17.64037
From FIM without spins     17.714249  20.874734  14.562402  0.025337  0.046574  0.843035   9.675273      0.071466        NaN       NaN
Scale Chosen               17.714249  20.874734  14.562402  0.025337  0.046574  0.843035   9.675273      0.071466  15.475191  17.64037
```
- offset_geocent_time = 1e-2
``` bash
                        cos_theta_jn        psi      ln(D)    ln(Mc)    ln(mu)  sin(dec)        ra  geocent_time      chi_1      chi_2
From FIM with spins        12.320986  17.529194  10.150868  0.094217  0.082120   0.57907  6.645346      0.000818  13.197059  15.277462
From FIM without spins      2.289882  13.897363   2.258190  0.021547  0.040905   0.00716  0.108739      0.000749        NaN        NaN
Scale Chosen                2.289882  13.897363   2.258190  0.021547  0.040905   0.00716  0.108739      0.000749  13.197059  15.277462
```
- offset_geocent_time = 1.0
``` bash
                        cos_theta_jn        psi     ln(D)   ln(Mc)    ln(mu)  sin(dec)        ra  geocent_time      chi_1      chi_2
From FIM with spins        11.763340  17.284476  9.707222  0.08487  0.079428  0.551311  6.327724       0.06651  12.560956  14.559556
From FIM without spins      2.289255  13.892263  2.257672  0.02107  0.040598  0.007133  0.108225       0.06651        NaN        NaN
Scale Chosen                2.289255  13.892263  2.257672  0.02107  0.040598  0.007133  0.108225       0.06651  12.560956  14.559556
```
