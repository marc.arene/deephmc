# Comparing different lengths for analytical trajectories in phase3

- In the HMC, a behavior which might slow down the algorithm is going around in circles several times on the same hamiltonian path and ending somewhere where we were half way through the trajectory already.
- What we are looking for is an optimal trajectory length which is long enough to allow the chain to travel far and decorrelate rapidly from the previous position, but not too long such that it would make loops.
- Hence we compare the first 100 pure analytical trajectories ran right after phase1 using different lengths
- The conclusion is that our choice of uniform draw around 100 steps seems to be the right one since the chain travels

![phase3_len_compare_full_2.png](phase3_len_compare_full_2.png)
