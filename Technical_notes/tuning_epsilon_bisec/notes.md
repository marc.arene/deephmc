# Tuning  epsilon and n_srt

## Tuning epsilon with a (sort of) bisection method

- Shape of the cost function we are dealing with:


  ![costadhoc_after_800_vs_analytical.png](costadhoc_after_800_vs_analytical.png)


- Method: with `n_srt` fixed to `6`
  1. Evaluate the adhoc-cost for three different stepsize values, like `[5.00e-03  1.00e-02  2.00e-02]`, with the constraint than an acceptance rate `<40%` is set to an infinite cost.
  2.
    - If the last stepsize produces the best cost, test a bigger stepsize of twice the previous value.
    - If the middle stepsize produces the best cost, test another stepsize falling in the middle with the second best stepsize.
    - If the first stepsize produces the best cost, test a stepsize twice smaller
  3. With the new cost evaluation, take the three new best stepsizes (which might be the same) and repeat step 2.
  4. After 6 stepsizes tested, pick the one which produced the best adhoc cost and note the expected acceptance rate.

### With 800 trajectories per stepsize evaluation
- Result when starting with `eps = [5.00e-03, 1.00e-02, 2.00e-02]`
```
                        5.00e-03  1.00e-02  2.00e-02  4.00e-02  3.00e-02  6.00e-02
Acc                     0.770781  0.685000  0.459318  0.360107  0.434783  0.162554
C = 1/(a*(eps-8*eps^2)  270       158       129            inf  102            inf
C_normed                2.679383  1.573000  1.284650       inf  1.000000       inf
```
![costadhoc_vs_eps_800.png](costadhoc_vs_eps_800.png)

<!-- ```py
eps = np.array([5.00e-03, 1.00e-02, 2.00e-02, 3.00e-02, 4.00e-02, 6.00e-02])
acc = np.array([0.770781, 0.685000, 0.459318, 0.434783, 0.360107, 0.162554])
costadhoc = 1/(acc*(eps-8*eps**2))
# cost = cost / cost.min()
# costadhoc = costadhoc / costadhoc.min()
plt.plot(eps, costadhoc, marker='.', label='1/(a*(eps-8*eps**2))'); plt.xscale('log'); plt.xlabel(r'epsilon'); plt.ylabel(r'cost-adhoc'); plt.legend(); plt.savefig('/Users/marcarene/projects/python/gwhmc/Technical_notes/tuning_epsilon_bisec/costadhoc_vs_eps_800.png'); plt.close()


eps = np.array([5.00e-03, 1.00e-02, 2.00e-02, 3.00e-02, 4.00e-02, 6.00e-02])
acc = np.array([0.770781, 0.685000, 0.459318, 0.434783, 0.360107, 0.162554])
costadhoc = 1/(acc*(eps-8*eps**2))
# cost = cost / cost.min()
# costadhoc = costadhoc / costadhoc.min()
epslin = np.logspace(np.log10(5e-3), np.log10(6e-2), 100)
epsmax = 0.07
amax = 1
costamax1 = 1 / (amax*(1 - epslin/epsmax)*epslin*(1-8*epslin))
amax = 0.8213
costamax08325 = 1 / (amax*(1 - epslin/epsmax)*epslin*(1-8*epslin))
plt.plot(eps, costadhoc, marker='.', label='Measured after 800 traj')
plt.plot(epslin, costamax1, label=r'Analytical with $r_{acc} = 1 - \epsilon/0.07$')
plt.plot(epslin, costamax08325, label=r'Analytical with $r_{acc} = 0.8213(1 - \epsilon/0.07)$')
plt.xscale('log')
plt.xlabel(r'$\epsilon$')
plt.ylabel(r'$C^{adhoc}$')
plt.title(r'$C^{adhoc} = 1 / (r_{acc}\epsilon(1-8\epsilon))$')
plt.legend()
plt.savefig('/Users/marcarene/projects/python/gwhmc/Technical_notes/tuning_epsilon_bisec/costadhoc_after_800_vs_analytical.png')
plt.close()
# plt.show()
``` -->



### With 400 trajectories per stepsize evaluation
- Result when starting with `eps = [5.00e-03, 1.00e-02, 2.00e-02]`
```
                         5.00e-03  1.00e-02  2.00e-02  7.50e-03  1.50e-02  3.00e-02
Acc                     0.797500  0.700000  0.374317  0.739348  0.619647  0.431217
C = 1/(a*eps)           3.244265  1.848073       inf  2.332957  1.391814  1.000000
C = 1/(a*(eps-8*eps^2)  2.568377  1.526669       inf  1.886220  1.202021  1.000000
```
![costadhoc_vs_eps_norm_case1.png](costadhoc_vs_eps_norm_case1.png)


- Result when starting with `eps = [5.00e-03, 2.00e-02, 4.00e-02]`
```
                         5.00e-03  2.00e-02    4.00e-02   8.00e-02  6.00e-02  3.00e-02
Acc                     0.797500  0.374317    0.330556   0.048780  0.185185  0.431217
C = 1/(a*eps)           3.315918  1.766180    1.000000   3.388194  1.190000  1.022086
C = 1/(a*(eps-8*eps^2)  2.568377  1.563442    1.093496   6.998291  1.701648  1.000000
```
![costadhoc_vs_eps_norm_case2.png](costadhoc_vs_eps_norm_case2.png)

<!-- ```py
eps = np.array([5.00e-03, 7.50e-03, 1.00e-02, 1.50e-02, 2.00e-02, 3.00e-02, 4.00e-02, 6.00e-02, 8.00e-02])
acc = np.array([0.797500, 0.739348, 0.700000, 0.619647, 0.374317, 0.431217, 0.330556, 0.185185, 0.048780])
cost = 1/(acc*eps)
costadhoc = 1/(acc*(eps-8*eps**2))
cost = cost / cost.min()
costadhoc = costadhoc / costadhoc.min()

plt.plot(eps, costadhoc, marker='.', label='1/(a*(eps-8*eps**2))'); plt.plot(eps, cost, marker='.', label='1/(a*eps)'); plt.xscale('log'); plt.xlabel(r'epsilon'); plt.ylabel(r'costs'); plt.legend(); plt.savefig('/Users/marcarene/projects/python/gwhmc/Technical_notes/tuning_epsilon_bisec/costs_vs_eps_norm.png'); plt.close()

eps = np.array([5.00e-03, 7.50e-03, 1.00e-02, 1.50e-02, 2.00e-02, 3.00e-02])
acc = np.array([0.797500, 0.739348, 0.700000, 0.619647, 0.374317, 0.431217])
costadhoc = 1/(acc*(eps-8*eps**2))
costadhoc = costadhoc / costadhoc.min()
plt.plot(eps, costadhoc, marker='.', label='1/(a*(eps-8*eps**2))'); plt.xscale('log'); plt.xlabel(r'epsilon'); plt.ylabel(r'costs'); plt.legend(); plt.savefig('/Users/marcarene/projects/python/gwhmc/Technical_notes/tuning_epsilon_bisec/costadhoc_vs_eps_norm_case1.png'); plt.close()


eps = np.array([5.00e-03, 2.00e-02, 3.00e-02, 4.00e-02, 6.00e-02, 8.00e-02])
acc = np.array([0.797500, 0.374317, 0.431217, 0.330556, 0.185185, 0.048780])
costadhoc = 1/(acc*(eps-8*eps**2))
costadhoc = costadhoc / costadhoc.min()
plt.plot(eps, costadhoc, marker='.', label='1/(a*(eps-8*eps**2))'); plt.xscale('log'); plt.xlabel(r'epsilon'); plt.ylabel(r'costs'); plt.legend(); plt.savefig('/Users/marcarene/projects/python/gwhmc/Technical_notes/tuning_epsilon_bisec/costadhoc_vs_eps_norm_case2.png'); plt.close()
``` -->


## Tuning n_srt
- 23500 trajectories run in Phase III with `n_srt in [3,6,9,12]`.
- Stepsize central value set at `3e-2` for analytical trajectories.

### Stepsize for hybrid and numerical trajectories fixed at `5e-3`

| `n_srt` | 3 | 6 | 9 | 12 |
| :--- | ---: | ---: | ---: | ---: |
| Acc | 45.5% | 40.5% | 43.7% | 41.6% |
| Analytical | 87.3% - 0.8h | 96.0% - 0.9h | 98.6% -  | 99.1% - 0.9h |
| Hybrid | 10.2% - 4.9h | 3.2% - 1.5h | 1.0% -  | 0.7% - 0.3h |
| Numerical | 2.5% - 6.0h | 0.8% - 1.9h | 0.4% -  | 0.2% - 0.6h |
| Duration PhIII | 13.3h | 4.8h | 2.6h | 1.9h |
| ACL_{max} | 60 | 28 | 16 | 16 |
| C_{tps} (s/SIS) | 122.0 | 20.7 | 6.3 | 4.6 |


### Stepsize for hybrid and numerical trajectories fixed at `1.5e-2`

| `n_srt` | 3 | 6 | 9 | 12 |
| :--- | ---: | ---: | ---: | ---: |
| Acc | 46.7% | 44.6% | 41.8% | 41.7% |
| Analytical | 87.1% - 0.7h | 96.3% - 0.8h | 98.1% - 0.8h | 98.5% - 0.9h |
| Hybrid | 9.4% - 4.5h | 2.5% - 1.2h | 1.1% - 0.5h | 0.6% - 0.3h |
| Numerical | 3.5% - 8.2h | 1.2% - 2.9h | 0.8% - 1.8h | 0.9% - 3.6h |
| Duration PhIII | 15.9h | 5.6h | 3.5h | 1.9h |
| ACL_{max} | 19 | 14 | 72 | 27 |
| C_{tps} (s/SIS) | 46.2 | 12.0 | 39.0 | 14.9 |

- Explanation for `ACL_{max}(n_srt=9) = 72`:
  - After 21346 trajectories, `ACL_{max} = 18`.
  - At traj 21346, >9 rejections in a row occured 14 times in a row lead to `ACL_{max} = 43`.
  - At traj 22065: 18 rejections in a row lead to `ACL_{max} = 75`.

## To do next
- double the nb of trajectories: 400 => 800
- plot unnormalized costs
- test nsrt = [3, 6, 9, 12]
