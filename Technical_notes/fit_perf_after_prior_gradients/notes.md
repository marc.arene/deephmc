# New fitting performances after taking prior gradient

## WITHOUT prior gradients, using 800 numerical trajectories

![](200325_compare_methods_reg_142560train_15840val.png)

These plots for the DNN actually refer to the non-optimized version, the regression was even better after, cf [here](https://gitlab.in2p3.fr/marc.arene/gwhmc/-/blob/log-post-grad/Technical_notes/compare_dnns_architectures/notes.md)

## WITH prior gradients, using 800 numerical trajectories

![](200417_compare_methods_reg_140940train_15660val.png)

## WITH prior gradients, using 1300 numerical trajectories
 ![](200421_compare_methods_reg_229860train_25540val.png)

## Performance comparisions on phase3
 || WITHOUT dlogP, 800 | WITH dlogP, 800| WITH dlogP, 1300
 |:-|-:|-:|-:|
 | Acceptance rate | 55% | 0% after 90 traj | 65% |
 | ACL max | psi: 18 |  | d_L: 16 |
 | Time / trajectory | 0.11 sec|  | 0.15 sec|
 | Time / SIS | 2.2 sec |  |  2.4 sec |
 |Phase 3 duration 5000 SIS| 3.0h |  | 3.3h |
