# Comparing forward and central differencing when computing the gradients dlogL
- We pick ~10 000 points over a set gathered during phase1 of a run, uniformly spread over the 1500 trajectories.
- For each of these we compute the relative error of forward differencing w.r.t. central where:
```py
dlogL_forward = (logL_plus - logL) / offset
dlogL_central = (logL_plus - logL_minus) / (2 * offset)
err = abs((dlogL_central - dlogL_forward) / dlogL_central)
```
- Here are the histograms of the errors for a run using IMRPhemomPv2_ROQ on the Binary1 injection:
![forward_vs_central_diff_offset1e-07](./forward_vs_central_diff_offset1e-07.png)
- They are not very informative because some relative error being very very big compared to the majority of others, the binning does not do well

- To have a better idea I artificially "squeezed" all errors > 10% into the same 10% bin:
![forward_vs_central_diff_offset1e-07_squeezed](./forward_vs_central_diff_offset1e-07_squeezed.png)

- Now here are the results comparing four different offsets, on the Binary1 injection using IMRPhenomPv2_ROQ:
![forward_vs_central_differencing_BNS1_ROQ_2](./forward_vs_central_differencing_BNS1_ROQ_2.png)

## Analysis for `chi_1` and `chi_2`

- Using IMRPhenomD, offset=1e-7, only on 100 points for the moment:

![forward_vs_central_diff_offset1e-07_rel_squeezed_chi](./forward_vs_central_diff_offset1e-07_rel_squeezed_chi.png)

- As I suspected, the high relative errors come from points where the gradient is small, but since the absolute error is ~constant => big relative error

![forward_vs_central_diff_offset1e-07_abs_chi](./forward_vs_central_diff_offset1e-07_abs_chi.png)

- I also made a run on 100 numerical trajectories with forward differencing on `chi_1` and `chi_2` in order to check the acceptance rate
  - Acc with central = 96% (out of 1200 traj)
  - Acc with forward = 98% (out of 47 traj)

- I also compared the two methods to compute the gradient: `logL+ - logL-` vs `<s-h|dh>` and they actually give somewhat different results:
  - For `chi_1`: `logL+ - logL-` is a lot worse with 10 points with `err_rel > 10%`, some getting `~500%`
  - For `chi_2`: `logL+ - logL-` is a lot better, all relative errors being constrained < 1%
