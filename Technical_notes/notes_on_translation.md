# TO DO (put this in Trello)
### Phase 1
- recomputation of sigpar when masses go beyond range
- write in a file Yann's randGen so I can import them in my code to check consistency of the chain generated in phase 1 with numerical gradients
  - Keep on printing all the different random numbers generated in gsl_ran_numbers.dat.
  - [Done] Then import it in python code to read from it
  - [Done] check that I get the same chain

### Phase 2
- why don't I get the same `fit_coeff_global` as Yann's ?
  - Look during q, r decomposition if I should append to my J matrix a column of ones (as for the svd decomposition)

### Phase 3
- HybridGradient() to translate. Using swig...?
- Rewrite `BoundaryCheck_On_Position()` with same if..else as `BoundaryCheck_On_Position_ParametrisedSpace()` to avoid RuntimeWarnings
- Try to see how those two previous functions could be merged into one. Which would be the one to keep to be the fastest?
- `TableGradient_Fit()`: Why don't I get the same `dlogL_pos[2]` whereas the other 2 are good ?
  - Yann and I have the same diagonal matrix: `s` for me, `w_Y` for him; except the 3 last ones: [7,8,9] which are very small for me but exactly set to 0 for him
  - Our matrices `u` and `v` have a lot of terms equals but many are different also
  - When Yann computes the pseudo_inverse, he sets the components which would have gone to infinity to zero. I did this manually and computed `fit_coeff_2` and the corresponding `Grad_2`.
  - I imported his `u`, `w`, and `v` matrices and then computed his `Grad_Yann` using my python code:
    ```python
    w_Y_inv = 1/w_Y
    w_Y_inv[[7,8,9]] = [0,0,0]
    pseudo_inverse_Yann = v_Y * w_Y_inv @ u_Y.T
    fit_coeff_Yann = pseudo_inverse_Yann @ dlogL_Y

    # Compute the analytical gradient
    Grad_Yann = fit_coeff_Yann @ np.append(q_pos_fit,1)
    ```
  - The result were at some point that:
    ```python
    Grad = -393.97
    Grad_Yann = 17.50
    Grad_2 = 17.81
    ```
    - 1st: I don't recover his result `dlogL_pos[2] = 35`, I get 17.50.
    - 2nd my `Grad_2` gives the same result.
    - One could think I have a factor 2 error, however if that was the case, it should also appear for `dlogL_pos[0]` and `dlogL_pos[3]`
    - I HAVE TO FIND THE REASON WHY I DONT RECOVER 35 !!!
  - Is my svd() decomposition better than Yann's?
    - When I import Yann's decomposition, it agrees to the matrix A decomposed at ~ 10^-5: `u_Y * w_Y @ v_Y.T - A_Yann.T ~ 1E-05`
    - The python decomposition agrees to 10^-10: `u * s_0 @ vh - A.T ~ 1E-10`.
    - Even comparing the python decomposition to Yann's matrix: `u * s_0 @ vh - A_Yann.T ~ 1E-07`
  - Try an svd on some simple test matrix in C and in python to make a simple comparision
  - Using n_2_fit = 50 instead of 20 I get the same `dlogL_pos[2] = -2.6` as Yann's. It's probably when we are in the case of singular values in A that are results start differing
  - The results are now in good accordance but I'm not exactly sure why...
    - I get the same `dlogL_pos[2] = 35` but the core code is the same, I hadn't changed it when this new result came up
    - What I thought would have been a good explanation is the fact that when I got the right python result: `dlogL_pos[2] = 35`, I had made changes in Yann's on the precision of printed numbers in output files: `%lf => %.17g`. Since this is used to print out the random numbers that I then read-in the python code to reproduce Yann's results, using a better precision on them could have been a good explanation on how I suddenly switched from `dlogL_pos[2] = 17` to `dlogL_pos[2] = 35`. However I tried reproducing `dlogL_pos[2] = 17` using `%lf` again but I still get `dlogL_pos[2] = 35`...!

# Phase 2
- To test that my QR fitting code gives the same coefficient as Yann's C code, I need to have as inputs exactly the same trajectory points. It's not the case because I can't use the same random number generator (randGen) as Yanns, hence my fitting points are different, hence the cubic fit coefficient are different. So I import from Yann's output the positions and values of the gradient of the fitting points
- Computation of the Jacobian with function `jac_cubic_QR()` tested and OK
- QR decomposition tests
  - the R matrix is the same => no actually
  - Q is not given explicitly by the gsl functions hence it's complicated to compare it with my python Q output
  - I checked that q*r = u_GSL in python
  - However I don't get the same `fit_coeff_global` as Yann's, so either it's because
    - the Q decomposition is not unique...? => if r is the same, q should also be
    - there's something wrong in Yann's code
    - there's something wrong in my code, and it should be in the following lines but I don't see where:
      ```python
      q, r = np.linalg.qr(u_GSL)
      pseudo_inverse = np.dot(np.linalg.inv(r), q.T) # has shape (220x400)
      LogPoints_transpose = LogPoints.T # has shape (9x400)

      fit_coeff_global = np.zeros((0,m_fit))
      for j in range(9):
          coeff_GSL = np.dot(pseudo_inverse, LogPoints_transpose[j]) # has shape (220,)
          # import IPython; IPython.embed()
          fit_coeff_global = np.concatenate((fit_coeff_global, coeff_GSL.reshape((1,m_fit))))
      ```
  - After importing in python Yann's Q, R, and LogPoints I arrive to the following conclusions:
    - Our Q, R decompositions are different
    - But they both recover the Jacobian: `np.allclose(Q_Yann_r @ R_Yann_r, q @ r) = True`
    - Our LogPoints are almost the same: `diff_LogP = 0.26 %`, but `np.allclose(LogPoints, LogPoints_Yann) = False`
    - My python fit_coeff are very different from Yann's: `diff_coeff_py_Yff = 56758 %`
    - fit_coeff using Q_Yann and R_Yann and python treatment with my LogPoints vs pure Yann's fit_coeff: `diff_coeff_Yc_Yff = 66 %`
    - fit_coeff using Q_Yann and R_Yann and python treatment with HIS LogPoints vs pure Yann's fit_coeff: `diff_coeff_Yc_Yff__LogPoints_Yann = 0.00002 %` and `np.allclose(fit_coeff_global_Yann_computed, fit_coeff_global_Yann_from_file) = True`. THIS MEANS THAT MY PYTHON TREATMENT IS CORRECT !
  - Comparision results on `dlogL_pos`:
    - Using fit_coeff directly imported from Yann's code: dlogL_pos_python and dlogL_pos_Yann are equal up to 5 digits.
    - Python treatment with FULLY Yann's input (Q, R AND LogPoints), ie using the `fit_coeff_global_Yann_computed` that have previously be shown to be the same as Yann's to a 0.00002% precision: dlogL_pos_python and dlogL_pos_Yann are close but not exactly the same. By eye there is ~2-5% difference, except for dlogL_pos[4] where it's more like 30%...
  - To do next:
    - At the beginning of phase 2, run a single numerical trajectory using 1000 points (q_pos_fit) for which I store each dlogL_pos_num[{1,4,5,6,7,8}]
    - The I do the cubic fit and for each of these points I calculate dlogL_pos_fit[{1,4,5,6,7,8}]
    - Plot those gradients: plt.plot(q_pos_fit[1], dlogL_pos_num[1], dlogL_pos_fit[1]), and do the overlap
    - q, r decomposition uses `jac_cubic_QR()` to compute the jacobian on the initial fit points and then derive the fitting coefficient, but it is not reused when computing the cubic function on new points (`GradientLogL_AnalyticalFit_Cubic() is used`); hence there might be a difference there
    - Implementation:
      - I coded up the 1000 step numerical trajectory at the end of phase 2
      - To be able to see if my analytical function approximates well the gradient w.r.t. the numerical one, I thought I should have quite a lot of fit points so that the cubic approximation is accurate enough
      - Hence in phase1 I ran more trajectories compared to what I usually did: 500, then 50.
      - I realised that only after a few trajectories, my points in the chain would differ quite a lot from Yann's. I had never realised this since before this test I would run only 2 numerical trajectories.
      - Explanation: my `BoundaryCheck_On_Position()` function didn't do the job correctly since 21 trajectories were broken out of 50: now corrected. Now when running 50 numerical trajectories in phase 1, the only difference I get with Yann at trajectory 46 where his code rejects the new point when mine accepts it.

- Look-up table creation with equivalent of `QuickSort_IndexArray_NR()`:
  - I used `np.argsort()` to retrieve the index of the sorted table wrt cos(inc)/psi/logD, hence no need to write an equivalent of `QuickSort_IndexArray_NR()`
  - I tested that I get exactly the same sorted table: `PointFit_SortCosInc` for instance as Yann's. For the comparison I has to use the same input points `qpos_olut_fit` imported from Yann's output as I can't generate exactly the same trajectories because I can't use the same random number generator


# General
- replace some `for` loops if possible
  - BNS_HMC_9D.py
  ```py
  with open(fileOutPointsFit_path, 'w') as fileOutPointsFit:
      for i in range(n_qpos_global_fit):
          for j in range(9):
              fileOutPointsFit.write("{:.17f} \t {:.17f}\t".format(qpos_global_fit[i][j],dlogL_global_fit[i][j]))
          fileOutPointsFit.write("\n")
  fileOutPointsFit.closed
  ```
  use `np.savetxt()`
  - Fit_NR.py
  ```py
  u_GSL = np.zeros((0,m_fit)) # need to initialize the matrix with this shape so we can do the concatenation of each line in the loop
  for i in range(n_pt_fit):
      afunc = jac_cubic_QR(PointsFit[i], 9, m_fit) # has shape (220,)
      u_GSL = np.concatenate((u_GSL, afunc.reshape((1,m_fit))))
  ```
  use ??
  - ctrl+F all other for loops to see if some could not be replaced by matrix multiplications etc...


xmgrace<xquartz
