# Runs comparing the influence of the 1000 vs 1500 numerical trajectories in phase1

- With IMRPhenomD on GW170817 injected data
- Comparing two runs having both a total of 51000 trajectories, but the 1st one running phase1 for 1000 numerical traj when the 2nd ones does it for 1500 traj.
- Bottom line is, surprisingly, the 500 extra numerical trajectories don't seem to make much of a difference for phase3...!

| |Phase1 duration|Nb qpos_fit|Phase2 duration|Acc Phase3 only|Nb of hybrid traj|Nb of num traj|Phase3 duration|Total run time|ACT|Nb of SIS|
|-|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|
|1000|22.1 hours|179800|147 sec|65.4%|2090 / 50 000|555 / 50 000|13.4 hours|35.6 hours|L=89, mu|574|
|1500|33.1 hours|267800|236 sec|68.5%|1730 / 49 500|514 / 49 500|12.5 hours|45.6 hours|L=95, mu|537|

### Autocorrelation plot of the run with 1000 numerical traj in phase1
![51000_autocorrelations_emcee_1000phase1.png](51000_autocorrelations_emcee_1000phase1.png)
### Autocorrelation plot of the run with 1500 numerical traj in phase1
![51000_autocorrelations_emcee_1500phase1.png](51000_autocorrelations_emcee_1500phase1.png)
