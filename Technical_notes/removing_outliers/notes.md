# Effect of removing outlying phase1 points on the phase2 cubic fit
- Polynomial fits are very sensitive to outliers hence I thought that could be my problem
- My idea was that some rare phase1 trajectories go somewhat far in parameter space (I guess because the initial drawn momenta are big) but are still accepted and hence recorded as future fitting points. However the values of the gradient on those trajectories have the same distribution as that of the global distribution when I would have expected them to be much smaller since they are located on the tail of the logL surface.
- So it made sense that these points could screw up the global cubic fit

- corner plots of the recorded values for future cubic fit after phase1 where we can see some outlying trajectories:
 ![294400_samples_corner](./294400_samples_corner.png)
- I found a standard way of removing outliers from a dataset: the Z score:
  ```python
  z = np.abs(stats.zscore(qpos_global_fit))
  qpos_global_fit_no_outliers = qpos_global_fit[(z < 2).all(axis=1)]
  ```
- And checked its effect on phase1 points:
- corner plots after removing ~ 50 000 outlying points:
 ![248828_samples_corner](./248828_samples_corner.png)
- However this does not seem to help since the regression plot on those new points only is as bad as before:
![248828_regression_cubic_TestedTrajs100_dlogL012345678](./248828_regression_cubic_TestedTrajs100_dlogL012345678.png)
