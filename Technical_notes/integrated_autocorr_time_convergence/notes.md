# Computing the integrated autocorrelation time of the chains
Good references here:
https://emcee.readthedocs.io/en/stable/tutorials/autocorr/
- Samples accumulated during the run are not independent from each other since the value, or position in parameter space, of each one depends on the previous samples. Not taking this autocorrelation of the chains into account would lead to an underestimated variance of the posterior distribution.
- To account for this, one needs to compute __L__, namely the __integrated autocorrelation time__ of each chain, which can be interpreted as "the number of steps that are needed before the chain “forgets” where it started".
- In a multi-dimensional problem as in our case, we have one _L_ per dimension and the maximum of those is taken as the _integrated autocorrelation time_ of the whole process.
- To get the statistically independent samples out of the correlated chain, one only needs to thin it by _L_.

## Computing _L_
- _L_ can be estimated using the normalized autocorrelation function of the chain through the following formula:
```
L_estimate = 1 + 2 * sigma_1^N(rho(lag))
```
where N is the length of the chain.

![100000_autocorrelations_emcee_2](./100000_autocorrelations_emcee_2.png)

- However, as one can see above, the autocorrelation functions get noisy at the end of the chain and one should stop the integration at the lag where the autocorrelation crosses zero. This lag is noted \\tau_max on the plot and for each chain the corresponding _L_ is given next to it.

## Convergence of _L_ as a function of the number of samples _N_
- Since we want to compute during the run the number of statistically independent samples (SIS) we are gathering, we shall compute _L_ on unfinished chain
- Even if the integration is stopped before the end of the chain, the shape of the autocorrelation function, and hence _L_, depends of the length of the chain.
- The true value of _L_ should be computed over an infinitely long chain. However we can show that our estimation of _L_ converges with the number of samples used which means we can restrict our computation to portions of the chain.
- This is all the more important that we want to compute the number of statistically independent samples (SIS) we are gathering __during the run__ and not at the end only, meaning we shall estimate _L_ on a non-finished chain

![L_convergence_2](./L_convergence_2.png) 

The peak appearing in `tc`, `ra` and `dec` means I should compute _L_ on length of at least ~1e4 to avoid an overestimation.

## Same plots but with runs showing more autocorrelations
- More autocorrelation on `psi` because its scale was badly set to a value way too small compare to its dynamical range, leading to a random walk behavior in this direction

![101500_autocorrelations_emcee](./101500_autocorrelations_emcee.png)
![L_convergence_bad_psi](./L_convergence_bad_psi.png)

- This run had shown more autocorrelations on the masses parameters

![100000_autocorrelations_emcee_bad_masses](./100000_autocorrelations_emcee_bad_masses.png)
![L_convergence_bad_masses](./L_convergence_bad_masses.png)
