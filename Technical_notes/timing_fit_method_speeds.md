# Comparing different fit methods speeds
- Contrary to what I would have thought, the cubic + OLUTs fit is a bit faster that the different regression methods I tried using the `sklearn` package.
  - Cubic + OLUTs
    - 1 analytical traj of 200 steps:       `0.55sec`
      - 6 dlogL cubic fitted:         1.47ms
      - 3 dlogL oluts fitted:         1.00ms
  - KNearestNeighbors
    - 1 analytical traj of 200 steps:       `0.71sec`
      - 9 dlogL KN fitted: 0.3ms * 9 = 2.7ms
  - RandomForest
    - 1 analytical traj of 200 steps:       `????sec`
      - 9 dlogL KN fitted: 0.5ms * 9 = 4.5ms
- I think these package are much faster when it comes to predict the values of a great number of samples at the time, however in our case we inherently need to predict the values of the gradient sample by sample...

## Thoughts about neural networks
- However I think a neural network could be of interest for different reasons:
  - it handles non linear problems very well so
  - I reckon it would deal really well with bimodal distributions since it is able to "catch features" in the data
  - it could be able to predict the values of all the gradients at the same time which would make being trained just once which would potentially make it a lot faster than our cubic+OLUTs fits
