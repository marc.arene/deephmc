## `[DONE]` AVANT LE DEPART EN DROME: AVOIR UN RUN PHASE 1 AVEC IMRPHENOMPV2 ROQ sur GW170817 !
  - `[DONE]` ROQ run completely diverging...
    - `[DONE]` try to run with IMRPhenomPv2 but without ROQ
    - `[DONE]` try to run on a simpler case than GW170817, like GW150914 where the ROQ basis won't be rescaled etc...
    - Why aren't my TaylorF2 runs working as well as before? That might be a clue why ROQ is not working as well...
    - `[DONE]` change the self.offset for likelihood_gradient
    - Am I going outside of the ROQ boundaries? Like chirp-mass etc ?
    - `[DONE]` In plot_traj.py, plot individual 0.5*p**2 to spot which parameter(s) is making the kinetic energy blow off
      - it's definitely `ln(mu)`
        - I noticed that in my dlogL_roq computation for `ln(mu)` I was using:
        `paru.parameters_new_dlnMc(self.likelihood.parameters_copy, self.offset)`
        instead of:
        `paru.parameters_new_dlnmu(self.likelihood.parameters_copy, self.offset)`
        - I had inverted the superior condition `parameters_plus['symmetric_mass_ratio'] > 0.25:` to inferior...
      - After correcting these silly mistakes, my first ROQ trajectories get accepted. When plotting the Hamiltonian along the traj, it has a weird oscillatory behavior which seems highly correlated with the kinetic energies of phi_c and psi
        - Can I run phase 1 with ROQ on a subset of parameters?
      - in `calculate_log_likelihood_gradient()` I had forgotten to copy back the original values of the parameters:
      `self.likelihood.parameters = self.likelihood.parameters_copy.copy()`
    - Analyse `python plot_trajectory.py ../__output_data/GW170817/bilbyoriented/SUB-D_012345678/PSD_1/0_100_2000_200_200_0.005_dlogL1/ROQ/ --traj_nb=25` at step ~175 there is a clear bump in the Hamiltonian clearly due to a bump in dlogL/dsindec
    - Analyse `python plot_trajectory.py ../__output_data/GW170817/bilbyoriented/SUB-D_012345678/PSD_1/0_15_2000_200_200_0.005_dlogL1/ROQ_scales_div_by_two/ --traj_nb=9`: there is a peak in dlogL/dlnmu
  - `[DONE]` Dividing the scales of phi_c and psi by two makes the Hamiltonian less oscillatory and improves the acceptance rate


## IMPROVING CODE STRUCTURE:
- `[DONE]` Let the approximant be an option `--approximant=`, and also part of the config.ini
- `[DONE]` Create a sub-directory based on the approximant name
- Include the ROQ boundaries in my boundaries
- Use the `likelihood_gradient` class even for TaylorF2 so that then I can easily switch to an object oriented structure?


## `[DONE]` ROQ TIME INTERPOLATION EFFECTS ON DLOGL
- `d_inner_h` in `likelihood.calculate_snrs()` is the steppy sub-product
- the reason why is that it is calculated using:
  ```py
  d_inner_h = interp1d(
      likelihood.weights['time_samples'][indices],
      d_inner_h_tc_array, kind='cubic', assume_sorted=True)(ifo_time)
  ```
- A step appears sometimes because varying `dec` or `ra` changes the `indices` on which the interpolation takes place, eg going from `indices = [1016, 1017, 1018, 1019, 1020]` to `indices = [1017, 1018, 1019, 1020, 1021]`
- This would cause a variation in the logL value of ~0.025 when the variation should be ~1e-5
- `indices` are computed the following way in `def _closest_time_indices(time, samples):`
  ```py
  closest = np.argmin(abs(samples - time))
  indices = [closest + ii for ii in [-2, -1, 0, 1, 2]]
  in_bounds = (indices[0] >= 0) & (indices[-1] < samples.size)
  return indices, in_bounds
  ```
- To smooth things (keeping an offset=1e-7) out I had to go from only 5 closest times to 17:
`indices = [closest + ii for ii in [-8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8]]`
- With an offset of 1e-6 I can use only the ??


## `[DONE]` fit_coeff precision and the constant offset of dlogL_ana along dlogL_num
- my fit_coeff being very big numbers (~1e17), I thought there would be a precision issue and hence decided to use np.longdouble, ie float128 at some point.
- In the `def QR_FittingMethod_Gradients(pt_fit, dlogL_fit)` replacing:
```
fit_coeff_global = pseudo_inverse @ dlogL_fit
```
with
```
fit_coeff_global = np.matmul(pseudo_inverse, dlogL_fit, dtype=np.longdouble)
```
improved the offset I had but did not wipe it out completely
- Another reason might be that I need more precision when printing pt_fit.dat and dlogL_fit.dat, maybe using `fmt = '%.25e'` instead of `fmt = '%.18e'`
=> didn't change anything
- try replacing the ` @ ` operator in:
```py
pseudo_inverse = vh.T * s_inv @ u.T
fit_coeff = pseudo_inverse @ dlogL_Y
Grad = fit_coeff @ np.append(q_pos_fit,1)
```
with `np.matmul(., ., dtype=np.longdouble)`

## Further analysis
- The new 1500 phase 1 trajectories didn't improve phase 2...
  - what if I try to fit the coeff with a subset of the 293000 points ?
  - there is still smth to check with the numerical precision of the fit_coeff and np.longdouble
  - I should still check that the analytical gradient evaluated on one of the fit points gives me the logL_fit back
  - `[DONE]` try on GW150914 because on GW170817 the ROQ seems to be railing on the priors
  - from the results of the bash scripts, it seems like maybe when `phi` is introduced, it screws up the fit of phase 2, both local and cubic.
  - What happens if I run phase1 with 1500 num traj without varying `phi` ?
    - `../__output_data/GW170817/SUB-D_0#2345678/PSD_1/1501_1500_2000_200_200_0.005_dlogL1/IMRPhenomPv2_ROQ/`
    - The fit starts being crappy when there is a peak in dlogL/dlnD at step ~620 no? Not really no
    - check some of the rejected trajectories in this run, like:
    ```
    ../Library/param_utils.py:116: RuntimeWarning: invalid value encountered in sqrt
    mass_1 = total_mass * (0.5 + np.sqrt(0.25 - symmetric_mass_ratio))
    21 -    rejected    - Acc = 95.2% - 0.08779 < 0.00000 - ??
    466 -    rejected    - Acc = 99.4% - 0.98598 < 0.00000 - ??
    ```
      - The reason for these rejection was because `self.log_likelihood_at_point` was computed in `def __init__()` instead of being recomputed at every point...
- make new run with 501_500 traj only, how does the analytical dlogL look like? It seems like it will be better fitted than after 1500 num traj, if yes I should understand why
- also, it looks like my run 1500 run is bouncing against boundaries in `ln(mu)` (and other parameters?) when I look at the corner plots
- `[DONE]` try replacing the local_fit with kneighbors routine
- `[DONE]` make the run with the scales not halved...?
- `[DONE]` make runs without phic AND without psi...?

- `[DONE]` check the warnings which appeared in SUB0345678:
```bash
../Library/BNS_HMC_Tools.py:94: RuntimeWarning: invalid value encountered in true_divide
d2_other_param_1 = (Matrix_ClosestPoint[other_param_index_1] - q_pos_fit[other_param_index_1])**2/SquaredScale[other_param_index_1]
../Library/BNS_HMC_Tools.py:95: RuntimeWarning: invalid value encountered in true_divide
d2_other_param_2 = (Matrix_ClosestPoint[other_param_index_2] - q_pos_fit[other_param_index_2])**2/SquaredScale[other_param_index_2]
```
  - they were only due to the fact that when `psi` is kept fixed, its scale is set to zero
- check out those peaks in the gradients when plotting the analytical gradient along the num one
  - they are due to a bounce of cos_inc boundary
  - why are dlogL/dcosinc and dlogL/dlnD not only very similar in shape but also in value? (ie almost identical)
- **About phic-psi bimodality**: When comparing the corner plot after phase1 of the run with all the parameters, where the fits are crappy, with that of the run without phic where the fit are "pretty good", I feel like the problem is that phic and psi are completely bimodal. Hence when the two parameters are included in the analysis, two modes appear in each combination of phic or psi with another parameter. Hence it seems natural that the cubic fit can't work. And moreover the local fit should then be done with all parameters presenting a bimodality, so it also makes sense that the OLUTs fit don't match that well
- QUESTION: if there is a bimodality within just one pair of parameters (as expected for lnD-cosinc), how can the cubic fit work for any of the parameters since it includes values explored on both mode for this pair of parameters ?

## Local OLUT fit analysis
- `[DONE]` do a breakpoint() in TableGradientFit in a case where the local fit works to watch how pt_Y and dlogL_Y look like
- check in both cases that `fit_coeff * pt_used_for_the_fit` gives back the right answer => if no it means the coeff are wrongly computed; compute the R**2 of the regression
- `[DONE]` try LUD decompostion instead of SVD => wasn't possible from what I remember...
- `[DONE]` compute the distance wrt to all the other parameters
- `[DONE]` why not directly selecting the 2000 points based on distance wrt to all parameters and not firstly wrt to the distance with the parameter we want to compute dlogL with?
- `[DONE]` did yann actually use the distances values to make the fit? => No
- `[DONE]` check that my TableGradientFit routines and Yann's give the same output when given in the same input => YES !
- `[DONE]` How can the 20 closest points have gradients between -10 and -30 and the fit gives me a fucking Grad=-70...! => means my points is a bit outside of the selected ones
- `[DONE]` try Yann's TableGradientFit in the case where it works with 0345678
- `[DONE]` Create a routine to make a corner plot of the 2000 and 200 points used for the fit
  - from these, it seems that when it's not working it's because for one of the parameters -lntc here- the 2000 and then 200 selected points' values for that parameter are far from the q_pos_fit[param_index] value... Humm really ?? Not sure anymore...
  - Try selecting the 10 000 closest points to cosinc and then
  - What if I first select the 2000 closest neighbors based on their distance wrt to all the params values, and then sub-select the 200 points around cosinc ?
  - I should try to do what supposedly KNN does, ie weight the influence of each point on the fit based on its distance to my point
  - Shouldn't I add some sort of correlation (FIM values ?) when computin the distance to account for the fact that some parameter have more influence than others when deriving the gradient wrt to one specific param, hence their distance matters more
  - `[DONE]` when using the OLUT, is it legid to compute the distance wrt to the parameter we are computing dlogL with ? => It's actually useless to compute the distance in the dimension the OLUT is sorted with since they will be very small compared to the distances in the other dimensions
- `[DONE]` method "true_nearest_neighbors" is too slow because I compute the distance wrt to ~ 300000 pts; which is probably why Ed and Yann went onto OLUTs
- `[DONE]` KNN is probably a lot more efficient than my own code
- `[DONE]` increase the order of the fit? 2nd order instead of linear?
- `[DONE]` At the end of phase1, why not updating the scales derived initially from the FIM with the distribution of points from phase1 ? => Cf `scale_vs_cov.md` for consistency results between the two.
- gather more points from phase1
- `[DONE]` try the linear fit keeping only points for which dlogL_Y is close to -15. If the fit works then try to understand how to only retain those points using some clever distance / metric etc... => not working
- make KNN work in the case where my linear fit works, ie SUB0345678
- `[DONE]` why not trying KNN to replace the global cubic fit ?
- why is KNN not working when I rescale the basis I ask it to fit ?
- if the OLUTs need a lot more point from phase1 than the cubic fit does, phase3 could be started by only running Hybrid trajectories for some time
- `[DONE]` make all the plots with different n1 and n2 but with the 600 000 phase1 points
- `[DONE]` run with phic up to 3000 phase1 traj: actually not necessary since I saw on the run without phic that pushing to 3000 num does not improve the fits
- `[DONE]` why not doing some brutal machine learning on phase1 ? Look at the jupyter notebooks from Asterics ! It may not be KNN that I should use
- `[DONE]` If it really is the bimodality in phic which is screwing the cubic fit, shouldn't I try to make the fit without considering phic ? => Not working
- shouldn't I try to derive some sort of correlation values between each gradient wrt to each parameter? at point of injection ?
- `[DONE]` introduce bounces on phic and psi instead of remapping into good range. Run phase1 with 1500 traj => didn't change either phase1 or phase3...
- since there is a bimodality in phic, did I make a mistake somewhere between the convertion of phic = 2 * phase ?
- `[DONE]` checkout out again that for IMRPhenomPv2, `phase` in the orbital phase and not the gravitational wave phase
- `[DONE]` Make new run on GW150914 up to 1500 num traj to confirm as well the bimodality in phic.
- In phase1, when setting --debug, maybe store the 10 first traj of the run and then only those trajectories where there is a `??` tag
- what was Ed's paper describing something to be careful with regarding phic and a plot that looks like my phic-psi histogram ?
- `[DONE]` make a run SUB01#345678 to see how it works. If it does work, then try any 8 parameter run, like SUB01234567#, to confirm that it's when phic and psi are together that things screw up. Actually maybe more SUB0123#5678, ie without Chirpmass since it's the parameter we measure the best. => The cubic fit is failing for SUB0123#5678 which means that indeed, apparently, it's when phic and psi are run together that things screw up...!
- For the cubic fit, what about selecting only every 5 points of accepted trajectory?
- If Yann got a phase3 Acc ~ 70%, maybe the gradient approximations that I am getting are actually sufficient to get this order of acceptance rate as well...!
- make a run on Binary1.ini to compare my phase1 with Yann's
- `[DONE]` I could sample `phase` instead of `phic`
- Implement supervised_reg in the phase2 and in phase3 trajectory: how does the 1000 steps traj responds? If it works as well as QR and OLUTs in the 02345678 case and as bad in the 012345678 case, then it proves that my phase2 fitting problem does not come from my phase2 code but from the phase1 data...
- In order to get some preliminary results, maybe I could start adding new parameters but keeping phi_c or psi constant..??!
- why the supervised fit is not working on single trajectory:
  - try the plot but on a phase1 trajectory => working perfectly!
  - train manually on a subset of phase1 and fit the first or last phase1 traj => not working at all...
  - this means that the model is overfitting the data which is in contradiction with the results I get with the script `supervised_regression.py`
  - Why don't I get a regression on the traj which is as good as what I get when I compute it over a whole test set which is part of the phase1 data ???
  - => It's because when splitting the phase1 data into a train and a test set which are randomly picked using `model_selection.train_test_split()`, then data from the test set are points of almost complete trajectories from the train set, hence the KN algorithm is able to find the points from the trajectory it comes from and approximate the gradient very well. However if the points of the test set are that of a new trajectory then there does not exists in the train set points close enough for the approximation to be satisfactory.
    - It would be interesting to know how points from phase1 are necessary to get a satisfactory approximation of the gradients with sklearn.
  - Actually Random Forests gives interesting results on the single traj plots compared to KNN.
    - surprisingly, the fit is the best for dlogL/dcosinc and dlogL/dlnD
  - I should try other 'method' algorithm than KNR and RF to check if there isn't one working better than RF.
  - If there needs a lot of points from phase1 to have good fitting results with sklearn, I could consider parallelizing phase1 since it's interest wouldn't be into starting a chain but into gathering points for the fit.
- Do I get the same phase1 corner plot, and phic bimodality, when running 0#2345678 with TaylorF2 ?
- How about a run SUB#12###### ?

### END OF SUMMER STATUS FOR ED:
- Introduced IMRPhenomPv2_ROQ
  - Phase1 Acc ~ 99%
  - Phase1 duration with 1500 traj ~ 3h
  - Phase2 not working better
- Phase2 fitting problems
  - In the full 9 parameter case
    - Cubic fit not working
    - Local OLUTs fit not working for `psi`, working relatively fine for `cos(inc)` and `lnD`
  - Removing either `phic` or `psi` from the estimation
    - both local and cubic fit work as expected
    - hence it seems that, for some reason, it's when both of them are being estimated that things screw up
    - when looking at the 9 parameters corner plots from phase1, `phic` has a bimodality which I think is unexpected
    - `phic` and `psi` are very much positively correlated, smth which I haven't seen in other corner plots results -did I?-
  - How come even the local OLUTs fit isn't working 'perfectly' in the psi case?
    - Used Yann's C code routine with my input data and it gives the same result => my python code for OLUTs isn't the culprit
    - I tried many different `(n1, n2)` configurations
    - I tried local 2nd and 3rd order fit but it didn't improve
    - In the end, after plotting the data, I think the trajectory I was focussing on was particularly outside of the phase1 gathered data points
  - Using machine learning on phase1 with the `sklearn` package
    - I thought it would be interesting to compare our fitting routines with those algorithms, plus it might shed light on why my phase2 isn't working
    - Different `Regressors` are available, like `KNeighborsRegressor`, `RandomForestRegressor`, `GradientBoostingRegressor`
    - `RandomForestRegressor` seems to be the best at fitting the gradients
    - However it's far from being as good as cubic fit
    - strangely the best fits it gives are for `cos(inc)` and `lnD`, which would be interesting for us since it's only using a global fit contrary to us using a local fit for those parameters

## LVC Warsaw ideas
- `[DONE]` run phase3 in the SUB02345678 case and check what the acceptance rate is !
  - AccP3 ~10%... => with the mistake `if 0 is sampler_dict['search_parameter_indices']`
  - However almost all my hybrid trajectories are accepted.
  - Looking at traj 3001 and the plot of anal vs num traj, I feel like it's only `psi` which is screwing things up
  - to confirm this, run with a hybrid2 version of the traj where I can choose which gradient is to be computed numerically, which is to be computed analytically either with cubic or OLUTs
- `[DONE]` run phase3 in the SUB01345678 case
  - run phase1 up to 1500 num traj first
- run phase3 on SUB0345678
- `[DONE]` Create a function that sorts out the OLUTs and call it in BNS_HMC_9D.py
- `[DONE]` Change end of phase3 s.t. the numbers in "Out of {} phase 3 trajectories : {} were analytical, {} were hybrid, {} were numerical" make sense
- `[TO DO !]` marginalize over `phic`
- `[TO DO !]` run over real GW170817 data
- SUB01345678: look at some phase1 rejected traj like: `538 -    rejected    - Acc = 99.3% - 0.12294 < 0.01008 - ??`
- `[DONE]` did I really make this mistake: `if 0 is sampler_dict['search_parameter_indices']` instead of `in` ?? yes...
- `[TO DO !]` create new bilby branch from the new master and add my personal updates
- Normally with my code I should get the same results for phase3 whether I run it with pure analytical trajectories or with fullhybrid traj where I set `search_parameter_indices_local_fit_full = -1`. However when I compare the first dlogL_pos that's being computed here is what I get:
  - AnalyticalGradientLeapfrog_ThreeDetectors_Trajectory():
  ```py
      array([-1.24156077e+01,  0.00000000e+00,  6.63549793e+00,
              1.15892978e+00, -1.30112715e+05,  1.27781553e+04,
              1.41242676e+01,  1.75074738e+02,  3.66366375e+05])
  ```
  - FullHybridGradientLeapfrog_ThreeDetectors_Trajectory()
  ```py
      array([-1.24156077e+01,  0.00000000e+00,  6.63549793e+00,
              1.15892978e+00, -1.30057809e+05,  1.27762939e+04,
              1.41911621e+01,  1.75134033e+02,  3.66371125e+05])
  ```
  - How come the cubic fit dlogL are changing ???
  - It looks like it comes from a numerical inaccuracy in the leapfrog as shown by:
  ```py
    ipdb> np.cos(np.arccos(cos_inc))
    -0.42977204036426914
    ipdb> cos_inc
    -0.429772040364269
  ```
- `[DONE]` run full 9 parameter case since I don't have the `0 is sampler_dict['search_parameter_indices']` error anymore...!
  - => Acc = 24%
  - run with fullhybrid = 0,2,3: => Acc = 30%
- `[TO DO !]` try neural networks with Keiras. What about gaussian processes ?
- `[DONE]` What about renormalizing phase1 data with a `qpos_scaler` and `dlogL_scaler` and then make the cubic fit? That might lead to smaller fit_coeff and a better precision.
- `[TO DO !]` In hybrid trajectories: instead of recording the numerical gradients computed and then update the oluts, why not at each step of the trajectory updating the oluts with the new q_pos_fit and dlogL using `np.searchsorted()` ? I should time both to check what is the fastest. If so I should do the same for numerical trajectories in phase3 => create a function `NumericalGradientLeapFrog_Phase3()`
- `fit_coeff_global` inaccuracy:
  - I was not writing the file with enough precision since the default one with numpy is 1e18 but I had coeffs of ~1e20 !
  - Strangely, even when writing to file with enough precision, an inaccuracy seems to take place as shown below:
  ```py
    ipdb> fit_coeff_global.max()
    4.1345629855536314762e+20
    ipdb> print('{:.30e}'.format(fit_coeff_global.max()))
    4.134562985553631641600000000000e+20
  ```
  - here `fit_coeff_global` was not loaded from any file but computed in cached memory after phase1.
  - `[DONE]` comparing the cubic fitted gradient values computed when using the "inaccurate" fit_coeff loaded from file vs those cached in memory it turns out that they are different (encore heureux) at ~1% precision but that does not explain the fact it does not overlapp the numerical gradients. The plots look exactly identical => I think having such big fit_coeff means they are inherently inaccurate. Rescaling could/should fix this ! => No...
    - `[DONE]` in `plot_regression_phase1.py`: check that with my `CubicFitRegressor` class system I get the same fit_coeff -on the 1500 trajectories as train set- as in my runs. I could even set the number of traj for test set to 1000 and check than the coeff I get are the same as that for my run with 500 num traj
    - `[DONE]` produce cubic regression plots in both cases 0#2345678 and 012345678 to compare them.
  - `[TO DO !]` Run on Binary1.in and check if I am getting a bimodality in phic-psi. Am I getting the same distributions as Yann's?
  - `[DONE]` Is my run on analytical PSDs legid since I am still uploading the roq weights from the spikee psds runs? The gradients computed and printed at beginning of the run don't corresponds between the roq ones and the 'manual' ones...
  - `[DONE]` what about removing outliers qpos in phase1 since polynomial regression are sensitive to them?
  - `[TO DO !]` Make a run just in the phic-psi plane: plot in 3D the logL: with the scattered points but also on a grid? make a 3D plot for the values of dlogL for each of the 2 gradients to understand how they are affected by the correlation and also by the bimodality
  - `[TO DO !]` Redo a TaylorF2 run with smooth PSDs?

### Ed asking during LVC
- What's my phase1 acceptance rate (same starting points) but with analytical trajectories replacing the numerical ones? That would prove that my phase2 fitting routines are working
  - should I do plots like the ones I have generated for the supervised learning ?
  - Why I am confident that my cubic fitting routines are working:
    - when phic or psi is kept fixed, the cubic fit is working
- plot the potential energy (or -logL) at point of injection just varying psi
- `[TO DO !]` Implement 'true' computation of the FIM when one or more parameters are kepts constant
  - why then don't we consider that in the 9 parameter case there are 8 other parameters being kept constant ?
- `[DONE]` Run 10000 traj in phase3, with hybrid as well, updating the oluts
- Run phase1 but with IMRPhenomD, spins set to 0, on BBH and BNS
- `[DONE]` compute covariance matrix of the scattered points from phase1 and compare to the scales we use

## TO DO RECAP
- `[DONE]` marginalize over `phic` and start adding the spins !
  - spot everywhere in the code where there is smth like: `size=9` or `n_param = 9`
  - remove dependencies to where in the code there is: `q_pos[9], q_pos[10] etc...` like in `BoundaryCheck_on_Position()`
  - get rid of the `search_parameter_indices` and use keys only instead
- `[DONE !]` Compare forward difference wrt central difference in the gradients computations:
  - understand why ra gives such different results
  - Firstly to know which offset I should use, I should compare on ~100 points (which ones?) which offset gives the best results
  - Then generate randomly ~10000 points in the 9 dimensional parameter space. For each of these compute the 9 gradients both with forward and central-difference and compute the relative error and absolute error between the two. Or should it be the absolute error divided by the scale of the gradient for that parameter ?
  - `self.likelihood.parameters = self.likelihood.parameters_copy.copy()` to correct
  - plot distributions of errors forward vs central
  - look at offset 1e-5 => 1e-8
- `[DONE !]` implement forward diff where it is sufficient
- `[DONE]` do a proper timing of: waveform_pol generation, exp_two_pi_f_dt, <s|h>, logL, dlogL => make technical note
- `[DONE !]` remove everywhere: `if likelihood_gradient is None` !
- `[DONE !]` turn on cos(theta) + 2
  - Acceptance rate drops down from 99% to 82%...! I had already noticed that `cos(theta) + 2` messes up my trajectories and it happened again here
  - The reason it did not happen for Yann is due to the formula he used for TF2 which does not include the phase-shift: `exp(-i * (Fx * cos(inc)) / (F+ * (0.5*(1 + cos(inc)**2)))))`. Hence for him cos(inc) being +1 or -1 was exactly the same but not in my case.
- `[TO DO !]` run short chain in phase3: ~5000 traj, and compute the R^2 over points of the analytical accepted trajectories
- `[DONE !]` finish using the likelihood object like in roq case but when not roq case
- `[DONE]` Finish cleaning up code !
  - Simplify param_names / keys.
  - rename config.py => CONSTANTS.py. Move param names / keys to config_file? Should still keep the official gw parameters keys in CONSTANTS though
  - Remove unecessary import config as conf => use autoflake ?
  - what to do with start_time ?
  - how to organise the sampler_inputs in config_file ?
- `[DONE]` Define priors in a prior file like in bilby_pipe
  - Create a prior.py file which would have one `class Uniform(Uniform)` inheriting from bilby's Uniform prior class.
  - would the PriorDict.from_file() method work on this and if yes how ?
  - add a method `def function` which would define which function to apply to it: np.cos of np.log or identity and it's corresponding inverse function
- `[TO DO !]` Run on Binary1.in and check if I am getting a bimodality in phic-psi. Am I getting the same distributions as Yann's?
  - Do it with IMRPhenomD
  - make regression plots
  - run phase3 for 10000 traj
- `[DONE]` try neural networks with Keiras. What about gaussian processes ?
- `[DONE]` run phase1 with IMRPhenomD. IMRPhenomP has apparently a hack on to define phic because of the way they handle precession.
- `[DONE]` prepare the steps to install gwhmc and bilby on Ed's laptop
- `[DONE]` In BNS_HMC_9D.py, split the initialization into callable chunks
- `[DONE]` create new bilby branch from the new master and add my personal updates
- `[forget]` Redo a TaylorF2 run with smooth PSDs?
- `[forget]` Setting priors using bilby's functions, removing the boundary stuff, all priors set from the config.ini file
- `[forget]` Why are `fit_coeff_good.dat` and `fit_coeff_global.dat` different? Do I get a different result each time I recompute them because of numerical instabilities...?
- `[DONE]` Implement 'true' computation of the FIM when one or more parameters are kepts constant
- `[TO DO !]` In hybrid trajectories: instead of recording the numerical gradients computed and then update the oluts, why not at each step of the trajectory updating the oluts with the new q_pos_fit and dlogL using `np.searchsorted()` ? I should time both to check what is the fastest. If so I should do the same for numerical trajectories in phase3 => create a function `NumericalGradientLeapFrog_Phase3()`
- `[DONE]` Make a run just in the phic-psi plane: plot in 3D the logL: with the scattered points but also on a grid? make a 3D plot for the values of dlogL for each of the 2 gradients to understand how they are affected by the correlation and also by the bimodality
- `[TO DO !]` Where do I lose time when computing `%timeit likelihood_gradient.calculate_log_likelihood_gradient() = 27ms` when `%timeit likelihood_gradient.likelihood.log_likelihood_ratio() = 1.56ms` and is called only 12 times ?
- `[TO DO !]` introduce sampler.chain and where chain would have chain.current_point...

- Gaussian mixture
- autodiff ou autograd, google tangeant sur github
- plotter vraiment à quoi ressemble dlogL/dpsi
- grid search cv


- `[DONE]` implement switch in cosi +1 => -1 together with phi_c + 2*arctan(Fx/F+)
  - it's a trick which should work in a single detector analysis, but with multiple detectors, the "effective phi_c"= phi_c + arctan(Fx/F+) is different for each of them since they have different antenna patterns, hence we can't do this.
  - is this also the reason why this cosi += 2 trick does not work even when marginalizing over phase ? I think it makes sense since if it would have worked, it means that we would not only marginalize over phase but also over ra, dec and psi which enter the antenna patterns.
- `[DONE]` sdoes phase-marginalization works with my `compute_h_dh()` function?
- compute autocorrelation, time-lag: look how Yann did it, how bilby does it
- run HMC with 170817 for 1e6 traj on CIT. Get results back. Need to marginalize over phic
- Then we will do post processing to determine the number N where tau (nb of independant sample length) is constant.

- `[DONE]` finish removing dependencies wrt search_param_indices
- `[DONE]` how do I deal with less than 9 parameters now? I should update likelihood.parameters with only those params which are part of the search_param_space right?
- `[DONE]`it looks like my code is a bit slower than before now: `1min20sec` for IMRPhenomD traj: I am back to `dlogL: 435 ms`! => I was regenerating the waveforms pol for each ifo
- `[DONE]` in likelihood_gradient: use .search_traj_functions
- `[DONE]` compare results with branch `dev`
- `[DONE]` Verify/finish that phase3 is working with new simplifications
  - AnaAlongNumGradientLeapfrog_ThreeDetectors()
  - modify `calculate_dlogL_logL_diff()` s.t. ROQ can work
  - remove dependencies to indices in `fileOutChain.write`, also in `injection_fparam`
- `[DONE]` My hybrid trajectories don't seem to be working
- `[DONE]` Switch on phase3 completely with 'Conducting new QR fit' and Acc <=0.65.
- `[DONE]` Compute the R**2 on the marginalized run
- `[DONE]` Implement the autocorrelation function in order to compute the lag and estimate the number of SIS points.
- `[DONE]` Redo the 1e5 traj run but setting new QR fit every 10000 new points instead of 50000. Then compare:
  - acceptance rate
  - duration of phase3
  - number of hybrid and numerical traj (best would be to compare the acc of anal traj only...)
- `[DONE]` Compare my autocorrelation functions with the emcee ones
- emcee page about autocorrelations: https://emcee.readthedocs.io/en/stable/tutorials/autocorr/#autocorr
- plot_autocorrelations_of_params using emcee
- `[DONE]` Implement possibility to run phase3 in several chunks as well if I don't want to wait 0.24 * 1e6 / 3600 = 67 hours !
  - create a function sampler.save_state() which save the q_pos, dlogL, acc, q_pos_global and local_fit
- `[TO DO !]` Make run over GW150914 like injection with IMRPhenomD
- `[TO DO !]` ROQ investigation about what Sylvain said:
  - psd avec fmax a l'inf
  - comparer avec non-roq pas extrapolée
  - regarder ini files LAL
- `[TO DO !]` Why is dlogL timed at ~330ms when it should take: 7*13ms + 15*4ms + 27*0.5ms = 164ms ?
- `[DONE]` compute autocorr on run without phase_marginalization to check if psi has the same autocorrelation. How do the walkers look like?
- `[TO DO !]` Why is autocorrelation on psi so high? How to get the next SIS with such a high lag ?
  - the psi scale: ~0.07 seems really small when looking at the range covered by the chain => hence the stochastic behaviour and high correlation?
  - I should implement the "correct" computation of the FIM when marginalizing over phase, maybe that would change the scale on psi => ask Ed which paper describe it
    - Is it normal that my projected FIM is almost uniform, ~1e12 everywhere ??
    - what does it tell that when not marginalizing over phase dlogL/dpsi = -53 and then when marginalizing dlogL/dpsi = -0.03 whereas for all the other parameters, their gradient change a bit but keep the same order of magnitude at least?
      - Maybe the problem here is that I am at the peak of the logL for psi when marginalizing since I am at the point of injection? So its gradient is very close to zero. But then why isn't the scale bigger?
  - actually what I am doing here is wrong since I should at least compute the FIM over the 9 parameters invert it and get the scales
  - artificially increase scale on psi to see the effect on the walkers and autocorrelations
- `[TO DO !]` plot slices of dlogL over the whole range of each parameter, keeping all the other constant at injection value.
- `[TO DO !]` try out the transformation I thought about if going from cosinc +1 to -1: ra += pi, dec += pi/2, psi += ?? => read more in Yann's thesis about it
- `[TO DO !]` adapt the scales of my parameters in phase1:
  - Every 300 num traj: compute integrated times for each param
  - For each of those having L > 150: double its scale if Acc > 65%
- `[DONE]` Look at dag_file_creation project to be able to run gwhmc on CIT using condor !
- `[DONE]` On CIT using condor:
  - the bash script could directly have the options wanted in it:
    `python BNS_HMC_9D.py --config_file=...`
  - so I could have a bash script containing like:
  ```
  [script to create the outdir] ?
  echo python BNS_HMC_9D.py --config_file=... > runme.sh
  chmod +x runme.sh
  cd outdir?
  condor_submit ../main.sub
  ```
- `[TO DO !]` Using condor on CIT:
  - reference the right ROQ basis path
  - understand why the job gets an error
    - why donnot I get the entire error in the .err file?
    - why aren't all the print statements printed into the .err or .out file ?
      - maybe the job has to end in order for them to appear in the .out file
  - why isn't my logger printing into /logger.log ? Because atom was hiding it !!

- `[TO DO !]` run automatically the `tail -f` command and keep only the fileHandler in the logger
- `[DONE]` `sampler.get_scales_from_samples()`: set them from the 68% range or from the covariance matrix ?
- `[DONE]` rerun phase1 but using scales of 0.5, pi/2 and 0.5 for cosiota, psi and lnD. Rerun phase1 keeping the scales just from what the FIM gives except for psi maybe. Then compare the corner plots and acceptance rate at the end of phase1 between both run.
- `[DONE]` plot logL vs iota-psi in 3D: we should find two modes indicating a pattern telling us how to switch from one branch to the other in cosiota.
- `[DONE]` plot walkers of length 1000 to check there aren't any plateaus
- `[DONE]` plot the integrated autocorr time: L as a function of tau_max to see it's convergence hence justifying we can stop the integration after tau_max ~ tau_zac. Make a technical note of this.
- `[DONE]` Looking at the corner plots of the run, I don't understand why the cubic fit IS working... Psi is completely bimodal, so if the culprit in the phic case was bimodality, then why is it working here?
  - bimodality in psi doesn't really matter because psi has very little effect in the change of logL, hence going from one mode to the other does change masses that much (which appear in the phase)
- `[DONE]` add as input of main.py the number of sis_samples wished
- `[DONE]` make video of exploration during phase1 by producing many plots !
- `[TO DO !]` Make runs with fRef within the freq band even though it shouldn't really matter because it's only used to initialize the spins and precession etc.
- `[DONE]` Why do I get a discrepancy between the logL at injection when computed during the .inject_signal() method vs likelihood.log_likelihood_ratio()? => because not phase_marginalization in the first case.
- `[TO DO !]` Introducing spins:
  - just chi1 and chi2
  - a1, a2, chieff and chip should be at the same time?
  - look at what the runs for GW170817 used and do the same
  - why difference between ROQ and non ROQ?
  - need to be able to plot rejected trajectories...!
  - try reducing the scales of all the spin parameters to see if accepted
  - understand why sometimes dlogL/dlnmu is bumping => rejected traj
  - implement a way to still retain points in rejected traj of phase1 for the fit with smart condition on Hamiltonian
- `[DONE]` IMRPhenomD
  - understand why scales of lnMc, lnmu and lntc get x10 which screws everything up...
- `[TO DO !]` Implement boundaries from ROQ as constraints
- `[TO DO !]` run over real GW170817 data
  - do the same structure as in `network.get_interferometer_with_open_data()`, but...
    - load the strain from the hdf5 file or not?
    - load the psd from the catalog file
    - understand why logL = -480, why bilby does certain things and not other as no highpass filter etc
      - follow all the steps on the losc tutorial which lead to the plots but apply that to GW170817real data and make the plot
      - do this on GW150914 to check that my script gives the same plot as the tutorial
        - yes !
        - why do they bandpass between 43 and 300 Hz? The f_CUT I compute is = 613 Hz and f_ISCO = 66 Hz: Try both upper limits to see
        - compute the snr with bilby to check I get correct answer and then do the same on 170817
         - The optimal snr computed seems correct, as expected the problem comes from the inner product between the data and the template: `d_inner_h`. When dealing with the injected signal, `d_inner_h` is very close to `h_inner_h`; but with the real data I get:
         ```
         d_inner_h_H1 = 24.115640121409307-1.205623587326565j, mf = 1.1442315243909515-0.05720405961538892j
         d_inner_h_L1 = -148.06215812591506+72.89780045847147j, mf = -5.180201818823724+2.5504512652185074j
         d_inner_h_V1 = 0.8803577226942961-3.865370691605366j, mf = 0.2675720711365441-1.174823841492547j
         ```
      - compare with what bilby renders in the time domain
      - The data from GWOSC should be bandpassed before the analysis but I don't think it should be whitened since the whitening of data and template is inherently part of the matched-filter analysis when we do the noise weighed inner product, no?
        - should not even be bandpassed: quote from the guide: " Note that such narrow bandpassing is only used for visualization purposes and is not employed in the LVC analyses."
      - Why don't I get my IMRPhenomD fourier domain template, whitened+bp, to display well in the time-domain wrt to time-domain strain as the gwosc tutorial does?
      - SNR mismatch: compute the opt_snr and mf_snr using template_fft from the file and not from lalsim
    - when should the windowing take place? before or after bandpassing? => before, bilby does it automatically when setting the frequency domain strain from the time domain one
    - now that the template matches the data in the time domain (!!!), understand properly why...!
      - I am using irff(), not ifft(): no `2*` line 270
      - phaseshift = 0 for me
      - I don't divide by d_eff: because my template is not scaled at 1Mpc I think
      - understand `peaksample`: I think that if I don't use it, offset could be left to indmax simply
    - Templates generated with lalsim + bilby and shifted in time by `time_shift` look how we expect, ie peak at `geocent_time + time_shift` in the time_domain, and hence comparing H1 and L1 they are naturally separated by the relative timeshift difference. For GW150914: `time_shift_L1_template = 0.004` and `time_shift_H1_template = 0.011`, consistently with the 7ms offset that was reported by the discovery. However when looking at the ifo data, this 7ms difference is visible BUT the data peaks BEFORE geocentime, as if `time_shift_L1_data = -0.018` and `time_shift_H1_data = -0.011`. I am not sure whether `time_shift_H1_data = -0.011 = -time_shift_H1_template` is a coincidence or the bottom line of the problem... Doing the same exercise with GW170817 if a bit more difficult since it's not visible by eye in the data.
    - What I also have to take into account is the `phase` at peak which should be added to the template s.t. it matches the data. And then add it to the input orbital phase given to the template generation.
    - Since this shift is constant whatever the value of geocent_time, it makes sense that when injecting a signal from a fourier-domain template, then the others match it well during the sampling.
- `[TO DO !]` To determine which gradient should be run on local fit, compute the R^2 at the end of phase1 and if it is < 0.9 they decide to run a local fit with OLUTs.
  - How do decide on the good threshold to consider for the coefficient of determination R**2?
  - => Take the 9 dimensional case and make a benchmark of multiple phase1 cases recording each time: nb of traj in phase1 used, R**2 for each parameter (using the cubic fit) at the end of phase1, acceptance rate in phase3 after 10000 trajectories, nb of hybrid and numerical traj in phase3


### Analysis to carry out
- run on real real data, not injected, real PSD
- introduce new parameters: chi_1, chi_2, lambda_1, lambda_2, a_1, a_2, chi_eff, chi_p...
- run on BBH
- run on single and double detections
- run on low snr detections
- run trajectories on IMRPhenomPv2 or D but compute MH ratio with SEOBNRv4








- do we need more than 1500 traj in phase1 adding chi1 and chi2
  - make run of 2500 num traj in phase1, saving state of sampler at 1500 and 2500
  - compare nb of hybrids and numerical traj after 5000 traj in phase3
  - compare also with a 1500 non spinning run
- phase1: introduce MH ratio constantly along the trajectory, storing points up until it would be rejected
- parallelize phase1
- finish implementing the roq boundaries into param boundaries


- make run with chi_1 and chi_2 boundaries being [-0.05, 0.05] instead of [0, 0.05]: do it with only 1000 numerical traj first to check how well it reacts
- forward or central for chi1 and chi2:
  - make the plot for offset 1e-5 => 1e-8
  - use log scale of x for relative errors
  - save data so that I don't have to recompute everything all the time
- make goodness of fit plots for 1500, 2000 and 2500 num traj
- write up the detailed plan of the thesis and of a publication
- make run with 1500 num, starting from the one at 1000.
    - make goodness of fit plot at the end of phase1 and compare with that of 1000
    - run 9000 trajectories in phase3 and compare Acc, atr, htr, ntr, runtimes etc
- from a run with 2500 num traj, make a benchmark of regression plots:
  - using from 100 000 to 250 000 fit points
  - always fitting the same test set = the last 237 000 points
  - compare the R**2 and its evolution
- regression plot with OLUTs


- running on real data: continue to understand why templates are shifted after geocent when data is before ?
  - solved now normally, I had to take the phase into account and also the multiple snr peaks for 170817
  - running on GW150914: understand why my scales and FIM are skyrocketting
    scale[ln(tc)] = 267.142242 when it is = 0.000004 for 170817
    ```
    GW170817
                 cos_theta_jn        psi     ln(D)    ln(Mc)    ln(mu)  sin(dec)        ra    ln(tc)     chi_1     chi_2
    From FIM          4.64261  39.606319  4.635155  0.000063  0.002778  0.010832  0.008152  0.000004  3.367521  3.723705
    Constrained       1.00000   1.570796  0.500000  0.000063  0.002778  0.010832  0.008152  0.000004  0.050000  0.050000

    GW150914
                 cos_theta_jn          psi        ln(D)    ln(Mc)    ln(mu)    sin(dec)           ra      ln(tc)      chi_1      chi_2
    From FIM     10991.586365  9508.012309  9017.451834  0.022368  0.044613  528.573434  6062.752419  267.142242  12.734167  14.773086
    Constrained      1.000000     1.570796     0.500000  0.022368  0.044613    1.000000     3.141593  267.142242   0.050000   0.050000
    ```
    - idea to tune the scales automatically: start with conservative scales, then for the first per say 100 numerical traj, record the total kinetic energy of each traj together with the contribution of each parameter, ie each `0.5*p[param]**2` of each traj. Each param contributing on average more than 1/nb_param-th to the total kinetic should see it's scale lowered
    ```py
    pmom_traj_np = np.asarray(pmom_traj)
    kinetic_contributions = 0.5 * pmom_traj_np ** 2
    kinetic_contributions.mean(axis=0) # array([ 5.80650871,  7.83704159,  3.06795167,  0.51875817, 1.52036153, 11.13973953, 10.43751894,  0.57015489, 0.57035825,  0.03857172])
    kinetic_contributions.sum(axis=1).mean() # 41.50696501204309
    # And note that kinetic_contributions.sum(axis=1).mean() = kinetic_contributions.mean(axis=0).sum()
    ```
  - run phase1 of GW170817 with scales from phase3 to see the drop in Acc and plot some traj!
- read paper on marginalization Alex Boucaud


## Christmas vacations advances
- real data runs
  - issue with gps time
  - it looks like after a handfull of numerical traj, the chain settles down
  - for GW150914: issues with the scales derived from FIM
    - except for masses parameter, the scales were complete nonsense being ~1000 (~10 for chi1 and chi2)
    - Constraining them to half of their range it would still not work because scale_geocent_time was not constrained and its scale was ~200...
    - Also I had to increase the offset when deriving wrt geocent_time from 1e-7 to 1e-6.
      - Why not using `geocent_time` directly instead of `ln(duration)`? I am still getting scales making no sense but it looks like increasing offset_geocent_time makes it better...
      - I tried projecting geocent_time out of the FIM but that did not improve the scales
    - tweeking the scales manually I can make phase1 work though
  - run on GW170817real:
    - trouble getting a sensible snr. After changing a bit the starting_point parameters, I start on a point where: mf_snr_H1 = 3.7, mf_snr_L1 = 6.7, mr_snr_V1 = 1.3, opt_snr = 35; versus "18.8, 26.4, and 2.0" reported in the observation paper...
    - Phase1 running well even though scales are a bit smallish, leading to a high Acc = 99.5 % and hence correlated chains, but resetting the scales in phase3 from the covariance matrix should solve this.
    - Phase3 not working after 1500 num traj, will see after 2000.
    - Phase1 corner plots look a bit different from what I get when injecting data: data is generally more scattered and for theta_jn its marginalized posterior distribution is much more peaky


- Ask Alex Boucaud about parallelization of phase1 on a GPU.
- read about parallelization
- Set up latex on overleaf. Gitlab project as well?
- start writing up !
- Try ROQ runs on IMRPhenomD ?
- iCloud archive issue



- make new GW170817real runs
  - push it to 1000 and 1500 then
  - measure phase1 acceptance rate when using scales from end of phase1
- remake the snr plots
- mail to Ed:
  Hi Ed,

  So in order to reproduce the correct SNRs in each interferometer, I had to set the values of the parameters from a digest page of the same approximant, using the ‘maxL’ column of the summary statistics table. For instance:
  - MRPhenomPv2_NRTidal:
  https://ldas-jobs.ligo.caltech.edu/~soichiro.morisaki/O2/G298048/C02_2/lalinferencemcmc/IMRPhenomPv2_NRTidal/1187008882.45-0/V1H1L1/posplots.html
  - IMRPhenomD_NRTidal:
  https://ldas-jobs.ligo.caltech.edu/~katerina.chatziioannou/O2/PE/G298048/ProductionRuns/NRtides_PhenomD_NSBH/lalinferencemcmc/IMRPhenomD_NRTidal/1187008882.45-0/V1H1L1

  Then starting the HMC run from this ‘maxL’ point were the SNRs are correct, the chain goes to points of similar SNR values which is expected.
  What I don’t understand and would have expected is that even if starting quite off the likelihood peak the chain would quickly be driven close to it and then the snrs would be fine. Isn’t that what is done in practice? Since the starting point is the triggered template. So I computed the SNR after 1500 numerical trajectories (of a run which started with SNR ~8) but it was still equal to ~8.


  Side note: using IMRPhenomPv2_NRTidal, I quickly tried phase1 with it, hence using 16 parameters, and, even though one trajectories takes ~6min to complete, phase1 is working !

- results with 1000 sis, run stats
- Side note: using IMRPhenomPv2_NRTidal, I quickly tried phase1 with it, hence using 16 parameters, and, even though one trajectories takes ~6min to complete, phase1 is working !
- I can compare directly with the LALInference run of the time
- I should still install lalinference on my computer
- Parallelization of phase1...
- determinant of FIM
- Why cubic fit not working on costheta, lnD?: because these two are much correlated with psi probably


- run 100 traj in phase3 with constant number of leapfrogs for phase3:
  - 20, 50, 100 and 150
  - super-impose the walkers plots of phase3 only of all these runs
  - plot two horizontal lines being the maximum and minimum of the 80000 samples run
- run phase1 with scales from covariance and check the acceptance rate
- run phase3 with cubic fit for theta and distance as well and check the acceptance rate
- I should still install lalinference on my computer: how to run lalinference_pipe ?


- Why not using a kinetic energy with the mass matrix M equal to the inverse of the covariance matrix as proposed by Radford page 22 ?
  - How to rewrite the multiple stepsize leap frog then ?
  - It would seem that instead of multiplying the momentum by its scale when doing the full step in position we should just do a matrix multiplication with the covariance matrix instead, but according the Radford p26, can we draw p_mom_0 from a normal distribution with mean zero and variance one ?
- Why not setting epsilon with respect to the smallest scale?
- In phase3, to avoid being stuck with hybrid trajectories at ~65% acc, run numerical traj if 2 hybrid are rejected in a row.


- Tuning epsilon:
  - From Neal Radford page 30: "For HMC, the cost to obtain an independent point will be proportional to 1/(Acc * epsilon)". From this he derives an optimal Acc=0.65%. However for us the situation is a bit different since we get a lower acceptance rate due to inaccuracies of our fit, so the optimal 65% is not valid for us. However the cost is still valid.
  - Run phase3.0 with different values of epsilon around 5e-3 for ~300 purely analytical trajectories
  - With the acceptance rate at the end, compute each cost relative to each epsilon
  - Introduce an ad-hoc cost being C_adhoc = 1/(Acc**2 * epsilon) to favor epsilon leading to high acceptances. Indeed in our scheme where we run hybrid and numerical traj, the cost we should consider is C = duration / (acc * eps), and since low acceptance rates lead to more hybrid and numerical traj, hence longer duration of the run this should be taken into account. We could run ~300 traj keeping our scheme and measuring the duration at the end, but if ever an epsilon leads to a really low acc, then those 300 traj would take too much time. Hence the idea of weighting the acceptance by squaring it.
- Tuning the scale of the parameter giving the highest autocorrelation:
  - Since I have seen that my computation of autocorrelation length converges after ~1e4 samples, I could wait until then, compute the L for each parameter and increase the scale of the one having the highest L.
  - Do I still set the scales with the new covariance matrix also?
  - How many times do I do this tuning? Because doing this kills the homogeneous property of my chain...


- make regression plots of the OLUTs fit
- check whether the HMC yields an autoregressive time series, in which case I could apply the simpler autocorrelation length's formula given page 71 of the "Markov Chain and Monte Carlo methods" pdf
- increase the number of points before new QR fit: 500 => ?
- shouldn't I set the stuck_count depending on the expected acceptance rate of my run with the epsilon chosen?
  - if acc_expected = 0.65 => proba(3 rejected in a row) = 0.35^3 = 0.043, meaning there is 4.3% chance of getting one hybrid traj, hence there should be about 4.3% hybrid traj overall.
    - The run with epsilon = 1e-2 has acc = 64% and n_hyb / n_ana = 2009 / 46706 = 0.043 !
    - The run with epsilon = 5e-3 has acc = 72% and n_hyb / n_ana = 1942 / 76804 = 0.025, which is = (1 - 0.72)^3 = 0.022
  - So if acc_expected = 0.50 => proba(3 rejected in a row) = 0.50^3 = 0.125 and I will get 12.5% of hybrid traj meaning one hybrid traj every other 8 traj which considerably slows down phase3 compared to 1 hybrid every other 20.
  - So why not setting the stuck count threshold s.t. we keep the number of hybrids to ~5%. This means the following equation:
    (1 - acc_expected)^stuck_thresh = 0.05 => stuck_thresh = round(ln(0.05) / ln(1 - acc_expected))
  - If I do this, my ah-hoc cost becomes useless since it was supposed to take the effect of more hybrid and num traj into account when choosing a lower acceptance rate. So I guess I could keep the "normal" cost = 1/(acc * epsilon)

- Skype with Ed:
  - Submitted the abstract
  - Kinetic energy with covariance matrix vs multiple stepsizes: what we are doing is using a diagonal mass matrix derived from the covariance matrix which is different from using the full covariance matrix no?
- Volunteer for writing tasks help and email John Veitch ?

- Try FIM determinant np.float64
- run pure analytical with epsilon 2e-2 for 100 000 or 250 000 traj
- make posterior comparisons tests: Kolmogorov-Smirnoff and/or KL test
- ask Carl-Johan Haster for lalinference on local computer
- read paper on parallel bilby:
  - https://arxiv.org/pdf/1909.11873.pdf
  - slides on bilby Gwtc1 comparision: https://dcc.ligo.org/DocDB/0166/G2000167/001/bilby_gwtc1_analysis.pdf
- write manuscript
- optimize analytical code ?
- uniformize computation of `cubic_jacobian()`
  - factor 10 speed-up for the cubic computation of 7 gradients !
  - factor 2-3 speed-up overall on phase3 because local oluts fit + hybrids and numerical
  - QR decomposition on 160 000 points using the new cubic_jacobian goes from
    - before: 117 sec = 57 sec + 60 sec = jacob_creating + QR_decomp
    - now   :  84 sec = 24 sec + 60 sec = jacob_creating + QR_decomp
  - integrer les Class: `CubicFitRegressor` etc
- run on GW150914
- Comparison-page-for-GW170817: https://git.ligo.org/bhooshan.gadre/pe-o3-s190425z/-/wikis/Comparison-page-for-GW170817
- LVC MEETING CGCA:
  - ESTA
  - Registration
  - DOM
- lalinference_mcmc tests:
  ```
  conda create -n lal
  conda activate lal
  conda install -c conda-forge openmpi
  conda install lalsuite or pip install lalsuite
  ```
- continue using `CubicFitRegressor`
- about autocorrelation function: in `/anaconda3/envs/hmc/lib/python3.7/site-packages/lalinference/bayespputils.py` > `def autocorrelation_length_estimate()`:
  "In words: the autocorrelation length is the shortest length so
  that the sum of the autocorrelation function is smaller than that
  length over a window of M times that length.

  The maximum window length is restricted to be len(series)/K as a
  safety precaution against relying on data near the extreme of the
  lags in the ACF, where there is a lot of noise.  Note that this
  implies that the series must be at least M*K*s samples long in
  order to get a reliable estimate of the ACL."

- create corner plots with m1-m2 and also their marginal distribution
- posterior over D_L is very different from that recovered by LALInf and Bilby...
  - carl's run: https://ldas-jobs.ligo.caltech.edu/~carl-johan.haster/O2/G298048/PE_paper/PEP_IMRPDNRT_noCal_128s_23Hz_spin0p05_mPSD/lalinferencemcmc/IMRPhenomD_NRTidalpseudoFourPN/1187008882.45-0/V1H1L1/posplots.html
  - bilby's: https://ldas-jobs.ligo.caltech.edu/~virginia.demilio/GWTC1-bilby/bilby_GWTC1_summary_pages_skymap_GW170817/html/GW170817_GW170817.html
  - In order to make proper comparisons, I should reweight LALInf or Bilby samples with the prior they use compared to mine. See Appendix A of https://www.overleaf.com/read/jjbpxvbkhtjh

- check that after thinning my hmc chains, the autocorrelation length is 1 or 0 !

- Comparing samplers:
  - https://git.ligo.org/lscsoft/bilby/-/wikis/minutes/200219
  - https://docs.google.com/presentation/d/1rlo2scXbrGiZWqKHqnqDbpTXjwikesX4YdHx4JGtB4s/edit#slide=id.g7e67a90c5e_2_89
  - https://docs.google.com/presentation/d/1pYctfmd5C0eEYzqLA6OB1xurlmND8FFEVtnSvBTArBY/edit#slide=id.g7e1924e024_0_50
  - https://git.ligo.org/isobel.romero-shaw/bilby_gwtc-1_analysis_and_verification

  Yes it does. Eventually it will be on CIT (and other clusters), but we don't have the hardware at the second. Sorry about that. We are using it on non-LIGO clusters (e.g. the AEI has a cluster, as does a few other places). What we are finding is that lots of universities have local clusters with a few hundred cores which are set up for high performance (rather than high throughput) clusters. It may be worth asking around if there is anything you can access ￼ but without that I'm afraid it is probably quicker for me to run it locally


- burn in part of lalinference
- lalinf verbose on how long it takes to produce sis
- ping vivien / carl about how lalinference computes sis
  - looks like I might need to run  `cbcBayesPostProc`

- Why using log of parameters for the HMC:
  - To reduce the scaling of the parameters and get a numerically stable FIM
  - D_L ~ 10^26, chirp_mass ~ 10^34kg ?; hence working in log of those parameters reduces their dynamic scales and produces a FIM numerically more stable. Indeed even each the samples in those params are always output in terms of Mpc and solar masses, in the end it is inputted in the waveforms as seconds and kg. The chain rules with log give: dlnL/dlnD = dlnL/dD * dD/dlnD = D * dlnL/dD
  - Since we don't compute the gradient w.r.t any prior it means we consider flat priors in each of those functions, ie our priors are flat in lnD, lnMc, lnmu etc...
  -

Monday 24/02:
- DNN and DL Workshop
  - replace cubic with 10-DNNs and look at phase3: acceptance? traj speed?
    - speed: 3 times slower
    ```
    %timeit cubic.predict(10) = 0.33ms
    %timeit oluts.predict(3)  = 1.0ms
    %timeit dnn.predict(10)   = 4.70ms
    meaning
    %timeit cubic_olut_traj = 1 phase3 trajectory = 0.20 sec
    %timeit dnn_olut_traj   = 1 phase3 trajectory = 0.60 sec
    ```
    - training is not going well: is the rescaling taking place properly ? => Nope, was using x_scaler on y_train data...
    - from Philip:
      - try decreasing the learning_rate of the optimizer
      - overfitting: try to simplify the network
  - implement 1-DNN: regression plots?
- LVK meeting
  - ping Viven/Carl or mattermost to know how estimation of lalinf_mcmc's SIS is done: look at `cbcBayesPostProc` first.
  - apples-to-apples comparisons with LALInf in terms of SIS
    -
  - Reweight posteriors with prior functions
    - See Appendix A of https://www.overleaf.com/read/jjbpxvbkhtjh

- Differences in data handling between lalinference and parallel bilby: https://git.ligo.org/serguei.ossokine/pb_li_comparison/-/wikis/data_conditioning


- create n_dim x n_dim plots dlogLs vs qpos, derived from the last line of the corner plots of qpos + dlogL_of_param
- make corner plots on dlogL only at end of phase1?
- try cubic with rescaling => put the rescale as a boolean param in CubicFitReg
- LALInferenceRun...
- for FitRegressor Classes:
  - create a common function: `def plot_regression()`
- implement for each FitRegressor Class a `def save(save_dir)` function and use it in `sampler.save_state()`, and then a `def load(load_dir)` and use it in `sampler.get_state()`
- in hybrid traj: use num grad only for params having R**2 < 0.9 in phase2-
- Reweight posteriors with prior functions : see Appendix A of https://www.overleaf.com/read/jjbpxvbkhtjh
- test DNNMG varying the training dataset size from phase1
- autotune DNNMG

From the workshop
  ```
  import inspect
  tfp.SimpleLeapfrogIntegrtor
  cholesky decompostion of the FIM for the scales
  ```
  - keras-tune hyper parameters
    - https://medium.com/swlh/hyper-parameter-tuning-for-keras-models-with-scikit-learn-library-dba47cf41551
    - https://machinelearningmastery.com/grid-search-hyperparameters-deep-learning-models-python-keras/
  - Use dropouts: https://machinelearningmastery.com/dropout-regularization-deep-learning-models-keras/
  - try reducing the complexity of my network

  - junpenglao@google.com
  - bjp@google.com



- try dropout on the input layer
- try adding `kernel_constraint=maxnorm(3)` in the hidden layers
- run phase3.0-tune-epsilon with DNN to see the output

- DNN trained on 220000 data is about as good as the OLUT in psi.
  - remember that `%time dnn.predict() = 0.6ms` and `%time olut.predict() = 0.9ms` meaning that a full prediction of dlogL with DNN and 1 olut takes `1.5ms`.
  - So if only DNN is used, we decrease that number to `0.6ms`
  - For an analytical traj of length 100, it means that `~60ms` should be used for the computation of the gradients vs `150ms` before.
  - The next time consuming part of the traj is computing the logL at the end of the traj, it takes `~26ms`
  - meaning with should arrive to an average for one analytical traj of:
    - `176ms` for DNN + 1 OLUT
    - `86ms` with DNN only
  - measuring it directly gave:
    - `200ms` for DNN + 1 OLUT
    - `90ms` with DNN only

- Is it legid to reweight correlated samples? Basically take a faction, ie 50%, of the 100 000 correlated samples which have L=20, and then thin the 50 000 reweighted samples with L=10?
- change `geocent_time` key to duration when it's duration used
- method to convert sample array with duration to geocent_time
- method to convert sample array with `chirp_mass, reduced_mass` to `mass_1, mass_2`
- What I want:
  - a corner plot in chirp-mass, reduced-mass of the samples with hmc_priors
  - a corner plot in mass_1, mass_2, of the samples with hmc_priors
  - a corner plot in chirp-mass, reduced-mass of the samples with LALInf priors
  - a corner plot in mass_1, mass_2, of the samples with LALInf priors
- overwrite the `result.plot_corner()` method by being able to choose the parameters on which to plot?

- geocent_time to duration
- run quick phase1 to check everything is ok
- run phase3 until 15000 sis are gathered, plot reweighet samples and save them in a file
- run KS tests between those samples and LALInf ones
- make the prior file set the names and boundaries of the search_parameters, basically replacing the config_params file
- sort out code that processes lalinf posterior file and creates the combined corner plots
- start presentation

- update sampler.plot_all_corners() s.t. the reweighted_sis comes from the new function sampler.get_result_reweighted_samples_sis()
- What to do with my warnings:
```
16:27 gwhmc INFO    : mass_1 minimum value was not consistent between old and new prior. Setting it to old value = 1.0
16:27 gwhmc INFO    : mass_1 maximum value was not consistent between old and new prior. Setting it to old value = 2.6
16:27 gwhmc INFO    : mass_2 minimum value was not consistent between old and new prior. Setting it to old value = 1.0
16:27 gwhmc INFO    : mass_2 maximum value was not consistent between old and new prior. Setting it to old value = 2.6
```
- clean new files created:
  - autocorrelation.py: keep old function as save, remove useless from sampler.py
  - prior.py
  - result.py
- check that plots in phase1/2/3 are working ok


- mass_ratio: hard cutoff at q=1
- m1-m2: always check that m1>m2
- reduced_mass: no boundaries

- compare with TidalP-7 which is doing IMRPhenomD-NRTides: are the samples available?
  - https://www.atlas.aei.uni-hannover.de/~mark.hannam/LVC/O2/PE/G298048/G298048_C00_NRtides_PhenomD/Faster/30_2048_lowspin/RUNDIR/lalinferencemcmc/IMRPhenomD_NRTidal/1187008882.45-0/V1H1L1/posplots.html
  - `/home/mark.hannam/O2/PE/GW170817/IMRPhenomD_NRTidal_Faster/30_2048_lowspin/lalinferencemcmc/IMRPhenomD_NRTidal/engine/lalinferencemcmc-0-V1H1L1-1187008882.45-0.hdf5`
- check how lalinference is doing the reweighting
  ```
  ￼<17:38:17> "Ed Porter": Carl sent the following
  ￼<17:38:27> "Ed Porter": this takes the individual hdf5 outputs from the different temperature chains and puts them all in one file https://git.ligo.org/lscsoft/lalsuite/-/blob/master/lalinference/python/cbcBayesCombinePTMCMCh5s.py
  ￼<17:38:36> "Ed Porter": this takes those files from multiple runs, and produces a set of posterior samples https://git.ligo.org/lscsoft/lalsuite/-/blob/master/lalinference/python/cbcBayesMCMC2pos.py
  ￼<17:38:53> "Ed Porter": old postproc script https://git.ligo.org/lscsoft/lalsuite/-/blob/master/lalinference/python/cbcBayesPostProc.py
  ￼<17:39:02> "Ed Porter": new postproc script https://git.ligo.org/lscsoft/pesummary
  ```
- introduce tidal parameters !!

- try new reweighting procedure
- introduce tidal parameters: how is it for phase1/2/3?
- derive w.r.t. real physical priors?

- What is the impact of this bit of code in bilby?
```py
if wf_func == lalsim_SimInspiralFD:
    dt = 1. / delta_frequency + (hplus.epoch.gpsSeconds + hplus.epoch.gpsNanoSeconds * 1e-9)
    h_plus *= np.exp(
        -1j * 2 * np.pi * dt * frequency_array)
    h_cross *= np.exp(
        -1j * 2 * np.pi * dt * frequency_array)
```

- Prior gradients:
  - run phase1
  - set geocent_duration as the search_parameters
  - set search_parameter_keys from Priors.name?
  - set boundaries in params and traj_params from their priors

  - clean up
  - offset setup
  - generate trajectory_parameters_keys automatically from function name
  - recode plot_all_corners

  - plot traj_prior and log_traj_prior_gradient over mc and mu, or lnmc and lnmu ?
  - set prior input file as input in config
  - write a tech note on prior gradients, the jacobian
  - plot trajectories which seem to be diverging

  - test further traj_func for chi:
    - one_over_chi
    - +/-np.log(abs(chi)): why is it trying to bounce when scale = 0.3 ?
    - +/-chi * (np.log(abs(chi)) - chimax)
    - 1/x**(1/3)
  - truncation:
    - use some sort a sinusoid when |chi| < chi_low to connect smoothly the two parts of the gradient
    - what about setting the gradient to 0 directly for some |chi| < chi_low where chi_low would correspond to the step of variation of chi in a trajectory
  - mass param jacobian diverging
    - try capping it to 1e3 and compare wrt 1e4 and 1e5
    - try in sampler.remap_q_pos_traj_in_boundaries() to modify the condition `eta_pos_new > 0.25` to `eta_pos_new > 0.2499`

  - make new run with bounce on `eta_pos_new > 0.2499` only; and bounce on `chi = 0.049` ?
  - check that my former corner plots were railing or not on mu on the right hand side
  - try making corner plots in pdf
  - plots phase1 data points as before
  - what's that tail in psi ?
  - what about running phase1 with uniform prior for all the traj-parameters, then make the fit and then run phase3 with correct priors ?
  - create n_dim x n_dim plots dlogLs vs qpos, derived from the last line of the corner plots of qpos + dlogL_of_param
  - make corner plots on dlogL only at end of phase1?
  - remove outliers !
  - what if a bigger scale on psi in phase1 would improve the exploration in psi and hence the fit? Ie, phase1 would require less trajectory because exploration on psi would have been faster?

  - implement tidal parameters: look at the source properties paper, dcc to find data samples
  - regression plots super-imposing DNN with OLUTs
  - what way on regression plots to show if outliers are the same on each plots

  - Phase1 timing:
    - why does it look like going from central to forward differencing in `chi` and `lambda` doesn't change the timing?
    - Why, even with central on both `chi` and `lambda`, does one dlogL times up to 579ms which is `~ 0.030ms * 19`, and `logL ~ 30ms`, meaning that it takes the same amount of time it would if I was to just compute the full 19 logL? Ie, it looks like my optimization of not recomputing the source frame waveform doesn't save time
    - can't iota and distance avoid recomputing the source frame polarization ?

  - create bilby issues:
    - 23Hz problem: `sampling_frequency=4096.000000000001 and  duration=116.99999999999999 multiply to 479232.00000000006`
    - `create_frequency_series()`
  - forward vs central for chi and lambda
    - run central but recording forward at the same time and compare
      - make a script which plots comparisons of central vs forward
    - run forward only to be sure... for 1300 phase1 traj.
      - is the acc worse than 92.5%?
      - what's the acceptance rate in phase3, worse than 52%?
    - run from 40Hz
      - I get an acceptance rate at the end of phase1 = 92.5% vs 94.5% when 30Hz. That's not due to the 10Hz missing (indirectly yes...) but because the scales derived in the 40Hz case were bigger in geocent_duration, chirp_mass and reduced_mass than that in the 30Hz case.

- SWITCH BACK TO CORRECT SEGMENT LENGTH:
```py
desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end+1]
```
to
```py
desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end]
```

- Central vs forward for spins and tides
  - run phase3 with the same scales and compare acceptance rates: is central still below forward?
    - after 11300: 52.8% for central vs 54.2% for forward
    - after 5000 sis: ??? for central vs 52.4% for forward
  - vary the validation set to see whether it has an effect on the regression plots
  - run forward on masses or tc to see the effect on acceptance
- Re-run IMRPhenomD_NRTidal from 40Hz, forward spins on spins and tides but SWITCHING BACK TO CORRECT SEGMENT LENGTH => compare with LALInf to check whether there is still a discrepancy in chirp_mass and tc


- write article:
  - plots of cubic + oluts and dnn
  - continue writing content in dnn etc
- read hajian paper
- compute the "global R^2" for dnn, oluts and cubic, just like the "val_loss" is the dnn training; to account for the fact the dnn is optimizing on the 12 gradients contrary to cubic and oluts.
- run central for 3000 traj...
- plot as 8x3, in .pdf or .eps, trim the data if necessary
- what about `logcosh` as loss function for the dnn
- reduce size of pdf with preview
- send mail to matteo and Ken for comité de these
- run 3000 trajectories with central for spins and forward for tides
- look at correlation matrix and coeff between spins and iota:
  - c_munu = C_munu/sqrt(C_mumu C_nunu)

- matplotlib file size
- full run with central/forward and 275000
- write paper

- regression plot: bigger R**2, thicker redlines, nogrid
- explain linear, ReLU, number of nodes per layers, nb of layers
- 80 epochs because the curve flattens
- add that adding another layer increases the prediction time by ??
- plot diverging gradients for spins
- create git for paper
- merge overleaf projects
- read papers on DNN:
  - https://arxiv.org/pdf/2004.11981.pdf
  - https://arxiv.org/pdf/2004.06226.pdf
- look O3b events:
  -  source 1) S200311bg - TGR polarisation test - SNR_H1 = 11.9, SNR_L1 = 11.0, SNR_V1 = 7.3
  - source 2) S191127p - high mass ratio : m1 = 80.5, m2 = 2.9 - requires DQ
  - source 3) - even higher mass ratio : m1 = 272, m2 = 3.2 - requires detchar
- modify launch_command_line.sh s.t. the duration is recorded when the run exits naturally
- Local LALInference run does not seem to produce the correct results since the chirp mass is arround 2.2 instead of 1.19
  - check the command_line.sh from Carl's run on CIT


- arxiv - search:
  - leila: https://arxiv.org/abs/2002.02460
  - eric: ads service
