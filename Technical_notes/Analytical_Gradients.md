# Analytical Gradients

## QR decomposition and cubic fit
### Context
- After some tests, it appeared that my QR decompostion was different from the one that Yann would obtain in his C code version.
- However QR decomposition is not unique and both would recover the decomposed matrix, ie the Jacobian.
- I would have expected however my fit coefficients to be the same as Yann's. This is surprisingly not the case and their values seem to be very sensitive to the precision of Q, R and dlogL (cf `notes_on_translation.md` for more details).
- Ultimately, my QR decomposition would prove right if the fit coefficients it outputs can approximate the gradients well enough compared to the numerical gradients.

### Action
- In order for my QR decomposition to be accurate enough, I needed to have enough fit points coming from phase 1. Hence I ran 1500 numerical trajectories of length l=200. With an acceptance rate of 89%, this lead to 268 000 recorded fit points used to do the QR decomposition.
- Then idea was to run at the end of phase 2, ie after the QR decomposition; a single numerical trajectory with 1000 leapfrog steps. At each step, parallel to the computation of the numerical gradient, I also computed the analytical gradient coming from the cubic fit.

### Results
- Numerical vs analytical gradients for a 1000 iterations trajectory ![gradient_num_vs_analyt_without_OLUTs](./gradient_num_vs_analyt_without_OLUTs.png)
- Analysis
  - As expected, we have 3 troublesome parameters: `cos(i)`, `psi` and `lnD`; they will be dealth with with OLUTs.
  - For the other parameters, my analytical gradients coming from the python QR decomposition does approximate the numerical gradients well enough.
  - One thing to note is that my gradients oscillate more rapidly than Yann's over the trajectory (p101 of his phd):
  ![numerical_vs_gradient_Yann](./numerical_vs_gradient_Yann.png)
    - First explanation: his used `epsilon=2.5e-3` when I used `epsilon=5e-3`. Below is the comparison of numerical gradients for 5 trajectories of 200 steps for 2 different values of epsilon.
    - ![epsilon_influence.png](epsilon_influence.png)
    - Secondly it seems as we will see later that the random number seed might have an influence on how rapidly the gradients oscillate, but this seems strange though
  - Another thing to note is that for the 3 troublesome parameters, Yann's cubic fit does not work properly but still gets the right shape when mine is completely flat in the 3 cases. 

## Ordered Look Up Tables (OLUTs)
To check that my implementation of ordered look up tables work, I used the same technique as before, adding in the 1000 leapfrog step numerical trajectory the computation of the 3 analytical gradients with OLUTs.

Here are the results with on the right column: the previous plot and on the left the plot with newly calculated gradients with OLUTs for `cosi`, `psi` and `lnD`. This has been done with the same phase 1 1500 trajectory, hence the same fit_coefficients for the cubic fit approximation. For the OLUTs, `n1=2000` and `n2=200` points have been used, following Yann's optimal results:
![gradient_num_vs_analyt_withorout_OLUTs.png](gradient_num_vs_analyt_withorout_OLUTs.png)

To get a better idea of how well my OLUTs work, I ran the OLUTs fit over different trajectories by changing the seed of my random number generator (but keeping the same fit_coeff of phase 1, hence the same OLUTs). Here are the plots for 3 different seeds (2, 3 and 4):
![gradient_num_vs_analytOLUTs.png](gradient_num_vs_analytOLUTs.png)
