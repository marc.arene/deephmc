# Comparing HMC's performances with LALInference / Bilby

## GW170817

### HMC settings
#### Used now
- flow = 30 Hz
- signal duration = 59 sec
- IMRPhenomD_NRTidal: 25.4ms
- 12 parameters

- IMRPhenomD_NRTidal: 57.9ms
- logL = 85ms
- 16 parameters

#### Could use
- flow = 23 Hz
- signal duration = 115 sec
- IMRPhenomD_NRTidal = ~116 ms?
- logL = ~160ms ?


### PBilby settings
- flow = 23 Hz
- signal duration = 115 sec
- IMRPhenomPv2_NRTidal

```py
ncores = 50*16 = 800
nlive = 2000
speedup = nlive * np.log(1 + ncores / nlive) = 673
runtime_ncores = 14 hours
runtime_onecore = runtime_ncores * speedup = 392 days
runtimebilby_onecore_30hz = runtimebilby_onecore * duration_hmc / duration_bilby = 201 days
```
