import numpy as np
import bilby.core.utils as bcu

_TOL = 14

def get_sampling_frequency_and_duration_from_frequency_array(frequency_array):
    """
    Calculate sampling frequency and duration from a frequency array

    Attributes:
    -------
    frequency_array: array_like
        Frequency array to get sampling_frequency/duration from: array_like

    Returns
    -------
    sampling_frequency, duration: float, float

    Raises
    -------
    ValueError: If the frequency_array is not evenly sampled.

    """

    tol = 1e-10
    if np.ptp(np.diff(frequency_array)) > tol:
        raise ValueError("Your frequency series was not evenly sampled")

    number_of_frequencies = len(frequency_array)
    delta_freq = frequency_array[-1] - frequency_array[-2]
    # delta_freq = frequency_array[1] - frequency_array[0]
    duration = np.round(1 / delta_freq, decimals=_TOL)

    sampling_frequency = np.round(2 * (number_of_frequencies - 1) / duration, decimals=14)
    return sampling_frequency, duration

def nfft(time_domain_strain, sampling_frequency):
    """ Perform an FFT while keeping track of the frequency bins. Assumes input
        time series is real (positive frequencies only).

    Parameters
    ----------
    time_domain_strain: array_like
        Time series of strain data.
    sampling_frequency: float
        Sampling frequency of the data.

    Returns
    -------
    frequency_domain_strain, frequency_array: (array_like, array_like)
        Single-sided FFT of time domain strain normalised to units of
        strain / Hz, and the associated frequency_array.

    """
    frequency_domain_strain = np.fft.rfft(time_domain_strain)
    frequency_domain_strain /= sampling_frequency

    frequency_array = np.linspace(
        0, sampling_frequency / 2, len(frequency_domain_strain))
    # df = sampling_frequency / len(time_domain_strain)
    # frequency_array = np.arange(0, sampling_frequency / 2, df)

    return frequency_domain_strain, frequency_array

if __name__ == '__main__':
    file = 'time_domain_strain_23Hz.dat'
    file = 'time_domain_strain_30Hz.dat'
    time_domain_strain = np.loadtxt(file)
    sampling_frequency = 4096

    frequency_domain_strain, frequency_array = bcu.nfft(time_domain_strain, sampling_frequency)

    sampling_frequency_new, duration_new = bcu.get_sampling_frequency_and_duration_from_frequency_array(frequency_array)

    try:
        bcu._check_legal_sampling_frequency_and_duration(sampling_frequency_new, duration_new)
        print('\nThe IllegalDurationAndSamplingFrequencyException was'
        ' NOT raised with CURRENT version of'
        ' `get_sampling_frequency_and_duration_from_frequency_array()`.'
        f' \nIndeed sampling_frequency={sampling_frequency_new} and'
        f' duration={duration_new} multiply to'
        f' {sampling_frequency_new * duration_new}')
    except bcu.IllegalDurationAndSamplingFrequencyException as error:
        print(f'\nThe IllegalDurationAndSamplingFrequencyException was '
        'raised for CURRENT version of `get_sampling_frequency_and_duration_from_frequency_array()`'
        f' with message: {error}')

    sampling_frequency_new, duration_new = get_sampling_frequency_and_duration_from_frequency_array(frequency_array)
    try:
        bcu._check_legal_sampling_frequency_and_duration(sampling_frequency_new, duration_new)
        print('\nThe IllegalDurationAndSamplingFrequencyException was'
        ' NOT raised with NEW version of'
        ' `get_sampling_frequency_and_duration_from_frequency_array()`.'
        f' \nIndeed sampling_frequency={sampling_frequency_new} and'
        f' duration={duration_new} multiply to'
        f' {sampling_frequency_new * duration_new}')
    except bcu.IllegalDurationAndSamplingFrequencyException as error:
        print(f'\nThe IllegalDurationAndSamplingFrequencyException was '
        'raised for NEW version of `get_sampling_frequency_and_duration_from_frequency_array()`'
        f' with message: {error}')
