# Tuning of `epsilon` before phase3

## Trying different values
- The step-size `epsilon` (more precisely the mean value from which we draw it) had always been fixed to 5e-3, tuned value that Ed and Yann had derived.

### Constant rejection threshold = 3
- I did 3 different runs with __50 000 trajectories__, modifying only the value of the step-size at the beginning of phase3 and deactivating the switch to hybrids-only if Acc < 65%. These runs kept the process of running 1 hybrid trajectory if 3 purely analytical were rejected in a row. I also have to mention here that for these 4 run, new QR decomposition was run every 500 newly gathered points by the numerical trajectories (unless for run with epsilon=5e-3 where it was every 10000...). 500 is a low threshold meaning runs with a low acceptance rate will spend a lot of time on QR decomposition.

epsilon|Acc|L_psi|L_others|n SIS|duration Ph3| n sec / SIS in Ph3|time on QR|estimated n sec / SIS, without QR
-:|-:|-:|-:|-:|-:|-:|-:|-:
__5.0e-3__ |72%|81|~60|618|10.1h|59 sec|~4min|59 sec
__7.5e-3__ |67%|48|~30|1042|11.4h|39 sec|~2.0h|32 sec
__1.0e-2__ |64%|43|~20|1163|14.2h|44 sec|~3.5h|33 sec
__2.0e-2__ |53%|23|~10|2174|23.5h|39 sec|~7h|27 sec


### Runs without hybrids or numerical
- New runs over __25 000 trajectories__

|epsilon|Acc|L_psi|L_others|n SIS|duration Ph3| Real cost: n sec / SIS in Ph3|Theoretical cost C=1/(Acc*eps)|Relative real cost|Relative theoretical cost|
|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|
__5.0e-3__ |67%|119|~80|210|2.8h|48 sec|298||
__1.0e-2__ |56%|45|~30|555|2.6h|17 sec|179||
__2.0e-2__ |41%|26|~10|961|2.7h|10 sec|122||
__2.5e-2__ |34%|31|~18|806|3.1h|14 sec|118||
__3.0e-2__ |27%|28|~16|892|2.5h|10 sec|123||
__4.0e-2__ |14%|49|~30|510|2.1h|15 sec|179||

- Run over __100 000 trajectories__

epsilon|Acc|L_max|L_others|n SIS|duration Ph3| Real cost: n sec / SIS in Ph3|Theoretical cost C=1/(Acc*eps)|Relative real cost|Relative theoretical cost
-:|-:|-:|-:|-:|-:|-:|-:|-:|-:
__2.0e-2__ |40%|233 (RA)|~10|430|10.2h|90 sec|122||

The problem here is that the chain got __stuck for 1800 consecutive trajectories__... Hence the need for hybrids and numerical to unstuck the chain.
We can see on the corner plot of the run that the spot where it got stuck corresponds to an unlikely place for the following parameters: RA, DEC, and tc which are the three parameters showing now the highest integrated autocorrelation length.



### Adaptive rejection threshold to have same proportions of hybrids
- Runs over __25 000 trajectories__, new QR decomposition done when 10% new points have been gathered (~every 20 000 traj) and with a stuck_count threshold set in accordance with the expected acceptance rate

#### Setting rej-count threshold considering 3 was a good choice

epsilon|Acc|L_psi|L_others|n SIS|duration Ph3|stuck count threshold| hybrid% | Real cost: n sec / SIS in Ph3|Theoretical cost C=1/(Acc*eps)|Relative real cost|Relative theoretical cost
-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:
__5.0e-3__ |71%|91|~75|275|5.0h|3|3.0%|65 sec|282|3.0|2.7
__1.0e-2__ |62%|39|~23|642|6.6h|3|5.1%|37 sec|161|1.7|1.5
__2.0e-2__ |47%|26|~13|962|6.0h|5|3.2%|22 sec|106|1.0|1.0
__3.0e-2__ |31%|37|~13|675|4.5h|9|1.7%|24 sec|106|1.1|1.2
__4.0e-2__ |20%|37|~17|675|7.0h|9|4.3%|37 sec|125|1.7|1.2
__4.0e-2__ |18%|39|~20|641|3.9h|14|1.7%|22 sec|139|1.0|1.3

#### Increasing rej-count threshold

epsilon|Acc|L_psi|L_others|n SIS|duration Ph3|stuck count threshold| hybrid% | Real cost: n sec / SIS in Ph3|Ntot traj
-:|-:|-:|-:|-:|-:|-:|-:|-:|-:|-:
__2.0e-2__ |43%|29|~13|862|3.1h|14|0.3%|13 sec|25 000
__2.0e-2__ |44%|27|~13|3703|12.1h|14|0.2%|12 sec|100 000
__2.0e-2__ |44%|26|~12|5769|17.8h|14|0.2%|11 sec|150 000


## Proper tuning
- From Neal Radford page 30: "For HMC, the cost to obtain an independent point will be proportional to `1/(Acc * epsilon)`". From this he derives an optimal `Acc_opt=65%`. However for us the situation is a bit different since we get a lower acceptance rate due to inaccuracies of our fit, so the optimal 65% is not valid for us. However the cost approach should still be valid.
- Hence my idea to run a 'phase3.0' with different values of epsilon around 5e-3 for only 300 analytical trajectories (+ hybrid or num if gets stuck) and compute the cost relative to each run. However since in our case we run hybrid and numerical trajectories when the chain gets stuck, we should introduce the duration of the run in our cost function:

epsilon | 5.00e-03 | 1.00e-02 | 2.00e-02 | 4.00e-02
-|-:|-:|-:|-:
__Acc after 300 traj__        | 73%      | 62%      | 47%      | 26%
__Duration (min)__            | 5.6 min  | 7.4 min  | 17.8 min | 34.0 min
__Cost = 1/(acc*eps)__        | 2.849315 | 1.686486 | 1.114286 | 1.000000
__Cost = duration/(acc*eps)__ | 1.285094 | 1.000000 | 1.602034 | 2.736808

- The conclusion we can draw from these results are consistent with our previous analysis where an `epsilon = 1e-2` lead to more SIS per seconds than `epsilon = 5e-3`. We can even relate our derived cost `C(eps=5e-3) = 1.28` to the time to get one SIS derived previously since `59 / 44 = 1.34`. Hence our cost estimation after 300 trajectories seem correct.
- The drawback is that for epsilons where the acceptance rate is quite low the duration of the 300 trajectories is not so negligible. With our setup, the maximum duration it would take to run those 300 trajectories would be in the case where all analytical are rejected, then the hybrid is rejected as well and we have to run a numerical trajectory every other 5 trajectories. This would lead to an acceptance rate of 20% if every numerical trajectory is accepted; which is not far from the 26% acceptance rate when epsilon = 4e-2. Hence we can say that at worse such a run would take about 40min. It would still be nice to reduce this.
- Hence my idea to run phase3.0 with 300 purely analytical trajectories, no hybrids or numerical. This way we are sure that each of the 300 trajectories run will take the same time, ie about 2 minutes.
- The problem then obviously is that I can't account for the duration into my cost computation.
- Since duration is directly related to the number of hybrid and numerical traj and hence the acceptance rate, we introduce an ad-hoc cost being C_adhoc = 1/(Acc**2 * epsilon) to favor epsilon leading to high acceptances.

epsilon | 5.00e-03 | 1.00e-02 | 2.00e-02 | 4.00e-02
-|-:|-:|-:|-:
__Acc after 300 purely anan traj__        | 75%      | 59%      | 32%      | 17%
__Duration (min)__                        | 2 min    | 2 min    | 2 min    | 2 min
__Cost = 1/(acc*eps)__                    | 1.857143 | 1.181818 | 1.094737 | 1.000000
__Cost = 1/(acc**2 * eps)__               | 1.234694 | 1.000000 | 1.716120 | 2.863919

- Hence with this setup we see that our ad-hoc costs estimate well the real ones which take into account the duration of the run.
