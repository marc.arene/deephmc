# Understanding how long it takes to compute 1 dlogL

## Timing one logL computation

```py
logL = <s|h> - 0.5 * <h|h>
h = h_ifo = h_source * np.exp(i*2*pi*f*dt)
```
```py
dlogL = <s|dh> - <h|dh>
dlogL_forward = <s|h+> - <s|h> - [<h|h+> - <h|h>]
dlogL_central = <s|h+> - <s|h-> - <h|h+ - h->
```
| Code chunk | h_source | exp(i * 2pi * f * dt)| s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|----|:----:|:----:|:---:|:----:|
| nb calls for 1 `logL` | 1 | n_ifos | n_ifos | 2 x n_ifos |
| nb calls `dlogL_forward` | 1 | n_ifos | n_ifos | 2 x n_ifos |
| nb calls `dlogL_central` | 2 | 2 x n_ifos | 2 x n_ifos | 3 x n_ifos |

| Code chunk | h_source | exp(i * 2pi * f * dt)| s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|----|:----:|:----:|:---:|:----:|
| IMRPhenomD, 30Hz | **12.3 ms** | 3.5 ms | 1.6 ms | 0.9 ms |
| IMRPhenomD_NRTidal, 30Hz | **25.4 ms** | 3.5 ms | 1.6 ms | 0.9 ms |
| IMRPhenomD_NRTidal, 23Hz | **?? ms** | ?? ms | ?? ms | ?? ms |
| IMRPhenomPv2_NRTidal, 30Hz | **57.9 ms** | 3.5 ms | 1.6 ms | 0.9 ms |
| IMRPhenomPv2_NRTidal, 23Hz | **?? ms** | ?? ms | ?? ms | ?? ms |

## Analysis IMRPhenomD with 10 parameters

### Caracteristics
- IMRPhenomD
- 10 parameters: aligned spins but phase marginalization
- flow = 30 Hz => duration = 59 sec
```py
len(frequency_domain_strain) = 120833
ifo.frequency_mask.sum() = 119063
```

### Timing at the end of phase1

```
11:20 gwhmc INFO    : After 1300 trajectories of length 200
11:01 gwhmc INFO    : Total time for phase1: 27.0 hours
11:01 gwhmc INFO    : Average time to compute 1 numerical trajectory: 7.5e+01 sec
11:01 gwhmc INFO    : Average time to compute one numerical gradient dlogL = 373ms
```

### Timing of functions directly
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  393 ms ± 3.5 ms ms
%timeit waveform_generator.frequency_domain_strain(self.likelihood.parameters):
  12.3 ms ± 74.1 µs
%timeit ifo.get_fd_time_translation(self.likelihood.parameters):
  3.5 ms ± 180 µs
%timeit signal_ifo[self.strain_data.frequency_mask] * exp_two_pi_f_dt:
  1.59 ms ± 108 µs
%timeit nwip(ifo.fd_strain, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration):
  902 µs ± 21.3 µs
```

### Timing analysis
- Parameters contributions to `dlogL`

  |  | Diff method | h_source | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
  |:----|:----|:----:|:----:|:----:|:----:|
  | At point | - | 1 | 3 | 3 | 6 |
  | cos(iota) | Forward | 1 | 0 | 3 | 6 |
  | psi | Forward | 0 | 0 | 3 | 6 |
  | ln(D) | Forward | 1 | 0 | 3 | 6 |
  | ln(Mc) | Central | 2 | 0 | 6 | 9 |
  | ln(mu) | Central | 2 | 0 | 6 | 9 |
  | sin(dec) | Forward | 0 | 3 | 3 | 6 |
  | ra | Forward | 0 | 3 | 3 | 6 |
  | ln(tc) | Central | 0 | 6 | 6 | 9 |
  | chi_1 | Central | 2 | 0 | 6 | 9 |
  | chi_2 | Central | 2 | 0 | 6 | 9 |
  | **TOTAL** | - | 11 | 15 | 48| 81  |
  | **IMRPhenomD, 30Hz** | **338ms** | 11x12.3ms | 15x3.5 ms | 48x1.6ms | 81x0.9ms |
  | **IMRPhenomD, 23Hz** | **???ms** | 11x??ms | 15x??ms | 48x??ms | 81x??ms |

```py
swg = 12.3
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 11
n_exp2pifdt = 15
n_stimesexp = 48
n_nwip = 81

timing = n_swg*swg + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
```



## Analysis for IMRPhenomD_NRTidal with 12 parameters

### Caracteristics
- IMRPhenomD_NRTidal
- 12 parameters: aligned spins, tides but phase marginalization
- flow = 30 Hz => duration = 59 sec
```py
len(frequency_domain_strain) = 120833
ifo.frequency_mask.sum() = 119063
```

### Timing at the end of phase1

```
11:20 gwhmc INFO    : After 670 trajectories of length 200
11:20 gwhmc INFO    : Total time for phase1: 24.7 hours
11:20 gwhmc INFO    : Average time to compute 1 numerical trajectory: 132.6 sec
11:20 gwhmc INFO    : Average time to compute one numerical gradient dlogL = 662ms
```

### Timing of functions directly
```py
%timeit likelihood_gradient.calculate_dlogL_s_minus_h_dh():
  672 ms ± 14.5 ms
%timeit waveform_generator.frequency_domain_strain(self.likelihood.parameters):
  25.4 ms ± 177
%timeit ifo.get_fd_time_translation(self.likelihood.parameters):
  3.5 ms ± 180 µs
%timeit signal_ifo[self.strain_data.frequency_mask] * exp_two_pi_f_dt:
  1.59 ms ± 108 µs
%timeit nwip(ifo.fd_strain, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration):
  902 µs ± 21.3 µs
```

### Timing analysis
- Parameters contributions to `dlogL`

|  | Diff method | h_source | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|:----|:----|:----:|:----:|:----:|:----:|
| At point | - | 1 | 3 | 3 | 6 |
| cos(iota) | Forward | 1 | 0 | 3 | 6 |
| psi | Forward | 0 | 0 | 3 | 6 |
| ln(D) | Forward | 1 | 0 | 3 | 6 |
| ln(Mc) | Central | 2 | 0 | 6 | 9 |
| ln(mu) | Central | 2 | 0 | 6 | 9 |
| sin(dec) | Forward | 0 | 3 | 3 | 6 |
| ra | Forward | 0 | 3 | 3 | 6 |
| ln(tc) | Central | 0 | 6 | 6 | 9 |
| chi_1 | Central | 2 | 0 | 6 | 9 |
| chi_2 | Central | 2 | 0 | 6 | 9 |
| lambda_1 | Central | 2 | 0 | 6 | 9 |
| lambda_2 | Central | 2 | 0 | 6 | 9 |
| **TOTAL** | - | 15 | 15 | 60 | 99 |
| **IMRPhenomD_NRTidal, 30Hz** | **619ms** | 15x25.4ms | 15x3.5ms | 60x1.6ms | 99x0.9ms |
| **IMRPhenomD_NRTidal, 23Hz** | **???ms** | 15x??ms | 15x??ms | 60x??ms | 99x??ms |

```py
swg = 25.4
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 15
n_exp2pifdt = 15
n_stimesexp = 60
n_nwip = 99

timing = n_swg*swg + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
```

- Parameters contributions to `dlogL` if forward for all params

|  | Diff method | h_source | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|:----|:----|:----:|:----:|:----:|:----:|
| At point | - | 1 | 3 | 3 | 6 |
| cos(iota) | Forward | 1 | 0 | 3 | 6 |
| psi | Forward | 0 | 0 | 3 | 6 |
| ln(D) | Forward | 1 | 0 | 3 | 6 |
| ln(Mc) | Forward | 1 | 0 | 3 | 6 |
| ln(mu) | Forward | 1 | 0 | 3 | 6 |
| sin(dec) | Forward | 0 | 3 | 3 | 6 |
| ra | Forward | 0 | 3 | 3 | 6 |
| ln(tc) | Forward | 0 | 3 | 3 | 6 |
| chi_1 | Forward | 1 | 0 | 3 | 6 |
| chi_2 | Forward | 1 | 0 | 3 | 6 |
| lambda_1 | Forward | 1 | 0 | 3 | 6 |
| lambda_2 | Forward | 1 | 0 | 3 | 6 |
| TOTAL | - | 9 | 12 | 39 | 78 |
| **IMRPhenomD_NRTidal, 30Hz** | **403ms** | 9x25.4ms | 12x3.5ms | 39x1.6ms | 78x0.9ms |
| **IMRPhenomD_NRTidal, 23Hz** | **???ms** | 9x??ms | 12x??ms | 39x??ms | 78x??ms |

```py
swg = 25.4
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 9
n_exp2pifdt = 12
n_stimesexp = 39
n_nwip = 78

timing = n_swg*swg + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
```


## Analysis forecasts IMRPhenomPv2_NRTidal with 16 parameters

|  | Diff method | h_source | exp(i * 2pi * f * dt) | s = s * exp(i * 2pi * f * dt) | < h1 \| h2 > |
|:----|:----|:----:|:----:|:----:|:----:|
| At point | - | 1 | 3 | 3 | 6 |
| cos(iota) | Forward | 1 | 0 | 3 | 6 |
| psi | Forward | 0 | 0 | 3 | 6 |
| ln(D) | Forward | 1 | 0 | 3 | 6 |
| ln(Mc) | Central | 2 | 0 | 6 | 9 |
| ln(mu) | Central | 2 | 0 | 6 | 9 |
| sin(dec) | Forward | 0 | 3 | 3 | 6 |
| ra | Forward | 0 | 3 | 3 | 6 |
| ln(tc) | Central | 0 | 6 | 6 | 9 |
| a_1 | Central | 2 | 0 | 6 | 9 |
| a_2 | Central | 2 | 0 | 6 | 9 |
| tilt_1 | Central | 2 | 0 | 6 | 9 |
| tilt_2 | Central | 2 | 0 | 6 | 9 |
| phi_12 | Central | 2 | 0 | 6 | 9 |
| phi_jl | Central | 2 | 0 | 6 | 9 |
| lambda_1 | Central | 2 | 0 | 6 | 9 |
| lambda_2 | Central | 2 | 0 | 6 | 9 |
| TOTAL | - | 23 | 15 | 84 | 135 |
| **IMRPhenomPv2_NRTidal, 30Hz** | **1.640sec** | 23x57.9ms | 15x3.5ms | 84x1.6ms | 135x0.9ms |
| **IMRPhenomPv2_NRTidal, 23Hz** | **3.284sec?** | 23x??ms | 15x??ms | 84x??ms | 135x??ms |

```py
swg = 57.9
exp2pifdt = 3.5
stimesexp = 1.6
nwip = 0.9

n_swg = 23
n_exp2pifdt = 15
n_stimesexp = 84
n_nwip = 135

timing = n_swg*swg + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
```
