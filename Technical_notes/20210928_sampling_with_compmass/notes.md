# Modifications brought and bugs encountered when trying to samples with `(m_1, m_2)`


## Remaining implementation
- [CORRECTED] Forward or backward differencing it trajectory gets on the equal mass line:
```py
# If on the equal mass line, central differencing on mass_1 could lead to a mass_1 <= mass_2 and thus we use only forward.
# Equivalently, central differencing on mass_2 could lead to a mass_2 >= mass_1 and thus we use only backward.
cond_forward = cond_forward or parameters_minus['mass_1'] <= parameters_minus['mass_2']
cond_backward = cond_backward or parameters_plus['mass_2'] >= parameters_plus['mass_1']
```

- [CORRECTED] chirp_mass directly indexed in `main.py`
```
File "main.py", line 682, in <module>
  idx_Mc = sampler.search_parameter_keys.index('chirp_mass')
ValueError: 'chirp_mass' is not in list
```
- set duration from low component masses in prior and minimum_frequency
  - Done, however even when setting the low boundaries to `mass_1_min = 1.3` and `mass_2_min = 1`, it leads to (with `flow = 30Hz`) `tc_3p5PN = 77.029 s` thus leading to a duration of 128 sec and not 64 sec.
  - I checked what was the minimum value of the total mass we reached during our HMC run from 30Hz and it was` m.min() = 2.75` solar masses which corresponds to `tc_3p5PN = 60.74 s`, reached with `m1[m.argmin()] = 1.394, m2[m.argmin()] = 1.356`. Note that during that same run, `m1.min() = 1.377, m2.min() = 1.062`.
  - So I am thinking there should really be some prior on the total mass as an input (thus coupled with a prior on the mass ratio), and from it we could derive the duration of the segment.
- investigate on how during the trajectory we can have a logL=516 when m1=m2=5.77
- investigate on the spike in dlogL/dm1 and dlogL/dm2
  - change the trigger parameters to the values when the gradient wrt comp masses is blowing up and check whether we get the same spiky values at the starting point: normally we should not.
  - Put a breakpoint at the computation of the initial gradient
  - Set another run with the initial trigger values and put a breakpoint at the computation of the first blowing gradient
  - Compare side by side why in the computation we get different numbers.

## Investigation on `RuntimeWarning`
```
$ python main.py --event=GW170817 --trigger_file=../examples/trigger_files/GW170817_IMRPhenomD_NRTidal.ini --config_file=../examples/config_files/config_GW170817_IMRPhenomD_NRTidal_compmass.ini --fit_method=dnn --outdir=../__output_data/GW170817real/compmass/
[...]
17:53 gwhmc INFO    : ****************************************************************************
17:53 gwhmc INFO    : PHASE 1 : NUMERICAL GRADIENT
17:53 gwhmc INFO    : Starting phase 1
/Users/marcarene/projects/python/bilby/bilby/gw/conversion.py:473: RuntimeWarning: invalid value encountered in sqrt
  t0 = np.cbrt(9 * a + np.sqrt(3) * np.sqrt(27 * a ** 2 - 4 * a ** 3))
```
- Implementing the forward/backward conditions when deriving on component masses seems to have resolved the RuntimeWarning problem. However

## Investigation on phase 1 trajectories being all rejected
```
13:33 gwhmc INFO    : ****************************************************************************
13:33 gwhmc INFO    : PHASE 1 : NUMERICAL GRADIENT
13:33 gwhmc INFO    : Starting phase 1
13:36 gwhmc INFO    :   1 -    rejected    - Acc =  0.0% - 0.18444 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
13:39 gwhmc INFO    :   2 -    rejected    - Acc =  0.0% - 0.34983 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
13:42 gwhmc INFO    :   3 -    rejected    - Acc =  0.0% - 0.56714 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
13:45 gwhmc INFO    :   4 -    rejected    - Acc =  0.0% - 0.16923 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
/Users/marcarene/projects/python/bilby/bilby/gw/conversion.py:473: RuntimeWarning: invalid value encountered in sqrt
  t0 = np.cbrt(9 * a + np.sqrt(3) * np.sqrt(27 * a ** 2 - 4 * a ** 3))
13:48 gwhmc INFO    :   5 -    rejected    - Acc =  0.0% - 0.58268 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
13:51 gwhmc INFO    :   6 -    rejected    - Acc =  0.0% - 0.66148 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
13:53 gwhmc INFO    :   7 -    rejected    - Acc =  0.0% - 0.62356 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
13:56 gwhmc INFO    :   8 -    rejected    - Acc =  0.0% - 0.28560 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
13:59 gwhmc INFO    :   9 -    rejected    - Acc =  0.0% - 0.66843 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
14:02 gwhmc INFO    :  10 -    rejected    - Acc =  0.0% - 0.52573 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
14:04 gwhmc INFO    :  11 -    rejected    - Acc =  0.0% - 0.00845 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
14:07 gwhmc INFO    :  12 -    rejected    - Acc =  0.0% - 0.00466 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
14:10 gwhmc INFO    :  13 -    rejected    - Acc =  0.0% - 0.73068 > 0.00000 - ?? - mf_snr = 32.152 - eta = 0.238082
```
- Why do the gradients wrt m1, m2 blow up to 1e6
  - starting the analysis with trigger parameters set equal to those when gradient is blowing up to 1e6:
  ```
  [parameters]
  mass_1=1.7260113067712004
  mass_2=1.1074173457413659
  chirp_mass=1.1976134456328078
  mass_ratio=0.6415735002603987
  luminosity_distance=21.227860281446073
  # iota=146*np.pi/180
  iota=2.04318308535
  theta_jn=2.0432148448695906
  psi=1.6085821868787602
  phase=2.10583022414
  ; phase=5.497787143782138
  # Value of geocent_time derived from snr match on data
  # It is 79ms after gwosc time = geocent_time=1187008882.4, where
  # the template in L1 matches best
  geocent_time=1187008882.4299982
  # value from fixed skyposition run
  ra=3.446053579947637
  # value from fixed skyposition run
  dec=-0.4080212249660714
  chi_1=0.027787347722216763
  chi_2=0.02785273955766263
  lambda_1=147.57305744248305
  lambda_2=209.14314374115907
  ```
  we get an initial gradient which does not blow up
  ```
  17:54 gwhmc INFO    : log_likelihood_gradient:
     cos_theta_jn           : 67.95580382143453
     psi                    : -4.600987566686257
     log_luminosity_distance: 28.943108752230046
     mass_1                 : -77259.16757947353
     mass_2                 : -130272.74606583879
     sin_dec                : -127.40168656878345
     ra                     : 143.9333670023783
     log_geocent_duration   : 1448324.0702008035
     chi_1                  : 3206.78785695761
     chi_2                  : 1871.742242135515
     lambda_1               : -0.08727643600064518
     lambda_2               : -0.021494091872572865
  ```
  - First we note that even though the trigger file was set with values above, the initial starting point values are a bit different:
  ```
  17:54 gwhmc INFO    : Triggered parameters derived from ../examples/trigger_files/GW170817_IMRPhenomD_NRTidal_grdt1e6.ini are:
     mass_1              : 1.7260113067712004
     mass_2              : 1.1074173457413659
     chirp_mass          : 1.1977068090866865
     mass_ratio          : 0.6416049196183886
     luminosity_distance : 21.227860281446073
     iota                : 2.04318308535
     theta_jn            : 2.0432148448695906
     psi                 : 1.6085821868787602
     phase               : 2.10583022414
     geocent_time        : 1187008882.4299982
     ra                  : 3.446053579947637
     dec                 : -0.4080212249660714
     chi_1               : 0.027787347722216763
     chi_2               : 0.02785273955766263
     lambda_1            : 147.57305744248305
     lambda_2            : 209.14314374115907
     total_mass          : 2.8334286525125663
     symmetric_mass_ratio: 0.23808410142658604
     reduced_mass        : 0.6745943146897968
  ```
  This is because all the mass parameters are re-derived from m1 and m2 values which indeed means that the values of chirp_mass, reduced_mass, mass_ratio etc at the point where the gradients blows up are not consistent with values of m1 and m2...
  - [CORRECTED]: it was because the function `sampler.q_pos_to_parameters_dict()` was missing a condition to update the orther mass parameters when component masses are used.
  - Then I put a breakpoint when the gradient wrt mass_1 is derived in the two cases and noticed that the value of geocent_duration is different in the two cases while geocent_time and start_time are the same:
    - Breakpoint at the starting point set with values from the point where gradient blew up
    ```
    ipdb> for key in parameters_plus.keys(): print(f'{key}: {parameters_plus[key]}')
    mass_1: 1.7260114067712005
    mass_2: 1.1074173457413659
    chirp_mass: 1.1977068422675274
    mass_ratio: 0.6416048824456957
    luminosity_distance: 21.227860281446073
    iota: 2.04318308535
    theta_jn: 2.0432148448695906
    psi: 1.6085821868787602
    phase: 2.10583022414
    geocent_time: 1187008882.4299982
    ra: 3.446053579947637
    dec: -0.4080212249660714
    chi_1: 0.027787347722216763
    chi_2: 0.02785273955766263
    lambda_1: 147.57305744248305
    lambda_2: 209.14314374115907
    total_mass: 2.8334287525125665
    symmetric_mass_ratio: 0.23808409841510472
    reduced_mass: 0.6745943299653893
    geocent_duration: 61.99982237815857
    ipdb> self.start_time
    1187008820.4301758
    ```
    - Breakpoint at step one of the first trajectory, where the gradient blows up, when using the usual starting point values
    ```
    ipdb> for key in parameters_plus.keys(): print(f'{key}: {parameters_plus[key]}')
    mass_1: 1.7260114067712005
    mass_2: 1.1074173457413659
    chirp_mass: 1.1977068422675274
    mass_ratio: 0.6416048824456957
    luminosity_distance: 21.227860281446073
    iota: 2.04318308535
    theta_jn: 2.0432148448695906
    psi: 1.6085821868787602
    phase: 2.10583022414
    geocent_time: 1187008882.4299982
    ra: 3.446053579947637
    dec: -0.4080212249660714
    chi_1: 0.027787347722216763
    chi_2: 0.02785273955766263
    lambda_1: 147.57305744248305
    lambda_2: 209.14314374115907
    total_mass: 2.8334287525125665
    symmetric_mass_ratio: 0.23808409841510472
    reduced_mass: 0.6745943299653893
    geocent_duration: 61.999822354995096
    ipdb> self.start_time
    1187008820.4301758
    ```
    - The thing is that both geocent_duration are Python consistent since:
    ```
    ipdb> 61.99982237815857 + 1187008820.4301758
    1187008882.4299982
    ipdb> 61.999822354995096 + 1187008820.4301758
    1187008882.4299982
    ```
    Which in fact is consistent with what I noticed when bilby was wrongly computing the geocent_duration
