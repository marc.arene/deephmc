# Results of the architecture optimization of the DNN

**The left picture/plot corresponds to the old architecture, the right one to the new one**

## Architectures
![models.png](models.png)

- The code defining each model highlights that I also modified the first activation function and the loss function
```py
def old_dnn(self):
    model = Sequential()
    model.add(Dense(
        units=10,
        input_dim=10,
        activation='relu'
        ))
    model.add(Dense(
        units=1000,
        activation='relu'
        ))
    model.add(Dense(
        units=100,
        activation='relu'
        ))
    model.add(Dense(
        units=10,
        activation='linear'
        ))
    model.compile(optimizer='adam', loss='mae')
    return model
```
```py
def new_dnn(self):
    model = Sequential()
    model.add(Dense(
        units=100,
        input_dim=10,
        activation='linear'
        ))
    model.add(Dense(
        units=100,
        activation='relu'
        ))
    model.add(Dense(
        units=1000,
        activation='relu'
        ))
    model.add(Dropout(
        rate=0.25
        ))
    model.add(Dense(
        units=10,
        activation='linear'
        ))
    model.compile(optimizer='adam', loss='mse')
    return model
```

## Training histories
![hist.png](hist.png)

- Notes:
  - In both cases the training set was the same = 142560 samples
  - In both cases the batchsize is = 1425, ie 1% of the training set
  - Old DNN training took ~6min vs ~15min for the new one
- The dropt-out layer in the new DNN is mainly responsible for the validation/test loss nicely following the train loss meaning we avoid over-fitting our data

## Regression comparisions
- Notes: on this plot, the old DNN was tested on a validation set twice smaller than that for the new DNN; meaning its performances shown here, compared to the new DNN, are _pumped up_ a bit.
![regressions.png](regressions.png)

## Impact on phase3
|| Old DNN | New DNN|
|:-|-:|-:|
| Acceptance rate | 45% | 55% |
| Time / trajectory | 0.11 sec | 0.11 sec|
| Time / SIS | 2.6 sec | 2.2 sec |
|Phase 3 duration 5000 SIS| 3.6h | 3.0h |
