# Introduction of spin parameters in the HMC

- What spin parameters to use wrt to approximant used...?
  - IMRPhenomPv2: `a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl`
  - IMRPhenomD: `chi_1, chi_2` => how does bilby deals with those??

## Using IMRPhenomPv2_ROQ: `a_1, a_2, tilt_1, tilt_2, phi_12, phi_jl`
- Using the high spin prior for the moment

- FIM
  - Even more singular than before (even though det(fim)~1e60...!)
  - I had to manually tweak the scales such that phase1 would work
  ```
            cos_theta_jn     psi     ln(D)    ln(Mc)    ln(mu)     sin(dec)    ra      ln(tc)       a_1       a_2     tilt_1     tilt_2     phi_12    phi_jl
  from FIM      1.436915  0.535519  1.443941  0.000025  0.003678  0.005605  0.004839  0.000002  0.936305  11.76086  10.043298  68.239147  76.135971  4.394672
  manual        1.000000  1.570796  0.500000  0.000025  0.003678  0.005605  0.004839  0.000002  0.044500   0.04450   0.157080   0.157080   3.141593  3.141593
  ```

- Phase1
  - Working quite well! Acc ~ 87%
  - Timing:
    - 1 num traj now takes ~12sec vs ~5sec before (using ROQ)
    - IMRPhenomPv2 non ROQ: 1 num traj ~ 4min ...! => phase1 with 1500 would be ~100h...
  - Some trajectories get rejected because -it seems- `dlogL/dlnmu` peaks for some unknown reason. Drop of ~8% in Acc
  ![dlogL_dlnmu_peak_1](dlogL_dlnmu_peak_1.png)
  ![dlogL_dlnmu_peak_2](dlogL_dlnmu_peak_2.png)
  - I tried plotting the logL around that point to look for any _steppy_ behavior but found nothing for the moment...
  - My take is that this is happening whenever `lnmu` is railing on a boundary at about `-0.382` corresponding to `mu = 0.69`, boundary we can see on the corner plots. Is it due to the ROQ compmin = 0.58?
  - Corner and walker plots:
  ![1500_samples](1500_samples.png)
  - Looking at the walker plots, I fear a high autocorrelation of the chains...
  ![1500_samples_walkers](1500_samples_walkers.png)

- Phase3
  - Analytical and hybrid get rejected
  - If scales are set from the 68% phase1 intervals, numerical traj get rejected too...!
  - [Make regression plots]

## Using IMRPhenomD: `chi_1, chi_2`

- Using low spin prior: `chi_1, chi_2 in [-0.05, 0.05]`
- Because of an initial mistake, the results below are for __wrong__ prior: `chi_1, chi_2 in [0, 0.05]`

### Phase1
- Injected values: `chi_1 = chi_2 = 0.02`
- Scales setting:
```
00:40 gwhmc INFO    : Computing the Fisher Information Matrix...
00:40 gwhmc INFO    : Projecting the FIM on subspace since phase is marginalized.
00:40 gwhmc INFO    : Scales set from the FIMs:
                         cos_theta_jn        psi     ln(D)    ln(Mc)    ln(mu)  sin(dec)        ra    ln(tc)     chi_1     chi_2
From FIM with spins         1.399171  11.852809  1.403088  0.000348  0.082559  0.005649  0.004946  0.000016  6.182205  7.104861
From FIM without spins      1.399025  11.852796  1.402968  0.000015  0.000543  0.005643  0.004943  0.000001       NaN       NaN
Scale Chosen                1.399025  11.852796  1.402968  0.000015  0.000543  0.005643  0.004943  0.000001  6.182205  7.104861
00:40 gwhmc WARNING : Scale of cos_theta_jn is bigger than its range meaning the FIM is singular and some scales might be wrongly derived
00:40 gwhmc WARNING : Reducing scale of cos_theta_jn from 1.40e+00 to 1.0
00:40 gwhmc WARNING : Setting scale of psi from 1.19e+01 to pi/2
00:40 gwhmc WARNING : Reducing scale of ln(D) from 1.40e+00 to 0.5
00:40 gwhmc WARNING : Reducing scale of chi_1 to half of the dynamical range from 6.18e+00 to 2.50e-02
00:40 gwhmc WARNING : Reducing scale of chi_2 to half of the dynamical range from 7.10e+00 to 2.50e-02
00:40 gwhmc INFO    : Scales setting:
              cos_theta_jn        psi     ln(D)    ln(Mc)    ln(mu)  sin(dec)        ra    ln(tc)     chi_1     chi_2
From FIM         1.399025  11.852796  1.402968  0.000015  0.000543  0.005643  0.004943  0.000001  6.182205  7.104861
Constrained      1.000000   1.570796  0.500000  0.000015  0.000543  0.005643  0.004943  0.000001  0.025000  0.025000
```
- End of phase1 result after 1500 trajectories:
```
18:44 gwhmc INFO    : 1500 - ** ACCEPTED ** - Acc = 96.4% - 0.78635 < 0.98911
18:44 gwhmc INFO    : Making corner plots of 289200 accumulated points.
18:44 gwhmc INFO    : Making corner plots of 1500 samples.
18:44 gwhmc INFO    : Total time for phase1: 32.3 hours
18:44 gwhmc INFO    : Average time to compute 1 numerical trajectory: 7.8e+01 sec
18:44 gwhmc INFO    : Average time to get 1 accepted trajectory     : 8e+01 sec
18:44 gwhmc INFO    : Average time to compute one numerical gradient dlogL = 387ms
```
![1500_samples_corner_chi12.png](1500_samples_corner_chi12.png)

## Phase3
- How many numerical trajectories needed in phase1 to have a good fit in phase3?
- Comparing results of 5000 trajectories in phase3 each produced after 1500, 2000 and 2500 numerical trajectories

| |Phase1 duration|Nb qpos_fit|Phase2 duration|Acc Phase3 only|Nb of hybrid traj|Nb of num traj|Phase3 duration|Total run time|
|-|-:|-:|-:|-:|-:|-:|-:|-:|
|1500|32.3 hours|289200|234 sec|74.9%|98 / 5000|57 / 5000|1.10 hours|33.5 hours|
|2000|43.0 hours|388000|314 sec|73.5%|82 / 5000|33 / 5000|0.91 hours|44.0 hours|
|2500|53.8 hours|487400|444 sec|77.6%|51 / 5000|18 / 5000|0.74 hours|54.6 hours|

### Goodness of fit comparisions

- Phase1 with 1500 numerical trajectories
![289200_regression_cubic_TestedTrajs100_dlogL01234567891516.png](289200_regression_cubic_TestedTrajs100_dlogL01234567891516.png)
- Given the good results from this run, we could have expected that 2000 and 2500 numerical trajectories in phase1 were not going to improve the fit much
- Phase1 with 2500 numerical trajectories
![487400_regression_cubic_TestedTrajs100_dlogL01234567891516.png](487400_regression_cubic_TestedTrajs100_dlogL01234567891516.png)
