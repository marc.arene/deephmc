# Analysis of phase1 acceptance rate depending on the scales it starts with

- From Neal Radford we know that the optimal acceptance rate of the HMC is 65%. We can achieve a higher rate but that would result in higher correlated samples and in the end a longer chain to run in order to get the same number of SIS.
- So if Acc > 65%, it means that our scales are to conservative; the hamiltonian trajectories are accurate but the chain is not moving far enough.
- At the beginning of phase1 we derive our scales from the FIM and it is hard to know in advance whether these are optimal.
- Updating the scales at the end of phase1 from the covariance matrix turned to be a good reajustment in between each parameter such that we don't have one parameter accounting for all of the correlation.
- However it is still not clear whether the _covariant scales_ are optimal with respect to the 65% acceptance rate we should aim for. Maybe they should all be increased or the same factor.
- Just looking at the acceptance rate from phase3 is not a good indicator since the main reason why it decreases compared with phase1 is the inaccuracies of the analytical fit of the gradients, not the new scales.
- And since the acceptance in phase3 remains higher than 65%, it seems quite clear that our scales are still too conservative.
- So the idea it to run several numerical trajectories but setting the scales from the covariance matrix of points gathered from a previous run, here 80 000.
- As long as the acceptance rate is superior to 65% we can increase all the scales by the same factor. Once this factor is known, we will introduce it in our algorithm at the beginning of phase3 to set the scales equal to the covariance matrix times this factor, hoping for the optimal autocorrelation length on the long run.
- In the following results, I should highlight that the length of numerical trajectories has been set to 100.

- GW170817 real data, using IMRPhenomD, computing the FIM on a high likelihood point.
- Scales derived from the FIM and then constrained are:
```
                          cos_theta_jn       psi     ln(D)    ln(Mc)    ln(mu)  sin(dec)        ra    ln(tc)     chi_1     chi_2
From FIM with spins         0.173234  0.165502  0.374001  0.001115  0.249736  0.028705  0.020026  0.000053  5.618018  8.608889
From FIM without spins      0.173001  0.165278  0.373359  0.000047  0.001618  0.028682  0.019995  0.000007       NaN       NaN
Constrained                 0.173001  1.570796  0.373359  0.000047  0.001618  0.028682  0.019995  0.000007  0.050000  0.050000
From 80 000 run             0.206028  0.949627  0.337924  0.000078  0.008215  0.033328  0.022384  0.000008  0.020707  0.025119
```

- Phase1 with the scales naturally derived (and constrained) from the FIM had led to an acceptance rate of 99.0% after 800 numerical trajectories (of length 200)


|Scales used|Nb of num traj run|Acceptance rate|
|-|-|-|
|FIM constrained|800|99.0%|
|Cov 80 000|300|98.3%|
|Cov 80 000 * 2|300|96.0%|
|Cov 80 000 * 4|300|90.3%|
|Cov 80 000 * 4|800|91.6%|
