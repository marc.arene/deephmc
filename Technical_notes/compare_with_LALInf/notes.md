# Comparing the HMC with LALInferenceMCMC

## GW170817
The web page referencing all the runs that have been carried out on GW170817, comparisions etc, at the time of the analysis is [here](https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/ParameterEstimationModelSelection/O2_PE/O2/1187008882p4457)

### LALInf-TaylorF2-Tides
- [Here](https://ldas-jobs.ligo.caltech.edu/~aaron.zimmerman/O2/G298048/TidalP1_MCMC/lalinferencemcmc/TaylorF2threePointFivePN/1187008882.45-0/V1H1L1/posplots.html) is the link of the LALInference run.
- The samples can be found [here](https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/ParameterEstimationModelSelection/O2_PE/O2/1187008882p4457) where the run is named _TidalP-1_ in the section _Runs used in October publications_

### HMC-IMRPhenomD_NRTidal
#### Settings
- Common settings between the two runs
  - `fhigh = 2048 Hz`
  - low spin prior: `a < 0.05`
  - phase marginalization

##### Differences in the global settings between the two runs

| | HMC | LALInf-TidalP-1 |
|:-|-|-|
| flow | 40Hz | 30Hz |
| Calibration uncertainty marginalization | No | Yes |
| PSD | GWTC1_GW170817_PSDs.dat | offline BW |
| H1 data | H-H1_LOSC_CLN_4_V1-1187007040-2048.hdf5 | H1_cleaned: H1_HOFT_C00_T1700401_v2 |
| L1 data | L-L1_LOSC_CLN_4_V1-1187007040-2048.hdf5 | L1_BW_deglitched: L1_HOFT_C00_T1700401_v3 |
| V1 data | V-V1_LOSC_CLN_4_V1-1187007040-2048.hdf5 | V1Online |


#### Corner plots comparision with HMC-IMRPhenomD_NRTidal

![corner_HMC_vs_LI_5000_theta_jn-psi-luminosity_distance](corner_HMC_vs_LI_IMRPhenomD_NRTidal/corner_HMC_vs_LI_5000_theta_jn-psi-luminosity_distance.png)

- The discrepancy in `chirp_mass` (and that in `tc`, see below) might be due to a wrong modification that I had made at the beginning of the run when selecting the segment length were I got it wrong in the duration by `1/sampling_frequency`... (See [this bilby issue](https://git.ligo.org/lscsoft/bilby/-/issues/480) I raised and closed)
![corner_HMC_vs_LI_5000_chirp_mass-reduced_mass-mass_1-mass_2](corner_HMC_vs_LI_IMRPhenomD_NRTidal/corner_HMC_vs_LI_5000_chirp_mass-reduced_mass-mass_1-mass_2.png)

![corner_HMC_vs_LI_5000_mass_1-mass_2-chi_1-chi_2](corner_HMC_vs_LI_IMRPhenomD_NRTidal/corner_HMC_vs_LI_5000_mass_1-mass_2-chi_1-chi_2-lambda_1-lambda_2.png)

![corner_HMC_vs_LI_5000_ra-dec-geocent_time](corner_HMC_vs_LI_IMRPhenomD_NRTidal/corner_HMC_vs_LI_5000_ra-dec-geocent_time.png)


### HMC-IMRPhenomD
#### Settings
- Common settings between the two runs
  - `flow = 30 Hz`
  - `fhigh = 2048 Hz`
  - low spin prior: `a < 0.05`
  - phase marginalization

##### Differences in the global settings between the two runs

| | HMC | LALInf-TidalP-1 |
|:-|-|-|
| Approximant | IMRPhenomD | TaylorF2-Tides |
| Calibration uncertainty marginalization | No | Yes |
| PSD | GWTC1_GW170817_PSDs.dat | offline BW |
| H1 data | H-H1_LOSC_CLN_4_V1-1187007040-2048.hdf5 | H1_cleaned: H1_HOFT_C00_T1700401_v2 |
| L1 data | L-L1_LOSC_CLN_4_V1-1187007040-2048.hdf5 | L1_BW_deglitched: L1_HOFT_C00_T1700401_v3 |
| V1 data | V-V1_LOSC_CLN_4_V1-1187007040-2048.hdf5 | V1Online |
| Mass priors | Flat in `mass_1/2` | Flat in `mass_1/2` ? |


 - Are the priors on masses the same? I thought LALInf was always using uniform priors in component masses, but then I am having a hard time understanding [this web page](https://ldas-jobs.ligo.caltech.edu/~aaron.zimmerman/O2/G298048/Comparisons/TP1vBBHPrior/), which is referenced on the main GW170817 PE page under _TP-1 vs BBH Prior of P-1_.

#### Corner plots comparison with HMC-IMRPhenomD (sampling directly with correct priors)
- The little discrepancy on the `luminosity_distance` marginalized posterior comes from the fact that the LALInf run gives a bit of support for a face-on binary, ie `theta_jn ~ 0`, leading to more samples at a high distance than what the HMC produces.
![corner_HMC_vs_LI_5313_theta_jn-psi-luminosity_distance](no_reweight/corner_HMC_vs_LI_5313_theta_jn-psi-luminosity_distance.png)

- There are slight discrepencies for both `chirp_mass` and `reduced_mass` which result in a difference on the location of the peak in the `mass_1 - mass_2` plane, however the global "banana" shape at the 68% CI overlapps perfectly.
![corner_HMC_vs_LI_5313_chirp_mass-reduced_mass-mass_1-mass_2](no_reweight/corner_HMC_vs_LI_5313_chirp_mass-reduced_mass-mass_1-mass_2.png)

- Spins overlap really nicely, the prior used here is uniform on `a_z` between [0, 0.05] and uniform in cos(tilt) to form a compound prior.
![corner_HMC_vs_LI_5313_mass_1-mass_2-chi_1-chi_2](no_reweight/corner_HMC_vs_LI_5313_mass_1-mass_2-chi_1-chi_2.png)

- Looking at the plots here, we can see that the HMC run provides a better sky localization than the LALInf run; this should be due to differences in the input data used.
![corner_HMC_vs_LI_5313_ra-dec-geocent_time](no_reweight/corner_HMC_vs_LI_5313_ra-dec-geocent_time.png)


#### Performances comparison
- 5313 SIS for the HMC

|| HMC | LALInferenceMCMC|
|:-|-:|-:|
| Acceptance rate | 65% | ??% |
| Integrated autocorrelation lenght | 16 | 707 |
| Time / SIS | 2.39 sec | ?? sec |
|Total run duration on 1 CPU| 30.7h | ??h |
|Phase1 duration on 1 CPU| 27.0h | / |
|Phase3 duration on 1 CPU| 3.5h | / |
|Estimated duration on 8 CPU| 7.5h | / |


## Comparision with REWEIGHTED HMC samples
##### Notes on priors
For the moment I can only reweight the HMC samples with "correct" priors, here corresponding to what I believe LALInf has used.
  ```py
  # HMC implicit priors used when computing hamiltonian trajectories
  theta_jn = Sine(name='theta_jn', minimum=0, maximum=np.pi, boundary='reflective')
  psi = Uniform(name='psi', minimum=0, maximum=np.pi, boundary='reflective')
  luminosity_distance = LogUniform(name='luminosity_distance', minimum=1, maximum=70, unit='Mpc', boundary='reflective')
  chirp_mass = LogUniform(name='chirp_mass', minimum=0.8705505632961241, maximum=2.2634314645699227, unit='$M_{\odot}$', boundary=None)
  reduced_mass = LogUniform(name='reduced_mass', minimum=0.5, maximum=1.3, boundary='reflective')
  mass_1 = Constraint(name='mass_1', minimum=1.0, maximum=2.6)
  mass_2 = Constraint(name='mass_2', minimum=1.0, maximum=2.6)
  ra = Uniform(name='ra', minimum=0, maximum=2 * np.pi, boundary='reflective')
  dec = Cosine(name='dec',  minimum=-np.pi / 2, maximum=np.pi / 2, boundary='reflective')
  geocent_time = LogUniform(name='geocent_time', minimum=tc_trig - 0.05, maximum=tc_trig + 0.05, boundary='reflective', latex_label='$t_c$', unit='$s$')
  chi_1 = Uniform(name='chi_1', minimum=-0.05, maximum=0.05, boundary='reflective')
  chi_2 = Uniform(name='chi_2', minimum=-0.05, maximum=0.05, boundary='reflective')
  ```

  ```py
  # LALInf assumed priors used for reweighting
  theta_jn = Sine(name='theta_jn', boundary='reflective')
  luminosity_distance = PowerLaw(name='luminosity_distance', alpha=2, minimum=10, maximum=100, unit='Mpc', boundary=None)
  psi = Uniform(name='psi', minimum=0, maximum=np.pi, boundary='periodic')
  chirp_mass = Constraint(name='chirp_mass', minimum=1.1838325, maximum=2.16847416667, unit='$M_{\odot}$')
  mass_1 = Uniform(name='mass_1', minimum=0.834498333333, maximum=7.73105475907, unit='$M_{\odot}$')
  mass_2 = Uniform(name='mass_2', minimum=0.834498333333, maximum=7.73105475907, unit='$M_{\odot}$')
  ra = Uniform(name='ra', minimum=0, maximum=2 * np.pi, boundary='periodic')
  dec = Cosine(name='dec', boundary='reflective')
  geocent_time = Uniform(name='geocent_time', minimum=tc_trig - 0.05, maximum=tc_trig + 0.05, boundary='reflective', latex_label='$t_c$', unit='$s$')
  chi_1 = bilby.gw.prior.AlignedSpin(name='chi_1', a_prior=Uniform(minimum=0, maximum=0.05), boundary='reflective')
  chi_2 = bilby.gw.prior.AlignedSpin(name='chi_2', a_prior=Uniform(minimum=0, maximum=0.05), boundary='reflective')
  ```

#### Corner plots
- The little discrepancy on the `luminosity_distance` marginalized posterior comes from the fact that the LALInf run gives a bit of support for a face-on binary, ie `theta_jn ~ 0`, leading to more samples at a high distance than what the HMC produces.
![corner_combined_cit_theta_jn-psi-luminosity_distance](corner_combined_cit_theta_jn-psi-luminosity_distance.png)

- There are slight discrepencies for both `chirp_mass` and `reduced_mass` which result in a difference on the location of the peak in the `mass_1 - mass_2` plane, however the global "banana" shape at the 68% CI overlapps perfectly.
![corner_combined_cit_chirp_mass-reduced_mass-mass_1-mass_2](corner_combined_cit_chirp_mass-reduced_mass-mass_1-mass_2.png)

- Spins overlap really nicely, the prior used here is uniform on `a_z` between [0, 0.05] and uniform in cos(tilt) to form a compound prior.
![corner_combined_cit_mass_1-mass_2-chi_1-chi_2](corner_combined_cit_mass_1-mass_2-chi_1-chi_2.png)

- Looking at the plots here, we can see that the HMC run provides a better sky localization than the LALInf run; this should be due to differences in the input data used.
![corner_combined_cit_ra-dec-geocent_time](corner_combined_cit_ra-dec-geocent_time.png)


#### Performances
|| HMC | LALInferenceMCMC|
|:-|-:|-:|
| Acceptance rate | 55% | ??% |
| Integrated autocorrelation lenght | 18 | 707 |
| Time / SIS | 2.2 sec | ?? sec |
|Total run duration on 1 CPU| 20h | ??h |
