
# Plan of action

1. Check numerical derivatives for phase 1 and for hybrid trajectories in phase 3
The question is what type of numerical differencing works best.  There are two ways of getting the derivatives of the
log likelihood.
    - Either: numerical differencing on the waveforms
    - Or: numerical differencing of the likelihood itself

2. Once you have results from 1), code up both the numerical and hybrid trajectories, and complete phase 3.

# 1: Integrating bilby waveform generation into the hmc

For the moment I only wrote a little script: `bilby_tests.py` which generates waveforms with bilby but:
- only for Binary1.in
- The array returned is in the hmc format: ie an array of real elements alternating real and imaginary parts

Do I want use the complex array structure of bilby in the hmc ?

Ultimately yes, but for the moment let's keep it simple so I don't have to spend time adapting the hmc code so it can work with complex arrays. Hence do the following:

1. Create a function: `sigpar_to_injection_parameters()` in which I write a function which takes as input the `sigpar` array used in hmc and returns the associated `injection_parameter` dictionary which bilby needs.
2. Write a script: `bilby_waveform.py` which is basically a copy of `bilby_tests.py` but more generic
3. Time rapidly in a notebook with `%timeit` how long it takes bilby to generate one waveform. Compare this with the python function: `tf2.TaylorF2_Waveform_Threedetectors()`
    - Look at notebook `Timing_WaveForm_Generation.ipynb` to have conclusion on the timing issue with bilby waveform generation
4. Using complex arrays in the hmc: out of the 24ms `bilby_wv.WaveForm_ThreeDetectors()` is taking:
    - 4ms are due to the zero padding
        - is this necessary? Why don't they do it/ need it in bilby?
        - can I optimize my code, find smth faster than `signal_H=np.pad(signal_H,(n1,n2),'constant',constant_values=(0,0+0j))` ?
    - 6ms are due to the ZipMerging to go from complex array to alternating real an imag parts
    => I should make the hmc complex-array compatible.
        - Either adapt the following functions:
            - buf.LVC_RiemannSum()
            - buf.Log_Likelihood_SNR_ThreeDetectors()
        - Or only use bilby's functions: .noise_weighted_inner_product(), .likelihood?()
            - use gw.utils.noise_weighted_inner_product()
                - TODO: make it more efficient by only integrating over non-zero indices
            - create my likelihood function
            - remove the hmc noise addition by bilby's.
            - Notes:
                - my snr and logL take 11ms to compute. I should try to reduce that a bit.
                - I don't always get the same logL and snr. Because the white noise creation is random and change due to the following lines in `utils.create_white_noise()`:
                    ```python
                        re1 = np.random.normal(0, norm1, len(frequencies))
                        im1 = np.random.normal(0, norm1, len(frequencies))
                    ```
            - Write one instanciation of noise for each detector in a file and read from it then
                - finish importing the written files
5. Sampling frequency
    - sigpar[14] vs sigpar[15]
    - now using sigpar[15] = f_high for the low bound prior on masses
    - try using sigpar[14] = f_high for the template we are considering
    - we sample at sigpar[15] so we are certain to sample fast enough for the smallest binary part of our prior, but we set the waveforms to zero after sigpar[14] because otherwise it would mean having frequency content over the last stable orbit
    - bilby's waveform generator does the following:
        - The frequency array for the signals and interferometers goes from 0Hz to sigpar[15] which is fine
        - However the signals are set to 0 not at sigpar[14] = 1802Hz but at some frequency after: 2047.98Hz, corresponding to the index = 71899 and I don't understand how.
6. Start_time vs geocent_time vs GMST
    - at the beginning at would fix `geocent_time = pu.gmst_to_gps_time(sigpar[18])` and `start_time` would vary with `sigpar[8]` using: `start_time = geocent_time - sigpar[8]`. However by doing this bilby's waveform generation wouldn't vary when sigpar[8] varies.
    - In `bilby.source.lal_binary_neutron_star()`, `geocent_time` is described to be the time at coalescence. However it is not used in the function, not passed to `lalsim.SimInspiralChooseFDWaveform()`... Checked [here](https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/group___l_a_l_sim_inspiral_taylor_x_x__c.html#gab06324f50ad04c1ecd078427d615e312) and [here](https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_inspiral_8c_source.html) (line 1050) to understant how `lalsim` is called and used.
    - However `geocent_time` is used when calling `ifo.get_detector_response()` => `ifo.antenna_response()` => `gwutils.get_polarization_tensor()` which runs:
    ```python
    greenwich_mean_sidereal_time = gps_time_to_gmst(time)
    theta, phi = ra_dec_to_theta_phi(ra, dec, greenwich_mean_sidereal_time)
    ```
    Hence when `geocent_time` varies, the waveform changes because a new gmst is considered. TRUE but...
    - `geocent_time` is also used to compute `dt = parameters['geocent_time'] + time_shift - self.strain_data.start_time` in `ifo.get_detector_response()`. This line corresponds to what Yann does when he first compute the phase at Handford: `phiwave = Taylorf2_phase_3_5PN(f, psi, m_secs, eta, tc, phic)` and then calculates the delta_phase `c0 = phi_wave - deltaT_TPI_LH*freq` for the other detectors.
    - On the contrary in the hmc code: the GMST angle which is set in the Binary.in file is never modified, but when `tc=sigpar[8]` varies, it is used in the phase computation: `Taylorf2_phase_3_5PN(f, psi, m_secs, eta, tc, phic)`

# 2: Numerical differencing on the waveform

3. Write a `TaylorF2_h_dh_Generation_ThreeDetectors_bilby()` which uses bilby's waveform generation to do the numerical differencing. Call this one in `dlogL_ThreeDetectors()`
    - rewrite the `noise_weighted_inner_product()` such that it only computes the integral over indices for which aa and bb are !=0. Hence add for inputs sigpar[14] to get `i_high=int(sigpar[14]*ifo.duration)`
    - then rewrite my new snr function using this new `noise_weighted_inner_product()` by copy pasting bilby's code.
    - time if it takes really less time with this new method
4. Time how long it takes for Phase 1 to complete
    - `bilby_wv.dlogL_ThreeDetectors()` => 325ms
    - `stf2.dlogL_ThreeDetectors()` => 32ms
    - Hence Phase1 should be 10 times slower than with the swig wrapped code... Indeed it should take: 1500 x 200 x 0.325 / 3600 h = 27.1 hours
    - run it with 15 num trajectories to check that it takes 0.27 hours = 16 minutes
    - FINISH coding up new `noise_weighted_inner_product()` which is 20% faster
5. I should probably finish coding `sigpar_new_dlnMc()` and `sigpar_new_dlnmu()` for the cases when m2 gets > m1

# 3: Numerical differencing on logL

1. Write a function `Log_Likelihood_ThreeDetectors_from_sigpar(template_ifos, sigpar, shf_L, shf_V, params, n)`
2. Write a function `dlogL_ThreeDetectors_2(template_ifos, shf_L, shf_V, sigpar, n_freq)` which computes the central differencing on the likelihood itself using the previous function created. It should look something like:
    ```python
    dlogL_ThreeDetectors_2():
        dlogL = np.empty(9)

        # Deriving w.r.t. np.cos(inc) where inc = sigpar[0]
        sigpar_minus = sigpar.copy()
        sigpar_minus[0] = np.arccos(np.cos(sigpar[0])-offset)

        sigpar_plus = sigpar.copy()
        sigpar_plus[0] = np.arccos(np.cos(sigpar[0])+offset)

        dlogL[0] = (Log_Likelihood_ThreeDetectors_from_sigpar(.., sigpar_plus, ..) - Log_Likelihood_ThreeDetectors_from_sigpar(.., sigpar_minus, ...) )/ (2*offset)

        # Deriving w.r.t. phi_c = sigpar[1]
        ...

        return dlogL
    ```
    - Does Ed expects me to actually write this function but using bilby's way of calculating the likelihood?
    - Probably yes, it would make more sens because otherwise it's quite clear that this method (using hmc to compute the likelihood differences) will be slower than the 1st one (18 waveform generations vs 17).
    - I should ask on slack how to retreive the likelihood with their `GravitationalWaveTransient` object


# Meeting with Ed

- Poor Acceptance rate at phase 1:
    - I had an error in my dh_wave calculations: only the real part was stored. But this does not improve my Acc
    - Comparing my scale computation wrt Yann's, there are some orders of magnitude difference for several parameters: theta, phi, psi?
    - Acc = 13%: trajectories not accepted either because:
        - Trajectory breaks before the end because of NaN: `General check on NaN is POSITIVE. Concerned indices are: [0]`
        - `logL_final = NaN` because `mf = NaN`, because `network_snr = np.sqrt(snr_1_squared + snr_2_squared + snr_3_squared) = np.sqrt( +417 - 157 - 873)`, because some `<s|h>` are negative
- Hints about things I might do wrong:
    - start_time vs geocent_time: cf above
    - Ra, Dec vs theta, phi: cf `core.utils.ra_dec_to_theta_phi()`:
    ```python
        phi = ra - gmst
        theta = np.pi / 2 - dec
    ```
- I have started using their `likelihood = GravitationalWaveTransient` object. It in fact does the same as what I coded up manually.
    - I don't understand why in my `bilby.example.tutorials.Understand GravitationalWaveTransient object` notebook it takes 4ms to call `likelihood.log_likelihood_ratio()` but it takes 23ms when I call it in the hmc code...

- compute overlap between hmc templates and bilby templates
- then compute the derivatives of the waveform at the value of the binary and compare with hmc

- print out at the end of each trajectory:
    - logL_final
    - logL_0
    - metropo ratio
    - uniform draw: `a`
- Compare Fisher Matrices:
    ```python
    FisherMatrix0 = bilby_wv.FisherMatrix_ThreeDetectors(sigpar, start_time, interferometers)
    FisherMatrix = tf2_for_bilby.FisherMatrix_ThreeDetectors(sigpar, n_freq, interferometers)
    ```
- Ask LALParam people how they compute the new chirp time with f_ref
- Why are likelihoods different: 1022 vs 991 when snr is the same ?
    - they are not if you compute `logL = opt_snr**2/2`
- On two different terminals run the trajectories with `tf2_for_bilby` in one and `bilby_wv` in the other, then using `import IPython; IPython.embed()` compare why the numbers get so different when they are initially the same !
    - on branch bilby: compute the new acceptance rate when using the Yann's scales: better than 40%?
- print h+ and hx for bilby and tf2
- What I know:
    - The waveforms from bilby and tf2 are different
        - => sigpar is badly converted into injection_parameters. I might get the same (approximately) snr and likelihood by chance. I should check for other Binary.in (or sigpars) if it is the case.
    - Then when compute the derivatives: dh_wave, since they are numerical it's very likely that for one of the parameters in bilby, I do numerical differencing on one sigpar when the injected param for bilby is not exactly that, hence the dh_wave_bilby is wrong, my dlogL are wrong, hence my FIM is wrong, hence my scales are wrong
        - => I should compare the dh_wave at the start
        - =>=> in compare.py: do one figure for each parameter with two plots: one for dh_wave_bilby[] and one for dh_wave_hmc[]

##### About using `np.exp(+1j * phi_wave)` vs `np.exp(-1j * phi_wave)`

- Because when I was comparing the time domain waveforms, hmc vs bilby, I would get the reversed function for the hmc (?), I thought it was because in hmc we compute `h_wave1 = A_H * AmpWave*np.exp(+1j*phi_wave)` when in bilby they do (which is in fact not the case...): `h_wave1 = A_H * AmpWave*np.exp(-1j*phi_wave)`. So I changed the plus into a minus in the hmc code, and I would still get good acceptance rates...
- However it turns out of course that I was mistaken and it's not what they do in bilby, and turning the plus into a minus adds pi to the 'phase' of the imaginary part of the signal.
- So I changed it back into a `h_wave1 = A_H * AmpWave*np.exp(+1j*phi_wave)` to do proper comparisions with bilby. However I was a bit puzzled by the fact I would still get a very good acceptance rate using the version with the minus sign.
- So I ran 20 numerical trajectories in both cases plus/minus sign in the exponential of the phase to compare the acceptance rate at the end of it:
    - `h_wave1 = A_H * AmpWave*np.exp(+1j*phi_wave)`: 100.00, 100.00, 100.00, 75.00, 60.00, 66.67, 71.43, 75.00, 77.78, 80.00, 81.82, 83.33, 84.62, 85.71, 86.67, 87.50, 88.24, 88.89, 89.47, 90.00
    - `h_wave1 = A_H * AmpWave*np.exp(-1j*phi_wave)`: 100.00, 100.00, 100.00, 100.00, 100.00, 100.00, 100.00, 87.50, 77.78, 80.00, 81.82, 83.33, 84.62, 85.71, 86.67, 87.50, 88.24, 88.89, 89.47, 90.00
- The initial logL and snr are also a bit different:
    - `h_wave1 = A_H * AmpWave*np.exp(+1j*phi_wave)`:
    ```sh
    Value of initial log L = 1022.239015
    Value of initial SNR = 44.736279
    ```
    - `h_wave1 = A_H * AmpWave*np.exp(-1j*phi_wave)`:
    ```sh
    Value of initial log L = 994.228775
    Value of initial SNR = 44.422116
    ```
- NOTE HOWEVER that, cf the next cell 'About f_ref', in lal they seem to use the convention with `exp(-1j)` !!

#### About f_fref

- [here](https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/group___l_a_l_sim_inspiral_taylor_x_x__c.html#gab06324f50ad04c1ecd078427d615e312) where the definition is given for TaylorF2
- [here](https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_inspiral_8c_source.html) line 255, is written:
    ```C
    /*
      * certain approximants adopt the convention that f_ref=0 refers to the start
      * of the waveform while other approximants adopt the convention that f_ref=0
      * refers to the end of the waveform. in the former case, this routine will
      * return the explicit value of f_ref, which will be f_min.
      */
    ```
- [here](https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_inspiral_taylor_f2_8c_source.html) is actually where the real code for TF2 is written !! line 461
- line 311 is written the following:
    ```C
    /* Compute the SPA phase at the reference point
    * N.B. f_ref == 0 means we define the reference time/phase at "coalescence"
    * when the frequency approaches infinity. In that case,
    * the integrals Eq. 3.15 of arXiv:0907.0700 vanish when evaluated at
    * f_ref == infinity. If f_ref is finite, we must compute the SPA phase
    * evaluated at f_ref, store it as ref_phasing and subtract it off.
    */
     REAL8 ref_phasing = 0.;
     if( f_ref != 0. ) {
         const REAL8 vref = cbrt(piM*f_ref);
         const REAL8 logvref = log(vref);
         const REAL8 v2ref = vref * vref;
         const REAL8 v3ref = vref * v2ref;
         const REAL8 v4ref = vref * v3ref;
         const REAL8 v5ref = vref * v4ref;
         const REAL8 v6ref = vref * v5ref;
         const REAL8 v7ref = vref * v6ref;
         const REAL8 v8ref = vref * v7ref;
         const REAL8 v9ref = vref * v8ref;
         const REAL8 v10ref = vref * v9ref;
         const REAL8 v12ref = v2ref * v10ref;
         ref_phasing += pfa7 * v7ref;
         ref_phasing += (pfa6 + pfl6 * logvref) * v6ref;
         ref_phasing += (pfa5 + pfl5 * logvref) * v5ref;
         ref_phasing += pfa4 * v4ref;
         ref_phasing += pfa3 * v3ref;
         ref_phasing += pfa2 * v2ref;
         ref_phasing += pfa1 * vref;
         ref_phasing += pfaN;

         /* Tidal terms in reference phasing */
         ref_phasing += pft12 * v12ref;
         ref_phasing += pft10 * v10ref;

         ref_phasing /= v5ref;
     } /* End of if(f_ref != 0) block */

     #pragma omp parallel for
     for (i = 0; i < freqs->length; i++) {
         const REAL8 f = freqs->data[i];
         const REAL8 v = cbrt(piM*f);
         const REAL8 logv = log(v);
         const REAL8 v2 = v * v;
         const REAL8 v3 = v * v2;
         const REAL8 v4 = v * v3;
         const REAL8 v5 = v * v4;
         const REAL8 v6 = v * v5;
         const REAL8 v7 = v * v6;
         const REAL8 v8 = v * v7;
         const REAL8 v9 = v * v8;
         const REAL8 v10 = v * v9;
         const REAL8 v12 = v2 * v10;
         REAL8 phasing = 0.;
         REAL8 dEnergy = 0.;
         REAL8 flux = 0.;
         REAL8 amp;

         phasing += pfa7 * v7;
         phasing += (pfa6 + pfl6 * logv) * v6;
         phasing += (pfa5 + pfl5 * logv) * v5;
         phasing += pfa4 * v4;
         phasing += pfa3 * v3;
         phasing += pfa2 * v2;
         phasing += pfa1 * v;
         phasing += pfaN;
    ```
    The for loop in this code is the equivalent of what the hmc code does with `Taylorf2_phase_3_5PN()` function
- This bit of the code, line 431, seems to be where the 'job' is done:
    ```C
         phasing /= v5;
         flux *= FTaN * v10;
         dEnergy *= dETaN * v;
         // Note the factor of 2 b/c phi_ref is orbital phase
         phasing += shft * f - 2.*phi_ref - ref_phasing;
         amp = amp0 * sqrt(-dEnergy/flux) * v;
         data[i+iStart] = amp * cos(phasing - LAL_PI_4) - amp * sin(phasing - LAL_PI_4) * 1.0j;
    ```
   **So `phi_ref` is the ORBITAL FREQUENCY, when our `phi_c` is the GW FREQUENCY! So I should take into account this factor two when translating `phi_c` into `phi_ref`: `phi_ref = phi_c / 2`**
- `shft` is described to be `/**< time shift to be applied to frequency-domain phase (sec)*/`
- At some point the following is computed:
    ```C
     /* extrinsic parameters */
     shft = LAL_TWOPI * (tC.gpsSeconds + 1e-9 * tC.gpsNanoSeconds);
     ```

### About `tc`

- Continuing from `shft` mentionned above
- [here](https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_inspiral_taylor_f2_8c_source.html) line 513 we can see that they set automatically `tc = -1/deltaF` !
- This is the same as doing: `tc = - Tobs` basically
- However the `deltaF` we input lal with is:
    - bilby.source: `delta_frequency = frequency_array[1] - frequency_array[0]`
    - consistent with the fact that the frequency array comes from the interferometers, which is set with:
        - detector.py
        ```py
        def _calculate_frequency_array(self):
        """ Calculate the frequency array """
        if (self.sampling_frequency is None) or (self.duration is None):
            raise ValueError(
                "You have not specified the sampling_frequency and duration")
        self.frequency_array = utils.create_frequency_series(
            sampling_frequency=self.sampling_frequency, duration=self.duration)
        ```
        - utils.py
        ```py
        def create_frequency_series(sampling_frequency, duration):
            """ Create a frequency series with the correct length and spacing.

            Parameters
            -------
            sampling_frequency: float
            duration: float
                duration of data

            Returns
            -------
            array_like: frequency series

            """
            number_of_samples = duration * sampling_frequency
            number_of_samples = int(np.round(number_of_samples))

            # prepare for FFT
            number_of_frequencies = (number_of_samples - 1) // 2
            delta_freq = 1. / duration

            frequencies = delta_freq * np.linspace(1, number_of_frequencies, number_of_frequencies)

            if len(frequencies) % 2 == 1:
                frequencies = np.concatenate(([0], frequencies, [sampling_frequency / 2.]))
            else:
                # no Nyquist frequency when N=odd
                frequencies = np.concatenate(([0], frequencies))

            return frequencies
        ```
    - Hence I should find a way for `tc_lal` to be equal and vary in the same way as our `tc_hmc` by playing on `delatF` and `duration` and `sampling_frequency` etc
    - Actually the thing to understand is that with lalsim, time shifts in the frequency domain are done post waveform generation using the following code for bilby:
    ```python
        time_shift = self.time_delay_from_geocenter(
        parameters['ra'],
        parameters['dec'],
        self.strain_data.start_time)
        dt = parameters['geocent_time'] + time_shift - self.strain_data.start_time

        signal_ifo = signal_ifo * np.exp(
            -1j * 2 * np.pi * dt * self.frequency_array)
    ```
    Here `dt` can be rewritten using `tc`:
    ```python
        dt = tc + time_shift
    ```

#### About `maximum_frequency`

- If we don't use the default bilby's `maximum_frequency = frequency_array[-1]` passed to `lalsim` but use `maximum_frequency = 0` in `source.py`, then `lalsim` computes the waveform up to the same maximum frequency than what we do in the hmc
- In the hmc code, we compute it with the formula: `f_high = sigpar[14] = vmax**3 / (PI * GEOM * params[11])` where `vmax = np.sqrt(1.0 / Rmin)`
- In `lalsim` the following is done:
    ```C
         /* external: SI; internal: solar masses */
         const REAL8 m1 = m1_SI / LAL_MSUN_SI;
         const REAL8 m2 = m2_SI / LAL_MSUN_SI;
         const REAL8 m = m1 + m2;
         const REAL8 m_sec = m * LAL_MTSUN_SI;  /* total mass in seconds */
         // const REAL8 eta = m1 * m2 / (m * m);
         const REAL8 piM = LAL_PI * m_sec;
         const REAL8 vISCO = 1. / sqrt(6.);
         const REAL8 fISCO = vISCO * vISCO * vISCO / piM;

        /* allocate htilde */
        if ( fEnd == 0. ) // End at ISCO
            f_max = fISCO;
    ```

### About `tc_hmc` and `tc_bilby`
- Running the `compare_waveform.py` script and looking at the `waveform_ratio` graph, it is clear that where things mess up is in the timeshifts applied in the frequency domain for each detector
- Actually the thing to understand is that with lalsim, time shifts in the frequency domain are done post waveform generation using the following code for bilby:
```python
    time_shift = self.time_delay_from_geocenter(
    parameters['ra'],
    parameters['dec'],
    self.strain_data.start_time)
    dt = parameters['geocent_time'] + time_shift - self.strain_data.start_time

    signal_ifo = signal_ifo * np.exp(
        -1j * 2 * np.pi * dt * self.frequency_array)
```
Here `dt` can be rewritten using `tc`:
```python
    dt = tc + time_shift
```
- But what is this `tc` just in the above equation?? The thing is, in my `su.sigpar_to_injection_parameters()` that's how I translate the `tc_hmc = sigpar[8]` into bilby's parameter:
    ```python
    geocent_time=start_time + sigpar[8]
    ```
     - The problem, I think, is that for bilby/lal, `geocent_time` is the time of coalescence but AT THE CENTER OF THE EARTH since afterwards the `time_shift` computed is between the center of the earth and the detector.
     - However `tc_hmc = sigpar[8]` is implicitly used to be the time of coalescence in Handford, since this detector is taken to be the point of reference.
     - So what I should code is the following:
        ```python
            geocent_time= tc_earth_center = start_time + sigpar[8] - delta_H_EarthCenter
        ```
- After implementing this change, I got a constant phase shift for `h_wave_1_bilby` vs `h_wave_1_hmc`. It turned out to be of value `pi/2` so artifically multiplying `h_wave_1_bilby` by `np.exp(-1j*np.pi/2)` did the job and the two waveforms now overlapp almost perfectly !! (overlapp = 0.998). However:
    - Why this constant phase shift of `pi/2` ?
    - I still get frequency dependant phase-shift for the two other interferometers... How is that possible ?
    - The numerical derivatives now agree for 6 of the 9 parameters, but still disagree for `psi, sin(theta), phi`.

##### Friday 7/12/18
- frequency-dependent phase-shift pb
- frequency-independant

##### Saturday 8/12/18
- ?

##### What I remember from the notes I lost
- frequency dependent phase-shift in the two other ifo:
    - Since I'm using the `-1j` convention in `TaylorF2.py`, I also need to change `TaylorF2.py` `c0 = phi_wave + deltaT_TPI_LH*freq` to `c0 = phi_wave - deltaT_TPI_LH*freq` to be consistent with how the time-shifts are applied
    - doing this I get frequency-independant phase-shift in the two other detectors
    - However this phase-shift still depends on Ra, Dec, inc and phi
- frequency independent phase-shift:
    - I realised that what is done in lal/bilby compared to hmc is the following:
    ```python
    # lal
    h_tilde_out = A * exp(-1j*phase) = A * exp(-1j*(2*pi*f*tc - phic - pi/4 + ...))
    h_tilde_plus = 0.5 * (1+cos(iota)**2) * h_tilde_out
    h_tilde_cross = -1j * cos(iota) * h_tilde_out

    # bilby
    h_tilde_ifo = F_plus * h_tilde_plus + F_cross * h_tilde_cross

    # Hence
    h_tilde_ifo = (0.5 * (1+cos(iota)**2)*F_plus -1j * cos(iota)*F_cross) * h_tilde_out
    h_tilde_ifo = sqrt((0.5 * (1+cos(iota)**2)*F_plus)**2  +  (cos(iota)*F_cross)**2 ) * exp(arctan(-cos(iota)*F_cross/(0.5 * (1+cos(iota)**2)*F_plus))) * h_tilde_out

    # When hmc does:
    h_tilde_H = A * exp(-1j*(2*pi*f*tc_H - phic - pi/4 + ...))
    amp = sqrt((0.5 * (1+cos(iota)**2)*F_plus)**2  +  (cos(iota)*F_cross)**2 )
    h_tilde_H *= amp
    ```
    - Hence we see that there is a phase-shift of `arctan(-cos(iota)*F_cross/(0.5 * (1+cos(iota)**2)*F_plus))` between the two methods
    - By adding it at the end of `ifo.get_detector_response()` I have been able to have good overlaps for the three interferometers, for which ever value of ra, dec, inc and phi !!!
- Caveats now...
    - I also had to artificially add `np.pi` to this phase-shift, don't know why...
    - Even though I'm getting good overlaps now, my numerical derivatives are still not corresponding for `theta` and `phi`, and these overlaps change with ra, dec...
    - Hence I get bad scales in phase 0 of hmc
    - Even using Yann's FIM, the acceptance rate with bilby is ~40%

## Next things to try
- DONE: plot `waveform_ratio.amplitude` and `waveform_ratio.phase`
- compare beam pattern functions for all possible values of `ra` and `dec`
- Is my conversion between `ra, dec` -> `theta, phi` done correctly ?
- Generalise plots in `compare_dh_waveforms.py` as done in `compare_waveforms.py`
- Run the compare scripts for Binary2.in
- Call another approximant than `TaylorF2`, like `IMRPhenomPv2`
- Write the script: `compare_different_tc.py` ?

## Meeting with Ed Tuesday 11/12/18
- What I had already noted here...
    - GW170817
    - no more phase_shift in get_detector_response()
    - forward difference at beginning and end of num der
    - numerical derivatives wrt to tc and phic: use the `np.exp(1j*2*np.pi*f*offset)` formula:
    ```python
    dh/dtc = (h(tc+offset)-h(tc-offset))/(2*offset)
           = (h(f_ref)*np.exp(i*2*pi*f*offset)-h(f_ref)*np.exp(i*2*pi*f*-offset))/2*offset
           = i*sin(2*pi*f*offset)*h(f_ref)/offset
    dh/dlntc = tc*dh/dtc
    ```
    ```python
    dh/dphic = (h(phic+offset)-h(phic-offset))/(2*offset)
           = (h(phi_ref)*np.exp(-i*offset)-h(phi_ref)*np.exp(i*offset))/2*offset
           = -i*sin(offset)*h(phi_ref)/offset
    ```
    - irfft() plot: compare time from 40Hz to f_isco to `ComputeChirpTime()` function
- Compare bilby vs hmc in time domain !
    - Change input params in Binary1.in a bit to see how it reacts
    - Understand how fft works with using `-1j` or `+1j` convention etc
    - Why am I getting smooth waveform in `compare_waveform.py` and not in `bilby_snr_logL_GW170817.py` ?
        - because the duration of the signal was not set properly, hence the waveform had to go from fmin to f_isco within an amount of time that was either too short or too long
- Run `bilby_snr_logL_GW170817.py` with psd from gwosc
- Compute numerical derivatives wrt new params, with new formula for tc and phic, and derive the scales
- Compute numerical derivatives using second method by doing a delta on the logLikelihood

#### Thursday 13/12/18
- need to create a script that has as inputs the gwosc data files and outputs a psd array for the frequency bins I want. Then I will be able to set the `interferometer.psd_array` to these
- scripts `lotsofplots.py` generates psd array from the gwosc input files. I should rewrite that so it only takes into account the part of the signal before the GW strain; take same offset as bilby does in `strain_psd.set_from_open_data()`
- bilby has routines to interpolate any psd array to create a new one, cf `strain_psd.create_power_spectral_density()`
-

## During Chritmas holidays
- Just assume that bilby is doing things right, so the idea is to write the hmc but with bilby's inputs.
- Create a new branch called bilbyoriented on which I should change the `sigpar` input parameters by bilby/lal input parameters
  1. change from `sigpar` to `parameters`
  2. change from `template_ifos = ` to `signal_ifos = `
- The derivatives should be taken w.r.t. `geocent_time` and not tc_Handford anymore
- To derive w.r.t. `geocent_time` and `phi_c`, use the 'new' analytic formulae which are already coded in `h_dh_Generation_ThreeDetectors_3()`
- Once this is done, check the values of the derivatives by looking at the FIM, scales ...
- Write code for a ToyHMC
- Reprendre depuis:
  ```bash
  $ pythonw bilby_snr_logL_GW170817.py
  18:53 bilby INFO    : Running bilby version: 0.3.3:

  Optimal logL = 1281.19
  Matched filter SNR = 50.67
  Optimal SNR = 50.71

  PSDs estimated from the following strain files:
  - H-H1_GWOSC_4KHZ_R1-1187006835-4096.hdf5
  - L-L1_GWOSC_4KHZ_R1-1187006835-4096.hdf5
  - V-V1_GWOSC_4KHZ_R1-1187006835-4096.hdf5
  Traceback (most recent call last):
    File "bilby_snr_logL_GW170817.py", line 221, in <module>
      InverseFisherMatrix = np.linalg.inv(FisherMatrix)
    File "/anaconda3/lib/python3.6/site-packages/numpy/linalg/linalg.py", line 513, in inv
      ainv = _umath_linalg.inv(a, signature=signature, extobj=extobj)
    File "/anaconda3/lib/python3.6/site-packages/numpy/linalg/linalg.py", line 90, in _raise_linalgerror_singular
      raise LinAlgError("Singular matrix")
  numpy.linalg.linalg.LinAlgError: Singular matrix
  ```

## Meeting with Ed post christmas monday:
- Modulations at the beginning of the time domain signals:
  - need to window the signal before IFFT it because what the algorithm sees is a signal in a square window which IFFT is a sinc, hence the modulations; so I need to use hanning window to optimize the retreival of information
- check for the whitening of the templates
- deriving wrt Mc and mu: when arriving at the point where the m1=m2
  - Mc: forward distance derivative
  - mu: backward distance derivative
  - see code snippet
  - see Porter and Cornish paper
- have a look at See Nishizawa et al. (2009) arXiv:0903.0528 and compare with Creighton et al

## Thursday 10th January meeting with Ed
- InverseFisherMatrix = `np.linalg.inv(FisherMatrix)`: what method used? Singular values should work: SVD or LUD
  - why does my FIM has zeros almost everywhere? => my new h_dh function is very likely to be wrong
  - invert my FIM using svd or lud and see if the error is still raised
  - with binary1.in: if I import Yann's FIM and invert it using `np.linalg.inv()`, do I get the same inverse ?
  - script bilby_snr_logL_GW170817.py:
    - code up a function: `injection_param_to_sigpar`
    - call `tf2_for_bilby.FisherMatrix_BNSWaveform_ThreeDetectors(sigpar, n_freq, shf_L, shf_V)`
    - to be sure of what Yann's code would have returned as FIM for GW170817, I should create a binaryGW170817.in file
- send values of injection_parameters I'm using
- calculate the FIM with Yann's code and my injection_params
- Integrate the code_snippet to compute dlnMc and dlnmu
  - replace fractions and divisions by decimals and multiplications
- With the new version of h_dh(), does tf2_for_bilby and bilby_wv compare well?
- run phase 1 with 10 trajectories to see the acceptance
  - use q_pos[] or injection_parameters[''] in bht.NumericalLeapFrog()?
  - Note: in `BoundaryCheck_On_Position()`, I commented the final line:
  ```py
  q_pos[14] = (1.0/np.sqrt(q_pos[19])) ** 3 / (PI * GEOM * q_pos[11])
  ```


- Maybe the problem is that before I was, for binary1.in:
  - using Yann's FIM, which is good
  - my h_dh function with num derivatives
  - but not the version of h_dh which suits bilby

#### To tell Ed
- My FIM was singular because of obvious mistake in my computation of the derivatives: I was not using parameters_minus and parameters_plus
- Why the LUD decomposition can't be the only explanation:
  - there are alreday discrepancies in the FIM_hmc vs FIM_bilby, before inverting
  - indeed when I was doing comparisions of scales vs scales0 before christmas, the scales from Yann's code where computed by inverting Yann's FIM with np.linalg.inv()

#### To do next
- `[DONE]`: run phase 1 with tf2_for_bilby's FIM
  - Acc = 66% after 20 trajectories, offset = 1e-6
- How can the scale in phic change if I change the offset when the derivative wrt phic is analytical?
- `[DONE]`: Integrate the code_snippet to compute dlnMc and dlnmu
  - replace fractions and divisions by decimals and multiplications
- run the hmc as a one dimensional problem starting with the distance.
  - actually it might be quite easy to code:
    - if in my function `q_pos_to_dictionary()` I only vary the distance and keep the other param constants, then even though my whole code will compute the derivatives for each of the 9 parameters, the `logL_final` in `NumLeapFrog` will only depend on the new value of the distance, depending itself on the value of the derivative wrt `ln(D)` and the trajectory should be accepted.
    - Then in `ComputeHamiltonian()` I should only sum over the momentum for distance
  - However the scale should just be the inverse of the FIM distance element
- `[DONE]`: give Ed access to my gitlab
- run the C code with `GW170817.in`
  - check that the FIM created is the same that my script `bilby_GW170817.py` creates
  - it shouldn't be exactly the same since the psds used are different, but orders of magnitude should compare well
- Ed's mail:
  - check what's the best offset using num derivatives wrt distance and phic
  - check what Yann was using: forward diff for amplitude param vs central diff for sky angles
  ```c
  dFplus_Psi_H = (dFp - F_plus_H)*1.0e+6;
  dFcross_Psi_H = (dFc - F_cross_H)*1.0e+6;
  LVC_Detector_Beam_Pattern_Functions(&dFp, &dFc, sigpar[7], sigpar[6]+1.0e-6, sigpar[2], sigpar[18], 1);
  dFplus_Theta_H = (dFp - F_plus_H)*1.0e+6;
  dFplus_Theta_H = dFplus_Theta_H*(1.0/cos(sigpar[6]));     // change to have derivative with respect to sin(theta)
  dFcross_Theta_H = (dFc - F_cross_H)*1.0e+6;
  dFcross_Theta_H = dFcross_Theta_H*(1.0/cos(sigpar[6]));   // change to have derivative with respect to sin(theta)
  LVC_Detector_Beam_Pattern_Functions(&dFp, &dFc, sigpar[7]+1.0e-6, sigpar[6], sigpar[2], sigpar[18], 1);
  dFplus_Phi_H = (dFp - F_plus_H)*1.0e+6;
  dFcross_Phi_H = (dFc - F_cross_H)*1.0e+6;
  ```

- `[DONE]` LDC connection page on virgo group wiki
- papers to read

#### To tell Ed
- I had a small but MAJOR mistake in my code...: I was drawing my Metroplolis Hastings' acceptance parameter `a` (where you then check if `a < np.exp(-(H-H_0))`) from a normal distribution centered on 0 instead of a uniform distribution between 0 and 1... Hence `a` would be negative half of the time which means that 50% of my trajectories would be accepted on average... I don't know how I could miss that...
- "Sub-dimentional" HMC
  - coded in such a way that I only have to set which parameter indices I want to run the hmc on (ie `0` for `cos(iota)`, `3` for `ln(D)`, `[0,3]` for both of them etc) and then the algorithm will vary these parameters but keep the other ones constant
    - Fisher Matrix is computed accordingly only one this parameter subset. Hence I can invert it to get the scales
    - I only compute the derivatives for parameters which I vary
  - I started as we said with a 1D-hmc over the `ln(D)` param => Acceptance_Rate = 100% after 10 trajectories.
  - I continued with bigger subset of parameters, and here are the acceptance rates (still only after 10 trajectories):
  ```
    - {phic, ln(D), ln(Mc), ln(mu), ln(tc)}                   => Acc = 100%
    - {phic, ln(D), ln(Mc), ln(mu), ln(tc), cos(iota)}        => Acc = 90%
    - {phic, ln(D), ln(Mc), ln(mu), ln(tc), psi}              => Acc = 90%
    - {phic, ln(D), ln(Mc), ln(mu), ln(tc), cos(iota), psi}   => Acc = 50%
    - {cos(iota)}                                             => Acc = 90%
    - {psi}                                                   => Acc = 100%
    - {sin(dec)}                                              => Acc = 0%
    - {ra}                                                    => Acc = 0%
  ```
  - Conclusions
    - I will run over more than 10 trajectories to be sure but I think we can be confident that the following parameters: `{phic, ln(D), ln(Mc), ln(mu), ln(tc)}` and their derivatives are fine
    - I'm not sure why adding both `cos(iota)` and `psi`, hence using: `{phic, ln(D), ln(Mc), ln(mu), ln(tc), cos(iota), psi}`, leads to 50% only, whereas adding separately `cos(iota)` or `psi` led to 90%. I would hence have expected an acceptance rate of ~80%. Again I will run on more than 10 trajectories to be more confident.
    - There is definitely a problem with `sin(dec)` and `ra`. However now it should be easier for me to investigate what's going wrong since I can keep all the other parameters constant.




  - Make my run reproducible by writing the generated noise in a file
    - REJECTED traj on cos(iota): do an import IPython at that traj number and step number to understand why the dlogL changes so much
  - import IPython; IPython.embed() and trajectories of ra and sin(dec)

## Meeting with Ed Monday 21/01
- keep offset = 1e-6 as default
- dh_cosi: forward or backward derivatives at the boundaries when cosi
- run 100 numerical on lntc
  - 99% acceptance, was 100% with analytical
- sin(dec) and ra:
  - plot values of each parameter and that of the Hamiltonian along the trajectory: momenta's contribution and logL contribution. For one accepted and one rejected
- ask Bilby:
  - "Keeping the same interferometer settings (sampling_frequency etc) and compute a set of waveforms + project them onto the detector without having to compute the waveform to frequencies higher than fisco"
  - when lal calls the TF2 waveform, it's expecting an fEnd and an f_ref
  - How can I call lalsim setting fEnd with 0 ?
  - [XLALSimInspiralChooseFDWaveform]( https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/group___l_a_l_sim_inspiral__c.html#ga50d4b23c4b6a80e93d4ed5ea7d90113b)
- Compute dlogL doing the (dlogL+ - dlogL-)/(2*offset)
  - for ln(D) and phic keep analytical formula

## Meeting with Ed Thursday 24/01
- dh_cosi: forward or backward derivatives at the boundaries when cosi:
  - Done, but does change the acceptance rate which is stil 90%
- run 100 numerical on lntc
  - 99% acceptance, was 100% with analytical
- Monitoring tool for any trajectory of a run !
  - Now have reproducible runs monitoring H, dlogL, q, logL along each step of the trajectories
  - cos(iota): problems occurs when cos(iota) switches from -1 to 1
  - ra and sin(dec): dlogL diverges ponctually along the trajectories, going up to 1e5
  - is it normal that for ACCEPTED trajectories the parameters varies so little
- Compute dlogL doing the (dlogL+ - dlogL-)/(2*offset): Ongoing
- reinstall bilby from source and create merge request with modifications on `lal_binary_neutron_star()`
- plot the trajectories in phase space y=p, x=q
- send Ed the link to bilby psds
- pick one of the picks in sin(dec) and ra; look at the waveforms at those two points and plot the waveforms at +delta and that at -delta, plot also the derivatives, check values of logL and the
- send Ed the parameter values at those points


- My scale wrt phic is exactly the same as that wrt to lnD when running hmc separately on those parameters (=4.690461e-02); is that normal ?
- run 100 trajectories on sindec and ra with different offsets

- logL=921.65 with bilby_pip to logL=920.44 with bilby_source
  - because use of `geocent_time` and not `start_time` in `bilby_source.gw.detector.get_detector_response()` for the time_shift between center of the earth and detector.


- What is the numerical instability wrt the offset exactly ?
- Make sense of the numbers plotted in `plot_traj.py` and those I get when stopping the code with IPython. What it the index i plotted?
- On my plots from `plot_traj.py` note the value of the scale used for parameter plotted and the method used for computing dlogL: 1 or 2.
- Keep the scale constant for comparison sake

- Compute one trajectory with l=500 steps for let's say offset=1e-5. Record the values of sin(dec) for that offset. Compute for each of these 500 values of sin(dec) the values of dlogL_dsindec for offsets from 1e-2 to 1e-7
- Stop the trajectory just before one instability in dlogL_dsindec, then compute for the 3 points (before the instable point, the instable point, after the instable point) the waveforms computed at: sin(dec), sin(dec)-offset, sin(dec)+offset. Plot the real (and then imag parts) of those 3 waveforms on the same plot. On another plot, plot the real (then imag) part of the diff = h(sin(dec)+offset) - h(sin(dec)-offset). On these plots, add a text giving the following numbers: sin(dec), logL, logL+offset, logL-offset, offset
- plot logL as a function of sin(dec)
- see if I shouldn't force the numerical precision in my arrays: `dtype=np.cfloat`

- Bilby: create merge request for f_End

Monday after 60 ans Maman:
- rerun `pythonw instability_offsets_comparison.py`
- make one plot per offset, zooming in each time 10 times closer to the injected value
- increase sampling_frequency
- check the numerical precision of the values hard coded in bilby to compute the time delays for each ifo
  - what if the culprit is `gmst = gps_time_to_gmst(time)`? it is recomputed on the fly at each call, hence theta and phi computed from it afterwards change a little bit each time and hence so does the time delay => NO because time here is fixed and equal to the input `geocent_time`, hence it does not change
  - make a script to compare yann's `LVC_Detector_DeltaGMST()` wrt `time_delay_geocentric(L1.vertex, H1.vertex, ra, dec, time)` varying `dec`, and the numerical derivative of these functions when varying `dec` with an offset from 1e-3 to 1e-7
- check numerical precision
- plot logL for each ifo independently
- plot 2*pi*f*delay(dec)[mod 2*pi] with f=150Hz on same plot as logL(dec) to see if jumps

- run the algorithm keeping ra and sin(dec) fixed and check that:
  - phase 1 goes well after 1000 trajectories
  - the chain converges to the posterior expected for GW170817
  - would I need to change phase 2 and 3 so that it can deal with 7 and not 9 parameters ?
- Questions for Ed:
  - if phase 1 ~ 2h for 10^3 trajectories, why not running 10^5 trajectories => ~200h = ~10days...
  - number of points on a D-dimensional grid scales exponentionnally with the number of dimensions. Is it the same for the number of points needed with MCMC techniques?

Meeting with Ed Friday 22/02
- tf2_for_bilby:
  - why is the FIM component for lnD_phic = 1e-9 => different from 0 ?
  - instead of comparing waveforms bilby vs tf2_for_bilby, I should compare the power spectra: sqrt(h_r**2 + h_i**2)
- Check that in lal: amp0 = -4 * m1 * m2 / ... is equal to our `A0` in the hmc code. The latter is equal normally to the amplitude in equation (6) of Ed and Yann's paper without the factor `Q` and `f**-7/6`
- what is the impact of changing the offset in my derivatives on their associated scales ?
- The `arctan(cos(i)*Fx/...)` can be integrated as an overall phase-shift in the waveform (ie in phic). Indeed, inclination, sky angles and polarisation angle don't change over the duration of the wave. And changing phic does not have any impact on the snr.


Size of the scales:
- impact of:
  - changing the offset
  - reducing the number of parameters
  - numerical vs analytical for phic and lnD

- write an equivalent of `logL_and_dlogL_vs_sindec.py` for each parameter
  - remove from` GW170817_logL_snr.py` the addition of h_wave to strain data.
  - make --offset=1e-3 usable
  - GW170817_set_psds: add the new_noise_realisation option for each psd choice

- Why are the structures of `logL(psi)` so different between hmc and bilby when running `logL_and_dlogL_vs_param.py` on psi (with offset = 1e-2 to have a broad range on psi) ?

- todo:
  - Eric:
    - check numerical precision
    - plot logL for each ifo independently
    - plot 2*pi*f*delay(dec)[mod 2*pi] with f=150Hz on same plot as logL(dec) to see if jumps
  - run analysis ifo per ifo, comparing hmc and bilby to see if one of them is screwing things up
  - run chains parameter per parameter, with GWTC-1 PSD, and check the Acc and the sampling results
  - run one  bilby's injection example on bns up to sampler with GW170817's input and GWTC-1 PSD and check if the posteriors are consistent
  - look at bilby's `ifo.plot_time_domain_data()` to check if I'm doing the right things when plotting my timedomain data

- At some point, either the waveform that I generate with bilby are the right ones or they are not.

### Results to tell Ed:
- Comparing the scales:
  - influence of varying the offset: going from offset_sindec = 1e-6 to 1e-3 has a major impact on the scales for phic, psi and sindec
  - when varying only one parameter (hence scale = 1/sqrt(FIM)): for all parameters there is a factor ~ 1.5 except for psi where = 0.002 and ra = 0.5 (with offset_ra = 1e-2)
- run `logL_and_dlogL_vs_param.py` with `--hmc` and without, successively on each parameter with an offset small enough to have a broad range on the parameter and hence view the logL as a whole: do logL_hmc(param) and logL_bilby(param) at least have the same shape ?
  - `cos(iota)`: YES but not exactly on the whole range: (-1, 1)
  - `phic`: YES
  - `psi`: NO: look at the whole (0, 2*pi) range
  - `ln(D)`: YES
  - `ln(Mc)`: NO: run on the whole range for bilby vs offset=1e-5 for hmc
  - `ln(mu)`: YES: but not on the same scale: run on whole range to see
  - `sin(dec)`: MORE OR LESS: see with offset=1e-2 or on whole range
  - `ra`: MORE OR LESS
  - `ln(tc)`: NO: see with step=1e-1 for hmc and step=1e-5 for bilby


- `tf2.h_dh_Generation_ThreeDetectors()`: add full code for lnMc and lnmu as done for bilby h_dh_Generation_ThreeDetectors.
- Modify code such that I can choose on which ifo to run the analysis on
- q[14] update
-

1. Correct in `BoundaryCheck_On_Position()`: recompute the upper-frequency q_pos[14], make `bilby_wv.Waveform_Three_Detector()` such that it recomputes the upper frequency each time before calling lalsim
2. Plot hmc's first trajectory to see if accepted
3. `tf2.h_dh_Generation_ThreeDetectors()`: add full code for lnMc and lnmu as done for bilby h_dh_Generation_ThreeDetectors.
4. Plot hmc's first trajectory to see if accepted
5. Make plots asked by Ed:
  - For each of the 9 parameters, plot
6. Prepare plots for bilby people at the LVC showing the steppiness of logL(sindec) and logL(ra)
7. Make merge request


Eric Thranes' input:
- artificially increase the time resolution by padding with zeros in the frequency domain for frequencies > ??
- set the noise to infinity for these frequencies, and signal to zero.

Eric Thranes' 2nd input:
- plot logL as a function of timeshift but keeping ra and dec constant...! This in order to see if the timescale with which logL changes on that plot is comparable to the timescale set by the sampling frequency
- Send Eric my code which plot the steppy logL(ra)


- plot with no noise: s = h
- keep all param constant except for `ln(D)`. Run a one dimensional leapfrog in `ln(D)`. 100 to 500 trajectories. Produce a ascii file

- float vs double precision in python: what is the default, which is best, why dtype=np.complex or np.cfloat ?
- from 1e-3 to 1e-5: gather informations on:
  - time_shift
  - ra and dec
- try using C-precision in python

- Add parameter per parameter, adding sky position at last

Next Meeting with Ed:
- My runs with:
  - [0]=`cos(iota)`: looks like the switch from -1 to 1 makes the acceptance rate fall down to 75% because only trajectories where this happens are rejected...
  - [1]=phic: looks smooth and good
  - [2]=psi: looks smooth and good
  - [4],[5],[3,4,5]=`ln(Mc)` and `ln(mu)`: weird steppy behaviour at epsilon=1e-7 but doesn't seem to affect acceptance rate...
  - [8]=`ln(tc)`: good acceptance rate but when plotting the trajectories they are steppy even though `dlogL` does not bump to 1e5 like it did with sky angles
    - do I still get this behavior if using `offset_lntc=0` ? => YES
  - [0,1,2,3]: cf plots: weird oscillating behavior of the trajectories, for each parameter, acceptance rate is quite low = 65% after 100 trajectories.
  - [1,2,3]: better acceptance rate but oscillatory behavior is here
  - [1,3]: no oscillatory behavior
  - [1,2]: oscillatory behavior !



#### Refactorization before chile
- `[DONE]` make dynamical injection parameters from a file
- `[DONE]` remove `import bilby` from `python_utils`
- `[DONE]` remove the `GW170817` name in files:
  - `[DONE]` `GW170817` shouldn't appear anymore
  - `[DONE]` these modules shouldn't be in a directory called `Tests` if I keep importing them in the main script => create a directory called: `intermediate scripts` ?
- `[DONE]` remove dependency to hmc waveform generation
  - `[DONE]` no more opts.hmc => no more opts.generate_from_bilby
- `[DONE]` simplify the call to bilby's waveform generation
- `[DONE]` remove dependencies of `import sigpar_utils as su`
  - `[DONE]` remove the `sigpar version of the functions` in `bilby_waveform.py`
- `[DONE]` remove dependency to Yann's function which are replaced by bilby's
- remove the `template_ifos =` structure
  - `[DONE]` For the moment simply using an np.array of shape = (len(ifos), len(h)) which will be returned by `bilby_wv.WaveForm_ThreeDetectors()`
  - `[DONE]` remove `signal_ifos` as input where `interferometers` is also an input
  - Check that I can run the code on only one or two detectors
    - why is `FIM.py` behaving weirdly with only one ifo ?
- remove `start_time`, `minimum_frequency`, `duration` and `sampling_frequency` as proper inputs of functions and just keep the `parameters` dictionary
  - should `duration` actually be just retreived from `duration = interferometers.duration` ?
- have the `verbose` option really control the prints.
- create a new and proper directory structure
  - reproduce bilby's structure ?
- do a global `autoflake` at the end


#### Meeting with Ed 14/05
- inclination to fix
  - bounce on boundary
- 17 to 14:
  - forward (or central differencing) for their derivatives of sindec, ra and psi
  - use analytical for phi_c
  - TODO:
    - run 100 trajectories for ra, dec and psi alone with forward differencing to check if that causes a drop in the acceptance rate: NO DROP !
- why is .get_detector_response() taking so long ?
- FIM with one detector ?
  - use --ifos= and not ifos=
  - use LUD instead of SVD decomposition => scipy
- run 1000 traj with all params and all params but cosi
- run hmc with other approximants than just TF2, like SpinTaylorF2 or TaylorF2_Tidal
- phase 3
- for some bns (like GW170817), inclination is unimodal => no need for OLUT => find a way to check at the end of phase 1 how modes there are in cos(iota)
- be able to from the beginning of the algorithm to set the number of independant samples (like 20000) => computing the autocorrelations in phase 3.

- plot histograms from the 500 trajectories

- How long does a gradient generation now takes ? Here is a global picture of how computation time is spent:
	- Phase 1: 1000 x 1trajectory
	- 1 trajectory: 200 x dlogL
	- dlogL:
		- h_dh:
			- phi_c, ln(D):			             1 x (h_source + 3 x (ifo.get_detector_response_geocent() + ifo.get_fd_time_translation()))
			- cosi, lnMc, lnmu:              3 x 2 x (h_source + 3 x ifo.get_detector_response_geocent())
			- psi, sin(dec), ra, ln(tc):     4 x 3 x (ifo.get_detector_response_geocent() + ifo.get_fd_time_translation())
		- s_dh - h_dh: 				             9 x 3 x 2 x noise_weighted_inner_product()
	With the following timings:
		- h_source ~ 9ms
    - ifo.get_detector_response_geocent() ~ 1ms
		- ifo.get_fd_time_translation() ~ 4ms
		- noise_weighted_inner_product() ~ 3ms
	This leads to:
		dlogL ~ (1 + 4x2) x (9ms + 3 x 5.5ms) + 3 x 3 x 5.5ms + 9 x 3 x 2 x 3ms
		dlogL ~ 279ms + 162ms
		dlogL ~ 441ms
	And when I time the function alone it gives me ~ 480ms (when we met on Tuesday it was 670ms). Hence rounding up to 500ms, 1 trajectory takes ~ 100sec and 1000 trajectories in phase 1 would lead to 27.8 hours. Since I would do the run over night I decided to “only” run 500 trajectories instead of 1000


#### Meeting with Ed 28/05
- show corner plots and walkers of the 500 trajectories
- `fd_time_translation` NOT recomputed for `cos(iota)`, `lnMc`, `lnmu`
- FIM with `H1` only
- Using other approximant like `IMRPhenomPv2`, using ROQ basis
- Putting code on CIT?
- Using CUPY_TF2 on CIT?
- comité de thèse

- TODO
  - for the noise_weighted_inner_product, fed in with i_low and i_high and compare timings
  - use inverse of power_spectral_density in nwip
    - Actually in bilby's code, whenever the `ifo.power_spectral_density_array` is called, it re-interpolates the original psd given...!! That operation alone takes 2.4ms out of the 3ms I measured. The operation of inverting the psd array 'only' takes 0.076ms.
      - Correcting this, one dlogL now takes 287ms instead of 460ms !
    - Now one nwip only takes 0.8ms
    - Also, out of this nwip, just calling `ifo.frequency_domain_strain` takes 0.244ms (or 0.500ms?) because they multiply it with ` * self.frequency_mask` at each call... changing this would result in a gain of ~10ms per dlogL computation => ~2.5sec per trajectory => ~30min for phase1 with 1000 trajectories
      - After correcting this using a fixed `.fd_strain` attribute, dlogL now takes 278ms instead of 287ms, ie phase1 with 1000 trajectories = 15.4h
    - Reducing to only one nwip in dlogL, it now takes 254ms, ie phase1 with 1000 trajectories = 14.1h
  - time again nwip using `noise_weighted_inner_product_new()` with `.inverse_power_spectral_density`
  - run 1000 trajectories over night and implement the `skipPhase1`
  - get phase 2 and 3 up and working
    - @QUESTION: in hybrid trajectories, why not computing `dlogL['phic']` as well since I can directly have it from the waveform as for `lnD` ?
    - create a branch where I run phase3 with just 1000 analytical traj to record what the Acc rate is
      - ~5%  only after 200 analytical trajectories
      - ~20% only after 58 hybrid trajectories
    - @QUESTION: why are `Nptfit_global` and `Nptfit_olut` calculated from `max_extra_traj_global` and `max_extra_traj_olut` and not the other way around ?
    - @QUESTION: why running the new QR fits every 1e5 n_traj  and not depending on the number of new points available for the fit since the last one ?
    - why do I get a break in cos(inc) for one of the trajectories ?
    - Plot analytical gradient vs numerical for a single trajectory of 1000 steps
  - rename files:
    - `fit_coeff_global.dat` => `coeff_cubic_fit_phase1.dat`
    - `dlogL_olut_fit/good.dat` => `dlogL_Fit_phase1.dat`
    - `pt_Fit_/good.dat` => `pt_Fit_phase1.dat`
  - install new version of bilby
  - use `ifo.template` and `ifo.gradient` ?
  - use approximant `TaylorF2Tidal` and incorporate the tidal parameters
  - then we'll do a run comparision between 500 and 1000 num traj to compare phase 3's acceptance rate


- Optimizations on the noise-weighted-inner-product:
  - When we left each other, 1 nwip ~ 3ms
  - Actually in bilby's code, whenever the `ifo.power_spectral_density_array` is called, it re-interpolates the original psd given...!! That operation alone takes 2.4ms out of the 3ms I measured. The operation of inverting the psd array 'only' takes 0.076ms.
  - Correcting this, one computation of the 9 gradients: `dlogL` now takes 287ms instead of 460ms !
  - Also, out of these 287ms, just calling `ifo.frequency_domain_strain` takes 0.244ms (or 0.500ms?) because bilby would multiply it with a mask ` * self.frequency_mask` at each call... Changing this would result in a gain of ~10ms per dlogL computation => ~2.5sec per trajectory => ~30min for phase1 with 1000 trajectories.
  - Correcting this `dlogL` now takes 278ms instead of 287ms, ie phase1 with 1000 trajectories = 15.4h
  - Then I reduced to only one nwip in `dlogL`: instead of computing `<s|dh> - <h|dh>`, I directly compute `<s-h|dh>` which is faster. It now takes 254ms, ie phase1 with 1000 trajectories should take ~ 14.1h
- Running phase1 with 1000 trajectories
  - Hence I reran phase1 with 1000 trajectories instead of the 500 run I had done before. As foreseen it took 13.9h. The acceptance rate was 81.9%.
  - Here is the corner plot after those 1000 numerical trajectories
- Phase 2 and 3
  - I retested phase 2 and finished coding up phase 3
  - The acceptance rate was really low for analytical and hybrid trajectories:
    - ~5%  only after 200 analytical trajectories
    - ~20% only after 58 hybrid trajectories
  - Hence I compared analytical vs numerical gradient for a single trajectory of 1000 steps; here is the comparision plot:
- To do next:
  - Continue phase 1 to go up to 1500 trajectories and check how the cubic fit behaves:
    - redo the corner plots at the end of phase1
    - redo the num vs ana comparision plot
    - do the same for 500 phase1 trajectories
  - If behaves well, then do a long run of phase 3 ?
    - 50 thousands, track the behavior of Acc, if things looks okay then we'll implement the version where we give as input the number of statistically independant points
  - Do a 1000 iterated loop, where I compute np.exp(1jx) vs np.cos(x) + 1jnp.sin(x), half angle formula, transcendental
  - install new version of bilby
  - use approximant `TaylorF2Tidal` and incorporate the tidal parameters

#### Meeting with Ed Friday 7th of June
  - plot the values of the parameters over the 1000 step trajectory in phase3, numerical values on top of analytical ones
  - How do you decide on how many number of independant samples points are needed for the posterior? => look for paper from Walter Del Pozzo
  - I could implement retaining points for the fit in phase1 even on trajectories which in the end are rejected by keeping track of the hamiltonian along the trajectory
  - Then instead of setting the number of initial trajectories in phase1, we could set the number of fit points we want to have, ie ~250 000
  - The reason why the criterion we use in phase1 to keep points for the fit is for the trajectory to be accepted and not just how far off H_final is from H_0 is because we want to keep points which are on the peak of the likelihood and not those which are on other modes (otherwise the cubic fit wouldn't work). And those trajectories which manage to go to another sub-peak are those for which the momentum are big which -according to Ed- leads to higher numerical instability

  - create a function which computes the boundaries for Mc and mu out of those from m1 and m2; use it in `BoundaryCheck_On_Position_ParametrisedSpace()` to avoid overflows in `exp()`
  - go up to 2000 trajectories in phase1


#### Mike hack session
1. get my code compliant with bilby 0.5.1
2. put it on the CIT
3. make my code compliant with cupy using talbot's gpus waveform tf2 generation

tf2 source frame on gpu = 2ms
logL on gpu = 4ms

#### Shawarma with Ed
- Book room for thursday 10 to 13
- is my QR/phase2 faster than Yann's really? With 247400 points from phase1: see blass
  - 40 sec for my python code
  - 300 sec = 5 min for Yann's C code
- do I need more precisions when printing the coefficients to the file? Am I reading my fit_coeff from the files all the time? If yes try running phase2 and phase3 right after each other to be sure that precision is not a problem here
  - I did and got exactly the same bad analytical approximation of my numerical gradients...
- why are my analytical trajectories blowing off when the analytical gradients are always smaller than my numerical ones ?
- why are the cubic fitted gradient all correlated/anticorrelated with each other? (cf plots with 300 steps)
  - seems like there is a problem when phic jumps from 0 to 2pi: I would say that maybe not enough points have been accumulated on one or the other boundary during phase1
- Show Ed the discepancy between `BoundaryCheck_On_Position_ParametrisedSpace()` and `BoundaryCheck_On_Position()`
- Try Mike's implementation of the fit
- Make 2D plots: cos(i)-lnD, Mc-mu, phic-psi, ra-dec, Mc-tc

- understanding spikes on dlogL/dlnmu:
  - along numerical trajectory at phase3 compute gradient with both methods and plot the logL itself
  - plot, as in january, logL vs mu around point where there is a suspicious peak in the trajectory
    - in bilby.gw.waveform_generator: `self.parameters = parameters` calls in a hidden way a whole lot of conversion function from `.gw.conversion` which modify my input values of `mass_1` and `mass_2` to a level of ~4e-15, which is only numerical precision a priori
      - After commenting those lines of codes in bilby responsible for this little discrepancy, I made sure that exactly the same values of `mass_1` are inputed to `lalsim` and I still go the steppy behavior
    - investigating where the steppy behavior comes from, we can see it when plotting `source_frame_polarizations['plus'].real.sum()` or `source_frame_polarizations['cross'].imag.sum()` but not the other two...

  - finish implementing the solution:
  ```
  last_idx_non_zero_minus = np.where(template_ifos_minus[0]!=0)[0][-1]
  last_idx_non_zero_plus = np.where(template_ifos_plus[0]!=0)[0][-1]

  if last_idx_non_zero_minus < last_idx_non_zero_plus:
      template_ifos_plus[:, last_idx_non_zero_plus] = np.zeros(len(interferometers))
  elif last_idx_non_zero_minus > last_idx_non_zero_plus:
      template_ifos_minus[:, last_idx_non_zero_minus] = np.zeros(len(interferometers))
  ```
  - Re-run phase1 for 1500 traj
  - Why did the traj wrt dlnMc change in the mail I sent to Ed ?
  - look at some of my rejected trajectories in phase1
- OLUT: look at sklearn.neighbors.KNeighborsRegressor
- adapt my fitting routines to less than 9 dimensions and check how it works on some subset of the coeff

- `[DONE]` injection Binary1.in
- `[DONE]` Fit with QR C code the new 270000 points
  - same result
- Do the OLUTs with yann's code as well
- Isn't trajectory #4 where np.exp(-(H-H_0))=2.596 a reason why the cubic fit wouldn't work? There is a boundary reflexion on cos(iota)=-1 which creates an artificial decrease in the Hamiltonian
  - Why don't we actually keep points from rejected trajectories? Because it somehow means that the gradient was badly calculated at some point since this rejected traj deviated from the true path.
- Compare as Mike did the cubic fit wrt to real values that were used for the fit in phase1

- `[DONE]` code 'properly' the new bounce on cosinc boundaries; run phase1+2+NumVsAna with this
- `[DONE]` should I code the same for the bounce on lnMc and lnmu ?
- implement reflection on the boundary for sin(theta) as well ?
- `[DONE]` implement proper bounces on boundaries in `BoundaryCheck_On_Position_ParametrisedSpace()` as well
- `[DONE]` finish changign `p_mom_meth2` with new proper bounce
- what is the real impact of `RemappingAngles_from_q_pos()`
- `[DONE]` code `bounce_on_masses()`, equivalent of `bounce_on_masses_fit()`
- `[DONE]` Run phase1 for ~80 traj and plot those trajectories having `abs(proba_Acc - 1) > 0.35` to see what is happening
- `[DONE]` remove the duplicate entries in pt_fit and dlogL_fit happening every 200 points.
- why have I lost ~40ms per dlogL computation just by coding the proper bounces on boundaries ?
- why not keeping only for fit points those trajectories where the oscillations of the Hamiltonian during the traj around its mean value is smaller than some number ?
  - Had to change the structure of my arrays in order to append often
  - This makes the code faster !
  - `[DONE]` Finish coding for phase3 the switch between numpy arrays and classic arrays
  - Now that I do this, I am missing some points when setting --debug to plot trajectories afterwards...
- `[DONE]` run traj #1 with different stepsize to see the impact on the oscillation of H
- Why has now the first np.exp(-(H-H_0)) changed for GW170817 ?
  - I have change the length of each trajectory, from 199 to 200... !

- Do the run with analytical PSDs
- Do run with higher SNR sources
- Do the OLUTs with yann's code as well
- OLUT: look at sklearn.neighbors.KNeighborsRegressor
- Do the fit (cubic and local) using Mike's method
- Compare as Mike did the cubic fit wrt to real values that were used for the fit in phase1
- adapt my fitting routines to less than 9 dimensions and check how it works on some subset of the coeff
- Run phase1+2+NumVsAna adding parameter by parameter to check which one is causing the problem

- Use IMRPhenomPv2 with ROQ: how fast? Does it improve phase2 fitting?
- adapt my fitting routines to less than 9 dimensions and check how it works on some subset of the coeff
- @QUESTION: Isn't there a problem that when plotting the time-domain waveforms with IMRPhenomD, I see a beautiful chirp starting at 30Hz up until merger and ringdown lasting 56.9 sec and when plotting with TaylorF2 it stops before Merger but lasts 56.9 seconds also ??
  - for TaylorF2, shouldn't there be a `duration_of_signal = tc_3p5PN + 2` and a `duration_of_segment = time to go from f_low to f_max` ?
- in bilby_waveform plot, add vertical lines for t(fISCO), t(fmax) ??
  - Isn't `ComputeChirpTime3p5PN(f_low, m1, m2)` supposed to compute the time it takes from flow to merger, hence at a frequency higher than fISCO?
  - Because for GW170817: `Compute_fISCO(Rmin, m1, m2) = 1604.82 Hz`, and `ComputeChirpTime3p5PN(f_low, m1, m2) = 56.91538269948342 s` with `f_low = 30 Hz`. I would expect `ComputeChirpTime3p5PN(f, m1, m2)` to equate zero for a frequency = frequency at merger time which hence should be higher than fISCO. However I get it to be equal to zero for `f = 1346 Hz < 1604.82 Hz = fISCO`
  - Now when I plot the time domain waveform with IMRPhenomPv2, I get the merger time at `~ 56.931 sec > 56.915 sec`
- @QUESTION: isn't there a problem with the discrepancy in the amplitude of the time domain waveform (around merger time at least) when comparing that of TaylorF2 (~1e-25) and that of IMRPhenomPv2 (~1e-26) ?
- Why is it that with `duration = 64` my waveforms and frequency_array have the same length but if `duration = 58, or 60 etc` then it bugs? How does LAL make use of `delta_frequency` ?
- Use ROQ !
  - run the roq_example.py
  - look at how works `ROQGravitationalWaveTransient` and how it makes the link between the roq basis and `bilby.gw.source.binary_black_hole_roq`
  - how to deal with the `linear` and `quadratic` keys of `waveform_polarizations`? => look at `bilby.gw.likelihood.ROQGravitationalWaveTransient.calculate_snrs` #2
- Run bilby's open_data_example on GW170817 to see how long it takes with dynesty
- Create and run roq example (with dynesty) but on GW170817 and bns_model: how long does it take? How do the corner plot look like?
- Do some runs with `minimum_frequency = 40Hz` to see how that speeds things up and what I loose on the acceptance rate

- using ROQ:
  - to know which segment I am supposed to use, apparently I should compute tchirp with a minimum frequency of 20Hz
  - In ROQ paper, apparently the minimum Mchirp is 1.4 when it is equal to 1.18 for GW170817
  - should I implement a method to compute `dlogL` which only takes as input `(logL, parameters)` where `logL` is a likelihood method?
  - with the GW150914 ROQ example I got:

  ```
  In [5]: %timeit likelihood.log_likelihood_ratio()
  1.29 ms ± 8.67 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)
  ```
  - be careful that in ... they use `interferometer.power_spectral_density_array` and `interferometer.frequency_domain_strain` which as I saw might reinterpolate each time the psd and multiplies by the mask for freq_array
  - code properly what the scale factor should be
  - code properly the limits for the injection_params to be able to be treated with ROQ: valid chirp_mass ? valid duration ? valid mass_ratio < 9 ?
    - check that the logL computed with IMRPhenomPv2 and IMRPhenomPv2_ROQ_scaled are the same, with an injection using IMRPhenomPv2 with non rescaled chirp mass but rescaled minimum, maximum and sampling freq (?)
  - with the scaling: can I compute dlogL for GW170817 with IMRPhenomPv2_ROQ? if yes does it compare with that of TaylorF2? what is btw the comparision of dlogL between TaylorF2 and IMRPhenomPv2?
    - `ValueError: Waveform longer than frequency array`: duration is = 213.33s after rescaling of the roq which seems to be the reason for that bug; but why doesn't it bug in bilby's roq_example when they rescaled the duration from 4s to 4s / 1.6 ?
- Adapt OLUTs so that it works with less than 9 dimension and then do it on the --search_parameter_indices=03 run
- Timing comparision IMRPhenomPv2 with and without ROQ
  - for a freq array of 524289 elements -going from flow=34Hz until fhigh=6963.2Hz- dlogL took 2.63s without ROQ versus 33ms with ROQ => speed up factor of 80 ! (Values of logL and dlogL of both methods being very close (logL=674 case))

- Understanding the different logL values and weights calculated by ROQ:
  - bottom line: lalsim seems to generate different waveforms depending on the maximum frequency it's inputed with: values of h_plus are different between 34Hz and 2048Hz if maximum_freq is 2048Hz or 6963Hz.
  - The weights built from the ROQ basis depend on the fd_strain injected
  - The sampling frequency for the ROQ basis to be built needs to be 2*roq_params['fhigh'] but the maximum_freq of the interferometers can be less, like 2048Hz



- TaylorF2:
  - Using:
  ```
  f_ISCO = Compute_LAL_fISCO(m1 * LAL_MSUN_SI, m2 * LAL_MSUN_SI)
  f_ISCO_run = Compute_LAL_fISCO(m1_min * LAL_MSUN_SI, m2_min * LAL_MSUN_SI)
  ```
  instead of my `Compute_fISCO()` actually changes a bit the SNR and logL value
  - => Try running phase1 with this configuration and see how the fitting works

AVANT LE DEPART EN DROME: AVOIR UN RUN PHASE 1 AVEC IMRPHENOMPV2 ROQ sur GW170817 !
  - ROQ run completely diverging...
    - try to run with IMRPhenomPv2 but without ROQ
    - try to run on a simpler case than GW170817, like GW150914 where the ROQ basis won't be rescaled etc...
    - Why aren't my TaylorF2 runs working as well as before? That might be a clue why ROQ is not working as well...
    - `[DONE]` change the self.offset for likelihood_gradient
    - Am I going outside of the ROQ boundaries? Like chirp-mass etc ?
    - In plot_traj.py, plot individual 0.5*p**2 to spot which parameter(s) is making the kinetic energy blow off
      - it's definitely `ln(mu)`
        - I noticed that in my dlogL_roq computation for `ln(mu)` I was using:
        `paru.parameters_new_dlnMc(self.likelihood.parameters_copy, self.offset)`
        instead of:
        `paru.parameters_new_dlnmu(self.likelihood.parameters_copy, self.offset)`
        - I had inverted the superior condition `parameters_plus['symmetric_mass_ratio'] > 0.25:` to inferior...
      - After correcting these silly mistakes, my first ROQ trajectories get accepted. When plotting the Hamiltonian along the traj, it has a weird oscillatory behavior which seems highly correlated with the kinetic energies of phi_c and psi
        - Can I run phase 1 with ROQ on a subset of parameters?
      - in `calculate_log_likelihood_gradient()` I had forgotten to copy back the original values of the parameters:
      `self.likelihood.parameters = self.likelihood.parameters_copy.copy()`
    - Analyse `python plot_trajectory.py ../__output_data/GW170817/bilbyoriented/SUB-D_012345678/PSD_1/0_100_2000_200_200_0.005_dlogL1/ROQ/ --traj_nb=25` at step ~175 there is a clear bump in the Hamiltonian clearly due to a bump in dlogL/dsindec
    - Analyse `python plot_trajectory.py ../__output_data/GW170817/bilbyoriented/SUB-D_012345678/PSD_1/0_15_2000_200_200_0.005_dlogL1/ROQ_scales_div_by_two/ --traj_nb=9`: there is a peak in dlogL/dlnmu
  - Dividing the scales of phi_c and psi by two makes the Hamiltonian less oscillatory and improves the acceptance rate


IMPROVING CODE STRUCTURE:
- Let the approximant be an option `--approximant=`, and also part of the config.ini
- Create a sub-directory based on the approximant name
- Include the ROQ boundaries in my boundaries
- Use the `likelihood_gradient` class even for TaylorF2 so that then I can easily switch to an object oriented structure?


PEAKS IN DLOGL WRT DEC AND RA USING ROQ:
- `d_inner_h` in `likelihood.calculate_snrs()` is the steppy sub-product
- the reason why is that it is calculated using:
  ```
  d_inner_h = interp1d(
      likelihood.weights['time_samples'][indices],
      d_inner_h_tc_array, kind='cubic', assume_sorted=True)(ifo_time)
  ```
- A step appears sometimes because varying `dec` or `ra` changes the `indices` on which the interpolation takes place, eg going from `indices = [1016, 1017, 1018, 1019, 1020]` to `indices = [1017, 1018, 1019, 1020, 1021]`
- This would cause a variation in the logL value of ~0.025 when the variation should be ~1e-5
- `indices` are computed the following way in `def _closest_time_indices(time, samples):`
  ```
  closest = np.argmin(abs(samples - time))
  indices = [closest + ii for ii in [-2, -1, 0, 1, 2]]
  in_bounds = (indices[0] >= 0) & (indices[-1] < samples.size)
  return indices, in_bounds
  ```
- To smooth things (keeping an offset=1e-7) out I had to go from only 5 closest times to 17:
`indices = [closest + ii for ii in [-8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8]]`
- With an offset of 1e-6 I can use only the


- fit_coeff precision and the constant offset of dlogL_ana along dlogL_num
 - my fit_coeff being very big numbers (~1e17), I thought there would be a precision issue and hence decided to use np.longdouble, ie float128 at some point.
 - In the `def QR_FittingMethod_Gradients(pt_fit, dlogL_fit)` replacing:
 ```
 fit_coeff_global = pseudo_inverse @ dlogL_fit
 ```
 with
 ```
 fit_coeff_global = np.matmul(pseudo_inverse, dlogL_fit, dtype=np.longdouble)
 ```
 improved the offset I had but did not wipe it out completely
 - Another reason might be that I need more precision when printing pt_fit.dat and dlogL_fit.dat, maybe using `fmt = '%.25e'` instead of `fmt = '%.18e'`
  => didn't change anything
 - try replacing the ` @ ` operator in:
 ```
 pseudo_inverse = vh.T * s_inv @ u.T
 fit_coeff = pseudo_inverse @ dlogL_Y
 Grad = fit_coeff @ np.append(q_pos_fit,1)
 ```
 with `np.matmul(., ., dtype=np.longdouble)`


- The new 1500 phase 1 trajectories didn't improve phase 2...
  - what if I try to fit the coeff with a subset of the 293000 points ?
  - there is still smth to check with the numerical precision of the fit_coeff and np.longdouble
  - I should still check that the analytical gradient evaluated on one of the fit points gives me the logL_fit back
  - try on GW150914 because on GW170817 the ROQ seems to be railing on the priors
  - from the results of the bash scripts, it seems like maybe when `phi` is introduced, it screws up the fit of phase 2, both local and cubic.
  - What happens if I run phase1 with 1500 num traj without varying `phi` ?
    - `../__output_data/GW170817/SUB-D_0#2345678/PSD_1/1501_1500_2000_200_200_0.005_dlogL1/IMRPhenomPv2_ROQ/`
    - The fit starts being crappy when there is a peak in dlogL/dlnD at step ~620 no?
    - check some of the rejected trajectories in this run, like:
    ```
    ../Library/param_utils.py:116: RuntimeWarning: invalid value encountered in sqrt
    mass_1 = total_mass * (0.5 + np.sqrt(0.25 - symmetric_mass_ratio))
    21 -    rejected    - Acc = 95.2% - 0.08779 < 0.00000 - ??
    466 -    rejected    - Acc = 99.4% - 0.98598 < 0.00000 - ??
    ```
      - The reason for these rejection was because `self.log_likelihood_at_point` was computed in `def __init__()` instead of being recomputed at every point...


  - make new run with 501_500 traj only, how does the analytical dlogL look like? It seems like it will be better fitted than after 1500 num traj, if yes I should understand why
  - also, it looks like my run 1500 run is bouncing against boundaries in `ln(mu)` (and other parameters?) when I look at the corner plots
  - try replacing the local_fit with kneighbors routine
  - make the run with the scales not halved...?
  - make runs without phic AND without psi...?

  - check the warnings which appeared in SUB0345678:
  ```
  ../Library/BNS_HMC_Tools.py:94: RuntimeWarning: invalid value encountered in true_divide
  d2_other_param_1 = (Matrix_ClosestPoint[other_param_index_1] - q_pos_fit[other_param_index_1])**2/SquaredScale[other_param_index_1]
../Library/BNS_HMC_Tools.py:95: RuntimeWarning: invalid value encountered in true_divide
  d2_other_param_2 = (Matrix_ClosestPoint[other_param_index_2] - q_pos_fit[other_param_index_2])**2/SquaredScale[other_param_index_2]
  ```
    - they were only due to the fact that when `psi` is kept fixed, its scale is set to zero
  - check out those peaks in the gradients when plotting the analytical gradient along the num one
    - they are due to a bounce of cos_inc boundary
    - why are dlogL/dcosinc and dlogL/dlnD not only very similar in shape but also in value? (ie almost identical)
  - When comparing the corner plot after phase1 of the run with all the parameters, where the fits are crappy, with that of the run without phic where the fit are "pretty good", I feel like the problem is that phic and psi are completely bimodal. Hence when the two parameters are included in the analysis, two modes appear in each combination of phic or psi with another parameter. Hence it seems natural that the cubic fit can't work. And moreover the local fit should then be done with all parameters presenting a bimodality, so it also makes sense that the OLUTs fit don't match that well
  - QUESTION: if there is a bimodality within just one pair of parameters (as expected for lnD-cosinc), how can the cubic fit work for any of the parameters since it includes values explored on both mode for this pair of parameters ?


  - do a breakpoint() in TableGradientFit in a case where the local fit works to watch how pt_Y and dlogL_Y look like
  - check in both cases that fit_coeff * pt_used_for_the_fit gives back the right answer => if no it means the coeff are wrongly computed; compute the R**2 of the regression
  - try LUD decompostion instead of SVD
  - compute the distance wrt to all the other parameters
  - why not directly selecting the 2000 points based on distance wrt to all parameters and not firstly wrt to the distance with the parameter we want to compute dlogL with?
  - did yann actually use the distances values to make the fit? => No
  - check that my TableGradientFit routines and Yann's give the same output when given in the same input => YES !
  - How can the 20 closest points have gradients between -10 and -30 and the fit gives me a fucking Grad=-70...!
  - try Yann's TableGradientFit in the case where it works with 0345678
  - Create a routine to make a corner plot of the 2000 and 200 points used for the fit
    - from these, it seems that when it's not working it's because for one of the parameters -lntc here- the 2000 and then 200 selected points' values for that parameter are far from the q_pos_fit[param_index] value... Humm really ?? Not sure anymore...
    - Try selecting the 10 000 closest points to cosinc and then
    - What if I first select the 2000 closest neighbors based on their distance wrt to all the params values, and then sub-select the 200 points around cosinc ?
    - I should try to do what supposedly KNN does, ie weight the influence of each point on the fit based on its distance to my point
    - Shouldn't I add some sort of correlation (FIM values ?) when computin the distance to account for the fact that some parameter have more influence than others when deriving the gradient wrt to one specific param, hence their distance matters more
    - `[DONE]` when using the OLUT, is it legid to compute the distance wrt to the parameter we are computing dlogL with ? => It's actually useless to compute the distance in the dimension the OLUT is sorted with since they will be very small compared to the distances in the other dimensions
  - method "true_nearest_neighbors" is too slow because I compute the distance wrt to ~ 300000 pts; which is probably why Ed and Yann went onto OLUTs
  - KNN is probably a lot more efficient than my own code
  - increase the order of the fit? 2nd order instead of linear?
  - At the end of phase1, why not updating the scales derived initially from the FIM with the distribution of points from phase1 ?
  - gather more points from phase1
  - `[DONE]` try the linear fit keeping only points for which dlogL_Y is close to -15. If the fit works then try to understand how to only retain those points using some clever distance / metric etc... => not working
  - make KNN work in the case where my linear fit works, ie SUB0345678
  - `[DONE]` why not trying KNN to replace the global cubic fit ?
  - why is KNN not working when I rescale the basis I ask it to fit ?
  - if the OLUTs need a lot more point from phase1 than the cubic fit does, phase3 could be started by only running Hybrid trajectories for some time
  - `[DONE]` make all the plots with different n1 and n2 but with the 600 000 phase1 points
  - `[DONE]` run with phic up to 3000 phase1 traj: actually not necessary since I saw on the run without phic that pushing to 3000 num does not improve the fits
  - `[DONE]` why not doing some brutal machine learning on phase1 ? Look at the jupyter notebooks from Asterics ! It may not be KNN that I should use
  - `[DONE]` If it really is the bimodality in phic which is screwing the cubic fit, shouldn't I try to make the fit without considering phic ? => Not working
  - shouldn't I try to derive some sort of correlation values between each gradient wrt to each parameter? at point of injection ?
  - `[DONE]` introduce bounces on phic and psi instead of remapping into good range. Run phase1 with 1500 traj => didn't change either phase1 or phase3...
  - since there is a bimodality in phic, did I make a mistake somewhere between the convertion of phic = 2 * phase ?
  - `[DONE]` checkout out again that for IMRPhenomPv2, `phase` in the orbital phase and not the gravitational wave phase
  - `[DONE]` Make new run on GW150914 up to 1500 num traj to confirm as well the bimodality in phic.
  - In phase1, when setting --debug, maybe store the 10 first traj of the run and then only those trajectories where there is a `??` tag
  - what was Ed's paper describing something to be careful with regarding phic and a plot that looks like my phic-psi histogram ?
  - `[DONE]` make a run SUB01#345678 to see how it works. If it does work, then try any 8 parameter run, like SUB01234567#, to confirm that it's when phic and psi are together that things screw up. Actually maybe more SUB0123#5678, ie without Chirpmass since it's the parameter we measure the best. => The cubic fit is failing for SUB0123#5678 which means that indeed, apparently, it's when phic and psi are run together that things screw up...!
  - For the cubic fit, what about selecting only every 5 points of accepted trajectory?
  - If Yann got a phase3 Acc ~ 70%, maybe the gradient approximations that I am getting are actually sufficient to get this order of acceptance rate as well...!
  - make a run on Binary1.ini to compare my phase1 with Yann's
  - `[DONE]` I could sample `phase` instead of `phic`
  - Implement supervised_reg in the phase2 and in phase3 trajectory: how does the 1000 steps traj responds? If it works as well as QR and OLUTs in the 02345678 case and as bad in the 012345678 case, then it proves that my phase2 fitting problem does not come from my phase2 code but from the phase1 data...
  - In order to get some preliminary results, maybe I could start adding new parameters but keeping phi_c or psi constant..??!
  - why the supervised fit is not working on single trajectory:
    - try the plot but on a phase1 trajectory => working perfectly!
    - train manually on a subset of phase1 and fit the first or last phase1 traj => not working at all...
    - this means that the model is overfitting the data which is in contradiction with the results I get with the script `supervised_regression.py`
    - Why don't I get a regression on the traj which is as good as what I get when I compute it over a whole test set which is part of the phase1 data ???
    - => It's because when splitting the phase1 data into a train and a test set which are randomly picked using `model_selection.train_test_split()`, then data from the test set are points of almost complete trajectories from the train set, hence the KN algorithm is able to find the points from the trajectory it comes from and approximate the gradient very well. However if the points of the test set are that of a new trajectory then there does not exists in the train set points close enough for the approximation to be satisfactory.
      - It would be interesting to know how points from phase1 are necessary to get a satisfactory approximation of the gradients with sklearn.
    - Actually Random Forests gives interesting results on the single traj plots compared to KNN.
      - surprisingly, the fit is the best for dlogL/dcosinc and dlogL/dlnD
    - I should try other 'method' algorithm than KNR and RF to check if there isn't one working better than RF.
    - If there needs a lot of points from phase1 to have good fitting results with sklearn, I could consider parallelizing phase1 since it's interest wouldn't be into starting a chain but into gathering points for the fit.
  - Do I get the same phase1 corner plot, and phic bimodality, when running 0#2345678 with TaylorF2 ?
  - How about a run SUB#12###### ?

### END OF SUMMER STATUS FOR ED:
  - Introduced IMRPhenomPv2_ROQ
    - Phase1 Acc ~ 99%
    - Phase1 duration with 1500 traj ~ 3h
    - Phase2 not working better
  - Phase2 fitting problems
    - In the full 9 parameter case
      - Cubic fit not working
      - Local OLUTs fit not working for `psi`, working relatively fine for `cos(inc)` and `lnD`
    - Removing either `phic` or `psi` from the estimation
      - both local and cubic fit work as expected
      - hence it seems that, for some reason, it's when both of them are being estimated that things screw up
      - when looking at the 9 parameters corner plots from phase1, `phic` has a bimodality which I think is unexpected
      - `phic` and `psi` are very much positively correlated, smth which I haven't seen in other corner plots results -did I?-
    - How come even the local OLUTs fit isn't working 'perfectly' in the psi case?
      - Used Yann's C code routine with my input data and it gives the same result => my python code for OLUTs isn't the culprit
      - I tried many different `(n1, n2)` configurations
      - I tried local 2nd and 3rd order fit but it didn't improve
      - In the end, after plotting the data, I think the trajectory I was focussing on was particularly outside of the phase1 gathered data points
    - Using machine learning on phase1 with the `sklearn` package
      - I thought it would be interesting to compare our fitting routines with those algorithms, plus it might shed light on why my phase2 isn't working
      - Different `Regressors` are available, like `KNeighborsRegressor`, `RandomForestRegressor`, `GradientBoostingRegressor`
      - `RandomForestRegressor` seems to be the best at fitting the gradients
      - However it's far from being as good as cubic fit
      - strangely the best fits it gives are for `cos(inc)` and `lnD`, which would be interesting for us since it's only using a global fit contrary to us using a local fit for those parameters

- run phase3 in the SUB02345678 case and check what the acceptance rate is !
  - AccP3 ~10%...
  - However almost all my hybrid trajectories are accepted.
  - Looking at traj 3001 and the plot of anal vs num traj, I feel like it's only `psi` which is screwing things up
  - to confirm this, run with a hybrid2 version of the traj where I can choose which gradient is to be computed numerically, which is to be computed analytically either with cubic or OLUTs
- run phase3 in the SUB01345678 case
  - run phase1 up to 1500 num traj first
- run phase3 on SUB0345678
- Create a function that sorts out the OLUTs and call it in BNS_HMC_9D.py
- marginalize over `phic`
