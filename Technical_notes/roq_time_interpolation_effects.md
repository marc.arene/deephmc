## ROQ TIME INTERPOLATION EFFECTS ON DLOGL
- Using ROQ with IMRPhenomPv2, I noticed **peaks in the dlogL with respect to RA and DEC**
- `d_inner_h` in `likelihood.calculate_snrs()` is the steppy sub-product
- the reason why is that it is calculated using:
  ```py
  d_inner_h = interp1d(
      likelihood.weights['time_samples'][indices],
      d_inner_h_tc_array, kind='cubic', assume_sorted=True)(ifo_time)
  ```
- A step appears sometimes because varying `dec` or `ra` changes the `indices` on which the interpolation takes place, eg going from `indices = [1016, 1017, 1018, 1019, 1020]` to `indices = [1017, 1018, 1019, 1020, 1021]`
- This would cause a variation in the logL value of ~0.025 when the variation should be ~1e-5
- `indices` are computed the following way in `def _closest_time_indices(time, samples):`
  ```py
  closest = np.argmin(abs(samples - time))
  indices = [closest + ii for ii in [-2, -1, 0, 1, 2]]
  in_bounds = (indices[0] >= 0) & (indices[-1] < samples.size)
  return indices, in_bounds
  ```
- To smooth things (keeping an offset=1e-7) out I had to go from only 5 closest times to 17:
`indices = [closest + ii for ii in [-8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8]]`
- With an offset of 1e-6 I can use only the ??
