# Comparing the scales derived from the FIM with the covariance matrix of points gathered at the end of phase1


- high spin prior case, injected spins values were the following:
```py
a_1=0.1
a_2=0.1
tilt_1=1.05
tilt_2=1.05
phi_12=1.7
phi_jl=0
```
```
                 cos_theta_jn         psi     ln(D)    ln(Mc)    ln(mu)  sin(dec)        ra    ln(tc)       a_1        a_2     tilt_1      tilt_2       phi_12    phi_jl
Purely from FIM      1.288124  154.697614  1.292055  0.000031  0.003688  0.005492  0.004751  0.000002  0.889571  76.906795  10.035784  445.421432  7847.663946  4.481528
Used for phase1      1.000000    1.570796  0.500000  0.000031  0.003688  0.005492  0.004751  0.000002  0.044500   0.044500   0.157080    0.157080     3.141593  3.141593
From 68% phase1      0.266769    1.996073  0.265272  0.000112  0.013141  0.009390  0.008763  0.000004  0.201534   0.523007   2.135951    1.522271     4.200172  3.748371
From cov matrix      0.126885    0.886011  0.129622  0.000057  0.007033  0.005035  0.004453  0.000002  0.109352   0.238861   0.892168    0.743156     1.803512  1.664683
```

- low spin prior case with `chi_1_inj = chi_2_inj = 0.02` using IMRPhenomD; scales were:
```
                   cos_theta_jn        psi     ln(D)    ln(Mc)    ln(mu)  sin(dec)        ra    ln(tc)     chi_1     chi_2
Purely from FIM      1.399171  11.852809  1.403088  0.000348  0.082559  0.005649  0.004946  0.000016  6.182205  7.104861
```
## Older analysis
- Number of points: 1500

|parameter| scale | sqrt(cov.diag) |
|:--------|--------:|--------:|
|cos_theta_jn|5.000e-01|1.321e-01|
|phic|1.571e+00|1.608e+00|
|psi|7.854e-01|8.106e-01|
|ln(D)|2.500e-01|1.648e-01|
|ln(Mc)|1.797e-05|1.795e-05|
|ln(mu)|5.323e-04|5.623e-04|
|sin(dec)|5.655e-03|5.222e-03|
|ra|4.756e-03|4.608e-03|
|ln(tc)|1.973e-06|2.032e-06|

- The corresponding corner plot is the following (here for instance is plotted `theta_jn` but the scale derived is that of `cos_theta_jn`)
![1500_samples_corner](./1500_samples_corner.png)
