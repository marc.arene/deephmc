import numpy as np
import h5py
import sys
sys.path.append('/Users/marcarene/projects/python') # Necessary to import gwhmc
sys.path.append('/Users/marcarene/projects/python/gwhmc') # Necessary because of the `import Library` in gwhmc modules ...
import gwhmc.Library.plots as hmcplt # necessary for the next import not to bug...
import gwhmc.Library.CONST as CONST
import gwhmc.Library.python_utils as pu
import gwhmc.Library.param_utils as paru
import gwhmc.core.autocorrelation as cautocorr
import gwhmc.gw.prior as hmcprior
from bilby.core.result import plot_multiple
import pandas as pd
import bilby.gw.conversion as conv
import bilby.core.result as bilbyres




def lal_samples_from_hdf5(file_path, lal_keys_to_extract=None):
    file = h5py.File(file_path, 'r')
    dset = file['lalinference/lalinference_mcmc/posterior_samples']

    if lal_keys_to_extract is None:
        lal_keys_to_extract = ['costheta_jn', 'polarisation', 'logdistance', 'chirpmass', 'q', 'declination', 'rightascension', 'time', 'a_spin1', 'a_spin2', 'lambda1', 'lambda2'] # if using the file before cbcBayesMCMC2pos
        # lal_keys_to_extract = ['costheta_jn', 'polarisation', 'logdistance', 'chirpmass', 'q', 'declination', 'rightascension', 'time', 'a_spin1', 'a_spin2', 'lambda1', 'lambda2', 'matched_filter_snr'] # if using the file before cbcBayesMCMC2pos
        # lal_keys_to_extract = ['theta_jn', 'psi', 'dist', 'mc', 'q', 'dec', 'ra', 'time', 'a1', 'a2'] # if using the file after cbcBayesMCMC2pos
        # lal_keys_to_extract = dset_values.dtype.names
    dset_values = dset[()]
    # import IPython; IPython.embed(); sys.exit()
    samples = dset_values[lal_keys_to_extract]
    samples = np.asarray(samples.tolist())

    samples[:,0] = np.arccos(samples[:,0])
    samples[:,2] = np.exp(samples[:,2])

    return samples

def add_m1m2mu_columns_to_posterior_in_Mc_q(posterior):
        """
        Returns a posterior with a 'mass_1' and a 'mass_2' column computed
        from the 'chirp_mass' and 'reduced_mass' existing.
        Insertion is at index right after the two existing ones.

        Parameters:
            posterior: pd.DataFrame
                must contain at least a 'chirp_mass' and a 'reduced_mass' column
        """
        posterior_new = posterior.copy()
        mass_1, mass_2 = paru.Mc_and_q_to_m1_and_m2(posterior_new['chirp_mass'], posterior_new['mass_ratio'])
        reduced_mass = mass_1 * mass_2 / (mass_1 + mass_2)
        symmetric_mass_ratio = mass_1 * mass_2 / (mass_1 + mass_2)**2
        idx_mc = posterior_new.keys().tolist().index('chirp_mass')
        idx_mass_ratio = posterior_new.keys().tolist().index('mass_ratio')
        posterior_new.insert(loc=idx_mass_ratio + 1, column='mass_2', value=mass_2)
        posterior_new.insert(loc=idx_mass_ratio + 1, column='mass_1', value=mass_1)
        posterior_new.insert(loc=idx_mass_ratio + 1, column='symmetric_mass_ratio', value=symmetric_mass_ratio)
        posterior_new.insert(loc=idx_mc + 1, column='reduced_mass', value=reduced_mass)
        return posterior_new

def add_m1m2q_columns_to_posterior_in_Mc_mu(posterior):
        """
        Returns a posterior with a 'mass_1' and a 'mass_2' column computed
        from the 'chirp_mass' and 'reduced_mass' existing.
        Insertion is at index right after the two existing ones.

        Parameters:
            posterior: pd.DataFrame
                must contain at least a 'chirp_mass' and a 'reduced_mass' column
        """
        posterior_new = posterior.copy()
        _, _, mass_1, mass_2 = paru.Mc_and_mu_to_M_eta_m1_and_m2(posterior_new['chirp_mass'], posterior_new['reduced_mass'])
        mass_ratio = mass_2 / mass_1
        symmetric_mass_ratio = mass_1 * mass_2 / (mass_1 + mass_2)**2
        idx_mc = posterior_new.keys().tolist().index('chirp_mass')
        idx_mu = posterior_new.keys().tolist().index('reduced_mass')
        posterior_new.insert(loc=idx_mu + 1, column='mass_2', value=mass_2)
        posterior_new.insert(loc=idx_mu + 1, column='mass_1', value=mass_1)
        posterior_new.insert(loc=idx_mu + 1, column='symmetric_mass_ratio', value=symmetric_mass_ratio)
        posterior_new.insert(loc=idx_mu + 1, column='mass_ratio', value=mass_ratio)
        return posterior_new

def plots_superimposed(res_02499, res_02500, **kwargs):
    plot_parameters_all = res_02500.posterior.columns.tolist()
    plot_parameters_all_compmass_only = plot_parameters_all.copy()
    plot_parameters_all_compmass_only.remove('chirp_mass')
    plot_parameters_all_compmass_only.remove('reduced_mass')
    plot_parameters_all_compmass_only.remove('mass_ratio')

    plot_parameters_0 =  ['theta_jn', 'psi', 'luminosity_distance']
    plot_parameters_1 =  ['mass_1', 'mass_2']
    plot_parameters_2 =  ['chirp_mass', 'geocent_time']
    plot_parameters_3 =  ['ra', 'dec']
    # plot_parameters_spins =  ['chi_1', 'chi_2']
    # plot_parameters_masses_spins =  ['mass_1', 'mass_2', 'chi_1', 'chi_2']
    plot_parameters_4 =  ['chi_1', 'chi_2']
    plot_parameters_5 =  ['lambda_1', 'lambda_2']
    plot_parameters_6 =  ['chirp_mass', 'reduced_mass', 'mass_ratio', 'symmetric_mass_ratio']
    # plot_parameters_6 =  ['chirp_mass', 'reduced_mass']
    # plot_parameters_combinations = [plot_parameters_spins_mu]
    plot_parameters_combinations = [plot_parameters_0, plot_parameters_1, plot_parameters_2, plot_parameters_3, plot_parameters_4, plot_parameters_5, plot_parameters_6]

    defaults_kwargs = dict(
        bins=50, smooth=0.9, label_kwargs=dict(fontsize=16),
        title_kwargs=dict(fontsize=16),
        truth_color='tab:orange',
        # quantiles=[0.05, 0.95],
        # quantiles=[0.16, 0.84],
        # levels=(0.6827, 0.9545, 0.9973),
        levels=(1 - np.exp(-0.5), 1 - np.exp(-2), 1 - np.exp(-9 / 2.)),
        plot_density=False, plot_datapoints=True, fill_contours=True,
        max_n_ticks=3)
    defaults_kwargs.update(kwargs)
    kwargs = defaults_kwargs.copy()

    # filename_base = f'../__output_corner_superimposed/30Hz/corner_02500{len(res_02500.posterior)}_vs_LI_'
    filename_base = f'./corner_02500{len(res_02500.posterior)}_vs_02499{len(res_02499.posterior)}_'
    for plot_parameters in plot_parameters_combinations:
        parameter_labels = CONST.get_latex_labels(plot_parameters)
        # kwargs = {}
        kwargs['corner_labels'] = parameter_labels
        filename = filename_base + '-'.join(plot_parameters) + '.png'
        # filename = filename_base + '-'.join(plot_parameters) + '.pdf'
        print(f'plotting corner {filename}')
        # hist2d_kwargs = dict(levels=[0.5, 0.90], bins=20)
        plot_multiple([res_02500, res_02499], filename=filename, parameters=plot_parameters, colours=['C0', 'C1'], **kwargs)


if __name__=='__main__':
    # posterior_02499_path = '/Users/marcarene/projects/lalinference_runs/GW170817/mcmc/30Hz/outdir/samples.hdf5'
    # hmc_run_path = '/Users/marcarene/projects/python/gwhmc/__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/200000_1300_2000_200_200_0.005/phase_marginalization/IMRPhenomD_NRTidal'
    # hmc_run_path = '/Users/marcarene/projects/python/gwhmc/__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/300000_3000_2000_200_200_0.005/phase_marginalization/IMRPhenomD_NRTidal'
    # samples_02500_path = hmc_run_path + '/sampler_state/samples.dat'
    # hmc_ext_analysis_dict_path = hmc_run_path + '/ext_analysis_dict.json'

    data_dir02499 = '../../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/_when_m2max0.96m1/300000_3000_2000_200_200_0.005_dnn_e60_b128/phase_marginalization/IMRPhenomD_NRTidal'
    samples_02499_path = data_dir02499 + '/sampler_state/samples.dat'

    data_dir02500 = '../../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/300000_3000_2000_200_200_0.005/phase_marginalization/IMRPhenomD_NRTidal'
    samples_02500_path = data_dir02500 + '/sampler_state/samples.dat'

    hmc_ext_analysis_dict_path = data_dir02500 + '/ext_analysis_dict.json'

    start_time = pu.json_to_dict(hmc_ext_analysis_dict_path)['start_time']

    # columns_lal = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'mass_ratio', 'dec', 'ra', 'geocent_time', 'chi_1', 'chi_2', 'lambda_1', 'lambda_2']
    columns = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'dec', 'ra', 'geocent_time', 'chi_1', 'chi_2', 'lambda_1', 'lambda_2']


    samples_02499 = np.loadtxt(samples_02499_path)
    samples_02500 = np.loadtxt(samples_02500_path)

    max_acl_02499 = cautocorr.get_maximum_integrated_time(samples_02499)
    max_acl_02500 = cautocorr.get_maximum_integrated_time(samples_02500)
    print(f'eta_max 0.2499: {len(samples_02499)} samples, max_acl = {max_acl_02499}')
    print(f'eta_max 0.2500: {len(samples_02500)} samples, max_acl = {max_acl_02500}')

    # samples_02499_sis = samples_02499[::max_acl_02499]
    # print(f'lal: {len(samples_02499_sis)} SIS')
    thin_len = int(len(samples_02500) / 5000)
    samples_02500_sis = samples_02500[::thin_len]
    print(f'hmc: Thinned the {len(samples_02500)} correlated samples by {thin_len} to retrieve exactly {len(samples_02500_sis)} SIS.')

    thin_len = int(len(samples_02499) / 5000)
    samples_02499_sis = samples_02499[::thin_len]
    print(f'hmc: Thinned the {len(samples_02499)} correlated samples by {thin_len} to retrieve exactly {len(samples_02499_sis)} SIS.')

    # samples_02500_sis = samples_02500[::max_acl_02500]
    samples_02499_sis[:,7] += start_time + 6.341934204101562e-05
    samples_02500_sis[:,7] += start_time + 6.341934204101562e-05

    posterior_02499_sis = pd.DataFrame(samples_02499_sis, columns=columns)
    posterior_02500_sis = pd.DataFrame(samples_02500_sis, columns=columns)
    posterior_02499_sis = add_m1m2q_columns_to_posterior_in_Mc_mu(posterior_02499_sis)
    posterior_02500_sis = add_m1m2q_columns_to_posterior_in_Mc_mu(posterior_02500_sis)


    res_02499 = bilbyres.Result(
        label=r'$\eta_{max} = 0.2499$',
        search_parameter_keys=posterior_02499_sis.keys().tolist(),
        # parameter_labels=parameter_labels,
        priors=None,
        posterior=posterior_02499_sis,
        meta_data={}
        )

    res_02500 = bilbyres.Result(
        label=r'$\eta_{max} = 0.25$',
        search_parameter_keys=posterior_02500_sis.keys().tolist(),
        # parameter_labels=parameter_labels,
        priors=None,
        # posterior=posterior_02500_sis,
        posterior=posterior_02500_sis,
        # posterior=posterior_02500_sis.sample(len(samples_02499_sis)),
        meta_data={}
        )

    kwargs = dict(quantiles = [0.05, 0.95])
    print('quantiles: {}'.format(kwargs['quantiles']))

    if True:
    # if False:
        plots_superimposed(res_02499, res_02500, **kwargs)

    if True:
        import gwhmc.Library.plots as hmcplt # necessary for the next import not to bug...
        import gwhmc.Library.CONST as CONST
        keys_for_table = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'mass_1', 'mass_2', 'dec', 'ra', 'geocent_time', 'chi_1', 'chi_2', 'lambda_1', 'lambda_2']

        for key in keys_for_table:
            key_latex = CONST.LATEX_LABELS[key]
            res_sum_02500 = res_02500.get_one_dimensional_median_and_error_bar(key, **kwargs).string
            res_sum_lal = res_02499.get_one_dimensional_median_and_error_bar(key, **kwargs).string
            print(f'{key_latex} & {res_sum_02500} & {res_sum_lal} \\\\[3pt]')


    # import IPython; IPython.embed()
