# GW170817 HMC - consequences of capping eta to 0.2499

![corner_025005001_vs_024995001_theta_jn-psi-luminosity_distance.png](corner_025005001_vs_024995001_theta_jn-psi-luminosity_distance.png)

![corner_025005001_vs_024995001_ra-dec.png](corner_025005001_vs_024995001_ra-dec.png)

![corner_025005001_vs_024995001_chirp_mass-geocent_time.png](corner_025005001_vs_024995001_chirp_mass-geocent_time.png)

![corner_025005001_vs_024995001_mass_1-mass_2.png](corner_025005001_vs_024995001_mass_1-mass_2.png)

![corner_025005001_vs_024995001_chirp_mass-reduced_mass-mass_ratio-symmetric_mass_ratio.png](corner_025005001_vs_024995001_chirp_mass-reduced_mass-mass_ratio-symmetric_mass_ratio.png)

![corner_025005001_vs_024995001_lambda_1-lambda_2.png](corner_025005001_vs_024995001_lambda_1-lambda_2.png)

![corner_025005001_vs_024995001_chi_1-chi_2.png](corner_025005001_vs_024995001_chi_1-chi_2.png)
