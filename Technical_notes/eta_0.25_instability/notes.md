# Instability on the equal mass line
- If a trajectory starts very close to eta = 0.25, even numerical trajectories get rejected, even if the stepsize is reduced
- Here the epsilon was set to `3.85e-04`
- see run in folder `__output_data/GW170817real/0_1_2_3_4_5_6_7_8_15_16_17_18/PSD_1/67079_3000_2000_200_200_0.005`
- plots generated with `gwhmc/Tests/plot_trajectory_ph3.py`

![traj_13_5-reduced_mass](traj_13_5-reduced_mass.png)

![traj_13_mc_mu](traj_13_mc_mu.png)

![traj_13_eta](traj_13_eta.png)
