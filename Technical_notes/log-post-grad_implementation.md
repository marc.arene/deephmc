# How to implement sampling with physical priors

## HMC trajectories
- Before:
```py
p_mom += 0.5 * epsilon * dlogL_pos * sampler.scales
```
- After:
```py
p_mom += 0.5 * epsilon * dlogPost * sampler.scales
```
- Where: `dlogPost = dlogL + dlogPrior`

- Should I keep deriving trajectories w.r.t. `ln(Mc)`, `ln(mu)`, `ln(dtc)` ? Meaning should I keep the concept of qpos_traj?
- I could in the sens that I would just have to compute the derivatives of the priors w.r.t. the traj functions
- Maybe run the HMC first with all log-traj_func set to None?
- Or I could have all my search_params have their priors, which defines the function in which it's prior is uniform, and derive the trajectories in those parameters?
  - How to deal with (Mc, mu) which will have uniform priors in (m1, m2)?
  - How to deal with complicated priors as for d_L and spins ?
- No best is to try moving directly in the search parameters themselves (except for `geocent_time` though where I still need to use `geocent_duration`).
  - Hence I would derive `dlogL/dq^mu` and `dlogPrior/dq^mu`
  - For parameters having a simple analytical prior, I could directly code up the corresponding derivative
- **offset for `geocent_duration` when traj_func = identity can't be equal to 1e-7 because of numerical precision, hence setting it to 1e-5 for the moment**



## Scale settings
- If not moving in `lnD` anymore, how should I constrain its scale?
- Before I would limit it to 0.5:
```
13:27 gwhmc WARNING : Scale of luminosity_distance is bigger than its range meaning the FIM is singular and some scales might be wrongly computed
```


## Prior derivative
- Need to compute `dlogLPrior` which will be a vector of size 10 containing:
```py
dlogPrior[param_key] = (priors[param_key].ln_prob(param_plus) - priors[param_key].ln_prob(param_minus)) / (2*epsilon)
```
- I'll want to have in BNS_HMC_Tools.trajectories:
```py
dlogPriors = sampler.priors_gradient.calculate_dlogPriors_np()
```
- Hence I need a PriorGradientsDict class which will be a list/dict of single PriorGradient
```py
class PriorGradientsDict(dict):
  def __init__(self, priors, offset_dict):
    self.priors = priors
    self.default_offset = 1e-7
    for key in search_parameters:
      self[key] = PriorGradient(priors[key])

  def gradients(self, parameters):
    gradients = []
    for key in self.keys():
      gradients.append(self[key].gradient(parameters[key]))
    return np.asarray(gradient)

  def log_gradients(self, parameters):
    log_gradients = []
    for key in self.keys():
      log_gradients.append(self[key].log_gradient(parameters[key]))
    return np.asarray(log_gradients)
```

- create a parent class:
```py
class PriorGradientParent():
  def __init__(self, prior, offset):
    self.prior = prior
    self.offset = 1e-7
    self.prior_class = type(prior)

  def gradient(self, val):
    gradient = (self.prior.prob(val + self.offset) - self.prior.prob(val - self.offset)) / (2*self.offset)
    return gradient

  def log_gradient(self, val):
    log_gradient = (self.prior.ln_prob(val + self.offset) - self.prior.ln_prob(val - self.offset)) / (2*self.offset)
    return log_gradient
```
- and then create a sub-class inheriting which will treate all specific cases:
```py
class PriorGradient(PriorGradientParent):
  def __init__(self, prior, offset):
    super(PriorGradient, self).__init__(prior, offset)

  if self.prior_class == bilby.core.prior.analytical.Uniform:
    def gradient(self, val):
      return 0

    def log_gradient(self, val):
      return 0
  elif toto:
    pass
```

- Define a CoupledPrior Class that will depend on two input priors and a jacobian transformation








## Setting parameters, boundaries etc from prior file
- Now the input prior file must replace my `config_parameter.json`
- Let's keep for the moment the `config_parameter.json` for the setting of trajectory functions, offsets and search_parameter_keys_local_fit.
- But `search_parameter_keys` and `boundaries` must be set by the prior file
- For `search_parameter_keys`: use the `base_sampler.Sampler._initialise_parameters()` method
- First: rename in my code `search_parameters_` into `search_parameter_`...

OR

- Leave it as it is set for the moment and keep the sampler.priors


## Compute hamiltonian
- Use now the complete formula for the potential energy
```py
U = -logL -logPi
```
- Where `logPi` should just be evaluated with:
```py
logPi = priors.ln_prob(qpos)
```

## Teamspeak with Ed
- Keep moving in ln(d), ln(Mc), ln(mu) etc but transform the priors set on those as traj_priors using the formula:
```
traj_prior = prior * jacobian
```
- Look at how lalinference defines its prior:
  - https://lscsoft.docs.ligo.org/lalsuite/lalinference/_l_a_l_inference_prior_8c_source.html#l00504
