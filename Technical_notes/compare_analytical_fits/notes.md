# Comparing the predictive performances of our analytic fit methods

- After running 800 numerical trajectories in phase1, where 792 were accepted leading to a total of 158400 points, we split this dataset into a training set and a validation set. The training set is used to _fit_ the different analytical methods (ie derive the cubic coeff, train the DNN, build the OLUTs). Then each method is asked to predict the true values for both the training set and the validation set.

- Caveat: while the cubic-fit and the OLUTs make the prediction for the value of one numerical gradient w.r.t. a parameter independently of the other gradients, the DNN has been trained by fitting ALL the gradients at the same time. This means that there is a certain limit in the interpretation of these plots since what really matters is how well ALL the gradients at ONE point are predicted by this or that method, which is not shown here.

- Table giving the correspondence between the number of points in the dataset, the corresponding number of phase1 trajectories (ie divide by 200) and the corresponding proportion it represents relative to the 158400 total.

  | n_samples | phase1% | n_traj equivalent |
  | -: | -: | -: |
  | __142560__ | 90% | 712 |
  | __118800__ | 75% | 594 |
  | __79200__  | 50% | 396 |
  | __39600__  | 25% | 198  |
  | __15840__  | 10% | 79  |

- Note on the R-squared score metric, ie the coefficient of determination:
  - scikitlearn describes [here](https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter) the different methods/scores to evaluate the performance of a fit-method
  - [here](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.r2_score.html#sklearn.metrics.r2_score) the page of the function used to compute the displayed _R^2_
  - And [here](https://scikit-learn.org/stable/modules/model_evaluation.html#mean-squared-error) a user guide / documentation about the coefficient of determination
  - Quoting this last web-page:

    _It represents the proportion of variance (of y) that has been explained by the independent variables in the model. It provides an indication of goodness of fit and therefore a measure of how well unseen samples are likely to be predicted by the model, through the proportion of explained variance._

    _**As such variance is dataset dependent, R² may not be meaningfully comparable across different datasets.** Best possible score is 1.0 and it can be negative (because the model can be arbitrarily worse). A constant model that always predicts the expected value of y, disregarding the input features, would get a R² score of 0.0._

## Using a validation set of 10%, ie 15840 samples
- For each training set size, the 15840 samples making the validation set are always the last samples from phase1, meaning in each case the validation set is the same.

### Training set = 50% = 79200 samples | Validation set = 10% = 15840 samples
![compare_methods_reg_79200train_15840val](compare_methods_reg_79200train_15840val.png)

### Training set = 75% = 118800 samples | Validation set = 10% = 15840 samples
![compare_methods_reg_79200train_15840val](compare_methods_reg_118800train_15840val.png)

### Training set = 90% = 142560 samples | Validation set = 10% = 15840 samples
![compare_methods_reg_79200train_15840val](compare_methods_reg_142560train_15840val.png)

### Interpretations
- It looks like the cubic fit is doing good already after 396 numerical traj (except on cos_theta_jn, psi and lnD of course) and not improving much until 712 are used. On the contrary the DNN is doing average after 396 but keeps improving when more data is added, even on the troublesome parameters.
- On the validation sets' R^2:
  - For the DNN: drop in R^2 for `psi` when we increase the training set from 50% to 75%: `R^2: 0.60 => 0.48`
  - For the OLUTs:
    - drop in R^2 for `cos_theta_jn` when going from 75% to 90%: `R^2: 0.67 => 0.60`
    - big drop in R^2 for `ln(D)` when going from 50% to 75%: `R^2: 0.76 => 0.34`
  - How to explain these drops ?

## Using a validation set of 25%, ie 39600 samples

- Let's not forget that the interpretability of the coefficient of determination is dataset-dependent: **"As such variance is dataset dependent, R² may not be meaningfully comparable across different datasets."**
- Hence let's use a bigger validation set by increasing it to 25% of phase1 samples, ie 39600 samples.
- Consistently we can only make plots with training sets of 50% and 75%

### Training set = 50% = 79200 samples | Validation set = 25% = 39600 samples
![compare_methods_reg_79200train_39600val](compare_methods_reg_79200train_39600val.png)

### Training set = 75% = 118800 samples | Validation set = 25% = 39600 samples
![compare_methods_reg_79200train_39600val](compare_methods_reg_118800train_39600val.png)

### Interpretations
- We don't see any suspicious drops in the coefficient of determination when increasing the training set size
