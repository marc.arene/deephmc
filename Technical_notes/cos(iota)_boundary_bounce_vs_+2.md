# How to deal with the -1, +1 boundaries on cos(iota)

- The first option is to perform a bounce, "as usual", by inverting the momentum. This works well.
- The option used in Yann's code was to add 2 to `cos(iota)` when going bellow `-1` and substract 2 when going above `+1`:
  ```py
  if cos_iota < -1:
      cos_iota += 2
  elif cos_iota > 1:
      cos_iota -= 2
  ```
- This worked for Yann and had the advantage of going from one mode to the other in the tooth-like distribution in the iota-distance plane
- However this never worked for me...!
- My explanation is that Yann's TF2 formula did not account for `cos(iota)` in the phase of the waveform through the term `exp(i * arctan(Fx * cos_iota/(F+ * 0.5 * (1 + cos_iota**2)))`. Since the amplitude depends only on `cos(iota)**2`, for him switching from -1 to 1 never made a difference.
- So then the trick I thought about was: adding `2*arctan(Fx/F+)` to `phic` when switching from `cos(iota) = +1` to `-1`
- This should work in a single detector analysis; however with multiple detectors the `“effective new phic” = phic(when cos(iota) was +1) + 2*arctan(Fx/F+)` is different for each of detector since they have different antenna patterns.
- So I simply cannot use this trick, hence I had to keep the bounce on boundary and I can’t get the “tooth-like” distribution in the iota-distance plane.
- Then I thought it wouldn’t matter if I am to marginalize over phase anyway ! But it still kills my trajectories… And I think it makes sense that it still does not work. Indeed if it were to work when marginalizing over phase, then it means that we would not only marginalize over phase but also over ra, dec and psi as they are inputs of the antenna patterns F+ and Fx.
