# How to set input parameters when using an ROQ basis
- An ROQ basis has certain ranges of validity for different parameters which must be taken into account when setting the values of the minimum, maximum frequency, chirp mass etc.


- Once `roq_params` is set we can derive the other parameters as follow:
  - 
