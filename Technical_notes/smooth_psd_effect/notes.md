# Effect on the cubic fit when using smooth PSDs instead of spiky ones

- I had forgotten to update the precomputed ROQ weights for my run yesterday so I had to redo it, I was basically running with the old PSDs.
- Using the smooth PSDs doesn’t improve my cubic fit unfortunately…
  - Regression plots are as bad:
    - spiky PSDs regression plots:
      ![spiky_PSDs_294400_regression_cubic_TestedTrajs100_dlogL012345678](./spiky_PSDs_294400_regression_cubic_TestedTrajs100_dlogL012345678.png)
    - smooth PSDs regression plots:
      ![smooth_PSDs_regression_cubic_TestedTrajs100_dlogL012345678](./smooth_PSDs_regression_cubic_TestedTrajs100_dlogL012345678.png)
  - Acceptance rate on phase3 using only analytical trajectories (with OLUTs for cos(inc), psi and ln(D)) after 100 trajectories is about the same:
    - spiky PSDs: 39.0%
    - smooth PSDs: 48.0%
  - I should still run phase1's numerical trajectories with analytical ones in the smooth PSDs case, I can't right now because I had forgotten to register the momenta during my run with smooth PSDs so I would need to redo the run...
    - spiky PSDs:
      - after the 200 trajectories of length 200: 19%
      - after the 200 trajectories of length 100: 26.9%
      - after the 200 trajectories of length ~100: 27.5%
    - smooth PSDs: ??%

- Quite strangely my phase1 acceptance rate decreases from 98% with the spiky PSDs to 92.3% with the smooth ones.
  - Looking at some phase1 numerical trajectories they seem to oscillate much more in the phic-psi plane than they already do with the spiky PSDs.
  - The corner plots after phase1 are consistent between both cases:
    - Corner plots with spiky PSDs:
      ![spiky_PSDs_corner](./spiky_PSDs_corner.png)
    - Corner plots with smooth PSDs:
      ![smooth_PSDs_corner](./smooth_PSDs_corner.png)
