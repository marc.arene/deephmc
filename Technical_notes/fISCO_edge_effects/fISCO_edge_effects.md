# DESCRIPTION OF THE PROBLEM I HAD

Originally, I noticed some weird spikes appearing sometimes in the numerical gradient of the logL wrt lnmu.

To make things simpler I decided to look at a subproduct of the logL while varying not lmmu but just one of the component masses.

I have an issue which is actually not `bilby` related but (it would seem) `lalsimulation` related; I am still asking here though !
So the algorithm I am working on needs to take the derivative of the waveform with respect to the parameters. Doing so with `mass_1` or `mass_2` leads to strange peaks in my gradient because the source frame waveform that `lalsim` outputs has a steppy behavior as you can see on the figure below:

I am attaching the python script with which you can generate this plot by just running `python steppy_hplus_over_m1.py` in your terminal, it only relies on `lalsim`.
Would anyone know why this happens and how to correct this ?

![fISCO_steppy_hplus_over_m1](./fISCO_steppy_hplus_over_m1.png)

```python
import numpy as np
import matplotlib.pyplot as plt
try:
    import lal
    import lalsimulation as lalsim
except ImportError:
    logger.warning("Please install lal and lalsimulation to run this script.")


parsec = 3.0856775814671916e+16  # m
solar_mass = 1.9884754153381438e+30  # Kg
SOL = 2.99792458e8                                       # Speed of light / m/s
G = 6.67428e-11
GEOM = (G * solar_mass / (SOL*SOL*SOL))

LAL_PI = 3.141592653589793238462643383279502884
LAL_MSUN_SI = 1.988546954961461467461011951140572744e30
LAL_MTSUN_SI = 4.925491025543575903411922162094833998e-6

def Compute_fISCO(Rmin, m1, m2):
    """
    Compute the frequency at the Innermost Stable Circular Orbit
    """

    vISCO = np.sqrt(1.0 / Rmin)
    Mt = m1 + m2
    fmax = vISCO**3 / (np.pi * GEOM * Mt)      # fmax signal
    return fmax

def Compute_LAL_fISCO(m1_SI, m2_SI):
    m1 = m1_SI / LAL_MSUN_SI
    m2 = m2_SI / LAL_MSUN_SI
    m_sec = (m1 + m2) * LAL_MTSUN_SI
    piM = LAL_PI * m_sec
    vISCO = 1 / np.sqrt(6)
    fISCO = vISCO**3 / piM

    return fISCO

if __name__=='__main__':

    luminosity_distance = 40
    mass_1 = 1.47 + 158*1e-7
    mass_2 = 1.27
    spin_1x = 0.0
    spin_1y = 0.0
    spin_1z = 0.0
    spin_2x = 0.0
    spin_2y = 0.0
    spin_2z = 0.0
    iota = 2.81
    phase = 5.497787143782138
    longitude_ascending_nodes = 0.0
    eccentricity = 0.0
    mean_per_ano = 0.0
    # delta_frequency = 0.017
    delta_frequency = 0.016973449670253923
    minimum_frequency = 30.0
    maximum_frequency = 0.0
    reference_frequency = 0.0
    approximant = 5 # Corresponds to `TaylorF2`
    lambda_1 = 0.0
    lambda_2 = 0.0

    waveform_dictionary = lal.CreateDict()
    lalsim.SimInspiralWaveformParamsInsertPNSpinOrder(waveform_dictionary, -1)
    lalsim.SimInspiralWaveformParamsInsertPNTidalOrder(waveform_dictionary, -1)
    lalsim.SimInspiralWaveformParamsInsertPNPhaseOrder(waveform_dictionary, -1)
    lalsim.SimInspiralWaveformParamsInsertPNAmplitudeOrder(waveform_dictionary, 0)
    lalsim.SimInspiralWaveformParamsInsertTidalLambda1(waveform_dictionary, lambda_1)
    lalsim.SimInspiralWaveformParamsInsertTidalLambda2(waveform_dictionary, lambda_2)


    luminosity_distance = luminosity_distance * 1e6 * parsec
    mass_2 = mass_2 * solar_mass

    step = 1e-7
    nb_points = 4
    mass_1_min = mass_1
    mass_1_max = mass_1_min + nb_points*step
    masses_1 = np.linspace(mass_1_min, mass_1_max, nb_points)

    steppy_sub_product = []
    hplus_list = []
    hcross_list = []
    f_isco_LAL_list = []

    for m1 in masses_1:
        m1 = m1 * solar_mass

        hplus, hcross = lalsim.SimInspiralChooseFDWaveform(
        m1, mass_2, spin_1x, spin_1y, spin_1z, spin_2x, spin_2y,
        spin_2z, luminosity_distance, iota, phase,
        longitude_ascending_nodes, eccentricity, mean_per_ano, delta_frequency,
        minimum_frequency, maximum_frequency, reference_frequency,
        waveform_dictionary, approximant)

        steppy_sub_product.append(hplus.data.data.real.sum())
        hplus_list.append(hplus.data.data)
        hcross_list.append(hcross.data.data)

        f_isco = Compute_fISCO(6, m1/solar_mass, mass_2/solar_mass)
        f_isco_LAL = Compute_LAL_fISCO(m1, mass_2)
        f_isco_LAL_list.append(f_isco_LAL)
        print('f_isco     = {}'.format(f_isco))
        print('f_isco_LAL = {} - array should be of size {} and be non-zero from iStart = {} \n'.format(f_isco_LAL, int(f_isco_LAL/delta_frequency + 1), int(minimum_frequency/delta_frequency)+1))
        # Note: the same steppy behavior happenss with `hcross.data.data.imag.sum()` but does not with `hplus.data.data.imag.sum()` or `hcross.data.data.real.sum()`

    # breakpoint()
    # import IPython; IPython.embed()
    # PLOT
    from matplotlib import rc

    plt.rcParams['text.usetex'] = False

    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'

    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'

    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10


    fig0 = plt.figure(figsize=(16,8))
    fig0.suptitle('Steppy behavior of source frame waveform when varying one component mass')
    # fig, axs = plt.subplots(2, 1, figsize=(16,8), gridspec_kw = {'height_ratios': [10, 1]})
    ax0 = plt.subplot(111)
    ax0.plot(masses_1, steppy_sub_product, linewidth=1, marker='o', markersize=3, label='hplus.real.sum()')
    ax0.set_xlabel('mass_1 (in solar masses)')
    ax0.set_ylabel('hplus.real.sum()')
    ax0.set_title('Step size between points =  {:.0e}'.format(step))
    ax0.legend(loc=(0.85,0.7))

    fig0.tight_layout()
    # fig.text(x=0.4, y=0.93, s='Step size between points =  {:.0e}'.format(step))
    fig0.subplots_adjust(top=0.88) # Needed so that fig.title and ax.title don't overlap

    # file_name = './.steppy_hplus_over_m1/steppy_hplus_over_m1.png'
    # fig.savefig(file_name)





    nb_of_freq = len(hplus_list[0])
    frequency_array = np.linspace(0, nb_of_freq*delta_frequency, nb_of_freq)

    hplus_list[2] = np.append(hplus_list[2], 0)
    hplus_list[3] = np.append(hplus_list[3], 0)

    colors = ['blue', 'orange', 'green', 'red']


    fig1 = plt.figure(figsize=(16,8))
    fig1.suptitle('Steppy behavior of source frame waveform when varying one component mass')
    # fig, axs = plt.subplots(2, 1, figsize=(16,8), gridspec_kw = {'height_ratios': [10, 1]})
    ax1 = plt.subplot(111)
    for i, hplus in enumerate(hplus_list):
        ax1.plot(frequency_array[-3:], hplus.real[-3:], linewidth=1, color=colors[i], marker='o', label='hplus.real - point #{}'.format(i+1))
        ax1.axvline(x=f_isco_LAL_list[i], linewidth=0.5, color=colors[i], label='f_ISCO_LAL- point #{}'.format(i+1))
    ax1.set_xlabel('frequency')
    ax1.set_ylabel('hplus.real')
    ax1.set_title('Real part of hplus for different mass_1')
    ax1.legend(loc=(0.85,0.7))

    fig1.tight_layout()
    # fig.text(x=0.4, y=0.93, s='Step size between points =  {:.0e}'.format(step))
    fig1.subplots_adjust(top=0.88) # Needed so that fig.title and ax.title don't overlap

    # file_name = './.steppy_hplus_over_m1/steppy_hplus_over_m1.png'
    # fig.savefig(file_name)


    plt.show()
```

# ANSWER TO THIS ISSUE

I have been able to determine to proper origin of this problem. It is indeed some sort of numerical precision issue but what happens exactly is that varying the mass parameters changes fISCO, ie the end frequency of the signal after which the source frame waveform is padded with zeros. This result in the possibility for two very similar waveform -which only differ in `mass_1` on the order of 1e-7- to be padded with zeros not exactly at the same index at +/- 1 difference; when this happens it is a significant enough difference between the two waveforms to create a step in the logL.

To see this more clearly, we can investigate what happens with the 4 points around the step:

![fISCO_steppy_hplus_over_m1_4pts](./fISCO_steppy_hplus_over_m1_4pts.png)

If we plot their corresponding waveforms in the frequency domain, we can see that they overlapp almost perfectly from 30Hz until their end frequency:

![fISCO_waveforms_overlapping](./fISCO_waveforms_overlapping.png)

...Actually if we pay a closer look at the end, we notice that a significant difference appears on the last point:
![fISCO_waveforms_end_5points](./fISCO_waveforms_end_5points.png)



When plotting their corresponding fISCO, we see that they get around the last sample which is why two of them are padded with zeros while the two others get a 'proper' value for `hplus.real` set at ~ -4e-25 which corresponds to the step in the logL we observe.

![fISCO_around_end_points](./fISCO_around_end_points.png)
