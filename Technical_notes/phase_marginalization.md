# Marginalizing over phase and compute dlogL

- General formula for the log-likelihood-ratio is:
```
logL = <s|h> - 0.5 * <h|h>
```
- It is then possible to marginalize over phase using bessel function _I0_ as explained in this [paper](https://arxiv.org/pdf/1809.02293.pdf), section C.2:
```
logL_phase_marg = logI0(|<s|h>|) - 0.5 * <h|h>
```
- As a note, when dealing with several detectors, the bessel function has to be applied on the sum of inner product and not on each one individually:
```
<s|h>_phase_marg = logI0(| <H1.s|H1.h> + <L1.s|L1.h>  + <V1.s|V1.h>|)
```
- Hence a drawback of marginalizing over phase is that one can no longer compute the derivative dlogL/dln(D) analytically.
  - Indeed before we had that:
  ```
  dlogL/dq = d<s|h>/dq - 0.5 * d<h|h>/dq
  dlogL/dq = <s|dh/dq> - <h|dh/dq>
  ```
  And for `ln(D)` since `dh/dln(D) = dh/dD * D = -h` we could write:
  ```
  dlogL/dln(D) = -<s|h> + <h|h>
  ```
  - However now we need to derive w.r.t the marginalize log likelihood:
  ```
  dlogL_phase_marg/dq = dlogI0(|<s|h>|)/dq - 0.5 * d<h|h>/dq
  ```
  Which we can't simplify that easily
