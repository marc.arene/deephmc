## Problem with aligned spin prior gradients

### Description
- Priors on $\chi_{1,z}$ and $\chi_{2,z}$ can be set uniform over $[-\chi_{max}, \chi_{max}]$, but if one wants to make comparisons with LIGO presessing priors, it's necessary to follow the following "zprior" (see eq (A.7) of [this paper](https://arxiv.org/pdf/1805.10457.pdf): https://arxiv.org/pdf/1805.10457.pdf, deriving from J Veitch's proposition):
$$ \pi_{zprior}(\chi_{i,z}) = \frac{-1}{2\chi_{max}} \ln(\frac{|\chi_{i,z}|}{\chi_{max}})$$

- This prior plots as follow:

![Prior distribution on $\chi_{i,z}$ used by LALInference](pichianaly.png)

- Hence the plot for $\ln\pi_{zprior}(\chi_{i,z})$ on figure 2

![log of the above prior distribution](logpichianaly.png)

- And hence the plot for $\frac{\partial \ln\pi_{zprior}(\chi_{i,z})}{\partial \chi_{i,z}}$ on figure 3

![Gradient of the log-prior demonstrating divergences at the boundaries and at $\chi_i=0$](dlogpichianaly.png)

We can derive the analytical expression for the gradient of the log-prior:
$$ \frac{\partial \ln\pi_{zprior}(\chi_{i,z})}{\partial \chi_{i,z}} = \frac{1}{\chi_{i,z}\; \ln(\frac{|\chi_{i,z}|}{\chi_{max}})}$$
And we see clearly why we have a problem at $\chi_i = 0,\; +\chi_{max},\; -\chi_{max}$. The divergence at $0$ can also easily be understood noting that the prior itself is not defined there.

These high gradient values produce unstable hamiltonian trajectories everytime $\chi_i$ crosses 0 or approaches one of the two edges.

The two plots on the right below demonstrate this:

![Numerical trajectory that was rejected due to the divergence in the gradient of the log-prior on $\chi_2$ when the latter goes from positive to negative value. ](traj_divergence_chi_10.png)

Here is how the performances are degraded due to this effect:

||$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned off|$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned on|
|-|-:|-:|
|Acceptance rate after 60 num traj|~98%|56%|

### Solutions tried

#### Using a smart parametrization
- The idea is to use a trajectory function such that $\frac{d \ln\pi^{f(\chi_i)}(\chi_i)}{d f(\chi_i)}$ is well defined on the whole range. We can use the formula from above.

##### 1st function: $f(x) = x(\ln|x|-1)$

- Considering:
$$\pi^{f(\chi_i)} = \pi^{\chi_i} \frac{1}{f'(\chi_i)}$$
$$\pi^{f(\chi_i)} = \frac{-1}{2\chi_{max}} \ln(\frac{|\chi_{i}|}{\chi_{max}})  \frac{1}{f'(\chi_i)}$$
We see that choosing $f'$ s.t.:
$$ f'(\chi_i) = \ln(\frac{|\chi_{i}|}{\chi_{max}}) $$
would lead to a uniform prior in $f(\chi_i)$ meaning the gradient would always be 0.

The corresponding function is:
$$ f(\chi_i) = \frac{\chi_{i}}{\chi_{max}}\left(\ln\left(\frac{|\chi_{i}|}{\chi_{max}}\right)-1\right) $$

- **Pros:**
  - This function is defined everywhere
  - The gradient of the log-prior is constantly null

- **Cons:**
  - However I couldn't derive easily an analytical expression for the inverse function make this choice hardly implementable.

##### 2nd function: $f(x) = \pm \ln|x|$

- Using:
$$\frac{d \ln\pi^{f(\chi_i)}(\chi_i)}{d f(\chi_i)} = \frac{d \ln\pi^{\chi_i}(\chi_i)}{d \chi_i} \frac{1}{f'(\chi_i)} + \frac{d\ln|1/f'(\chi_i)|}{df(\chi_i)}$$

$$\frac{d \ln\pi^{f(\chi_i)}(\chi_i)}{d f(\chi_i)} = \frac{1/f'(\chi_i)}{\chi_{i,z}\; \ln(\frac{|\chi_{i,z}|}{\chi_{max}})} + \frac{d\ln|1/f'(\chi_i)|}{df(\chi_i)}$$

We see that choosing
$$ f(\chi_i) = \ln(\frac{|\chi_{i}|}{\chi_{max}}) $$

yields
$$\frac{d \ln\pi^{f(\chi_i)}(\chi_i)}{d f(\chi_i)} = \frac{1}{\ln(\frac{|\chi_{i,z}|}{\chi_{max}})} + 1$$

This parametrization gets rid of the divergence at $\chi_i = 0$ (which is the most important one since that's where the chain is going to spend most of its time) but not at the boundaries.

However due to the absolute value this function is not bijective, hence we shall use:
$$ f(\chi_i) = sgn(\chi_i) \ln(\frac{|\chi_{i}|}{\chi_{max}}) $$

- **Pros:**
  - Get rid of divergence in $0$
  - The inverse function is easy to implement.

- **Cons:**
  - This parametrization is still not defined at $\chi_i=0$ where the gradient of the log-prior diverges. Hence there is no chance for our hamiltonian trajectories to cross the vertical axis and the chain will only explore one half of the posterior. I tried proposing specific jumps by inverting $\chi_i$ but after 40 trajectories it never got close to being accepted.


##### Truncating the gradient
- The idea is to put a hard cut on the maximum values of the gradient both at the boundaries and at $0$ to prevent any sudden divergence that a discretized trajectory would be enable to handle.
- The implementation is the following:
$$ \text{if}\quad |\chi_i| < \chi_i^{low} \quad \frac{\partial \ln\pi(\chi_{i})}{\partial \chi_{i}} = \frac{1}{\chi_i^{low}\; \ln(\frac{|\chi_i^{low}|}{\chi_{max}})}$$

$$ \text{elif}\quad |\chi_i| < \chi_i^{high} \quad \frac{\partial \ln\pi(\chi_{i})}{\partial \chi_{i}} = \frac{1}{\chi_i^{high}\; \ln(\frac{|\chi_i^{high}|}{\chi_{max}})}$$

$$ \text{else} \quad \frac{\partial \ln\pi(\chi_{i})}{\partial \chi_{i}} = \frac{1}{\chi_i\; \ln(\frac{|\chi_i|}{\chi_{max}})}$$

After a few tests, I have chosen for the moment $\chi_i^{low}=0.005$ and $\chi_i^{high} = 0.045$ which leads to the following plot:

![Plot of the truncated log-prior on $\chi_i$ using $\chi_i^{low}=0.005$ and $\chi_i^{high} = 0.045$](tpgchi_trunc.png)

- **Pros:**
  - The prior and its gradient are defined everywhere
  - Trajectories can cross the vertical boundary
  - No parametrization
  - Easy to implement

- **Cons:**
  - We don't follow exact hamiltonian trajectories
  - We still have a discontinuity at $\chi_i = 0$
  - However this turns out not to be such a problem since overall in the trajectories the gradient of the log-prior does not account for much in the total gradient of the log-posterior.

- **Results:**
  - After implementing the truncation of the gradients, here are the results:

  ||$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned off|$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned on|
  |-|-:|-:|
  |Acceptance rate after 600 num traj|~98%|87%|

- As long as we compute the MH ratio with the correct prior and manage to keep a high acceptance rate, this truncation just means we use an even more approximated hamiltonian trajectory but that won't bias the results.
- However the drop in acceptance rate is not negligible and rejected trajectories are still due to the crossing of the vertical axis for $\chi_i$ where the bump in the log-prior gradient induces induces a bump in the hamiltonian hence not conserved anymore. This is shown on figure 7:

![The second red peak happens because $\chi_1$ quickly goes negative and back to positive again. This induces a peak in hamiltonian.](traj_truncPi_chi1_3.png)

- To circumvent this I decided to replace the truncation by a smooth function when $|\chi_i| <= \chi_i^{low}$

##### Smooth negative-postive transition with sinusoid

- Looking at the plot of the truncated prior, it seemed natural to patch the region $|\chi_i| < \chi_i^{low}$ with a sine function which would smoothly make the link $\chi_i = - \chi_i^{low}$ and $\chi_i = + \chi_i^{low}$

- On figure 6 is the plot of such a patch:

![Plot of the smoothened version of the log-prior gradient where the correct gradient between $-\chi_i^{low}$ and $+\chi_i^{low}$ is replaced by a sinusoid joining the two. Here we set $\chi_i^{low} = 0.005$. On this plot the truncation at the boundaries is not shown but I still apply it.](tpgchi_smooth.png)

- Note that to make the transition even smoother, the maximum and minimum of the sine function are not set at $\mp \chi^{low}$ but just a bit after and a bit before.

- And on figure 7 a plot a a hamiltonian trajectory using this smoothened version of the log-prior gradient:

![On the top right hand plot we can see how our new log-prior gradient does not reproduce the peak we had earlier with the same intensity, hence the peak in the hamiltonian is dimmer.](traj_smoothenedPi__chi1_3.png)

![On the top right hand plot we can see how our new log-prior gradient handles smoothly the transition firstly when chi goes from positive to negative, then negative to positive and finally we see the truncation when $\chi_i$ approaches $\chi_{max}$. Plot generated using: `python plot_trajectory.py ../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16/PSD_3/22_22_2000_200_200_0.005/phase_marginalization/IMRPhenomD/ --traj_nb=3`](traj_smoothenedPi_3.png)

- **Pros:**
  - The prior and its gradient are defined everywhere
  - Trajectories can cross the vertical boundary
  - No parametrization
  - Easy to implement
  - NO DISCONTINUITY IN THE GRADIENT

- **Cons:**
  - We don't follow exact hamiltonian trajectories
  - However this turns out not to be such a problem since overall in the trajectories the gradient of the log-prior does not account for much in the total gradient of the log-posterior.

- **Results:**
  - After some tests to fine tune $\chi_i^{low}$, I set it to $0.001$ and below are the results with this smooth transition of the gradients:

  ||$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned off|$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned on|
  |-|-:|-:|
  |Acceptance rate after 400 num traj|~98%|94%|

##### Smooth negative-postive transition with tanh

- Same idea as before but replacing the sinusoid by a hyperbolic tangeant, with $\chi_i^{low} = 0.02$

- **Results:**
  - After some tests to fine tune $\chi_i^{low}$, I set it to $0.001$ and below are the results with this smooth transition of the gradients:

  ||$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned off|$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned on|
  |-|-:|-:|
  |Acceptance rate after 100 num traj|~98%|86%|
  - We get a lower acceptance rate than with the sine function with $\chi_i^{low} = 0.001$: 86% vs 94%

- **Interpretations:**
  - The range over which we use the smooth function is bigger, as a result trajectories which spend a lot of time in that range show a bigger drift in the hamiltonian conservation since they are not using the correct gradient related to the true prior, meaning the acceptance probability gets lower.
  - This can be seen on the figure ?? showing such a trajectory

  ![This trajectory spends all its time with $|\chi_2| < 0.02$ where the hyperbolic tangeant is used instead of the real gradient of the log-prior which would diverge at $\chi_2=0$. It results in a slow drift in the hamiltonian giving an acceptance probability at the end point of 34% only.](traj_smoothtanh_chi2_72.png)

  - Plot generated using:
  ```bash
  python plot_trajectory.py ../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16/PSD_3/101_101_2000_200_200_0.005/phase_marginalization/IMRPhenomD/ --traj_nb=72
  ```

\ln(\left(\left|frac{x}{y}\right|\right))
\left( \right. // \left. right)
