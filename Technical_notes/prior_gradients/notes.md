# Sampling with any priors distribution

## Context
- Up until now, in our hamiltonian trajectories, we were equating the gradient of the posterior distribution to that of the likelihood function meaning we were implicitly using uniform priors in the *trajectory parameters*.
- Let's name these priors the **trajectory-priors: $\pi^{f_{traj}(\theta)}$** where $\theta$ is any GW astrophysical parameter we are interested in and $f_{traj}(\theta)$ is it's corresponding parametrised version using a *trajectory function* $f_{traj}$ which I might sometimes abreviate to $f$.
- In order to be able to account for any prior distribution $\pi^{\theta}$ on our astrophysical parameters: $\{\theta\}$, we must do three things:
  1. Find $\pi^{traj(\theta)}$ corresponding to $\pi^{\theta}$ for all $\{\theta\}$
  2. Compute the trajectory prior gradients: in our hamiltonian trajectories
  3. Update the Hamiltonian's potential energy with $\ln(\pi^{traj(\theta)})$ in the Metropolis-Hastings ratio.

## Why using trajectory functions in the first place
- FIM stability
- Have globaly comparable ranges for our parameters, even if that is really accounted for with the scales

## General formula linking $\pi^{\theta}$ to $\pi^{f_{traj}(\theta)}$

### From $\pi^{\theta}$ to $\pi^{f_{traj}(\theta)}$
See [this wikipedia page](https://en.wikipedia.org/wiki/Integration_by_substitution#Application_in_probability) for a more rigorous explanation.
$$ p(\theta \in [a, b]) =  \int_{\theta \in [a, b]} \pi^{\theta}(\theta) d\theta$$
$$ p(\theta \in [a, b]) =  \int_{\theta \in [a, b]} \pi^{\theta}(\theta) \frac{d\theta}{df(\theta)}df(\theta)$$
Hence
$$ p(f(\theta) \in f([a, b])) =  \int_{f(\theta) \in f([a, b])} \pi^{\theta}(\theta)  \left\lvert \frac{1}{f'(\theta)} \right\rvert df(\theta)$$
and

$$\pi^{f(\theta)}(\theta) = \pi^{\theta}(\theta) \left\lvert \frac{1}{f'(\theta)} \right\rvert$$

This is just an application of a variable substitution in one dimension where $\left\lvert \frac{1}{f'(\theta)} \right\rvert = |{f^{-1}}'(\theta)| = |\mathcal{J}_{f(\theta) \rightarrow \theta}|$ where $\mathcal{J}_{f(\theta) \rightarrow \theta}$ is the determinant of the jacobian of the transformation going from $f(\theta) \rightarrow \theta$. The absolute value basically accounts for the fact that if $f$ is decreasing over $[a,b]$ then the corresponding bounds in the new integral are reverted.

And using that formula as the prior for $f_{traj}(\theta)$ will yield a distribution on $\theta$ following $\pi^{\theta}$ which is what we are looking for.

### Deriving $\frac{d \ln\pi^{f_{traj}(\theta)}(\theta)}{d f_{traj}(\theta)}$

$$\frac{d \ln\pi^{f(\theta)}(\theta)}{d f(\theta)} = \frac{d \ln\pi^{(\theta)}(\theta)}{d f(\theta)} + \frac{d \ln|1/f'(\theta)|}{d f(\theta)}$$

$$\frac{d \ln\pi^{f(\theta)}(\theta)}{d f(\theta)} = \frac{d \ln\pi^{(\theta)}(\theta)}{d \theta} \frac{1}{f'(\theta)} + \frac{d \ln|1/f'(\theta)|}{d f(\theta)}$$



### Specific cases

#### Priors uniform in $f_{traj}$
From
$$\pi^{f(\theta)}(\theta) = \pi^{\theta}(\theta) \left\lvert \frac{1}{f'(\theta)} \right\rvert$$

We see that if we choose $f_{traj}$ such that
$$ |f'(\theta)| \propto \frac{1}{\pi^{\theta}(\theta)} $$

Then we get a prior uniform in $f_{traj}(\theta)$ which gradient can directly be set to zero.

It is the case for the following parameters:
|Parameter|$\pi^{\theta}_{LI}(\theta)$ | $f_{traj}$ | $|f'_{traj}|$ | Meaning $\pi_{LI}$ is uniform in...|
|:-|:-|:-|:-|:-|:-|
|$\theta_{JN}$|$sin(\theta_{JN})/2$| $cos(\theta_{JN})$ | $sin(\theta_{JN})$ |$cos(\theta_{JN})$|
|$\psi$|$1/\pi$|$\psi$| 1 |$\psi$|
|$RA$|$\frac{1}{2\pi}$|$RA$| 1 |$RA$|
|$DEC$|$cos(DEC)/2$|$sin(DEC)$| $cos(DEC)$ |$sin(DEC)$|

#### $f_{traj} = logarithm$
It is the case for $d_L$, $\delta t_c$, $\mathcal{M}$ and $\mu$.

Let's note that
$$f'(\theta) = \frac{d\ln(\theta)}{d\theta} = 1/\theta$$

Hence:
$$\pi^{f(\theta)}(\theta) = \pi^{\theta}(\theta) \left\lvert \frac{1}{f'(\theta)} \right\rvert = \pi^{\theta}(\theta) \left\lvert \theta \right\rvert$$

And
$$\frac{d \ln\pi^{\ln(\theta)}(\theta)}{d \ln(\theta)} = \frac{d \ln\pi^{\theta}(\theta)}{d \ln(\theta)}  + \frac{d \ln(\theta)}{d \ln(\theta)}$$
$$\frac{d \ln\pi^{\ln(\theta)}(\theta)}{d \ln(\theta)} = \frac{d \ln\pi^{\theta}(\theta)}{d \ln(\theta)}  + 1$$

Since the prior on $\delta t_c$ is uniform, we can check that our gradient on $\ln(\delta t_c)$ is constant equal to 1.

For $d_L$, the prior can often be taken to be uniform in luminosity volume, ie $\pi^{d_L} \propto d_L^2$. In that case we can check that our gradient w.r.t. $\ln(d_L)$ is constant and equal to 3.


#### Mass parameters
For mass parameters, priors are defined on component masses $m_1$ and $m_2$, usually uniform. However not only do we sample in chirp-mass and reduced-mass but we also use the logarithm as their trajectory functions. These two effects have to be properly accounted for.

The change in variables is now two-dimensional and the formula reads as:
$$\pi^{(\mathcal{M}, \mu)}(\mathcal{M}, \mu) =  \pi^{m_1}(m_1(\mathcal{M}, \mu))\; \pi^{m_2}(m_2(\mathcal{M}, \mu))\; |\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}(\mathcal{M}, \mu)|$$

Where $\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}$ is the determinant of the jacobian matrix defined for the transformation $(\mathcal{M}, \mu) \rightarrow (m_1, m_2)$.

Since the right hand side is not separable in variables, we can't express the would have been corresponding prior distributions on $\mathcal{M}$ alone and $\mu$ alone; these two parameters are linked by a joint prior distribution.

The jacobian for the trajectory functions still needs to be taken into account leading to:
$$\pi^{(\ln\mathcal{M},\; \ln\mu)}(\mathcal{M}, \mu) = \pi^{(\mathcal{M}, \mu)}(\mathcal{M}, \mu)\; \mathcal{M}\; \mu$$

Hence we can finally write:
$$\pi^{(\ln\mathcal{M},\; \ln\mu)}(\mathcal{M}, \mu) =\pi^{m_1}(m_1(\mathcal{M}, \mu))\; \pi^{m_2}(m_2(\mathcal{M}, \mu))\; |\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}(\mathcal{M}, \mu)|\; \mathcal{M}\; \mu$$


###### Deriving $\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}$
- We will compute the jacobian for the inverse transformation, easier to derive, and then use the property:
$$\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)} = \mathcal{J}_{(m_1, m_2) \rightarrow (\mathcal{M}, \mu) }^{-1}$$
- The jacobian matrix when going from $(m_1, m_2) \rightarrow  (\mathcal{M}, \mu)$ is:

$$
J_{(m_1, m_2) \rightarrow  (\mathcal{M}, \mu)}
=  
\begin{bmatrix}
    \frac{\partial{\mathcal{M}}}{\partial{m_1}} & \frac{\partial{\mathcal{M}}}{\partial{m_2}} \\
    \frac{\partial{\mathcal{\mu}}}{\partial{m_1}} & \frac{\partial{\mathcal{\mu}}}{\partial{m_2}}
\end{bmatrix}
$$

- With:
$$ \mathcal{M} = \frac{(m_1 m_2)^{3/5}}{(m_1+m_2)^{1/5}} $$
$$ \mu = \frac{(m_1 m_2)}{(m_1+m_2)} $$

- Leading to:
$$ \frac{\partial \mathcal{M}}{\partial m_1} = \frac{\mathcal{M}}{5} \frac{2m_1+3m_2}{m_1(m_1+m_2)} $$
$$ \frac{\partial \mathcal{M}}{\partial m_2} = \frac{\mathcal{M}}{5} \frac{3m_1+2m_1}{m_2(m_1+m_2)} $$
$$ \frac{\partial \mu}{\partial m_1} = \frac{m_2^2}{(m_1+m_2)^2} $$
$$ \frac{\partial \mu}{\partial m_2} = \frac{m_1^2}{(m_1+m_2)^2} $$

- We can compute the determinant of the jacobian:
$$
|J_{(m_1, m_2) \rightarrow  (\mathcal{M}, \mu)}| = \mathcal{J}_{(m_1, m_2) \rightarrow  (\mathcal{M}, \mu)} = \frac25 \mathcal{M} \frac{m_1 - m_2}{(m_1+m_2)^2} $$

- Finally we can write:
$$ \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)} = \frac52 \frac{1}{\mathcal{M}} \frac{(m_1+m_2)^2}{m_1 - m_2}$$



###### Notes on the jacobian
- Note: I checked that, using these formula for $\frac{\partial \mathcal{M}}{\partial m_i}$, I recover:
$$\mathcal{J}_{(\mathcal{M}, q) \rightarrow (m_1, m_2)} = \frac{m_1^2}{\mathcal{M}}$$
as stated in eq. (21) of [_J. Veitch et al_](https://link.aps.org/doi/10.1103/PhysRevD.91.042003) PE paper.
- It is interesting to note that $\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}$ can be expressed using only the symmetric mass ratio $\eta$:
  - First note that:
  $$ M_T = m_1 + m_2=  \frac{\mathcal{M}^{5/2}}{\mu^{3/2}}$$
  $$ \eta = \frac{m_1m_2}{(m_1 + m_2)^2} = \left(\frac{\mu}{\mathcal{M}}\right)^{5/2} $$
  - Leading to:
  $$ \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)} = \frac52 \frac{M_T^2}{\mathcal{M}} \frac{1}{m_1 - m_2}$$
  - And using:
  $$ m_1 - m_2 = 2M_T \sqrt{1/4 -\eta} $$
  - We get:
  $$ \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)} = \frac54 \eta^{-3/5} \frac{1}{\sqrt{1/4 -\eta}}$$


## Problem with aligned spin prior gradients

### Description
- Priors on $\chi_{1,z}$ and $\chi_{2,z}$ can be set uniform over $[-\chi_{max}, \chi_{max}]$, but if one wants to make comparisons with LIGO presessing priors, it's necessary to follow the following "zprior" (see eq (A.7) of [this paper](https://arxiv.org/pdf/1805.10457.pdf): https://arxiv.org/pdf/1805.10457.pdf, deriving from J Veitch's proposition):
$$ \pi_{zprior}(\chi_{i,z}) = \frac{-1}{2\chi_{max}} \ln(\frac{|\chi_{i,z}|}{\chi_{max}})$$

- This prior plots as follow:

![Prior distribution on $\chi_{i,z}$ used by LALInference](pichianaly.png)

- Hence the plot for $\ln\pi_{zprior}(\chi_{i,z})$ on figure 2

![log of the above prior distribution](logpichianaly.png)

- And hence the plot for $\frac{\partial \ln\pi_{zprior}(\chi_{i,z})}{\partial \chi_{i,z}}$ on figure 3

![Gradient of the log-prior demonstrating divergences at the boundaries and at $\chi_i=0$](dlogpichianaly.png)

We can derive the analytical expression for the gradient of the log-prior:
$$ \frac{\partial \ln\pi_{zprior}(\chi_{i,z})}{\partial \chi_{i,z}} = \frac{1}{\chi_{i,z}\; \ln(\frac{|\chi_{i,z}|}{\chi_{max}})}$$
And we see clearly why we have a problem at $\chi_i = 0,\; +\chi_{max},\; -\chi_{max}$. The divergence at $0$ can also easily be understood noting that the prior itself is not defined there.

These high gradient values produce unstable hamiltonian trajectories everytime $\chi_i$ crosses 0 or approaches one of the two edges.

The two plots on the right below demonstrate this:

![Numerical trajectory that was rejected due to the divergence in the gradient of the log-prior on $\chi_2$ when the latter goes from positive to negative value. ](traj_divergence_chi_10.png)

Here is how the performances are degraded due to this effect:

||$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned off|$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned on|
|-|-:|-:|
|Acceptance rate after 60 num traj|~98%|56%|

### Solutions tried

#### Using a smart parametrization
- The idea is to use a trajectory function such that $\frac{d \ln\pi^{f(\chi_i)}(\chi_i)}{d f(\chi_i)}$ is well defined on the whole range. We can use the formula from above.

##### 1st function: $f(x) = x(\ln|x|-1)$

- Considering:
$$\pi^{f(\chi_i)} = \pi^{\chi_i} \frac{1}{f'(\chi_i)}$$
$$\pi^{f(\chi_i)} = \frac{-1}{2\chi_{max}} \ln(\frac{|\chi_{i}|}{\chi_{max}})  \frac{1}{f'(\chi_i)}$$
We see that choosing $f'$ s.t.:
$$ f'(\chi_i) = \ln(\frac{|\chi_{i}|}{\chi_{max}}) $$
would lead to a uniform prior in $f(\chi_i)$ meaning the gradient would always be 0.

The corresponding function is:
$$ f(\chi_i) = \frac{\chi_{i}}{\chi_{max}}\left(\ln\left(\frac{|\chi_{i}|}{\chi_{max}}\right)-1\right) $$

- **Pros:**
  - This function is defined everywhere
  - The gradient of the log-prior is constantly null

- **Cons:**
  - However I couldn't derive easily an analytical expression for the inverse function make this choice hardly implementable.

##### 2nd function: $f(x) = \pm \ln|x|$

- Using:
$$\frac{d \ln\pi^{f(\chi_i)}(\chi_i)}{d f(\chi_i)} = \frac{d \ln\pi^{\chi_i}(\chi_i)}{d \chi_i} \frac{1}{f'(\chi_i)} + \frac{d\ln|1/f'(\chi_i)|}{df(\chi_i)}$$

$$\frac{d \ln\pi^{f(\chi_i)}(\chi_i)}{d f(\chi_i)} = \frac{1/f'(\chi_i)}{\chi_{i,z}\; \ln(\frac{|\chi_{i,z}|}{\chi_{max}})} + \frac{d\ln|1/f'(\chi_i)|}{df(\chi_i)}$$

We see that choosing
$$ f(\chi_i) = \ln(\frac{|\chi_{i}|}{\chi_{max}}) $$

yields
$$\frac{d \ln\pi^{f(\chi_i)}(\chi_i)}{d f(\chi_i)} = \frac{1}{\ln(\frac{|\chi_{i,z}|}{\chi_{max}})} + 1$$

This parametrization gets rid of the divergence at $\chi_i = 0$ (which is the most important one since that's where the chain is going to spend most of its time) but not at the boundaries.

However due to the absolute value this function is not bijective, hence we shall use:
$$ f(\chi_i) = sgn(\chi_i) \ln(\frac{|\chi_{i}|}{\chi_{max}}) $$

- **Pros:**
  - Get rid of divergence in $0$
  - The inverse function is easy to implement.

- **Cons:**
  - This parametrization is still not defined at $\chi_i=0$ where the gradient of the log-prior diverges. Hence there is no chance for our hamiltonian trajectories to cross the vertical axis and the chain will only explore one half of the posterior. I tried proposing specific jumps by inverting $\chi_i$ but after 40 trajectories it never got close to being accepted.


##### Truncating the gradient
- The idea is to put a hard cut on the maximum values of the gradient both at the boundaries and at $0$ to prevent any sudden divergence that a discretized trajectory would be enable to handle.
- The implementation is the following:
$$ \text{if}\quad |\chi_i| < \chi_i^{low} \quad \frac{\partial \ln\pi(\chi_{i})}{\partial \chi_{i}} = \frac{1}{\chi_i^{low}\; \ln(\frac{|\chi_i^{low}|}{\chi_{max}})}$$

$$ \text{elif}\quad |\chi_i| < \chi_i^{high} \quad \frac{\partial \ln\pi(\chi_{i})}{\partial \chi_{i}} = \frac{1}{\chi_i^{high}\; \ln(\frac{|\chi_i^{high}|}{\chi_{max}})}$$

$$ \text{else} \quad \frac{\partial \ln\pi(\chi_{i})}{\partial \chi_{i}} = \frac{1}{\chi_i\; \ln(\frac{|\chi_i|}{\chi_{max}})}$$

After a few tests, I have chosen for the moment $\chi_i^{low}=0.005$ and $\chi_i^{high} = 0.045$ which leads to the following plot:

![Plot of the truncated log-prior on $\chi_i$ using $\chi_i^{low}=0.005$ and $\chi_i^{high} = 0.045$](tpgchi_trunc.png)

- **Pros:**
  - The prior and its gradient are defined everywhere
  - Trajectories can cross the vertical boundary
  - Easy to implement

- **Cons:**
  - We don't follow exact hamiltonian trajectories
  - However this turns out not to be such a problem since overall in the trajectories the gradient of the log-prior does not account for much in the total gradient of the log-posterior.

##### Result
After implementing the truncation of the gradients, here are the results:

||$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned off|$\partial_{\chi_{i,z}} \ln\pi_{zprior}(\chi_{i,z})$ turned on|
|-|-:|-:|
|Acceptance rate after 600 num traj|~98%|87%|

- As long as we compute the MH ratio with the correct prior and manage to keep a high acceptance rate, this truncation just means we use an even more approximated hamiltonian trajectory but that won't bias the results.
