## Problem with masses prior gradient

As a reminder:
$$\pi^{(\ln\mathcal{M},\; \ln\mu)}(\mathcal{M}, \mu) =\pi^{m_1}(m_1(\mathcal{M}, \mu))\; \pi^{m_2}(m_2(\mathcal{M}, \mu))\; |\mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}(\mathcal{M}, \mu)|\; \mathcal{M}\; \mu$$

Where:
$$ \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)} = \frac52 \frac{1}{\mathcal{M}} \frac{(m_1+m_2)^2}{m_1 - m_2} $$
$$ \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)} = \frac54 \eta^{-3/5} \frac{1}{\sqrt{1/4 -\eta}}$$

is the determinant of the jacobian going from $(\mathcal{M}, \mu) \rightarrow (m_1, m_2)$.

Hence when taking the derivative of the log prior with respect to either $\ln\mathcal{M}$ or $\ln\mu$, a divergence appears when $m_1 = m_2$ ie $\eta=0.25$.


![Numerical hamiltonian trajectory. When $eta$ approaches $0.25$, the jacobian $J$ starts diverging which creates a jump in the Hamiltonian and the trajectory ends up being rejected.](traj_divergence_mu_75.png)

![Same trajectory but focusing on the chirp-mass path. We see consistently a divergence in its log-prior gradient but it is not it causing the jump in the hamiltonian.](traj_divergence_mc_75.png)

Plots created using:
```bash
python plot_trajectory.py ../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16/PSD_3/800_800_2000_200_200_0.005_smoothsinchi_1e-3/phase_marginalization/IMRPhenomD/ --traj_nb=75
```

The problem here is that our chain doesn't have the time to bounce on the $\eta=0.25$ before the gradient starts diverging a killing the trajectory.

### Solution: capping the gradient

- I have simply put a hard cut on the maximum value that the gradient can take, $10^4$ as a first try.
- This doesn't solve entirely the problem as we can see on the following trajectory plot:

![$\mu$ bounces several times on the equal mass boundary and while its gradient of the log-prior is capped to $10^4$, this still produces little perturbations in the hamiltonian. The latter are resorbed and appear as peaks but there is still a slight increase in the hamiltonian value once the peak is finished.](traj_mu_capped_43.png)


Plot generated using the following, note that the actual trajectory number is 143 because I had deleted the records for the 1st 100 ones:
```bash
python plot_trajectory.py ../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16/PSD_3/800_800_2000_200_200_0.005/phase_marginalization/IMRPhenomD/ --traj_nb=43
```

### Analytical formula for the gradient of the log-jacobian

- Up until now (as for the above plots), the gradients of the log-prior for chirp-mass and reduced-mass where computed numerically, using the log of the first formula above.
- However it turns out to be quite easy to derive the analytical expression for the gradient of the log-jacobian.

- First remember that:
$$ \eta = \frac{m_1m_2}{(m_1 + m_2)^2} = \left(\frac{\mu}{\mathcal{M}}\right)^{5/2} $$

- Hence:
$$ \frac{\partial \ln \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}}{\partial\ln\mathcal{M}} = -\frac52 \frac{\partial \ln \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}}{\partial\ln\eta} $$
$$ \frac{\partial \ln \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}}{\partial\ln\mu} = \frac52 \frac{\partial \ln \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}}{\partial\ln\eta} $$

- And using the expression of the Jacobian as a function of $\eta$ only:
$$ \frac{\partial \ln \mathcal{J}_{(\mathcal{M}, \mu) \rightarrow (m_1, m_2)}}{\partial\ln\eta} = -\frac35 + \frac12 \frac{\eta}{1/4 - \eta}$$
