# Setting DeepHMC for a run on a new event
- The bash line to launch will typically look like
```bash
python main.py \
--event=GW170817 \
--trigger_file=../examples/trigger_files/GW170817_IMRPhenomD_NRTidal.ini \
--config_file=../examples/config_files/config_GW170817_IMRPhenomD_NRTidal.ini \
--fit_method=dnn
```
- The `--event` option defines the name of the event and will be used to find input files in the directory `local_path_to/gwhmc/__input_data/GW170817/`, knowing that `main.py` is stored in `/gwhmc/main.py`.


## Configuration files
The current way config files work is unfortunately a bit complicated and confusing, it should be simplified in the long term.

### The `config.ini` file
- Path given in the `--config_file` option of the bash `python main.py` command line.
- Create a `config.ini` file, following the example of `gwhmc/examples/config_files/config_GW170817_IMRPhenomD_NRTidal.ini`: it must contain 3 sections named `[analysis]`, `[parameters]` and `[hmc]`:
```ini
[analysis]
# Interferometers to run the analysis on; must be separated by a comma.
ifos = H1,L1,V1
approximant = IMRPhenomD_NRTidal
# Names of the ifo in the dictionary here must match those defined above.
minimum_frequency_ifos = {H1: 30.0, L1: 30.0, V1: 30.0}
maximum_frequency = 2048.0
reference_frequency = 20
roq = False
roq_b_matrix_directory = /Users/marcarene/roq/ROQ_data/IMRPhenomPv2/
# 1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.
psd = 1
psds_single_file = ../__input_data/GWTC1_PSDs/GWTC1_GW170817_PSDs.dat
#psd_dict = {H1: /path/to/H1_PSD.dat, L1: /path/to/H1_PSD.dat, V1: /path/to/H1_PSD.dat}
phase_marginalization = True
# `lal_binary_black_hole` or `lal_binary_neutron_star`. If not set, `lal_binary_neutron_star` is selected by default.
# These are the waveform models defined in bilby.gw.source
frequency_domain_source_model = lal_binary_neutron_star

[parameters]
prior_file=../gw/prior_files/GW170817_LALInf_IMRPhenomD_NRTidal.prior

[hmc]
# Total number of trajectories of HMC
n_traj_hmc_tot = 1000000
# Number of trajectories using  numericalgradients in phase 1.
n_traj_fit = 1500
# Number of iterations if phase1 loop to go over for this run. This permits to split phase1 into several chunks. If set to 0, then the value will be set to that of n_traj_fit
n_traj_for_this_run = 300000
# Number of points used to sub-select points in Ordered Look-Up Tables for local fit bimodal parameters like (cos(inc), psi, logD)
n_fit_1 = 2000
# Number of points sub-selected from the above n1 ones to perform a linera fit
n_fit_2 = 200
# Length of numerical trajectories during phase1
length_num_traj = 200
# Stepsize between steps of trajectories used for phase1.
epsilon0 = 5e-3
# Python file to configure your search parameter space
config_hmc_parameters_file = ../examples/config_params/GW170817_IMRPhenomD_NRTidal.json
# Number of statistically independant samples desired.
n_sis = 5000
```
- PSDs can either be set from a single file containing the PSDs of each ifos. They are stored on DCC in such files for GWTC1 events. In that case use the `psds_single_file =` option.
- If each PSD is stored in one file then use the `psd_dict = ` option to define each path.
- If none of the above, then PSDs are estimated strom the strain data using the Welch method.
- The `psd = 1` (or 2 or 3) option was used in case of injections but it certainly broken now.
- `n_fit_1` and `n_fit_2` should be removed from the code since we use the DNN now and not OLUTs.

### The `prior.prior` file
- Its path is defined in the `[parameters]` section of the above `config.ini` file after the key `prior_file =`.
- At the moment some examples of prior files can be found in... ...`/gwhmc/gw/prior_files`.
- They are handled as Bilby does since they defines Bilby prior functions and look like:
```
# Example of priors for GW170817 using IMRPhenomD_NRTidal
theta_jn = Sine(name='theta_jn', boundary='reflective')
psi = Uniform(name='psi', minimum=0, maximum=np.pi, boundary='periodic')
luminosity_distance = PowerLaw(name='luminosity_distance', alpha=2, minimum=10, maximum=100, unit='Mpc', boundary=None)
mass_1 = Uniform(name='mass_1', minimum=0.5, maximum=7.7, unit='$M_{\odot}$')
mass_2 = Uniform(name='mass_2', minimum=0.5, maximum=7.7, unit='$M_{\odot}$')
chirp_mass = Constraint(name='chirp_mass', minimum=1.1838325, maximum=2.16847416667, unit='$M_{\odot}$')
# reduced_mass = Constraint(name='reduced_mass', minimum=0.5, maximum=1.3, unit='$M_{\odot}$')
dec = Cosine(name='dec', boundary='reflective')
ra = Uniform(name='ra', minimum=0, maximum=2 * np.pi, boundary='periodic')
geocent_time = Uniform(name='geocent_time', minimum=-np.inf, maximum=np.inf, boundary='reflective', latex_label='$t_c$', unit='$s$')
chi_1 = bilby.gw.prior.AlignedSpin(name='chi_1', a_prior=Uniform(minimum=0, maximum=0.05), boundary='reflective')
chi_2 = bilby.gw.prior.AlignedSpin(name='chi_2', a_prior=Uniform(minimum=0, maximum=0.05), boundary='reflective')
lambda_1 = Uniform(name='lambda_1', minimum=0, maximum=5000)
lambda_2 = Uniform(name='lambda_2', minimum=0, maximum=5000)
```
- Each line will be translated into the corresponding Bilby prior function, just as if we had written python code in this text file.
- The `boundary='reflective'` argument is not used in DeepHMC since we handle boundaries differently.
- Similar to what is done in Bilby, the prior on `geocent_time` is set internally by DeepHMC to be flat at `+/- 0.1` around the merger time.

### The `config_params.json` file
- Path defined in the `[hmc]` section of the `config.ini` file under the key `config_hmc_parameters_file = `.
- The information it contains could probably be directly defined in the `config.ini` file, which would be simpler, by writing some "string-dictionary" (just like for the `minimum_frequency_ifos = {H1: 20.0, L1: 30.0, V1: 20.0}`) which would then be converted into proper dictionaries using the `core.utils.convert_string_to_dict` method, but at the time I did not know about that handy bilby_pipe function so I used a json file to handle the nested structure of the information.
- So this json file typically looks like:
```json
{
  "search_parameter_keys": [
    "theta_jn",
    "psi",
    "luminosity_distance",
    "chirp_mass",
    "reduced_mass",
    "dec",
    "ra",
    "geocent_time",
    "chi_1",
    "chi_2",
    "lambda_1",
    "lambda_2"
  ],
  "trajectory_functions": {
      "theta_jn": "cosine",
      "psi": "None",
      "luminosity_distance": "log",
      "chirp_mass": "log",
      "reduced_mass": "log",
      "dec": "sine",
      "ra": "None",
      "geocent_time": "log",
      "chi_1": "None",
      "chi_2": "None",
      "lambda_1": "None",
      "lambda_2": "None"
  },
  "search_parameter_keys_local_fit": [
    "theta_jn",
    "psi",
    "luminosity_distance"
  ],
  "parameters_offsets": {
      "theta_jn": 1e-7,
      "phase": 1e-7,
      "psi": 1e-7,
      "luminosity_distance": 1e-7,
      "chirp_mass": 1e-7,
      "reduced_mass": 1e-7,
      "dec": 1e-7,
      "ra": 1e-7,
      "geocent_time": 1e-7
  }
}
```
- Most importantly it defines the search parameter space of the analysis, so here 12 dimensional and implicitely marginalizing over phase at coalescence (which option must therefore be set to `True` in the `config.ini` file).
- `geocent_time` will automatically be converted internally into `geocent_duration` because otherwise the HMC suffers from numerical precision issues when computing numerical gradients on a GPS time. But final results are then converted back to `geocent_time` by simply adding the `start_time` of the analysis.
- Each search parameter can have a "trajectory function" which defines the space in which trajectories are computed. Priors, defined for instance on `luminosity_distance` and not on `log(luminosity_distance)`, automatically adapt to the trajectory functions used. If set to `None` or not set in this file then the identity function is used.
- `search_parameter_keys_local_fit` made sense for the OLUTs but should be removed from the code now (removing the key directly in the file might break the code so careful...).
- `parameters_offsets` defines the offsets which will be used when computing numerical differenciation. If not specified for a parameter, the default value will be `1e-7`.
- Note that priors can be defined on component masses even though we sample in `log(chirp_mass)` and `log(reduced_mass)`.
- Note that if not marginalizing over the phase at coalescence, then the key `phase` refers to the *orbital* phase, to be consistent with Bilby, and not the phase at coalescence.

### The `trigger.ini` file
- Path given in the `--trigger_file` option of the bash `python main.py` command line.
- It defines the parameter values of the starting point of the analysis.
- For GW170817 using IMRPhenomD_NRTidal and marginalizing over the phase at coalescence, it can look like
```ini
[parameters]
mass_1=1.72592086595
mass_2=1.10730509114
luminosity_distance=21.1702759705
theta_jn=2.04318308535
psi=1.61012417485
geocent_time=1187008882.43
ra=3.44616
dec=-0.408084
chi_1=0.027842426346
chi_2=0.0278262466181
lambda_1=130.651548464
lambda_2=210.695175639
```
- `chirp_mas`, `total_mass`, `symmetric_mass_ratio`, `mass_ratio` and `reduced_mass` are automatically computed from the two component masses, but not the other way around, hence one must specify `mass_1` and `mass_2` in this file.


## Input `.hdf5` strain files
- The `--event` option of the bash `python main.py` command line defines the name of the event and will be used to find input files in the directory `local_path_to/gwhmc/__input_data/[event_name]/`, knowing that `main.py` is stored in `/gwhmc/main.py`. But note that at the moment is it hard-coded to look in the relative path `../__input_data/[event_name]`. This behavior should of course be modified...
- Since at the moment the path for each strain file is not specified in any configuration file, the code lists every `.hdf5` file present in the directory `../__input_data/[event_name]/` assuming they all refer to one strain file per interferometer (for some reason I hard-coded in `main.py` that for GW170817 it looks into `../__input_data/GW170817/LOSC/` ...).
- Then to differentiate between each ifo it is assumed that each file starts with a consistent prefix: `H-H1_[...].hdf5` or `L-L1_[...].hdf5` or `V-V1_[...].hdf5`.
- These files can be downloaded from the GWOSC website.
- If the sampling frequency defined in `config.ini` is higher than that used in the `.hdf5` file, an error is thrown, if smaller then the strain data is automatically down-sampled accordingly by keeping only 1 strain sample every `N` where `N = int(sampling_frequency_from_strain_file / sampling_frequency_from_config_file)`.
