# On Ed's computer

## Install my bilby's fork properly on my computer and make Ed clone my fork

## Clone bilby's repo
```
$ cd
$ mkdir projects
$ cd projects
$ mkdir python
$ cd python
$ git clone git@git.ligo.org:marc.arene/bilby.git
```

## Create conda environment and install bilby from source

- Create new conda environment with desired python version
```
$ conda create -n hmc python=3.7.3
```

- Then going to the directory where I have git cloned bilby's repo:
```
$ conda activate hmc
(hmc) $ conda install h5py<3.0.0
(hmc) $ conda install --file requirements.txt
(hmc) $ python setup.py develop
(hmc) $ conda install --file optional_requirements.txt
(hmc) $ conda install ipython
```

## Clone the gwhmc repo

```
$ cd ~/projects/python
$ git clone git@gitlab.in2p3.fr:marc.arene/gwhmc.git
```


## On CIT


- Source environment to have access to conda
```
$ source /cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/etc/profile.d/conda.sh
```
- export variable to avoid permission error on qt packages:
```
$ export CONDA_PKGS_DIRS="/local/marc.arene/conda/pkgs"
```

- Clone ligo env in new env named here 'hmc' and then activate it:
```
$ conda create --name hmc --clone ligo-py37
$ conda activate hmc
```
- Clone with https instead of ssh:
```
(hmc) $ git clone https://gitlab.in2p3.fr/marc.arene/gwhmc.git
(hmc) $ git clone https://git.ligo.org/marc.arene/bilby.git
```

## Using tensorflow backend

- There is an easy mess to get into when using keras and keras-tuner, here is why:
- Natural `conda install keras` will install the keras package with a tensorflow (TF) backend optimized for my CPU, but that will be TF v1.14
- Keras-tuner needs TF v2.0 to run. Hence when installing it, it updated the tensorflow backend but then the `import keras` would throw the following error:
```py
RuntimeError: It looks like you are trying to use a version of multi-backend Keras that does not support TensorFlow 2.0. We recommend using `tf.keras`, or alternatively, downgrading to TensorFlow 1.14.
```

- I therefore updated to TF v2.0 which however is not optimized for my CPU. One difference is that instead of doing `import keras` one has to do `import tensorflow.keras`
- Hence when I did this, my DNN using now TF v2.0 but un-optimized for my CPU, was 250 times slower than previously
- Unfortunately, from the [tensorflow official doc](https://www.tensorflow.org/install/source#build_the_pip_package), it doesn't seem possible at the moment to build TF 2.0 with the right compiled flags for CPU only support.
- Going back wasn't/isn't that easy...
- I cloned my current conda env
- in the cloned env I uninstalled TF and all packages depending on it (hence keras ones)
- After reading different stuff online:
  - https://stackoverflow.com/questions/47068709/your-cpu-supports-instructions-that-this-tensorflow-binary-was-not-compiled-to-u
  - https://github.com/lakshayg/tensorflow-build
  - https://stackoverflow.com/questions/41293077/how-to-compile-tensorflow-with-sse4-2-and-avx-instructions
  I used the second link to pip install TF v1.14 build with the correct support for my CPU.
- However doing this gave the following log:
```
(hmcclone) marcarene in ~/projects/python/gwhmc/Library on dev [!$]
$ pip install --ignore-installed --upgrade "https://github.com/lakshayg/tensorflow-build-archived/releases/download/tf1.14.1-mojave-py3.7/tensorflow-1.14.1-cp37-cp37m-macosx_10_9_x86_64.whl" --user
Collecting tensorflow==1.14.1 from https://github.com/lakshayg/tensorflow-build-archived/releases/download/tf1.14.1-mojave-py3.7/tensorflow-1.14.1-cp37-cp37m-macosx_10_9_x86_64.whl
  Downloading https://github.com/lakshayg/tensorflow-build-archived/releases/download/tf1.14.1-mojave-py3.7/tensorflow-1.14.1-cp37-cp37m-macosx_10_9_x86_64.whl (105.9MB)
    100% |████████████████████████████████| 105.9MB 213kB/s
Collecting tensorflow-estimator<1.15.0rc0,>=1.14.0rc0 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/3c/d5/21860a5b11caf0678fbc8319341b0ae21a07156911132e0e71bffed0510d/tensorflow_estimator-1.14.0-py2.py3-none-any.whl (488kB)
    100% |████████████████████████████████| 491kB 1.1MB/s
Collecting numpy<2.0,>=1.16.0 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/81/14/6d7c914dac1cb2b596d2adace4aa4574d20c0789780f1339d535e69e271f/numpy-1.18.2-cp37-cp37m-macosx_10_9_x86_64.whl (15.1MB)
    100% |████████████████████████████████| 15.1MB 967kB/s
Collecting grpcio>=1.8.6 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/35/f5/9dce584256802e9d26f51e689bf42b365442c6c52f1af8a44c86ad62359a/grpcio-1.27.2-cp37-cp37m-macosx_10_9_x86_64.whl (2.5MB)
    100% |████████████████████████████████| 2.5MB 3.0MB/s
Collecting gast>=0.2.0 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/d6/84/759f5dd23fec8ba71952d97bcc7e2c9d7d63bdc582421f3cd4be845f0c98/gast-0.3.3-py2.py3-none-any.whl
Collecting absl-py>=0.7.0 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/1a/53/9243c600e047bd4c3df9e69cfabc1e8004a82cac2e0c484580a78a94ba2a/absl-py-0.9.0.tar.gz (104kB)
    100% |████████████████████████████████| 112kB 5.2MB/s
Collecting six>=1.10.0 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/65/eb/1f97cb97bfc2390a276969c6fae16075da282f5058082d4cb10c6c5c1dba/six-1.14.0-py2.py3-none-any.whl
Collecting tensorboard<1.15.0,>=1.14.0 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/91/2d/2ed263449a078cd9c8a9ba50ebd50123adf1f8cfbea1492f9084169b89d9/tensorboard-1.14.0-py3-none-any.whl (3.1MB)
    100% |████████████████████████████████| 3.2MB 1.2MB/s
Collecting protobuf>=3.6.1 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/4c/25/c057a298635d08d087a20f51ff4287d821814208ebb045d84ea65535b3e3/protobuf-3.11.3-cp37-cp37m-macosx_10_9_x86_64.whl (1.3MB)
    100% |████████████████████████████████| 1.3MB 2.6MB/s
Collecting wheel>=0.26 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/8c/23/848298cccf8e40f5bbb59009b32848a4c38f4e7f3364297ab3c3e2e2cd14/wheel-0.34.2-py2.py3-none-any.whl
Collecting keras-applications>=1.0.6 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/71/e3/19762fdfc62877ae9102edf6342d71b28fbfd9dea3d2f96a882ce099b03f/Keras_Applications-1.0.8-py3-none-any.whl (50kB)
    100% |████████████████████████████████| 51kB 3.4MB/s
Collecting astor>=0.6.0 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/c3/88/97eef84f48fa04fbd6750e62dcceafba6c63c81b7ac1420856c8dcc0a3f9/astor-0.8.1-py2.py3-none-any.whl
Collecting google-pasta>=0.1.6 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/a3/de/c648ef6835192e6e2cc03f40b19eeda4382c49b5bafb43d88b931c4c74ac/google_pasta-0.2.0-py3-none-any.whl (57kB)
    100% |████████████████████████████████| 61kB 3.3MB/s
Collecting termcolor>=1.1.0 (from tensorflow==1.14.1)
  Using cached https://files.pythonhosted.org/packages/8a/48/a76be51647d0eb9f10e2a4511bf3ffb8cc1e6b14e9e4fab46173aa79f981/termcolor-1.1.0.tar.gz
Collecting wrapt>=1.11.1 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/82/f7/e43cefbe88c5fd371f4cf0cf5eb3feccd07515af9fd6cf7dbf1d1793a797/wrapt-1.12.1.tar.gz
Collecting keras-preprocessing>=1.0.5 (from tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/28/6a/8c1f62c37212d9fc441a7e26736df51ce6f0e38455816445471f10da4f0a/Keras_Preprocessing-1.1.0-py2.py3-none-any.whl (41kB)
    100% |████████████████████████████████| 51kB 7.4MB/s
Collecting werkzeug>=0.11.15 (from tensorboard<1.15.0,>=1.14.0->tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/ba/a5/d6f8a6e71f15364d35678a4ec8a0186f980b3bd2545f40ad51dd26a87fb1/Werkzeug-1.0.0-py2.py3-none-any.whl (298kB)
    100% |████████████████████████████████| 307kB 1.8MB/s
Collecting setuptools>=41.0.0 (from tensorboard<1.15.0,>=1.14.0->tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/70/b8/b23170ddda9f07c3444d49accde49f2b92f97bb2f2ebc312618ef12e4bd6/setuptools-46.0.0-py3-none-any.whl (582kB)
    100% |████████████████████████████████| 583kB 1.1MB/s
Collecting markdown>=2.6.8 (from tensorboard<1.15.0,>=1.14.0->tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/ab/c4/ba46d44855e6eb1770a12edace5a165a0c6de13349f592b9036257f3c3d3/Markdown-3.2.1-py2.py3-none-any.whl (88kB)
    100% |████████████████████████████████| 92kB 1.4MB/s
Collecting h5py (from keras-applications>=1.0.6->tensorflow==1.14.1)
  Downloading https://files.pythonhosted.org/packages/1a/8b/4d01ae9a9d50a0bcc7b0b9aae41785d8d9de6fa9bba04dc20b1582181d2d/h5py-2.10.0-cp37-cp37m-macosx_10_6_intel.whl (3.0MB)
    100% |████████████████████████████████| 3.0MB 1.3MB/s
Building wheels for collected packages: absl-py, termcolor, wrapt
  Building wheel for absl-py (setup.py) ... done
  Stored in directory: /Users/marcarene/Library/Caches/pip/wheels/8e/28/49/fad4e7f0b9a1227708cbbee4487ac8558a7334849cb81c813d
  Building wheel for termcolor (setup.py) ... done
  Stored in directory: /Users/marcarene/Library/Caches/pip/wheels/7c/06/54/bc84598ba1daf8f970247f550b175aaaee85f68b4b0c5ab2c6
  Building wheel for wrapt (setup.py) ... done
  Stored in directory: /Users/marcarene/Library/Caches/pip/wheels/b1/c2/ed/d62208260edbd3fa7156545c00ef966f45f2063d0a84f8208a
Successfully built absl-py termcolor wrapt
pegasus-wms 4.9.1 requires pam==0.1.4, which is not installed.
pegasus-wms 4.9.1 requires plex==2.0.0dev, which is not installed.
pegasus-wms 4.9.1 has requirement boto==2.48.0, but you ll have boto 2.49.0 which is incompatible.
pegasus-wms 4.9.1 has requirement Flask-SQLAlchemy==0.16, but you ll have flask-sqlalchemy 2.3.2 which is incompatible.
pegasus-wms 4.9.1 has requirement itsdangerous==0.24, but you ll have itsdangerous 1.1.0 which is incompatible.
pegasus-wms 4.9.1 has requirement Jinja2==2.8.1, but you ll have jinja2 2.10.1 which is incompatible.
pegasus-wms 4.9.1 has requirement MarkupSafe==1.0, but you ll have markupsafe 1.1.1 which is incompatible.
pegasus-wms 4.9.1 has requirement pyOpenSSL==17.5.0, but you ll have pyopenssl 19.0.0 which is incompatible.
pegasus-wms 4.9.1 has requirement requests==2.18.4, but you ll have requests 2.21.0 which is incompatible.
pegasus-wms 4.9.1 has requirement SQLAlchemy==0.8.0, but you ll have sqlalchemy 1.3.2 which is incompatible.
pegasus-wms 4.9.1 has requirement Werkzeug==0.14.1, but you ll have werkzeug 1.0.0 which is incompatible.
Installing collected packages: tensorflow-estimator, numpy, six, grpcio, gast, absl-py, setuptools, protobuf, werkzeug, markdown, wheel, tensorboard, h5py, keras-applications, astor, google-pasta, termcolor, wrapt, keras-preprocessing, tensorflow
  The scripts f2py, f2py3 and f2py3.7 are installed in '/Users/marcarene/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  The script markdown_py is installed in '/Users/marcarene/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  The script wheel is installed in '/Users/marcarene/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  The script tensorboard is installed in '/Users/marcarene/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  The scripts freeze_graph, saved_model_cli, tensorboard, tf_upgrade_v2, tflite_convert, toco and toco_from_protos are installed in '/Users/marcarene/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
Successfully installed absl-py-0.9.0 astor-0.8.1 gast-0.3.3 google-pasta-0.2.0 grpcio-1.27.2 h5py-2.10.0 keras-applications-1.0.8 keras-preprocessing-1.1.0 markdown-3.2.1 numpy-1.18.2 protobuf-3.11.3 setuptools-46.0.0 six-1.14.0 tensorboard-1.14.0 tensorflow-1.14.1 tensorflow-estimator-1.14.0 termcolor-1.1.0 werkzeug-1.0.0 wheel-0.34.2 wrapt-1.12.1
```

- This worked but installed TF and it's dependencies (including numpy...) into $`HOME/.local/lib/python3.7/site-packages`. Now the default numpy imported is that from this directory... And I'm back to not using keras-tuner since I'm on TF v1.14 and not v2.0

- I renamed `$HOME/.local/` into `$HOME/.local_temp/` so that it's these packages aren't imported anymore.

- In the end I'm using two conda environments:
  - env hmc_tf1 were keras was installed using `conda install keras` and is running on tensorflow v1 (hence tf1)
    - I get multiple times the following warning at very beginning of the run because my numpy version is 1.18 when TF v1.14 was created with numpy 1.17 (?): `FutureWarning: Passing (type, 1) or '1type' as a synonym of type is deprecated; in a future version of numpy, it will be understood as (type, (1,)) / '(1,)type'.`
    - computing dlogL takes ~0.4ms
    - I still get the following warning: `Your CPU supports instructions that this TensorFlow binary was not compiled to use: AVX2 FMA`
  - env hmc were tensorflow was installed using `conda install keras-tuner`, keras is called with `import tensorflow.keras as keras`
    - I can use keras-tuner
    - computing dlogL takes ~40ms
