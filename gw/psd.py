import numpy as np
import matplotlib.pyplot as plt
import sys


def plot_psds_from_files(psds_files_paths, save_file_path=None, fmin=23, fmax=2048, labels=None):
    if type(psds_files_paths) != list:
        psds_files_paths = [psds_files_paths]
    freq_arrays = []
    psds_arrays = []
    for psd_file_path in psds_files_paths:
        freq_and_psd_array = np.loadtxt(psd_file_path)
        freq_arrays.append(freq_and_psd_array[:,0])
        psds_arrays.append(freq_and_psd_array[:,1])

    plot_psds(freq_arrays[0], psds_arrays, save_file_path, fmin, fmax, labels)

def plot_psds(freq_array, psds_arrays, save_file_path=None, fmin=23, fmax=2048, labels=None):
    psd_min = 1
    psd_max = -1
    mask = np.logical_and(fmin <= freq_array, freq_array <= fmax)
    for i in range(len(psds_arrays)):
        if labels is not None:
            plt.plot(freq_array, psds_arrays[i], label=labels[i])
        else:
            plt.plot(freq_array, psds_arrays[i])
        psd_min = min(min(psds_arrays[i][mask]), psd_min)
        psd_max = max(max(psds_arrays[i][mask]), psd_max)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim((fmin, fmax))
    plt.ylim((psd_min, psd_max))
    plt.xlabel('f (Hz)')
    plt.ylabel('PSD')
    plt.legend()
    if save_file_path is None:
        plt.show()
    else:
        plt.savefig(save_file_path)

if __name__ == '__main__':
    # dir = '/Users/marcarene/projects/python/gwhmc/__input_data/GW190412/discovery_repo'
    # psds_files_paths=['/pe_PSDs_glitch_median_PSD_forLI_H1.dat', '/pe_PSDs_glitch_median_PSD_forLI_L1.dat', '/pe_PSDs_glitch_median_PSD_forLI_V1.dat']
    # save_file_path = dir + '/pe_PSDs_glitch_median_PSD_plot.png'

    dir = '/Users/marcarene/projects/python/gwhmc/__input_data/GW190814'
    psds_files_paths=['/glitch_median_PSD_forLI_H1.dat', '/glitch_median_PSD_forLI_L1.dat', '/glitch_median_PSD_forLI_V1.dat']
    save_file_path = dir + '/4096-psd_plot.png'
    #
    # dir = '/Users/marcarene/projects/python/gwhmc/__input_data/GW190412/1024'
    # psds_files_paths=['/H1-psd.dat', '/L1-psd.dat', '/V1-psd.dat']
    # save_file_path = dir + '/1024-psd_plot.png'



    psds_files_paths = [dir + file for file in psds_files_paths]
    labels = ['H', 'L', 'V']

    plot_psds_from_files(psds_files_paths, fmax=2048, labels=labels, save_file_path=save_file_path)
