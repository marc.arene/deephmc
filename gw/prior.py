import numpy as np
import bilby
import Library.param_utils as paru
import core.utils as cut
import pandas as pd
import bilby.core.prior as bcp
import bilby.gw.prior as bgp

def identity(value):
    """
    Function needed for parameter which are sampled without any function around them.
    """
    return value

def log_val_plus_one(val):
    return np.log(val + 1)

def log_val_plus_one_inv(val):
    return np.exp(val) - 1

def one_over_x(val):
    return 1 / val

def one_over_x_inv(val):
    return 1 / val

def signval_log_abs_val(val):
    if val >= 0:
        return np.log(val)
    else:
        return -np.log(-val)

def signval_log_abs_val_inv(traj_val):
    if traj_val <= 0:
        return np.exp(traj_val)
    else:
        return -np.exp(-traj_val)


def map_func_to_inv_func(func):
    if func == identity:
        inv_func = identity
    elif func == np.log:
        inv_func = np.exp
    elif func == np.cos:
        inv_func = np.arccos
    elif func == np.sin:
        inv_func = np.arcsin
    elif func == one_over_x:
        inv_func = one_over_x_inv
    elif func == signval_log_abs_val:
        inv_func = signval_log_abs_val_inv
    elif func == log_val_plus_one:
        inv_func = log_val_plus_one_inv
    else:
        raise ValueError(f'No inverse function mapping found for function {func}')
    return inv_func

def inverse_traj_func_derivative(traj_func, val):
    """
    If f is the trajectory function, this returns 1/f'(val)
    """
    if traj_func == identity:
        inverse_traj_func_derivative = 1
    elif traj_func == np.log:
        inverse_traj_func_derivative = val
    elif traj_func == np.cos:
        inverse_traj_func_derivative = -1 / np.sin(val)
    elif traj_func == np.sin:
        inverse_traj_func_derivative = 1 / np.cos(val)
    elif traj_func == one_over_x:
        inverse_traj_func_derivative = val**2
    elif traj_func == signval_log_abs_val:
        inverse_traj_func_derivative = val
    elif traj_func == log_val_plus_one:
        inverse_traj_func_derivative = val + 1
    else:
        raise ValueError(f'traj_func = {traj_func.__name__} but dparam/d{traj_func.__name__}(param) has not been implemented yet.')
    return inverse_traj_func_derivative

TRAJECTORY_FUNCTIONS = {
    'cosine': np.cos,
    'cosinus': np.cos,
    'cos': np.cos,
    'sine': np.sin,
    'sinus': np.sin,
    'sin': np.sin,
    'log': np.log,
    'logarithm': np.log,
    'none': identity,
    'identity': identity,
    'one_over_x': one_over_x,
    'signval_log_abs_val': signval_log_abs_val,
    'log_val_plus_one': log_val_plus_one
}

TRAJECTORY_INVERSE_FUNCTIONS = {
    'cosine': np.arccos,
    'cosinus': np.arccos,
    'cos': np.arccos,
    'sine': np.arcsin,
    'sinus': np.arcsin,
    'sin': np.arcsin,
    'log': np.exp,
    'logarithm': np.exp,
    'none': identity,
    'identity': identity
}


def jacobian_mc_mu_to_m1_m2_from_mc_m1_m2(chirp_mass, mass_1, mass_2):
    jacobian = 5/2 * (mass_1 + mass_2) ** 2 / (chirp_mass * (mass_1 - mass_2))
    return jacobian

def jacobian_mc_mu_to_m1_m2_from_mc_mu(chirp_mass, reduced_mass):
    eta = paru.Mc_mu_to_eta(chirp_mass, reduced_mass)
    return jacobian_mc_mu_to_m1_m2_from_eta(eta)

def jacobian_mc_mu_to_m1_m2_from_eta(eta):
    jacobian = 5/4 * eta**(-3/5) * (1/4 - eta)**(-1/2)
    return jacobian

def jacobian_traj_param_to_param(traj_func, val):
    return abs(inverse_traj_func_derivative(traj_func, val))


class TrajPrior(bcp.base.Prior):
    def __init__(self, name, prior, traj_func, latex_label=None, unit=None, minimum=-np.inf, maximum=np.inf, check_range_nonzero=True, boundary=None):
        super(TrajPrior, self).__init__(name, latex_label, unit, minimum, maximum, check_range_nonzero, boundary)
        self.prior = prior
        self.traj_func = traj_func
        self.set_min_and_max()
        self.boundary = 'reflective'

    def set_min_and_max(self):
        if self.traj_func in [one_over_x, signval_log_abs_val]:
            self.minimum = -np.inf
            self.maximum = np.inf
        else:
            traj_func_of_min = self.traj_func(self.prior.minimum)
            traj_func_of_max = self.traj_func(self.prior.maximum)
            self.minimum = min([traj_func_of_min, traj_func_of_max])
            self.maximum = max([traj_func_of_min, traj_func_of_max])

    def _prob(self, val):
        return self.prior.prob(val) * jacobian_traj_param_to_param(self.traj_func, val)

    def _ln_prob(self, val):
        return self.prior.ln_prob(val) + np.log(jacobian_traj_param_to_param(self.traj_func, val))

    def prob(self, parameters_dict):
        val = parameters_dict[self.prior.name]
        return self._prob(val)

    def ln_prob(self, parameters_dict):
        val = parameters_dict[self.prior.name]
        return self._ln_prob(val)

class McMuCombinedPriorFromComponentMasses(object):
    def __init__(self, prior_mc, prior_mu, prior_mass_1, prior_mass_2):
        self.name = 'chirp_mass_and_reduced_mass'
        self.prior_mc = prior_mc
        self.prior_mu = prior_mu
        self.prior_mass_1 = prior_mass_1
        self.prior_mass_2 = prior_mass_2
        if type(prior_mc) != bcp.base.Constraint or type(prior_mu) != bcp.base.Constraint:
            raise ValueError(f'Priors on chirp_mass and reduced_mass must be defined as Constraint priors.')

    def _prob(self, chirp_mass, reduced_mass):
        pi_mc = self.prior_mc.prob(chirp_mass) # Returns 0 or 1 since it's a constraint
        pi_mu = self.prior_mu.prob(reduced_mass) # Returns 0 or 1 since it's a constraint
        mass_1, mass_2 = paru.Mc_and_mu_to_m1_and_m2(chirp_mass, reduced_mass)
        pi_1 = self.prior_mass_1.prob(mass_1)
        pi_2 = self.prior_mass_2.prob(mass_2)
        jacobian = jacobian_mc_mu_to_m1_m2_from_mc_m1_m2(chirp_mass, mass_1, mass_2)
        return pi_mc * pi_mu * pi_1 * pi_2 * jacobian

    def _ln_prob(self, chirp_mass, reduced_mass):
        ln_pi_mc = self.prior_mc.ln_prob(chirp_mass) # Returns -inf or 0 since it's a constraint
        ln_pi_mu = self.prior_mu.ln_prob(reduced_mass) # Returns -inf or 0 since it's a constraint
        mass_1, mass_2 = paru.Mc_and_mu_to_m1_and_m2(chirp_mass, reduced_mass)
        ln_pi_1 = self.prior_mass_1.ln_prob(mass_1)
        ln_pi_2 = self.prior_mass_2.ln_prob(mass_2)
        ln_jacobian = np.log(jacobian_mc_mu_to_m1_m2_from_mc_m1_m2(chirp_mass, mass_1, mass_2))
        return ln_pi_mc + ln_pi_mu + ln_pi_1 + ln_pi_2 + ln_jacobian

    def prob(self, parameters_dict):
        chirp_mass = parameters_dict['chirp_mass']
        reduced_mass = parameters_dict['reduced_mass']
        return self._prob(chirp_mass, reduced_mass)

    def ln_prob(self, parameters_dict):
        chirp_mass = parameters_dict['chirp_mass']
        reduced_mass = parameters_dict['reduced_mass']
        return self._ln_prob(chirp_mass, reduced_mass)

class TrajCombinedPriorFromComponentMasses(object):
    def __init__(self, name, mc_mu_combined_prior, traj_func_mc, traj_func_mu):
        self.name = name
        self.mc_mu_combined_prior = mc_mu_combined_prior
        self.traj_func_mc = traj_func_mc
        self.traj_func_mu = traj_func_mu

    def set_min_and_max(self):
        traj_mc_min_and_max = self.traj_func([self.mc_mu_combined_prior.prior_mc.minimum, self.mc_mu_combined_prior.prior_mc.maximum])
        self.minimum = min(traj_min_and_max)
        self.maximum = max(traj_min_and_max)

    def _prob(self, chirp_mass, reduced_mass):
        pi_mc_mu = self.mc_mu_combined_prior._prob(chirp_mass, reduced_mass)
        traj_jacobian_mc = jacobian_traj_param_to_param(self.traj_func_mc, chirp_mass)
        traj_jacobian_mu = jacobian_traj_param_to_param(self.traj_func_mu, reduced_mass)
        return pi_mc_mu * traj_jacobian_mc * traj_jacobian_mu

    def _ln_prob(self, chirp_mass, reduced_mass):
        ln_pi_mc_mu = self.mc_mu_combined_prior._ln_prob(chirp_mass, reduced_mass)
        ln_traj_jacobian_mc = np.log(jacobian_traj_param_to_param(self.traj_func_mc, chirp_mass))
        ln_traj_jacobian_mu = np.log(jacobian_traj_param_to_param(self.traj_func_mu, reduced_mass))
        return ln_pi_mc_mu + ln_traj_jacobian_mc + ln_traj_jacobian_mu

    def prob(self, parameters_dict):
        chirp_mass = parameters_dict['chirp_mass']
        reduced_mass = parameters_dict['reduced_mass']
        return self._prob(chirp_mass, reduced_mass)

    def ln_prob(self, parameters_dict):
        chirp_mass = parameters_dict['chirp_mass']
        reduced_mass = parameters_dict['reduced_mass']
        return self._ln_prob(chirp_mass, reduced_mass)

class GWTrajPriors(dict):
    def __init__(self, priors, search_parameter_keys, dict_trajectory_functions_str):
        self.priors = priors
        self.search_parameter_keys = search_parameter_keys
        self.search_trajectory_functions = self.get_trajectory_functions(dict_trajectory_functions_str)
        self.search_trajectory_parameters_keys = self.get_trajectory_parameters_keys()
        self.search_trajectory_inv_functions = self.get_trajectory_inverse_functions()
        # self.start_time = start_time
        for idx, key in enumerate(search_parameter_keys):
            traj_key = self.search_trajectory_parameters_keys[idx]
            traj_func = self.search_trajectory_functions[idx]
            self[traj_key] = TrajPrior(traj_key, priors[key], traj_func)
        if 'chirp_mass' in search_parameter_keys and 'reduced_mass' in search_parameter_keys:
            idx_mc = search_parameter_keys.index('chirp_mass')
            idx_mu = search_parameter_keys.index('reduced_mass')
            traj_func_mc = self.search_trajectory_functions[idx_mc]
            traj_func_mu = self.search_trajectory_functions[idx_mu]
            traj_key_mc = self.search_trajectory_parameters_keys[idx_mc]
            traj_key_mu = self.search_trajectory_parameters_keys[idx_mu]
            combined_key = traj_key_mc + '_and_' + traj_key_mu
            mc_mu_combined_prior = McMuCombinedPriorFromComponentMasses(priors['chirp_mass'], priors['reduced_mass'], priors['mass_1'], priors['mass_2'])
            self[combined_key] = TrajCombinedPriorFromComponentMasses(combined_key, mc_mu_combined_prior, traj_func_mc, traj_func_mu)
            self.combined_key = combined_key
        else:
            self.combined_key = ''

    def prob(self, parameters_dict):
        res = 1
        for traj_key in self:
            res *= self[traj_key].prob(parameters_dict)
        return res

    def ln_prob(self, parameters_dict):
        res = 0
        for traj_key in self:
            # if traj_key in ['chi_1', 'chi_2']:
            #     pass
            # else:
            #     res += self[traj_key].ln_prob(parameters_dict)
            res += self[traj_key].ln_prob(parameters_dict)
        return res

    def get_trajectory_parameters_keys(self):
        search_trajectory_parameters_keys = []
        for idx, key in enumerate(self.search_parameter_keys):
            traj_func = self.search_trajectory_functions[idx]
            if traj_func == identity:
                prefix = ''
            else:
                prefix = traj_func.__name__ + '_'
            search_trajectory_parameters_keys.append(f'{prefix}{key}')
        return search_trajectory_parameters_keys

    def get_trajectory_functions(self, dict_trajectory_functions_str):
        search_trajectory_functions = []
        for key in self.search_parameter_keys:
            if key not in dict_trajectory_functions_str.keys():
                func_name = 'none'
            else:
                func_name = dict_trajectory_functions_str[key]
            traj_func = TRAJECTORY_FUNCTIONS[func_name.lower()]
            search_trajectory_functions.append(traj_func)
        return search_trajectory_functions

    def get_trajectory_inverse_functions(self):
        search_trajectory_inv_functions = []
        for traj_func in self.search_trajectory_functions:
            traj_inv_func = map_func_to_inv_func(traj_func)
            search_trajectory_inv_functions.append(traj_inv_func)
        return search_trajectory_inv_functions







def set_chirp_mass_min_max(priors):
    # These two lines take into account in bilby if 'chirp_mass'
    # is already part of priors
    mc_min = priors.minimum_chirp_mass
    mc_max = priors.maximum_chirp_mass
    priors['chirp_mass'] = bcp.base.Constraint(name='chirp_mass', minimum=mc_min, maximum=mc_max, unit='$M_{\odot}$')
    return priors

def set_reduced_mass_min_max(priors):
    eta_max = 0.25
    eta_min =  priors['mass_1'].maximum * priors['mass_2'].minimum / (priors['mass_1'].maximum + priors['mass_2'].minimum)**2
    mu_max = priors.maximum_chirp_mass * eta_max**(2/5)
    mu_min = priors.minimum_chirp_mass * eta_min**(2/5)
    # mu_max = priors['mass_1'].maximum / 2
    # mu_min = priors['mass_2'].minimum / 2
    if 'reduced_mass' not in priors.keys():
        priors['reduced_mass'] = bcp.base.Constraint(name='reduced_mass', minimum=mu_min, maximum=mu_max, unit='$M_{\odot}$')
    return priors

def complete_priors_geocent_time_and_duration(priors, geocent_time_start, start_time):
    # Set boundaries for geocent_time prior
    deltaT = 0.1
    priors['geocent_time'].minimum = geocent_time_start - deltaT / 2
    priors['geocent_time'].maximum = geocent_time_start + deltaT / 2

    # Create prior on geocent_duration
    priors['geocent_duration'] = priors['geocent_time'].__class__(minimum=priors['geocent_time'].minimum - start_time, maximum=priors['geocent_time'].maximum - start_time)
    priors['geocent_duration'].name = 'geocent_duration'
    priors['geocent_duration'].latex_label = '$\\delta t_c$'
    priors['geocent_duration'].unit = 's'
    priors['geocent_duration'].boundary = 'reflective'
    return priors

def reset_aligned_spin_boundaries(priors):
    """
    If aligned spin priors are defined with the "zprior", its log gradient diverge
    at the boundaries (0.05 for low spin, 0.89 for high spins). As a result we reset
    the boundary to 99% of the maximum value so that the hamiltonian trajectories will
    bounce right before the divergence.
    """
    for key in priors.keys():
        if priors[key].__class__ == bgp.AlignedSpin:
            priors[key].maximum = 0.99 * priors[key].maximum
            priors[key].minimum = 0.99 * priors[key].minimum

def fill_in_input_priors(priors, geocent_time_start, start_time):
    set_chirp_mass_min_max(priors)
    set_reduced_mass_min_max(priors)
    complete_priors_geocent_time_and_duration(priors, geocent_time_start, start_time)
    reset_aligned_spin_boundaries(priors)
    return priors

# METHODS TO COMPUTE WEIGHTS AND SAMPLE FROM THE ORIGINAL DISTRIBUTION
# ACCORDING TO THOSE WEIGHTS, LEAVING THE VALUES OF THE SAMPLES UNCHANGED

def get_weights_by_new_prior(posterior, old_priors, new_priors, prior_names=None):
    """ Calculate a list of sample weights based on the ratio of new to old priors.
        This is a rewritten version of bilby's same name method; this one being clearer,
        faster and gives the same results.

        Parameters
        ----------
        posterior: pd.DataFrame
            The posterior samples with keys corresponding to that in old_priors and new_priors
        old_priors: PriorDict,
            The prior used in the generation of the original samples.

        new_priors: PriorDict,
            The prior to use to reweight the samples.

        prior_names: list
            A list of the priors to include in the ratio during reweighting.

        Returns
        -------
        weights: array-like,
            A list of sample weights.

            """
    # Shared priors - these will form a ratio
    if prior_names is not None:
        shared_parameters_keys = [key for key in new_priors if
                             key in old_priors and key in prior_names]
    else:
        shared_parameters_keys = [key for key in new_priors if key in old_priors]

    weights = np.ones(len(posterior))
    for prior_key in shared_parameters_keys:
        weights *= new_priors[prior_key].prob(posterior[prior_key]) / old_priors[prior_key].prob(posterior[prior_key])

    return weights

def add_m1m2_columns_to_posterior_in_Mc_mu(posterior):
        """
        Returns a posterior with a 'mass_1' and a 'mass_2' column computed
        from the 'chirp_mass' and 'reduced_mass' existing.
        Insertion is at index right after the two existing ones.

        Parameters:
            posterior: pd.DataFrame
                must contain at least a 'chirp_mass' and a 'reduced_mass' column
        """
        posterior_new = posterior.copy()
        _, _, mass_1, mass_2 = paru.Mc_and_mu_to_M_eta_m1_and_m2(posterior_new['chirp_mass'], posterior_new['reduced_mass'])
        idx_insert = max(
            posterior_new.keys().tolist().index('chirp_mass'),
            posterior_new.keys().tolist().index('reduced_mass')
        ) + 1
        posterior_new.insert(loc=idx_insert, column='mass_1', value=mass_1)
        posterior_new.insert(loc=idx_insert + 1, column='mass_2', value=mass_2)
        return posterior_new

def convert_Mc_mu_to_flat_in_component_mass_prior(posterior, old_priors, new_priors, fraction=None):
    """
    Basically a copy-paste of bilby's `bilby.gw.prior.convert_to_flat_in_component_mass_prior()` but
    using a different jacobian since we use the reduced mass `mu` and not the mass_ratio `q`.
    Converts samples with a defined prior in chirp-mass and reduced-mass to flat in component mass
    by resampling with the posterior with weights defined as ratio in new:old prior values times the
    jacobian which for F(Mc, mu) -> G(m1, m2) is defined as
    J := 5/2 * (m1 + m2)**2 / (Mc * (m1 - m2))

    Parameters
    ----------
    result: bilby.core.result.Result
        The output result complete with priors and posteriors
    fraction: float [0, 1]
        The fraction of samples to draw (default=0.25). Note, if too high a
        fraction of samples are draw, the reweighting will not be applied in
        effect.
    """
    if True:
    # if getattr(self, "priors") is not None:
        for key in ['chirp_mass', 'reduced_mass']:
            if key not in old_priors.keys():
                bilby.gw.prior.BilbyPriorConversionError("{} Prior not found in result object".format(key))
            if isinstance(old_priors[key], bilby.gw.prior.Constraint):
                bilby.gw.prior.BilbyPriorConversionError("{} Prior should not be a Constraint".format(key))
            # if key not in new_priors.keys():
            #     bilby.gw.prior.BilbyPriorConversionError("{} Prior not found in the list of new priors asked".format(key))
            # if not isinstance(new_priors[key], bilby.gw.prior.Constraint):
            #     bilby.gw.prior.BilbyPriorConversionError("{} Prior should be a Constraint for the new priors".format(key))
        for key in ['mass_1', 'mass_2']:
            if not isinstance(old_priors[key], bilby.gw.prior.Constraint):
                bilby.gw.prior.BilbyPriorConversionError("{} Prior should be a Constraint Prior".format(key))
            if isinstance(new_priors[key], bilby.gw.prior.Constraint):
                bilby.gw.prior.BilbyPriorConversionError("{} Prior should not be a Constraint for the new priors".format(key))
    else:
        bilby.gw.prior.BilbyPriorConversionError("No prior in the result: unable to convert")

    for key in ['chirp_mass', 'reduced_mass']:
        new_priors[key] = bilby.gw.prior.Constraint(old_priors[key].minimum, old_priors[key].maximum, key, latex_label=old_priors[key].latex_label)
        new_priors[key].minimum = old_priors[key].minimum
        new_priors[key].maximum = old_priors[key].maximum
    for key in ['mass_1', 'mass_2']:
        # new_priors[key] = bilby.gw.prior.Uniform(old_priors[key].minimum, old_priors[key].maximum, key, latex_label=old_priors[key].latex_label, unit="$M_{\odot}$")
        if new_priors[key].minimum != old_priors[key].minimum:
            new_priors[key].minimum = old_priors[key].minimum
            cut.logger.info(f'{key} minimum value was not consistent between old and new prior. Setting it to old value = {old_priors[key].minimum}')
        if new_priors[key].maximum != old_priors[key].maximum:
            new_priors[key].maximum = old_priors[key].maximum
            cut.logger.info(f'{key} maximum value was not consistent between old and new prior. Setting it to old value = {old_priors[key].maximum}')


    posterior_temp = add_m1m2_columns_to_posterior_in_Mc_mu(posterior)

    mass_param_keys = ['chirp_mass', 'reduced_mass', 'mass_1', 'mass_2']
    weights_masses = get_weights_by_new_prior(posterior_temp, old_priors, new_priors, prior_names=mass_param_keys)
    jacobian = jacobian_mc_mu_to_m1_m2_from_mc_m1_m2(posterior_temp["chirp_mass"], posterior_temp["mass_1"], posterior_temp["mass_2"])
    weights_masses *= jacobian

    other_params = posterior_temp.keys().tolist()
    for mass_key in mass_param_keys:
        other_params.remove(mass_key)
    weights_other_params = get_weights_by_new_prior(posterior_temp, old_priors, new_priors, prior_names=other_params)

    weights = weights_masses * weights_other_params

    cut.logger.info("Resampling posterior to flat-in-component mass")
    effective_sample_size = sum(weights)**2 / sum(weights**2)
    n_posterior = len(posterior)
    if fraction is None:
        fraction = effective_sample_size / n_posterior
    if fraction > effective_sample_size / n_posterior:
        cut.logger.warning(
            "Sampling posterior of length {} with fraction {}, but "
            "effective_sample_size / len(posterior) = {}. This may produce "
            "biased results"
            .format(n_posterior, fraction, effective_sample_size / n_posterior)
        )

    posterior_reweighted = posterior_temp.sample(frac=fraction, weights=weights, replace=True)

    return posterior_reweighted


# METHODS TO COMPUTE WEIGHTS AND MODIFY THE VALUES OF THE ORIGINAL SAMPLES

def get_weights_by_new_prior_new(posterior, old_priors, new_priors, prior_names=None):
    """ Calculate a list of sample weights based on the ratio of new to old priors.
        This is a rewritten version of bilby's same name method; this one being clearer,
        faster and gives the same results.

        Parameters
        ----------
        posterior: pd.DataFrame
            The posterior samples with keys corresponding to that in old_priors and new_priors
        old_priors: PriorDict,
            The prior used in the generation of the original samples.

        new_priors: PriorDict,
            The prior to use to reweight the samples.

        prior_names: list
            A list of the priors to include in the ratio during reweighting.

        Returns
        -------
        weights: array-like,
            A list of sample weights.

            """
    # Shared priors - these will form a ratio
    posterior_keys = posterior.keys().tolist()
    if prior_names is not None:
        shared_parameters_keys = [key for key in new_priors if
                             key in old_priors and key in posterior_keys and key in prior_names]
    else:
        shared_parameters_keys = [key for key in new_priors if key in old_priors and key in posterior_keys]

    weights = np.ones_like(posterior)
    weights = pd.DataFrame(weights, index=posterior.index, columns=posterior.columns)
    for prior_key in shared_parameters_keys:
        print(prior_key)
        weights[prior_key] = new_priors[prior_key].prob(posterior[prior_key]) / old_priors[prior_key].prob(posterior[prior_key])

    return weights
