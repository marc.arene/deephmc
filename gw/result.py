import numpy as np
import pandas as pd
import bilby
import sys
sys.path.append('../')
import Library.param_utils as paru
import Library.CONST as CONST
import core.utils as cut
import copy
import gw.prior as gwprior

def get_weights_by_new_prior_bilby(posterior, old_priors, new_priors, prior_names=None):
    """ Calculate a list of sample weights based on the ratio of new to old priors

        Parameters
        ----------
        posterior: pd.DataFrame
            The posterior samples with keys corresponding to that in old_priors and new_priors
        old_priors: PriorDict,
            The prior used in the generation of the original samples.

        new_priors: PriorDict,
            The prior to use to reweight the samples.

        prior_names: list
            A list of the priors to include in the ratio during reweighting.

        Returns
        -------
        weights: array-like,
            A list of sample weights.

            """
    weights = []

    # Shared priors - these will form a ratio
    if prior_names is not None:
        shared_parameters = {key: posterior[key] for key in new_priors if
                             key in old_priors and key in prior_names}
    else:
        shared_parameters = {key: posterior[key] for key in new_priors if key in old_priors}

    parameters = [{key: posterior[key][i] for key in shared_parameters.keys()}
                  for i in range(len(posterior))]

    for i in range(len(posterior)):
        weight = 1
        for prior_key in shared_parameters.keys():
            val = posterior[prior_key][i]
            # weight *= new_priors.evaluate_constraints(parameters[i])
            weight *= evaluate_constraints(new_priors, parameters[i])
            weight *= new_priors[prior_key].prob(val) / old_priors[prior_key].prob(val)
        weights.append(weight)

    return weights

def evaluate_constraints(priors, sample):
    """
    Using this function instead of that defined in bilby's PriorDict class
    because the conversion line doesn't handle the definition of `reduced_mass`
    as a mass parameter.
    So I just skip it here for the moment.
    The function causing the problem is:
    `bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters()`
    """
    # out_sample = self.conversion_function(sample)
    out_sample = sample
    prob = 1
    for key in priors:
        if isinstance(priors[key], bilby.gw.prior.Constraint) and key in out_sample:
            prob *= priors[key].prob(out_sample[key])
    return prob

def get_weights_by_new_prior(posterior, old_priors, new_priors, prior_names=None):
    """ Calculate a list of sample weights based on the ratio of new to old priors.
        This is a rewritten version of bilby's same name method; this one being clearer,
        faster and gives the same results.

        Parameters
        ----------
        posterior: pd.DataFrame
            The posterior samples with keys corresponding to that in old_priors and new_priors
        old_priors: PriorDict,
            The prior used in the generation of the original samples.

        new_priors: PriorDict,
            The prior to use to reweight the samples.

        prior_names: list
            A list of the priors to include in the ratio during reweighting.

        Returns
        -------
        weights: array-like,
            A list of sample weights.

            """
    # Shared priors - these will form a ratio
    if prior_names is not None:
        shared_parameters_keys = [key for key in new_priors if
                             key in old_priors and key in prior_names]
    else:
        shared_parameters_keys = [key for key in new_priors if key in old_priors]

    weights = np.ones(len(posterior))
    for prior_key in shared_parameters_keys:
        weights *= new_priors[prior_key].prob(posterior[prior_key]) / old_priors[prior_key].prob(posterior[prior_key])

    return weights


class Result(bilby.core.result.Result):
    def __init__(self, **kwargs):
        """
        A wrapper around Bilby's Result class
        """
        super(Result, self).__init__(**kwargs)

    def convert_Mc_q_to_flat_in_component_mass_prior(self, fraction=0.25):
        return bilby.gw.prior.convert_to_flat_in_component_mass_prior(self)

    def convert_Mc_mu_to_flat_in_component_mass_prior_old(self, new_priors, fraction=None):
        """
        Basically a copy-paste of bilby's `bilby.gw.prior.convert_to_flat_in_component_mass_prior()` but
        using a different jacobian since we use the reduced mass `mu` and not the mass_ratio `q`.
        Converts samples with a defined prior in chirp-mass and reduced-mass to flat in component mass
        by resampling with the posterior with weights defined as ratio in new:old prior values times the
        jacobian which for F(Mc, mu) -> G(m1, m2) is defined as
        J := 5/2 * (m1 + m2)**2 / (Mc * (m1 - m2))

        Parameters
        ----------
        result: bilby.core.result.Result
            The output result complete with priors and posteriors
        fraction: float [0, 1]
            The fraction of samples to draw (default=0.25). Note, if too high a
            fraction of samples are draw, the reweighting will not be applied in
            effect.
        """
        if getattr(self, "priors") is not None:
            for key in ['chirp_mass', 'reduced_mass']:
                if key not in self.priors.keys():
                    bilby.gw.prior.BilbyPriorConversionError("{} Prior not found in result object".format(key))
                if isinstance(self.priors[key], bilby.gw.prior.Constraint):
                    bilby.gw.prior.BilbyPriorConversionError("{} Prior should not be a Constraint".format(key))
                # if key not in new_priors.keys():
                #     bilby.gw.prior.BilbyPriorConversionError("{} Prior not found in the list of new priors asked".format(key))
                # if not isinstance(new_priors[key], bilby.gw.prior.Constraint):
                #     bilby.gw.prior.BilbyPriorConversionError("{} Prior should be a Constraint for the new priors".format(key))
            for key in ['mass_1', 'mass_2']:
                if not isinstance(self.priors[key], bilby.gw.prior.Constraint):
                    bilby.gw.prior.BilbyPriorConversionError("{} Prior should be a Constraint Prior".format(key))
                if isinstance(new_priors[key], bilby.gw.prior.Constraint):
                    bilby.gw.prior.BilbyPriorConversionError("{} Prior should not be a Constraint for the new priors".format(key))
        else:
            bilby.gw.prior.BilbyPriorConversionError("No prior in the result: unable to convert")

        for key in ['chirp_mass', 'reduced_mass']:
            new_priors[key] = bilby.gw.prior.Constraint(self.priors[key].minimum, self.priors[key].maximum, key, latex_label=self.priors[key].latex_label)
            new_priors[key].minimum = self.priors[key].minimum
            new_priors[key].maximum = self.priors[key].maximum
        for key in ['mass_1', 'mass_2']:
            # new_priors[key] = bilby.gw.prior.Uniform(old_priors[key].minimum, old_priors[key].maximum, key, latex_label=old_priors[key].latex_label, unit="$M_{\odot}$")
            if new_priors[key].minimum != self.priors[key].minimum:
                new_priors[key].minimum = self.priors[key].minimum
                cut.logger.info(f'{key} minimum value was not consistent between old and new prior. Setting it to old value = {self.priors[key].minimum}')
            if new_priors[key].maximum != self.priors[key].maximum:
                new_priors[key].maximum = self.priors[key].maximum
                cut.logger.info(f'{key} maximum value was not consistent between old and new prior. Setting it to old value = {self.priors[key].maximum}')


        posterior_temp = gwprior.add_m1m2_columns_to_posterior_in_Mc_mu(self.posterior)

        mass_param_keys = ['chirp_mass', 'reduced_mass', 'mass_1', 'mass_2']
        weights_masses = get_weights_by_new_prior(posterior_temp, self.priors, new_priors, prior_names=mass_param_keys)
        jacobian = 5/2 * (posterior_temp["mass_1"] + posterior_temp["mass_2"]) ** 2 / (posterior_temp["chirp_mass"] * (posterior_temp["mass_1"] - posterior_temp["mass_2"]))
        weights_masses *= jacobian

        other_params = posterior_temp.keys().tolist()
        for mass_key in mass_param_keys:
            other_params.remove(mass_key)
        weights_other_params = get_weights_by_new_prior(posterior_temp, self.priors, new_priors, prior_names=other_params)

        weights = weights_masses * weights_other_params

        cut.logger.info("Resampling posterior to flat-in-component mass")
        effective_sample_size = sum(weights)**2 / sum(weights**2)
        n_posterior = len(self.posterior)
        if fraction is None:
            fraction = effective_sample_size / n_posterior
        if fraction > effective_sample_size / n_posterior:
            cut.logger.warning(
                "Sampling posterior of length {} with fraction {}, but "
                "effective_sample_size / len(posterior) = {}. This may produce "
                "biased results"
                .format(n_posterior, fraction, effective_sample_size / n_posterior)
            )

        posterior_reweighted = posterior_temp.sample(frac=fraction, weights=weights, replace=True)
        result_new = Result(
            search_parameter_keys=posterior_reweighted.keys().tolist(),
            priors=new_priors,
            posterior=posterior_reweighted,
            meta_data={}
        )
        result_new.meta_data["reweighted_to_flat_in_component_mass"] = True
        return result_new

    def get_result_convert_Mc_mu_to_flat_in_component_mass_prior(self, new_priors, fraction=None):
        posterior_reweighted = gwprior.convert_Mc_mu_to_flat_in_component_mass_prior(self.posterior, self.priors, new_priors, fraction=fraction)
        result_new = Result(
            search_parameter_keys=posterior_reweighted.keys().tolist(),
            priors=new_priors,
            posterior=posterior_reweighted,
            meta_data={}
        )
        result_new.meta_data["reweighted_to_flat_in_component_mass"] = True
        return result_new

    def plot_corner_in_component_masses(self, truth_dict=None, priors=True, titles=True, save=True, filename=None, outdir=None, **kwargs):
        if 'mass_1' not in self.posterior.keys() or 'mass_2' not in self.posterior.keys():
            self.posterior = gwprior.add_m1m2_columns_to_posterior_in_Mc_mu(self.posterior)

        if truth_dict is None:
            plot_parameters = self.posterior.keys().tolist()
            plot_parameters.remove('chirp_mass')
            plot_parameters.remove('reduced_mass')
            latex_labels = CONST.get_latex_labels(plot_parameters)
        else:
            plot_parameters = {}
            for key in self.posterior.keys():
                if key not in ['chirp_mass', 'reduced_mass']:
                    plot_parameters[key] = truth_dict[key]
            latex_labels = CONST.get_latex_labels(plot_parameters.keys())
        if save:
            if filename is None and outdir is not None:
                filename = f'{outdir}/{len(self.posterior)}_corner.png'
            elif filename is None and outdir is None:
                raise ValueError(f'Specify either filename or outdir for plot creation')
        self.plot_corner(parameters=plot_parameters, priors=priors, titles=titles, save=save, filename=filename, labels=latex_labels, **kwargs)

    def plot_corner_not_in_component_masses(self, truth_dict=None, priors=None, titles=True, save=True, filename=None, outdir=None, **kwargs):
        comp_masses_present = 'mass_1' in self.posterior.keys() and 'mass_2' in self.posterior.keys()

        if truth_dict is None:
            plot_parameters = self.posterior.keys().tolist()
            if comp_masses_present:
                plot_parameters.pop('mass_1')
                plot_parameters.pop('mass_2')
        else:
            plot_parameters = {}
            for key in self.posterior.keys():
                if key not in ['mass_1', 'mass_2']:
                    plot_parameters[key] = truth_dict[key]

        if save:
            if filename is None and outdir is not None:
                filename = f'{outdir}/{len(self.posterior)}_corner.png'
            elif filename is None and outdir is None:
                raise ValueError(f'Specify either filename or outdir for plot creation')

        latex_labels = CONST.get_latex_labels(plot_parameters.keys())
        self.plot_corner(parameters=plot_parameters, priors=priors, titles=titles, save=save, filename=filename, labels=latex_labels, **kwargs)
