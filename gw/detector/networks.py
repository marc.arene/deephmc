import numpy as np
from bilby.gw.detector.networks import InterferometerList as BilbyInterferometerList
from .interferometer import Interferometer as GwhmcInterferometer
import core.utils as cut

class InterferometerList(BilbyInterferometerList):
    """
    Wrapper of bilby's class: InterferomerterList, allowing to use gwhmc's class: Interferomerter, which wraps itself bilby's class: Interferomerter.
    """

    def __init__(self, interferometers):
        bilby_ifo_list = BilbyInterferometerList(interferometers)
        # At this point, `self` is a list of bilby Interferometer
        # For each of these, the loop swaps them with an equivalent gwhmc Interferomter
        # instantiated from the bilby one
        for bilby_ifo in bilby_ifo_list:
            init_params = {'name': bilby_ifo.name, 'power_spectral_density': bilby_ifo.power_spectral_density, 'minimum_frequency': bilby_ifo.minimum_frequency, 'maximum_frequency': bilby_ifo.maximum_frequency, 'length': bilby_ifo.length, 'latitude': bilby_ifo.latitude, 'longitude': bilby_ifo.longitude, 'elevation': bilby_ifo.elevation, 'xarm_azimuth': bilby_ifo.xarm_azimuth, 'yarm_azimuth': bilby_ifo.yarm_azimuth, 'xarm_tilt': bilby_ifo.xarm_tilt, 'yarm_tilt': bilby_ifo.yarm_tilt, 'calibration_model': bilby_ifo.calibration_model}
            gwhmc_ifo = GwhmcInterferometer(**init_params)
            # gwhmc_ifo = GwhmcInterferometer(bilby_ifo.name, bilby_ifo.power_spectral_density, bilby_ifo.minimum_frequency, bilby_ifo.maximum_frequency, bilby_ifo.length, bilby_ifo.latitude, bilby_ifo.longitude, bilby_ifo.elevation, bilby_ifo.xarm_azimuth, bilby_ifo.yarm_azimuth, bilby_ifo.xarm_tilt, bilby_ifo.yarm_tilt, bilby_ifo.calibration_model)
            self.append(gwhmc_ifo)

    def optimal_snr(self):
        if self[0].meta_data == {}:
            raise ValueError("No meta data defined for the ifo of the network meaning no injection has been made. Cannot compute the optimal snr.")
        opt_snr_sqrd = 0
        for ifo in self:
            opt_snr_sqrd += ifo.meta_data['optimal_SNR']**2
        opt_snr = np.sqrt(opt_snr_sqrd)

        cut.logger.info("Network optimal SNR = {:.2f}".format(opt_snr))
        return opt_snr

    def matched_filter_snr(self):
        if self[0].meta_data == {}:
            raise ValueError("No meta data defined for the ifo of the network meaning no injection has been made. Cannot compute the mf snr.")
        mf_snr_sqrd = 0
        for ifo in self:
            mf_snr_sqrd += ifo.meta_data['matched_filter_SNR']**2
        mf_snr = np.sqrt(mf_snr_sqrd)

        cut.logger.info("Network matched filter SNR = {:.2f}".format(mf_snr))
        return mf_snr

    def log_likelihood(self):
        if self[0].meta_data == {}:
            raise ValueError("No meta data defined for the ifo of the network meaning no injection has been made. Cannot compute the log likelhood.")
        logL = 0
        for ifo in self:
            logL += ifo.meta_data['log_likelihood']

        cut.logger.info("Network log likelihood (no phase marginalization) = {:.2f}".format(logL))
        return logL
