import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np

from bilby.gw.detector.interferometer import Interferometer as BilbyInterferometer
from bilby.gw.utils import noise_weighted_inner_product as gwutilsnwip

import Library.param_utils as paru
import Library.bilby_utils as bilby_utils
import Library.CONST as CONST

class Interferometer(BilbyInterferometer):
    """

    """
    def __init__(self, name, power_spectral_density, minimum_frequency, maximum_frequency, length, latitude, longitude, elevation, xarm_azimuth, yarm_azimuth, xarm_tilt=0., yarm_tilt=0., calibration_model=None):
        BilbyInterferometer.__init__(self, name, power_spectral_density, minimum_frequency, maximum_frequency, length, latitude, longitude, elevation, xarm_azimuth, yarm_azimuth, xarm_tilt, yarm_tilt, calibration_model)
        self._power_spectral_density_array_masked_windowed = None
        self.frequency_domain_strain_masked = None

    def get_detector_response_geocent(self, waveform_polarizations, parameters):
        """ Get the detector response for a particular waveform before timeshifting it from geocent, ie it is the response of and identically oriented detector but located at the center of the earth.

        Parameters
        -------
        waveform_polarizations: dict
            polarizations of the waveform
        parameters: dict
            parameters describing position and time of arrival of the signal

        Returns
        -------
        array_like: A 3x3 array representation of the detector response (signal observed in the interferometer)
        """
        signal = {}
        for mode in waveform_polarizations.keys():
            det_response = self.antenna_response(
                parameters['ra'],
                parameters['dec'],
                parameters['geocent_time'],
                parameters['psi'], mode)

            signal[mode] = waveform_polarizations[mode] * det_response
        signal_ifo = sum(signal.values())

        # signal_ifo *= self.strain_data.frequency_mask

        return signal_ifo

    def get_fd_time_translation(self, parameters):

        time_shift = self.time_delay_from_geocenter(
            parameters['ra'], parameters['dec'], parameters['geocent_time'])
        # time_shift = 0
        # print(f'time_shift = {time_shift}')
        # Be careful to first substract the two GPS times which are ~1e9 sec.
        # And then add the time_shift which varies at ~1e-5 sec
        dt_geocent = parameters['geocent_time'] - self.strain_data.start_time
        dt = dt_geocent + time_shift

        two_pi_f_dt = -1j * 2 * np.pi * dt * self.strain_data.frequency_array[self.strain_data.frequency_mask]
        # # [~3ms]
        exp_two_pi_f_dt = np.exp(two_pi_f_dt)
        # print(f'   exp_two_pi_f_dt computed for {self.name}')

        return exp_two_pi_f_dt

    def get_detector_response(self, waveform_polarizations, parameters, exp_two_pi_f_dt=None):
        """ Get the detector response for a particular waveform. Same function as in
        `bilby.detector.get_detector_response()` but calling the above sub-functions. `exp_two_pi_f_dt` is added as an input for cases where it can be precomputed.
        Parameters
        -------
        waveform_polarizations: dict
            polarizations of the waveform
        parameters: dict
            parameters describing position and time of arrival of the signal

        Returns
        -------
        array_like: A 3x3 array representation of the detector response (signal observed in the interferometer)
        """
        # 1ms
        signal_ifo = self.get_detector_response_geocent(waveform_polarizations, parameters)
        if exp_two_pi_f_dt is None:
            # 3.8ms
            exp_two_pi_f_dt = self.get_fd_time_translation(parameters)

        # 0.8ms
        signal_ifo[self.strain_data.frequency_mask] = signal_ifo[self.strain_data.frequency_mask] * exp_two_pi_f_dt

        # 1.1ms
        # signal_ifo[self.strain_data.frequency_mask] *= self.calibration_model.get_calibration_factor(self.strain_data.frequency_array[self.strain_data.frequency_mask], prefix='recalib_{}_'.format(self.name), **parameters)

        return signal_ifo

    def noise_weighted_inner_product(self, aa, bb):
        """
        Allows to compute the nwip of any two signals in the interferometers. Existing methods in Bilby allow for <s|h> and <h|h> but we also need <h|dh> in the HMC.
        """
        return gwutilsnwip(
            aa=aa[self.strain_data.frequency_mask],
            bb=bb[self.strain_data.frequency_mask],
            power_spectral_density=self.power_spectral_density_array_masked_windowed,
            duration=self.strain_data.duration)

    @property
    def power_spectral_density_array_masked_windowed(self):
        """
        Pre-compute the masked and windowed psd array and cache it in this property to avoid unnecessary computations in the noise-weighted-inner-products.
        """
        if self._power_spectral_density_array_masked_windowed is None:
            self._power_spectral_density_array_masked_windowed = self.power_spectral_density_array[self.strain_data.frequency_mask]
        return self._power_spectral_density_array_masked_windowed

    def inner_product_mask_fixed(self, signal):
        """
        Same as Bilby's original `interferometer.inner_product()` method but using "pre-masked" arrays and "pre-masked-windowed" psd-array to save some computation time.
        Parameters
        ----------
        signal: array_like
            Array containing the signal

        Returns
        -------
        float: The optimal signal to noise ratio possible squared
        """
        if self.frequency_domain_strain_masked is None:
            self.frequency_domain_strain_masked = self.strain_data.frequency_domain_strain[self.strain_data.frequency_mask]

        return gwutilsnwip(
            aa=signal[self.strain_data.frequency_mask],
            bb=self.frequency_domain_strain_masked,
            power_spectral_density=self.power_spectral_density_array_masked_windowed,
            duration=self.strain_data.duration)
