import numpy as np
from bilby.gw.likelihood import GravitationalWaveTransient as BilbyLikelihood
from bilby.gw.likelihood import ROQGravitationalWaveTransient as ROQBilbyLikelihood
import core.utils as cut

class Likelihood(BilbyLikelihood):
    """
    Wrapper of bilby's class: GravitationalWaveTransient, allowing to add several methods such as a neat computation of the network snrs
    """
    def __init__(self, interferometers, waveform_generator,
                 time_marginalization=False, distance_marginalization=False,
                 phase_marginalization=False, priors=None,
                 distance_marginalization_lookup_table=None,
                 jitter_time=True):
        super(Likelihood, self).__init__(interferometers, waveform_generator, time_marginalization, distance_marginalization, phase_marginalization, priors, distance_marginalization_lookup_table, jitter_time)
        self._setup_phase_marginalization()
        self.snr_dict = {}

    def calculate_network_snrs(self):
        snr_dict = {}

        waveform_polarizations =\
            self.waveform_generator.frequency_domain_strain(self.parameters)

        if waveform_polarizations is None:
            return np.nan_to_num(-np.inf)

        d_inner_h_net = 0.
        optimal_snr_net_squared = 0.
        matched_filter_snr_net_squared = 0.
        if self.time_marginalization:
            if self.jitter_time:
                self.parameters['geocent_time'] += self.parameters['time_jitter']
            d_inner_h_tc_array = np.zeros(
                self.interferometers.frequency_array[0:-1].shape,
                dtype=np.complex128)

        for ifo in self.interferometers:
            per_detector_snr = self.calculate_snrs(
                waveform_polarizations=waveform_polarizations,
                interferometer=ifo)

            snr_dict[ifo.name] = {}
            snr_dict[ifo.name]['log_l_ratio'] = np.real(per_detector_snr.d_inner_h) - 0.5 * np.real(per_detector_snr.optimal_snr_squared)
            snr_dict[ifo.name]['optimal_snr'] = np.real(per_detector_snr.optimal_snr_squared)**0.5
            snr_dict[ifo.name]['complex_matched_filter_snr'] = per_detector_snr.complex_matched_filter_snr
            d_inner_h_phasemarg = self._bessel_function_interped(abs(per_detector_snr.d_inner_h))
            snr_dict[ifo.name]['matched_filter_snr_phasemarg'] = d_inner_h_phasemarg / snr_dict[ifo.name]['optimal_snr']

            d_inner_h_net += per_detector_snr.d_inner_h
            optimal_snr_net_squared += np.real(per_detector_snr.optimal_snr_squared)
            matched_filter_snr_net_squared += np.real(per_detector_snr.complex_matched_filter_snr)**2


            if self.time_marginalization:
                d_inner_h_tc_array += per_detector_snr.d_inner_h_squared_tc_array

        if self.time_marginalization:
            log_l = self.time_marginalized_likelihood(
                d_inner_h_tc_array=d_inner_h_tc_array,
                h_inner_h=optimal_snr_net_squared)
            if self.jitter_time:
                self.parameters['geocent_time'] -= self.parameters['time_jitter']

        elif self.distance_marginalization:
            log_l = self.distance_marginalized_likelihood(
                d_inner_h=d_inner_h_net, h_inner_h=optimal_snr_net_squared)

        elif self.phase_marginalization:
            log_l = self.phase_marginalized_likelihood(
                d_inner_h=d_inner_h_net, h_inner_h=optimal_snr_net_squared)

        else:
            log_l = np.real(d_inner_h_net) - optimal_snr_net_squared / 2

        snr_dict['network'] = {}
        snr_dict['network']['log_l_ratio'] = log_l
        snr_dict['network']['optimal_snr'] = optimal_snr_net_squared**0.5
        snr_dict['network']['matched_filter_snr'] = matched_filter_snr_net_squared**0.5
        snr_dict['network']['matched_filter_snr_phasemarg'] = self._bessel_function_interped(abs(d_inner_h_net)) / optimal_snr_net_squared**0.5

        self.snr_dict = snr_dict

        return snr_dict

    def log_likelihood_ratio(self):
        try:
            snr_dict = self.calculate_network_snrs()
            log_l_ratio = float(snr_dict['network']['log_l_ratio'])
        except RuntimeError as error_msg:
            cut.logger.warning(f'A RuntimeError was raised while calling the log_likelihood_ratio() method with message:\n {error_msg}\nReturning -np.inf')
            log_l_ratio = -np.inf
        finally:
            return log_l_ratio

class ROQLikelihood(ROQBilbyLikelihood):
    """
    Wrapper of bilby's class: ROQGravitationalWaveTransient, allowing to add several methods such as a neat computation of the network snrs. Basically a copy paste of `class Likelihood(BilbyLikelihood)` but for the ROQ case because multiple inheritance would get messy...
    """
    def __init__(self, interferometers, waveform_generator, priors,
                 weights=None, linear_matrix=None, quadratic_matrix=None,
                 roq_params=None,
                 distance_marginalization=False, phase_marginalization=False,
                 distance_marginalization_lookup_table=None):
        super(ROQGravitationalWaveTransient, self).__init__(
            interferometers=interferometers,
            waveform_generator=waveform_generator, priors=priors,
            distance_marginalization=distance_marginalization,
            phase_marginalization=phase_marginalization,
            time_marginalization=False,
            distance_marginalization_lookup_table=distance_marginalization_lookup_table,
            jitter_time=False)
        self._setup_phase_marginalization()

    def calculate_network_snrs(self):
        snr_dict = {}

        waveform_polarizations =\
            self.waveform_generator.frequency_domain_strain(self.parameters)

        if waveform_polarizations is None:
            return np.nan_to_num(-np.inf)

        d_inner_h_net = 0.
        optimal_snr_net_squared = 0.
        matched_filter_snr_net_squared = 0.
        if self.time_marginalization:
            if self.jitter_time:
                self.parameters['geocent_time'] += self.parameters['time_jitter']
            d_inner_h_tc_array = np.zeros(
                self.interferometers.frequency_array[0:-1].shape,
                dtype=np.complex128)

        for ifo in self.interferometers:
            per_detector_snr = self.calculate_snrs(
                waveform_polarizations=waveform_polarizations,
                interferometer=ifo)

            snr_dict[ifo.name] = {}
            snr_dict[ifo.name]['log_l_ratio'] = np.real(per_detector_snr.d_inner_h) - 0.5 * np.real(per_detector_snr.optimal_snr_squared)
            snr_dict[ifo.name]['optimal_snr'] = np.real(per_detector_snr.optimal_snr_squared)**0.5
            snr_dict[ifo.name]['complex_matched_filter_snr'] = per_detector_snr.complex_matched_filter_snr
            d_inner_h_phasemarg = self._bessel_function_interped(abs(per_detector_snr.d_inner_h))
            snr_dict[ifo.name]['matched_filter_snr_phasemarg'] = d_inner_h_phasemarg / snr_dict[ifo.name]['optimal_snr']

            d_inner_h_net += per_detector_snr.d_inner_h
            optimal_snr_net_squared += np.real(per_detector_snr.optimal_snr_squared)
            matched_filter_snr_net_squared += np.real(per_detector_snr.complex_matched_filter_snr)**2


            if self.time_marginalization:
                d_inner_h_tc_array += per_detector_snr.d_inner_h_squared_tc_array

        if self.time_marginalization:
            log_l = self.time_marginalized_likelihood(
                d_inner_h_tc_array=d_inner_h_tc_array,
                h_inner_h=optimal_snr_net_squared)
            if self.jitter_time:
                self.parameters['geocent_time'] -= self.parameters['time_jitter']

        elif self.distance_marginalization:
            log_l = self.distance_marginalized_likelihood(
                d_inner_h=d_inner_h_net, h_inner_h=optimal_snr_net_squared)

        elif self.phase_marginalization:
            log_l = self.phase_marginalized_likelihood(
                d_inner_h=d_inner_h_net, h_inner_h=optimal_snr_net_squared)

        else:
            log_l = np.real(d_inner_h_net) - optimal_snr_net_squared / 2

        snr_dict['network'] = {}
        snr_dict['network']['log_l_ratio'] = log_l
        snr_dict['network']['optimal_snr'] = optimal_snr_net_squared**0.5
        snr_dict['network']['matched_filter_snr'] = matched_filter_snr_net_squared**0.5
        snr_dict['network']['matched_filter_snr_phasemarg'] = self._bessel_function_interped(abs(d_inner_h_net)) / optimal_snr_net_squared**0.5

        return snr_dict

        def log_likelihood_ratio(self):
            snr_dict = self.calculate_network_snrs()
            return float(snr_dict['network']['log_l_ratio'])
