# DeepHMC

Contacts:
 - Marc Arène (arene.marc@gmail.com)
 - Ed Porter

## Description
DeepHMC has been developed as part of the PhD in Physics of the Universe: _DeepHMC: a deep neural network Hamiltonian Monte Carlo algorithm for the inference of compact binary sources of gravitational waves_, defended by Dr. Marc Arène in March 2021 and supervised by Dr. Edward Porter at the University of Paris Cité, AstroParticle and Cosmology Laboratory.

The article describing the algorithm should be published in early 2022.

Essentially, DeepHMC is a Hamiltonian Monte Carlo algorithm, tuned to perform parameter estimation on compact binary sources of GWs, and which circumvents the bottleneck of computing numerical gradients of the log-likelihood by using a deep neural network approximation of these gradients.

At the moment, the algorithm has been fully tested on the BNS signal GW170817.

The structure of the repo still inherits that of the original algorithm written in `C`, which can be a bit confusing. A decent amount of cleaning still needs to be performed.


## Setup and install the source code

Please follow the dedicated explanations described in [how_to_install_for_ed.md](./how_to_install_for_ed.md)

## Run the algorithm and understand the configuration files

Cf [how_to_set_a_run.md](./how_to_set_a_run.md)

## Run Phase II and III from a pre-saved Phase I

Cf [how_to_run_from_saved_phase1.md](./how_to_run_from_saved_phase1.md)

## Debug Phase I when all trajectories are being rejected

It can happen that even numerical trajectories in Phase I get rejected and one has to understand what is happening, at it happened so many times to me during my PhD. To analyze the problem, I have developed a script which plots the trajectories in all dimensions, which often allows one to understand why the Hamiltonian is not conserved, i.e. why trajectories get rejected. To learn how to use this script, please refer to [how_to_visualize_trajectories.md](./how_to_visualize_trajectories).
