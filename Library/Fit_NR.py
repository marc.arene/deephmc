import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np
import itertools as it
import core.utils as cut
import time
from sklearn import preprocessing

# switch to the right env to import kerastuner which needs TF 2.0
try:
    from kerastuner import HyperModel
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.layers import Dense, Dropout
    from tensorflow.keras.optimizers import Adam
    from tensorflow.keras import losses
    from tensorflow.keras.models import load_model
    from tensorflow.keras.utils import plot_model
    import tensorflow.keras.backend as K
    import tensorflow
except Exception as e:
    try:
        from keras.models import Sequential
        from keras.layers import Dense, Dropout
        from keras import losses
        from keras.models import load_model
        from keras.utils import plot_model
        import keras.backend as K
        import tensorflow
        class HyperModel():
            def __init__():
                cut.logger.error(f'keras-tuner needs tensorflow version 2.0+. Load the correct conda env having it.')
                sys.exit()
    except Exception as e:
        cut.logger.warning('NO KERAS PACKAGE WAS FOUND, YOU WILL NOT BE ABLE TO USE THE DNN.')
        class HyperModel():
            def __init__():
                pass


import Library.python_utils as pu

def update_train_data(x_train, y_train, x_new, y_new):
    """
    Append points computed during a numerical trajectory to the existing set of gathered data.

    Parameters:
        x_train: list, NOT numpy array
            Positions of the trained data already gathered, in the "trajectory" parameter space.
        y_train: list, NOT numpy array
            Gradients of the trained data already gathered.
        x_new: list, NOT numpy array
            New positions in the "trajectory" parameter space to append to the existing training set.
        y_new: list, NOT numpy array
            New gradients to append to the existing training set.

    Note: We require list and not numpy arrays because np.append() is slower than appending on basic python lists (because a new copy of the arrays needs to be generated each time).
    """
    for i in range(len(x_new)):
        x_train.append(x_new[i])
        y_train.append(y_new[i])

def QR_FittingMethod_Gradients(pt_fit, dlogL_fit, sampler):
    """
    QR inversion routine from GSL, used for the cubic fit of the 6 good parameters

    Parameters
    ----------
    pt_fit : array_like.
        Array of shape (n_pt_fitx9) containing the abscissa of the points, ie the values of the 9 parameters, recorded n_pt_fit times in parameters space.
    dlogL_fit : array_like.
        Array of shape (n_pt_fitx9) containing the ordinate of the points, ie the values of dLog wrt each param, recorded n_pt_fit times.

    Returns
    -------
    fit_coeff_global : array_like.
        Array of shape (9xm_fit) containing on each line the fitting coefficients to compute dlogL with respect to the parameter
    """

    # This loop takes ~30 sec when n_pt_fit~3e5
    n_pt_fit = pt_fit.shape[0]
    cubic_jacobian_matrix = []
    for i in range(n_pt_fit):
        cubic_jacobian_row = cubic_jacobian(pt_fit[i]).tolist()
        cubic_jacobian_matrix.append(cubic_jacobian_row)

    cubic_jacobian_matrix = np.asarray(cubic_jacobian_matrix, dtype=np.float64)
    q, r = np.linalg.qr(cubic_jacobian_matrix)
    pseudo_inverse = np.matmul(np.linalg.inv(r), q.T, dtype=np.longdouble)

    # With this just one matrix multiplication we solve the 9 overdetermined systems of equations. Each vector solution of size 220 is then contained in the columns of fit_coeff_global

    fit_coeff_global = np.matmul(pseudo_inverse, dlogL_fit, dtype=np.longdouble)
    # fit_coeff_global = pseudo_inverse @ dlogL_fit # has shape (220, 9)

    # This transpose is just here to keep the same shape has Yann's
    fit_coeff_global = fit_coeff_global.T  # has shape (9, 220)


    return fit_coeff_global

def get_combinations_for_vector(vector_x, exponent):
    """
    Returns all the possible combinations one has to make between
    the items of `vector_x` to produce the terms of order `exponent`.

    Parameters:
    ----------
    vector_x: array-like of size n
    exponent: power of the terms

    Returns:
    --------
    array: size = (n+e-1)! / e! / (n-1)! where e = exponent
        python list of exponent-uplets

    Examples:
    --------
    >> vector_x = ['q0', 'q1', 'q2']
    >> get_combinations_for_vector(vector_x, 2)
    [('q0', 'q0'),
     ('q0', 'q1'),
     ('q0', 'q2'),
     ('q1', 'q1'),
     ('q1', 'q2'),
     ('q2', 'q2')]

    >> vector_x = ['q0', 'q1']
    >> get_combinations_for_vector(vector_x, 3)
    [('q0', 'q0', 'q0'),
     ('q0', 'q0', 'q1'),
     ('q0', 'q1', 'q1'),
     ('q1', 'q1', 'q1')]
    """
    return list(it.combinations_with_replacement(vector_x, exponent))

def cubic_jacobian(vector_x):
    """
    Returns the list of all the terms part of the full cubic jacobian for vector_x.
    This function is quite optimized but replacing it with sklearn PolynomialFeatures might be worth checking:
    https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.PolynomialFeatures.html#sklearn.preprocessing.PolynomialFeatures

    Parameters:
    ----------
    vector_x: array like of size n

    Returns:
    --------
    full jacobian of size 1 + n + n*(n+1)/2 + n*(n+1)*(n+2)/6

    Example:
    --------
    Imagining `a` and `b` are real numbers, the vector [a, b] of size
    2 will yield a cubic jacobian of size 10:
    >>> vector_x = [a, b]
    >>> cubic_jacobian(vector_x)
    [a**3, a**2 * b, a * b**2, b**3, a**2, a * b, b**2, a, b, 1]

    """
    # Creating all combinations of 2-uplets from vector_x
    comb_quadratic = np.asarray(get_combinations_for_vector(vector_x, 2))
    # Multiplying elements of each 2-uplets together produces all the
    # quadratic terms
    quadratic_terms = comb_quadratic[:, 0] * comb_quadratic[:, 1]

    # Similarly for the cubic terms using 3-uplets
    comb_cubic = np.asarray(get_combinations_for_vector(vector_x, 3))
    cubic_terms = comb_cubic[:, 0] * comb_cubic[:, 1] * comb_cubic[:, 2]

    # Combine everything to get the full cubic jacobian
    return np.concatenate((cubic_terms, quadratic_terms, vector_x , [1]))

def get_OLUT(table_x, table_y, idx_to_sort):
    sort_array_index = np.argsort(table_x[:, idx_to_sort])
    olut_x = table_x[sort_array_index]
    olut_y = table_y[sort_array_index]
    return olut_x, olut_y

def get_olut_dict(table_x, table_y, param_idx, param_key):
    olut_x, olut_y = get_OLUT(table_x, table_y, param_idx)
    return {param_key: (olut_x, olut_y)}

def get_oluts_dict(q_pos, dlogL, sampler):
    oluts_dict = {}
    for idx in sampler.search_parameter_indices_local_fit:
        traj_key = sampler.search_trajectory_parameters_keys[idx]
        olut_dict = get_olut_dict(q_pos, dlogL[:, idx], idx, traj_key)
        oluts_dict.update(olut_dict)
    return oluts_dict

def find_closest_points_of_olut(vector_x, olut_x, olut_y, n_fit_1, idx_sort):
    """
    Returns the n_fit_1 closest points of the sorted table PointFit_Sort to the value local_x_vector[param_index], where PointFit_Sort is already sorted wrt to PointFit_Sort[param_index].

    Parameter
    ---------
    local_x_vector: array_like, shape = (n_param,)
        The parametrised vector position
    PointFit_Sort: array_like, shape = (n_param+1, n_pt_fit)
        The table of recorded points along numerical trajectories which we want to use for the fit; sorted wrt PointFit_Sort[param_index]. Its last line: PointFit_Sort[n_param] (hence the n_param+1's line) contains the values of the gradients of the points.
    n_pt_fit: int
        Number of fitting points
    n_param: int
        Number of parameters used in our model (9 or 15)
    n_fit_1: int
        Number of closest points selected. We actually keep n_fit_1 + 1 points.
    param_index: int
        Index of the line with respect to which PointFit_Sort is sorted. Note that PointFit_Sort should already be sorted that way

    Return
    ------
    Matrix_ClosestPoint: array_like, shape = (n_param, n_fit_1+1)
        Contains the n_fit_1+1 closest points.
    Vector_GradientClosestPoint: array_like, shape = (n_fit_1+1,)
        Contains the n_fit_1+1 corresponding values of the gradients

    Note
    ----
    If q_pos_traj[param_index] falls out close to the borders of PointFit_Sort[param_index], we can't select n_fit_1+1 points symmetrically left and right wrt its position. If so the indices which would go out of bounds select circularly on the other edge of the vector.

    """

    # Find the closest index for the searched parameter
    idx_closest = np.searchsorted(olut_x[:, idx_sort], vector_x[idx_sort]) - 1

    # Allocate vector and matrices for closest points in cos(inc) around IndexClosest
    limLeft = int(idx_closest - (n_fit_1/2))         # left limit index
    limRight = int(idx_closest + (n_fit_1/2))        # right limit index
    n_pt_fit = olut_x.shape[0]
    # import IPython; IPython.embed()
    # Select the n_fit_1 closest points. In python slicing is more natural.
    if 0 <= limLeft and limRight < n_pt_fit:
        olut_x_closest = olut_x[limLeft:limRight+1]
        olut_y_closest = olut_y[limLeft:limRight+1]
    elif limLeft < 0:
        olut_x_closest = np.concatenate((olut_x[limLeft:], olut_x[:limRight+1]), axis=0)
        olut_y_closest = np.concatenate((olut_y[limLeft:], olut_y[:limRight+1]))
    elif n_pt_fit <= limRight:
        olut_x_closest = np.concatenate((olut_x[limLeft:], olut_x[:limRight-n_pt_fit+1]), axis=0)
        olut_y_closest = np.concatenate((olut_y[limLeft:], olut_y[0:limRight-n_pt_fit+1]))

    return olut_x_closest, olut_y_closest

def find_nearest_neighbors(vector_x, table_x, table_y, indices_to_compute_distance, squared_scales, n_neighbors):
    distance_to_pts_sqd = np.zeros(table_x.shape[0])

    for idx in indices_to_compute_distance:
        distance_to_pts_sqd += (table_x[:, idx] - vector_x[idx])**2/squared_scales[idx]

    idx_closest = np.argpartition(distance_to_pts_sqd, n_neighbors)[:n_neighbors]

    table_x_n_neighbors = table_x[idx_closest] # shape=(n_fit_2, n_param)
    table_y_n_neighbors = table_y[idx_closest]

    return table_x_n_neighbors, table_y_n_neighbors


class GlobalFitRegressor(object):
    """
    Encapsulating class for the different global fit methods like cubic or DNN
    """
    def __init__(self, name, nick_name, max_train_set_size=int(1e6), **kwargs):
        self.name = name
        self.nick_name = nick_name
        self.max_train_set_size = max_train_set_size
        self.x_trained = []
        self.y_trained = []
        self.new_x_for_train = []
        self.new_y_for_train = []
        self.plot_color = kwargs.get('plot_color', 'black')
        self.nb_of_num_traj_since_last_fit = 0

    def fit(self, x_train, y_train, **kwargs):
        # Define you fit function in your encapsulation fit() of your class
        # It has to end with the following two lines
        self.x_trained = x_data.tolist()
        self.y_trained = y_data.tolist()

    def predict(self, x_data, **kwargs):
        pass

    @property
    def nb_pts_before_new_fit(self):
        return int(0.5 * len(self.x_trained))

    def enough_new_pts_stored(self):
        return False
        # return self.nb_pts_before_new_fit <= len(self.new_x_for_train)

    def not_too_many_pts(self, new_x_for_train, new_y_for_train):
        return len(self.x_trained) + len(self.new_x_for_train) + len(new_x_for_train) <= self.max_train_set_size

    def propose_new_fit(self, new_x_for_train, new_y_for_train, **kwargs):
        if self.not_too_many_pts(new_x_for_train, new_y_for_train):
            # breakpoint()
            update_train_data(self.new_x_for_train, self.new_y_for_train, new_x_for_train, new_y_for_train)
            # if self.enough_new_pts_stored():
            self.nb_of_num_traj_since_last_fit += 1
            num_traj_threshold = 50
            if self.nb_of_num_traj_since_last_fit >= num_traj_threshold:
                cut.logger.info(f'{num_traj_threshold} numerical trajectories have been run since last derivation of the fit.')
                x_train = self.x_trained.copy()
                y_train = self.y_trained.copy()
                update_train_data(x_train, y_train, self.new_x_for_train, self.new_y_for_train)
                # It is important to convert to numpy array here in the if
                # and not outside because x_train and y_train being large arrays
                # their transformations to np.array takes ~200ms each; since
                # .new_fit() is called at the end of each traj, that would
                # double the run time (1 anal traj is also ~200ms)
                cut.logger.info(f'Conducting new {self.nick_name} fit with {len(x_train)} fit points, ie {len(self.new_x_for_train)} more points.')
                hist = self.fit(np.asarray(x_train), np.asarray(y_train), **kwargs)
                self.new_x_for_train = []
                self.new_y_for_train = []
                self.nb_of_num_traj_since_last_fit = 0

    def save_training_sets(self, dir):
        cut.check_directory_exists_and_if_not_mkdir(dir)
        np.savetxt(dir + f'/qpos_trained.dat', self.x_trained, fmt='%.25e')
        np.savetxt(dir + f'/dlogL_trained.dat', self.y_trained, fmt='%.25e')
        np.savetxt(dir + f'/new_qpos_for_train.dat', self.new_x_for_train, fmt='%.25e')
        np.savetxt(dir + f'/new_dlogL_for_train.dat', self.new_y_for_train, fmt='%.25e')

    def load_training_sets(self, dir):
        self.x_trained = np.loadtxt(dir + '/qpos_trained.dat').tolist()
        self.y_trained = np.loadtxt(dir + '/dlogL_trained.dat').tolist()
        # # If new_qpos/dlogL_for_train.dat are empty files, we ignore
        # # the warnings raised by `np.loadtxt()` when loading those
        # import warnings
        # with warnings.catch_warnings():
        #     warnings.simplefilter("ignore")
        #     self.new_x_for_train = np.loadtxt(dir + '/new_qpos_for_train.dat').tolist()
        #     self.new_y_for_train = np.loadtxt(dir + '/new_dlogL_for_train.dat').tolist()
        self.new_x_for_train = []
        self.new_y_for_train = []


class CubicFitRegressor(GlobalFitRegressor):

    def __init__(self, pseudo_inverse=None):
        super(CubicFitRegressor, self).__init__('CubicFitRegressor', 'cubic', plot_color='green')
        self.pseudo_inverse = pseudo_inverse
        self.cubic_fit_coeff = CubicFitCoeff()
        self.save_dir_suffix = self.nick_name

    @property
    def coeff(self):
        return self.cubic_fit_coeff.coeff

    def qr_decompose(self, x_data):
        cut.logger.info(f'Creating cubic jacobian of {x_data.shape[0]} points...')
        cubic_jacobian_matrix = []
        t1 = time.time()
        for i in range(x_data.shape[0]):
            cubic_jacobian_line = cubic_jacobian(x_data[i]).tolist()
            cubic_jacobian_matrix.append(cubic_jacobian_line)
        cubic_jacobian_matrix = np.asarray(cubic_jacobian_matrix, dtype=np.float64)
        t2 = time.time()
        cut.logger.info(f'It took {t2-t1:.1f} sec.')
        cut.logger.info(f'Computing QR decomposition of the cubic jacobian matrix of shape {cubic_jacobian_matrix.shape}...')
        q, r = np.linalg.qr(cubic_jacobian_matrix)
        t3 = time.time()
        cut.logger.info(f'It took {t3-t2:.1f} sec.')
        return q, r

    def compute_pseudo_inverse(self, x_data):
        q, r = self.qr_decompose(x_data)
        cut.logger.info(f'Computing the pseudo inverse = R^(-1) * Q.T ...')
        t1 = time.time()
        self.pseudo_inverse = np.matmul(np.linalg.inv(r), q.T, dtype=np.longdouble)
        t2 = time.time()
        cut.logger.info(f'It took {t2-t1:.1f} sec.')
        return self.pseudo_inverse

    def fit(self, x_data, y_data):
        if self.cubic_fit_coeff.coeff is not None:
            cut.logger.warning('Deleting old fit coeffs and computing new ones.')
            self.cubic_fit_coeff = CubicFitCoeff()
            self.pseudo_inverse = None
        if self.pseudo_inverse is None:
            cut.logger.info(f'Starting the cubic fit over {len(x_data)} data points...')
            self.compute_pseudo_inverse(x_data)
        cut.logger.info(f'Computing the cubic fit coeffs...')
        t1 = time.time()
        self.cubic_fit_coeff.coeff = np.matmul(self.pseudo_inverse, y_data, dtype=np.longdouble)
        t2 = time.time()
        cut.logger.info(f'It took {t2-t1:.1f} sec.')
        self.x_trained = x_data.tolist()
        self.y_trained = y_data.tolist()
        return self.cubic_fit_coeff

    def predict(self, x_data):
        return self.cubic_fit_coeff.predict(x_data)

    def save(self, dir):
        self.save_training_sets(dir)
        cut.check_directory_exists_and_if_not_mkdir(dir + f'/{self.save_dir_suffix}')
        np.savetxt(dir + f'/{self.save_dir_suffix}/fit_coeff_global.dat', self.coeff, fmt='%.25e')

    def load(self, dir):
        self.load_training_sets(dir)
        self.cubic_fit_coeff.coeff = np.loadtxt(dir + f'/{self.save_dir_suffix}/fit_coeff_global.dat', dtype=np.longdouble)

class CubicFitCoeff:

    def __init__(self, coeff=None):
        self.coeff = coeff

    def predict(self, x_data):
        # We distinguish cases when x_data is a vector, ie prediction
        # of a single data point, and when it's a matrix, ie prediction
        # of a batch a data points.
        if len(x_data.shape) == 1:
            cubic_jacobian_array = cubic_jacobian(x_data)
        else:
            cubic_jacobian_matrix = []
            for i in range(x_data.shape[0]):
                cubic_jacobian_row = cubic_jacobian(x_data[i]).tolist()
                cubic_jacobian_matrix.append(cubic_jacobian_row)
            cubic_jacobian_array = np.asarray(cubic_jacobian_matrix)
        return cubic_jacobian_array @ self.coeff

class DNNRegressorMultipleGradients(GlobalFitRegressor):
    """
    One DNN predicting all gradients at the same time.
    """
    def __init__(self, n_dim, batch_size, epochs=100, steps_per_epoch=100, **kwargs):
        self.n_dim = n_dim
        super(DNNRegressorMultipleGradients, self).__init__('DNNRegressorList', 'dnn', plot_color='blue')
        default_kwargs = dict(
            kernel_initializer='normal',
            activation='relu',
            loss='mean_squared_error',
            # loss='mean_absolute_error',
            optimizer='adam'
        )
        default_kwargs.update(kwargs)
        kwargs = default_kwargs.copy()
        self.kernel_initializer = kwargs['kernel_initializer']
        self.activation = kwargs['activation']
        # self.loss = mean_squared_error_custom
        self.loss = kwargs['loss']
        self.optimizer = kwargs['optimizer']
        self.epochs = epochs
        self.batch_size = batch_size
        self.steps_per_epoch = steps_per_epoch
        self.model = self.set_single_dnn()
        tf_version = f'tf_{tensorflow.__version__}'
        self.save_dir_suffix = f'{self.nick_name}_{tf_version}/epochs{self.epochs}_batchsize{self.batch_size}'

    def set_single_dnn(self):
        method = Sequential()
        method.add(Dense(
            units=self.n_dim * 10,
            input_dim=self.n_dim,
            kernel_initializer=self.kernel_initializer,
            activation='linear'
            ))
        method.add(Dense(
            units=self.n_dim * 10,
            kernel_initializer=self.kernel_initializer,
            activation='relu'
            ))
        method.add(Dense(
            units=self.n_dim * 100,
            kernel_initializer=self.kernel_initializer,
            activation='relu'
            ))
        method.add(Dropout(
            rate=0.25
            ))
        method.add(Dense(
            units=self.n_dim,
            activation='linear'
            ))
        # Compile the model/method
        # Cf https://keras.io/losses/ for loss functions available
        # from keras import losses
        # loss = keras.losses.logcosh(y_true, y_pred)
        # loss = 'mse'
        # loss = 'mean_absolute_error'
        # optimizer = 'rmsprop'
        # optimizer = 'adam'
        # For a mean squared error regression problem
        method.compile(optimizer=self.optimizer, loss=self.loss)
        return method

    def set_scaler(self, data):
        return preprocessing.StandardScaler().fit(data)

    def fit(self, x_train, y_train, batch_size=None, epochs=None, verbose=1, validation_data=None):
        if batch_size is None:
            if self.batch_size is None:
                batch_size = int(len(x_train) / self.steps_per_epoch)
            else:
                batch_size = self.batch_size
        if epochs is None:
            epochs = self.epochs
        cut.logger.info(f'For the training the DNN, batch_size = {batch_size} and epochs = {epochs}.')
        cut.logger.info(f'Setting scaler functions for x_data and y_data.')
        self.x_scaler = self.set_scaler(x_train)
        self.y_scaler = self.set_scaler(y_train)
        x_train_scaled = self.x_scaler.transform(x_train)
        y_train_scaled = self.y_scaler.transform(y_train)
        if validation_data is not None:
            x_val = self.x_scaler.transform(validation_data[0])
            y_val = self.y_scaler.transform(validation_data[1])
            val_data_for_dnn = (x_val, y_val)
        else:
            val_data_for_dnn = None
        cut.logger.info(f'Fitting the DNN model...')
        hist = self.model.fit(x_train_scaled, y_train_scaled, batch_size=batch_size, epochs=epochs, verbose=verbose, validation_data=val_data_for_dnn)
        self.x_trained = x_train.tolist()
        self.y_trained = y_train.tolist()

        return hist

    def predict(self, x_data):
        # If x_data contains only one sample, ie shape=(X,) reshape it
        # to create one row such that after shape=(1, X)
        # This is needed for the rescaling as well as for the dnn.predict().
        only_one_sample = len(x_data.shape) == 1
        if only_one_sample:
            x_data = x_data.reshape(1, -1)

        x_data_scaled = self.x_scaler.transform(x_data)
        y_data_predicted_scaled = self.model.predict(x_data_scaled)
        # import IPython; IPython.embed(); sys.exit()
        y_data_predicted = self.y_scaler.inverse_transform(y_data_predicted_scaled)

        # Don't return an array containing only 1 element, ie shape=(1, X)
        # but return an array of shape=(X,)
        if only_one_sample:
            return y_data_predicted[0]
        else:
            return y_data_predicted

    def save(self, dir):
        self.save_training_sets(dir)
        dnn_mg_dir_epochs_batchsize = dir + f'/{self.save_dir_suffix}'
        cut.check_directory_exists_and_if_not_mkdir(dnn_mg_dir_epochs_batchsize)
        self.model.save(dnn_mg_dir_epochs_batchsize + f'/dnn_mg_model.hdf5')
        # If the model was loaded from a saved state, it doesn't have a history
        # if 'history' in self.model.__dict__.keys():
        #     pu.save_dict_to_json(self.model.history.params, dnn_mg_dir_epochs_batchsize + '/hist_params.json')
        plot_model(self.model, show_shapes=True, to_file=dnn_mg_dir_epochs_batchsize + '/model.png')

    def load(self, dir):
        self.load_training_sets(dir)
        cut.logger.info(f'Loading the saved DNN...')
        dnn_mg_dir_epochs_batchsize = dir + f'/{self.save_dir_suffix}'
        self.model = load_model(dnn_mg_dir_epochs_batchsize + f'/dnn_mg_model.hdf5')
        self.x_scaler = self.set_scaler(self.x_trained)
        self.y_scaler = self.set_scaler(self.y_trained)

pn = np.array([2, 6, 2, 1, 1, 2, 2, 1, 2, 2, 5, 5])
pnsqrd = pn**2

def mean_squared_error_custom(y_true, y_pred):
    if not K.is_tensor(y_pred):
        y_pred = K.constant(y_pred)
    y_true = K.cast(y_true, y_pred.dtype)
    return K.mean(K.square(y_pred - y_true)/pnsqrd, axis=-1)

# losses.mean_squared_error_custom = mean_squared_error_custom

class OLUTRegressor(object):
    """
    """

    def __init__(self, param_key, param_idx, indices_to_compute_distance, squared_scales):
        self.name = 'OLUTRegressor'
        self.nick_name = 'olut'
        self.param_key = param_key
        self.param_idx = param_idx
        self.indices_to_compute_distance = indices_to_compute_distance
        self.squared_scales = squared_scales
        self.olut_x = None
        self.olut_y = None
        self.n_fit_1 = 2000
        self.n_fit_2 = 200

    def set_olut(self, x_data, y_data):
        sort_array_index = np.argsort(x_data[:, self.param_idx])
        self.olut_x = x_data[sort_array_index]
        self.olut_y = y_data[sort_array_index]
        return self.olut_x, self.olut_y

    def update_olut(self, new_x_data, new_y_data):
        x_data = np.append(self.olut_x, new_x_data, axis=0)
        y_data = np.append(self.olut_y, new_y_data)
        self.set_olut(x_data, y_data)

    def fit_and_predict(self, local_x_vector):
    	"""
    	Look up table fit routine for:
    		cos(inc) if param_idx = 0
    		psi		 if param_idx = 2
    		logD	 if param_idx = 3
    	"""

    	olut_x_n1, olut_y_n1 = find_closest_points_of_olut(local_x_vector, self.olut_x, self.olut_y, self.n_fit_1, self.param_idx)

    	indices_to_compute_distance = self.indices_to_compute_distance.copy()
    	indices_to_compute_distance.remove(self.param_idx)

    	olut_x_n2, olut_y_n2 = find_nearest_neighbors(local_x_vector, olut_x_n1, olut_y_n1, indices_to_compute_distance, self.squared_scales, self.n_fit_2)

    	A = np.append(olut_x_n2, np.ones((self.n_fit_2, 1)), axis=1)
    	A = np.asarray(A, dtype=np.float64)
    	jac_local_x_vector = np.append(local_x_vector, 1)
    	# Now do the singular value decomposition on A. Since we are only interested in the pseudo_inverse, we don't need to compute all components of u, hence we save so time using the option full_matrices=False. This returns a matrice u of shape (n_fit_2, m_fit_global) instead of (n_fit_2, n_fit_2).
    	u, s, vh = np.linalg.svd(A, full_matrices=False)

    	# Retreive the indices of s where its values are too small, corresponding to a degeneracy in the columns of A (meaning some columns can be equated to others by linear combinations). See Numerical Recipes>SVD for detailed explanations. Basically those small values mean A has a nullspace (Ker{A}), and trying to invert them in s would throw off very large numbers with important round-off errors. These big numbers would pull off the searched solution: fit_coeff towards the nullspace and in the end the chi-squared merit result would not be the smallest possible. Equating those to zero in the inverse matrix basically unables us to get rid of those very linear combinations which make A degenerate.
    	# The criteria for being too small, named formally the "condition number of a matrix", is for the relative precision of the number: s/s.max to be smaller than the machine floating-point precision. For python, we here deal with float64 number, which precision is given (and explained here: https://docs.python.org/3/tutorial/floatingpoint.html, https://fr.wikipedia.org/wiki/Epsilon_d%27une_machine#cite_ref-b_2-0) to be 2**-52 ~ 2.22e-16. We take here 1.0e-15
    	TOL = 1.0e-15
    	idx_singular = np.where(s < s.max() * TOL)[0] #[0] is added because np.where() throws a weird structure I want to get rid of.
    	s_inv = 1/s
    	# Now set those values to zero in the inverse matrix:
    	s_inv[idx_singular] = np.zeros(len(idx_singular))
    	vhT_sinv = np.matmul(vh.T, np.diag(s_inv), dtype=np.longdouble)
    	pseudo_inverse = np.matmul(vhT_sinv, u.T, dtype=np.longdouble)
    	# pseudo_inverse = np.matmul(vh.T * s_inv, u.T, dtype=np.longdouble)
    	fit_coeff = np.matmul(pseudo_inverse, olut_y_n2, dtype=np.longdouble)
    	y_predicted = np.matmul(fit_coeff, jac_local_x_vector, dtype=np.longdouble)
    	########################################################################################################
    	if False:
    	# if i_debug >= 65 and param_idx==0:
    		import corner
    		# bilby default corner kwargs. Overwritten by anything passed to kwargs
    		defaults_kwargs = {}
    		# defaults_kwargs = dict(bins=25, smooth=0.9, label_kwargs=dict(fontsize=16), title_kwargs=dict(fontsize=16), color='#0072C1', truth_color='tab:orange', plot_density=False, plot_datapoints=True, fill_contours=False, max_n_ticks=3)
    		kwargs = {}
    		kwargs['labels'] = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
    		kwargs['plot_contours'] = False

    		kwargs['truths'] = local_x_vector[sampler_dict['search_parameter_indices']]

    		defaults_kwargs.update(kwargs)
    		kwargs = defaults_kwargs

    		samples_n1 = olut_x_n1[:, sampler_dict['search_parameter_indices']]
    		figure_n1 = corner.corner(samples_n1, **kwargs)
    		figure_n1.savefig(directory + 'i{}_{}pts.png'.format(i_debug, samples_n1.shape[0]), dpi=300)

    		samples_n2 = olut_x_n2[:, sampler_dict['search_parameter_indices']]
    		figure_n2 = corner.corner(samples_n2, **kwargs)
    		figure_n2.savefig(directory + 'i{}_{}pts.png'.format(i_debug, samples_n2.shape[0]), dpi=300)
    	return y_predicted

class OLUTsRegressor(list):
    """
    List of OLUTRegressor.
    """

    def __init__(self, param_keys, param_indices, indices_to_compute_distance, squared_scales, max_nb_pts_for_oluts=int(1e6)):
        self.name = 'OLUTsRegressor'
        self.nick_name = 'oluts'
        # Here I just need the keys of the local params, not all the keys
        self.param_keys = param_keys
        self.param_indices = param_indices
        self.indices_to_compute_distance = indices_to_compute_distance
        for i, param_idx in enumerate(param_indices):
            olut_reg = OLUTRegressor(param_keys[i], param_idx, indices_to_compute_distance, squared_scales)
            self.append(olut_reg)
        self.squared_scales = squared_scales
        self.max_nb_pts_for_oluts = max_nb_pts_for_oluts
        self.save_dir_suffix = self.nick_name
        self.plot_color = 'purple'

    @property
    def squared_scales(self):
        return self._squared_scales

    @squared_scales.setter
    def squared_scales(self, squared_scales):
        # This setter is needed to update the scales of each olut
        # when we do a global update on the OLUTsReg object.
        self._squared_scales = squared_scales
        for olut_reg in self:
            olut_reg.squared_scales = squared_scales

    def set_oluts(self, x_data, y_data):
        for olut_reg in self:
            olut_reg.set_olut(x_data, y_data[:, olut_reg.param_idx])

    def predict(self, x_data):
        single_prediction = len(x_data.shape) == 1
        if single_prediction:
            x_data.reshape(1, -1)
        else:
            cut.logger.info(f'Predicting values from OLUTs on batch of size {x_data.shape[0]}')
        y_data_predicted = np.zeros_like(x_data)
        for i, local_x_vector in enumerate(x_data):
            for olut_reg in self:
                y_data_predicted[i, olut_reg.param_idx] = olut_reg.fit_and_predict(local_x_vector)
            percent_progess = i/len(x_data) * 100
            if percent_progess != 0 and percent_progess % 10 == 0:
                cut.logger.info(f'  {percent_progess/100:.0%} of data predicted...')

        if single_prediction:
            return y_data_predicted[0]
        else:
            return y_data_predicted

    def fit_predict_update(self, local_x_vector, local_y_vector):
        """
        For every OLUT this function will fit the data around `local_x_vector`, predict the value for the corresponding element of `local_y_vector` and update it.
        """
        for olut_reg in self:
            local_y_vector[olut_reg.param_idx] = olut_reg.fit_and_predict(local_x_vector)
        return local_y_vector

    def update_oluts(self, new_x_data, new_y_data):
        not_too_many_pts = len(self[0].olut_x) + len(new_x_data) < self.max_nb_pts_for_oluts
        if not_too_many_pts:
            cut.logger.info(f'       Updating oluts with {len(new_x_data)} new points')
            if type(new_y_data) == list:
                new_y_data = np.asarray(new_y_data)
            for olut_reg in self:
                olut_reg.update_olut(new_x_data, new_y_data[:, olut_reg.param_idx])

    def save(self, dir):
        save_dir = dir
        cut.check_directory_exists_and_if_not_mkdir(save_dir)
        for i, olut_reg in enumerate(self):
            np.savetxt(save_dir + f'/olut_{i}_x.dat', olut_reg.olut_x, fmt='%.25e')
            np.savetxt(save_dir + f'/olut_{i}_y.dat', olut_reg.olut_y, fmt='%.25e')

    def load(self, dir):
        for i, olut_reg in enumerate(self):
            olut_reg.olut_x = np.loadtxt(dir + f'/olut_{i}_x.dat')
            olut_reg.olut_y = np.loadtxt(dir + f'/olut_{i}_y.dat')


# from tensorflow.keras.optimizers import Adam
class MyHyperModel(HyperModel):
# class MyHyperModel(HyperModel):

    def __init__(self, n_dim):
        self.n_dim = n_dim

    def build(self, hp):
        kernel_initializer = 'normal'
        activation = 'relu'
        # Specify model
        model = Sequential()

        # Range of models to build
        # for i in range(hp.Int('num_layers', min_value=1, max_value=4, step=1)):
        #     model.add(Dense(
        #         units=hp.Int('units_' + str(i), min_value=self.n_dim*10, max_value=self.n_dim*100, step=self.n_dim*10),
        #         input_dim=self.n_dim,
        #         kernel_initializer=kernel_initializer,
        #         activation='relu'))
        if False:
            model.add(Dense(
                units=hp.Int('units_0', min_value=self.n_dim, max_value=self.n_dim*100, step=self.n_dim*40),
                input_dim=self.n_dim,
                kernel_initializer=kernel_initializer,
                activation='linear'))
            model.add(Dense(
                units=hp.Int('units_1', min_value=self.n_dim, max_value=self.n_dim*100, step=self.n_dim*40),
                input_dim=self.n_dim,
                kernel_initializer=kernel_initializer,
                activation='relu'))
            model.add(Dense(
                units=hp.Int('units_2', min_value=self.n_dim, max_value=self.n_dim*100, step=self.n_dim*40),
                input_dim=self.n_dim,
                kernel_initializer=kernel_initializer,
                activation='relu'))
        if True:
            model.add(Dense(
                units=hp.Choice('units_0', values=[self.n_dim*5, self.n_dim*10]),
                input_dim=self.n_dim,
                kernel_initializer=kernel_initializer,
                activation='linear'))
            model.add(Dense(
                units=hp.Choice('units_1', values=[self.n_dim*100, self.n_dim*200]),
                kernel_initializer=kernel_initializer,
                activation='relu'))
            model.add(Dropout(
                rate=0.25))
            # model.add(Dropout(
            #     rate=hp.Choice('dropout_0', values=[0.0, 0.25])))
            model.add(Dense(
                units=hp.Choice('units_2', values=[self.n_dim*5, self.n_dim*10]),
                kernel_initializer=kernel_initializer,
                activation='relu'))
        # Output layer
        # model.add(Dense(units=self.n_dim, kernel_initializer=kernel_initializer, activation=activation))
        # model.add(Dense(
        #     units=self.n_dim,
        #     activation='linear'
        #     ))
        model.add(Dense(
            units=self.n_dim,
            activation='linear'))

        # Compile the constructed model and return it
        # from keras.optimizers import Adam
        # model.compile(
        #     optimizer=Adam(hp.Choice('learning_rate', values=[1e-2, 1e-3, 1e-4])),
        #     loss='mae'
        # )
        model.compile(
            optimizer='adam',
            loss='mse'
        )

        return model




if __name__=='__main__':
    from kerastuner.tuners import RandomSearch, Hyperband, BayesianOptimization
    import matplotlib.pyplot as plt
    import pandas as pd
    import python_utils as pu
    dir = '../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16/PSD_3/1500_1500_2000_200_200_0.005/phase_marginalization/IMRPhenomD/sampler_state'
    x_data = np.loadtxt(dir + '/qpos_traj_phase1.dat')
    y_data = np.loadtxt(dir + '/dlogL_traj_phase1.dat')
    val_prop = 0.25
    train_prop = 0.75
    idx_train = int(x_data.shape[0] * train_prop)
    idx_val = int(x_data.shape[0] * val_prop)
    x_train = x_data[:idx_train]
    y_train = y_data[:idx_train]
    x_val = x_data[-idx_val:]
    y_val = y_data[-idx_val:]
    x_scaler = preprocessing.StandardScaler().fit(x_train)
    y_scaler = preprocessing.StandardScaler().fit(y_train)
    x_train = x_scaler.transform(x_train)
    y_train = y_scaler.transform(y_train)
    x_val = x_scaler.transform(x_val)
    y_val = y_scaler.transform(y_val)
    validation_data = (x_val, y_val)

    n_dim = x_train.shape[1]
    epochs = 20
    batch_size = 1425
    # fit_method = DNNRegressorMultipleGradients(
    #     n_dim=n_dim,
    #     batch_size=batch_size,
    #     epochs=epochs
    # )
    #
    # kwargs = dict(
    #     batch_size=batch_size,
    #     epochs=epochs,
    #     validation_data=validation_data
    #     )
    # fit_method.fit(x_train, y_train, **kwargs)
    # breakpoint()
    # fit_method.model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1, validation_data=validation_data)
    # breakpoint()
    hypermodel = MyHyperModel(n_dim=n_dim)

    # Construct the Hyperband tuner using the hypermodel class created
    project_name = '.dnn_hp_tune'
    # tuner = Hyperband(
    #     hypermodel,
    #     objective='val_accuracy',
    #     # objective='val_precision',
    #     # metrics='mse',
    #     metrics=keras.metrics.mse(),
    #     # metrics=[keras.metrics.mse(), keras.metrics.Recall(name='recall')],
    #     max_epochs=10,
    #     # max_trials=5,
    #     seed=10,
    #     project_name=project_name)

    tuner = RandomSearch(
        hypermodel,
        objective='val_loss',
        # metrics=['mae'],
        executions_per_trial=1, # number of models that should be built and fit for each trial
        max_trials=20, # max total number of trials
        seed=10,
        project_name=project_name
        )


    # Search for the best parameters of the neural network using the contructed Hypberband tuner
    tuner.search(x_train, y_train, epochs=epochs, validation_data=validation_data, batch_size=batch_size)

    # Get the best hyperparameters from the search
    params = tuner.get_best_hyperparameters()[0]
    print('Best hyper parameters found are:')
    pu.print_dict(params.values)
    # Build the model using the best hyperparameters
    model = tuner.hypermodel.build(params)

    epochs = 80
    fit_method = DNNRegressorMultipleGradients(
        n_dim=n_dim,
        batch_size=batch_size,
        epochs=epochs
    )
    fit_method.model = model
    kwargs = dict(
        batch_size=batch_size,
        epochs=epochs,
        validation_data=validation_data
        )
    hist = fit_method.fit(x_train, y_train, **kwargs)
    best_model_dir = project_name + f'/best_model_fit'
    cut.check_directory_exists_and_if_not_mkdir(best_model_dir)
    hist_file_path = best_model_dir + f'/dnn_mg_model_hist.png'
    plt.plot(hist.history['loss'])
    plt.plot(hist.history['val_loss'])
    plt.title('Model loss for the DNN')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend([f'Train {len(x_train)}', f'Valid {len(x_val)}'], loc='upper left')
    plt.savefig(hist_file_path)
    plt.close()

    import Library.CONST as CONST
    import Library.plots as plots
    search_trajectory_parameters_keys = ["cos_theta_jn", "psi", "ln(D)", "ln(Mc)", "ln(mu)", "sin(dec)", "ra", "ln(tc)", 'chi_1', 'chi_2']
    dlogL_latex_labels = CONST.get_dlogL_latex_labels(search_trajectory_parameters_keys)
    plots.make_regression_plots(fit_method, x_train, y_train, best_model_dir, validation_data=validation_data, y_labels=dlogL_latex_labels)
    fit_method.save(best_model_dir)

    # # Train the best fitting model
    # model.fit(x_train, y_train, epochs=epochs, validation_data=validation_data, batch_size=1425)
    #
    # # Check the accuracy plots
    # hyperband_accuracy_df = pd.DataFrame(model.history.history)
    #
    # hyperband_accuracy_df[['loss', 'val_loss']].plot()
    # plt.title('Loss & Accuracy Per EPOCH')
    # plt.xlabel('EPOCH')
    # plt.ylabel('Accruacy')
    # plt.show()





########### OLD

# class DNNRegressorList(list):
#     """
#     A list of DNNs, each predicting one gradient only from the n_dim point in parameter space.
#     """
#     def __init__(self, n_dim, n_regressors, epochs=100, batch_size=4000, steps_per_epoch=100, **kwargs):
#         self.name = 'DNNRegressorList'
#         self.nick_name = 'DNN'
#         default_kwargs = dict(
#             kernel_initializer='normal',
#             activation='relu',
#             loss='mean_absolute_error',
#             optimizer='adam'
#         )
#         default_kwargs.update(kwargs)
#         kwargs = default_kwargs.copy()
#         self.n_dim = n_dim
#         self.kernel_initializer = kwargs['kernel_initializer']
#         self.activation = kwargs['activation']
#         self.loss = kwargs['loss']
#         self.optimizer = kwargs['optimizer']
#         self.epochs = epochs
#         self.batch_size = batch_size
#         self.steps_per_epoch = steps_per_epoch
#         for reg in range(n_regressors):
#             self.append(self.set_single_dnn())
#
#     def set_single_dnn(self):
#         method = Sequential()
#         method.add(Dense(units=self.n_dim * 10, input_dim=self.n_dim, kernel_initializer=self.kernel_initializer, activation=self.activation))
#         method.add(Dense(units=self.n_dim * 100, kernel_initializer=self.kernel_initializer, activation=self.activation))
#         method.add(Dense(units=self.n_dim * 10, kernel_initializer=self.kernel_initializer, activation=self.activation))
#         # method.add(Dense(units=n_cubic_coeff, kernel_initializer='normal', activation='relu'))
#         # method.add(Dense(units=n_dim - 1, activation='relu'))
#         # method.add(Dense(units=n_dim - 1, activation='tanh'))
#         # method.add(Dense(units=int(qpos_train.shape[1]/2), activation='relu'))
#         method.add(Dense(units=1))
#         # Compile the model/method
#         # Cf https://keras.io/losses/ for loss functions available
#         # from keras import losses
#         # loss = keras.losses.logcosh(y_true, y_pred)
#         # loss = 'mse'
#         # loss = 'mean_absolute_error'
#         # optimizer = 'rmsprop'
#         # optimizer = 'adam'
#         # For a mean squared error regression problem
#         method.compile(optimizer=self.optimizer, loss=self.loss)
#         return method
#
#     def set_scaler(self, data):
#         return preprocessing.StandardScaler().fit(data)
#
#     def fit(self, x_train, y_train, batch_size=None, epochs=None, verbose=1, validation_split=0.1, shuffle=True, validation_data=None):
#         if batch_size is None:
#             batch_size = int(len(x_train) / self.steps_per_epoch)
#         if epochs is None:
#             epochs = self.epochs
#         cut.logger.info(f'For the training of DNNs, batch_size = {batch_size} and epochs = {epochs}.')
#         cut.logger.info(f'Setting scaler functions for x_data and y_data.')
#         self.x_scaler = self.set_scaler(x_train)
#         self.y_scaler = self.set_scaler(y_train)
#         x_train_scaled = self.x_scaler.transform(x_train)
#         y_train_scaled = self.y_scaler.transform(y_train)
#         if validation_data is not None:
#             x_val = self.x_scaler.transform(validation_data[0])
#             y_val = self.y_scaler.transform(validation_data[1])
#         else:
#             val_data_for_dnn = None
#         hist_list = []
#         for i, dnn in enumerate(self):
#             if validation_data is not None:
#                 val_data_for_dnn = (x_val, y_val[:, i])
#             else:
#                 val_data_for_dnn = None
#             cut.logger.info(f'Fitting dlogL wrt param {i} with dnn...')
#             # import IPython; IPython.embed();sys.exit()
#             hist = dnn.fit(x_train_scaled, y_train_scaled[:, i], batch_size=batch_size, epochs=epochs, verbose=verbose, validation_split=validation_split, validation_data=val_data_for_dnn)
#             # cut.logger.info(f'Fit history:\n{hist.history}')
#             hist_list.append(hist)
#         return hist_list
#
#     def predict(self, x_data):
#         # If x_data contains only one sample, ie shape=(X,) reshape it
#         # to create one row such that after shape=(1, X)
#         # This is needed for the rescaling as well as for the dnn.predict().
#         only_one_sample = len(x_data.shape) == 1
#         if only_one_sample:
#             x_data = x_data.reshape(1, -1)
#
#         x_data_scaled = self.x_scaler.transform(x_data)
#         y_data_predicted_scaled_list = []
#         for dnn in self:
#             # y_data_predicted_scaled_list.append(dnn.predict(x_data_scaled).T[0].tolist())
#             y_data_predicted_scaled_list.append(dnn.predict(x_data_scaled))
#         y_data_predicted_scaled = np.concatenate(y_data_predicted_scaled_list, axis=1)
#         # import IPython; IPython.embed(); sys.exit()
#         y_data_predicted = self.y_scaler.inverse_transform(y_data_predicted_scaled)
#
#         # Don't return an array containing only 1 element, ie shape=(1, X)
#         # but return an array of shape=(X,)
#         if only_one_sample:
#             return y_data_predicted[0]
#         else:
#             return y_data_predicted
#
#     def save(self, save_dir):
#         pass

# def nb_of_cubic_coeff(n):
#     """
#     Returns the number of coefficients needed to define a cubic function which has `n` variables / input parameters.
#
#     Parameters:
#     ----------
#     n : int
#         number of parameters
#     """
#
#     m_fit = int(1 + n + n*(n+1)/2 + n*(n+1)*(n+2)/6)
#     return m_fit

# def jac_cubic_QR(x, m):
#     """
#     Expression of the cubic jacobian used in QR routine
#
#     Parameters
#     ----------
#     x : array_like.
#         Position array in parameter space.
#     m : integer.
#         Number of parameters.
#     Returns
#     -------
#     afunc : array_like.
#         Vector containing the polynomial values of the cubic fit at position x. If x is of length m, then afunc should be of length 1 + m + m(m+1)/2 + m(m+1)(m+2)/6. 220 for m = 9.
#     """
#     l = 0
#     m_fit = nb_of_cubic_coeff(m)
#     afunc = [np.longdouble(0) for i in range(m_fit)]
#     for i in range(m):
#         afunc[l] = x[i]
#         l+=1
#     for i in range(m):
#         for j in range(i,m):
#             afunc[l] = x[i] * x[j]
#             l+=1
#     for i in range(m):
#         for j in range(i,m):
#             for k in range(j,m):
#                 afunc[l] = x[i] * x[j] * x[k]
#                 l+=1
#     afunc[l] = 1.0
#     return afunc



########## TESTS I DID IN QR_FittingMethod_Gradients() FUNCTION

    # # Note: here I do a concatenation to keep the same structure as Yann's for fit_coeff_global, ie a flat array of 9x220 coeffs.
    # dlogL_fit_transpose = dlogL_fit.T # has shape (9, n_pt_fit)
    # fit_coeff_global = np.zeros((0,m_fit))
    # for j in range(sampler_dict['n_dim']):
    #     coeff_GSL = np.dot(pseudo_inverse, dlogL_fit_transpose[j]) # has shape (220,)
    #     # import IPython; IPython.embed()
    #     fit_coeff_global = np.concatenate((fit_coeff_global, coeff_GSL.reshape((1,m_fit))))

    #
    # fit_coeff_global_Yann_from_file = np.loadtxt('/Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/originalCCodeCompile/output_data/BNS_HMC_CoeffFit_1.dat')
    # Q_Yann = np.loadtxt('/Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/originalCCodeCompile/output_data/Q_from_QR_decomp.dat')
    # R_Yann = np.loadtxt('/Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/originalCCodeCompile/output_data/R_from_QR_decomp.dat')
    # dlogL_fit_Yann = np.loadtxt('/Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/originalCCodeCompile/output_data/dlogL_fit_1.dat').T
    #
    # R_Yann_r = R_Yann[:m_fit, :]
    # Q_Yann_r = Q_Yann[:, :m_fit]
    # # np.allclose(Q_Yann_r @ R_Yann_r, q @ r)
    # u_GSL_Yann = Q_Yann_r @ R_Yann_r
    #
    # diff_u_matrix = np.abs((cubic_jacobian_matrix - u_GSL_Yann)/u_GSL_Yann)
    # diff_u = np.max(diff_u_matrix) * 100
    #
    # diff_q_matrix = np.abs((q - Q_Yann_r)/Q_Yann_r)
    # diff_q = np.max(diff_q_matrix) * 100
    #
    # diff_r_matrix = np.abs((r - R_Yann_r)/R_Yann_r) # contains 'nan' values because division by 0
    # diff_r_matrix[np.where(np.isnan(diff_r_matrix))] = 0
    # diff_r = np.max(diff_r_matrix) * 100
    #
    # diff_LogP_matrix = np.abs((dlogL_fit - dlogL_fit_Yann)/dlogL_fit_Yann)
    # diff_LogP = np.max(diff_LogP_matrix) * 100
    #
    # fit_coeff_global = pseudo_inverse @ dlogL_fit
    # fit_coeff_global = fit_coeff_global.T
    #
    # diff_coeff_py_Yff_matrix = np.abs((fit_coeff_global - fit_coeff_global_Yann_from_file)/fit_coeff_global_Yann_from_file)
    # diff_coeff_py_Yff = np.max(diff_coeff_py_Yff_matrix) * 100
    #
    # pseudo_inverse_Yann = np.linalg.inv(R_Yann_r) @ Q_Yann_r.T
    # fit_coeff_global_Yann_computed = pseudo_inverse_Yann @ dlogL_fit
    # fit_coeff_global_Yann_computed = fit_coeff_global_Yann_computed.T
    #
    # diff_coeff_Yc_Yff_matrix = np.abs((fit_coeff_global_Yann_computed - fit_coeff_global_Yann_from_file)/fit_coeff_global_Yann_from_file)
    # diff_coeff_Yc_Yff = np.max(diff_coeff_Yc_Yff_matrix) * 100
    #
    # diff_coeff_Yc_py_matrix = np.abs((fit_coeff_global_Yann_computed - fit_coeff_global)/fit_coeff_global)
    # diff_coeff_Yc_py = np.max(diff_coeff_Yc_py_matrix) * 100
    #
    #
    # # Now doing the same but using exactly Yann's dlogL_fit: dlogL_fit_Yann instead of mine
    # dlogL_fit_copy = dlogL_fit.copy()
    # dlogL_fit = dlogL_fit_Yann.copy()
    # # dlogL_fit = dlogL_fit_copy
    #
    # fit_coeff_global = pseudo_inverse @ dlogL_fit
    # fit_coeff_global = fit_coeff_global.T
    #
    # diff_coeff_py_Yff_matrix = np.abs((fit_coeff_global - fit_coeff_global_Yann_from_file)/fit_coeff_global_Yann_from_file)
    # diff_coeff_py_Yff = np.max(diff_coeff_py_Yff_matrix) * 100
    #
    # pseudo_inverse_Yann = np.linalg.inv(R_Yann_r) @ Q_Yann_r.T
    # fit_coeff_global_Yann_computed = pseudo_inverse_Yann @ dlogL_fit
    # fit_coeff_global_Yann_computed = fit_coeff_global_Yann_computed.T
    #
    # diff_coeff_Yc_Yff_matrix = np.abs((fit_coeff_global_Yann_computed - fit_coeff_global_Yann_from_file)/fit_coeff_global_Yann_from_file)
    # diff_coeff_Yc_Yff = np.max(diff_coeff_Yc_Yff_matrix) * 100
    #
    # diff_coeff_Yc_py_matrix = np.abs((fit_coeff_global_Yann_computed - fit_coeff_global)/fit_coeff_global)
    # diff_coeff_Yc_py = np.max(diff_coeff_Yc_py_matrix) * 100
    #
    # r_py = cubic_jacobian_matrix @ fit_coeff_global.T - dlogL_fit
    # r_py_2 = np.diag(r_py.T @ r_py)
    #
    # r_Yff = cubic_jacobian_matrix @ fit_coeff_global_Yann_from_file.T - dlogL_fit
    # r_Yff_2 = np.diag(r_Yff.T @ r_Yff)
    #
    # r_Yc = cubic_jacobian_matrix @ fit_coeff_global_Yann_computed.T - dlogL_fit
    # r_Yc_2 = np.diag(r_Yc.T @ r_Yc)

    #
    # dlogL_fit = dlogL_fit_Yann.copy()
    # fit_coeff_global = pseudo_inverse @ dlogL_fit
    # fit_coeff_global = fit_coeff_global.T
