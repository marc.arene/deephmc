# import IPython; IPython.embed();
import numpy as np
# from bilby.gw.detector import Interferometer as bilby_ifo
# from bilby.gw.calibration import Recalibrate
#
# class Interferometer(bilby_ifo):
#     # def __init__(self, bilby_ifo):
#     #     bilby_ifo.__init__(bilby_ifo.name, bilby_ifo.power_spectral_density, bilby_ifo.minimum_frequency, bilby_ifo.maximum_frequency, bilby_ifo.length, bilby_ifo.latitude, bilby_ifo.longitude, bilby_ifo.elevation, bilby_ifo.xarm_azimuth, bilby_ifo.yarm_azimuth, bilby_ifo.xarm_tilt, bilby_ifo.yarm_tilt, bilby_ifo.calibration_model)
#
#     @property
#     def psd_array(self):
#         return self._psd_array
#
#     @psd_array.setter
#     def psd_array(self, psd_array):
#         self._psd_array = psd_array
#
#     @property
#     def inverse_psd_array(self):
#         return self._inverse_psd_array
#
#     @inverse_psd_array.setter
#     def inverse_psd_array(self, inverse_psd_array):
#         self._inverse_psd_array = inverse_psd_array
#
#     @property
#     def fd_strain(self):
#         """ Same as `.frequency_domain_strain` except a setter is defined, hence once set there is no `* .frequency_mask` as done in  `.strain_data.frequency_domain_strain` """
#         return self._fd_strain
#
#     @fd_strain.setter
#     def fd_strain(self, fd_strain):
#         self._fd_strain = fd_strain



def get_detector_response_geocent(ifo, waveform_polarizations, parameters):

    signal = {}
    det_response = {}
    # import ipdb; ipdb.set_trace()
    # [~0.33ms = 2 x (0.025ms + 0.140ms)]
    for mode in waveform_polarizations.keys():
        # [0.025ms]
        det_response[mode] = ifo.antenna_response(
            parameters['ra'],
            parameters['dec'],
            parameters['geocent_time'],
            parameters['psi'], mode)
        # [0.140ms]
        signal[mode] = waveform_polarizations[mode] * det_response[mode]

    # [~0.25ms]
    signal_ifo = sum(signal.values())

    # [0.25ms]
    # This line is meant to make sure that we don't take into account frequency below `minimum_frequency` and above `maximum_frequency`
    # signal_ifo *= ifo.strain_data.frequency_mask

    return signal_ifo

def get_fd_time_translation(ifo, parameters):

    # [0.009ms]
    time_shift = ifo.time_delay_from_geocenter(
        parameters['ra'], parameters['dec'], parameters['geocent_time'])

    # It is important to split the computation of `dt` with those two lines !
    # Indeed since `parameters['geocent_time']` and `ifo.strain_data.start_time` are ~1e9, their difference needs to be computed first before adding `time_shift` (which is ~1e-2). Otherwise variations in `time_shift` of the order of 1e-6 are not taken into account in `dt`
    dt_geocent = parameters['geocent_time'] - ifo.strain_data.start_time
    dt = dt_geocent + time_shift

    # phase_shift = 0
    # phase_shift += np.angle((0.5*(1+np.cos(parameters['iota'])**2)*det_response['plus']) - 1j * np.cos(parameters['iota'])*det_response['cross'])
    # phase_shift += np.pi

    # import IPython; IPython.embed();
    # [~3.8ms]
    # signal_ifo = signal_ifo * np.exp(-1j * 2 * np.pi * dt * ifo.frequency_array)

    # # [~0.57ms]
    phase = -1j * 2 * np.pi * dt * ifo.frequency_array
    # # [~3ms]
    fd_time_translation = np.exp(phase)

    return fd_time_translation

def get_detector_response(ifo, waveform_polarizations, parameters):
    """
    Same function as `bilby.detector.get_detector_response()` but with the addition of a `phase_shift` that compensates for how hmc adds the two polarizations. The +np.pi addition however is completely empirical to me at the moment, I don't understand why I had to add it for the hmc and bilby waveforms to overlap.

    Get the detector response for a particular waveform

    Parameters
    -------
    waveform_polarizations: dict
        polarizations of the waveform
    parameters: dict
        parameters describing position and time of arrival of the signal

    Returns
    -------
    array_like: A 3x3 array representation of the detector response (signal observed in the interferometer)
    """
    signal_ifo = get_detector_response_geocent(ifo, waveform_polarizations, parameters)

    fd_time_translation = get_fd_time_translation(ifo, parameters)

    # # [~0.2ms]
    signal_ifo = signal_ifo * fd_time_translation


    # [0.5ms]
    # If there is no calibration model set then this line just multiplies by an array of ones...
    # signal_ifo *= ifo.calibration_model.get_calibration_factor(ifo.frequency_array, prefix='recalib_{}_'.format(ifo.name), **parameters)
    # import IPython; IPython.embed();
    return signal_ifo
