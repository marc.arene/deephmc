def get_analytical_timing_for_dlogL(t_h_source=9, t_get_detector_response_geocent=1.3, t_get_fd_time_translation=4, t_noise_weighted_inner_product=0.7, t_dh=0.7, nb_of_ifos=3, n_traj_phase1=1000, length_num_traj=200):
    """
    Analytical estimation in millisecond of the time it should take to compute one numerical gradient `dlogL` for the 9 parameters. This is based on the following description of what is implemented in bilby_waveform.dlogL_ThreeDetectors():

	- dlogL:
    	- h_dh:
    		- phi_c, ln(D):			             1 x (h_source + 3 x (ifo.get_detector_response_geocent() + ifo.get_fd_time_translation()))
    		- cosi, lnMc, lnmu:              3 x 2 x (h_source + 3 x ifo.get_detector_response_geocent())
    		- psi, sin(dec), ra, ln(tc):     4 x 3 x (ifo.get_detector_response_geocent() + ifo.get_fd_time_translation())
    	- s_dh - h_dh: 				             9 x 3 x 2 x noise_weighted_inner_product()

    With the present default values, the result is dlogL = 377ms, when timing it as a whole with %%timeit gives 450ms and averaging over 10 trajectories gives 460ms.

    """

    phi_c_and_lnD = t_h_source + nb_of_ifos * (t_get_detector_response_geocent + t_get_fd_time_translation)

    # Factor 2 here because we do central differencing
    cosi = 2 * (t_h_source + nb_of_ifos * t_get_detector_response_geocent)
    lnMc = cosi
    lnmu = cosi

    # No factor 2 because we do forward differencing
    psi = nb_of_ifos * (t_get_detector_response_geocent + t_get_fd_time_translation)
    sin_dec = psi
    ra = psi

    # Factor 2 here because we do central differencing again
    ln_tc = 2 * psi

    # Take into account the time it takes to compute the difference of two large arrays: (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset)
    nine_dh = 9 * nb_of_ifos * t_dh

    h_dh =  phi_c_and_lnD + cosi + lnMc + lnmu + psi + sin_dec + ra + ln_tc + nine_dh

    s_dh_minus_h_dh = 9 * nb_of_ifos * t_noise_weighted_inner_product

    dlogL = h_dh + s_dh_minus_h_dh

    one_traj = length_num_traj * dlogL / n_traj_phase1 # in seconds
    phase_1 = n_traj_phase1 * one_traj # in seconds

    print("dlogL = {:.0f}ms".format(dlogL))
    print("1 trajectory of {} steps = {:.1f}s".format(length_num_traj, one_traj))
    print("Phase 1 with {} trajectories = {:.1f}hrs".format(n_traj_phase1, phase_1/3600))
    # import IPython; IPython.embed()


if __name__=='__main__':
    get_analytical_timing_for_dlogL()
