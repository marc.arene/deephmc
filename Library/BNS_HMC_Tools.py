import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np
from sklearn import neighbors
import itertools as it

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.sklearn_fit as sklf
import Library.Fit_NR as fnr

import core.utils as cut

import Codes.dlogL as dlogL

damping_epsilon = False

def NumericalGradientLeapfrog_ThreeDetectors(q_pos_0, logL_0, dlogL_0, randGen, epsilon, lengthT, traj_index, sampler):
	"""

	"""
	#	Initialization of the momentum: draw from a gaussian distribution with mean 0 and sigma=1
	p_mom_0 = randGen.normal(loc=0, scale=1.0, size=sampler.n_dim)

	# Main HMC routine with numerical gradients
	q_pos, p_mom, dlogL_pos, pt_fit_traj, dlogL_fit_traj, BreakIndic, H_p_logL_logP_traj, pmom_traj, dlogTrajPi_traj = NumericalGradientLeapfrog_ThreeDetectors_Trajectory(q_pos_0, p_mom_0, dlogL_0, logL_0, epsilon, lengthT, traj_index, sampler)


	# Compute the Log-likelihood at the end if there was no NaN
	if BreakIndic==0:
		# Hamiltonian at the start of the trajectory
		qpos_dict_0 = sampler.q_pos_to_parameters_dict(q_pos_0)
		logTrajPi_0 = sampler.traj_prior_gradients.traj_priors.ln_prob(qpos_dict_0)
		H_0 = computeHamiltonian(p_mom_0, logL_0, logTrajPi_0)

		# Code snippet only used when the `--plot_traj` option was set for the run.
		# It records data for each trajectory in a `/plot_traj` specific folder which
		# can then be plotted using the /Tests/plot_trajectory.py script.
		# Here we simply add the starting point of the trajectory.
		if RUN_CONST.plot_traj:
			H_p_logL_logP_0 = [H_0, 0.5 * (p_mom_0**2).sum(), logL_0, logTrajPi_0]
			sampler.dict_of_trajectories['H_p_logL_logP'].append(H_p_logL_logP_0)
			sampler.dict_of_trajectories['pmom_trajs'].append(p_mom_0.tolist())
			q_pos_traj_0 = sampler.q_pos_to_q_pos_traj(q_pos_0)
			sampler.dict_of_trajectories['qpos_traj'].append(q_pos_traj_0.tolist())
			sampler.dict_of_trajectories['dlogL'].append(dlogL_0)
			dlogTrajPi_0 = sampler.traj_prior_gradients.calculate_dlogPi_np(qpos_dict_0)
			sampler.dict_of_trajectories['dlogTrajPi_traj'].append(dlogTrajPi_0.tolist())

		# Hamiltonian at the end of the trajectory
		# sampler.update_likelihood_parameters_from_q_pos(q_pos)
		qpos_dict_final = sampler.q_pos_to_parameters_dict(q_pos)
		sampler.likelihood_gradient.likelihood.parameters.update(qpos_dict_final)
		logL_final = sampler.likelihood.log_likelihood_ratio()
		logTrajPi_final = sampler.traj_prior_gradients.traj_priors.ln_prob(qpos_dict_final)
		H = computeHamiltonian(p_mom, logL_final, logTrajPi_final)

		# Compute the probability of acceptance for the trajectory
		proba_acc = np.exp(-(H-H_0))

		# draw random number
		a = randGen.uniform(low=0, high=1)

		if a < proba_acc:	# Accept position
			# Copy the values of the final position and gradient as a new starting position for the next trajectory of the HMC
			q_pos_0 = q_pos.copy()
			dlogL_0 = dlogL_pos.copy()

			logL_0 = logL_final							# update initial log-likelihood to final log-likelihood
			Accept = 1										# set acceptation flag to 1
		else:	# Reject position
			Accept = 0
	else:	# Reject position because of divergence and/or NaN
		Accept = 0
		proba_acc = 0
		a = 1


	# Code snippet only used when the `--plot_traj` option was set for the run.
	# It records data for each trajectory in a `/plot_traj` specific folder which
	# can then be plotted using the /Tests/plot_trajectory.py script
	if RUN_CONST.plot_traj:
		if BreakIndic!=0:
			H_p_logL_logP_0 = [0, 0, 0, 0]
			sampler.dict_of_trajectories['H_p_logL_logP'].append(0)
			sampler.dict_of_trajectories['pmom_trajs'].append([0]*sampler.n_dim)
			sampler.dict_of_trajectories['qpos_traj'].append([0]*sampler.n_dim)
			sampler.dict_of_trajectories['dlogL'].append(0)
			sampler.dict_of_trajectories['dlogTrajPi_traj'].append([0]*sampler.n_dim)
		for i in range(len(pt_fit_traj)):
			sampler.dict_of_trajectories['H_p_logL_logP'].append(H_p_logL_logP_traj[i])
			sampler.dict_of_trajectories['pmom_trajs'].append(pmom_traj[i])
			sampler.dict_of_trajectories['qpos_traj'].append(pt_fit_traj[i])
			sampler.dict_of_trajectories['dlogL'].append(dlogL_fit_traj[i])
			sampler.dict_of_trajectories['dlogTrajPi_traj'].append(dlogTrajPi_traj[i])
		sampler.dict_of_trajectories['lengths'].append(lengthT)

	return q_pos_0, logL_0, dlogL_0, pt_fit_traj, dlogL_fit_traj, Accept, a, proba_acc

def NumericalGradientLeapfrog_ThreeDetectors_Trajectory(q_pos_0, p_mom_0, dlogL_0, logL_0, epsilon, lengthT, traj_index, sampler):
	"""
	HMC routine for a single trajectory where the gradients are all computed numerically.

	"""

	BreakIndic = 0			# flag indicating if there is a NaN in the code

	# Initialise position and gradient
	q_pos_traj = sampler.q_pos_to_q_pos_traj(q_pos_0)
	p_mom = p_mom_0.copy()
	dlogL_pos = dlogL_0.copy()
	q_pos_dict = sampler.q_pos_to_parameters_dict(q_pos_0)
	dlogTrajPi = sampler.traj_prior_gradients.calculate_dlogPi_np(q_pos_dict)
	# breakpoint()
	pt_fit_traj = []
	dlogL_fit_traj = []
	H_p_logL_logP_traj = []
	pmom_traj = []
	dlogTrajPi_traj = []
	# Main trajectory loop.
	# Marc: Note that the first point of the trajectory is technically not recorded here, but it is the same as the last point of the previous trajectory
	if damping_epsilon:
		idx_Mc = sampler.search_parameter_keys.index('chirp_mass')
		idx_mu = sampler.search_parameter_keys.index('reduced_mass')
		Mc_traj_inv_func = sampler.search_trajectory_inv_functions[idx_Mc]
		mu_traj_inv_func = sampler.search_trajectory_inv_functions[idx_mu]
		eta_thres = 0.2499
		epsilon_orig = epsilon
	for i in range(lengthT):
		############################################
		#	1/2 step in momentum (leapfrog algorithm)
		############################################
		p_mom_new = p_mom + 0.5 * epsilon * (dlogL_pos + dlogTrajPi) * sampler.scales

		############################################
		# Full step in position (leapfrog algorithm). We move in fitted vector meaing that it is already written as jumps in log for masses and tc, and cosine/sine for inc/latitude
		############################################
		q_pos_traj_new = q_pos_traj.copy()
		q_pos_traj_new += epsilon * p_mom_new * sampler.scales

		q_pos_traj, p_mom, BreakIndic = sampler.remap_q_pos_traj_in_boundaries(q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon)
		if BreakIndic:
			cut.logger.info(f"   TRAJECTORY {traj_index} BROKE AT STEP i = {i}")
			if RUN_CONST.plot_traj:
				# Pad the rest of the trajectory with the last value of each measure
				# so that for plotting purposes we know each traj has the same nb of elements
				if i == 0:
					# H_p_logL_logP_traj = [sampler.dict_of_trajectories['H_p_logL_logP'][-1]] * lengthT
					# pmom_traj = [sampler.dict_of_trajectories['pmom_traj'][-1]] * lengthT
					# dlogTrajPi_traj = [sampler.dict_of_trajectories['dlogTrajPi_traj'][-1]] * lengthT
					# pt_fit_traj = [sampler.dict_of_trajectories['pt_fit_traj'][-1]] * lengthT
					# dlogL_fit_traj = [sampler.dict_of_trajectories['dlogL_fit_traj'][-1]] * lengthT
					H_p_logL_logP_traj = [[0,0,0,0]] * lengthT
					pmom_traj = [[0]*sampler.n_dim] * lengthT
					dlogTrajPi_traj = [[0]*sampler.n_dim] * lengthT
					pt_fit_traj = [[0]*sampler.n_dim] * lengthT
					dlogL_fit_traj = [[0]*sampler.n_dim] * lengthT
				else:
					H_p_logL_logP_traj = H_p_logL_logP_traj + [H_p_logL_logP_traj[-1]] * (lengthT - i)
					pmom_traj = pmom_traj + [pmom_traj[-1]] * (lengthT - i)
					dlogTrajPi_traj = dlogTrajPi_traj + [dlogTrajPi_traj[-1]] * (lengthT - i)
					pt_fit_traj = pt_fit_traj + [pt_fit_traj[-1]] * (lengthT - i)
					dlogL_fit_traj = dlogL_fit_traj + [dlogL_fit_traj[-1]] * (lengthT - i)
			break

		if damping_epsilon:
			# Damping epsilon when getting close to equal mass line boundary
			mc = Mc_traj_inv_func(q_pos_traj[idx_Mc])
			mu = mu_traj_inv_func(q_pos_traj[idx_mu])
			eta = paru.Mc_mu_to_eta(mc, mu)
			# if False:
			if eta > eta_thres:
				epsilon = epsilon_orig * ((0.25 - eta) / (0.25 - eta_thres))**0.5
			else:
				epsilon = epsilon_orig
			# cut.logger.info(f'                   {i}: eta = {eta:6f} - epsilon = {epsilon:.6f}')



		############################################
		#	1/2 step in momentum (leapfrog algorithm)
		############################################
		sampler.update_likelihood_parameters_from_q_pos_traj(q_pos_traj)
		dlogL_pos = sampler.likelihood_gradient.calculate_dlogL_np()
		q_pos_dict = sampler.likelihood.parameters
		dlogTrajPi = sampler.traj_prior_gradients.calculate_dlogPi_np(q_pos_dict)
		p_mom += 0.5 * epsilon * (dlogL_pos + dlogTrajPi) * sampler.scales


		pt_fit_traj.append(q_pos_traj.tolist())
		dlogL_fit_traj.append(dlogL_pos.tolist())

		if RUN_CONST.plot_traj:
			logL = sampler.likelihood_gradient.log_likelihood_at_point
			logTrajPi = sampler.traj_priors.ln_prob(q_pos_dict)
			H = computeHamiltonian(p_mom, logL, logTrajPi)
			H_p_logL_logP_traj.append([H, 0.5 * (p_mom**2).sum(), logL, logTrajPi])
			pmom_traj.append(p_mom.tolist())
			dlogTrajPi_traj.append(dlogTrajPi.tolist())

	q_pos = sampler.q_pos_traj_to_q_pos(q_pos_traj)

	return q_pos, p_mom, dlogL_pos, pt_fit_traj, dlogL_fit_traj, BreakIndic, H_p_logL_logP_traj, pmom_traj, dlogTrajPi_traj

def AnalyticalGradientLeapfrog_ThreeDetectors(q_pos_0, logL_0, dlogL_0, randGen, epsilon, lengthT, fit_method, oluts_fit, traj_index, sampler):
	"""
	HMC routine for a single trajectory where the gradients are all computed using the fit (cubic for good parameters, look-up table fit for the others)
	"""

	#	Initialization of the momentum: draw from a gaussian distribution with mean 0 and sigma=1
	p_mom_0 = randGen.normal(loc=0, scale=1.0, size=sampler.n_dim)

	# Main HMC routine with analytical gradients
	q_pos_traj, p_mom, dlogL_pos, BreakIndic = AnalyticalGradientLeapfrog_ThreeDetectors_Trajectory(q_pos_0, p_mom_0, dlogL_0, epsilon, lengthT, fit_method, oluts_fit, traj_index, sampler)

	# Compute the Log-likelihood at the end if there was no NaN
	if BreakIndic==0:
		# Hamiltonian at the start of the trajectory
		qpos_dict_0 = sampler.q_pos_to_parameters_dict(q_pos_0)
		logTrajPi_0 = sampler.traj_prior_gradients.traj_priors.ln_prob(qpos_dict_0)
		H_0 = computeHamiltonian(p_mom_0, logL_0, logTrajPi_0)

		# Hamiltonian at the end of the trajectory
		# sampler.update_likelihood_parameters_from_q_pos_traj(q_pos_traj)
		# logL_final = sampler.likelihood.log_likelihood_ratio()
		# H = computeHamiltonian(p_mom, logL_final)
		qpos_dict_final = sampler.q_pos_traj_to_parameters_dict(q_pos_traj)
		sampler.likelihood_gradient.likelihood.parameters.update(qpos_dict_final)
		logL_final = sampler.likelihood.log_likelihood_ratio()
		logTrajPi_final = sampler.traj_prior_gradients.traj_priors.ln_prob(qpos_dict_final)
		H = computeHamiltonian(p_mom, logL_final, logTrajPi_final)

		# Compute the probability of acceptance for the trajectory
		proba_acc = np.exp(-(H-H_0))

		# draw random number
		a = randGen.uniform(low=0, high=1)

		if a < proba_acc:	# Accept position
			# Copy the values of the final position and gradient as a new starting position for the next trajectory of the HMC
			q_pos_0 = sampler.q_pos_traj_to_q_pos(q_pos_traj)
			dlogL_0 = dlogL_pos.copy()

			logL_0 = logL_final							# update initial log-likelihood to final log-likelihood
			Accept = 1										# set acceptation flag to 1

		else:	# Reject position
			Accept = 0
	else:	# Reject position because of divergence and/or NaN
		Accept = 0
		proba_acc = 0
		a = 1

	return q_pos_0, logL_0, dlogL_0, Accept, a, proba_acc

def AnalyticalGradientLeapfrog_ThreeDetectors_Trajectory(q_pos_0, p_mom_0, dlogL_0, epsilon, lengthT, fit_method, oluts_fit, traj_index, sampler):
	"""
	HMC routine for a single trajectory where the gradients are all computed analytically.

	"""

	BreakIndic = 0			# flag indicating if there is a NaN in the code

	# Initialise position, fitting position and gradient. Fitted position vector is used in the fitting routine while position vector is tailored for waveform generation
	q_pos_traj = sampler.q_pos_to_q_pos_traj(q_pos_0)
	p_mom = p_mom_0.copy()
	dlogL_pos = dlogL_0.copy()
	q_pos_dict = sampler.q_pos_to_parameters_dict(q_pos_0)
	dlogTrajPi = sampler.traj_prior_gradients.calculate_dlogPi_np(q_pos_dict)

	# Marc: use lengthT-1 loops because there are (lengthT-1) steps for a trajectory containing lengthT points
	if damping_epsilon:
		idx_Mc = sampler.search_parameter_keys.index('chirp_mass')
		idx_mu = sampler.search_parameter_keys.index('reduced_mass')
		Mc_traj_inv_func = sampler.search_trajectory_inv_functions[idx_Mc]
		mu_traj_inv_func = sampler.search_trajectory_inv_functions[idx_mu]
		eta_thres = 0.2499
		epsilon_orig = epsilon
	for i in range(1, lengthT):
		############################################
		#	1/2 step in momentum (leapfrog algorithm)
		############################################
		p_mom_new = p_mom + 0.5 * epsilon * (dlogL_pos + dlogTrajPi) * sampler.scales

		############################################
		# Full step in position (leapfrog algorithm). We move in fitted vector meaing that it is already written as jumps in log for masses and tc, and cosine/sine for inc/latitude
		############################################
		q_pos_traj_new = q_pos_traj.copy()
		q_pos_traj_new += epsilon * p_mom_new * sampler.scales

		q_pos_traj, p_mom, BreakIndic = sampler.remap_q_pos_traj_in_boundaries(q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon)
		if BreakIndic:
			cut.logger.info(f"   TRAJECTORY {traj_index} BROKE AT STEP i = {i}")
			break

		if damping_epsilon:
			mc = Mc_traj_inv_func(q_pos_traj[idx_Mc])
			mu = mu_traj_inv_func(q_pos_traj[idx_mu])
			eta = paru.Mc_mu_to_eta(mc, mu)
			# if False:
			if eta > eta_thres:
				epsilon = epsilon_orig * ((0.25 - eta) / (0.25 - eta_thres))**0.5
			else:
				epsilon = epsilon_orig
			# cut.logger.info(f'                   {i}: eta = {eta:6f} - epsilon = {epsilon:.6f}')




		############################################
		#	1/2 step in momentum (leapfrog algorithm)
		############################################
		# Compute gradients for all parameters with global fit
		dlogL_pos = fit_method.predict(q_pos_traj)
		q_pos_dict = sampler.q_pos_traj_to_parameters_dict(q_pos_traj)
		dlogTrajPi = sampler.traj_prior_gradients.calculate_dlogPi_np(q_pos_dict)
		# Update gradients for which global fit doesn't work with
		# OLUTs fit
		if fit_method.__class__ == fnr.CubicFitRegressor:
			dlogL_pos = oluts_fit.fit_predict_update(q_pos_traj, dlogL_pos)

		p_mom += 0.5 * epsilon * (dlogL_pos + dlogTrajPi) * sampler.scales


	return q_pos_traj, p_mom, dlogL_pos, BreakIndic

def HybridGradientLeapfrog_ThreeDetectors(q_pos_0, logL_0, dlogL_0, randGen, epsilon, lengthT, fit_method, traj_index, sampler):
	"""
	HMC routine for a single trajectory where the gradients are computed numerically for the bad parameters and analytically for the good
	"""
	#	Initialization of the momentum: draw from a gaussian distribution with mean 0 and sigma=1
	p_mom_0 = randGen.normal(loc=0, scale=1.0, size=sampler.n_dim)

	# Main HMC routine with numerical gradients
	q_pos, p_mom, dlogL_pos, pt_fit_traj, dlogL_fit_traj, BreakIndic, H_p_logL_logP_traj, pmom_traj = HybridGradientLeapfrog_ThreeDetectors_Trajectory(q_pos_0, p_mom_0, dlogL_0, epsilon, lengthT, fit_method, traj_index, sampler)

	# Compute the Log-likelihood at the end if there was no NaN
	if BreakIndic==0:
		# Hamiltonian at the start of the trajectory
		qpos_dict_0 = sampler.q_pos_to_parameters_dict(q_pos_0)
		logTrajPi_0 = sampler.traj_prior_gradients.traj_priors.ln_prob(qpos_dict_0)
		H_0 = computeHamiltonian(p_mom_0, logL_0, logTrajPi_0)

		# Hamiltonian at the end of the trajectory
		# sampler.update_likelihood_parameters_from_q_pos(q_pos)
		# logL_final = sampler.likelihood.log_likelihood_ratio()
		# H = computeHamiltonian(p_mom, logL_final)

		qpos_dict_final = sampler.q_pos_to_parameters_dict(q_pos)
		sampler.likelihood_gradient.likelihood.parameters.update(qpos_dict_final)
		logL_final = sampler.likelihood.log_likelihood_ratio()
		logTrajPi_final = sampler.traj_prior_gradients.traj_priors.ln_prob(qpos_dict_final)
		H = computeHamiltonian(p_mom, logL_final, logTrajPi_final)

		# Compute the probability of acceptance for the trajectory
		proba_acc = np.exp(-(H-H_0))

		# draw random number
		a = randGen.uniform(low=0, high=1)

		if a < proba_acc:	# Accept position
			# Copy the values of the final position and gradient as a new starting position for the next trajectory of the HMC
			q_pos_0 = q_pos.copy()
			dlogL_0 = dlogL_pos.copy()

			logL_0 = logL_final							# update initial log-likelihood to final log-likelihood
			Accept = 1										# set acceptation flag to 1
		else:	# Reject position
			Accept = 0
	else:	# Reject position because of divergence and/or NaN
		Accept = 0
		proba_acc = 0
		a = 1

	return q_pos_0, logL_0, dlogL_0, pt_fit_traj, dlogL_fit_traj, Accept, a, proba_acc

def HybridGradientLeapfrog_ThreeDetectors_Trajectory(q_pos_0, p_mom_0, dlogL_0, epsilon, lengthT, fit_method, traj_index, sampler):
	"""
	HMC routine for a single trajectory where the gradients are all computed numerically.

	"""

	BreakIndic = 0			# flag indicating if there is a NaN in the code

	# Initialise position and gradient
	q_pos_traj = sampler.q_pos_to_q_pos_traj(q_pos_0)
	p_mom = p_mom_0.copy()
	dlogL_pos = dlogL_0.copy()
	q_pos_dict = sampler.q_pos_to_parameters_dict(q_pos_0)
	dlogTrajPi = sampler.traj_prior_gradients.calculate_dlogPi_np(q_pos_dict)
	pt_fit_traj = []
	dlogL_fit_traj = []
	H_p_logL_logP_traj = []
	pmom_traj = []


	# Main trajectory loop.
	# Marc: use lengthT-1 loops because there are (lengthT-1) steps for a trajectory containing lengthT points
	if damping_epsilon:
		idx_Mc = sampler.search_parameter_keys.index('chirp_mass')
		idx_mu = sampler.search_parameter_keys.index('reduced_mass')
		Mc_traj_inv_func = sampler.search_trajectory_inv_functions[idx_Mc]
		mu_traj_inv_func = sampler.search_trajectory_inv_functions[idx_mu]
		eta_thres = 0.2499
		epsilon_orig = epsilon
	for i in range(lengthT):
		############################################
		#	1/2 step in momentum (leapfrog algorithm)
		############################################
		p_mom_new = p_mom + 0.5 * epsilon * (dlogL_pos + dlogTrajPi) * sampler.scales

		############################################
		# Full step in position (leapfrog algorithm). We move in fitted vector meaing that it is already written as jumps in log for masses and tc, and cosine/sine for inc/latitude
		############################################
		q_pos_traj_new = q_pos_traj.copy()
		q_pos_traj_new += epsilon * p_mom_new * sampler.scales

		q_pos_traj, p_mom, BreakIndic = sampler.remap_q_pos_traj_in_boundaries(q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon)
		if BreakIndic:
			cut.logger.info(f"   TRAJECTORY {traj_index} BROKE AT STEP i = {i}")
			break

		if damping_epsilon:
			mc = Mc_traj_inv_func(q_pos_traj[idx_Mc])
			mu = mu_traj_inv_func(q_pos_traj[idx_mu])
			eta = paru.Mc_mu_to_eta(mc, mu)
			# if False:
			if eta > eta_thres:
				epsilon = epsilon_orig * ((0.25 - eta) / (0.25 - eta_thres))**0.5
			else:
				epsilon = epsilon_orig
			# cut.logger.info(f'                   {i}: eta = {eta:6f} - epsilon = {epsilon:.6f}')

		############################################
		#	1/2 step in momentum (leapfrog algorithm)
		############################################
		# Good global parameters gradient fit for: (Phic, Mc, Mu, theta, phi, tc)
		dlogL_pos = fit_method.predict(q_pos_traj)
		dlogL_pos = sampler.get_dlogL_hybrid_from_dlogL_ana(q_pos_traj, dlogL_pos)
		q_pos_dict = sampler.q_pos_traj_to_parameters_dict(q_pos_traj)
		dlogTrajPi = sampler.traj_prior_gradients.calculate_dlogPi_np(q_pos_dict)
		p_mom += 0.5 * epsilon * (dlogL_pos + dlogTrajPi) * sampler.scales

		pt_fit_traj.append(q_pos_traj.tolist())
		dlogL_fit_traj.append(dlogL_pos.tolist())

	q_pos = sampler.q_pos_traj_to_q_pos(q_pos_traj)

	return q_pos, p_mom, dlogL_pos, pt_fit_traj, dlogL_fit_traj, BreakIndic, H_p_logL_logP_traj, pmom_traj

def computeHamiltonian(p_mom, logL, logTrajPi):
	"""
	Routine that computes the Hamiltonian given the momenta and the log-likelihood
	"""
	return 0.5 * (p_mom ** 2).sum() - logL - logTrajPi
