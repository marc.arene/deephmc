import sys
sys.path.append('../')
import Library.python_utils as pu
import numpy as np


def get_bases_params(roq_directory = '/Users/marcarene/roq/ROQ_data/IMRPhenomPv2/'):

    segment_lenghts = ['4s', '8s', '16s', '32s', '64s', '128s']

    params_bases = {}

    for segment in segment_lenghts:
        # params_bases[segment] = np.genfromtxt(roq_directory + segment + '/' + "params.dat", names=True)
        params_bases[segment] = {}
        params_of_segment = np.genfromtxt(roq_directory + segment + '/' + "params.dat", names=True)
        for key in params_of_segment.dtype.names:
            params_bases[segment][key] = params_of_segment[key] * 1

    return params_bases

def find_best_segment_from_chirp_mass(chirp_mass, params_bases):
    """
    Return the best ROQ segment adapted to the chirp_mass which has triggered together with the scale factor needed to apply to that segment if the chirp mass is out of the segment range.

    Since segments overlap in chirp mass ranges, the best segment is taken to be the one where the chirp mass triggered is closest to its average chirp mass range value.
    """

    min_distance = 999 #randomly big number
    best_segment = ''
    segments_scale_factors = {}
    for segment in params_bases.keys():
        chirp_mass_range_average = (params_bases[segment]['chirpmassmin'] + params_bases[segment]['chirpmassmax']) / 2
        distance_to_average_chirp_mass = abs(chirp_mass - chirp_mass_range_average)
        if distance_to_average_chirp_mass < min_distance:
            min_distance = distance_to_average_chirp_mass
            best_segment = segment
        segments_scale_factors[segment] = chirp_mass_range_average / chirp_mass

    # If the chirp mass triggered is in the range of chirp masses of the best segment then we leave the scale factor to one
    if params_bases[best_segment]['chirpmassmin'] < chirp_mass  and chirp_mass < params_bases[best_segment]['chirpmassmax']:
        scale_factor = 1
    else:
        scale_factor = round(segments_scale_factors[best_segment], 1)

    # By convention, sampling_frequency and duration have to multiply to an integer.
    # Otherwise bilby will raise an error in core.utils._check_legal_sampling_frequency_and_duration()
    # And it happens, for instance with scale_factor = 1.9 that these rescaled duration and samp_f don't multiply to an int...
    duration = params_bases[best_segment]['seglen'] / scale_factor
    sampling_frequency = 2 * params_bases[best_segment]['fhigh'] * scale_factor
    num = sampling_frequency * duration
    _TOL = 14
    while np.abs(num - np.round(num)) > 10**(-_TOL):
        scale_factor += 0.1
        duration = params_bases[best_segment]['seglen'] / scale_factor
        sampling_frequency = 2 * params_bases[best_segment]['fhigh'] * scale_factor
        num = sampling_frequency * duration
        if chirp_mass < params_bases[best_segment]['chirpmassmin']/scale_factor or params_bases[best_segment]['chirpmassmax']/scale_factor < chirp_mass:
            raise ValueError("Could not find a proper scale factor for the ROQ basis.")

    return params_bases[best_segment], scale_factor

def get_best_segment_params_from_chirp_mass(chirp_mass, roq_directory = '/Users/marcarene/roq/ROQ_data/IMRPhenomPv2/'):

    params_bases = get_bases_params(roq_directory)
    params_best_basis, scale_factor = find_best_segment_from_chirp_mass(chirp_mass, params_bases)

    return params_best_basis, scale_factor

def rescale_params(params, scale_factor):
    rescaled_params = params.copy()
    rescaled_params['flow'] *= scale_factor
    rescaled_params['fhigh'] *= scale_factor
    rescaled_params['seglen'] /= scale_factor
    rescaled_params['chirpmassmin'] /= scale_factor
    rescaled_params['chirpmassmax'] /= scale_factor
    rescaled_params['compmin'] /= scale_factor
    return rescaled_params

if __name__=='__main__':

    chirp_mass = 1.18

    params_bases = get_bases_params()

    pu.print_dict(params_bases)

    params_best_basis, scale_factor = find_best_segment_from_chirp_mass(chirp_mass, params_bases)

    print("\nBest segment for chirp_mass = {} is :".format(chirp_mass))
    pu.print_dict(params_best_basis)
    print("With a scale factor of {} to apply to the basis.".format(scale_factor))

    import IPython; IPython.embed()
