import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import numpy as np

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.CONST as CONST
from bilby.gw import conversion as conv



def injection_parameters_to_q_pos(injection_parameters, start_time):
    q_pos = np.zeros(9)
    if 'iota' in injection_parameters.keys():
        q_pos[0] = injection_parameters['iota']
    else:
        q_pos[0] = injection_parameters['theta_jn']
    # q_pos[1] = injection_parameters['phase'] * 2
    q_pos[1] = injection_parameters['phase']
    q_pos[2] = injection_parameters['psi']
    q_pos[3] = injection_parameters['luminosity_distance']
    q_pos[4] = injection_parameters['chirp_mass']
    q_pos[5] = injection_parameters['reduced_mass']
    q_pos[6] = injection_parameters['dec']
    q_pos[7] = injection_parameters['ra']
    q_pos[8] = injection_parameters['geocent_time']-start_time
    # q_pos[9] = injection_parameters['mass_1']
    # q_pos[10] = injection_parameters['mass_2']
    # q_pos[11] = injection_parameters['total_mass']
    # q_pos[12] = injection_parameters['symmetric_mass_ratio']

    return q_pos

def q_pos_to_dictionary(q_pos, start_time):
    """
    True version of this conversion function. Using in trajectories the version bellow so as to run effectively the hmc over a subset of parameters, ie dimensions.
    The point of this is to target which parameter(s) mess-up the acceptance rate.
    """
    total_mass, symmetric_mass_ratio, mass_1, mass_2 = Mc_and_mu_to_M_eta_m1_and_m2(q_pos[4], q_pos[5])
    dictionary = dict()
    dictionary['iota'] = q_pos[0]
    dictionary['theta_jn'] = q_pos[0]
    dictionary['phase'] = q_pos[1]
    dictionary['psi'] = q_pos[2]
    dictionary['luminosity_distance'] = q_pos[3]
    dictionary['chirp_mass'] = q_pos[4]
    dictionary['reduced_mass'] = q_pos[5]
    dictionary['dec'] = q_pos[6]
    dictionary['ra'] = q_pos[7]
    dictionary['geocent_time'] = q_pos[8] + start_time
    dictionary['mass_1'] = mass_1
    dictionary['mass_2'] = mass_2
    dictionary['total_mass'] = total_mass
    dictionary['symmetric_mass_ratio'] = symmetric_mass_ratio
    dictionary['chi_1'] = 0 # bilby raises an error of this key is absent
    dictionary['chi_2'] = 0 # bilby raises an error of this key is absent

    return dictionary

def q_pos_to_dictionary_old(q_pos, start_time, dictionary=None):
    if dictionary is None:
        dictionary = {}
        # dictionary = set_inj.ini_file_to_dict()
    if 0 in sampler_dict['search_parameter_indices']:
        dictionary['iota'] = q_pos[0]
        dictionary['theta_jn'] = q_pos[0]
    if 1 in sampler_dict['search_parameter_indices']:
        dictionary['phase'] = q_pos[1] * 0.5
    if 2 in sampler_dict['search_parameter_indices']:
        dictionary['psi'] = q_pos[2]
    if 3 in sampler_dict['search_parameter_indices']:
        dictionary['luminosity_distance'] = q_pos[3]
    if 4 in sampler_dict['search_parameter_indices']:
        dictionary['chirp_mass'] = q_pos[4]
        # dictionary['mass_1'] = q_pos[9]
        # dictionary['mass_2'] = q_pos[10]
        # dictionary['total_mass'] = q_pos[11]
        # dictionary['symmetric_mass_ratio'] = q_pos[12]
    if 5 in sampler_dict['search_parameter_indices']:
        dictionary['reduced_mass'] = q_pos[5]
        # dictionary['mass_1'] = q_pos[9]
        # dictionary['mass_2'] = q_pos[10]
        # dictionary['total_mass'] = q_pos[11]
        # dictionary['symmetric_mass_ratio'] = q_pos[12]
    if 6 in sampler_dict['search_parameter_indices']:
        dictionary['dec'] = q_pos[6]
    if 7 in sampler_dict['search_parameter_indices']:
        dictionary['ra'] = q_pos[7]
    if 8 in sampler_dict['search_parameter_indices']:
        dictionary['geocent_time'] = q_pos[8] + start_time
    dictionary['chi_1'] = 0 # bilby raises an error of this key is absent
    dictionary['chi_2'] = 0 # bilby raises an error of this key is absent

    return dictionary

def q_pos_to_q_pos_traj(q_pos):
    q_pos_traj = q_pos.copy()
    q_pos_traj[0] = np.cos(q_pos[0])
    # q_pos_traj[1] = q_pos[1]
    # q_pos_traj[2] = q_pos[2]
    q_pos_traj[3] = np.log(q_pos[3])
    q_pos_traj[4] = np.log(q_pos[4])
    q_pos_traj[5] = np.log(q_pos[5])
    q_pos_traj[6] = np.sin(q_pos[6])
    # q_pos_traj[7] = q_pos[7]
    q_pos_traj[8] = np.log(q_pos[8])
    return q_pos_traj

def q_pos_traj_to_q_pos(q_pos_traj):
    q_pos = q_pos_traj.copy()
    q_pos[0] = np.arccos(q_pos_traj[0])
    # q_pos[1] = q_pos_traj[1]
    # q_pos[2] = q_pos_traj[2]
    q_pos[3] = np.exp(q_pos_traj[3])
    q_pos[4] = np.exp(q_pos_traj[4])
    q_pos[5] = np.exp(q_pos_traj[5])
    q_pos[6] = np.arcsin(q_pos_traj[6])
    # q_pos[7] = q_pos_traj[7]
    q_pos[8] = np.exp(q_pos_traj[8])

    # # Compute masses parameters for waveform generation
    # q_pos[11] = np.power(q_pos[4], 2.5) * np.power(q_pos[5], -1.5)	 # m
    # q_pos[12] = np.power((q_pos[5]/q_pos[4]), 2.5)				# eta
    # q_pos[9] = q_pos[11] * (0.5 + np.sqrt(0.25 - q_pos[12]))	# m1
    # q_pos[10] = q_pos[11] - q_pos[9]						 # m2
    # q_pos[14] = (1.0/np.sqrt(q_pos[19]))**3 / (PI * GEOM * q_pos[11])	  # fmax(signal) for waveform at Y
    return q_pos

def q_pos_traj_to_dictionnary(q_pos_traj, start_time):
    q_pos = q_pos_traj_to_q_pos(q_pos_traj)
    dictionnary = q_pos_to_dictionary(q_pos, start_time)
    return dictionnary

def component_masses_to_reduced_mass(mass_1, mass_2):
    """
    Convert the component masses of a binary to its reduced mass.

    Parameters
    ----------
    mass_1: float
        Mass of the heavier object
    mass_2: float
        Mass of the lighter object

    Return
    ------
    reduced_mass: float
        Reduced mass of the binary
    """

    return (mass_1 * mass_2) / (mass_1 + mass_2)

def Mc_and_mu_to_M_and_eta(chirp_mass, reduced_mass):
    total_mass = Mc_mu_to_M(chirp_mass, reduced_mass)
    symmetric_mass_ratio = Mc_mu_to_eta(chirp_mass, reduced_mass)
    return total_mass, symmetric_mass_ratio

def M_and_eta_to_m1_and_m2(total_mass, symmetric_mass_ratio):
    mass_1 = total_mass * (0.5 + np.sqrt(0.25 - symmetric_mass_ratio))
    mass_2 = total_mass - mass_1
    return mass_1, mass_2

def Mc_and_mu_to_M_eta_m1_and_m2(chirp_mass, reduced_mass):
    total_mass, symmetric_mass_ratio = Mc_and_mu_to_M_and_eta(chirp_mass, reduced_mass)
    mass_1, mass_2 = M_and_eta_to_m1_and_m2(total_mass, symmetric_mass_ratio)
    return total_mass, symmetric_mass_ratio, mass_1, mass_2

def Mc_and_mu_to_m1_and_m2(chirp_mass, reduced_mass):
    _, _, mass_1, mass_2 = Mc_and_mu_to_M_eta_m1_and_m2(chirp_mass, reduced_mass)
    return mass_1, mass_2

def Mc_and_q_to_m1_and_m2(chirp_mass, mass_ratio):
    from bilby.gw.conversion import chirp_mass_and_mass_ratio_to_total_mass
    M = chirp_mass_and_mass_ratio_to_total_mass(chirp_mass, mass_ratio)
    mass_1 = M / (1 + mass_ratio)
    mass_2 = M - mass_1
    return mass_1, mass_2

def Mc_mu_to_eta(chirp_mass, reduced_mass):
    return (reduced_mass/chirp_mass)**(5/2)

def Mc_mu_to_M(chirp_mass, reduced_mass):
    return  chirp_mass**2.5 * reduced_mass**(-1.5)

def update_mass_parameters_from_component_masses(dict):
    """
    Updates the dictionnary with corresponding values for the other mass parameters given values of mass_1 and mass_2.
    We add:
        chirp mass, reduced mass, total mass, symmetric mass ratio, mass ratio

    Parameters
    ----------
    dict: dict
        The input dictionary with component masses 'mass_1' and 'mass_2'

    Returns
    -------
    dict: dict
        The updated dictionary with the additional keys: 'chirp_mass', 'reduced_mass', 'total_mass', 'symmetric_mass_ratio', 'mass_ratio'.
    """
    dict['chirp_mass'] = conv.component_masses_to_chirp_mass(dict['mass_1'], dict['mass_2'])
    dict['reduced_mass'] = component_masses_to_reduced_mass(dict['mass_1'], dict['mass_2'])
    dict['symmetric_mass_ratio'] = conv.component_masses_to_symmetric_mass_ratio(dict['mass_1'], dict['mass_2'])
    dict['total_mass'] = conv.component_masses_to_total_mass(dict['mass_1'], dict['mass_2'])
    dict['mass_ratio'] = conv.component_masses_to_mass_ratio(dict['mass_1'], dict['mass_2'])
    return dict

def update_mass_parameters_from_chirp_mass_and_reduced_mass(dict):
    """
    Updates the dictionnary with corresponding values for the other mass parameters given values of chirp_mass and reduced_mass.
    We add:
        mass_1, mass_2, total mass, symmetric mass ratio, mass ratio

    Parameters
    ----------
    dict: dict
        The input dictionary with component masses 'mass_1' and 'mass_2'

    Returns
    -------
    dict: dict
        The updated dictionary with the additional keys: 'mass_1', 'mass_2', 'total_mass', 'symmetric_mass_ratio', 'mass_ratio'.
    """
    dict['total_mass'], dict['symmetric_mass_ratio'], dict['mass_1'], dict['mass_2'] = paru.Mc_and_mu_to_M_eta_m1_and_m2(dict['chirp_mass'], dict['reduced_mass'])
    dict['mass_ratio'] = conv.component_masses_to_mass_ratio(dict['mass_2'], dict['mass_1'])
    return dict

def mass_parameters_from_mass_offset(parameters_old, mass_param_key, offset):
    """
    Returns the new parameter dictionary when the log of the chirp mass (respectively reduced mass) is varied by an offset while keeping the reduced mass (respectively chirp mass) constant.
    When varying one of these two parameters, the other mass parameters: m1/m2/M/eta need to be recomputed:
        m1  = parameters['mass_1']
        m2  = parameters['mass_2']
        M   = parameters['total_mass']
        eta = parameters['symmetric_mass_ratio']

    Parameters
    ----------
        parameters: dictionary
            The dictionary containing all the parameters needed to generate a waveform
        mass_param_key: string, either 'chirp_mass' or 'reduced_mass'
            Parameter key which you are varying
        offset: float
            offset by which you vary the log of the chirp mass / reduced mass
    Return
    ------
        parameters_new: dictionary
            A copy of the input dictionary with all mass parameters updated
    """

    parameters_new = parameters_old.copy()

    parameters_new[mass_param_key] = np.exp(np.log(parameters_old[mass_param_key]) + offset)

    parameters_new['total_mass'], parameters_new['symmetric_mass_ratio'], parameters_new['mass_1'], parameters_new['mass_2'] = Mc_and_mu_to_M_eta_m1_and_m2(parameters_new['chirp_mass'], parameters_new['reduced_mass'])

    return parameters_new

def parameters_new_dlnMc(parameters_old, dlnMc):

    return mass_parameters_from_mass_offset(parameters_old, 'chirp_mass', dlnMc)

def parameters_new_dlnmu(parameters_old, dlnmu):

    return mass_parameters_from_mass_offset(parameters_old, 'reduced_mass', dlnmu)

def remap_sky_angles(ra, dec):
    x = np.cos(dec) * np.cos(ra)
    y = np.cos(dec) * np.sin(ra)
    z = np.sin(dec)
    dec_new = np.arcsin(z)
    ra_new = np.arctan2(y, x)
    return ra_new, dec_new





def parameters_new_dlnMc_old(parameters_old,dlnMc):
    """
    Returns the new parameters array when the log of the chirp mass is varied by dlnMc while keeping the reduced mass mu constant.
    When doing so, several parameters values change: m1/m2/M/eta.
        m1  = parameters['mass_1']
        m2  = parameters['mass_2']
        M   = parameters['total_mass']
        eta = parameters['symmetric_mass_ratio']

    return parameters_new
    """
    parameters_new = parameters_old.copy()

    m1  = parameters_old['mass_1']
    m2  = parameters_old['mass_2']
    M   = parameters_old['total_mass']
    eta = parameters_old['symmetric_mass_ratio']

    # Mc = mu * eta^-2/5 => dlnMc = -2/5*dlneta when mu is kept constant
    deta= -5/2*eta*dlnMc

    # Mc = mu^3/5 * M^2/5 => dlnMc = 2/5*dlnM when mu is kept constant
    dM  = 5/2*M*dlnMc

    # Using the equations dM = dm1 + dm2 and 0 = dmu = 1/M^2 * [m2^2*dm1 + m1^2*dm2] one finds:
    dm1 = m1**2/(m1**2-m2**2)*dM
    dm2 = m2**2/(m2**2-m1**2)*dM

    parameters_new['mass_1'] += dm1                                                    # m1_new
    parameters_new['mass_2'] += dm2                                                   # m2_new
    # Swap m1 and m2 values if m2 becomes > m1:
    if parameters_new['mass_2']>parameters_new['mass_1']:
        biggest_mass = parameters_new['mass_2']
        parameters_new['mass_2'] = parameters_new['mass_1']
        parameters_new['mass_1'] = biggest_mass
    parameters_new['total_mass'] += dM                                                     # M_new
    parameters_new['symmetric_mass_ratio'] += deta    # eta_new
    # parameters_new[4] = parameters_new['total_mass'] * pow(parameters_new['symmetric_mass_ratio'], 0.6)               # Mc which should be = np.exp(ln(parameters_old[4])+dlnMc)
    parameters_new['chirp_mass'] = np.exp(np.log(parameters_old['chirp_mass'])+dlnMc)

    # # fmax ~ constant/M, hence dfmax = - fmax/M*dM
    # dfmax = -parameters_old[14]/M*dM
    # parameters_new[14] += dfmax              # fmax signal


    # # tc=tchirp is calculated from eta, which has a new value. But tc is a parameter of the waveform which should be kept constant while deriving w.r.t. Mc...
    # fsamp = 2.0 * parameters_old[15]                                # fsamp = 2 fmax_integration
    #
    # tchirp = ComputeChirpTime3p5PN(parameters_new[13], parameters_new)
    # parameters_new[8] = tchirp          # tc
    # Tobs = 1.1 * tchirp
    # parameters_new[16] = Tobs           # Tobs
    #
    # n=array_length_calculator(Tobs, fsamp)

    # print("Checking consistency of results:\n")
    # print("ln(mu_new)-ln(mu_old) = {}".format(np.log(parameters_new[5])-np.log(parameters_old[5])))
    # print("ln(Mc_new)-ln(Mc_old) = {}".format(np.log(parameters_new[4])-np.log(parameters_old[4])))
    # print("dM = {} and M_new-M_old = {}".format(dM,parameters_new['total_mass']-parameters_old['total_mass']))

    return parameters_new

def parameters_new_dlnmu_old(parameters_old,dlnmu):
    """
    Returns the new parameters array when the log of the reduced mass is varied by dlnmu while keeping the chirp mass Mc constant.
    When doing so, several parameters values change: m1/m2/M/eta.
        m1  = parameters['mass_1']
        m2  = parameters['mass_2']
        M   = parameters['total_mass']
        eta = parameters['symmetric_mass_ratio']

    return parameters_new
    """
    parameters_new = parameters_old.copy()

    m1  = parameters_old['mass_1']
    m2  = parameters_old['mass_2']
    M   = parameters_old['total_mass']
    eta = parameters_old['symmetric_mass_ratio']

    # eta = (mu/Mc^)5/2 => dlneta = 5/2*dlnmu when Mc is kept constant
    deta= 5/2*eta*dlnmu

    # M = mu^-3/2 * Mc^5/2 => dlnM = -3/2*dlnmu when Mc is kept constant
    dM  = -3/2*M*dlnmu

    # Using dMc = 0 => = alpha*dm1 + beta*dm2=0 with:
    alpha = 3*M/m1-1
    beta  = 3*M/m2-1

    # and then using dM = dm1 + dm2 one finds:
    dm1 = beta/(beta-alpha)*dM
    dm2 = alpha/(alpha-beta)*dM

    parameters_new['mass_1'] += dm1
    parameters_new['mass_2'] += dm2
    # Swap m1 and m2 values if m2 becomes > m1:
    if parameters_new['mass_2']>parameters_new['mass_1']:
        biggest_mass = parameters_new['mass_2']
        parameters_new['mass_2'] = parameters_new['mass_1']
        parameters_new['mass_1'] = biggest_mass
    parameters_new['total_mass'] += dM
    parameters_new['symmetric_mass_ratio'] += deta

    parameters_new['reduced_mass'] = np.exp(np.log(parameters_old['reduced_mass'])+dlnmu)

    # print("Checking consistency of results:\n")
    # print("ln(mu_new)-ln(mu_old) = {}".format(np.log(parameters_new[5])-np.log(parameters_old[5])))
    # print("ln(Mc_new)-ln(Mc_old) = {}".format(np.log(parameters_new[4])-np.log(parameters_old[4])))
    # print("dM = {} and M_new-M_old = {}".format(dM,parameters_new['total_mass']-parameters_old['total_mass']))

    return parameters_new
