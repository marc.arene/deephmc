# Define here constant variable
import numpy as np

# indices of interests to run the hmc on a subset of the 9 parameters:
# 0 = cos(iota)
# 1 = phic
# 2 = psi
# 3 = ln(D)
# 4 = ln(Mc)
# 5 = ln(mu)
# 6 = sin(dec)
# 7 = ra
# 8 = ln(tc)
PARAMETERS_KEYS = ['theta_jn', 'phase', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'dec', 'ra', 'geocent_time']

TRAJ_PARAMETERS_KEYS = ["cos_theta_jn", "phase", "psi", "ln(D)", "ln(Mc)", "ln(mu)", "sin(dec)", "ra", "ln(tc)"]

PARAMETERS_KEYS_LOCAL_FIT = ["cos_theta_jn", "psi", "ln(D)"]

_, PARAMETERS_INDICES_LOCAL_FIT, _ = np.intersect1d(TRAJ_PARAMETERS_KEYS, PARAMETERS_KEYS_LOCAL_FIT, return_indices=True)
PARAMETERS_INDICES_LOCAL_FIT.sort()
PARAMETERS_INDICES_LOCAL_FIT = PARAMETERS_INDICES_LOCAL_FIT.tolist()
# PARAMETERS_INDICES_LOCAL_FIT = [0, 2, 3]

HISTORICAL_PARAMETERS_INDICES = {
  "theta_jn": 0,
  "phase": 1,
  "psi": 2,
  "luminosity_distance": 3,
  "chirp_mass": 4,
  "reduced_mass": 5,
  "dec": 6,
  "ra": 7,
  "geocent_time": 8,
  "geocent_duration": 8,
  "a_1": 9,
  "a_2": 10,
  "tilt_1": 11,
  "tilt_2": 12,
  "phi_12": 13,
  "phi_jl": 14,
  "chi_1": 15,
  "chi_2": 16,
  'lambda_1': 17,
  'lambda_2': 18,
  'lambda_tilde': 19,
  'delta_lambda_tilde': 20
}

IFOS_POSSIBLE = ['H1', 'L1', 'V1']

flag_source_frame = False
flag_detector_frame = True

from bilby.gw.prior import Prior

LATEX_LABELS = Prior._default_latex_labels.copy()
LATEX_LABELS['cos(theta_jn)'] = '$cos(\\theta_{JN})$'
LATEX_LABELS['costheta_jn'] = '$cos(\\theta_{JN})$'
if flag_detector_frame:
    LATEX_LABELS['chirp_mass'] = '$\mathcal{M}^{det}$'
    LATEX_LABELS['log_chirp_mass'] = '$\\ln(\\mathcal{M}^{det})$'
    LATEX_LABELS['ln(Mc)'] = '$\\ln(\\mathcal{M}^{det})$'
    LATEX_LABELS['reduced_mass'] = '$\\mu^{det}$'
    LATEX_LABELS['log_reduced_mass'] = '$\\ln(\\mu^{det})$'
    LATEX_LABELS['ln(mu)'] = '$\\ln(\\mu^{det})$'
    LATEX_LABELS['mass_1'] = '$m_1^{det}$'
    LATEX_LABELS['mass_2'] = '$m_2^{det}$'
    LATEX_LABELS['log_mass_1'] = '$\\ln(m_1^{det})$'
    LATEX_LABELS['log_mass_2'] = '$\\ln(m_2^{det})$'
    LATEX_LABELS['total_mass'] = '$M^{det}$'
else:
    LATEX_LABELS['chirp_mass'] = '$\mathcal{M}$'
    LATEX_LABELS['log_chirp_mass'] = '$\\ln(\\mathcal{M})$'
    LATEX_LABELS['ln(Mc)'] = '$\\ln(\\mathcal{M})$'
    LATEX_LABELS['reduced_mass'] = '$\\mu$'
    LATEX_LABELS['log_reduced_mass'] = '$\\ln(\\mu)$'
    LATEX_LABELS['ln(mu)'] = '$\\ln(\\mu)$'
    LATEX_LABELS['mass_1'] = '$m_1$'
    LATEX_LABELS['mass_2'] = '$m_2$'
    LATEX_LABELS['log_mass_1'] = '$\\ln(m_1)$'
    LATEX_LABELS['log_mass_2'] = '$\\ln(m_2)$'
    LATEX_LABELS['total_mass'] = '$M$'
if flag_source_frame:
    LATEX_LABELS['chirp_mass_source'] = '$\mathcal{M}^{s}$'
    LATEX_LABELS['reduced_mass_source'] = '$\\mu^{s}$'
    LATEX_LABELS['mass_1_source'] = '$m_1^{s}$'
    LATEX_LABELS['mass_2_source'] = '$m_2^{s}$'
    LATEX_LABELS['total_mass_source'] = '$M^{s}$'
else:
    LATEX_LABELS['chirp_mass_source'] = '$\mathcal{M}$'
    LATEX_LABELS['reduced_mass_source'] = '$\\mu$'
    LATEX_LABELS['mass_1_source'] = '$m_1$'
    LATEX_LABELS['mass_2_source'] = '$m_2$'
    LATEX_LABELS['total_mass_source'] = '$M$'
LATEX_LABELS['phi_c'] = '$\\phi_c$'
LATEX_LABELS['polarisation'] = '$\\psi$'
LATEX_LABELS['tc_3p5PN'] = '$t_c$'
LATEX_LABELS['delta_tc'] = '$\\delta t_c$'
LATEX_LABELS['luminosity_distance'] = '$D_L$'
LATEX_LABELS['ln(D)'] = '$\\ln(D_L)$'
LATEX_LABELS['ln(d)'] = '$\\ln(D_L)$'
LATEX_LABELS['ln(d_L)'] = '$\\ln(D_L)$'
LATEX_LABELS['log_luminosity_distance'] = '$\\ln(D_L)$'
LATEX_LABELS['logdistance'] = '$\\ln(D_L)$'
LATEX_LABELS['sin(dec)'] = '$\sin\\left(\\delta\\right)$'
LATEX_LABELS['sin_dec'] = '$\sin\\left(\\delta\\right)$'
LATEX_LABELS['declination'] = '$\\delta$'
LATEX_LABELS['dec'] = '$\\delta$'
LATEX_LABELS['redshift'] = '$z$'
# LATEX_LABELS['sin(dec)'] = '$sin(\\mathrm{DEC})$'
# LATEX_LABELS['sin_dec'] = '$sin(\\mathrm{DEC})$'
# LATEX_LABELS['declination'] = '$\\mathrm{DEC}$'
LATEX_LABELS['rightascension'] = '$\\alpha$'
LATEX_LABELS['ra'] = '$\\alpha$'
# LATEX_LABELS['rightascension'] = '$\\mathrm{RA}$'
LATEX_LABELS['ln(tc)'] = '$\\ln(t_c)$'
LATEX_LABELS['log_geocent_time'] = '$\\ln(t_c)$'
LATEX_LABELS['ln(delta_tc)'] = '$\\ln(\\delta t_c)$'
LATEX_LABELS['log_geocent_duration'] = '$\\ln(\\delta t_c)$'
LATEX_LABELS['logL'] = '$\\ln(\\mathcal{L})$'
LATEX_LABELS['dlogL/d'] = '$\\partial \\ln\\mathcal{L}/\\partial$'
LATEX_LABELS['chi_1'] = '$\\chi_1$'
LATEX_LABELS['a_spin1'] = '$\\chi_1$'
LATEX_LABELS['one_over_x_chi_1'] = '$1/\\chi_1$'
LATEX_LABELS['signval_log_abs_val_chi_1'] = '$+/-\\ln(|\\chi_1|)$'
LATEX_LABELS['chi_2'] = '$\\chi_2$'
LATEX_LABELS['a_spin2'] = '$\\chi_2$'
LATEX_LABELS['one_over_x_chi_2'] = '$1/\\chi_2$'
LATEX_LABELS['signval_log_abs_val_chi_2'] = '$+/-\\ln(|\\chi_2|)$'
LATEX_LABELS['duration'] = '$\\delta t_c$'
LATEX_LABELS['geocent_duration'] = '$\\delta t_c$'
LATEX_LABELS['chi_eff'] = '$\\chi_{eff}$'

LATEX_UNITS = {}
LATEX_UNITS['mass_1'] = 'M$_{\\odot}$'
LATEX_UNITS['mass_2'] = 'M$_{\\odot}$'
LATEX_UNITS['chirp_mass'] = 'M$_{\\odot}$'
LATEX_UNITS['reduced_mass'] = 'M$_{\\odot}$'
LATEX_UNITS['total_mass'] = 'M$_{\\odot}$'
LATEX_UNITS['mass_1_source'] = 'M$_{\\odot}$'
LATEX_UNITS['mass_2_source'] = 'M$_{\\odot}$'
LATEX_UNITS['chirp_mass_source'] = 'M$_{\\odot}$'
LATEX_UNITS['reduced_mass_source'] = 'M$_{\\odot}$'
LATEX_UNITS['total_mass_source'] = 'M$_{\\odot}$'
LATEX_UNITS['luminosity_distance'] = 'Mpc'
LATEX_UNITS['theta_jn'] = 'rad'
LATEX_UNITS['psi'] = 'rad'
LATEX_UNITS['ra'] = 'rad'
LATEX_UNITS['dec'] = 'rad'
LATEX_UNITS['phi_c'] = 'rad'
LATEX_UNITS['phase'] = 'rad'
LATEX_UNITS['geocent_time'] = 's'
LATEX_UNITS['geocent_duration'] = 's'
LATEX_UNITS['delta_tc'] = 's'
LATEX_UNITS['duration'] = 's'


def get_latex_labels(keys_array):
    labels = []
    for key in keys_array:
        if key in LATEX_LABELS.keys():
            labels.append(LATEX_LABELS[key])
        else:
            labels.append(key)
    return labels

def get_latex_labels_with_unit(keys_array):
    labels = []
    for key in keys_array:
        if key in LATEX_LABELS.keys():
            latex_label = LATEX_LABELS[key]
            if key in LATEX_UNITS.keys():
                latex_label += f' ({LATEX_UNITS[key]})'
            labels.append(latex_label)
        else:
            labels.append(key)
    return labels

def get_latex_label_with_unit(key):
    label = get_latex_labels_with_unit([key])[0]
    return label

def get_dlogL_latex_labels(keys_array):
    labels = []
    for key in keys_array:
        if key in LATEX_LABELS.keys():
            labels.append(LATEX_LABELS['dlogL/d'] + LATEX_LABELS[key])
        else:
            labels.append(LATEX_LABELS['dlogL/d'] + key)
    return labels

SPIN_PARAMETERS = ['chi_1', 'chi_2', 'a_1', 'a_2', 'tilt_1', 'tilt_2', 'phi_12', 'phi_jl']



def identity(param):
    return param
PARAMETERS_FUNCTIONS = [np.cos, identity, identity, np.log, np.log, np.log, np.sin, identity, np.log]
PARAMETERS_INVERSE_FUNCTIONS = [np.arccos, identity, identity, np.exp, np.exp, np.exp, np.arcsin, identity, np.exp]

N_TRAJ_TO_CHECK_SIS_DEFAULT = 5000
N_TRAJ_TO_SAVE_STATE = 5000
