import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np
import pandas as pd

from bilby.gw.likelihood import GravitationalWaveTransient, ROQGravitationalWaveTransient
from bilby.gw.utils import noise_weighted_inner_product as nwip
import bilby.gw.conversion as conv

import Library.param_utils as paru
import Library.python_utils as pu
import Library.CONST as CONST

import core.utils as cut
import gw.prior as prior

# This constant defines the parameters for which is it necessary
# to recompute the source frame waveform polarizations (which is
# computationally expensive) when deriving numerical gradients.
# So it is essentially True for parameters appearing in the phase of
# the waveform and False for those appearing in the amplitude (except theta_jn).
RECOMPUTE_WAVEFORM_POLARIZATIONS = {
    'theta_jn': True,
    'phase': True,
    'psi': False,
    'luminosity_distance': True,
    'mass_1': True,
    'mass_2': True,
    'chirp_mass': True,
    'reduced_mass': True,
    'dec': False,
    'ra': False,
    'geocent_time': False,
    'geocent_duration': False,
    'chi_1': True,
    'chi_2': True,
    'a_1': True,
    'a_2': True,
    'tilt_1': True,
    'tilt_2': True,
    'phi_12': True,
    'phi_jl': True,
    'lambda_1': True,
    'lambda_2': True,
    'lambda_tilde': True,
    'delta_lambda_tilde': True
}

# This constant defines the parameters for which is it necessary
# to recompute the exponentiation of the phase-shift (which is
# a bit computationally expensive) from geocenter to the ifo
# when deriving numerical gradients.
# So it is only True for parameters which define the time-delay, ie
# dec, ra and time of coalescence.
RECOMPUTE_EXP_TWO_PI_F_DT = {
    'theta_jn': False,
    'phase': False,
    'psi': False,
    'luminosity_distance': False,
    'mass_1': False,
    'mass_2': False,
    'chirp_mass': False,
    'reduced_mass': False,
    'dec': True,
    'ra': True,
    'geocent_time': True,
    'geocent_duration': True,
    'chi_1': False,
    'chi_2': False,
    'a_1': False,
    'a_2': False,
    'tilt_1': False,
    'tilt_2': False,
    'phi_12': False,
    'phi_jl': False,
    'lambda_1': False,
    'lambda_2': False,
    'lambda_tilde': False,
    'delta_lambda_tilde': False
}

# Default numerical differencing method used if not defined by the user
# in the configuration files.
DIFFERENCING_METHOD_DEFAULT = {
    'theta_jn': 'forward',
    'phase': 'central',
    'psi': 'forward',
    'luminosity_distance': 'forward',
    'chirp_mass': 'central',
    'reduced_mass': 'central',
    'dec': 'forward',
    'ra': 'forward',
    'geocent_time': 'central',
    'geocent_duration': 'central',
    'chi_1': 'central',
    'chi_2': 'central',
    'lambda_1': 'forward',
    'lambda_2': 'forward',
    'a_1': 'central',
    'a_2': 'central',
    'tilt_1': 'central',
    'tilt_2': 'central',
    'phi_12': 'central',
    'phi_jl': 'central',
    'lambda_tilde': 'central',
    'delta_lambda_tilde': 'central'
}


class LikelihoodGradient(object):
    """

    """
    def __init__(self, likelihood, priors, dict_parameters_offsets=None, dict_diff_method=None, offset=1e-7):
        self.likelihood = likelihood
        self.priors = priors
        self.search_parameter_keys = priors.search_parameter_keys
        self.dict_parameters_offsets = dict_parameters_offsets
        self.log_likelihood_gradient = dict()
        self.dict_diff_method = self.get_dict_diff_method(dict_diff_method)

    @property
    def n_dim(self):
        if self.search_parameter_keys is None:
            return 0
        else:
            return len(self.search_parameter_keys)

    @property
    def dict_parameters_offsets(self):
        return self._dict_parameters_offsets

    @dict_parameters_offsets.setter
    def dict_parameters_offsets(self, dict_parameters_offsets):
        if dict_parameters_offsets is not None:
            self._dict_parameters_offsets = dict_parameters_offsets.copy()
        else:
            self._dict_parameters_offsets = dict()
        for key in self.search_parameter_keys:
            if key not in self._dict_parameters_offsets.keys():
                self._dict_parameters_offsets.update({key: 1e-7})

    def calculate_log_likelihood_gradient(self):
        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()

        for key in self.search_parameter_keys:
            offset = self.dict_parameters_offsets[key]
            self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
            self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] - offset
            log_likelihood_minus = self.likelihood.log_likelihood_ratio()

            self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] + offset
            log_likelihood_plus = self.likelihood.log_likelihood_ratio()

            self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * offset)

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    def get_dict_diff_method(self, dict_diff_method=None):
        new_dict_diff_method = dict()
        if dict_diff_method is None:
            dict_diff_method = dict()
        for key in self.search_parameter_keys:
            if key in dict_diff_method.keys():
                if dict_diff_method[key] in ['central', 'forward']:
                    new_dict_diff_method[key] = dict_diff_method[key]
                else:
                    raise ValueError(f'Method of differenciation for {key} was {dict_diff_method[key]} but it can only be either "central" or "forward".')
            else:
                if key in DIFFERENCING_METHOD_DEFAULT.keys():
                    new_dict_diff_method[key] = DIFFERENCING_METHOD_DEFAULT[key]
                else:
                    new_dict_diff_method[key] = 'central'
        return new_dict_diff_method

class GWTransientLikelihoodGradient(LikelihoodGradient):
    """

    """
    def __init__(self, likelihood, traj_priors,  dict_trajectory_functions_str, dict_parameters_offsets=None, dict_diff_method=None, offset=1e-7):
        LikelihoodGradient.__init__(self, likelihood, traj_priors, dict_parameters_offsets, dict_diff_method, offset)
        self.traj_priors = traj_priors
        # "Unfortunately" it is important to set those attributes as copies of
        # traj_priors and NOT as @properties deriving from it because when computing the FIM
        # we need to modify their values temporarily if phase is marginalized
        self.search_trajectory_parameters_keys = traj_priors.search_trajectory_parameters_keys
        self.search_trajectory_functions = traj_priors.search_trajectory_functions
        self.search_trajectory_inv_functions = traj_priors.search_trajectory_inv_functions
        self.dict_trajectory_parameters_keys = self.get_dict_trajectory_parameters_keys()
        self.start_time = likelihood.interferometers[0].strain_data.start_time
        self.dlogL_method = 's_minus_h_dh'
        # self.dlogL_method = 'logL_diff'
        self.overlaps = []

    def get_dict_trajectory_parameters_keys_old(self, dict_trajectory_parameters_keys):
        """
        Fills the gaps of the inputed dictionary: dict_trajectory_parameters_keys, associating a search_parameter to its 'trajectory-parameter-name'. Indeed when a search_parameter is its own trajectory-parameter we allow the user to say 'none' or not mention it.
        """
        dict_traj_param_keys_full = dict_trajectory_parameters_keys.copy()
        for key in self.search_parameter_keys:
            if key not in dict_trajectory_parameters_keys.keys() or dict_trajectory_parameters_keys[key].lower() == 'none':
                dict_traj_param_keys_full[key] = key
        return dict_traj_param_keys_full

    def get_dict_trajectory_parameters_keys(self):
        """
        Fills the gaps of the inputed dictionary: dict_trajectory_parameters_keys, associating a search_parameter to its 'trajectory-parameter-name'. Indeed when a search_parameter is its own trajectory-parameter we allow the user to say 'none' or not mention it.
        """
        dict = {}
        for idx, key in enumerate(self.search_parameter_keys):
            dict[key] = self.search_trajectory_parameters_keys[idx]
        return dict

    @property
    def interferometers(self):
        return self.likelihood.interferometers

    @property
    def uses_roq(self):
        return isinstance(self.likelihood, ROQGravitationalWaveTransient)

    def calculate_log_likelihood_gradient(self, search_parameter_keys=None):
        # Reset gradient
        self.log_likelihood_gradient = {}

        use_logL_diff_method = self.dlogL_method == 'logL_diff' or isinstance(self.likelihood, ROQGravitationalWaveTransient)

        # By default we want to compute the numerical gradient over all the searched parameters.
        # For hybrid trajectories only a subset of the parameter space will be numerically computed.
        if search_parameter_keys is None: search_parameter_keys = self.search_parameter_keys.copy()

        if use_logL_diff_method:
            return self.calculate_dlogL_logL_diff(search_parameter_keys)
        else:
            return self.calculate_dlogL_s_minus_h_dh(search_parameter_keys)

    def calculate_dlogL_logL_diff(self, search_parameter_keys):
        """
        [PROBABLY BROKEN] Computation of the numerical gradient vector using the method where `dlogL = (logL_plus - logL_minus) / (2*offset)` (in the case of central differencing).
        The code belows worked at some point but was not updated for some time in the profit of using the other method: `self.calculate_dlogL_s_minus_h_dh()`. Fixing it should not take too long though.
        Note that one has to use this method if using ROQs.
        """
        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()
        self.log_likelihood_at_point = self.likelihood.log_likelihood_ratio()

        for idx, lalsim_key in enumerate(search_parameter_keys):
            # Reset the parameters to their original value
            self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
            traj_func = self.search_trajectory_functions[idx]
            traj_inv_func = self.search_trajectory_inv_functions[idx]
            traj_param = traj_func(self.likelihood.parameters_at_point[lalsim_key])
            diff_method = self.dict_diff_method[lalsim_key]
            # traj_key = self.dict_trajectory_parameters_keys[lalsim_key]
            offset = self.dict_parameters_offsets[lalsim_key]
            if lalsim_key == 'theta_jn':
                # If we are at the boundary where iota = +/- pi, we only do a forward difference in cos(iota)
                param_plus = traj_inv_func(traj_param + offset)
                param_minus = traj_inv_func(traj_param - offset)
                if np.isnan(param_minus):
                # if traj_param - offset < -1:
                    parameters_plus = {lalsim_key: param_plus}
                    self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
                # If we are at the boundary where iota = 0, we only do a backward difference in cos(iota)
                elif np.isnan(param_plus):
                # elif traj_param + offset > 1:
                    parameters_minus = {lalsim_key: param_minus}
                    self.compute_dlogL_of_param_backward_diff(lalsim_key, parameters_minus)
                # Otherwise we would keep the central difference but forward is sufficient for this parameter
                else:
                    parameters_plus = {lalsim_key: param_plus}
                    self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
            # elif lalsim_key == 'phase':
            #     parameters_plus = {lalsim_key: traj_inv_func(traj_param + offset)}
            #     self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
            # elif lalsim_key == 'psi':
            #     parameters_plus = {lalsim_key: traj_inv_func(traj_param + offset)}
            #     self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
            elif lalsim_key == 'luminosity_distance':
                if traj_func == np.log and not(self.likelihood.phase_marginalization):
                    traj_key = self.dict_trajectory_parameters_keys[lalsim_key]
                    d_inner_h = 0
                    h_inner_h = 0
                    waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.likelihood.parameters)
                    for ifo in self.interferometers:
                        # `calculate_snrs()` works for ROQ and non-ROQ likelihoods
                        per_detector_snr = self.likelihood.calculate_snrs(waveform_polarizations=waveform_polarizations, interferometer=ifo)
                        d_inner_h += per_detector_snr.d_inner_h
                        h_inner_h += per_detector_snr.optimal_snr_squared.real
                    self.log_likelihood_gradient[traj_key] = -np.real(d_inner_h) + h_inner_h
                else:
                    parameters_plus = {lalsim_key: traj_inv_func(traj_param + offset)}
                    self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
            elif lalsim_key =='chirp_mass':
                parameters_plus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, offset)
                parameters_minus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, -offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a backward step will give parameters_minus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use forward difference
                if parameters_minus['symmetric_mass_ratio'] > 0.25:
                    self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
                else:
                    self.compute_dlogL_of_param_central_diff(lalsim_key, parameters_plus, parameters_minus)
            elif lalsim_key =='reduced_mass':
                parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, offset)
                parameters_minus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
                if parameters_plus['symmetric_mass_ratio'] > 0.25:
                    self.compute_dlogL_of_param_backward_diff(lalsim_key, parameters_minus)
                else:
                    self.compute_dlogL_of_param_central_diff(lalsim_key, parameters_plus, parameters_minus)
            # elif lalsim_key == 'dec':
            #     parameters_plus = {lalsim_key: traj_inv_func(traj_param + offset)}
            #     self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
            # elif lalsim_key == 'ra':
            #     parameters_plus = {lalsim_key: traj_inv_func(traj_param + offset)}
            #     self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
            # elif lalsim_key == 'geocent_duration':
            #     # For BBH signal, an offset = 1e-7 was to small compared to the variations of tc and 1e-5 works better
            #     # For geocent_time we can't take the derivative w.r.t. the gps time, which is ~1e10 sec, otherwise even with an offset of 1e-8 the gps_plus would be offseted by about 200sec !. Hence derive w.r.t. geocent_duration.
            #     traj_param = traj_func(self.likelihood.parameters_at_point[lalsim_key])
            #     parameters_plus = {lalsim_key: traj_inv_func(traj_param + offset)}
            #     parameters_minus = {lalsim_key: traj_inv_func(traj_param - offset)}
            #     self.compute_dlogL_of_param_central_diff(lalsim_key, parameters_plus, parameters_minus)
            elif diff_method == 'forward':
                parameters_plus = {lalsim_key: traj_inv_func(traj_param + offset)}
                self.compute_dlogL_of_param_forward_diff(lalsim_key, parameters_plus)
            else:
                parameters_plus = {lalsim_key: traj_inv_func(traj_param + offset)}
                parameters_minus = {lalsim_key: traj_inv_func(traj_param - offset)}
                if lalsim_key == 'geocent_duration':
                    # We need to update the geocent_time value since that's what bilby's
                    # likelihood functions uses
                    parameters_plus['geocent_time'] = parameters_plus['geocent_duration'] + self.start_time
                    parameters_minus['geocent_time'] = parameters_minus['geocent_duration'] + self.start_time
                self.compute_dlogL_of_param_central_diff(lalsim_key, parameters_plus, parameters_minus)

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    def compute_dlogL_of_param_central_diff(self, param_key, parameters_plus, parameters_minus):
        traj_key = self.dict_trajectory_parameters_keys[param_key]
        offset = self.dict_parameters_offsets[param_key]
        self.likelihood.parameters.update(parameters_plus)
        log_likelihood_plus = self.likelihood.log_likelihood_ratio()
        self.likelihood.parameters.update(parameters_minus)
        log_likelihood_minus = self.likelihood.log_likelihood_ratio()
        self.log_likelihood_gradient[traj_key] = (log_likelihood_plus - log_likelihood_minus) / (2 * offset)

    def compute_dlogL_of_param_forward_diff(self, param_key, parameters_plus):
        traj_key = self.dict_trajectory_parameters_keys[param_key]
        offset = self.dict_parameters_offsets[param_key]
        self.likelihood.parameters.update(parameters_plus)
        log_likelihood_plus = self.likelihood.log_likelihood_ratio()
        self.log_likelihood_gradient[traj_key] = (log_likelihood_plus - self.log_likelihood_at_point) / offset

    def compute_dlogL_of_param_backward_diff(self, param_key, parameters_minus):
        traj_key = self.dict_trajectory_parameters_keys[param_key]
        offset = self.dict_parameters_offsets[param_key]
        self.likelihood.parameters.update(parameters_minus)
        log_likelihood_minus = self.likelihood.log_likelihood_ratio()
        self.log_likelihood_gradient[traj_key] = (self.log_likelihood_at_point - log_likelihood_minus) / offset

    def calculate_dlogL_s_minus_h_dh(self, search_parameter_keys):
        """
        This function should be merged with the one it is calling below.
        """
        # This will compute all the ifo.dh[traj_key], for all ifos and all keys
        # It also computes ifo.template_at_point which we use below
        # print('\n\nIn calculate_dlogL_s_minus_h_dh() \n\n')
        # import IPython; IPython.embed(); sys.exit()
        self.compute_h_dh(search_parameter_keys)

        # for param_key in search_parameter_keys:
        #     traj_key = self.dict_trajectory_parameters_keys[param_key]
        #     self.log_likelihood_gradient[traj_key] = 0
        #     for ifo in self.interferometers:
        #         # s_inner_dh = nwip(ifo.fd_strain, ifo.dh[traj_key], ifo.psd_array, ifo.strain_data.duration)
        #         # h_inner_dh = nwip(ifo.template_at_point, ifo.dh[traj_key], ifo.psd_array, ifo.strain_data.duration)
        #         # self.log_likelihood_gradient[traj_key] += s_inner_dh - h_inner_dh
        #         self.log_likelihood_gradient[traj_key] += ifo.log_likelihood_gradient[traj_key]

        return self.log_likelihood_gradient

    def compute_h_dh(self, parameters_keys):
        """
        Computation of the numerical gradient vector using the method where dlogL = <s-h|dh>, allowing (for some parameters) to simplify the calculations analytically. Can't work with a likelihood using ROQ basis. Central, forward of backward differencing is used depending on what was set for each parameter and on where we are in parameter space (eg on a boundary). The numerical gradient will be computed on the "trajectory parameters" associated to the parameters_keys in input.
        The point at which the gradient vector is computed is defined by the values stored in `self.likelihood.parameters`.

        Parameters
        ----------
        parameters_keys: list or array
            Standard key names defined in Bilby associated to the trajectory_parameters on which the gradient vector will be computed.

        Returns
        -------
        self.log_likelihood_gradient: dict
            Dictionary containing one trajectory_parameter_key per parameter_key in input with the associated log-likelihood gradient stored.

        Example
        -------
        If one wants to compute the gradient vector on `['psi', 'log(chirp_mass)', 'log(reduced_mass)', 'cos(theta_jn)']`, normally among the search_parameter_keys of the analysis should be `['psi', 'chirp_mass', 'reduced_mass', 'theta_jn']` and the trajectory functions `[None, 'log', 'log', 'cosine']`. Then the dictionary containinng the gradient vector computed by this function will look like `self.log_likelihood_gradient = {'psi': xx.x, 'log(chirp_mass)': yy.y, 'log(reduced_mass)': zz.z, 'cos(theta_jn)': ww.w}`.

        Important
        ---------
        The computation of the fisher matrix calls this function (well `self.calculate_dlogL_s_minus_h_dh()`), not to retrieve the gradient values but so that for each ifo and each parameter `ifo.dh[param_key]` is computed and updated which allows then to compute the FIM values. So the `ifo.dh`
        """

        # First we compute the log_likelihood at the point of interest, defined by `self.likelihood.parameters`,
        # and store the intermediate computations which we will reuse for numerical differencing under
        # `self.s_inner_h_at_point` and `self.h_inner_h_at_point`.
        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()
        waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.likelihood.parameters)
        self.likelihood.waveform_polarizations_at_point = waveform_polarizations.copy()
        self.log_likelihood_at_point = 0
        self.s_inner_h_at_point = 0
        self.h_inner_h_at_point = 0
        mf_snr_sqrd_at_point = 0

        for ifo in self.interferometers:
            ifo.exp_two_pi_f_dt_at_point = ifo.get_fd_time_translation(self.likelihood.parameters)
            ifo.template_at_point = ifo.get_detector_response(waveform_polarizations, self.likelihood.parameters, exp_two_pi_f_dt=ifo.exp_two_pi_f_dt_at_point)
            ifo.dh = {}
            # # [DEBUG]: old way of computing inner products which does not use the strain_data.window_factor
            # ifo.s_inner_h_at_point = nwip(ifo.fd_strain, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration)
            # ifo.h_inner_h_at_point = np.real(nwip(ifo.template_at_point, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration))
            ifo.s_inner_h_at_point = ifo.inner_product_mask_fixed(ifo.template_at_point)
            ifo.h_inner_h_at_point = np.real(ifo.optimal_snr_squared(ifo.template_at_point))
            ifo.matched_filter_snr_at_point = np.real(ifo.s_inner_h_at_point) / ifo.h_inner_h_at_point**0.5
            self.s_inner_h_at_point += ifo.s_inner_h_at_point
            self.h_inner_h_at_point += ifo.h_inner_h_at_point
            mf_snr_sqrd_at_point += ifo.matched_filter_snr_at_point**2
        self.matched_filter_snr_at_point = mf_snr_sqrd_at_point**0.5
        if self.likelihood.phase_marginalization:
            self.s_inner_h_at_point = self.likelihood._bessel_function_interped(abs(self.s_inner_h_at_point))
            self.matched_filter_snr_at_point = self.s_inner_h_at_point / self.h_inner_h_at_point**0.5
        # Note that at the moment the value of `self.log_likelihood_at_point` computed here is used in `BNS_HMC_Tools.py`
        # at the end of numerical trajectories if the `--plot__traj` option was set.
        self.log_likelihood_at_point = np.real(self.s_inner_h_at_point) - 0.5 * self.h_inner_h_at_point

        # Then for each parameter asked we compute the gradient of its associated "trajectory parameter".
        for lalsim_key in parameters_keys:
            idx_search = self.search_parameter_keys.index(lalsim_key)
            traj_func = self.search_trajectory_functions[idx_search]
            # For the FIM computation or hybrid gradients: `parameters_keys`
            # can be different from `self.search_parameter_keys`
            if not(self.likelihood.phase_marginalization) and lalsim_key == 'luminosity_distance' and traj_func == np.log:
                # Here since h ~ constant/D, dh/dD = -h/D. Hence dh/dlnD = D*dh/dD = -h
                # So we don't use differencing ! However this can't be used if
                # marginalizing over phase since we should derive the bessel function
                # w.r.t. ln(D) and it's not that simple...!
                traj_key = self.dict_trajectory_parameters_keys[lalsim_key]
                self.log_likelihood_gradient[traj_key] = 0
                for ifo in self.interferometers:
                    ifo.dh[traj_key] = -ifo.template_at_point # used when computing scales
                # dlogL = s_inner_dh - h_inner_dh = -s_inner_h + h_inner_h
                self.log_likelihood_gradient[traj_key] += -np.real(self.s_inner_h_at_point) + self.h_inner_h_at_point
                return self.log_likelihood_gradient
            else:
                traj_inv_func = self.search_trajectory_inv_functions[idx_search]
                traj_param = traj_func(self.likelihood.parameters_at_point[lalsim_key])
                diff_method = self.dict_diff_method[lalsim_key]
                offset = self.dict_parameters_offsets[lalsim_key]
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = traj_inv_func(traj_param + offset)
                parameters_minus[lalsim_key] = traj_inv_func(traj_param - offset)
                prior_min = self.traj_priors.priors[lalsim_key].minimum
                prior_max = self.traj_priors.priors[lalsim_key].maximum
                cond_forward = np.isnan(parameters_minus[lalsim_key]) or parameters_minus[lalsim_key] < prior_min
                cond_forward = cond_forward or diff_method == 'forward'
                cond_backward = np.isnan(parameters_plus[lalsim_key]) or parameters_plus[lalsim_key] > prior_max
            if lalsim_key in ['chirp_mass', 'reduced_mass']:
                parameters_plus = paru.update_mass_parameters_from_chirp_mass_and_reduced_mass(parameters_plus)
                parameters_minus = paru.update_mass_parameters_from_chirp_mass_and_reduced_mass(parameters_minus)
                if lalsim_key == 'chirp_mass':
                    # If parameters['symmetric_mass_ratio'] = 0.25: a backward step will give parameters_minus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use forward difference
                    cond_forward = cond_forward or parameters_minus['symmetric_mass_ratio'] > 0.25
                elif lalsim_key == 'reduced_mass':
                    # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
                    cond_backward = cond_backward or parameters_plus['symmetric_mass_ratio'] > 0.25
            elif lalsim_key in ['mass_1', 'mass_2']:
                parameters_plus = paru.update_mass_parameters_from_component_masses(parameters_plus)
                parameters_minus = paru.update_mass_parameters_from_component_masses(parameters_minus)
                # If on the equal mass line, central differencing on mass_1 could lead to a mass_1 <= mass_2 and thus we use only forward.
                if lalsim_key == 'mass_1':
                    cond_forward = cond_forward or parameters_minus['mass_1'] <= parameters_minus['mass_2']
                # Equivalently, central differencing on mass_2 could lead to a mass_2 >= mass_1 and thus we use only backward.
                elif lalsim_key == 'mass_2':
                    cond_backward = cond_backward or parameters_plus['mass_2'] >= parameters_plus['mass_1']
            elif lalsim_key == 'geocent_duration':
                # We need to update the geocent_time value since that's what bilby's
                # likelihood functions uses
                parameters_plus['geocent_time'] = parameters_plus['geocent_duration'] + self.start_time
                parameters_minus['geocent_time'] = parameters_minus['geocent_duration'] + self.start_time
            else:
                pass
            if cond_forward:
                self.compute_dh_forward_diff_of_ifos(lalsim_key, parameters_plus)
            elif cond_backward:
                self.compute_dh_backward_diff_of_ifos(lalsim_key, parameters_minus)
            else:
                self.compute_dh_central_diff_of_ifos(lalsim_key, parameters_plus, parameters_minus)

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    def compute_dh_central_diff_of_ifos(self, param_key, parameters_plus, parameters_minus):
        traj_param_key = self.dict_trajectory_parameters_keys[param_key]
        recompute_wf_pol = RECOMPUTE_WAVEFORM_POLARIZATIONS[param_key]
        recompute_exp_two_pi_f_dt = RECOMPUTE_EXP_TWO_PI_F_DT[param_key]
        offset = self.dict_parameters_offsets[param_key]
        if recompute_wf_pol:
            # if False:
            if param_key == 'luminosity_distance':
                waveform_polarizations_plus = {}
                d_over_dplus = self.likelihood.parameters_at_point['luminosity_distance'] / parameters_plus['luminosity_distance']
                for mode in self.likelihood.waveform_polarizations_at_point.keys():
                    waveform_polarizations_plus[mode] = d_over_dplus * self.likelihood.waveform_polarizations_at_point[mode]
                waveform_polarizations_minus = {}
                d_over_dminus = self.likelihood.parameters_at_point['luminosity_distance'] / parameters_minus['luminosity_distance']
                for mode in self.likelihood.waveform_polarizations_at_point.keys():
                    waveform_polarizations_minus[mode] = d_over_dminus * self.likelihood.waveform_polarizations_at_point[mode]
            else:
                # if param_key == 'mass_1':
                #     import IPython; IPython.embed(); sys.exit()
                # import ipdb; ipdb.set_trace()
                waveform_polarizations_plus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_plus)
                waveform_polarizations_minus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_minus)
        else:
            waveform_polarizations_plus = self.likelihood.waveform_polarizations_at_point.copy()
            waveform_polarizations_minus = self.likelihood.waveform_polarizations_at_point.copy()

        s_inner_h_plus = 0
        s_inner_h_minus = 0
        h_inner_dh = 0
        for ifo in self.interferometers:
            self.compute_dh_central_diff_of_ifo(traj_param_key, waveform_polarizations_plus, parameters_plus, waveform_polarizations_minus, parameters_minus, ifo, offset, compute_exp_two_pi_f_dt=recompute_exp_two_pi_f_dt)
            s_inner_h_plus += ifo.s_inner_h_plus
            s_inner_h_minus += ifo.s_inner_h_minus
            h_inner_dh += np.real(ifo.h_inner_dh)

        if self.likelihood.phase_marginalization:
            s_inner_h_plus = self.likelihood._bessel_function_interped(abs(s_inner_h_plus))
            s_inner_h_minus = self.likelihood._bessel_function_interped(abs(s_inner_h_minus))

        s_inner_dh = (s_inner_h_plus - s_inner_h_minus) / (2 * offset)
        self.log_likelihood_gradient[traj_param_key] = np.real(s_inner_dh) - h_inner_dh

    def compute_dh_central_diff_of_ifo(self, traj_param_key, waveform_polarizations_plus, parameters_plus, waveform_polarizations_minus, parameters_minus, ifo, offset, compute_exp_two_pi_f_dt=True):
        if compute_exp_two_pi_f_dt:
            exp_two_pi_f_dt_plus = None
            exp_two_pi_f_dt_minus = None
        else:
            exp_two_pi_f_dt_plus = ifo.exp_two_pi_f_dt_at_point
            exp_two_pi_f_dt_minus = ifo.exp_two_pi_f_dt_at_point

        template_plus = ifo.get_detector_response(waveform_polarizations_plus, parameters_plus, exp_two_pi_f_dt_plus)
        template_minus = ifo.get_detector_response(waveform_polarizations_minus, parameters_minus, exp_two_pi_f_dt_minus)

        dh = (template_plus - template_minus) / (2 * offset)
        # [IMPORTANT]: Even though not used in this function, `ifo.dh[traj_param_key]` must be updated here
        # as at the moment it is used in `self._compute_fisher_matrix()`.
        ifo.dh[traj_param_key] = dh

        # # [DEBUG]: old way of computing inner products which does not use the strain_data.window_factor
        # ifo.s_inner_h_plus = nwip(ifo.fd_strain, template_plus, ifo.psd_array, ifo.strain_data.duration)
        # ifo.s_inner_h_minus = nwip(ifo.fd_strain, template_minus, ifo.psd_array, ifo.strain_data.duration)
        # ifo.h_inner_dh = nwip(ifo.template_at_point, dh, ifo.psd_array, ifo.strain_data.duration)
        ifo.s_inner_h_plus = ifo.inner_product_mask_fixed(template_plus)
        ifo.s_inner_h_minus = ifo.inner_product_mask_fixed(template_minus)
        ifo.h_inner_dh = ifo.noise_weighted_inner_product(ifo.template_at_point, dh)

        return dh

    def compute_dh_forward_diff_of_ifos(self, param_key, parameters_plus):
        traj_param_key = self.dict_trajectory_parameters_keys[param_key]
        recompute_wf_pol = RECOMPUTE_WAVEFORM_POLARIZATIONS[param_key]
        recompute_exp_two_pi_f_dt = RECOMPUTE_EXP_TWO_PI_F_DT[param_key]
        offset = self.dict_parameters_offsets[param_key]
        # if param_key == 'dec':
        #     import IPython; IPython.embed(); sys.exit()
        if recompute_wf_pol:
            if param_key == 'luminosity_distance':
                waveform_polarizations_plus = {}
                d_over_dplus = self.likelihood.parameters_at_point['luminosity_distance'] / parameters_plus['luminosity_distance']
                for mode in self.likelihood.waveform_polarizations_at_point.keys():
                    waveform_polarizations_plus[mode] = d_over_dplus * self.likelihood.waveform_polarizations_at_point[mode]
            else:
                waveform_polarizations_plus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_plus)
        else:
            waveform_polarizations_plus = self.likelihood.waveform_polarizations_at_point.copy()

        s_inner_h_plus = 0
        h_inner_dh = 0
        for ifo in self.interferometers:
            self.compute_dh_forward_diff_of_ifo(traj_param_key, waveform_polarizations_plus, parameters_plus, ifo, offset, compute_exp_two_pi_f_dt=recompute_exp_two_pi_f_dt)
            s_inner_h_plus += ifo.s_inner_h_plus
            h_inner_dh += np.real(ifo.h_inner_dh)

        if self.likelihood.phase_marginalization:
            s_inner_h_plus = self.likelihood._bessel_function_interped(abs(s_inner_h_plus))

        s_inner_dh = (s_inner_h_plus - self.s_inner_h_at_point) / offset
        self.log_likelihood_gradient[traj_param_key] = np.real(s_inner_dh) - h_inner_dh

    def compute_dh_forward_diff_of_ifo(self, traj_param_key, waveform_polarizations_plus, parameters_plus, ifo, offset, compute_exp_two_pi_f_dt=True):
        if compute_exp_two_pi_f_dt:
            exp_two_pi_f_dt_plus = None
        else:
            exp_two_pi_f_dt_plus = ifo.exp_two_pi_f_dt_at_point

        template_plus = ifo.get_detector_response(waveform_polarizations_plus, parameters_plus, exp_two_pi_f_dt_plus)

        dh = (template_plus - ifo.template_at_point) / offset
        # [IMPORTANT]: Even though not used in this function, `ifo.dh[traj_param_key]` must be updated here
        # as at the moment it is used in `self._compute_fisher_matrix()`.
        ifo.dh[traj_param_key] = dh

        # # [DEBUG]: old way of computing inner products which does not use the strain_data.window_factor
        # ifo.s_inner_h_plus = nwip(ifo.fd_strain, template_plus, ifo.psd_array, ifo.strain_data.duration)
        # ifo.h_inner_dh = nwip(ifo.template_at_point, dh, ifo.psd_array, ifo.strain_data.duration)
        ifo.s_inner_h_plus = ifo.inner_product_mask_fixed(template_plus)
        ifo.h_inner_dh = ifo.noise_weighted_inner_product(ifo.template_at_point, dh)

        return dh

    def compute_dh_backward_diff_of_ifos(self, param_key, parameters_minus):
        traj_param_key = self.dict_trajectory_parameters_keys[param_key]
        recompute_wf_pol = RECOMPUTE_WAVEFORM_POLARIZATIONS[param_key]
        recompute_exp_two_pi_f_dt = RECOMPUTE_EXP_TWO_PI_F_DT[param_key]
        offset = self.dict_parameters_offsets[param_key]
        if recompute_wf_pol:
            if param_key == 'luminosity_distance':
                waveform_polarizations_minus = {}
                d_over_dminus = self.likelihood.parameters_at_point['luminosity_distance'] / parameters_minus['luminosity_distance']
                for mode in self.likelihood.waveform_polarizations_at_point.keys():
                    waveform_polarizations_minus[mode] = d_over_dminus * self.likelihood.waveform_polarizations_at_point[mode]
            else:
                waveform_polarizations_minus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_minus)
        else:
            waveform_polarizations_minus = self.likelihood.waveform_polarizations_at_point.copy()

        s_inner_h_minus = 0
        h_inner_dh = 0
        for ifo in self.interferometers:
            self.compute_dh_backward_diff_of_ifo(traj_param_key, waveform_polarizations_minus, parameters_minus, ifo, offset, compute_exp_two_pi_f_dt=recompute_exp_two_pi_f_dt)
            s_inner_h_minus += ifo.s_inner_h_minus
            h_inner_dh += np.real(ifo.h_inner_dh)

        if self.likelihood.phase_marginalization:
            s_inner_h_minus = self.likelihood._bessel_function_interped(abs(s_inner_h_minus))

        s_inner_dh = (self.s_inner_h_at_point - s_inner_h_minus) / offset
        self.log_likelihood_gradient[traj_param_key] = np.real(s_inner_dh) - h_inner_dh

    def compute_dh_backward_diff_of_ifo(self, traj_param_key, waveform_polarizations_minus, parameters_minus, ifo, offset, compute_exp_two_pi_f_dt=True):
        if compute_exp_two_pi_f_dt:
            exp_two_pi_f_dt_minus = None
        else:
            exp_two_pi_f_dt_minus = ifo.exp_two_pi_f_dt_at_point

        template_minus = ifo.get_detector_response(waveform_polarizations_minus, parameters_minus, exp_two_pi_f_dt_minus)

        dh = (ifo.template_at_point - template_minus) / offset
        # [IMPORTANT]: Even though not used in this function, `ifo.dh[traj_param_key]` must be updated here
        # as at the moment it is used in `self._compute_fisher_matrix()`.
        ifo.dh[traj_param_key] = dh

        # # [DEBUG]: old way of computing inner products which does not use the strain_data.window_factor
        # ifo.s_inner_h_minus = nwip(ifo.fd_strain, template_minus, ifo.psd_array, ifo.strain_data.duration)
        # ifo.h_inner_dh = nwip(ifo.template_at_point, dh, ifo.psd_array, ifo.strain_data.duration)
        ifo.s_inner_h_minus = ifo.inner_product_mask_fixed(template_minus)
        ifo.h_inner_dh = ifo.noise_weighted_inner_product(ifo.template_at_point, dh)

        return dh

    def get_fisher_matrix(self, parameters_keys=None):
        cut.logger.info('Computing the Fisher Information Matrix...')
        if parameters_keys is None:
            parameters_keys = self.search_parameter_keys
        # If phase if being marginalized, we compute the FIM with it
        # and then project it out. see https://arxiv.org/abs/gr-qc/0203020 for more details.
        phase_is_marginalized = 'phase' not in self.search_parameter_keys
        if phase_is_marginalized:
            parameters_keys = ['phase'] + parameters_keys
            self.search_parameter_keys = ['phase'] + self.search_parameter_keys
            self.search_trajectory_parameters_keys = ['phase'] + self.search_trajectory_parameters_keys
            self.search_trajectory_functions = [prior.identity] + self.search_trajectory_functions
            self.search_trajectory_inv_functions = [prior.identity] + self.search_trajectory_inv_functions
            self.dict_trajectory_parameters_keys.update({'phase': 'phase'})
            self.dict_diff_method.update({'phase': 'central'})
            self.dict_parameters_offsets.update({'phase': 1e-7})
            from bilby.core.prior.analytical import Uniform
            self.traj_priors.priors['phase'] = Uniform(name='phase', minimum=0, maximum=2 * np.pi, boundary='reflective')
        # If the likelihood uses ROQ basis, then its waveform_generator
        # does not really generate a waveform but the linear and quadratic
        # components sufficient to compute the log_likelihood().
        # However here for the FIM we need to compute the derivatives
        # of the waveforms. Hence we temporarily switch the roq waveform
        # generator source model to its non-roq counterpart and switch
        # it back at the end.
        if self.uses_roq:
            import bilby.gw.source as source
            roq_fd_source_model = self.likelihood.waveform_generator.frequency_domain_source_model
            if 'neutron_star' in roq_fd_source_model.__name__:
                non_roq_fd_source_model = source.lal_binary_neutron_star
            elif 'black_hole' in roq_fd_source_model.__name__:
                non_roq_fd_source_model = source.lal_binary_black_hole
            else:
                raise ValueError(f'Cannot associate a non roq frequency domain source model to the roq one: {roq_fd_source_model_name} in order to compute the fisher matrix.')
            self.likelihood.waveform_generator.frequency_domain_source_model = non_roq_fd_source_model

        # self.dict_parameters_offsets.update({'geocent_time': 1e-2})
        fisher_matrix = self._compute_fisher_matrix(parameters_keys)
        # self.dict_parameters_offsets.update({'geocent_time': 1e-6})

        # Let's project the FIM, ie compute it on the sub-manifold without phase
        if phase_is_marginalized:
            cut.logger.info('Projecting the FIM on subspace since phase is marginalized.')
            fisher_matrix = self.project_fisher_matrix(fisher_matrix, 0)
        # Switch the properties of the class back to their original values
        if self.uses_roq:
            self.likelihood.waveform_generator.frequency_domain_source_model = roq_fd_source_model
        if phase_is_marginalized:
            parameters_keys.remove('phase')
            self.search_parameter_keys.remove('phase')
            self.search_trajectory_parameters_keys.remove('phase')
            self.search_trajectory_functions.pop(0)
            self.search_trajectory_inv_functions.pop(0)
            self.traj_priors.priors.pop('phase')

        traj_param_keys = [self.dict_trajectory_parameters_keys[key] for key in parameters_keys]

        df_fisher_matrix = pd.DataFrame(fisher_matrix, columns=traj_param_keys, index=traj_param_keys)
        # import IPython; IPython.embed(); sys.exit()
        return df_fisher_matrix

    def project_fisher_matrix(self, fisher_matrix, idx_out):
        # First sub-select from FIM as the (N-1, N-1) matrix by removing the first row and column
        indices_to_keep = np.arange(0, fisher_matrix.shape[0])
        indices_to_keep = indices_to_keep.tolist()
        indices_to_keep.remove(idx_out)
        ixgrid = np.ix_(indices_to_keep, indices_to_keep)
        fisher_matrix_proj = fisher_matrix.copy()
        # Then substract the phase components of the full FIM to project
        for row in range(len(fisher_matrix_proj)):
            for col in range(len(fisher_matrix_proj)):
                fisher_matrix_proj[row, col] -= fisher_matrix[row, idx_out] * fisher_matrix[idx_out, col] / fisher_matrix[idx_out, idx_out]

        fisher_matrix_proj = fisher_matrix_proj[ixgrid]
        return fisher_matrix_proj

    def _compute_fisher_matrix(self, parameters_keys):

        # [IMPORTANT]: we do not call this function to retrieve the gradient values but so that
        # for each ifo and each parameter: `ifo.dh[param_key]` is computed and updated,
        # which values are then used below.
        self.calculate_dlogL_s_minus_h_dh(parameters_keys)

        # Compute the FIM from the inner product of the derivatives
        fisher_matrix = np.zeros((len(parameters_keys), len(parameters_keys)))
        for i, param_key_row in enumerate(parameters_keys):
            key_row = self.dict_trajectory_parameters_keys[param_key_row]
            for j, param_key_col in enumerate(parameters_keys):
                key_col = self.dict_trajectory_parameters_keys[param_key_col]
                for ifo in self.interferometers:
                    # fisher_matrix[i][j] += np.real(nwip(ifo.dh[key_row], ifo.dh[key_col], ifo.psd_array, ifo.strain_data.duration))
                    fisher_matrix[i][j] += np.real(ifo.noise_weighted_inner_product(ifo.dh[key_row], ifo.dh[key_col]))

        return fisher_matrix

    def convert_dlogL_dict_to_np_array(self, dlogL_dict):
        dict_keys = dlogL_dict.keys()
        dlogL_np = np.zeros(len(dict_keys))
        for i, key in enumerate(dict_keys):
            dlogL_np[i] = dlogL_dict[key]

        return dlogL_np

    def calculate_dlogL_np(self, dlogL_dict=None, search_parameter_keys=None):
        if dlogL_dict is None:
            dlogL_dict = self.calculate_log_likelihood_gradient(search_parameter_keys)
        return self.convert_dlogL_dict_to_np_array(dlogL_dict)
