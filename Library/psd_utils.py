# Standard python numerical analysis imports:
import sys
import numpy as np
# from scipy import signal
# from scipy.interpolate import interp1d
# from scipy.signal import butter, filtfilt, iirdesign, zpk2tf, freqz
import h5py
# import json

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

import bilby.gw.detector as bgd
import gwpy

def get_strain_from_hdf5(hdf5_file_path):
    """
    Returns the entire strain stored in the hdf5 file and other meta data
    """

    hdf5 = h5py.File(hdf5_file_path, 'r')
    strain = hdf5['strain/Strain'][()]
    number_of_samples = len(strain)
    start_time = hdf5['meta/GPSstart'][()]
    duration = hdf5['meta/Duration'][()]
    sampling_frequency = number_of_samples / duration

    return strain, start_time, duration, sampling_frequency, number_of_samples

def get_psd_from_strain(strain, sampling_frequency, NFFT=None):
    """
    Returns the PSD estimated from this strain together with the frequency array associated
    """
    sampling_frequency = int(sampling_frequency)

    if NFFT is None: NFFT = 4*sampling_frequency
    Pxx, freqs = mlab.psd(strain, Fs = sampling_frequency, NFFT = NFFT)

    return Pxx, freqs

def get_psd_from_strain_before_event(strain, sampling_frequency, strain_start_time, signal_start_time, signal_duration, psd_duration=100, psd_offset=-1024, filter_freq=None, NFFT=None):

    strain_psd_start_time = signal_start_time + signal_duration + psd_offset

    if strain_psd_start_time + psd_duration >= signal_start_time:
        raise ValueError('Your psd_offset is not big enough, your psd estimation overlaps with the start of the signal')

    duration_until_strain_psd_starts = strain_psd_start_time - strain_start_time

    if duration_until_strain_psd_starts < 0:
        raise ValueError('Your psd_offset is too big, you are asking for it to start {:.1f} seconds before the beginning of the data'.format(duration_until_strain_psd_starts))

    index_start = int(duration_until_strain_psd_starts * sampling_frequency)
    index_end = int((duration_until_strain_psd_starts + psd_duration) * sampling_frequency) + 1

    strain_psd = strain[index_start : index_end + 1]

    # Low pass filter. Following how it is coded in:
    # bilby.detector.get_interferometer_with_open_data() > strain_psd.low_pass_filter(): bilby.detector.low_pass_filter()
    if not(filter_freq is None or filter_freq >= sampling_frequency / 2):
        bp = gwpy.signal.filter_design.lowpass(
            filter_freq, sampling_frequency)
        strain = gwpy.timeseries.TimeSeries(
            strain_psd, sample_rate=sampling_frequency)
        strain = strain.filter(bp, filtfilt=True)
        strain_psd = strain.value

    Pxx, freqs = get_psd_from_strain(strain_psd, sampling_frequency, NFFT)

    return Pxx, freqs



def get_psd_from_hdf5(hdf5_file_path, NFFT=None):

    strain, start_time, duration, sampling_frequency, number_of_samples = get_strain_from_hdf5(hdf5_file_path)

    Pxx, freqs = get_psd_from_strain(strain, sampling_frequency, NFFT)

    return Pxx, freqs




def get_psd_from_hdf5_before_event(hdf5_file_path, signal_start_time, signal_duration, psd_duration=100, psd_offset=-1024, filter_freq=None, NFFT=None):

    strain, strain_start_time, duration, sampling_frequency, number_of_samples = get_strain_from_hdf5(hdf5_file_path)

    Pxx, freqs = get_psd_from_strain_before_event(strain, sampling_frequency, strain_start_time, signal_start_time, signal_duration, psd_duration, psd_offset, filter_freq, NFFT)

    return Pxx, freqs

def set_interferometer_psd_from_hdf5_file(interferometer, hdf5_file_path, signal_start_time, signal_duration, psd_duration=100, psd_offset=-1024, filter_freq=None, NFFT=None):

    Pxx, freqs = get_psd_from_hdf5_before_event(hdf5_file_path, signal_start_time, signal_duration, psd_duration, psd_offset, filter_freq, NFFT)
    # Pxx, freqs = get_psd_from_hdf5(hdf5_file_path, NFFT)

    interferometer.power_spectral_density = bgd.PowerSpectralDensity(psd_array=Pxx, frequency_array=freqs)
