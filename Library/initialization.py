import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import numpy as np
import Library.python_utils as pu
import Library.CONST as CONST
import core.utils as cut


def get_sampler_dict(config_dict):
    sampler_dict = pu.json_to_dict(config_dict['hmc']['config_hmc_parameters_file'])

    sampler_dict['search_parameter_indices'] = []
    for key in sampler_dict['search_parameter_keys']:
        sampler_dict['search_parameter_indices'].append(CONST.HISTORICAL_PARAMETERS_INDICES[key])
    return sampler_dict

def sampler_dict_update_geocent_time_by_duration(sampler_dict):
    """
    Takes the dictionary containing the list of search parameter keys and updates the key `geocent_time` to `geocent_duration` since it will be the duration used by the HMC and not GPS time for numerical precision reasons when computing numerical gradients on that parameter.

    Parameters
    ----------
    sampler_dict: dictionary
        Dictionary which must contain the keys `search_parameter_keys` (which is the array describing the parameters of the search), `trajectory_functions` (like 'log' for the keys `chirp_mass` and `reduced_mass` or `cosine` for `theta_jn`) and `parameters_offsets` (offsets used for each key when computing numerical gradients, typically 1e-7).

    Returns
    -------
    sampler_dict: dictionary
        Returns the input dictionary where the search_parameter_key `geocent_time` has been updated to `geocent_duration`.
    """
    cut.logger.info('As usual, for numerical precision reasons for its gradients, the HMC samples in geocent_duration instead of the GPS geocent_time and converts back to it afterwards.')
    idx_geocent_time = sampler_dict['search_parameter_keys'].index('geocent_time')
    sampler_dict['search_parameter_keys'][idx_geocent_time] = 'geocent_duration'
    sampler_dict['trajectory_functions']['geocent_duration'] = sampler_dict['trajectory_functions']['geocent_time']
    sampler_dict['parameters_offsets']['geocent_duration'] = sampler_dict['parameters_offsets'].get('geocent_time', 'none')
    return sampler_dict

def get_output_dir(inj_name, search_parameter_keys, config_dict, sub_dir=None):
    """
    Parameters
    ----------
    inj_name: string
        Name of the injection, or the GW event.
    search_parameter_keys: array like
        List of the keys defining the parameters of the search.
    config_dict: dictionary
        Dict containing some key options for the run such as the total number of trajectories in phase 1
    sub_dir: string, optional
        Optional string which will be appended at the end of the output_dir string built from the previous inputs.

    Returns
    -------
    outdir: string
        The path of the directory where output files will be stored, note this string ends with a slash `/`.

    Notes
    -----
    At the moment `outdir` is hard coded to start as `outdir = '../__output_data/[inj_name]/... `.
    The end of the string is built to have the structure `'../__output_data/[inj_name]/[search_parameter_nbs]/[psd_option]/[config_options]/[approximant_name]/`.
    - [search_parameter_nbs] is built from `search_parameter_keys` and assigns a predefined integer to each key to built a string.
    - [psd_option]: should be removed, it was there to distinguish runs using fake PSDs vs PSDs estimated from the strain vs PSDs read from input files.
    - [approximant_name]: eg `IMRPhenomD_NRTidal` etc...
    """

    # Parse the inputs relative to the HMC algorithm
    n_traj_hmc_tot = config_dict['hmc']['n_traj_hmc_tot']
    n_traj_fit = config_dict['hmc']['n_traj_fit']
    n_traj_for_this_run = config_dict['hmc']['n_traj_for_this_run']
    n_fit_1 = config_dict['hmc']['n_fit_1']
    n_fit_2 = config_dict['hmc']['n_fit_2']
    length_num_traj = config_dict['hmc']['length_num_traj']
    epsilon0 = config_dict['hmc']['epsilon0']

    # example: outdir = '../__output_data/GW170817/bilbyoriented/'
    outdir = '../__output_data/' + inj_name + '/'

    # example: outdir = '../__output_data/GW170817/bilbyoriented/SUB-D_######6##/'
    # outdir_subD = 'SUB-D_'
    outdir_subD = ''

    historical_param_indices = []
    for key in search_parameter_keys:
        historical_param_indices.append(CONST.HISTORICAL_PARAMETERS_INDICES[key])
    historical_param_indices.sort()
    outdir_subD += '_'.join([str(idx) for idx in historical_param_indices])
    # for i in historical_param_indices:
    #     if i in search_parameter_indices:
    #         outdir_subD+='{}'.format(i)
    #     else:
    #         outdir_subD += '#'
    outdir += outdir_subD + '/'

    # example: outdir = '../__output_data/GW170817/SUB-D_######6##/PSD_1/'
    outdir_psd = get_outdir_psd(config_dict)
    outdir += outdir_psd

    # example: outdir = '../__output_data/GW170817/SUB-D_######6##/PSD_1/NO_SEED/'
    # if opts.no_seed: outdir += 'NO_SEED/'

    # if len(search_parameter_indices) == 1 and (search_parameter_indices[0]!=1 or search_parameter_indices[0]!=3):
    #     offset_suffix = '_{:.0e}'.format(parameters_offsets[search_parameter_indices[0]])
    outdir_opts = "{}_{}_{}_{}_{}_{}/".format(n_traj_hmc_tot, n_traj_fit, n_fit_1, n_fit_2, length_num_traj, epsilon0)
    outdir += outdir_opts

    if 'phase_marginalization' in  config_dict['analysis'].keys():
        if config_dict['analysis']['phase_marginalization']:
            if 'phase' in search_parameter_keys:
                raise ValueError('"phase" is part of the search_parameter_keys => you cannot marginalize over phase and estimate this parameter at the same time !')
            outdir += 'phase_marginalization/'

    approx_suffix = config_dict['analysis']['approximant']
    if config_dict['analysis']['roq'] and config_dict['analysis']['approximant'] == 'IMRPhenomPv2':
        approx_suffix += '_ROQ'
    outdir += approx_suffix + '/'

    if sub_dir is not None: outdir += sub_dir + '/'

    return outdir

def get_outdir_psd(config_dict):
    # Old way of setting the psds when using /Codes/set_psds.py
    psd = config_dict['analysis'].get('psd', None)
    if psd is not None:
        outdir_psd = 'PSD_{}/'.format(psd)
    else:
        # If new way of setting PSDs, we make the correspondance with the convention used in /Codes/set_psds.py s.t. the outdir naming stays the same, for the moment...
        psds_single_file = config_dict['analysis'].get('psds_single_file', None)
        psd_dict = config_dict['analysis'].get('psd_dict', None)
        if psds_single_file is None and psd_dict is None:
            # means the psds have been computed from strain file
            outdir_psd = 'PSD_3/'
        else:
            # Means the psds have been set from a file
            outdir_psd = 'PSD_2/'
    return outdir_psd
