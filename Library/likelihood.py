import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np

from bilby.gw.likelihood import GravitationalWaveTransient

import Library.param_utils as paru
import Library.CONST as CONST


class Likelihood(object):
    pass

class GWLikelihood(GravitationalWaveTransient):
    """

    """
    def __init__(self, interferometers, waveform_generator,
                 time_marginalization=False, distance_marginalization=False,
                 phase_marginalization=False, priors=None,
                 distance_marginalization_lookup_table=None,
                 jitter_time=True):
        GravitationalWaveTransient.__init__(self, interferometers, waveform_generator,
                 time_marginalization, distance_marginalization,
                 phase_marginalization, priors,
                 distance_marginalization_lookup_table,
                 jitter_time)
