from sklearn import neighbors


def local_fit(PointFit_Sort, dlogL, q_pos_traj, n_neighbors=200, weights='distance'):
    X = PointFit_Sort.T
    Y = dlogL
    knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
    fit = knn.fit(X, Y)
    return fit.predict([q_pos_traj])[0]

def get_knn_fit(PointFit_Sort, dlogL, n_neighbors=200, weights='distance'):
    X = PointFit_Sort.T
    Y = dlogL
    knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
    knn_fit = knn.fit(X, Y)
    return knn_fit
