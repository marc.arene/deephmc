import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np

import bilby.gw.utils as gwutils
import Library.python_utils as pu

def scalar_product_basic(array_a, array_b):
    integrand = array_a.real * array_b.real + array_a.imag * array_b.imag
    return np.sum(integrand)

def overlap(array_a, array_b):
    aa = scalar_product_basic(array_a, array_a)
    bb = scalar_product_basic(array_b, array_b)
    ab = scalar_product_basic(array_a, array_b)
    return ab / np.sqrt(aa * bb)

def noise_weighted_inner_product(aa, bb, power_spectral_density, duration):
    """
    This version is 20% faster than bilby's one using (np.conj(aa) * bb).real.
    """
    integrand = (aa.real * bb.real + aa.imag * bb.imag) / power_spectral_density
    nwip = 4 / duration * np.sum(integrand)

    return nwip

def noise_weighted_inner_product_new(aa, bb, inverse_power_spectral_density, duration):
    """
    This version is 20% faster than bilby's one using (np.conj(aa) * bb).real.
    """
    integrand = (aa.real * bb.real + aa.imag * bb.imag) * inverse_power_spectral_density
    nwip = 4 / duration * np.sum(integrand)

    return nwip

def nwip_network(aa_list, bb_list, interferometers):

    if len(aa_list)!=len(bb_list) and len(aa_list)!=len(interferometers):
        print("Error: Lists are of different length for nwip")
        return

    nwip_net = 0
    for i in range(len(interferometers)):
        nwip_net += noise_weighted_inner_product(
            aa=aa_list[i],
            bb=bb_list[i],
            power_spectral_density=interferometers[i].psd_array,
            duration=interferometers[i].strain_data.duration)

    return nwip_net


def nwip_h_h_network(template_ifos, interferometers):

    nwip_h_h__network = 0
    for i in range(len(interferometers)):
        nwip_h_h__network += noise_weighted_inner_product(aa=template_ifos[i], bb=template_ifos[i], power_spectral_density=interferometers[i].psd_array, duration=interferometers[i].strain_data.duration)

    return nwip_h_h__network


def nwip_s_h_network(template_ifos, interferometers):

    nwip_s_h_network = 0
    for i in range(len(interferometers)):
        nwip_s_h_network += noise_weighted_inner_product(aa=template_ifos[i], bb=interferometers[i].frequency_domain_strain, power_spectral_density=interferometers[i].psd_array, duration=interferometers[i].strain_data.duration)

    return nwip_s_h_network


def loglikelihood_snr(template_ifos, interferometers):

    nwip_s_h_net = nwip_s_h_network(template_ifos, interferometers)
    nwip_h_h_net = nwip_h_h_network(template_ifos, interferometers)
    logL = nwip_s_h_net - 0.5 * nwip_h_h_net

    mf_snr = np.sqrt(nwip_s_h_net)
    opt_snr = np.sqrt(nwip_h_h_net)

    return logL, mf_snr, opt_snr


def loglikelihood(template_ifos, interferometers):

    nwip_s_h_net = nwip_s_h_network(template_ifos, interferometers)
    nwip_h_h_net = nwip_h_h_network(template_ifos, interferometers)
    logL = nwip_s_h_net - 0.5 * nwip_h_h_net

    return logL






def optimal_network_snr_bilby(template_ifos, interferometers):

    network_snr_squared = 0
    for i in range(len(interferometers)):
        network_snr_squared += gwutils.optimal_snr_squared(signal=template_ifos[i], power_spectral_density=interferometers[i].psd_array, duration=interferometers[i].strain_data.duration).real

    return np.sqrt(network_snr_squared)


def matched_filter_network_snr_bilby(template_ifos, interferometers):

    network_snr_squared = 0
    for i in range(len(interferometers)):
        network_snr_squared += gwutils.matched_filter_snr_squared(signal=template_ifos[i], frequency_domain_strain=interferometers[i].frequency_domain_strain, power_spectral_density=interferometers[i].psd_array, duration=interferometers[i].strain_data.duration).real

    return np.sqrt(network_snr_squared)


def loglikelihood_snr_bilby(template_ifos, interferometers):

    mf_snr = matched_filter_network_snr_bilby(template_ifos, interferometers)
    opt_snr = optimal_network_snr_bilby(template_ifos, interferometers)
    logL = mf_snr**2 - 0.5 * opt_snr**2

    return logL, mf_snr, opt_snr


def noise_weighted_inner_product_bilby(aa, bb, interferometer):

    nwip = gwutils.noise_weighted_inner_product(aa, bb, interferometer.psd_array, interferometer.strain_data.duration)

    return nwip

def noise_weighted_inner_product_2(aa, bb, interferometer):
    """
    This version is meant to accelerate the inner product computation by not summing over the indices where one or the other array is 0 since it does not contribute to the integral.
    However it requires calculating i_low and i_high before.
    It turns out that it is a bit slower to do so (1.0ms vs 1.28ms) if for instance: len(aa)=len(bb)=77187, i_low=1405, i_high=63267.
    However if we are going to use longer arrays padded with zeros to get to the next power of two, their length should get up to ~132000 and then it might be interesting to use this version of the function.
    I have also timed the improvement by passing directly into the function i_low=1405, i_high=63267; it took 0.944ms instead of 1.00ms, so not such a big improvement.
    In comparision, buf.LVC_RiemannSum() would take 0.425ms ...
    """
    indices_aa_non_zero = np.where(aa!=0)[0]
    indices_bb_non_zero = np.where(bb!=0)[0]
    i_low = min(indices_aa_non_zero[0], indices_bb_non_zero[0])
    i_high = min(indices_aa_non_zero[-1], indices_bb_non_zero[-1])

    nwip = gwutils.noise_weighted_inner_product(
        aa[i_low:i_high+1],
        bb[i_low:i_high+1],
        interferometer.psd_array[i_low:i_high+1],
        interferometer.strain_data.duration
    )

    return nwip
