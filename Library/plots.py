import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

import numpy as np
import corner

import sys
sys.path.append('../')
import Library.CONST as CONST
import Library.python_utils as pu
import core.utils as cut

from matplotlib import rcParams
rcParams["text.usetex"] = True
rcParams["font.serif"] = "Computer Modern Roman"
rcParams["font.family"] = "serif"
rcParams['text.latex.preamble'] = r'\providecommand{\mathdefault}[1][]{}'




def init_result_object(samples, injection_parameters, search_parameter_keys=['theta_jn', 'phase', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'dec', 'ra', 'geocent_time'], label='no_label', outdir=''):
    import pandas as pd
    import bilby
    result = bilby.core.result.Result()

    result.label = label
    result.samples = samples
    result.injection_parameters = injection_parameters
    result.search_parameter_keys = search_parameter_keys
    # default_latex_labels = bilby.gw.prior.Prior._default_latex_labels
    # default_latex_labels['reduced_mass'] = '$\\mu$'
    # default_latex_labels['phi_c'] = '$\\phi_c$'
    # default_latex_labels['tc_3p5PN'] = '$t_c$'
    # # default_latex_labels['cos_theta_jn'] = '$cos(\\theta_{jn})$'
    result.parameter_labels = CONST.get_latex_labels(search_parameter_keys)
    # for k in result.search_parameter_keys:
    #     if k in default_latex_labels.keys():
    #         result.parameter_labels.append(default_latex_labels[k])
    #     else:
    #         result.parameter_labels.append(k)
    # # result.parameter_labels = [default_latex_labels[k] for k in result.search_parameter_keys]
    # result.parameter_labels_with_unit = result.parameter_labels
    result.parameter_labels_with_unit = CONST.get_latex_labels(search_parameter_keys)
    data_frame = pd.DataFrame(result.samples, columns=result.search_parameter_keys)
    result.posterior = data_frame

    # Needed in order to use result.plot_walkers()
    result.walkers = np.array([samples])
    result.nburn = 0

    return result

def plot_walkers_and_corner_from_samples(samples, injection_parameters, search_parameter_keys, save=True, label='no_label', outdir='./plots', plot_walkers=False, **kwargs):

    result = init_result_object(samples, injection_parameters, search_parameter_keys=search_parameter_keys, label=label)
    result.plot_corner(save=save, outdir=outdir, **kwargs)
    if save and plot_walkers:
        result.plot_walkers(outdir=outdir)

    # result.plot_single_density('ra', truth=injection_parameters['ra'], save=False)
    # result.plot_marginals()

def plot_corner_simple(data, path=None, **kwargs):

    defaults_kwargs = dict(
        bins=50, smooth=0.9, label_kwargs=dict(fontsize=16),
        title_kwargs=dict(fontsize=16), color='#0072C1',
        truth_color='tab:orange', quantiles=[0.16, 0.84],
        levels=(1 - np.exp(-0.5), 1 - np.exp(-2), 1 - np.exp(-9 / 2.)),
        plot_density=False, plot_datapoints=True, fill_contours=True,
        max_n_ticks=3)
    defaults_kwargs.update(kwargs)
    kwargs = defaults_kwargs

    fig = corner.corner(data, **kwargs)
    # axes = fig.get_axes()
    if path is None:
        plt.show()
    else:
        fig.savefig(path, dpi=300)


def plot_regression_on_ax(data_true, data_predicted, ax=None, label='', data_label='data', **kwargs):
    from sklearn import metrics
    """Summary plot of regression performance.
    """
    # color='blue'
    fontsize_label = 25
    ax = ax or plt.gca()
    ax.scatter(data_true, data_predicted, lw=0, alpha=0.5, s=10, **kwargs)
    ax.set_xlabel('True ' + data_label, fontsize=fontsize_label)
    ax.set_ylabel('Predicted ' + data_label, fontsize=fontsize_label)
    x_min = data_true.min()
    x_max = data_true.max()
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(x_min, x_max)
    ax.plot([x_min, x_max], [x_min, x_max], 'r--')
    R2 = metrics.r2_score(data_true, data_predicted)
    # mse = metrics.mean_squared_error(data_true, data_predicted)
    ax.text(0.05, 0.9, f'{label}$R^2 = {R2:.2f}$', transform=ax.transAxes, fontsize=40, color='white', bbox={'facecolor':'black', 'alpha':0.5, 'pad':5})
    # ax.text(0.05, 0.8, f'{label}$MSE = {mse:.2f}$', transform=ax.transAxes, fontsize=28, **kwargs)
    return ax

def plot_regressions(data_true, data_predicted, data_labels=None, title=None, path=None, **kwargs):

    if data_true.shape != data_predicted.shape:
        raise ValueError(f'data_true and data_predicted do not have the same shape: {data_true.shape} vs {data_predicted.shape}')
    if len(data_true.shape) == 1:
        data_true = data_true.reshape(-1, 1)
        data_predicted = data_predicted.reshape(-1, 1)

    n_plots = data_true.shape[1]

    if data_labels is None:
        data_labels = ['data' for i in range(n_plots)]
    if len(data_labels) != n_plots:
        raise ValueError(f'Nb of data_labels ({len(data_labels)}) do not correspond to nb of plots ({n_plots})')

    figsize_0 = 8
    fig, axes = plt.subplots(n_plots, 1, sharex=False, sharey=False, figsize=(figsize_0, figsize_0 * n_plots))
    for i in range(n_plots):
        plot_regression_on_ax(data_true[:, i], data_predicted[:, i], ax=axes[i], data_label=data_labels[i], **kwargs)
    if title is None:
        title = f'Regression plots'
    plt.tight_layout()
    # import IPython; IPython.embed(); sys.exit()
    fig.text(x=0.5, y=0.99, s=title, fontsize=26, horizontalalignment='center')
    fig.text(x=0.5, y=0.98, s=f'{data_true.shape[0]:,} data points', fontsize=18, horizontalalignment='center')
    fig.subplots_adjust(top=0.97)

    if path is not None:
        fig.savefig(path)

    return fig


def make_regression_plots(fit_method, x_train, y_train, outdir, validation_data=None, y_labels=None, **kwargs):
    """
    This function assumes the `fit_method` has been trained on (x_train, y_train). It will predict values from `x_train`, compare them to the true ones: `y_train` and create a one regression subplot per gradients in `y_train`. If `validation_data` is provided: `(x_val, y_val)`, it will do the same and merge the two figures.
    """
    kwargs.update(color=fit_method.plot_color)
    outdir += f'/{fit_method.save_dir_suffix}'
    cut.check_directory_exists_and_if_not_mkdir(outdir)
    cut.logger.info(f'Making regression plots for {fit_method.nick_name}')
    cut.logger.info('Predicting values on train set...')
    y_train_predicted = fit_method.predict(x_train)
    cut.logger.info('Saving predicted data on train set...')
    np.savetxt(outdir + f'/dlogL_predicted_{fit_method.nick_name}_{len(y_train_predicted)}train.dat', y_train_predicted)
    cut.logger.info('Plotting on train set...')
    path_train = outdir + f'/{fit_method.nick_name}_reg_{len(y_train)}train.png'
    title = f'{fit_method.nick_name}-fit regression on phase1 - train set'
    if y_labels is None:
        y_labels = [rf'$y_{i}$' for i in range(y_train.shape[1])]
    plot_regressions(y_train, y_train_predicted, path=path_train, title=title, data_labels=y_labels, **kwargs)
    if validation_data is not None:
        x_val, y_val = validation_data
        cut.logger.info('Predicting values on validation set...')
        y_val_predicted = fit_method.predict(x_val)
        cut.logger.info('Saving predicted data on validation set...')
        np.savetxt(outdir + f'/dlogL_predicted_{fit_method.nick_name}_{len(y_val_predicted)}val.dat', y_val_predicted)
        cut.logger.info('Plotting on validation set...')
        path_val = outdir + f'/{fit_method.nick_name}_reg_{len(y_val)}val.png'
        title = f'{fit_method.nick_name}-fit regression on phase1 - validation set'
        plot_regressions(y_val, y_val_predicted, path=path_val, title=title, data_labels=y_labels, **kwargs)
        path_train_val = outdir + f'/{fit_method.nick_name}_reg_{len(y_train)}train_{len(y_val)}val.png'
        import subprocess
        subprocess.run(f'convert {path_train} {path_val} +append {path_train_val}', shell=True)
        subprocess.run(f'rm {path_train} {path_val}', shell=True)


def plot_hist_dlogL_phase1(dlogL_traj_phase1, search_trajectory_parameters_keys, filename, title=None):
    dlogL_latex_labels = CONST.get_dlogL_latex_labels(search_trajectory_parameters_keys)
    n_plots = dlogL_traj_phase1.shape[1]
    figsize_0 = 8
    fig, axes = plt.subplots(n_plots, 1, sharex=False, sharey=False, figsize=(figsize_0, figsize_0 * n_plots))
    for i, ax in enumerate(axes):
        ax.hist(dlogL_traj_phase1[:, i], bins=100)
        ax.set_xlabel(dlogL_latex_labels[i])
        if title is not None and i == 0:
            ax.set_title(title)
    fig.tight_layout()
    fig.savefig(filename)

def plot_dlogL_vs_qpos(qpos_train, dlogL_train, qpos_val, dlogL_val, search_trajectory_parameters_keys, outdir, filename=None, title=None):
    qpos_latex_label = CONST.get_latex_labels(search_trajectory_parameters_keys)
    dlogL_latex_labels = CONST.get_dlogL_latex_labels(search_trajectory_parameters_keys)
    label_fontsize = 64
    tick_fontsize = 32
    label_kwargs=dict(fontsize=label_fontsize)
    defaults_kwargs = dict(
        bins=50, smooth=0.9, label_kwargs=label_kwargs,
        title_kwargs=label_kwargs,
        plot_density=False, plot_datapoints=True, fill_contours=True,
        max_n_ticks=3)

    dlogL_train = dlogL_train[:, :3]
    qpos_train = qpos_train[:, :3]
    dlogL_val = dlogL_val[:, :3]
    qpos_val = qpos_val[:, :3]

    fig, axes = plt.subplots(dlogL_train.shape[1], dlogL_train.shape[1], figsize=(50,50))
    for i in range(axes.shape[0]):
        for j in range(axes.shape[1]):
            corner.hist2d(qpos_train[:, j], dlogL_train[:, i], ax=axes[i, j], color='C0', **defaults_kwargs)
            corner.hist2d(qpos_val[:, j], dlogL_val[:, i], ax=axes[i, j], color='C1', **defaults_kwargs)
            # import IPython; IPython.embed(); sys.exit()
            if i == axes.shape[0]-1:
                axes[i, j].set_xlabel(f'{qpos_latex_label[j]}', **label_kwargs)
                for tick in axes[i, j].xaxis.get_major_ticks():
                    tick.label.set_fontsize(tick_fontsize)
                # import IPython; IPython.embed(); sys.exit()
            else:
                axes[i, j].get_xaxis().set_visible(False)
            if j == 0:
                axes[i, j].set_ylabel(f'{dlogL_latex_labels[i]}', **label_kwargs)
                for tick in axes[i, j].yaxis.get_major_ticks():
                    tick.label.set_fontsize(tick_fontsize)
            else:
                axes[i, j].get_yaxis().set_visible(False)
            # if i == 0 and j == axes.shape[1]:
            #     axes[i, j].

    fig.tight_layout()
    fig.subplots_adjust(top=0.88) # Needed so that fig.title and ax.title don't overlap
    fig.suptitle(f'dlogL vs qpos - FORWARD', fontsize=int(label_fontsize*1.2))
    fig.text(x=0.5, y=0.92, s=f'train set = {len(qpos_train):7,}', color='C0', horizontalalignment='center', fontsize=label_fontsize)
    fig.text(x=0.5, y=0.90, s=f'val set = {len(qpos_val):7,}', color='C1', horizontalalignment='center', fontsize=label_fontsize)

    if filename is None:
        filename = f'dlogL_vs_qpos_{len(dlogL_train)}train_{len(dlogL_val)}val.png'
    cut.check_directory_exists_and_if_not_mkdir(outdir)
    fig.savefig(outdir + '/' + filename)

def plot_training_history(hist_dict, filepath, **kwargs):

    cut.logger.info(f'Plotting training history...')
    default_kwargs = dict(
        title = 'Learning history',
        xlabel = 'Epoch',
        ylabel = 'Mean squarred error loss',
    )
    default_kwargs.update(kwargs)
    kwargs = default_kwargs
    fig = plt.figure()
    plt.plot(hist_dict['loss'], label='Training set')
    if 'val_loss' in hist_dict.keys():
        plt.plot(hist_dict['val_loss'], label='Validation set')
    plt.xlabel(kwargs['xlabel'])
    plt.ylabel(kwargs['ylabel'])
    plt.title(kwargs['title'])
    plt.legend(loc='upper right')
    plt.savefig(filepath)
    plt.close()
    cut.logger.info(f'Done.')


# OLD STUFFs



def plot_num_and_ana_gradients(dlogL_num, dlogL_ana, opts_dict=None, save=True, show_plot=False, file_name=None, outdir='./plots', alongNum=True):
    from matplotlib import rc
    plt.rcParams['text.usetex'] = False
    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'
    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'
    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10

    if opts_dict is None:
        n_traj_fit = '??'
        length_num_traj = '??'
    else:
        n_traj_fit = opts_dict['n_traj_fit']
        length_num_traj = opts_dict['length_num_traj']

    steps = np.arange(len(dlogL_num[0]))

    # params_to_plot = [4, 5]
    params_to_plot = list(range(dlogL_num.shape[0]))

    fig, axs = plt.subplots(nrows=len(params_to_plot), figsize=(15, 3 * dlogL_num.shape[0]))
    # fig, axs = plt.subplots(nrows=dlogL_num.shape[0], figsize=(15, 3 * dlogL_num.shape[0]))
    # fig, axs = plt.subplots(dlogL_num.shape[0], 1, figsize=(16,8.2))
    gradient_names = ['dlogL/d' + fpname for fpname in CONST.TRAJ_PARAMETERS_KEYS]
    if alongNum:
        label_ana = 'analytical along num trajectory'
        title = 'Analytical gradients computed along a numerical trajectory'
        file_name_default = '{}_dlogL_ana_along_num_single_traj_{}'.format(n_traj_fit, dlogL_num.shape[1])
    else:
        label_ana = 'analytical'
        title = 'Gradients of two independent trajectories starting from same point'
        file_name_default = '{}_dlogL_ana_vs_num_single_traj_{}'.format(n_traj_fit, dlogL_num.shape[1])
    for i, param_index in enumerate(params_to_plot):
    # for param_index in range(dlogL_num.shape[0]):
        axs[i].plot(steps, dlogL_num[param_index], label='numerical')
        axs[param_index].plot(steps, dlogL_ana[param_index], label=label_ana)
        axs[i].set_xlabel("steps", color='black')
        axs[i].set_ylabel("{}".format(gradient_names[param_index]), color='black')
        axs[i].legend()


    fig.suptitle(title + " - Fits done after {} trajectories of length {} in phase 1".format(n_traj_fit, length_num_traj))
    fig.tight_layout()
    fig.subplots_adjust(top=0.95)

    if show_plot:
        plt.show()

    if file_name is None:
        file_name = file_name_default
    file_name = outdir + '/' + file_name + '.png'
    if save:
        pu.mkdirs(outdir)
        fig.savefig(file_name)


def plot_num_vs_ana_positions(q_pos_num, q_pos_ana, opts_dict=None, save=True, show_plot=False, label=None, outdir='./plots'):
    from matplotlib import rc
    plt.rcParams['text.usetex'] = False
    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'
    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'
    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10

    steps = np.arange(len(q_pos_num[0]))

    fig, axs = plt.subplots(nrows=q_pos_num.shape[0], figsize=(15, 3 * q_pos_num.shape[0]))
    # fig, axs = plt.subplots(q_pos_num.shape[0], 1, figsize=(16,8.2))
    for param_index in range(q_pos_num.shape[0]):
        axs[param_index].plot(steps, q_pos_num[param_index], label='numerical')
        axs[param_index].plot(steps, q_pos_ana[param_index], label='analytical')
        axs[param_index].set_xlabel("steps", color='black')
        axs[param_index].set_ylabel("{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]), color='black')
        axs[param_index].legend()

    if opts_dict is None:
        n_traj_fit = '??'
        length_num_traj = '??'
    else:
        n_traj_fit = opts_dict['n_traj_fit']
        length_num_traj = opts_dict['length_num_traj']

    fig.suptitle("Position comparison for fits done after {} trajectories of length {} in phase 1".format(n_traj_fit, length_num_traj))
    fig.tight_layout()
    fig.subplots_adjust(top=0.95)

    if show_plot:
        plt.show()

    if label is None:
        label = 'q_pos_ana_vs_num_single_traj_{}'.format(q_pos_num.shape[1])
    file_name = outdir + '/' + label + '.png'
    if save:
        fig.savefig(file_name)

def plot_num_gradients_meth12_and_logL(dlogL_num_meth1, dlogL_num_meth2, logL, opts_dict=None, save=True, show_plot=False, file_name=None, outdir='./plots'):
    from matplotlib import rc
    plt.rcParams['text.usetex'] = False
    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'
    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'
    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10

    steps = np.arange(len(dlogL_num_meth1[0]))

    fig, axs = plt.subplots(nrows=dlogL_num_meth1.shape[0], figsize=(15, 3 * dlogL_num_meth1.shape[0]))
    # fig, axs = plt.subplots(dlogL_num_meth1.shape[0], 1, figsize=(16,8.2))
    gradient_names = ['dlogL/d' + fpname for fpname in CONST.TRAJ_PARAMETERS_KEYS]

    title = 'Comparision of the two methods to get numerical gradients'
    file_name_default = 'dlogL_num_1vs2_single_traj_{}'.format(dlogL_num_meth1.shape[1])
    for param_index in range(dlogL_num_meth1.shape[0]):
        axs[param_index].plot(steps, dlogL_num_meth1[param_index], label='num: <s-h|dh>')
        axs[param_index].plot(steps, dlogL_num_meth2[param_index], label='num: logL+ - logL-')
        axs[param_index].set_xlabel("steps", color='black')
        axs[param_index].set_ylabel("{}".format(gradient_names[param_index]), color='black')
        axs[param_index].legend()

        axlogL = axs[param_index].twinx()
        axlogL.plot(steps, logL, label='logL', color='green')
        axlogL.set_ylabel('logL', color='green')
        axlogL.tick_params('y', colors='green')
        # axlogL.set_ylim(logL.min(), logL.max())



    fig.suptitle(title)
    fig.tight_layout()
    fig.subplots_adjust(top=0.95)

    if show_plot:
        plt.show()

    if file_name is None:
        file_name = file_name_default
    file_name = outdir + '/' + file_name + '.png'
    if save:
        fig.savefig(file_name)


def twoD_plots(samples, injection_parameters, search_parameter_keys=['theta_jn', 'phase', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'dec', 'ra', 'geocent_time'], save=True, label=None, outdir='./plots'):

    # idx_tuples = [(6,7), (4,8)]
    idx_tuples = [(0,3), (4,5), (1,2), (6,7), (4,8)]
    nb_of_2D_plots = len(idx_tuples)
    # fig, axs = plt.subplots(nb_of_2D_plots, 1, sharex=True)
    fig, axs = plt.subplots(nb_of_2D_plots, 1, sharex=False, figsize=(15, 15 * nb_of_2D_plots))

    fig.suptitle('2D plots of {} samples'.format(samples.shape[0]))

    for i in range(nb_of_2D_plots):
        axs[i].plot(samples[:, idx_tuples[i][0]], samples[:, idx_tuples[i][1]], 'o')
        # axs[i].scatter(samples[:, idx_tuples[i][0]], samples[:, idx_tuples[i][1]])
        axs[i].set_xlabel(search_parameter_keys[idx_tuples[i][0]])
        axs[i].set_ylabel(search_parameter_keys[idx_tuples[i][1]])
        # axs[i].set_xlim(samples[:, idx_tuples[i][0]].min(), samples[:, idx_tuples[i][0]].max())
        # axs[i].set_autoscalex_on(True)
        # axs[i].set_autoscaley_on(True)
        # axs[i].set_autoscale_on(True)


    fig.tight_layout()
    fig.subplots_adjust(top=0.96) # Needed so that fig.title and ax.title don't overlap

    if label is None:
        label = '2D_plots_{}'.format(samples.shape[0])
    file_name = outdir + '/' + label + '.png'
    if save:
        fig.savefig(file_name)


def perso_plot_walker(walk, parameter_name, figsize=(15,5)):

    idxs = np.arange(len(walk))
    plt.figure(figsize=figsize)
    plt.plot(idxs, walk)
    plt.xlabel("Iterations")
    plt.ylabel(parameter_name)
    plt.show()


def perso_plot_walkers(walks, parameters_names, figsize=(15,15), title=""):

    nb_of_parameters = len(walks)
    nb_plots_first_column = int((nb_of_parameters + 1) / 2)

    idxs = np.arange(len(walks[0]))
    fig, axs = plt.subplots(nb_plots_first_column, 2, sharex=True, figsize=figsize)

    fig.suptitle(title)

    for i in range(nb_plots_first_column):
        axs[i][0].plot(idxs, walks[i], linewidth=1)
        # axs[i].set_xlabel("Iterations")
        axs[i][0].set_ylabel(parameters_names[i])

    for i in range(nb_plots_first_column, nb_of_parameters):
        axs[i - nb_plots_first_column][1].plot(idxs, walks[i], linewidth=1)
        # axs[i].set_xlabel("Iterations")
        axs[i - nb_plots_first_column][1].set_ylabel(parameters_names[i])

    # If odd number of chains to plot, then we hide the last axe of the second column because it's an empty plot
    if nb_of_parameters%2!=0:
        # axs[len(axs)-1][1].set_visible(False)
        fig.delaxes(axs[len(axs)-1][1])

    fig.tight_layout()

    plt.show()


def perso_plot_corner(samples, labels=None):

    fig = corner.corner(samples, labels=labels)
    plt.show()


if __name__=='__main__':
    import config as conf
    import sys
    sys.path.append('../')
    import Codes.set_injection_parameters as set_inj

    outdir_traj = '/Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/newPythonCode/__output_data/GW170817/bilbyoriented/SUB-D_012345678/PSD_1/0_500_2000_200_200_0.005_dlogL1'
    file_path_traj = outdir_traj + '/samples.dat'



    # plt.show()
