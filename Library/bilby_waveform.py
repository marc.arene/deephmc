import sys
sys.path.append('../')
import numpy as np
import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.bilby_utils as bilby_utils
import Library.python_utils as pu
import Library.param_utils as paru
import Library.bilby_detector as bilby_det
import Library.CONST as CONST

def get_source_frame_polarizations(parameters, interferometers, waveform_arguments):
    # Create the waveform_generator using a LAL Binary Neutron Star source function
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=interferometers.duration,
        sampling_frequency=interferometers.sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
        waveform_arguments=waveform_arguments
        )
    # pu.print_dict({"waveform_arguments": waveform_arguments})
    # [Marc]: h+ and hx in source frame
    source_frame_polarizations = waveform_generator.frequency_domain_strain(parameters)

    return source_frame_polarizations

def WaveForm_ThreeDetectors_from_hsource_old(parameters, interferometers, source_frame_polarizations):

    template_ifos = list()
    for ifo in interferometers:
        # [Marc]: projecting h+ and hx into each detector's frame
        # template_ifo = ifo.get_detector_response(source_frame_polarizations, parameters)
        template_ifo = bilby_det.get_detector_response(ifo, source_frame_polarizations, parameters)

        template_ifos.append(template_ifo)

    return template_ifos

def WaveForm_ThreeDetectors_old(parameters, interferometers, waveform_arguments):

    # [Marc]: h+ and hx in source frame
    source_frame_polarizations = get_source_frame_polarizations(parameters, interferometers, waveform_arguments)

    template_ifos = WaveForm_ThreeDetectors_from_hsource_old(parameters, interferometers, source_frame_polarizations)

    return template_ifos


def WaveForm_ThreeDetectors(parameters, interferometers, waveform_arguments, source_frame_polarizations=None, fd_time_translation_ifos=None):

    # [Marc]: h+ and hx in source frame
    if source_frame_polarizations is None:
        source_frame_polarizations = get_source_frame_polarizations(parameters, interferometers, waveform_arguments)

    template_ifos = list()
    for i, ifo in enumerate(interferometers):
        template_ifo_geocent = bilby_det.get_detector_response_geocent(ifo, source_frame_polarizations, parameters)
        if fd_time_translation_ifos is None:
            fd_time_translation_ifo = bilby_det.get_fd_time_translation(ifo, parameters)
        else:
            fd_time_translation_ifo = fd_time_translation_ifos[i]
        template_ifo = template_ifo_geocent * fd_time_translation_ifo

        template_ifos.append(template_ifo)

    return np.asarray(template_ifos)

def h_dh_Generation_ThreeDetectors(parameters, start_time, interferometers, waveform_arguments, **sampler_dict):
    """
    Version using the 'new' formulae for derivatives w.r.t. tc and phic.

    3-detectors version of TaylorF2 waveform computation and its NUMERICAL derivative with respect to the 9 parameters.
    Inputs:
    -------
        double *sigpar
        int n_freq
    Results:
    --------
        return h_wave1, h_wave2, h_wave3, dtemplate_ifos
    """

    offset = 1e-7

    source_frame_polarizations = get_source_frame_polarizations(parameters, interferometers, waveform_arguments)
    template_ifos = list()
    fd_time_translation_ifos = list()
    for ifo in interferometers:
        template_ifo_geocent = bilby_det.get_detector_response_geocent(ifo, source_frame_polarizations, parameters)
        fd_time_translation_ifo = bilby_det.get_fd_time_translation(ifo, parameters)
        template_ifo = template_ifo_geocent * fd_time_translation_ifo

        template_ifos.append(template_ifo)
        fd_time_translation_ifos.append(fd_time_translation_ifo)

    number_of_parameters = 9
    waveform_length = len(template_ifos[0])

    dtemplate_ifos = np.empty((len(interferometers), number_of_parameters,waveform_length), dtype=np.cfloat)

    if 0 in sampler_dict['search_parameter_indices']:
        offset_cosi = sampler_dict['parameter_offsets'][0]
        # Deriving w.r.t. np.cos(inc) where inc = parameters[0]
        # If we are at the boundary where iota = +/- pi, we only do a forward difference in cos(iota)
        if np.cos(parameters['theta_jn']) - offset_cosi < -1:
            parameters_plus = parameters.copy()
            parameters_plus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn']) + offset_cosi)

            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 0] = (template_ifos_plus[i] - template_ifos[i])/offset_cosi

        # If we are at the boundary where iota = 0, we only do a backward difference in cos(iota)
        elif np.cos(parameters['theta_jn']) + offset_cosi > 1:
            parameters_minus = parameters.copy()
            parameters_minus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])-offset_cosi)

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 0] = (template_ifos[i] - template_ifos_minus[i])/offset_cosi

        # Otherwise we keep the central difference
        else:
            parameters_minus = parameters.copy()
            parameters_minus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])-offset_cosi)

            parameters_plus = parameters.copy()
            parameters_plus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])+offset_cosi)

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 0] = (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset_cosi)

    if 1 in sampler_dict['search_parameter_indices']:
        # Deriving w.r.t. phi_c = parameters['phase'] * 2
        # h(f) = A * np.exp()
        # dh/dphic    = (h(phic+offset)-h(phic-offset))/(2*offset)
        #             = (h(phi_c)*np.exp(i*offset)-h(phi_c)*np.exp(-i*offset))/2*offset # if -1j convention used
        #             = i*sin(offset)*h(phi_ref)/offset
        #             = i * h # since offset is really small
        offset_phic = sampler_dict['parameter_offsets'][1]
        offset_phic = 1e-7
        if offset_phic==0:
            for i in range(len(interferometers)):
                dtemplate_ifos[i, 1] = 1j * template_ifos[i]
        else:
            parameters_minus = parameters.copy()
            parameters_minus['phase'] = parameters['phase']-offset_phic/2

            parameters_plus = parameters.copy()
            parameters_plus['phase'] = parameters['phase']+offset_phic/2

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 1] = (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset_phic)

    if 2 in sampler_dict['search_parameter_indices']:
        offset_psi = sampler_dict['parameter_offsets'][2]
        # Deriving w.r.t. psi = parameters['psi']
        # parameters_minus = parameters.copy()
        # parameters_minus['psi'] = parameters['psi']-offset_psi

        parameters_plus = parameters.copy()
        parameters_plus['psi'] = parameters['psi']+offset_psi

        # FORWARD DIFFERENCING
        # There is no need to regenerate the source frame waveform since psi only acts on the projection, hence we save up a bit of computation by not doing it
        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, source_frame_polarizations=source_frame_polarizations)
        for i in range(len(interferometers)):
            dtemplate_ifos[i, 2] = (template_ifos_plus[i] - template_ifos[i])/offset_psi

    if 3 in sampler_dict['search_parameter_indices']:
        # Deriving w.r.t. ln(D) where D = parameters['luminosity_distance']
        # Here since h ~ constant/D, dh/dD = -h/D. Hence dh/dlnD = D*dh/dD = -h
        offset_lnD = sampler_dict['parameter_offsets'][3]
        if offset_lnD == 0:
            for i in range(len(interferometers)):
                dtemplate_ifos[i, 3] = -template_ifos[i]
        else:
            parameters_minus = parameters.copy()
            parameters_minus['luminosity_distance'] = np.exp(np.log(parameters['luminosity_distance'])-offset_lnD)

            parameters_plus = parameters.copy()
            parameters_plus['luminosity_distance'] = np.exp(np.log(parameters['luminosity_distance'])+offset_lnD)

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 3] = (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset_lnD)

    if 4 in sampler_dict['search_parameter_indices']:
        offset_lnMc = sampler_dict['parameter_offsets'][4]
        # Deriving w.r.t. ln(Mc)
        parameters_minus = paru.parameters_new_dlnMc(parameters,-offset_lnMc)
        parameters_plus = paru.parameters_new_dlnMc(parameters,offset_lnMc)

        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

        # If parameters['symmetric_mass_ratio'] = 0.25: a backward step will give parameters_minus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use forward difference
        if parameters_minus['symmetric_mass_ratio'] > 0.25:
            # Varying the mass parameters changes fISCO, ie the end frequency of the signal after which the source frame waveform is padded with zeros. This result in the possibility of template_plus and template_minus being padded with zeros not exactly at the same index at +/- 1 difference; when this happens it creates an artificial bump in the gradient which we want to avoid.
            last_idx_non_zero = np.where(template_ifos[0]!=0)[0][-1]
            last_idx_non_zero_plus = np.where(template_ifos_plus[0]!=0)[0][-1]

            # Since template_ifos is used for the other gradients, we don't want to modify its last non zero element as it could impact them
            template_ifos_temp = template_ifos.copy()

            if last_idx_non_zero < last_idx_non_zero_plus:
                template_ifos_plus[:, last_idx_non_zero_plus] = np.zeros(len(interferometers))
            elif last_idx_non_zero > last_idx_non_zero_plus:
                template_ifos_temp[:, last_idx_non_zero] = np.zeros(len(interferometers))

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 4] = (template_ifos_plus[i] - template_ifos_temp[i])/offset_lnMc
        else:
            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            # Varying the mass parameters changes fISCO, ie the end frequency of the signal after which the source frame waveform is padded with zeros. This result in the possibility of template_plus and template_minus being padded with zeros not exactly at the same index at +/- 1 difference; when this happens it creates an artificial bump in the gradient which we want to avoid.

            last_idx_non_zero_minus = np.where(template_ifos_minus[0]!=0)[0][-1]
            last_idx_non_zero_plus = np.where(template_ifos_plus[0]!=0)[0][-1]

            if last_idx_non_zero_minus < last_idx_non_zero_plus:
                template_ifos_plus[:, last_idx_non_zero_plus] = np.zeros(len(interferometers))
            elif last_idx_non_zero_minus > last_idx_non_zero_plus:
                template_ifos_minus[:, last_idx_non_zero_minus] = np.zeros(len(interferometers))

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 4] = (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset_lnMc)

    if 5 in sampler_dict['search_parameter_indices']:
        offset_lnmu = sampler_dict['parameter_offsets'][5]
        # Deriving w.r.t. ln(mu)
        parameters_minus = paru.parameters_new_dlnmu(parameters,-offset_lnmu)
        parameters_plus = paru.parameters_new_dlnmu(parameters,offset_lnmu)

        template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

        # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
        if parameters_plus['symmetric_mass_ratio'] > 0.25:
            # Varying the mass parameters changes fISCO, ie the end frequency of the signal after which the source frame waveform is padded with zeros. This result in the possibility of template_plus and template_minus being padded with zeros not exactly at the same index at +/- 1 difference; when this happens it creates an artificial bump in the gradient which we want to avoid.
            last_idx_non_zero_minus = np.where(template_ifos_minus[0]!=0)[0][-1]
            last_idx_non_zero = np.where(template_ifos[0]!=0)[0][-1]

            # Since template_ifos is used for the other gradients, we don't want to modify its last non zero element as it could impact them
            template_ifos_temp = template_ifos.copy()

            if last_idx_non_zero_minus < last_idx_non_zero:
                template_ifos_temp[:, last_idx_non_zero] = np.zeros(len(interferometers))
            elif last_idx_non_zero_minus > last_idx_non_zero_plus:
                template_ifos_minus[:, last_idx_non_zero_minus] = np.zeros(len(interferometers))

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 5] = (template_ifos_temp[i] - template_ifos_minus[i])/offset_lnmu
        else:
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            # Varying the mass parameters changes fISCO, ie the end frequency of the signal after which the source frame waveform is padded with zeros. This result in the possibility of template_plus and template_minus being padded with zeros not exactly at the same index at +/- 1 difference; when this happens it creates an artificial bump in the gradient which we want to avoid.
            last_idx_non_zero_minus = np.where(template_ifos_minus[0]!=0)[0][-1]
            last_idx_non_zero_plus = np.where(template_ifos_plus[0]!=0)[0][-1]

            if last_idx_non_zero_minus < last_idx_non_zero_plus:
                template_ifos_plus[:, last_idx_non_zero_plus] = np.zeros(len(interferometers))
            elif last_idx_non_zero_minus > last_idx_non_zero_plus:
                template_ifos_minus[:, last_idx_non_zero_minus] = np.zeros(len(interferometers))

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 5] = (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset_lnmu)

    if 6 in sampler_dict['search_parameter_indices']:
        offset_sindec = sampler_dict['parameter_offsets'][6]
        # Deriving w.r.t. np.sin(dec) where dec = parameters['dec']
        # parameters_minus = parameters.copy()
        # parameters_minus['dec'] = np.arcsin(np.sin(parameters['dec'])-offset_sindec)

        parameters_plus = parameters.copy()
        parameters_plus['dec'] = np.arcsin(np.sin(parameters['dec'])+offset_sindec)

        # FORWARD DIFFERENCING
        # There is no need to regenerate the source frame waveform since sindec only acts on the projection, hence we save up a bit of computation by not doing it
        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, source_frame_polarizations=source_frame_polarizations)
        for i in range(len(interferometers)):
            dtemplate_ifos[i, 6] = (template_ifos_plus[i] - template_ifos[i])/offset_sindec

    if 7 in sampler_dict['search_parameter_indices']:
        offset_ra = sampler_dict['parameter_offsets'][7]
        # Deriving w.r.t. ra = parameters['ra']
        # parameters_minus = parameters.copy()
        # parameters_minus['ra'] = parameters['ra']-offset_ra

        parameters_plus = parameters.copy()
        parameters_plus['ra'] = parameters['ra']+offset_ra

        # FORWARD DIFFERENCING
        # There is no need to regenerate the source frame waveform since ra only acts on the projection, hence we save up a bit of computation by not doing it
        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, source_frame_polarizations=source_frame_polarizations)
        for i in range(len(interferometers)):
            dtemplate_ifos[i, 7] = (template_ifos_plus[i] - template_ifos[i])/offset_ra


    if 8 in sampler_dict['search_parameter_indices']:
        # Deriving w.r.t. ln(tc) where tc = parameters['meta']['tc_3p5PN']
        # dh/dlntc = tc*dh/dtc
        # dh/dlntc = -i * 2*pi*f*tc * h(f_ref)
        offset_lntc = sampler_dict['parameter_offsets'][8]
        if offset_lntc==0:
            two_pi_f_tc = -1j * 2*np.pi*interferometers.frequency_array*(parameters['geocent_time']-start_time)
            for i in range(len(interferometers)):
                dtemplate_ifos[i, 8] = two_pi_f_tc * template_ifos[i]
        else:
            parameters_minus = parameters.copy()
            parameters_minus['geocent_time'] = np.exp(np.log(parameters['geocent_time'] - start_time) - offset_lntc) + start_time
            # print("parameters_minus['geocent_time'] = {}".format(parameters_minus['geocent_time']))

            parameters_plus = parameters.copy()
            parameters_plus['geocent_time'] = np.exp(np.log(parameters['geocent_time'] - start_time) + offset_lntc) + start_time
            # print("parameters_plus['geocent_time'] = {}".format(parameters_plus['geocent_time']))

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, source_frame_polarizations=source_frame_polarizations)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, source_frame_polarizations=source_frame_polarizations)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 8] = (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset_lntc)

    return template_ifos, dtemplate_ifos

def h_dh_Generation_ThreeDetectors_Hybrid(parameters, interferometers, waveform_arguments, **sampler_dict):
    """
    """

    offset = 1e-7

    source_frame_polarizations = get_source_frame_polarizations(parameters, interferometers, waveform_arguments)
    template_ifos = list()
    fd_time_translation_ifos = list()
    for ifo in interferometers:
        template_ifo_geocent = bilby_det.get_detector_response_geocent(ifo, source_frame_polarizations, parameters)
        fd_time_translation_ifo = bilby_det.get_fd_time_translation(ifo, parameters)
        template_ifo = template_ifo_geocent * fd_time_translation_ifo

        template_ifos.append(template_ifo)
        fd_time_translation_ifos.append(fd_time_translation_ifo)

    number_of_parameters = 9
    waveform_length = len(template_ifos[0])

    dtemplate_ifos = np.empty((len(interferometers), number_of_parameters, waveform_length), dtype=np.cfloat)

    if 0 in sampler_dict['search_parameter_indices']:
        offset_cosi = sampler_dict['parameter_offsets'][0]
        # Deriving w.r.t. np.cos(inc) where inc = parameters[0]
        # If we are at the boundary where iota = +/- pi, we only do a forward difference in cos(iota)
        if np.cos(parameters['theta_jn']) - offset_cosi < -1:
            parameters_plus = parameters.copy()
            parameters_plus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn']) + offset_cosi)

            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 0] = (template_ifos_plus[i] - template_ifos[i])/offset_cosi

        # If we are at the boundary where iota = 0, we only do a backward difference in cos(iota)
        elif np.cos(parameters['theta_jn']) + offset_cosi > 1:
            parameters_minus = parameters.copy()
            parameters_minus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])-offset_cosi)

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 0] = (template_ifos[i] - template_ifos_minus[i])/offset_cosi

        # Otherwise we keep the central difference
        else:
            parameters_minus = parameters.copy()
            parameters_minus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])-offset_cosi)

            parameters_plus = parameters.copy()
            parameters_plus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])+offset_cosi)

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 0] = (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset_cosi)

    if 2 in sampler_dict['search_parameter_indices']:
        offset_psi = sampler_dict['parameter_offsets'][2]
        # Deriving w.r.t. psi = parameters['psi']
        # parameters_minus = parameters.copy()
        # parameters_minus['psi'] = parameters['psi']-offset_psi

        parameters_plus = parameters.copy()
        parameters_plus['psi'] = parameters['psi']+offset_psi

        # FORWARD DIFFERENCING
        # There is no need to regenerate the source frame waveform since psi only acts on the projection, hence we save up a bit of computation by not doing it
        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, source_frame_polarizations=source_frame_polarizations)
        for i in range(len(interferometers)):
            dtemplate_ifos[i, 2] = (template_ifos_plus[i] - template_ifos[i])/offset_psi

    if 3 in sampler_dict['search_parameter_indices']:
        # Deriving w.r.t. ln(D) where D = parameters['luminosity_distance']
        # Here since h ~ constant/D, dh/dD = -h/D. Hence dh/dlnD = D*dh/dD = -h
        offset_lnD = sampler_dict['parameter_offsets'][3]
        if offset_lnD == 0:
            for i in range(len(interferometers)):
                dtemplate_ifos[i, 3] = -template_ifos[i]
        else:
            parameters_minus = parameters.copy()
            parameters_minus['luminosity_distance'] = np.exp(np.log(parameters['luminosity_distance'])-offset_lnD)

            parameters_plus = parameters.copy()
            parameters_plus['luminosity_distance'] = np.exp(np.log(parameters['luminosity_distance'])+offset_lnD)

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments, fd_time_translation_ifos=fd_time_translation_ifos)

            for i in range(len(interferometers)):
                dtemplate_ifos[i, 3] = (template_ifos_plus[i] - template_ifos_minus[i])/(2*offset_lnD)

    return template_ifos, dtemplate_ifos

def FisherMatrix_ThreeDetectors(parameters, start_time, interferometers, waveform_arguments, **sampler_dict):
    """
    Computation of the 3-detector Fisher Information Matrix.

    Returns the FisherMatrix as a (9,9) np.matrix()
    """

    number_of_parameters = 9

    template_ifos, dtemplate_ifos = h_dh_Generation_ThreeDetectors(parameters, start_time, interferometers, waveform_arguments, **sampler_dict)

    duration = interferometers.duration

    FisherMatrix = np.zeros((number_of_parameters,number_of_parameters))
    FisherMatrix_debug = [[None for k in range(sampler_dict['n_dim'])] for k in range(sampler_dict['n_dim'])]

    for i in range(number_of_parameters):
        for j in range(number_of_parameters):
            for k in range(len(interferometers)):
                FisherMatrix[i][j] += bilby_utils.noise_weighted_inner_product(dtemplate_ifos[k, i], dtemplate_ifos[k, j], interferometers[k].psd_array, duration)
            # FisherMatrix_debug[i][j] = str(H_i_j) + '|' + str(L_i_j) + '|' + str(V_i_j)

    # FisherMatrix = np.array(FisherMatrix)

    return FisherMatrix

def dlogL_ThreeDetectors1(parameters, start_time, interferometers, waveform_arguments, l=None, i=None, **sampler_dict):
    """
    Returns the gradient of the log-likelihood evaluated on sigpar.

    return dlogL
    """
    # import IPython; IPython.embed()
    # Compute waveform and derivatives of waveform at initial position [~320ms = (1 + 4*2) * (9ms + 3*7ms) + 3 * 3*7ms]
    template_ifos, dtemplate_ifos = h_dh_Generation_ThreeDetectors(parameters, start_time, interferometers, waveform_arguments, **sampler_dict)

    duration = interferometers.duration
    # Compute gradient of log-likelihood. Cf equation (8.5) in Yann's
    dlogL = np.zeros(9)

    # [~160ms = 9 x 3 x 2 x 0.5ms]
    for j in sampler_dict['search_parameter_indices']:
        for k in range(len(interferometers)):
            s_minus_h = interferometers[k].fd_strain - template_ifos[k]
            dlogL[j] += bilby_utils.noise_weighted_inner_product(s_minus_h, dtemplate_ifos[k, j], interferometers[k].psd_array, duration)


    logL = bilby_utils.loglikelihood(template_ifos, interferometers)

    return dlogL, logL

def dlogL_ThreeDetectors2(parameters, start_time, interferometers, waveform_arguments, l=None, i=None, **sampler_dict):
    """

    """

    offset = 1e-7

    template_ifos = WaveForm_ThreeDetectors(parameters, interferometers, waveform_arguments)

    logL = bilby_utils.loglikelihood(template_ifos, interferometers)

    dlogL = np.zeros(9)

    if 0 in sampler_dict['search_parameter_indices']:
        offset_cosi = sampler_dict['parameter_offsets'][0]
        # Deriving w.r.t. np.cos(inc) where inc = parameters[0]
        # If we are at the boundary where iota = +/- pi, we only do a forward difference in cos(iota)
        if np.cos(parameters['theta_jn']) - offset_cosi < -1:
            parameters_plus = parameters.copy()
            parameters_plus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn']) + offset_cosi)

            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)

            logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

            dlogL[0] = (logL_plus - logL)/offset_cosi
        # If we are at the boundary where iota = 0, we only do a backward difference in cos(iota)
        elif np.cos(parameters['theta_jn']) + offset_cosi > 1:
            parameters_minus = parameters.copy()
            parameters_minus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])-offset_cosi)

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)

            logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)

            dlogL[0] = (logL - logL_minus)/offset_cosi
        # Otherwise we keep the central difference
        else:
            parameters_minus = parameters.copy()
            parameters_minus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])-offset_cosi)

            parameters_plus = parameters.copy()
            parameters_plus['theta_jn'] = np.arccos(np.cos(parameters['theta_jn'])+offset_cosi)

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)

            logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)
            logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

            dlogL[0] = (logL_plus - logL_minus)/(2*offset_cosi)

    if 1 in sampler_dict['search_parameter_indices']:
        # Deriving w.r.t. phi_c = parameters['phase'] * 2
        # h(f) = A * np.exp()
        # dh/dphic    = (h(phic+offset)-h(phic-offset))/(2*offset)
        #             = (h(phi_c)*np.exp(i*offset)-h(phi_c)*np.exp(-i*offset))/2*offset # if -1j convention used
        #             = i*sin(offset)*h(phi_ref)/offset
        #             = i * h # since offset is really small
        offset_phic = sampler_dict['parameter_offsets'][1]
        if offset_phic==0:
            # logL = s_h - 0.5 * h_h
            # dlogL = s_dh - h_dh
            # dh/dphic = 1j*h
            dtemplate_ifos_phic = []
            for i in range(len(interferometers)):
                dtemplate_ifos_phic.append(1j * template_ifos[i])

            s_dh = bilby_utils.nwip_s_h_network(dtemplate_ifos_phic, interferometers)
            h_dh = bilby_utils.nwip_network(template_ifos, dtemplate_ifos_phic, interferometers)
            dlogL[1] = s_dh - h_dh
        else:
            parameters_minus = parameters.copy()
            parameters_minus['phase'] = parameters['phase']-offset_phic/2

            parameters_plus = parameters.copy()
            parameters_plus['phase'] = parameters['phase']+offset_phic/2

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)

            logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)
            logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

            dlogL[1] = (logL_plus - logL_minus)/(2*offset_phic)

    if 2 in sampler_dict['search_parameter_indices']:
        offset_psi = sampler_dict['parameter_offsets'][2]
        # Deriving w.r.t. psi = parameters['psi']
        parameters_minus = parameters.copy()
        parameters_minus['psi'] = parameters['psi']-offset_psi

        parameters_plus = parameters.copy()
        parameters_plus['psi'] = parameters['psi']+offset_psi

        template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)
        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)

        logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)
        logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

        dlogL[2] = (logL_plus - logL_minus)/(2*offset_psi)

    if 3 in sampler_dict['search_parameter_indices']:
        # logL = s_h - 0.5 * h_h
        # dlogL = s_dh - h_dh
        # dh/dlnD = -h
        # dlogL/dlnD = - s_h + h_h
        s_h = bilby_utils.nwip_s_h_network(template_ifos, interferometers)
        h_h = bilby_utils.nwip_h_h_network(template_ifos, interferometers)
        dlogL[3] = - s_h + h_h

    if 4 in sampler_dict['search_parameter_indices']:
        offset_lnMc = sampler_dict['parameter_offsets'][4]
        # Deriving w.r.t. ln(Mc)
        parameters_minus = paru.parameters_new_dlnMc(parameters,-offset_lnMc)
        parameters_plus = paru.parameters_new_dlnMc(parameters,offset_lnMc)

        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)
        logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

        # If parameters['symmetric_mass_ratio'] = 0.25: a backward step will give parameters_minus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use forward difference
        if parameters_minus['symmetric_mass_ratio'] > 0.25:
            dlogL[4] = (logL_plus - logL)/offset_lnMc
        else:
            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)

            logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)

            dlogL[4] = (logL_plus - logL_minus)/(2*offset_lnMc)

    if 5 in sampler_dict['search_parameter_indices']:
        offset_lnmu = sampler_dict['parameter_offsets'][5]
        # Deriving w.r.t. ln(mu)
        parameters_minus = paru.parameters_new_dlnmu(parameters,-offset_lnmu)
        parameters_plus = paru.parameters_new_dlnmu(parameters,offset_lnmu)

        template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)

        logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)

        # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
        if parameters_plus['symmetric_mass_ratio'] > 0.25:
            dlogL[5] = (logL - logL_minus)/offset_lnmu
        else:
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)

            logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

            dlogL[5] = (logL_plus - logL_minus)/(2*offset_lnmu)

    if 6 in sampler_dict['search_parameter_indices']:
        offset_sindec = sampler_dict['parameter_offsets'][6]
        # Deriving w.r.t. np.sin(dec) where dec = parameters['dec']
        parameters_minus = parameters.copy()
        parameters_minus['dec'] = np.arcsin(np.sin(parameters['dec'])-offset_sindec)

        parameters_plus = parameters.copy()
        parameters_plus['dec'] = np.arcsin(np.sin(parameters['dec'])+offset_sindec)

        template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)
        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)

        logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)
        logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

        dlogL[6] = (logL_plus - logL_minus)/(2*offset_sindec)

        # if l == 1 and i in [30,31,32]:
        #     import IPython; IPython.embed();
            # cd '/Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/newPythonCode/__output_data/ GW170817/bilbyoriented/SUB-D_######6##/logL920.44/0e+00_1_2000_200_34_0.005_dlogL2_1e-07/instability_analysis'
            # pu.mkdirs('step_30')
            # cd step_30/
            # values = {'sin_dec': np.sin(parameters['dec']), 'offset': offset_sindec, 'logL_plus': logL_plus, 'logL_minus': logL_minus, 'dlogL': dlogL[6]}
            # np.save('values',values)
            # np.savetxt('h_wave_1_minus.dat', h_wave_1_minus.view(float))
            # np.savetxt('h_wave_1_plus.dat', h_wave_1_plus.view(float))
            # np.savetxt('h_wave_1.dat', h_wave_1.view(float))

    if 7 in sampler_dict['search_parameter_indices']:
        offset_ra = sampler_dict['parameter_offsets'][7]
        # Deriving w.r.t. ra = parameters['ra']
        parameters_minus = parameters.copy()
        parameters_minus['ra'] = parameters['ra']-offset_ra

        parameters_plus = parameters.copy()
        parameters_plus['ra'] = parameters['ra']+offset_ra

        template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)
        template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)

        logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)
        logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

        dlogL[7] = (logL_plus - logL_minus)/(2*offset_ra)

    if 8 in sampler_dict['search_parameter_indices']:
        # logL = s_h - 0.5 * h_h
        # dlogL = s_dh - dh_dh
        # dh/dlntc = tc*dh/dtc
        # dh/dlntc = -i * 2*pi*f*tc * h(f_ref)
        offset_lntc = sampler_dict['parameter_offsets'][8]
        if offset_lntc==0:
            two_pi_f_tc = -1j * 2*np.pi*interferometers.frequency_array*(parameters['geocent_time']-start_time)
            dh_Wave_1 = two_pi_f_tc * h_wave_1
            dh_Wave_2 = two_pi_f_tc * h_wave_2
            dh_Wave_3 = two_pi_f_tc * h_wave_3
            dh_Waves = [dtemplate_ifos]
            s_dh = bilby_utils.nwip_s_h_network(dtemplate_ifos, interferometers)
            h_dh = bilby_utils.nwip_network(template_ifos, dh_Waves, interferometers)
            dlogL[8] = s_dh - h_dh
        else:
            parameters_minus = parameters.copy()
            parameters_minus['geocent_time'] = np.exp(np.log(parameters['geocent_time'] - start_time) - offset_lntc) + start_time
            # print("parameters_minus['geocent_time'] = {}".format(parameters_minus['geocent_time']))

            parameters_plus = parameters.copy()
            parameters_plus['geocent_time'] = np.exp(np.log(parameters['geocent_time'] - start_time) + offset_lntc) + start_time
            # print("parameters_plus['geocent_time'] = {}".format(parameters_plus['geocent_time']))

            template_ifos_minus = WaveForm_ThreeDetectors(parameters_minus, interferometers, waveform_arguments)
            template_ifos_plus = WaveForm_ThreeDetectors(parameters_plus, interferometers, waveform_arguments)

            logL_minus = bilby_utils.loglikelihood(template_ifos_minus, interferometers)
            logL_plus = bilby_utils.loglikelihood(template_ifos_plus, interferometers)

            dlogL[8] = (logL_plus - logL_minus)/(2*offset_lntc)

    return dlogL, logL

def dlogL_ThreeDetectors(parameters, start_time, interferometers, waveform_arguments, l=None, i=None, **sampler_dict):
    if sampler_dict['dlogL']=='dlogL2':
        return dlogL_ThreeDetectors2(parameters, start_time, interferometers, waveform_arguments, l, i, **sampler_dict)
    else:
        return dlogL_ThreeDetectors1(parameters, start_time, interferometers, waveform_arguments, l, i, **sampler_dict)

def dlogL_ThreeDetectors_Hybrid(parameters, start_time, interferometers, waveform_arguments, l=None, i=None, **sampler_dict):
    """
    Returns the gradient of the log-likelihood evaluated on sigpar.

    return dlogL
    """
    # import IPython; IPython.embed()
    # Compute waveform and derivatives of waveform at initial position [~320ms = (1 + 4*2) * (9ms + 3*7ms) + 3 * 3*7ms]
    template_ifos, dtemplate_ifos = h_dh_Generation_ThreeDetectors_Hybrid(parameters, interferometers, waveform_arguments, **sampler_dict)

    duration = interferometers.duration
    # Compute gradient of log-likelihood. Cf equation (8.5) in Yann's
    dlogL = np.zeros(9)

    # [~160ms = 9 x 3 x 2 x 0.5ms]
    for j in [0,2,3]:
        for k in range(len(interferometers)):
            s_minus_h = interferometers[k].fd_strain - template_ifos[k]
            dlogL[j] += bilby_utils.noise_weighted_inner_product(s_minus_h, dtemplate_ifos[k, j], interferometers[k].psd_array, duration)

    # if l==1 and i in [89,90,91]:
    #     print("dlogL_ThreeDetectors()")
    #     import IPython; IPython.embed();

    return dlogL

if __name__=='__main__':
    from optparse import OptionParser
    import Codes.set_injection_parameters as set_inj

    usage = """%prog [options]
    Plots the three waveforms in the time domain"""
    parser=OptionParser(usage)
    # parser.add_option("--psd",default=1,action="store",type="int",help="""1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.""",metavar="INT from {1,2,3}")
    parser.add_option("--ifos", default='H1,L1,V1', action="store", type="string", help=""" Interferometers to run the analysis on. Default is 'H1,L1,V1' """)

    (opts,args)=parser.parse_args()

    plot_on_same_figure = True

    # SET THE INJECTION PARAMETERS
    injection_parameters = set_inj.ini_file_to_dict()
    pu.print_dict(injection_parameters)
    minimum_frequency = injection_parameters['meta']['minimum_frequency']
    maximum_frequency_ifo = injection_parameters['meta']['maximum_frequency_ifo']
    duration = injection_parameters['meta']['duration']
    sampling_frequency = injection_parameters['meta']['sampling_frequency']
    f_high = injection_parameters['meta']['f_high']
    start_time = injection_parameters['meta']['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    ifos_possible = ['H1', 'L1', 'V1']
    ifos_chosen = opts.ifos.split(',')
    if set.intersection(set(ifos_possible), set(ifos_chosen)) != set(ifos_chosen):
        raise ValueError("the '--ifos' option was wrongly set, you must choose between {}. Example: '--ifos=H1,V1'. ".format(ifos_possible))
    interferometers = bilby.gw.detector.InterferometerList(ifos_chosen)
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        ifo.maximum_frequency = maximum_frequency_ifo
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time
    # import IPython; IPython.embed()
    template_ifos = WaveForm_ThreeDetectors(injection_parameters, interferometers, waveform_arguments)


    import matplotlib.pyplot as plt
    from matplotlib import rc
    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    ## for Palatino and other serif fonts use:
    # rc('font',**{'family':'serif','serif':['Palatino']})
    rc('text', usetex=True)
    plt.rc('text', usetex=False)
    plt.rc('font', family='serif')
    plt.rc('font', size=20)
    plt.rc('font', weight='bold')

    ifo_colors = ['red', 'green', 'purple']

    from scipy import signal

    f_low = minimum_frequency

    # indices = np.where((f_low<interferometers.frequency_array) & (interferometers.frequency_array<f_high))[0]


    h_wave_td = []
    for h_fd in template_ifos:
        dwindow = signal.tukey(h_fd.size, alpha=1./8)
        h_td = np.fft.irfft(h_fd * dwindow)
        # h_td = np.fft.irfft(h_fd)
        # h_td_unwindowed = h_td / dwindow
        h_wave_td.append(h_td)

    for ifo_index in range(len(interferometers)):
        number_of_samples = len(h_wave_td[ifo_index])
        duration_td = number_of_samples / sampling_frequency

    time = np.linspace(start=0, stop=duration_td, num=number_of_samples)
    idx_start = np.where(time>56.915)[0][0]
    idx_stop = np.where(time<56.935)[0][-1]

    fISCO = set_inj.Compute_fISCO(6, injection_parameters['mass_1'], injection_parameters['mass_2'])
    time_until_fISCO = set_inj.compute_time_from_flow_to_fhigh_3p5PN(minimum_frequency, fISCO, injection_parameters['mass_1'], injection_parameters['mass_2'])

    if plot_on_same_figure:
        plt.figure(figsize=(15,10))
        for ifo_index in range(len(interferometers)):
            plt.plot(time, h_wave_td[ifo_index], linewidth=1, color=ifo_colors[ifo_index], label=interferometers[ifo_index].name)
            # plt.plot(time[idx_start:idx_stop], h_wave_td[ifo_index][idx_start:idx_stop], linewidth=1, color=ifo_colors[ifo_index], label=interferometers[ifo_index].name)
        plt.axvline(x=time_until_fISCO, linewidth=1, color='blue', label='time to reach fISCO')
        plt.axvline(x=injection_parameters['meta']['tc_3p5PN'], color='black', linewidth=1, label='tc')
        text = 'f_low = {:.2f}Hz\nf_ISCO = {:.2f}Hz\nmax_freq = {:.2f}Hz\nsamp_freq= {:.2f}Hz\ntc_3p5PN = {:.4f}s'.format(minimum_frequency, fISCO, injection_parameters['meta']['maximum_frequency_ifo'], sampling_frequency, injection_parameters['meta']['tc_3p5PN'])
        plt.text(duration-7, h_wave_td[0].max()*2/3, text, fontsize=10)
        # import IPython; IPython.embed()
        plt.xlabel("Time (sec)")
        plt.ylabel("h")
        plt.legend()
        plt.title(" GW170817 - {}".format(injection_parameters['meta']['approximant']))
    else:
        for ifo_index in range(len(interferometers)):
            plt.figure(figsize=(15,5))
            plt.plot(time, h_wave_td[ifo_index], linewidth=1, color=ifo_colors[ifo_index])
            plt.xlabel("Time (sec)")
            plt.ylabel("h")
            plt.title(title_prefix + "GW170817 - {}, fmin={}Hz".format(injection_parameters['meta']['approximant'], f_low))

    plt.show()

    sys.exit()

    # C02 PE followup (for catalog paper)
    # https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/ParameterEstimationModelSelection/O2_PE/O2/1187008882p4457#C02_PE_followup_.28for_catalog_paper.29

    # nb2: fixed sky position, TF2 MCMC run on C02 data on GW170817 from PE group:
    # https://ldas-jobs.ligo.caltech.edu/~aaron.zimmerman/O2/G298048/C02/C02-1_TF2LS/lalinferencemcmc/TaylorF2threePointFivePN/1187008882.45-0/V1H1L1/posplots.html

    # nb6: non fixed sky position
    # https://ldas-jobs.ligo.caltech.edu/~aaron.zimmerman/O2/G298048/C02/C02-6_TF2NoFix/lalinferencemcmc/TaylorF2threePointFivePN/1187008882.45-0/V1H1L1/posplots.html
