import numpy as np
import os
import json
import time

def read_dat_file(fileName,columnSep=None):
    """
    Returns a numpy array with shape (n,m) where n is the number of lines in the fileName (each should be separated by '\n') and m the number of columns (each should be separated by columnSep where default None value is interprated as a space or '\t'?).
    Returns:
    -------
        np.array of shape (n,m)
            n = nb of lines in the file - separator = '\n'
            m = nb of columns in the file - separator = columnSep, default='\t'
    """

    with open(fileName) as file:
        read_data=file.readlines()
    file.closed

    array=[]

    for i in range(len(read_data)):
        array.append(list(map(float,read_data[i].split(columnSep))))

    np_array=np.array(array)

    return np_array.transpose()

def write_dat_file(dataArray,fileName,columnSep='\t',floatPrecision=12):
    """
    Writes the (n,m) shaped dataArray into the a file, separating the n lines by '\n' and the m columns by columnSep.
    Numbers are written in scientific format with a default float precision of 12 digits.

    To write the file into another directory than the current one, use absolute path for "fileName" or relatvie path like: fileName='../data/myFileName.dat'
    """

    with open(fileName,"w") as outputFile:
        for i in range(len(dataArray)):
            for j in range(len(dataArray[i])-1):
                outputFile.write("{:.{}e}{}".format(dataArray[i][j],floatPrecision,columnSep))
            outputFile.write("{:.{}e}\n".format(dataArray[i][-1],floatPrecision,columnSep))
        outputFile.closed

def mkdirs(path):
    """
    Helper function. Make the given directory, creating intermediate
    dirs if necessary, and don't complain about it already existing.
    """
    if os.access(path,os.W_OK) and os.path.isdir(path): return
    else: os.makedirs(path)

def print_dict(dictionary, indent=3, align_keys=True):
    """
    Prints a dictionary as a succession of print(key: value), dealing with nested dictionaries.

    Parameters:
        indent: int, default=3
            number of spaces used to indent the nested dictionaries
        align_keys: boolean, default=True
            aligns vertically the printed values
    """
    indent_align_key_str = ''
    # We need to know the biggest length of all keys in order to align the prints
    if align_keys:
        max_length_key = 0
        for key in dictionary.keys():
            max_length_key = max(max_length_key, len(key))

    # Multiply a space string by the number of indent wished to get the string which will be appended before printing each key
    indent_str = ' ' * indent

    # Now iterate over the dictionary
    for key, value in dictionary.items():
        if align_keys:
            indent_align_key = max(max_length_key - len(key), 0)
            indent_align_key_str = ' ' * indent_align_key
        # If the key contains itself a dictionary, we recursively call this `print_dict()` function but multiplying the indent by 2 such that the nested structure appears well on the print
        if type(value)==dict:
            print(indent_str + key + indent_align_key_str + ':')
            print_dict(value, indent*2, align_keys)
        else:
            print(indent_str + key + indent_align_key_str + ': {}'.format(value))

def get_sub_dict(dictionary, keys):

    sub_dict = dict((k, dictionary[k]) for k in keys if k in dictionary)

    return sub_dict

def savetxt_complex(path, complex_array):
    """
    Saves a complex array into a text file using np.savetxt().

    Parameters:
    -----------
        path: /file/path/file_name.extension
        complex_array: numpy array of complex numbers
    """
    np.savetxt(path, complex_array.view(float))
    return 0

def loadtxt_complex(path):
    """
    Loads a complex array from a text file using np.loadtxt().

    Parameters:
    -----------
        path: /file/path/file_name.extension

    Returns:
    --------
        Numplex array of complex numbers.
    """
    return np.loadtxt(path).view(complex)

def config_parser_to_dict(config):
    """
    Converts a configparser comming from an ini file to a python dictionary. Inputs will try to be converted into floats, and will be treated as text inputs if that fails.

    Parameters:
    -----------
    config: configparser.ConfigParser()
        Contains the content of the ini file through `config.read('file.ini')`

    Returns:
    --------
    dictionary: dict
        A dictionary containing one primary key per section in the ini file. Each of these keys is itself another dictionary with the section options as keys.

    """
    dictionary = {}
    for section in config.sections():
        dictionary[section] = {}
        for option in config.options(section):
            try:
                dictionary[section][option] = config.getint(section, option)
            except ValueError:
                try:
                    dictionary[section][option] = config.getboolean(section, option)
                except ValueError:
                    try:
                        dictionary[section][option] = config.getfloat(section, option)
                    except ValueError:
                        dictionary[section][option] = config.get(section, option)

    return dictionary

def save_dict_to_json(dictionary, file_path, indent=2):
    name_and_extension = file_path.rsplit('.', maxsplit=1)
    filename_json = name_and_extension[0] + '.json'
    with open(filename_json, 'w') as file:
        json.dump(dictionary, file, indent=indent)
    file.close

def json_to_dict(file_path):
    with open(file_path, 'r') as file:
        dictionary = json.load(file)
    file.close
    return dictionary

def get_time_stamp_compact():
    # import time
    # year = str(time.localtime().tm_year)[-2:]
    # month = str(time.localtime().tm_mon)
    # day = str(time.localtime().tm_mday)
    # hour = str(time.localtime().tm_hour)
    # min = str(time.localtime().tm_min)
    # sec = str(time.localtime().tm_sec)
    # time_stamp = year + month + day + '_' + hour + min + '_' + sec
    # import datetime
    # d = datetime.datetime.today()
    # time_stamp = '{:%y%m%d_%H%M%S}'.format(d)
    import time
    time_stamp = time.strftime('%y%m%d_%H%M%S')
    return time_stamp

def scalar_product(array_a, array_b):
    integrand = array_a.real * array_b.real + array_a.imag * array_b.imag
    return np.sum(integrand)

def overlap(array_a, array_b):
    aa = scalar_product(array_a, array_a)
    bb = scalar_product(array_b, array_b)
    ab = scalar_product(array_a, array_b)
    return ab / np.sqrt(aa * bb)

# def scalarProduct(h1,h2,step=1):
#     """
#     Computes the scalar product defined by:
#         <h1|h2>=step*sum(h1*h2_c+h1_c*h2)*1/2
#                =step*sum(h1_real*h2_real + h1_img*h2_img)
#
#     Here h1 and h2 are expected to store their real and imaginary parts on respectively their even and odd indices (hence total length should be divisible by 2).
#     """
#
#     if len(h1)!=len(h2):
#         raise ValueError("Input vectors are not of same length: len(h1)={} and len(h2)={}".format(len(h1),len(h2)))
#
#     prod=0
#     arg=0
#
#     nbOfComplex=int(len(h1)/2)
#
#     for i in range(nbOfComplex):
#         j = (2 * i)
#         k = j + 1
#         prod = h1[j] * h2[j] + h1[k] * h2[k]
#         arg += prod
#
#     arg*=step
#
#     return arg
#
# def overlap(h1,h2):
#     """
#     return scalarProduct(h1,h2,1)/sqrt(scalarProduct(h1,h1,1)*scalarProduct(h2,h2,1))
#     """
#     # "Natural" version
#     h1_h1_1 = scalarProduct(h1,h1,1)
#     h2_h2_1 = scalarProduct(h2,h2,1)
#     h1_h2_1 = scalarProduct(h1,h2,1)
#     sqrt_h1_h1_h2_h2 = np.sqrt(h1_h1_1*h2_h2_1)
#
#     # print("scalarProduct(h1,h1,1):\n{}".format(Decimal(h1_h1_1)))
#     # print("scalarProduct(h2,h2,1):\n{}".format(Decimal(h2_h2_1)))
#     # print("np.sqrt(scalarProduct(h1,h1,1)*scalarProduct(h2,h2,1)):\n{}".format(Decimal(sqrt_h1_h1_h2_h2)))
#     # print("scalarProduct(h1,h2,1):\n{}\n".format(Decimal(h1_h2_1)))
#
#
#
#     # return scalarProduct(h1,h2,1)/np.sqrt(scalarProduct(h1,h1,1)*scalarProduct(h2,h2,1))
#     return h1_h2_1/sqrt_h1_h1_h2_h2

    # # Normalised scalar product version
    # h1_h1_norm = np.sqrt(scalarProduct(h1,h1,1))
    # h2_h2_norm = np.sqrt(scalarProduct(h2,h2,1))
    # h1_normalised = h1/h1_h1_norm
    # h2_normalised = h2/h2_h2_norm
    # return scalarProduct(h1_normalised,h2_normalised,1)
