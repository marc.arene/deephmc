import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np

from bilby.gw.likelihood import GravitationalWaveTransient, ROQGravitationalWaveTransient

import Library.param_utils as paru
import Library.bilby_utils as bilby_utils
import Library.CONST as CONST


def identity(value):
    """
    Function needed for parameter which are sampled without any function around them.
    """
    return value

TRAJECTORY_FUNCTIONS = {
    'cosine': np.cos,
    'cosinus': np.cos,
    'cos': np.cos,
    'sine': np.sin,
    'sinus': np.sin,
    'sin': np.sin,
    'log': np.log,
    'logarithm': np.log,
    'none': identity,
    'identity': identity
}

TRAJECTORY_INVERSE_FUNCTIONS = {
    'cosine': np.arccos,
    'cosinus': np.arccos,
    'cos': np.arccos,
    'sine': np.arcsin,
    'sinus': np.arcsin,
    'sin': np.arcsin,
    'log': np.exp,
    'logarithm': np.exp,
    'none': identity,
    'identity': identity
}

class LikelihoodGradient(object):
    """

    """
    def __init__(self, likelihood, search_parameter_keys, dict_parameters_offsets=None):
        self.likelihood = likelihood
        self.search_parameter_keys = search_parameter_keys
        self.dict_parameters_offsets = dict_parameters_offsets
        self.log_likelihood_gradient = dict()
        # for key in search_parameter_keys:
        #     self.log_likelihood_gradient[key] = None
        self.offset = 1e-7

    def calculate_log_likelihood_gradient(self):
        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()

        for key in self.search_parameter_keys:
            self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
            self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] - self.offset
            log_likelihood_minus = self.likelihood.log_likelihood_ratio()

            self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] + self.offset
            log_likelihood_plus = self.likelihood.log_likelihood_ratio()

            self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    @property
    def n_dim(self):
        if self.search_parameter_keys is None:
            return 0
        else:
            return len(self.search_parameter_keys)

class GWTransientLikelihoodGradient(LikelihoodGradient):
    """

    """
    def __init__(self, likelihood, search_parameter_keys,  dict_trajectory_functions_str, dict_trajectory_parameters_keys, dict_parameters_offsets=None):
        LikelihoodGradient.__init__(self, likelihood, search_parameter_keys, dict_parameters_offsets)
        self.search_trajectory_functions = self.get_trajectory_functions(dict_trajectory_functions_str)
        self.search_trajectory_inv_functions = self.get_trajectory_inverse_functions(dict_trajectory_functions_str)
        self.dict_trajectory_parameters_keys = self.get_dict_trajectory_parameters_keys(dict_trajectory_parameters_keys)
        self.search_trajectory_parameters_keys = self.get_trajectory_parameters_keys(self.dict_trajectory_parameters_keys)
        self.start_time = likelihood.interferometers[0].strain_data.start_time
        self.dlogL_method = 's_minus_h_dh'
        # self.dlogL_method = 'logL_diff'

    def get_trajectory_functions(self, dict_trajectory_functions_str):
        parameters_functions = []
        for key in self.search_parameter_keys:
            if key not in dict_trajectory_functions_str.keys():
                func_name = 'none'
            else:
                func_name = dict_trajectory_functions_str[key]
            parameters_functions.append(TRAJECTORY_FUNCTIONS[func_name.lower()])
        return parameters_functions

    def get_trajectory_inverse_functions(self, dict_trajectory_functions_str):
        parameters_inv_functions = []
        for key in self.search_parameter_keys:
            if key not in dict_trajectory_functions_str.keys():
                func_name = 'none'
            else:
                func_name = dict_trajectory_functions_str[key]
            parameters_inv_functions.append(TRAJECTORY_INVERSE_FUNCTIONS[func_name.lower()])
        return parameters_inv_functions

    def get_dict_trajectory_parameters_keys(self, dict_trajectory_parameters_keys):
        """
        Fills the gaps of the inputed dictionary: dict_trajectory_parameters_keys, associating a search_parameter to its 'trajectory-parameter-name'. Indeed when a search_parameter is its own trajectory-parameter we allow the user to say 'none' or not mention it.
        """
        dict_traj_param_keys_full = dict_trajectory_parameters_keys.copy()
        for key in self.search_parameter_keys:
            if key not in dict_trajectory_parameters_keys.keys() or dict_trajectory_parameters_keys[key].lower() == 'none':
                dict_traj_param_keys_full[key] = key
        return dict_traj_param_keys_full

    def get_trajectory_parameters_keys(self, dict_trajectory_parameters_keys):
        search_trajectory_parameters_keys = []
        for key in self.search_parameter_keys:
            search_trajectory_parameters_keys.append(dict_trajectory_parameters_keys[key])
        return search_trajectory_parameters_keys

    @property
    def interferometers(self):
        return self.likelihood.interferometers

    @property
    def uses_roq(self):
        return isinstance(self.likelihood, ROQGravitationalWaveTransient)

    def calculate_log_likelihood_gradient(self, search_trajectory_parameters_keys=None):
        use_logL_diff_method = self.dlogL_method == 'logL_diff' or isinstance(self.likelihood, ROQGravitationalWaveTransient)

        if use_logL_diff_method:
            return self.calculate_dlogL_logL_diff(search_trajectory_parameters_keys=search_trajectory_parameters_keys)
        else:
            return self.calculate_dlogL_s_minus_h_dh(search_trajectory_parameters_keys=search_trajectory_parameters_keys)

    def calculate_dlogL_logL_diff(self, search_trajectory_parameters_keys=None):
        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()
        self.log_likelihood_at_point = self.likelihood.log_likelihood_ratio()

        # By default we want to compute the numerical gradient over all the searched parameters.
        # For hybrid trajectories only a subset of the parameter space will be numerically computed.
        if search_trajectory_parameters_keys is None:
            search_trajectory_parameters_keys = self.search_trajectory_parameters_keys.copy()
        for key in search_trajectory_parameters_keys:
            if key=='cos_theta_jn':
                lalsim_key = 'theta_jn'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

                # If we are at the boundary where iota = +/- pi, we only do a forward difference in cos(iota)
                if np.cos(self.likelihood.parameters_at_point[lalsim_key]) - self.offset < -1:
                    self.likelihood.parameters[lalsim_key] = np.arccos(np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                    log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
                # If we are at the boundary where iota = 0, we only do a backward difference in cos(iota)
                elif np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset > 1:
                    self.likelihood.parameters[lalsim_key] = np.arccos(np.cos(self.likelihood.parameters_at_point[lalsim_key]) - self.offset)
                    log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                    self.log_likelihood_gradient[key] = (self.log_likelihood_at_point - log_likelihood_minus) / self.offset
                # Otherwise we would keep the central difference but forward is sufficient for this parameter
                else:
                    self.likelihood.parameters[lalsim_key] = np.arccos(np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                    log_likelihood_plus = self.likelihood.log_likelihood_ratio()

                    self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='phi_c':
                lalsim_key = 'phase'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset / 2
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='phase':
                lalsim_key = 'phase'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='psi':
                lalsim_key = 'psi'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[lalsim_key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ln(D)':
                lalsim_key = 'luminosity_distance'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                if True:
                    d_inner_h = 0
                    h_inner_h = 0
                    waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.likelihood.parameters)
                    for ifo in self.interferometers:
                        # `calculate_snrs()` works for ROQ and non-ROQ likelihoods
                        per_detector_snr = self.likelihood.calculate_snrs(
                            waveform_polarizations=waveform_polarizations,
                            interferometer=ifo)
                        d_inner_h += per_detector_snr.d_inner_h.real
                        h_inner_h += per_detector_snr.optimal_snr_squared.real
                    self.log_likelihood_gradient[key] = -d_inner_h + h_inner_h
                else:
                    self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                    log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                    # forward is sufficient for this parameter
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ln(Mc)':
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

                self.likelihood.parameters = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()

                parameters_minus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, -self.offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a backward step will give parameters_minus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use forward difference
                if parameters_minus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
                else:
                    self.likelihood.parameters = parameters_minus
                    log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='ln(mu)':
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

                self.likelihood.parameters = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -self.offset)
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()

                parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, self.offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
                if parameters_plus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = (self.log_likelihood_at_point - log_likelihood_minus) / self.offset
                else:
                    self.likelihood.parameters = parameters_plus
                    log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='sin(dec)':
                lalsim_key = 'dec'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ra':
                lalsim_key = 'ra'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[lalsim_key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ln(tc)':
                # The following line was added because for BBH signal, an offset = 1e-7 was to small compared to the variations of tc and 1e-5 worked better
                # self.offset = sampler_dict['parameters_offsets'][8]
                lalsim_key = 'geocent_time'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

                self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) - self.offset) + self.start_time
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()

                self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) + self.offset) + self.start_time
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()

                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)

                # self.offset = 1e-7
            # The other parameters handled by this else case are: psi, ra
            else:
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] - self.offset
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    def calculate_dlogL_s_minus_h_dh(self, search_trajectory_parameters_keys=None):

        # By default we want to compute the numerical gradient over all the searched parameters.
        # For hybrid trajectories only a subset of the parameter space will be numerically computed.
        if search_trajectory_parameters_keys==None:
            search_trajectory_parameters_keys = self.search_trajectory_parameters_keys.copy()

        # This will compute all the ifo.dh[key], for all ifos and all keys
        # It also computes ifo.template_at_point which we use below
        self.compute_h_dh(search_trajectory_parameters_keys)

        for key in search_trajectory_parameters_keys:
            self.log_likelihood_gradient[key] = 0
            for ifo in self.interferometers:
                s_minus_h = ifo.fd_strain - ifo.template_at_point
                self.log_likelihood_gradient[key] += bilby_utils.noise_weighted_inner_product(s_minus_h, ifo.dh[key], ifo.psd_array, ifo.strain_data.duration)

        return self.log_likelihood_gradient

    def compute_h_dh(self, search_trajectory_parameters_keys=None):
        """
        Using the method where dlogL = <s-h|dh>, allowing (for some parameters) to analytically simplify the calculations. Can't work with a likelihood using ROQ basis.
        """

        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()

        waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.likelihood.parameters)
        self.likelihood.waveform_polarizations_at_point = waveform_polarizations
        self.log_likelihood_at_point = 0
        for ifo in self.interferometers:
            ifo.exp_two_pi_f_dt_at_point = ifo.get_fd_time_translation(self.likelihood.parameters)
            ifo.template_at_point = ifo.get_detector_response(waveform_polarizations, self.likelihood.parameters, exp_two_pi_f_dt=ifo.exp_two_pi_f_dt_at_point)
            ifo.dh = {}

        # Compute all ifos.dlogL
        for idx, lalsim_key in enumerate(self.search_parameter_keys):
            if lalsim_key=='theta_jn':
                traj_key = self.search_trajectory_parameters_keys[idx]
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                traj_param = self.search_trajectory_functions[idx](self.likelihood.parameters_at_point[lalsim_key])
                parameters_plus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param + self.offset)
                parameters_minus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param - self.offset)
                # If we are at the boundary where iota = +/- pi, we only do a forward difference in cos(iota)
                if traj_param < -1:
                    self.compute_dh_forward_diff_of_ifos(traj_key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                # If we are at the boundary where iota = 0, we only do a backward difference in cos(iota)
                elif traj_param > 1:
                    self.compute_dh_backward_diff_of_ifos(traj_key, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                # Otherwise we would keep central diff but it forward diff is sufficient anyway for this parameter
                else:
                    self.compute_dh_forward_diff_of_ifos(traj_key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif lalsim_key=='phase':
                traj_key = self.search_trajectory_parameters_keys[idx]
                parameters_plus = self.likelihood.parameters_at_point.copy()
                traj_param = self.search_trajectory_functions[idx](self.likelihood.parameters_at_point[lalsim_key])
                parameters_plus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param + self.offset)
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(traj_key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif lalsim_key=='psi':
                traj_key = self.search_trajectory_parameters_keys[idx]
                parameters_plus = self.likelihood.parameters_at_point.copy()
                traj_param = self.search_trajectory_functions[idx](self.likelihood.parameters_at_point[lalsim_key])
                parameters_plus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param + self.offset)
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(traj_key, parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=False)
            elif lalsim_key=='luminosity_distance':
                traj_key = self.search_trajectory_parameters_keys[idx]
                # Here since h ~ constant/D, dh/dD = -h/D. Hence dh/dlnD = D*dh/dD = -h
                # So we don't use central difference !
                if self.search_trajectory_functions[idx] == np.log:
                    for ifo in self.interferometers:
                        dh = - ifo.template_at_point
                        ifo.dh[traj_key] = dh
                else:
                    parameters_plus = self.likelihood.parameters_at_point.copy()
                    parameters_minus = self.likelihood.parameters_at_point.copy()
                    traj_param = self.search_trajectory_functions[idx](self.likelihood.parameters_at_point[lalsim_key])
                    parameters_plus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param + self.offset)
                    parameters_minus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param - self.offset)
                    self.compute_dh_central_diff_of_ifos(traj_key, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif lalsim_key=='chirp_mass':
                traj_key = self.search_trajectory_parameters_keys[idx]
                if self.search_trajectory_functions[idx] == np.log:
                    parameters_plus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, self.offset)
                    parameters_minus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, -self.offset)
                    # If parameters['symmetric_mass_ratio'] = 0.25: a backward step will give parameters_minus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use forward difference
                    if parameters_minus['symmetric_mass_ratio'] > 0.25:
                        self.compute_dh_forward_diff_of_ifos(traj_key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                    else:
                        self.compute_dh_central_diff_of_ifos(traj_key, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    raise ValueError("The gradient of logL w.r.t. another function than ln(Mc) hasn't been coded yet !")
            elif lalsim_key=='reduced_mass':
                traj_key = self.search_trajectory_parameters_keys[idx]
                if self.search_trajectory_functions[idx] == np.log:
                    parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, self.offset)
                    parameters_minus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -self.offset)
                    # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
                    if parameters_plus['symmetric_mass_ratio'] > 0.25:
                        self.compute_dh_forward_diff_of_ifos(traj_key, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                    else:
                        self.compute_dh_central_diff_of_ifos(traj_key, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    raise ValueError("The gradient of logL w.r.t. another function than ln(mu) hasn't been coded yet !")
            elif lalsim_key=='dec':
                traj_key = self.search_trajectory_parameters_keys[idx]
                parameters_plus = self.likelihood.parameters_at_point.copy()
                traj_param = self.search_trajectory_functions[idx](self.likelihood.parameters_at_point[lalsim_key])
                parameters_plus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param + self.offset)
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(traj_key, parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif lalsim_key=='ra':
                traj_key = self.search_trajectory_parameters_keys[idx]
                parameters_plus = self.likelihood.parameters_at_point.copy()
                traj_param = self.search_trajectory_functions[idx](self.likelihood.parameters_at_point[lalsim_key])
                parameters_plus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param + self.offset)
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(traj_key, parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif lalsim_key=='geocent_time':
                # The following line was added because for BBH signal, an offset = 1e-7 was to small compared to the variations of tc and 1e-5 worked better
                # self.offset = sampler_dict['parameters_offsets'][8]
                traj_key = self.search_trajectory_parameters_keys[idx]
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                # For geocent_time we can't take the derivative w.r.t. the gps time, which is ~1e10 sec, otherwise even with an offset of 1e-8 the gps_plus would be offseted by about 200sec !. Hence derive w.r.t. geocent_duration.
                traj_param = self.search_trajectory_functions[idx](self.likelihood.parameters_at_point[lalsim_key] - self.start_time)
                parameters_plus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param + self.offset) + self.start_time
                parameters_minus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param - self.offset) + self.start_time
                self.compute_dh_central_diff_of_ifos(traj_key, parameters_plus, parameters_minus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
                # self.offset = 1e-7
            # The other parameters handled by this else case are: psi, ra
            else:
                traj_key = self.search_trajectory_parameters_keys[idx]
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                traj_param = self.search_trajectory_functions[idx](self.likelihood.parameters_at_point[lalsim_key])
                parameters_plus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param + self.offset)
                parameters_minus[lalsim_key] = self.search_trajectory_inv_functions[idx](traj_param - self.offset)
                self.compute_dh_central_diff_of_ifos(traj_key, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient


    def compute_h_dh_old(self, search_trajectory_parameters_keys=None):
        """
        Using the method where dlogL = <s-h|dh>, allowing (for some parameters) to analytically simplify the calculations. Can't work with a likelihood using ROQ basis.
        """

        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()

        waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.likelihood.parameters)
        self.likelihood.waveform_polarizations_at_point = waveform_polarizations
        self.log_likelihood_at_point = 0
        for ifo in self.interferometers:
            ifo.exp_two_pi_f_dt_at_point = ifo.get_fd_time_translation(self.likelihood.parameters)
            ifo.template_at_point = ifo.get_detector_response(waveform_polarizations, self.likelihood.parameters, exp_two_pi_f_dt=ifo.exp_two_pi_f_dt_at_point)
            ifo.dh = {}

        # Compute all ifos.dlogL
        for key in search_trajectory_parameters_keys:
            if key=='cos_theta_jn':
                lalsim_key = 'theta_jn'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                cos_theta_jn_plus = np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset
                cos_theta_jn_minus = np.cos(self.likelihood.parameters_at_point[lalsim_key]) - self.offset
                parameters_plus[lalsim_key] = np.arccos(cos_theta_jn_plus)
                parameters_minus[lalsim_key] = np.arccos(cos_theta_jn_minus)
                # If we are at the boundary where iota = +/- pi, we only do a forward difference in cos(iota)
                if cos_theta_jn_minus < -1:
                    self.compute_dh_forward_diff_of_ifos(key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                # If we are at the boundary where iota = 0, we only do a backward difference in cos(iota)
                elif cos_theta_jn_plus > 1:
                    self.compute_dh_backward_diff_of_ifos(key, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                # Otherwise we would keep central diff but it forward diff is sufficient anyway for this parameter
                else:
                    self.compute_dh_forward_diff_of_ifos(key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='phi_c':
                lalsim_key = 'phase'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset / 2
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='phase':
                lalsim_key = 'phase'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='psi':
                lalsim_key = 'psi'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(key, parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=False)
            elif key=='ln(D)':
                lalsim_key = 'luminosity_distance'
                # Here since h ~ constant/D, dh/dD = -h/D. Hence dh/dlnD = D*dh/dD = -h
                # So we don't use central difference !
                for ifo in self.interferometers:
                    dh = - ifo.template_at_point
                    ifo.dh[key] = dh
            elif key=='ln(Mc)':
                parameters_plus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, self.offset)
                parameters_minus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, -self.offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a backward step will give parameters_minus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use forward difference
                if parameters_minus['symmetric_mass_ratio'] > 0.25:
                    self.compute_dh_forward_diff_of_ifos(key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    self.compute_dh_central_diff_of_ifos(key, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='ln(mu)':
                parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, self.offset)
                parameters_minus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -self.offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
                if parameters_plus['symmetric_mass_ratio'] > 0.25:
                    self.compute_dh_forward_diff_of_ifos(key, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    self.compute_dh_central_diff_of_ifos(key, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='sin(dec)':
                lalsim_key = 'dec'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(key, parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif key=='ra':
                lalsim_key = 'ra'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] += self.offset
                # forward diff is sufficient anyway for this parameter
                self.compute_dh_forward_diff_of_ifos(key, parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif key=='ln(tc)':
                # The following line was added because for BBH signal, an offset = 1e-7 was to small compared to the variations of tc and 1e-5 worked better
                # self.offset = sampler_dict['parameters_offsets'][8]
                lalsim_key = 'geocent_time'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) + self.offset) + self.start_time
                parameters_minus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) - self.offset) + self.start_time
                self.compute_dh_central_diff_of_ifos(key, parameters_plus, parameters_minus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)

                # self.offset = 1e-7
            # The other parameters handled by this else case are: psi, ra
            else:
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] += self.offset
                parameters_minus[lalsim_key] -= self.offset
                self.compute_dh_central_diff_of_ifos(key, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True)


        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    def compute_dh_central_diff_of_ifos(self, param_key, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True):
        if compute_waveform_polarizations:
            waveform_polarizations_plus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_plus)
            waveform_polarizations_minus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_minus)
        else:
            waveform_polarizations_plus = self.likelihood.waveform_polarizations_at_point
            waveform_polarizations_minus = self.likelihood.waveform_polarizations_at_point

        for ifo in self.interferometers:
            self.compute_dh_central_diff_of_ifo(param_key, waveform_polarizations_plus, parameters_plus, waveform_polarizations_minus, parameters_minus, ifo, compute_exp_two_pi_f_dt=compute_exp_two_pi_f_dt)

    def compute_dh_forward_diff_of_ifos(self, param_key, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True):
        if compute_waveform_polarizations:
            waveform_polarizations_plus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_plus)
        else:
            waveform_polarizations_plus = self.likelihood.waveform_polarizations_at_point

        for ifo in self.interferometers:
            self.compute_dh_forward_diff_of_ifo(param_key, waveform_polarizations_plus, parameters_plus, ifo, compute_exp_two_pi_f_dt=compute_exp_two_pi_f_dt)

    def compute_dh_backward_diff_of_ifos(self, param_key, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True):
        if compute_waveform_polarizations:
            waveform_polarizations_minus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_minus)
        else:
            waveform_polarizations_minus = self.likelihood.waveform_polarizations_at_point

        for ifo in self.interferometers:
            self.compute_dh_backward_diff_of_ifo(param_key, waveform_polarizations_minus, parameters_minus, ifo, compute_exp_two_pi_f_dt=compute_exp_two_pi_f_dt)

    def compute_dh_central_diff_of_ifo(self, param_key, waveform_polarizations_plus, parameters_plus, waveform_polarizations_minus, parameters_minus, ifo, compute_exp_two_pi_f_dt=True):
        if compute_exp_two_pi_f_dt:
            exp_two_pi_f_dt_plus = None
            exp_two_pi_f_dt_minus = None
        else:
            exp_two_pi_f_dt_plus = ifo.exp_two_pi_f_dt_at_point
            exp_two_pi_f_dt_minus = ifo.exp_two_pi_f_dt_at_point

        template_plus = ifo.get_detector_response(waveform_polarizations_plus, parameters_plus, exp_two_pi_f_dt_plus)
        template_minus = ifo.get_detector_response(waveform_polarizations_minus, parameters_minus, exp_two_pi_f_dt_minus)

        dh = (template_plus - template_minus) / (2 * self.offset)
        ifo.dh[param_key] = dh

        return dh

    def compute_dh_forward_diff_of_ifo(self, param_key, waveform_polarizations_plus, parameters_plus, ifo, compute_exp_two_pi_f_dt=True):
        if compute_exp_two_pi_f_dt:
            exp_two_pi_f_dt_plus = None
        else:
            exp_two_pi_f_dt_plus = ifo.exp_two_pi_f_dt_at_point

        template_plus = ifo.get_detector_response(waveform_polarizations_plus, parameters_plus, exp_two_pi_f_dt_plus)

        dh = (template_plus - ifo.template_at_point) / self.offset
        ifo.dh[param_key] = dh

        return dh

    def compute_dh_backward_diff_of_ifo(self, param_key, waveform_polarizations_minus, parameters_minus, ifo, compute_exp_two_pi_f_dt=True):
        if compute_exp_two_pi_f_dt:
            exp_two_pi_f_dt_minus = None
        else:
            exp_two_pi_f_dt_minus = ifo.exp_two_pi_f_dt_at_point

        template_minus = ifo.get_detector_response(waveform_polarizations_minus, parameters_minus, exp_two_pi_f_dt_minus)

        dh = (ifo.template_at_point - template_minus) / self.offset
        ifo.dh[param_key] = dh

        return dh

    # def get_scales(self):
    #     fisher_matrix = self.get_fisher_matrix()
    #
    #     # If I want to run the hmc on less than 9 dimensions, my 9x9 FIM will be really singular, hence I should remove the rows and columns of the parameters which I keep fixed to get a non-singular sub_FIM which I can invert, otherwise just run the last commented two lines.
    #     # ixgrid = np.ix_(sampler_dict['search_parameter_indices'], sampler_dict['search_parameter_indices'])
    #     # if len(sampler_dict['search_parameter_indices'])<9:
    #     #     sub_FIM = fisher_matrix[ixgrid]
    #     #     inverse_sub_FIM = np.linalg.inv(sub_FIM)
    #     #     sub_scale = np.sqrt(inverse_sub_FIM.diagonal())
    #     #     scale = np.zeros(9)
    #     #     scale[sampler_dict['search_parameter_indices']] = sub_scale
    #     # else:
    #     inverse_fisher_matrix = np.linalg.inv(fisher_matrix)
    #     scale = np.sqrt(inverse_fisher_matrix.diagonal())
    #     # classic decomp
    #     ###################
    #
    #     ###################
    #     # SVD decomp of FIM
    #     # if len(sampler_dict['search_parameter_indices'])<9:
    #     #     u, s, vh = np.linalg.svd(sub_FIM)
    #     #     inverse_sub_FIM_SVD = vh.T * 1/s @ u.T
    #     #     sub_scale_SVD = np.sqrt(inverse_sub_FIM_SVD.diagonal())
    #     #     scale_SVD = np.zeros(9)
    #     #     scale_SVD[sampler_dict['search_parameter_indices']] = sub_scale_SVD
    #     # else:
    #     if False:
    #         # u * s @ vh = u @ np.diag(s) @ vh = FisherMatrix
    #         u, s, vh = np.linalg.svd(fisher_matrix)
    #         inverse_fisher_matrix_svd = vh.T * 1/s @ u.T
    #         scale_svd = np.sqrt(inverse_fisher_matrix_svd.diagonal())
    #     # SVD decomp of FIM
    #     ###################
    #
    #     ###################
    #     # LU decomp of FIM
    #     # if len(sampler_dict['search_parameter_indices'])<9:
    #     #     p, l, u = lu(sub_FIM)
    #     #     inverse_sub_FIM_LU = np.linalg.inv(u) @ np.linalg.inv(l) @ np.linalg.inv(p)
    #     #     sub_scale_LU = np.sqrt(inverse_sub_FIM_LU.diagonal())
    #     #     scale_LU = np.zeros(9)
    #     #     scale_LU[sampler_dict['search_parameter_indices']] = sub_scale_LU
    #     # else:
    #     if False:
    #         from scipy.linalg import lu
    #         p, l, u = lu(fisher_matrix)
    #         inverse_fisher_matrix_lud = np.linalg.inv(u) @ np.linalg.inv(l) @ np.linalg.inv(p)
    #         scale_lu = np.sqrt(inverse_fisher_matrix_lud.diagonal())
    #     # LU decomp of FIM
    #     ###################
    #
    #     # Store the scales in a dictionnary. It should latter be attached
    #     # to the `Sampler` object rather than the likelihoood_gradient.
    #     self.scales = {}
    #     for i, key in enumerate(self.search_parameter_keys):
    #         self.scales[key] = scale[i]
    #
    #     return scale

    def get_fisher_matrix(self):

        # If the likelihood uses ROQ basis, then its waveform_generator
        # does not really generate a waveform but the linear and quadratic
        # components sufficient to compute the log_likelihood().
        # However here for the FIM we need to compute the derivatives
        # of the waveforms. Hence we temporarily switch the roq waveform
        # generator source model to its non-roq counterpart and switch
        # it back at the end.
        if self.uses_roq:
            import bilby.gw.source as source
            roq_fd_source_model = self.likelihood.waveform_generator.frequency_domain_source_model
            if 'neutron_star' in roq_fd_source_model.__name__:
                non_roq_fd_source_model = source.lal_binary_neutron_star
            elif 'black_hole' in roq_fd_source_model.__name__:
                non_roq_fd_source_model = source.lal_binary_black_hole
            else:
                raise ValueError(f'Cannot associate a non roq frequency domain source model to the roq one: {roq_fd_source_model_name} in order to compute the fisher matrix.')
            self.likelihood.waveform_generator.frequency_domain_source_model = non_roq_fd_source_model

        self.calculate_dlogL_s_minus_h_dh(self.search_trajectory_parameters_keys)

        fisher_matrix = np.zeros((self.n_dim, self.n_dim))
        for i, key_row in enumerate(self.search_trajectory_parameters_keys):
            for j, key_column in enumerate(self.search_trajectory_parameters_keys):
                for ifo in self.interferometers:
                    fisher_matrix[i][j] += bilby_utils.noise_weighted_inner_product(ifo.dh[key_row], ifo.dh[key_column], ifo.psd_array, ifo.strain_data.duration)

        if self.uses_roq:
            self.likelihood.waveform_generator.frequency_domain_source_model = roq_fd_source_model

        return fisher_matrix

    ##### OLD FUNCTIONS ######
    def calculate_dlogL_s_minus_h_dh_old(self, search_parameter_keys=None):
        """
        Using the method where dlogL = <s-h|dh>, allowing (for some parameters) to analytically simplify the calculations. Can't work with a likelihood using ROQ basis.
        """
        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()

        waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.likelihood.parameters)
        self.likelihood.waveform_polarizations_at_point = waveform_polarizations
        self.log_likelihood_at_point = 0
        for ifo in self.interferometers:
            ifo.exp_two_pi_f_dt_at_point = ifo.get_fd_time_translation(self.likelihood.parameters)
            ifo.template_at_point = ifo.get_detector_response(waveform_polarizations, self.likelihood.parameters, exp_two_pi_f_dt=ifo.exp_two_pi_f_dt_at_point)
            ifo.s_minus_h = ifo.fd_strain - ifo.template_at_point
            s_inner_h = bilby_utils.noise_weighted_inner_product(ifo.fd_strain, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration)
            h_inner_h = bilby_utils.noise_weighted_inner_product(ifo.template_at_point, ifo.template_at_point, ifo.psd_array, ifo.strain_data.duration)
            self.log_likelihood_at_point += s_inner_h - 0.5 * h_inner_h

        # By default we want to compute the numerical gradient over all the searched parameters.
        # For hybrid trajectories only a subset of the parameter space will be numerically computed.
        if search_parameter_keys==None:
            search_parameter_keys = self.search_parameter_keys.copy()

        # Compute all ifos.dlogL
        for key in search_parameter_keys:
            if key=='cos_theta_jn':
                lalsim_key = 'theta_jn'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                cos_theta_jn_plus = np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset
                cos_theta_jn_minus = np.cos(self.likelihood.parameters_at_point[lalsim_key]) - self.offset
                parameters_plus[lalsim_key] = np.arccos(cos_theta_jn_plus)
                parameters_minus[lalsim_key] = np.arccos(cos_theta_jn_minus)
                # If we are at the boundary where iota = +/- pi, we only do a forward difference in cos(iota)
                if cos_theta_jn_minus < -1:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                # If we are at the boundary where iota = 0, we only do a backward difference in cos(iota)
                elif cos_theta_jn_plus > 1:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_backward_diff(parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                # Otherwise we would keep central diff but it forward diff is sufficient anyway for this parameter
                else:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='phi_c':
                lalsim_key = 'phase'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset / 2
                # forward diff is sufficient anyway for this parameter
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='phase':
                lalsim_key = 'phase'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                # forward diff is sufficient anyway for this parameter
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='psi':
                lalsim_key = 'psi'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                # forward diff is sufficient anyway for this parameter
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=False)
            elif key=='ln(D)':
                lalsim_key = 'luminosity_distance'
                # Here since h ~ constant/D, dh/dD = -h/D. Hence dh/dlnD = D*dh/dD = -h
                # So we don't use central difference !
                self.log_likelihood_gradient[key] = 0
                for ifo in self.interferometers:
                    dh = - ifo.template_at_point
                    dlogL = bilby_utils.noise_weighted_inner_product(ifo.s_minus_h, dh, ifo.psd_array, ifo.strain_data.duration)
                    self.log_likelihood_gradient[key] += dlogL
            elif key=='ln(Mc)':
                parameters_plus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, self.offset)
                parameters_minus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, -self.offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a backward step will give parameters_minus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use forward difference
                if parameters_minus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='ln(mu)':
                parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, self.offset)
                parameters_minus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -self.offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
                if parameters_plus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_backward_diff(parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='sin(dec)':
                lalsim_key = 'dec'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                # forward diff is sufficient anyway for this parameter
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif key=='ra':
                lalsim_key = 'ra'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] += self.offset
                # forward diff is sufficient anyway for this parameter
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif key=='ln(tc)':
                # The following line was added because for BBH signal, an offset = 1e-7 was to small compared to the variations of tc and 1e-5 worked better
                # self.offset = sampler_dict['parameters_offsets'][8]
                lalsim_key = 'geocent_time'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) + self.offset) + self.start_time
                parameters_minus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) - self.offset) + self.start_time
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)

                # self.offset = 1e-7
            # The other parameters handled by this else case are: psi, ra
            else:
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] += self.offset
                parameters_minus[lalsim_key] -= self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True)


        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    def compute_dlogL_central_diff(self, parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True):
        if compute_waveform_polarizations:
            waveform_polarizations_plus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_plus)
            waveform_polarizations_minus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_minus)
        else:
            waveform_polarizations_plus = self.likelihood.waveform_polarizations_at_point
            waveform_polarizations_minus = self.likelihood.waveform_polarizations_at_point
        dlogL_total = 0
        for ifo in self.interferometers:
            if compute_exp_two_pi_f_dt:
                exp_two_pi_f_dt_plus = None
                exp_two_pi_f_dt_minus = None
            else:
                exp_two_pi_f_dt_plus = ifo.exp_two_pi_f_dt_at_point
                exp_two_pi_f_dt_minus = ifo.exp_two_pi_f_dt_at_point
            template_plus = ifo.get_detector_response(waveform_polarizations_plus, parameters_plus, exp_two_pi_f_dt_plus)
            template_minus = ifo.get_detector_response(waveform_polarizations_minus, parameters_minus, exp_two_pi_f_dt_minus)
            dh = (template_plus - template_minus) / (2 * self.offset)
            dlogL = bilby_utils.noise_weighted_inner_product(ifo.s_minus_h, dh, ifo.psd_array, ifo.strain_data.duration)
            dlogL_total += dlogL
        return dlogL_total

    def compute_dlogL_forward_diff(self, parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True):
        if compute_waveform_polarizations:
            waveform_polarizations_plus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_plus)
        else:
            waveform_polarizations_plus = self.likelihood.waveform_polarizations_at_point
        dlogL_total = 0
        for ifo in self.interferometers:
            if compute_exp_two_pi_f_dt:
                exp_two_pi_f_dt_plus = ifo.get_fd_time_translation(parameters_plus)
            else:
                exp_two_pi_f_dt_plus = ifo.exp_two_pi_f_dt_at_point
            template_plus = ifo.get_detector_response(waveform_polarizations_plus, parameters_plus, exp_two_pi_f_dt_plus)
            dh = (template_plus - ifo.template_at_point) / self.offset
            dlogL = bilby_utils.noise_weighted_inner_product(ifo.s_minus_h, dh, ifo.psd_array, ifo.strain_data.duration)
            dlogL_total += dlogL
        return dlogL_total

    def compute_dlogL_backward_diff(self, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True):
        if compute_waveform_polarizations:
            waveform_polarizations_minus = self.likelihood.waveform_generator.frequency_domain_strain(parameters_minus)
        else:
            waveform_polarizations_minus = self.likelihood.waveform_polarizations_at_point
        dlogL_total = 0
        for ifo in self.interferometers:
            if compute_exp_two_pi_f_dt:
                exp_two_pi_f_dt_minus = ifo.get_fd_time_translation(parameters_minus)
            else:
                exp_two_pi_f_dt_minus = ifo.exp_two_pi_f_dt_at_point
            template_minus = ifo.get_detector_response(waveform_polarizations_minus, parameters_minus, exp_two_pi_f_dt_minus)
            dh = (ifo.template_at_point - template_minus) / self.offset
            dlogL = bilby_utils.noise_weighted_inner_product(ifo.s_minus_h, dh, ifo.psd_array, ifo.strain_data.duration)
            dlogL_total += dlogL
        return dlogL_total





    def convert_dlogL_dict_to_np_array(self, dlogL_dict):

        dict_keys = dlogL_dict.keys()
        dlogL_np = np.zeros(len(dict_keys))
        for i, key in enumerate(dict_keys):
            if key in dict_keys:
                dlogL_np[i] = dlogL_dict[key]

        return dlogL_np

    def calculate_dlogL_np(self, dlogL_dict=None, search_trajectory_parameters_keys=None):
        if dlogL_dict is None:
            if search_trajectory_parameters_keys is None:
                search_trajectory_parameters_keys = self.search_trajectory_parameters_keys
            dlogL_dict = self.calculate_log_likelihood_gradient(search_trajectory_parameters_keys=search_trajectory_parameters_keys)

        return self.convert_dlogL_dict_to_np_array(dlogL_dict)

class GWTransientLikelihoodGradient_central(GWTransientLikelihoodGradient):
    """
    Central difference for all parameters
    """
    def __init__(self, likelihood, search_parameter_keys, offset=1e-7):
        GWTransientLikelihoodGradient.__init__(self, likelihood, search_parameter_keys, offset)
        self.start_time = likelihood.interferometers[0].strain_data.start_time
        self.type = 'central'

    @property
    def interferometers(self):
        return self.likelihood.interferometers

    def calculate_dlogL_logL_diff(self, search_parameter_keys=None):
        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()
        self.log_likelihood_at_point = self.likelihood.log_likelihood_ratio()

        # By default we want to compute the numerical gradient over all the searched parameters.
        # For hybrid trajectories only a subset of the parameter space will be numerically computed.
        if search_parameter_keys==None:
            search_parameter_keys = self.search_parameter_keys.copy()
        for key in search_parameter_keys:
            if key=='cos_theta_jn':
                lalsim_key = 'theta_jn'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.arccos(np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[lalsim_key] = np.arccos(np.cos(self.likelihood.parameters_at_point[lalsim_key]) - self.offset)
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='phi_c':
                lalsim_key = 'phase'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset / 2
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] - self.offset / 2
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='phase':
                lalsim_key = 'phase'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] - self.offset
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='psi':
                lalsim_key = 'psi'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] - self.offset
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[lalsim_key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='ln(D)':
                lalsim_key = 'luminosity_distance'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key]) - self.offset)
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='ln(Mc)':
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                parameters_minus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, -self.offset)
                if parameters_minus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
                else:
                    self.likelihood.parameters = parameters_minus
                    log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='ln(mu)':
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -self.offset)
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, self.offset)
                if parameters_plus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = (self.log_likelihood_at_point - log_likelihood_minus) / self.offset
                else:
                    self.likelihood.parameters = parameters_plus
                    log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='sin(dec)':
                lalsim_key = 'dec'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) - self.offset)
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='ra':
                lalsim_key = 'ra'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] - self.offset
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[lalsim_key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            elif key=='ln(tc)':
                lalsim_key = 'geocent_time'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) + self.offset) + self.start_time
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) - self.offset) + self.start_time
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)
            else:
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] - self.offset
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - log_likelihood_minus) / (2 * self.offset)

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    def calculate_dlogL_s_minus_h_dh(self, search_parameter_keys=None):
        """
        Using the method where dlogL = <s-h|dh>, allowing (for some parameters) to analytically simplify the calculations. Can't work with a likelihood using ROQ basis.
        """

        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()

        waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.likelihood.parameters)
        self.likelihood.waveform_polarizations_at_point = waveform_polarizations
        for ifo in self.interferometers:
            ifo.exp_two_pi_f_dt_at_point = ifo.get_fd_time_translation(self.likelihood.parameters)
            ifo.template_at_point = ifo.get_detector_response(waveform_polarizations, self.likelihood.parameters, exp_two_pi_f_dt=ifo.exp_two_pi_f_dt_at_point)
            ifo.s_minus_h = ifo.fd_strain - ifo.template_at_point

        # By default we want to compute the numerical gradient over all the searched parameters.
        # For hybrid trajectories only a subset of the parameter space will be numerically computed.
        if search_parameter_keys==None:
            search_parameter_keys = self.search_parameter_keys.copy()

        # Compute all ifos.dlogL
        for key in search_parameter_keys:
            if key=='cos_theta_jn':
                lalsim_key = 'theta_jn'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                cos_theta_jn_plus = np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset
                cos_theta_jn_minus = np.cos(self.likelihood.parameters_at_point[lalsim_key]) - self.offset
                parameters_plus[lalsim_key] = np.arccos(cos_theta_jn_plus)
                parameters_minus[lalsim_key] = np.arccos(cos_theta_jn_minus)
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='phi_c':
                lalsim_key = 'phase'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset / 2
                parameters_minus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] - self.offset / 2
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='phase':
                lalsim_key = 'phase'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                parameters_minus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] - self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='psi':
                lalsim_key = 'psi'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                parameters_minus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] - self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=False)
            elif key=='ln(D)':
                lalsim_key = 'luminosity_distance'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_minus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key]) - self.offset)
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='ln(Mc)':
                parameters_plus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, self.offset)
                parameters_minus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, -self.offset)
                if parameters_minus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='ln(mu)':
                parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, self.offset)
                parameters_minus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -self.offset)
                if parameters_plus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_backward_diff(parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='sin(dec)':
                lalsim_key = 'dec'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                parameters_minus[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) - self.offset)
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif key=='ra':
                lalsim_key = 'ra'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] += self.offset
                parameters_minus[lalsim_key] -= self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif key=='ln(tc)':
                lalsim_key = 'geocent_time'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) + self.offset) + self.start_time
                parameters_minus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) - self.offset) + self.start_time
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            else:
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_minus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] += self.offset
                parameters_minus[lalsim_key] -= self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_central_diff(parameters_plus, parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True)

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

class GWTransientLikelihoodGradient_forward(GWTransientLikelihoodGradient):
    """
    Forward difference for all parameters
    """
    def __init__(self, likelihood, search_parameter_keys, offset=1e-7):
        GWTransientLikelihoodGradient.__init__(self, likelihood, search_parameter_keys, offset)
        self.start_time = likelihood.interferometers[0].strain_data.start_time
        self.type = 'forward'

    @property
    def interferometers(self):
        return self.likelihood.interferometers

    def calculate_dlogL_logL_diff(self, search_parameter_keys=None):
        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()
        self.log_likelihood_at_point = self.likelihood.log_likelihood_ratio()

        # By default we want to compute the numerical gradient over all the searched parameters.
        # For hybrid trajectories only a subset of the parameter space will be numerically computed.
        if search_parameter_keys==None:
            search_parameter_keys = self.search_parameter_keys.copy()
        for key in search_parameter_keys:
            if key=='cos_theta_jn':
                lalsim_key = 'theta_jn'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.arccos(np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='phi_c':
                lalsim_key = 'phase'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset / 2
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='phase':
                lalsim_key = 'phase'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='psi':
                lalsim_key = 'psi'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[lalsim_key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ln(D)':
                lalsim_key = 'luminosity_distance'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ln(Mc)':
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ln(mu)':
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -self.offset)
                log_likelihood_minus = self.likelihood.log_likelihood_ratio()
                parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, self.offset)
                if parameters_plus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = (self.log_likelihood_at_point - log_likelihood_minus) / self.offset
                else:
                    self.likelihood.parameters = parameters_plus
                    log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                    self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='sin(dec)':
                lalsim_key = 'dec'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ra':
                lalsim_key = 'ra'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                # forward is sufficient for this parameter
                self.log_likelihood_gradient[lalsim_key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            elif key=='ln(tc)':
                lalsim_key = 'geocent_time'
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) + self.offset) + self.start_time
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset
            else:
                # Reset the parameters to their original value
                self.likelihood.parameters = self.likelihood.parameters_at_point.copy()
                self.likelihood.parameters[key] = self.likelihood.parameters_at_point[key] + self.offset
                log_likelihood_plus = self.likelihood.log_likelihood_ratio()
                self.log_likelihood_gradient[key] = (log_likelihood_plus - self.log_likelihood_at_point) / self.offset

        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient

    def calculate_dlogL_s_minus_h_dh(self, search_parameter_keys=None):
        """
        Using the method where dlogL = <s-h|dh>, allowing (for some parameters) to analytically simplify the calculations. Can't work with a likelihood using ROQ basis.
        """

        self.likelihood.parameters_at_point = self.likelihood.parameters.copy()

        waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.likelihood.parameters)
        self.likelihood.waveform_polarizations_at_point = waveform_polarizations
        for ifo in self.interferometers:
            ifo.exp_two_pi_f_dt_at_point = ifo.get_fd_time_translation(self.likelihood.parameters)
            ifo.template_at_point = ifo.get_detector_response(waveform_polarizations, self.likelihood.parameters, exp_two_pi_f_dt=ifo.exp_two_pi_f_dt_at_point)
            ifo.s_minus_h = ifo.fd_strain - ifo.template_at_point

        # By default we want to compute the numerical gradient over all the searched parameters.
        # For hybrid trajectories only a subset of the parameter space will be numerically computed.
        if search_parameter_keys==None:
            search_parameter_keys = self.search_parameter_keys.copy()

        # Compute all ifos.dlogL
        for key in search_parameter_keys:
            if key=='cos_theta_jn':
                lalsim_key = 'theta_jn'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                cos_theta_jn_plus = np.cos(self.likelihood.parameters_at_point[lalsim_key]) + self.offset
                parameters_plus[lalsim_key] = np.arccos(cos_theta_jn_plus)
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='phi_c':
                lalsim_key = 'phase'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset / 2
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='phase':
                lalsim_key = 'phase'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='psi':
                lalsim_key = 'psi'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = self.likelihood.parameters_at_point[lalsim_key] + self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=False)
            elif key=='ln(D)':
                lalsim_key = 'luminosity_distance'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='ln(Mc)':
                parameters_plus = paru.parameters_new_dlnMc(self.likelihood.parameters_at_point, self.offset)
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='ln(mu)':
                parameters_plus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, self.offset)
                parameters_minus = paru.parameters_new_dlnmu(self.likelihood.parameters_at_point, -self.offset)
                # If parameters['symmetric_mass_ratio'] = 0.25: a forward step will give parameters_plus['symmetric_mass_ratio'] > 0.25 which is unphysical, hence we only use backward difference
                if parameters_plus['symmetric_mass_ratio'] > 0.25:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_backward_diff(parameters_minus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
                else:
                    self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=False)
            elif key=='sin(dec)':
                lalsim_key = 'dec'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.arcsin(np.sin(self.likelihood.parameters_at_point[lalsim_key]) + self.offset)
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif key=='ra':
                lalsim_key = 'ra'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] += self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            elif key=='ln(tc)':
                lalsim_key = 'geocent_time'
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] = np.exp(np.log(self.likelihood.parameters_at_point[lalsim_key] - self.start_time) + self.offset) + self.start_time
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=False, compute_exp_two_pi_f_dt=True)
            else:
                parameters_plus = self.likelihood.parameters_at_point.copy()
                parameters_plus[lalsim_key] += self.offset
                self.log_likelihood_gradient[key] = self.compute_dlogL_forward_diff(parameters_plus, compute_waveform_polarizations=True, compute_exp_two_pi_f_dt=True)


        self.likelihood.parameters = self.likelihood.parameters_at_point.copy()

        return self.log_likelihood_gradient
