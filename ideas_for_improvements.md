# Ideas to improve the algorithm on different aspects

## After reading more carefully Aurélien Géron's book on ML

- Use a validation set to fine tune the DNN's hyperparameters (eg nb of layers, neuron per layer etc) using RandomSearch for instance, and keep the test set to properly measure the generalization afterwards.
- Instead of standardizing the inputs with Scikit-Learn's `StandardScaler`, it appears that DNN tend to prefer a min-max scaling which can be performed with Scikit-Learn's `MinMaxScaler`.
- For activation functions, switch from ReLu to SELU (or ELU if we now use the `MinMaxScaler`) or leaky ReLU if SELU/ELU are too slow; see Aurélien Géron p337 and 338.
- Adding polynomial features to the training set, see chapter 5 p158 of Aurélien Géron.
- Use better Search algorithms libraries than RandomSearch such as the ones listed p322 of Aurélien Géron's: Hyperopt, Hyperas etc.
- Perform a coarse Search of the DNN hyperparameters, and then a more refined Search close to the best values.
- Following Aurélien Géron's advice: "In general you will get more bang for your buck by increasing the number of layers instead of the number of neurons per layer.", we should try a DNN with 4, 5 maybe 6 layers with each fewer neurons.
- During training of the DNN, implement `EarlyStopping` callback (cf Chap 10 of Aurélien Géron's, p315): "it will interrupt training when it measures no progress on the validation set for a number of epochs (defined by the `patience` argument), and it will optionnally roll back to the best model."
