import logging
import os


logger = logging.getLogger('gwhmc')

def check_directory_exists_and_if_not_mkdir(directory):
    """ Checks if the given directory exists and creates it if it does not exist

    Parameters
    ----------
    directory: str
        Name of the directory

    """
    if not os.path.exists(directory):
        os.makedirs(directory)
        logger.debug('Making directory {}'.format(directory))
    else:
        logger.debug('Directory {} exists'.format(directory))

def setup_logger(logger, outdir=None, label=None, log_level='INFO'):
    """ Setup logging output: call at the start of the script to use

    Parameters
    ----------
    outdir, label: str
        If supplied, write the logging output to outdir/label.log
    log_level: str, optional
        ['debug', 'info', 'warning']
        Either a string from the list above, or an integer as specified
        in https://docs.python.org/2/library/logging.html#logging-levels
    print_version: bool
        If true, print version information
    """

    if type(log_level) is str:
        try:
            level = getattr(logging, log_level.upper())
        except AttributeError:
            raise ValueError('log_level {} not understood'.format(log_level))
    else:
        level = int(log_level)

    # logger = logging.getLogger('gwhmc')
    logger.propagate = False
    logger.setLevel(level)

    if any([type(h) == logging.StreamHandler for h in logger.handlers]) is False:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(name)s %(levelname)-8s: %(message)s', datefmt='%H:%M'))
        stream_handler.setLevel(level)
        logger.addHandler(stream_handler)

    if any([type(h) == logging.FileHandler for h in logger.handlers]) is False:
        if label:
            if outdir:
                check_directory_exists_and_if_not_mkdir(outdir)
            else:
                outdir = '.'
            if outdir[-1] == '/':
                outdir = outdir[:-1]
            log_file = '{}/{}.log'.format(outdir, label)
            file_handler = logging.FileHandler(log_file)
            file_handler.setFormatter(logging.Formatter(
                '%(asctime)s %(name)s %(levelname)-8s: %(message)s', datefmt='%H:%M'))

            file_handler.setLevel(level)
            logger.addHandler(file_handler)

    for handler in logger.handlers:
        handler.setLevel(level)

def dictionary_to_formatted_string(dictionary, indent=3, align_keys=True, decimal_format=None):
    """
    Formats a dictionary such that the outputted string will be printed with the nested structure.

    Parameters
    ----------
    indent: int, default=3
        Number of spaces used to indent the nested dictionaries
    align_keys: boolean, default=True
        Aligns vertically the printed values
    decimal_format: int, default=None
        Number of decimals to print for numbers in the dictionary using the format {:.xf} where x is `decimal_format`. If left to None not format will be applied.

    Returns
    -------
    string_to_print: string
        String to be printed with line breaks that follow the nested structure of the input dictionary.
    """
    string_to_print = ''
    indent_align_key_str = ''
    # We need to know the biggest length of all keys in order to align the prints
    if align_keys:
        max_length_key = 0
        for key in dictionary.keys():
            max_length_key = max(max_length_key, len(key))

    # Multiply a space string by the number of indent wished to get the string which will be appended before printing each key
    indent_str = ' ' * indent

    # Now iterate over the dictionary
    for key, value in dictionary.items():
        if align_keys:
            indent_align_key = max(max_length_key - len(key), 0)
            indent_align_key_str = ' ' * indent_align_key
        # If the key contains itself a dictionary, we recursively call this `dictionary_to_formatted_string()` function but multiplying the indent by 2 such that the nested structure appears well on the print
        if type(value) == dict:
            string_to_print += '\n' + indent_str + key + indent_align_key_str + ':'
            string_to_print += dictionary_to_formatted_string(value, indent*2, align_keys, decimal_format)
        else:
            if isinstance(value, (int, float, complex)) and not isinstance(value, bool) and decimal_format is not None:
                value_str = '{:.{}f}'.format(value, decimal_format)
            else:
                value_str = f'{value}'
            string_to_print += '\n' + indent_str + key + indent_align_key_str + ': ' + value_str

    return string_to_print




import re
import ast

def strip_quotes(string):
    """
    Copy of function in bilby_pipe: https://git.ligo.org/marc.arene/bilby_pipe/-/blob/master/bilby_pipe/utils.py
    """
    try:
        return string.replace('"', "").replace("'", "")
    except AttributeError:
        return string

def string_to_int_float(s):
    """
    Copy of function in bilby_pipe: https://git.ligo.org/marc.arene/bilby_pipe/-/blob/master/bilby_pipe/utils.py
    """
    try:
        return int(s)
    except ValueError:
        try:
            return float(s)
        except ValueError:
            return s


def convert_string_to_dict(string, key=None):
    """
    Copy of function in bilby_pipe: https://git.ligo.org/marc.arene/bilby_pipe/-/blob/master/bilby_pipe/utils.py
    Convert a string repr of a string to a python dictionary

    Parameters
    ----------
    string: str
        The strng to convert
    key: str (None)
        A key, used for debugging
    """

    string = strip_quotes(string)
    # Convert equals to colons
    string = string.replace("=", ":")
    # Force double quotes around everything
    string = re.sub('(\w+)\s?:\s?("?[^,"}]+"?)', '"\g<1>":"\g<2>"', string)  # noqa
    # Evaluate as a dictionary of str: str
    try:
        dic = ast.literal_eval(string)
    except ValueError as e:
        if key is not None:
            raise ValueError(
                "Error {}. Unable to parse {}: {}".format(e, key, string)
            )
        else:
            raise ValueError("Error {}. Unable to parse {}".format(e, string))

    # Convert values to bool/floats/ints where possible
    for key in dic:
        if dic[key].lower() == "true":
            dic[key] = True
        elif dic[key].lower() == "false":
            dic[key] = False
        else:
            dic[key] = string_to_int_float(dic[key])

    return dic


setup_logger(logger, log_level='INFO')


if __name__ == '__main__':

    setup_logger(log_level='INFO')

    logger.info("Testing the info method")
    logger.warning("Testing the warning method")

    simple_dict = {'tutu': 999}
    nested_dict = {'toto': 123, 'person': {'firstName': "Marc", 'lastName': "Arène"}}
    formatted_sdict = dictionary_to_formatted_string(simple_dict)
    formatted_ndict = dictionary_to_formatted_string(nested_dict)
    import IPython; IPython.embed(); sys.exit()
    logger.warning(f"Printing nested dict: {formatted_ndict}")
