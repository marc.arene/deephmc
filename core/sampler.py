import numpy as np
import sys
sys.path.append('../')
import matplotlib.pyplot as plt
import os
import pandas as pd

import Library.param_utils as paru
import Library.CONST as CONST
import Library.python_utils as pu
import Library.Fit_NR as fnr

import core.utils as cut
import core.autocorrelation as cautocorr
import gw.result as gwresult
import gw.prior as gwprior

class GWHMCSampler(object):
    def __init__(self, starting_point_parameters, outdir, likelihood_gradient, traj_prior_gradients, search_parameter_keys_local_fit, n_traj_hmc_tot, n_traj_fit,  n_traj_for_this_run, n_fit_1, n_fit_2, length_num_traj, epsilon0, n_sis, priors_for_reweight=None):

        self.starting_point_parameters = starting_point_parameters
        self.outdir = outdir
        self.likelihood_gradient = likelihood_gradient
        self.traj_prior_gradients = traj_prior_gradients
        # self.dict_parameters_boundaries = self.get_parameters_boundaries_from_default(parameters_boundaries)
        # self.trajectory_parameters_boundaries = self.get_trajectory_parameters_boundaries()
        # self.search_parameter_indices = search_parameter_indices
        self.search_parameter_keys_local_fit = search_parameter_keys_local_fit
        self.search_parameter_keys_global_fit = self.get_search_parameter_keys_global_fit()
        self.search_parameter_keys_num_in_hybrid = self.search_parameter_keys_local_fit.copy()
        self.search_parameter_indices_local_fit = self.get_search_parameter_indices_local_fit()
        self.search_parameter_indices_global_fit = self.get_search_parameter_indices_global_fit()
        # self.dict_search_parameter_indices = self.get_dict_search_parameter_indices()
        self.n_traj_hmc_tot = n_traj_hmc_tot
        self.n_traj_fit = n_traj_fit
        self.n_traj_for_this_run = n_traj_for_this_run
        self.n_fit_1 = n_fit_1
        self.n_fit_2 = n_fit_2
        self.length_num_traj = length_num_traj
        self.epsilon0 = epsilon0
        self.n_sis = n_sis
        self.priors_for_reweight = priors_for_reweight
        self.samples = []
        self.acc_rates = []
        self.chain_metrics = []
        self.print_out_boundaries()

    @property
    def search_parameter_keys(self):
        return self.likelihood_gradient.search_parameter_keys

    @property
    def search_trajectory_functions(self):
        return self.likelihood_gradient.search_trajectory_functions

    @property
    def search_trajectory_inv_functions(self):
        return self.likelihood_gradient.search_trajectory_inv_functions

    @property
    def search_trajectory_parameters_keys(self):
        return self.likelihood_gradient.search_trajectory_parameters_keys

    @property
    def search_keys_and_traj_keys(self):
        return list(zip(self.search_parameter_keys, self.search_trajectory_parameters_keys))

    @property
    def dict_trajectory_parameters_keys(self):
        return self.likelihood_gradient.dict_trajectory_parameters_keys

    @property
    def n_dim(self):
        return len(self.search_parameter_keys)

    @property
    def likelihood(self):
        return self.likelihood_gradient.likelihood

    @property
    def start_time(self):
        return self.likelihood_gradient.likelihood.interferometers[0].strain_data.start_time

    @property
    def scales_dict(self):
        if self.scales is None:
            self.set_scales_from_fisher_matrix()
        return self.trajectory_parameters_array_to_dict(self.scales)

    @property
    def approximant(self):
        return self.likelihood.waveform_generator.waveform_arguments['waveform_approximant']

    @property
    def traj_priors(self):
        return self.traj_prior_gradients.traj_priors

    @property
    def priors(self):
        return self.traj_prior_gradients.traj_priors.priors

    def get_search_parameter_keys_global_fit(self):
        search_parameter_keys_global_fit = self.search_parameter_keys.copy()
        for key in self.search_parameter_keys_local_fit:
            search_parameter_keys_global_fit.remove(key)
        return search_parameter_keys_global_fit

    def get_search_parameter_indices_local_fit(self):
        search_parameter_indices_local_fit = []
        for key in self.search_parameter_keys_local_fit:
            search_parameter_indices_local_fit.append(self.search_parameter_keys.index(key))
        return search_parameter_indices_local_fit

    def get_search_parameter_indices_global_fit(self):
        search_parameter_indices_global_fit = []
        for key in self.search_parameter_keys_global_fit:
            search_parameter_indices_global_fit.append(self.search_parameter_keys.index(key))
        return search_parameter_indices_global_fit

    def get_df_of_boundaries_from_priors(self, priors, keys):
        minima = [priors[key].minimum for key in keys]
        maxima = [priors[key].maximum for key in keys]
        df_bounds = pd.DataFrame([minima, maxima], columns=keys, index=['min', 'max'])
        return df_bounds
        cut.logger.info(f'Boundaries set as follow:\n {df_bounds.T}')

    def print_out_boundaries(self):
        df_param_bounds = self.get_df_of_boundaries_from_priors(self.priors, self.search_parameter_keys)
        df_traj_param_bounds = self.get_df_of_boundaries_from_priors(self.traj_priors, self.search_trajectory_parameters_keys)
        cut.logger.info(f'Boundaries set as follow:\n{df_param_bounds.T}\n\n{df_traj_param_bounds.T}')

    def trajectory_parameters_array_to_dict(self, array):
        dict = {}
        for i, key in enumerate(self.search_trajectory_parameters_keys):
            dict[key] = array[i]
        return dict

    def get_scales_from_df_fisher_matrix(self, df_fisher_matrix):

        columns = df_fisher_matrix.index.tolist()
        ###################
        # classic decomp
        if False:
            inverse_fisher_matrix = np.linalg.inv(df_fisher_matrix)
            scales = np.sqrt(inverse_fisher_matrix.diagonal())
        # classic decomp
        ###################

        ###################
        # SVD decomp of FIM
        if True:
            # u * s @ vh = u @ np.diag(s) @ vh = FisherMatrix
            u, s, vh = np.linalg.svd(df_fisher_matrix)
            inverse_fisher_matrix_svd = vh.T * 1/s @ u.T
            scales = np.sqrt(inverse_fisher_matrix_svd.diagonal())
        # SVD decomp of FIM
        ###################

        ###################
        # LU decomp of FIM
        if False:
            from scipy.linalg import lu
            p, l, u = lu(df_fisher_matrix)
            inverse_fisher_matrix_lud = np.linalg.inv(u) @ np.linalg.inv(l) @ np.linalg.inv(p)
            scales = np.sqrt(inverse_fisher_matrix_lud.diagonal())
        # LU decomp of FIM
        ###################

        ###################
        # GW150914 setting...
        # Use sub-parts of the FIM only
        if False:
            fim = df_fisher_matrix.to_numpy()
            scales = np.zeros(len(fim))

            fim012 = fim[[0,1,2]][:, [0,1,2]]
            inv_fim012 = np.linalg.inv(fim012)
            scales012 = np.sqrt(inv_fim012.diagonal())

            fim3489 = fim[[3,4,8,9]][:, [3,4,8,9]]
            inv_fim3489 = np.linalg.inv(fim3489)
            scales3489 = np.sqrt(inv_fim3489.diagonal())

            fim567 = fim[[5,6,7]][:, [5,6,7]]
            inv_fim567 = np.linalg.inv(fim567)
            scales567 = np.sqrt(inv_fim567.diagonal())

            scales[0] = scales012[0]
            scales[1] = scales012[1]
            scales[2] = scales012[2]
            scales[3] = scales3489[0]
            scales[4] = scales3489[1]
            scales[8] = scales3489[2]
            scales[9] = scales3489[3]
            scales[5] = scales567[0]
            scales[6] = scales567[1]
            scales[7] = scales567[2]
        # Use sub-parts of the FIM only
        ###################

        return pd.DataFrame(scales.reshape((1, -1)), columns=columns)

    def get_scales_from_fim_spins_vs_no_spins(self):
        df_fisher_matrix = self.likelihood_gradient.get_fisher_matrix()
        df_scales = self.get_scales_from_df_fisher_matrix(df_fisher_matrix)

        # If there are spin parameters, we derive scales from the FIM
        # computed without those and choose the best scales, ie the
        # minimum ones
        spin_traj_keys = [self.dict_trajectory_parameters_keys[key] for key in CONST.SPIN_PARAMETERS if key in self.dict_trajectory_parameters_keys.keys()]

        if len(spin_traj_keys) != 0:
            # We extract the trajectory key of each spin_param from
            # the FIM labels directly
            traj_param_keys_no_spins = df_fisher_matrix.index.tolist()
            for spin_key in spin_traj_keys:
                if spin_key in traj_param_keys_no_spins:
                    traj_param_keys_no_spins.remove(spin_key)

            # Using the symmetric property of the FIM, we can sub-select
            # the rows and colums desired using its transpose.
            df_fim_temp = df_fisher_matrix[traj_param_keys_no_spins]
            df_fisher_matrix_no_spins = df_fim_temp.transpose()[traj_param_keys_no_spins]
            df_scales_no_spins = self.get_scales_from_df_fisher_matrix(df_fisher_matrix_no_spins)

            # Now we will compare the common scales derived from both FIM
            # and keep the smallest one. Indeed because spin parameters are
            # correlated with mass params, sometimes we get a scale 10 times
            # bigger with the FIM_with_spins
            df_scales_with_spins = df_scales.copy()
            for traj_key in traj_param_keys_no_spins:
                if df_scales_with_spins[traj_key][0] > df_scales_no_spins[traj_key][0]:
                    df_scales[traj_key] = df_scales_no_spins[traj_key]


            df_scales_logger = df_scales_with_spins.append(df_scales_no_spins, sort=False)
            df_scales_logger = df_scales_logger.append(df_scales)
            df_scales_logger.index = ['From FIM with spins', 'From FIM without spins', 'Scale Chosen']
            cut.logger.info(f'Scales set from the FIMs:\n {df_scales_logger.T}')

        return df_scales

    def set_scales_from_fim_and_boundaries(self):
        # GW170817 setting...
        df_scales_from_fim = self.get_scales_from_fim_spins_vs_no_spins()
        # GW150914 setting...
        # df_fisher_matrix = self.likelihood_gradient.get_fisher_matrix()
        # df_scales_from_fim = self.get_scales_from_df_fisher_matrix(df_fisher_matrix)

        scales = df_scales_from_fim.to_numpy()[0].copy()
        # For highly correlated parameters, like phic-psi, cosinc-lnD, one might want to reduce even more the scale of these params than quarter of their range in case numerical trajectories get rejected. Note that reducing scales too much will produce accurate trajectories likely to be accepted but a high correlation of the chains due to a random walk behaviour that will result in the requirement of more total points needed.
        warn_singular_fim = True
        for idx, key in enumerate(self.search_parameter_keys):
            traj_param_key = self.search_trajectory_parameters_keys[idx]
            max = self.traj_priors[traj_param_key].maximum
            min = self.traj_priors[traj_param_key].minimum
            # boundaries_of_param = self.trajectory_parameters_boundaries[idx]
            half_range = abs(max - min) / 2
            # quarter_range = abs(boundaries_of_param[1] - boundaries_of_param[0]) / 4
            # if key == 'theta_jn':
            #     if scales[idx] > 1:
            #         if warn_singular_fim:
            #             cut.logger.warning(f'Scale of {traj_param_key} is bigger than its range meaning the FIM is singular and some scales might be wrongly derived')
            #             warn_singular_fim = False
            #         cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to 1.0')
            #         scales[idx] = 1
            # if key == 'psi':
            #     if scales[idx] < np.pi/2 or scales[idx] > np.pi:
            #         cut.logger.warning(f'Setting scale of {traj_param_key} from {scales[idx]:.2e} to pi/2')
            #         scales[idx] = np.pi/2
            # if key == 'luminosity_distance':
            #     if scales[idx] > 1:
            #         if warn_singular_fim:
            #             cut.logger.warning(f'Scale of {traj_param_key} is bigger than its range meaning the FIM is singular and some scales might be wrongly computed')
            #             warn_singular_fim = False
            #         cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to 0.5')
            #         scales[idx] = 0.5
            # if key == 'lambda_tilde' and scales[idx]>200:
            #     scales[idx] = 200
            #     cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scales[idx]:.2e}')
            # if key == 'delta_lambda_tilde' and scales[idx]>200:
            #     scales[idx] = 200
            #     cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scales[idx]:.2e}')
            # if key == 'geocent_time':
            #     scales[idx] = 0.0001
            #     cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scales[idx]:.2e}')
            # # if key == 'chirp_mass':
            # #     scales[idx] = 0.01
            # #     cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scales[idx]:.2e}')
            # # if key == 'reduced_mass':
            # #     scales[idx] = 0.01
            # #     cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scales[idx]:.2e}')
            # if key == 'dec':
            #     scales[idx] = 0.3
            #     cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scales[idx]:.2e}')
            # if key == 'ra':
            #     scales[idx] = np.pi/2
            #     cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scales[idx]:.2e}')
            if scales[idx] > half_range:
                cut.logger.warning(f'Reducing scale of {traj_param_key} to half of the dynamical range from {scales[idx]:.2e} to {half_range:.2e}')
                scales[idx] = half_range
            if np.isnan(scales[idx]):
                cut.logger.warning(f'Setting scale of {traj_param_key} to half of the dynamical range = {half_range:.2e} because it was NaN.')
                scales[idx] = half_range
            if scales[idx] < 1e-15:
                cut.logger.warning(f'Setting scale of {traj_param_key} to half of the dynamical range = {half_range:.2e} because it was equal to zero.')
                scales[idx] = half_range
            if key in ['mass_1', 'mass_2']:
                scale_new = scales[idx]/10
                cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scale_new:.2e}, because previous analysis showed it was to high...')
                scales[idx] = scale_new
            # if key in ['chi_1', 'chi_2']:
            #     scale_new = scales[idx]/4
            #     cut.logger.warning(f'Reducing scale of {traj_param_key} from {scales[idx]:.2e} to {scale_new:.2e}, because previous analysis showed it was to high...')
            #     scales[idx] = scale_new
            # if idx in [8,9,10,11]:
            #     scales[idx] /= 10
            #     cut.logger.warning(f'Dividing scale of {traj_param_key} by 10 to {scales[idx]:.2e}')
            # if idx in [3]:
            #     scales[idx] /= 10
            #     cut.logger.warning(f'Dividing scale of {traj_param_key} by 10 to {scales[idx]:.2e}')
            # if idx in [4]:
            #     scales[idx] /= 20
            #     cut.logger.warning(f'Dividing scale of {traj_param_key} by 20 to {scales[idx]:.2e}')
            # if idx in [12,13]:
            #     scales[idx] /= 5
            #     cut.logger.warning(f'Dividing scale of {traj_param_key} by 5 to {scales[idx]:.2e}')

        self.scales = scales

        df_scales_logger = pd.DataFrame([df_scales_from_fim.to_numpy()[0], self.scales], columns=self.search_trajectory_parameters_keys, index=['From FIM', 'Constrained'])
        cut.logger.info(f'Scales setting:\n {df_scales_logger.T}')

        return scales

    def get_scales_from_quantiles(self, quantiles=[16, 84]):
        """
        We derive the scale of each trajectory parameter as the difference between an upper and a lower quantile over the distribution of samples.
        """
        quants = np.percentile(self.traj_samples, quantiles, axis=0)
        return quants[1] - quants[0]

    def set_scales_from_quantiles(self):
        old_scales = self.scales
        new_scales = self.get_scales_from_quantiles()
        df = pd.DataFrame([old_scales, new_scales], columns=self.search_trajectory_parameters_keys, index=['old', 'new'])
        cut.logger.info(f'Setting scales from the distribution of samples accumulated:\n {df}')
        self.scales = new_scales

    def get_scales_from_covariance_matrix(self):
        cov = np.cov(self.traj_samples.T)
        return np.sqrt(cov.diagonal())

    def set_scales_from_covariance_matrix(self):
        old_scales = self.scales
        new_scales = self.get_scales_from_covariance_matrix()
        df = pd.DataFrame([old_scales, new_scales], columns=self.search_trajectory_parameters_keys, index=['old', 'new'])
        cut.logger.info(f'Setting scales from the covariance matrix of samples accumulated:\n {df.T}')
        self.scales = new_scales

    def get_correlation_matrix(self):
        cov = np.cov(self.traj_samples.T)
        inv_sqrt_cov_diag = 1 / np.sqrt(cov.diagonal())
        inv_sqrt_cov_diag_matrix = np.diag(inv_sqrt_cov_diag)
        corr = inv_sqrt_cov_diag_matrix @ cov @ inv_sqrt_cov_diag_matrix
        return corr

    def q_pos_to_q_pos_traj(self, q_pos):
        q_pos_traj = q_pos.copy()
        for i in range(len(q_pos)):
            q_pos_traj[i] = self.search_trajectory_functions[i](q_pos[i])
        return q_pos_traj

    def q_pos_traj_to_q_pos(self, q_pos_traj):
        q_pos = q_pos_traj.copy()
        for i in range(len(q_pos_traj)):
            q_pos[i] = self.search_trajectory_inv_functions[i](q_pos_traj[i])
        return q_pos

    def parameters_dict_to_q_pos(self, parameters_dict):
        """
        Converts a dictionary containing the search parameters values into a numpy array with these values keeping the order of self.search_parameter_keys
        """
        q_pos = np.zeros(self.n_dim)

        for idx, key in enumerate(self.search_parameter_keys):
            q_pos[idx] = parameters_dict[key]

        # For geocent_time we actually sample the 'geocent_duration' for numerical precision reasons. Hence we need to substract the start_time of the strain_data.
        if 'geocent_time' in self.search_parameter_keys:
            idx = self.search_parameter_keys.index('geocent_time')
            q_pos[idx] -= self.start_time
        return q_pos

    def q_pos_to_parameters_dict(self, q_pos):
        """
        Converts the array q_pos containing the position values of a point in the search parameter space to the corresponding dictionary. The correspondance between a key and the index is known through sampler.search_parameter_keys
        """
        parameters_dict = {}
        for idx, key in enumerate(self.search_parameter_keys):
            parameters_dict[key] = q_pos[idx]

        # Since we actually sample the 'geocent_duration' (for numerical precision reasons) we need to add the start_time of the strain_data to recover the gps time: 'geocent_time' needed as an input in lalinference
        # Normally 'geocent_time' should never be in the search parameters keys since at the beginning of the run we switch the key 'geocent_time' with 'geocent_duration' using the function Library.initialization.sampler_dict_update_geocent_time_by_duration()
        if 'geocent_time' in self.search_parameter_keys:
            parameters_dict['geocent_duration'] = parameters_dict['geocent_time'] - self.start_time
        if 'geocent_duration' in self.search_parameter_keys:
            parameters_dict['geocent_time'] = parameters_dict['geocent_duration'] + self.start_time

        if 'chirp_mass' in self.search_parameter_keys and 'reduced_mass' in self.search_parameter_keys:
            parameters_dict = paru.update_mass_parameters_from_chirp_mass_and_reduced_mass(parameters_dict)
        elif 'mass_1' in self.search_parameter_keys and 'mass_2' in self.search_parameter_keys:
            parameters_dict = paru.update_mass_parameters_from_component_masses(parameters_dict)
        else:
            cut.logger.error("The search parameter space must contain either ['chirp_mass', 'reduced_mass']  or ['mass_1', 'mass_2']; at the moment it reads: {}.".format(self.search_parameter_keys))
        return parameters_dict

    def q_pos_traj_to_parameters_dict(self, q_pos_traj):
        q_pos = self.q_pos_traj_to_q_pos(q_pos_traj)
        parameters_dict = self.q_pos_to_parameters_dict(q_pos)
        return parameters_dict

    def q_pos_traj_to_parameters_traj_dict(self, q_pos_traj):
        parameters_traj = {}
        for i, traj_key in enumerate(self.search_trajectory_parameters_keys):
            parameters_traj[traj_key] = q_pos_traj[i]
        return parameters_traj

    def parameters_dict_to_parameters_traj_dict(self, parameters_dict):
        parameters_traj_dict = parameters_dict.copy()
        q_pos = self.parameters_dict_to_q_pos(parameters_dict)
        q_pos_traj = self.q_pos_to_q_pos_traj(q_pos)
        dict_updates = self.q_pos_traj_to_parameters_traj_dict(q_pos_traj)
        parameters_traj_dict.update(dict_updates)
        return parameters_traj_dict

    def update_likelihood_parameters_from_q_pos(self, q_pos):
        parameters_dict = self.q_pos_to_parameters_dict(q_pos)
        self.likelihood_gradient.likelihood.parameters.update(parameters_dict)

    def update_likelihood_parameters_from_q_pos_traj(self, q_pos_traj):
        parameters_dict = self.q_pos_traj_to_parameters_dict(q_pos_traj)
        self.likelihood_gradient.likelihood.parameters.update(parameters_dict)

    def get_dlogL_hybrid_from_dlogL_ana(self, q_pos_traj, dlogL_ana):
        """
        Updates dlogL computed analytically on the "cubic" parametes with numerical gradients for those it's been specified on.
        """
        dlogL_hybrid = dlogL_ana.copy()
        self.update_likelihood_parameters_from_q_pos_traj(q_pos_traj)
        dlogL_num_dict = self.likelihood_gradient.calculate_log_likelihood_gradient(self.search_parameter_keys_num_in_hybrid)
        for param_key in self.search_parameter_keys_num_in_hybrid:
            param_index = self.search_parameter_keys.index(param_key)
            traj_key = self.search_trajectory_parameters_keys[param_index]
            dlogL_hybrid[param_index] = dlogL_num_dict[traj_key]

        return dlogL_hybrid

    def remap_q_pos_traj_in_boundaries(self, q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon):
        BreakIndic = False
        # When moving with chirp_mass and reduced_mass, we can get to unphysical spots of parameters space where the symmetric_mass_ratio is superior to 0.25.
        # We perform a bounce if that happens
        if 'chirp_mass' in self.search_parameter_keys and 'reduced_mass' in self.search_parameter_keys:
            idx_Mc = self.search_parameter_keys.index('chirp_mass')
            idx_mu = self.search_parameter_keys.index('reduced_mass')
            Mc_traj_inv_func = self.search_trajectory_inv_functions[idx_Mc]
            mu_traj_inv_func = self.search_trajectory_inv_functions[idx_mu]
            mc_pos_new = Mc_traj_inv_func(q_pos_traj_new[idx_Mc])
            mu_pos_new = mu_traj_inv_func(q_pos_traj_new[idx_mu])
            mtot_pos_new, eta_pos_new = paru.Mc_and_mu_to_M_and_eta(mc_pos_new, mu_pos_new)
            # We don't use eta_bounce=0.25 to prevent the gradient of the log-prior, involving
            # the jacobian to diverge "too much" when getting to close to 0.25.
            # eta_bounce = 0.2499
            # eta_bounce = 0.2495
            eta_bounce = 0.25
            mass_1_outside = False
            mass_2_outside = False
            if eta_pos_new <= eta_bounce:
                # Check that individual masses are in their prior range. Indeed it can happen that mc and mu are inside there range but not m1 or m2
                mass_1_new, mass_2_new = paru.M_and_eta_to_m1_and_m2(mtot_pos_new, eta_pos_new)
                mass_1_outside = not(self.priors['mass_1'].is_in_prior_range(mass_1_new))
                mass_2_outside = not(self.priors['mass_2'].is_in_prior_range(mass_2_new))
            bounce_cond = np.isnan(eta_pos_new) or eta_pos_new > eta_bounce or mass_1_outside or mass_2_outside
            if bounce_cond:
                # cut.logger.info(f"   Trajectory bounces on masses: eta = {eta_pos_new:.4f}")
                p_mom_new, q_pos_traj_new = self.bounce_on_boundary(idx_Mc, q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon)
                p_mom_new, q_pos_traj_new = self.bounce_on_boundary(idx_mu, q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon)
                mc_pos_new = Mc_traj_inv_func(q_pos_traj_new[idx_Mc])
                mu_pos_new = mu_traj_inv_func(q_pos_traj_new[idx_mu])
                mtot_pos_new, eta_pos_new = paru.Mc_and_mu_to_M_and_eta(mc_pos_new, mu_pos_new)
                mass_1_outside = False
                mass_2_outside = False
                if eta_pos_new <= eta_bounce:
                    # Check that individual masses are in their prior range. Indeed in can happen that mc and mu are inside there range but not m1 or m2
                    mass_1_new, mass_2_new = paru.M_and_eta_to_m1_and_m2(mtot_pos_new, eta_pos_new)
                    mass_1_outside = not(self.priors['mass_1'].is_in_prior_range(mass_1_new))
                    mass_2_outside = not(self.priors['mass_2'].is_in_prior_range(mass_2_new))
                    if mass_1_outside or mass_2_outside:
                        cut.logger.info(f"   Trajectory diverge in masses parameter: m1 = {mass_1_new:.2f}, m2 = {mass_2_new:.2f} twice in a row")
                        BreakIndic = True
                        return q_pos_traj_new, p_mom_new, BreakIndic
                if np.isnan(eta_pos_new) or eta_pos_new > eta_bounce:
                    cut.logger.info(f"   Trajectory diverge in masses parameter: eta = {eta_pos_new:.2f} > {eta_bounce:.2f}, twice in a row")
                    BreakIndic = True
                    return q_pos_traj_new, p_mom_new, BreakIndic
                else:
                    Mc_traj_key = self.search_trajectory_parameters_keys[idx_Mc]
                    mu_traj_key = self.search_trajectory_parameters_keys[idx_mu]
                    ln_Mc_is_outside = not(self.traj_priors[Mc_traj_key].is_in_prior_range(q_pos_traj_new[idx_Mc]))
                    ln_mu_is_outside = not(self.traj_priors[mu_traj_key].is_in_prior_range(q_pos_traj_new[idx_mu]))
                    if ln_Mc_is_outside or ln_mu_is_outside:
                        cut.logger.info(f"   Trajectory diverge in masses parameter: Mc = {np.exp(q_pos_traj_new[idx_Mc])}, mu = {np.exp(q_pos_traj_new[idx_mu])}")
                        BreakIndic = True
                        return q_pos_traj_new, p_mom_new, BreakIndic
        elif 'mass_1' in self.search_parameter_keys and 'mass_2' in self.search_parameter_keys:
            idx_mass_1 = self.search_parameter_keys.index('mass_1')
            idx_mass_2 = self.search_parameter_keys.index('mass_2')
            mass_1_new = q_pos_traj_new[idx_mass_1]
            mass_2_new = q_pos_traj_new[idx_mass_2]
            if mass_2_new > mass_1_new:
                print(f'                 Bouncing on equal mass boundary since m1 = {mass_1_new} < m2 = {mass_2_new}')
                p_mom_new, q_pos_traj_new = self.bounce_on_boundary(idx_mass_1, q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon)
                p_mom_new, q_pos_traj_new = self.bounce_on_boundary(idx_mass_2, q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon)
                print('                    => After bounce: m1 = {} and  m2 = {}'.format(q_pos_traj_new[idx_mass_1], q_pos_traj_new[idx_mass_2]))
                # temp = mass_2_new
                # mass_2_new = mass_1_new
                # mass_1_new = temp
        else:
            cut.logger.error("The search parameter space must contain either ['chirp_mass', 'reduced_mass']  or ['mass_1', 'mass_2']; at the moment it reads: {}.".format(self.search_parameter_keys))

        # For each parameter, we check that the leap-frog has kept it inside its priors boundaries.
        # If not we perform a bounce. If that is not sufficient we break the trajectory
        for idx, (key, traj_key) in enumerate(self.search_keys_and_traj_keys):
            is_outside = not(self.traj_priors[traj_key].is_in_prior_range(q_pos_traj_new[idx]))
            if is_outside:
                # For ra and dec we don't bounce on the boundary but remapp their value by going to cartesian coordinates first and back to ra dec then
                if key in ['ra', 'dec']:
                    idx_ra = self.search_parameter_keys.index('ra')
                    idx_dec = self.search_parameter_keys.index('dec')
                    ra_inv_func = self.search_trajectory_inv_functions[idx_ra]
                    dec_inv_func = self.search_trajectory_inv_functions[idx_dec]
                    ra = ra_inv_func(q_pos_traj_new[idx_ra])
                    dec = dec_inv_func(q_pos_traj_new[idx_dec])
                    ra_new, dec_new = paru.remap_sky_angles(ra, dec)
                    ra_func = self.search_trajectory_functions[idx_ra]
                    dec_func = self.search_trajectory_functions[idx_dec]
                    q_pos_traj_new[idx_ra] = ra_func(ra_new)
                    q_pos_traj_new[idx_dec] = dec_func(dec_new)
                    cut.logger.info(f'   Remapped sky angles (ra, dec) because {traj_key} was out of range.')
                elif key == 'phase':
                    phase_inv_func = self.search_trajectory_inv_functions[idx]
                    phase_func = self.search_trajectory_functions[idx]
                    phase_new = phase_inv_func(q_pos_traj_new[idx]) % (2 * np.pi)
                    q_pos_traj_new[idx] = phase_func(phase_new)
                elif key == 'psi':
                    psi_inv_func = self.search_trajectory_inv_functions[idx]
                    psi_func = self.search_trajectory_functions[idx]
                    psi_new = psi_inv_func(q_pos_traj_new[idx]) % np.pi
                    q_pos_traj_new[idx] = psi_func(psi_new)
                else:
                    p_mom_new, q_pos_traj_new = self.bounce_on_boundary(idx, q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon)
                    if key in ['mass_1', 'mass_2']:
                        print(f'                 Bouncing on prior mass boundary for {key}')
                is_still_outside = not(self.traj_priors[traj_key].is_in_prior_range(q_pos_traj_new[idx]))
                if is_still_outside:
                    BreakIndic = True
                    cut.logger.info(f"   Bounce on {traj_key} couldn't map it back: {q_pos_traj_new[idx]:.2e}")
                    return q_pos_traj_new, p_mom_new, BreakIndic


        # General check on NaN in the code and break trajectory if there is
        if np.isnan(q_pos_traj_new).any():
            idx = np.where(np.isnan(q_pos_traj_new))[0]
            cut.logger.info(f"   General check on NaN is POSITIVE. Concerned indices are: {idx}")
            BreakIndic = True
        if np.isinf(q_pos_traj_new).any():
            cut.logger.info("   General check on Inf is POSITIVE")
            BreakIndic = True

        return q_pos_traj_new, p_mom_new, BreakIndic

    def bounce_on_boundary(self, idx, q_pos_traj_new, q_pos_traj, p_mom_new, p_mom, dlogL_pos, epsilon):
        # Flip sign of momentum and move backwards
        # The momenta we want to negate is the un-updated one before the 1/2 leapfrog step hence we then directly apply to it the 1/2 leapfrog step.
        p_mom_new[idx] = -p_mom[idx] + 0.5 * epsilon * dlogL_pos[idx] * self.scales[idx]
        q_pos_traj_new[idx] = q_pos_traj[idx] + epsilon * p_mom_new[idx] * self.scales[idx]
        return p_mom_new, q_pos_traj_new

    def plot_autocorrelations_of_parameters(self, savefig=True, with_times_and_lags=True):
        fig = plt.figure(figsize=(15,10))
        ax = fig.add_subplot(1,1,1)

        samples = np.asarray(self.samples)
        max_lag = samples.shape[0] - 1
        lags = np.arange(max_lag) + 1
        for idx, param_key in enumerate(self.search_parameter_keys):
            chain = samples[:, idx]
            # We remove the first index here because that corresponds to
            # the autocorrelation with no lag (ie lag=0) and rho(lag=0) = 1
            rho = cautocorr.autocorrelation(chain)[1:]
            L = int(cautocorr.autocorrelation_length_estimate(chain)) + 1
            latex_label = CONST.LATEX_LABELS[param_key]
            full_latex_label = latex_label
            traj_key = self.likelihood_gradient.dict_trajectory_parameters_keys[param_key]
            full_latex_label += f', L={L}, ' + rf'$\sigma =$ {self.scales_dict[traj_key]:.1e}'
            ax.plot(lags, rho, label=rf'{full_latex_label}')

        ax.set_xscale('log')
        ax.set_xlabel(r'lag $\tau$')
        ax.set_ylabel(r'$\rho(\tau)$')
        chain_len = len(self.samples)
        log10_len = int(np.log10(chain_len))
        str_len = f'{chain_len/10**log10_len:1.1f}'
        if str_len == '1.0': str_len = ''
        latex_len = '$' + str_len + f' \\cdot 10^{log10_len}' '$'
        ax.set_title(rf'Autocorrelations on chains of length {latex_len}')
        # ax.set_title(rf'Autocorrelations on chains of length {self.samples.shape[0]:1.1e}')
        ax.legend()
        if savefig:
            fig.savefig(self.outdir + f'/plots/{len(self.samples)}_autocorrelations_emcee.png')
        else:
            plt.show()

    def save_state(self, count, n_traj_already_run, duration1, acc, q_pos, dlogL, logL, randGen, qpos_traj_phase1=None, dlogL_traj_phase1=None, fit_method=None, oluts_fit=None, stuck_seq_and_traj_list=None, count_phase3=0, duration2=0, duration3=0, atr=0, htr=0, ntr=0, atr_time=0, htr_time=0, ntr_time=0, stuck_count=0, hybrid_stuck=0, next_traj_to_check_sis=None, state_dir_suffix=''):
        cut.logger.info('Saving state of the run in /outdir/sampler_state/ ...')
        state_dir = self.outdir + 'sampler_state/' + state_dir_suffix
        # pu.mkdirs(state_dir)
        cut.check_directory_exists_and_if_not_mkdir(state_dir)
        state = {}
        state['count'] = count
        state['count_phase3'] = count_phase3
        state['n_traj_already_run'] = n_traj_already_run
        if next_traj_to_check_sis is None:
            next_traj_to_check_sis = CONST.N_TRAJ_TO_CHECK_SIS_DEFAULT
        state['next_traj_to_check_sis'] = next_traj_to_check_sis
        state['acc'] = acc
        state['duration1'] = duration1
        state['duration2'] = duration2
        state['duration3'] = duration3
        state['atr'] = atr
        state['htr'] = htr
        state['ntr'] = ntr
        state['atr_time'] = atr_time
        state['htr_time'] = htr_time
        state['ntr_time'] = ntr_time
        state['stuck_count'] = stuck_count
        state['hybrid_stuck'] = hybrid_stuck
        pu.save_dict_to_json(state, state_dir + 'state.json')


        np.savetxt(state_dir + "scales.dat", self.scales)
        np.savetxt(state_dir + 'q_pos.dat', q_pos)
        np.savetxt(state_dir + 'dlogL.dat', dlogL)
        np.savetxt(state_dir + 'logL.dat', [logL])
        if qpos_traj_phase1 is not None:
            np.savetxt(state_dir + "qpos_traj_phase1.dat", qpos_traj_phase1, fmt='%.25e')
            np.savetxt(state_dir + "dlogL_traj_phase1.dat", dlogL_traj_phase1, fmt='%.25e')
        np.savetxt(state_dir + "samples.dat", np.asarray(self.samples), header=' | '.join(self.search_parameter_keys))
        np.savetxt(state_dir + "chain_metrics.dat", np.asarray(self.chain_metrics), fmt=['%.10e', '%.10e', '%.10e', '%.0f'], header=' | '.join(['mf_snr', 'log_l', 'acc', 'accepted']))
        if stuck_seq_and_traj_list is None:
            stuck_seq_and_traj_list = []
        np.savetxt(state_dir + "rejection_sequences.dat", np.asarray(stuck_seq_and_traj_list), fmt='%d', header='traj_nb | rejection_count')
        if fit_method is not None:
            fit_method.save(state_dir + 'global_fit')
        if oluts_fit is not None:
            oluts_fit.save(state_dir + 'oluts_fit')

        randGen_state = randGen.get_state()
        randGen_state_dict = dict(state0=randGen_state[0], state2=randGen_state[2], state3=randGen_state[3], state4=randGen_state[4])
        pu.save_dict_to_json(randGen_state_dict, state_dir + 'randGen_state.json')
        np.savetxt(state_dir + 'randGen_state_keys.dat', randGen_state[1])

    def get_state(self, state_dir=None):
        randGen = np.random.RandomState(seed=2)
        if state_dir is None:
            state_dir = self.outdir + 'sampler_state/'
        if os.path.exists(state_dir):
            cut.logger.info(f'Loading former state of the sampler from {state_dir} ...')
            state = pu.json_to_dict(state_dir + 'state.json')
            count = state['count']
            count_phase3 = state['count_phase3']
            n_traj_already_run = state['n_traj_already_run']
            acc = state['acc']
            duration1 = state['duration1']
            duration2 = state['duration2']
            duration3 = state['duration3']
            atr = state['atr']
            htr = state['htr']
            ntr = state['ntr']
            atr_time = state.get('atr_time', 0)
            htr_time = state.get('htr_time', 0)
            ntr_time = state.get('ntr_time', 0)
            stuck_count = state['stuck_count']
            hybrid_stuck = state['hybrid_stuck']
            next_traj_to_check_sis = state.get('next_traj_to_check_sis', n_traj_already_run + CONST.N_TRAJ_TO_CHECK_SIS_DEFAULT)
            cut.logger.info(f"{n_traj_already_run} trajectories had been run already, Acc = {acc:2.2%}.")

            cut.logger.info("Loading scales, fit files and samples accumulated...")
            self.scales = np.loadtxt(state_dir + 'scales.dat')
            q_pos = np.loadtxt(state_dir + 'q_pos.dat')
            dlogL = np.loadtxt(state_dir + 'dlogL.dat')
            logL = np.loadtxt(state_dir + 'logL.dat').reshape(-1)[0]
            if os.path.exists(state_dir + 'qpos_traj_phase1.dat'):
                qpos_traj_phase1 = np.loadtxt(state_dir + 'qpos_traj_phase1.dat').tolist()
                dlogL_traj_phase1 = np.loadtxt(state_dir + 'dlogL_traj_phase1.dat').tolist()
            else:
                qpos_traj_phase1 = []
                dlogL_traj_phase1 = []

            self.samples = np.loadtxt(state_dir + 'samples.dat').tolist()
            if os.path.exists(state_dir + 'chain_metrics.dat'):
                self.chain_metrics = np.loadtxt(state_dir + 'chain_metrics.dat').tolist()
            else:
                self.chain_metrics = []


            cut.logger.info("Setting the random number generator as it was at the end of previous run...")
            randGen_state_dict = pu.json_to_dict(state_dir + 'randGen_state.json')
            randGen_state_keys = np.loadtxt(state_dir + 'randGen_state_keys.dat')
            randGen_state_keys = randGen_state_keys.astype(int)
            randGen_state = (randGen_state_dict['state0'], randGen_state_keys, randGen_state_dict['state2'], randGen_state_dict['state3'], randGen_state_dict['state4'])
            randGen.set_state(randGen_state)

            if os.path.exists(state_dir + 'rejection_sequences.dat'):
                stuck_seq_and_traj_list = np.loadtxt(state_dir + 'rejection_sequences.dat', dtype=np.int).tolist()
            else:
                stuck_seq_and_traj_list = []
        else:
            count = 0
            count_phase3 = 0
            n_traj_already_run = 0
            duration1 = 0
            duration2 = 0
            duration3 = 0
            acc = 0
            next_traj_to_check_sis = CONST.N_TRAJ_TO_CHECK_SIS_DEFAULT
            q_pos = self.parameters_dict_to_q_pos(self.starting_point_parameters)

            logTrajPi = self.traj_priors.ln_prob(self.starting_point_parameters)
            cut.logger.info(f'traj_priors.ln_prob() = {logTrajPi}')
            dlogTrajPi_dict = self.traj_prior_gradients.log_traj_prior_gradients(self.starting_point_parameters)
            dlogTrajPi_dict_formatted = cut.dictionary_to_formatted_string(dlogTrajPi_dict)
            cut.logger.info(f'traj_prior_gradients: \n{dlogTrajPi_dict_formatted}')

            self.likelihood.parameters = self.starting_point_parameters.copy()
            logL = self.likelihood.log_likelihood_ratio()
            cut.logger.info(f"likelihood.log_likelihood_ratio() = {logL}")
            # breakpoint()
            # waveform_polarizations = self.likelihood.waveform_generator.frequency_domain_strain(self.starting_point_parameters)
            dlogL_dict = self.likelihood_gradient.calculate_log_likelihood_gradient()
            dlogL_dict_formatted = cut.dictionary_to_formatted_string(dlogL_dict)
            cut.logger.info(f'log_likelihood_gradient: {dlogL_dict_formatted}')
            dlogL = self.likelihood_gradient.convert_dlogL_dict_to_np_array(dlogL_dict)

            self.set_scales_from_fim_and_boundaries()

            qpos_traj_phase1 = []
            dlogL_traj_phase1 = []
            # fit_method = None
            qpos_olut_fit = []
            dlogL_olut_fit = []
            oluts_dict = None
            # oluts_fit = None
            atr = 0
            htr = 0
            ntr = 0
            atr_time = 0
            htr_time = 0
            ntr_time = 0
            stuck_count = 0
            hybrid_stuck = 0
            stuck_seq_and_traj_list = []

        return count, n_traj_already_run, duration1, acc, q_pos, dlogL, logL, randGen, qpos_traj_phase1, dlogL_traj_phase1, stuck_seq_and_traj_list, count_phase3, duration2, duration3, atr, htr, ntr, atr_time, htr_time, ntr_time, stuck_count, hybrid_stuck, next_traj_to_check_sis

    def set_oluts_regressor(self):
        param_keys = [self.search_trajectory_parameters_keys[i] for i in self.search_parameter_indices_local_fit]
        param_indices = self.search_parameter_indices_local_fit
        idx_for_distance = self.search_parameter_indices_local_fit
        squared_scales = self.scales**2
        oluts_fit = fnr.OLUTsRegressor(param_keys, param_indices, idx_for_distance, squared_scales)
        return oluts_fit

    @property
    def traj_samples(self):
        samples = np.asarray(self.samples)
        traj_samples = np.empty(samples.shape)
        for idx in range(self.n_dim):
            traj_func = self.search_trajectory_functions[idx]
            traj_samples[:, idx] = traj_func(samples[:, idx])
        return traj_samples

    def get_posterior_with_geocent_time_not_duration(self):
        samples = np.asarray(self.samples)
        duration_index = self.search_parameter_keys.index('geocent_duration')
        # duration_index = self.search_parameter_keys.index('geocent_time')
        samples[:, duration_index] += self.start_time
        columns = self.search_parameter_keys.copy()
        columns[duration_index] = 'geocent_time'
        return pd.DataFrame(samples, columns=columns)

    def get_posterior(self, sis=True, reweighted=False, comp_mass=False):
        posterior = self.get_posterior_with_geocent_time_not_duration()
        if reweighted:
            # It is important not to create copies of these priors as
            # the convert_to_flat...() function might add keys to these
            # dictionaries which will be needed in the sampler.get_result()
            old_priors = self.priors
            new_priors = self.priors_for_reweight
            # It is important to sort the index here if we are to compute
            # the autocorrelation length on these reweighted samples
            posterior = gwprior.convert_Mc_mu_to_flat_in_component_mass_prior(posterior, old_priors, new_priors).sort_index()
        if comp_mass and not(reweighted): #if reweight, comp_mass are already added
            from gw.prior import add_m1m2_columns_to_posterior_in_Mc_mu
            posterior = add_m1m2_columns_to_posterior_in_Mc_mu(posterior)
        if sis:
            max_acl = cautocorr.get_maximum_integrated_time(posterior.to_numpy())
            posterior = posterior[::max_acl]
        return posterior

    def get_result(self, sis=True, reweighted=False, comp_mass=False):
        posterior = self.get_posterior(sis=sis, reweighted=reweighted, comp_mass=comp_mass)
        search_parameter_keys = posterior.keys().tolist()
        parameter_labels = CONST.get_latex_labels(search_parameter_keys)

        if reweighted:
            priors = self.priors_for_reweight.copy()
        else:
            priors = self.priors.copy()

        result = gwresult.Result(
            search_parameter_keys=search_parameter_keys,
            parameter_labels=parameter_labels,
            priors=priors,
            injection_parameters=self.starting_point_parameters.copy(),
            posterior=posterior,
            meta_data={}
            )
        return result

    def plot_corner(self, sis=True, reweighted=False, priors=True, save=True, outdir=None, filename=None, in_comp_mass=False):
        result = self.get_result(sis=sis, reweighted=reweighted)

        if outdir is None:
            outdir = self.outdir
        cut.check_directory_exists_and_if_not_mkdir(outdir)
        if save and filename is None:
            if reweighted:
                reweight_suffix = 'reweighted'
            else:
                reweight_suffix = 'trajpriors'
            if sis:
                sis_suffix = 'sis'
            else:
                sis_suffix = 'corr'
            if in_comp_mass:
                comp_suffix = 'm1m2'
            else:
                comp_suffix = 'mcmu'
            filename = outdir + f'corner_{reweight_suffix}_{sis_suffix}_{comp_suffix}_{len(result.posterior)}_samples.png'
        if in_comp_mass:
            result.plot_corner_in_component_masses(truth_dict=self.starting_point_parameters, priors=priors, save=save, filename=filename)
        else:
            result.plot_corner_not_in_component_masses(truth_dict=self.starting_point_parameters, priors=priors, save=save, filename=filename)

    def plot_all_corners(self, priors=True, save=True):
        cut.logger.info(f'Plotting corner plots corresponding to combinations of reweighting/comp_mass plane/sis ...')
        outdir = self.outdir + 'plots/'
        cut.check_directory_exists_and_if_not_mkdir(outdir)
        if self.priors_for_reweight is None:
            reweight_options = [False]
            cut.logger.info(f'No priors for reweighting have been passed.')
        else:
            reweight_options = [False, True]
        for reweighted in reweight_options:
            for sis in [False, True]:
                for in_comp_mass in [False, True]:
                    self.plot_corner(sis=sis, reweighted=reweighted, priors=priors, save=save, outdir=outdir, in_comp_mass=in_comp_mass)

    def plot_hmc_vs_li(self, posterior_cit_path= '/Users/marcarene/projects/lalinference_runs/GW170817/CIT/TidalP-1/post_tides.dat', posterior_cit_label='LALInf-TF2tides', sis=True, reweighted=False):
        result_hmc = self.get_result(sis=sis, reweighted=reweighted, comp_mass=True)
        result_hmc.label = f'HMC-{self.approximant}'
        # result_hmc.label = f'Central diff on spins and tides'
        # import IPython; IPython.embed(); sys.exit()
        # file = '../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/wrong_idx_segment/200000_1300_2000_200_200_0.005_forward/phase_marginalization/IMRPhenomD_NRTidal/sampler_state/samples.dat'
        # self.samples = np.loadtxt(file)
        # result_cit = self.get_result(sis=sis, reweighted=reweighted, comp_mass=True)
        # result_cit.label = f'Forward diff on spins and tides'





        posterior_cit = pd.read_csv(posterior_cit_path, sep='\t')
        del posterior_cit['Unnamed: 0']
        posterior_cit.columns = result_hmc.posterior.columns.copy()
        result_cit = gwresult.Result(
            label=posterior_cit_label,
            search_parameter_keys=posterior_cit.keys().tolist(),
            parameter_labels=result_hmc.parameter_labels,
            priors=self.priors,
            posterior=posterior_cit.sample(len(result_hmc.posterior)),
            meta_data={}
            )

        from bilby.core.result import plot_multiple
        # outdir = self.outdir + 'plots/corner_central_vs_forward/'
        outdir = self.outdir + 'plots/corner_HMC_vs_LI/'
        cut.check_directory_exists_and_if_not_mkdir(outdir)
        # filename_base = outdir + f'corner_central_vs_forward_{len(result_hmc.posterior)}_'
        filename_base = outdir + f'corner_HMC_vs_LI_{len(result_hmc.posterior)}_'

        plot_parameters_all = result_hmc.posterior.columns.tolist()
        plot_parameters_all_compmass_only = plot_parameters_all.copy()
        plot_parameters_all_compmass_only.remove('chirp_mass')
        plot_parameters_all_compmass_only.remove('reduced_mass')

        plot_parameters_theta_psi_dist =  ['theta_jn', 'psi', 'luminosity_distance']
        plot_parameters_masses =  ['chirp_mass', 'reduced_mass', 'mass_1', 'mass_2']
        plot_parameters_sky_time =  ['ra', 'dec', 'geocent_time']
        # plot_parameters_spins =  ['chi_1', 'chi_2']
        # plot_parameters_masses_spins =  ['mass_1', 'mass_2', 'chi_1', 'chi_2']
        plot_parameters_masses_spins_tides =  ['mass_1', 'mass_2', 'chi_1', 'chi_2', 'lambda_1', 'lambda_2']
        # plot_parameters_combinations = [plot_parameters_spins_mu]
        plot_parameters_combinations = [plot_parameters_all_compmass_only, plot_parameters_theta_psi_dist, plot_parameters_masses, plot_parameters_sky_time, plot_parameters_masses_spins_tides]
        # plot_parameters_combinations = [['lambda_1', 'lambda_2']]
        # plot_parameters.remove('mass_1')
        # plot_parameters.remove('mass_2')
        for plot_parameters in plot_parameters_combinations:
            filename = filename_base + '-'.join(plot_parameters) + '.png'
            # hist2d_kwargs = dict(levels=[0.5, 0.90], bins=20)
            plot_multiple([result_hmc, result_cit], filename=filename, parameters=plot_parameters)

    def plot_walkers(self, label=None, outdir=None, idx_start_stop=(None, None)):
        result = self.get_result(sis=False, reweighted=False)
        idx_start, idx_stop = idx_start_stop
        samples = np.asarray(self.samples)
        result.walkers = np.asarray([samples[idx_start:idx_stop, :]])
        result.nburn = 0
        if label is None:
            if idx_start is None: idx_start = 0
            if idx_stop is None: idx_stop = len(samples)
            result.label = f'{idx_start}-{idx_stop}'
        if outdir is None:
            outdir = self.outdir + 'plots'
        result.plot_walkers(outdir=outdir)

    def plot_corner_qpos_phase1(self, qpos_traj_phase1):
        columns = sampler.search_trajectory_parameters_keys.copy()
        df_qpos_phase1 = pd.DataFrame(qpos_traj_phase1, columns=columns)
        parameter_labels = CONST.get_latex_labels(columns)
        result = gwresult.Result(
            search_parameter_keys=columns,
            parameter_labels=parameter_labels,
            parameter_labels_with_unit=parameter_labels,
            priors=priors,
            posterior=df_qpos_phase1,
            meta_data={}
            )
        filename = sampler.outdir + f'plots/corner_qpos_train_{len(qpos_traj_phase1)}.png'
        result.plot_corner(outdir=sampler.outdir + 'plots', filename=filename)

    def plot_corner_dlogL_vs_qpos_phase1(self, qpos_traj_phase1, dlogL_traj_phase1, dlogL_key='psi'):
        # qpos_traj_phase1 = np.asarray(qpos_traj_phase1)
        # dlogL_traj_phase1 = np.asarray(dlogL_traj_phase1)
        dlogL_key='psi'
        dlogL_idx = sampler.search_parameter_keys.index(dlogL_key)
        qpos_dlogL = np.append(qpos_traj_phase1, dlogL_traj_phase1[:, dlogL_idx].reshape(-1,1), axis=1)
        columns = sampler.search_trajectory_parameters_keys.copy()
        df_qpos_dlogL = pd.DataFrame(qpos_dlogL, columns=columns + [f'dlogL_d{dlogL_key}'])
        qpos_labels = CONST.get_latex_labels(columns)
        dlogL_label = CONST.get_dlogL_latex_labels([dlogL_key])
        parameter_labels = qpos_labels + [f'dlogL_d{dlogL_key}']
        result = gwresult.Result(
            search_parameter_keys=list(df_qpos_dlogL.columns),
            parameter_labels=parameter_labels,
            parameter_labels_with_unit=parameter_labels,
            posterior=df_qpos_dlogL,
            meta_data={}
            )
        filename = sampler.outdir + f'plots/corner_qpos_dlogL_train_{len(qpos_traj_phase1)}.png'
        fig_psi = result.plot_corner(save=False)
        # result.plot_corner(outdir=sampler.outdir + 'plots', filename=filename)

    def plot_hist_dlogL_phase1(self, dlogL_traj_phase1):
        import Library.plots as plots
        dlogL_traj_phase1 = np.asarray(dlogL_traj_phase1)
        outdir = self.outdir + 'plots/phase1'
        cut.check_directory_exists_and_if_not_mkdir(outdir)

        filename_with_outliers = outdir + '/dlogL_hists_outliers.png'
        title = 'Distribution of the gradients BEFORE removing outliers'
        plots.plot_hist_dlogL_phase1(dlogL_traj_phase1, self.search_trajectory_parameters_keys, filename_with_outliers, title=title)

        import scipy
        z = abs(scipy.stats.zscore(dlogL_traj_phase1, axis=0))
        dlogL_traj_phase1_no_outliers = dlogL_traj_phase1[(z < 4).all(axis=1)]

        filename_without_outliers = outdir + '/dlogL_hists_no_outliers.png'
        title = 'Distribution of the gradients AFTER removing outliers'
        plots.plot_hist_dlogL_phase1(dlogL_traj_phase1_no_outliers, self.search_trajectory_parameters_keys, filename_without_outliers, title=title)

        import subprocess
        filename_merge = outdir + '/dlogL_hists.png'
        subprocess.run(f'convert {filename_with_outliers} {filename_without_outliers} +append {filename_merge}', shell=True)
        subprocess.run(f'rm {filename_with_outliers} {filename_without_outliers}', shell=True)
