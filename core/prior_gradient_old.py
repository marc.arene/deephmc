import numpy as np
import bilby.core.prior as bcp
import bilby.gw.prior as bgp
import Library.likelihood_gradient as lg
import Library.param_utils as paru
import gw.prior as prior

class PriorGradient(object):
    def __init__(self, prior, offset=1e-7):
        self.prior = prior
        self.offset = offset
        self.prior_class = type(prior)
        self.log_prior_gradient_analytical_exists = self.prior_class in [bcp.analytical.Uniform, bcp.analytical.Sine, bcp.analytical.Cosine]

    def get_val_plus(self, val):
        return val + self.offset

    def get_val_minus(self, val):
        return val - self.offset

    def log_prior_gradient(self, val):
        val_plus = self.get_val_plus(val)
        val_minus = self.get_val_minus(val)
        if np.isnan(val_minus) or val_minus < self.prior.minimum:
            log_prior_gradient = self.log_prior_gradient_forward_diff(val, val_plus)
        elif np.isnan(val_plus) or val_plus > self.prior.maximum:
            log_prior_gradient = self.log_prior_gradient_backward_diff(val, val_minus)
        elif self.log_prior_gradient_analytical_exists:
            log_prior_gradient = self.log_prior_gradient_analytical(val)
        else:
            log_prior_gradient = self.log_prior_gradient_central_diff(val_minus, val_plus)
        return log_prior_gradient

    def log_prior_gradient_central_diff(self, val_minus, val_plus):
        log_prior_gradient = (self.prior.ln_prob(val_plus) - self.prior.ln_prob(val_minus)) / (2*self.offset)
        return log_prior_gradient

    def log_prior_gradient_forward_diff(self, val, val_plus):
        log_prior_gradient = (self.prior.ln_prob(val_plus) - self.prior.ln_prob(val)) / self.offset
        return log_prior_gradient

    def log_prior_gradient_backward_diff(self, val, val_minus):
        log_prior_gradient = (self.prior.ln_prob(val) - self.prior.ln_prob(val_minus)) / self.offset
        return log_prior_gradient

    def _log_prior_gradient_analytical(self, val):
        if self.prior_class == bcp.analytical.Uniform:
            res = 0
        elif self.prior_class == bcp.analytical.Sine:
            res = 1 / np.tan(val)
        elif self.prior_class == bcp.analytical.Cosine:
            res = -np.tan(val)
        else:
            pass
        return res

    def log_prior_gradient_analytical(self, val):
        return self._log_prior_gradient_analytical(val)

class TrajPriorGradient(PriorGradient):
    def __init__(self, prior, traj_func, offset=1e-7):
        super(TrajPriorGradient, self).__init__(prior, offset)
        self.traj_func = traj_func
        self.traj_inv_func = lg.map_func_to_inv_func(traj_func)

    def get_val_plus(self, val):
        traj_val = self.traj_func(val)
        traj_val_plus = traj_val + self.offset
        return self.traj_inv_func(traj_val_plus)

    def get_val_minus(self, val):
        traj_val = self.traj_func(val)
        traj_val_plus = traj_val - self.offset
        return self.traj_inv_func(traj_val_plus)

    def log_traj_jacobian_gradient(self, val):
        """
        Returns `dln(|1/f'(param)|)/df(param)` where `f = traj_func`.

        Explications:
        ------------
        `pi^{traj_param} = pi^{param} * |J_{traj_param -> param}|`, where pi^{traj_param} is the prior w.r.t. traj_param (= traj_func(param)) corresponding to the prior `pi^{param}` defined on param itself, and `J_{traj_param -> param}` is the determinant of the jacobian corresponding to the tranformation traj_param -> param; hence giving `J_{traj_param -> param} = dparam/dtraj_param = 1 / f'(val)` if `f = traj_func`.
        Hence this jacobian must be derived when taking the gradient of the log prior w.r.t. traj_param.
        """
        if self.traj_func == prior.identity:
            log_traj_jacobian_gradient = 0
        elif self.traj_func == np.log:
            log_traj_jacobian_gradient = 1
        elif self.traj_func == np.cos:
            log_traj_jacobian_gradient = np.cos(val) / np.sin(val)**2
        elif self.traj_func == np.sin:
            log_traj_jacobian_gradient = np.sin(val) / np.cos(val)**2
        else:
            raise ValueError(f'traj_func = {self.traj_func.__name__} but dln(1/|jacobian(param)|)/d{self.traj_func.__name__}(param) where jacobian = d{self.traj_func.__name__}(param)/d(param) has not been implemented yet.')
        return log_traj_jacobian_gradient

    def inverse_traj_func_derivative(self, val):
        """
        If f is the trajectory function, this returns 1/f'(val)
        """
        if self.traj_func == prior.identity:
            inverse_traj_func_derivative = 1
        elif self.traj_func == np.log:
            inverse_traj_func_derivative = val
        elif self.traj_func == np.cos:
            inverse_traj_func_derivative = -1 / np.sin(val)
        elif self.traj_func == np.sin:
            inverse_traj_func_derivative = 1 / np.cos(val)
        else:
            raise ValueError(f'traj_func = {self.traj_func.__name__} but dparam/d{self.traj_func.__name__}(param) has not been implemented yet.')
        return inverse_traj_func_derivative

    def log_prior_gradient_analytical(self, val):
        """
        The analytical formula for the gradient of the prior takes the derivative with respect to the parameter and not traj_func(parameter); hence we must account for dparam/dtraj_func(param) using the chain rule.
        """
        log_prior_gradient = self._log_prior_gradient_analytical(val)
        inverse_traj_func_derivative = self.inverse_traj_func_derivative(val)
        return log_prior_gradient * inverse_traj_func_derivative

    def _log_traj_prior_gradient(self, val):
        # If the prior asked on theta was Sine, that's equivalent to a prior Uniform in cos(theta); hence if the trajectory function used is cosine we know directly that the gradient of it's uniform prior is zero. Likewise for declination if prior on declination asked was in Cosine, meaning uniform in sin(dec).
        if self.traj_func == np.cos and self.prior_class == bcp.analytical.Sine:
            return 0
        elif self.traj_func == np.sin and self.prior_class == bcp.analytical.Cosine:
            return 0
        else:
            log_prior_gradient_wrt_trajparam = self.log_prior_gradient(val)
            log_traj_jacobian_gradient = self.log_traj_jacobian_gradient(val)
            return log_prior_gradient_wrt_trajparam + log_traj_jacobian_gradient

    def log_traj_prior_gradient(self, params_dict):
        val = params_dict[self.prior.name]
        return self._log_traj_prior_gradient(val)

class TrajPriorGradientForMasses(TrajPriorGradient):
    def __init__(self, prior, traj_func, prior_gradient_mass_1, prior_gradient_mass_2, offset=1e-7):
        super(TrajPriorGradientForMasses, self).__init__(prior, traj_func, offset)
        self.prior_gradient_mass_1 = prior_gradient_mass_1
        self.prior_gradient_mass_2 = prior_gradient_mass_2
        error1 = type(prior_gradient_mass_1) != bcp.analytical.Uniform
        error2 = type(prior_gradient_mass_2) != bcp.analytical.Uniform
        if error1 or error2:
            raise ValueError(f'Only uniform priors on component masses are handled for the moment.')

    def ln_mass_jacobian(self, mass_param_1, mass_param_2):
        chirp_mass = mass_param_1
        reduced_mass = mass_param_2
        if self.prior.name == 'reduced_mass':
            chirp_mass = mass_param_2
            reduced_mass = mass_param_1
        return np.log(prior.jacobian_mc_mu_to_m1_m2_from_mc_mu(chirp_mass, reduced_mass))

    def log_mass_jacobian_gradient(self, mass_param_tovary, mass_param_cst):
        mptv_plus = self.get_val_plus(mass_param_tovary)
        mptv_minus = self.get_val_minus(mass_param_tovary)
        cond_forward = cond_backward = False
        if self.prior.name == 'chirp_mass':
            # If eta = 0.25: a backward step in chirp_mass will give eta > 0.25 which is unphysical, hence we only use forward difference
            _, eta, _, _ = paru.Mc_and_mu_to_M_eta_m1_and_m2(mptv_minus, mass_param_cst)
            cond_forward = eta > 0.25
        else:
            # If eta = 0.25: a forward step in reduced mass will give eta > 0.25 which is unphysical, hence we only use backward difference
            _, eta, _, _ = paru.Mc_and_mu_to_M_eta_m1_and_m2(mass_param_cst, mptv_plus)
            cond_backward = eta > 0.25
        if np.isnan(mptv_minus) or mptv_minus < self.prior.minimum or cond_forward:
            log_mass_jacobian_gradient = self.log_mass_jacobian_gradient_forward_diff(mass_param_tovary, mptv_plus, mass_param_cst)
        elif np.isnan(mptv_plus) or mptv_plus > self.prior.maximum or cond_backward:
            log_mass_jacobian_gradient = self.log_mass_jacobian_gradient_backward_diff(mass_param_tovary, mptv_minus, mass_param_cst)
        # elif self.log_mass_jacobian_gradient_analytical_exists:
        #     log_mass_jacobian_gradient = self.log_mass_jacobian_gradient_analytical(mass_param_tovary, mass_param_cst)
        else:
            log_mass_jacobian_gradient = self.log_mass_jacobian_gradient_central_diff(mptv_minus, mptv_plus, mass_param_cst)
        return log_mass_jacobian_gradient

    def log_mass_jacobian_gradient_central_diff(self, mptv_minus, mptv_plus, mass_param_cst):
        log_mass_jacobian_gradient = (self.ln_mass_jacobian(mptv_plus, mass_param_cst) - self.ln_mass_jacobian(mptv_minus, mass_param_cst)) / (2*self.offset)
        return log_mass_jacobian_gradient

    def log_mass_jacobian_gradient_forward_diff(self, mass_param_tovary, mptv_plus, mass_param_cst):
        log_mass_jacobian_gradient = (self.ln_mass_jacobian(mptv_plus, mass_param_cst) - self.ln_mass_jacobian(mass_param_tovary, mass_param_cst)) / self.offset
        return log_mass_jacobian_gradient

    def log_mass_jacobian_gradient_backward_diff(self, mass_param_tovary, mptv_minus, mass_param_cst):
        log_mass_jacobian_gradient = (self.ln_mass_jacobian(mass_param_tovary, mass_param_cst) - self.ln_mass_jacobian(mptv_minus, mass_param_cst)) / self.offset
        return log_mass_jacobian_gradient

    def _log_traj_prior_gradient(self, mass_param_1, mass_param_2):
        log_prior_gradient_wrt_trajparam = 0 # Only true if uniform in mass_1 and mass_2
        log_mass_jacobian_gradient = self.log_mass_jacobian_gradient(mass_param_1, mass_param_2)
        log_traj_jacobian_gradient = self.log_traj_jacobian_gradient(mass_param_1)
        return log_prior_gradient_wrt_trajparam + log_mass_jacobian_gradient + log_traj_jacobian_gradient

    def log_traj_prior_gradient(self, mass_params_dict):
        if self.prior.name == 'chirp_mass':
            mass_param_1 = mass_params_dict['chirp_mass']
            mass_param_2 = mass_params_dict['reduced_mass']
        elif self.prior.name == 'reduced_mass':
            mass_param_1 = mass_params_dict['reduced_mass']
            mass_param_2 = mass_params_dict['chirp_mass']
        return self._log_traj_prior_gradient(mass_param_1, mass_param_2)

class GWPriorGradients(dict):
    def __init__(self, priors, search_parameter_keys, search_trajectory_functions, dict_trajectory_parameters_keys, dict_parameters_offsets, start_time, offset=1e-7):
        self.priors = priors
        self.search_parameter_keys = search_parameter_keys
        self.search_trajectory_functions = search_trajectory_functions
        self.dict_trajectory_parameters_keys = dict_trajectory_parameters_keys
        self.dict_parameters_offsets = dict_parameters_offsets
        self.start_time = start_time
        # self.traj_prior_gradients = {}
        for idx, key in enumerate(search_parameter_keys):
            traj_key = self.dict_trajectory_parameters_keys[key]
            traj_func = self.search_trajectory_functions[idx]
            offset = self.dict_parameters_offsets[key]
            if type(priors[key]) != bcp.base.Constraint:
                if key == 'geocent_time':
                    prior = priors[key].__class__(minimum=priors[key].minimum - start_time, maximum=priors[key].maximum - start_time)
                    prior.name = 'geocent_duration'
                    prior.latex_label = '$\\delta t_c$'
                    prior.unit = 's'
                    prior.boundary = 'reflective'
                else:
                    prior = priors[key]
                self[traj_key] = TrajPriorGradient(prior, traj_func, offset)
            elif key in ['chirp_mass', 'reduced_mass']:
                self[traj_key] = TrajPriorGradientForMasses(priors[key], traj_func, priors['mass_1'], priors['mass_2'], offset)
            else:
                raise ValueError(f'Prior on {key} should not be defined as a Constraint.')

    def calculate_log_prior_gradients(self, parameter_values):
        log_gradients = {}
        for lalsim_key in self.search_parameter_keys:
            if lalsim_key == 'geocent_time':
                parameter_values['geocent_duration'] = parameter_values['geocent_time'] - self.start_time
            traj_key = self.dict_trajectory_parameters_keys[lalsim_key]
            log_gradients[traj_key] = self[traj_key].log_traj_prior_gradient(parameter_values)
        return log_gradients

    def convert_dlogPi_dict_to_np_array(self, dlogPi_dict):
        dict_keys = dlogPi_dict.keys()
        dlogPi_np = np.zeros(len(dict_keys))
        for i, key in enumerate(dict_keys):
            dlogPi_np[i] = dlogPi_dict[key]

        return dlogPi_np

    def calculate_dlogPi_np(self, parameter_values):
        dlogPi_dict = self.calculate_log_prior_gradients(parameter_values)
        return self.convert_dlogPi_dict_to_np_array(dlogPi_dict)




def dJ_deta(eta):
    return -5/4 * eta**(-3/5) * (1/4 - eta)**(-1/2) * (3/5 * eta**-1 + 1/2 * (1/4 - eta)**-1)

def dJ_deta_2(eta):
    return 5/4 * (-3/5) * eta**(-8/5) * (1/4 - eta)**(-1/2) + 5/4 * eta**(-3/5) * (-1/2) * (1/4 - eta)**(-3/2)

def dJ_deta_num(eta):
    from gw.prior import jacobian_mc_mu_to_m1_m2_from_eta
    offset = 0.00001
    eta_p = eta + offset
    eta_m = eta - offset
    J_plus = jacobian_mc_mu_to_m1_m2_from_eta(eta_p)
    J_minus = jacobian_mc_mu_to_m1_m2_from_eta(eta_m)
    return (J_plus - J_minus) / (2*offset)

def deta_dmc(eta):
    return -5/2 * eta**(-7/2)

def deta_dmu(eta):
    return 5/2 * eta**(3/2)


# qpos_dict = sampler.q_pos_to_parameters_dict(q_pos)
# qpos_dict.pop('total_mass')
# qpos_dict.pop('symmetric_mass_ratio')
# priors.ln_prob(qpos_dict)
