import numpy as np
import core.utils as cut
import Library.BNS_HMC_Tools as bht
import time

# def next_stepsize_to_test(stepsizes, costs):
#     stepsizes_all = stepsizes.copy()
#     # Keep only the two best stepsizes
#     argsort_costs = np.argsort(costs)[:2]
#     stepsizes = stepsizes[argsort_costs]
#     costs = costs[argsort_costs]
#     # Order the stepsizes from smallest to biggest, and apply this ordering to their costs
#     argsort_stepsizes = np.argsort(stepsizes)
#     stepsizes = stepsizes[argsort_stepsizes]
#     costs = costs[argsort_stepsizes]
#     argsort_costs = np.argsort(costs)
#
#     idx_opt = argsort_costs[0]
#
#     # If the bigger stepsize of the two produced a acceptance rate too low, its cost is set to np.inf and the next stepsize to test
#     if costs[1] == np.inf:

def next_stepsize_to_test(stepsizes, costs):
    """
    From the list of `stepsizes` and their corresponding `costs` to produce an independent sample, returns the next stepsize value to test in the benchmark to get to the minimum.
    Follows a method ressembling the bisection method by using the 3 best stepsizes.
    """
    stepsizes_all = stepsizes.copy()
    # Keep only the three best stepsizes
    argsort_costs = np.argsort(costs)[:3]
    stepsizes = stepsizes[argsort_costs]
    costs = costs[argsort_costs]
    # Order the stepsizes from smallest to biggest, and apply this ordering to their costs
    argsort_stepsizes = np.argsort(stepsizes)
    stepsizes = stepsizes[argsort_stepsizes]
    costs = costs[argsort_stepsizes]
    argsort_costs = np.argsort(costs)

    idx_opt = argsort_costs[0]
    # If the biggest stepsize of the three considered produces the optimal cost, we are on the left side of the cost-parabola and the next stepsize to test should be of bigger value
    if idx_opt == 2:
        new_eps = stepsizes[2] * 2
        if new_eps in stepsizes_all:
            new_eps = stepsizes[2] * 1.5
            if new_eps in stepsizes_all:
                new_eps = stepsizes[2] * 1.25
                if new_eps in stepsizes_all:
                    new_eps = stepsizes[2] * 1.125
    # If the smallest stepsize of the three considered produces the optimal cost, we are on the right side of the cost-parabola and the next stepsize to test should be of smaller value
    elif idx_opt == 0:
        new_eps = stepsizes[0] / 2
        if new_eps in stepsizes_all:
            new_eps = stepsizes[2] / 1.5
            if new_eps in stepsizes_all:
                new_eps = stepsizes[2] / 1.25
                if new_eps in stepsizes_all:
                    new_eps = stepsizes[2] / 1.125
    # If the middle stepsize of the three considered produces the optimal cost, we are in the middle of the cost-parabola and the next stepsize to test should be either on the right or left side of this middle
    # This `else` is equivalent to `elif idx_opt == 1:`
    else:
        # If the second best is the smallest stepsize,
        if argsort_costs[1] == 0:
            new_eps = stepsizes[1] - (stepsizes[1] - stepsizes[0]) / 2
        else:
            new_eps = stepsizes[1] + (stepsizes[2] - stepsizes[1]) / 2
    cut.logger.info(f'The three stepsizes: {stepsizes} with costs: {costs}, lead to testing as new stepsize: {new_eps}')
    return  new_eps

def evaluate_cost_and_acc_for_eps(loc_eps, sampler, fit_method, oluts_fit, nsrt):

    epsilon_hyb_num = 5e-3
    length_traj_min = 50
    length_traj_stuck_min = 20
    traj_type_analytical = f'analy-{fit_method.nick_name}'
    traj_type_hybrid = 'hybrid'
    traj_type_numerical = 'numerical'
    count, n_traj_already_run, duration1, Acc, q_pos, dlogL, logL, randGen, _, _, _, count_phase3, duration2, duration3, atr, htr, ntr, atr_time, htr_time, ntr_time, stuck_count, hybrid_stuck, next_traj_to_check_sis  = sampler.get_state()
    sampler.set_scales_from_covariance_matrix()
    # oluts_fit = sampler.set_oluts_regressor() # For the moment, need to initialize it even if dnn is used...
    # if fit_method.nick_name == 'cubic':
    #     cut.logger.info('Creating the OLUTs from phase1 data...')
    #     oluts_fit.set_oluts(np.asarray(x_train), np.asarray(y_train))


    start = time.time()
    acc_eps = 1
    count_eps = 0
    stuck_count = 0
    hybrid_stuck = 0

    n_traj_for_cost = 800
    cut.logger.info(f'Running {n_traj_for_cost} trajectories with loc_epsilon = {loc_eps:.2e}, n_srt = {nsrt}')
    # HMC using analytical fitting values for the gradients of the log-likelihood
    for traj_index in range(1, n_traj_for_cost + 1):
        # Draw epsilon according to a Gaussian law limited between 0.001 and 0.01
        # rannum = randGen.normal(loc=0, scale=1.5)
        # epsilon = sampler.epsilon0 + rannum * 1.0e-3
        scale_eps = 0.3 * loc_eps
        epsilon = randGen.normal(loc=loc_eps, scale=scale_eps)
        # epsilon = randGen.normal(loc=sampler.epsilon0, scale=0.3 * sampler.epsilon0) * 1.0e-3
        if epsilon < loc_eps - 2 * scale_eps:
            epsilon = loc_eps - 2 * scale_eps
        if epsilon > loc_eps + 2 * scale_eps:
            epsilon = loc_eps + 2 * scale_eps

        if stuck_count < nsrt:
            length_traj = length_traj_min + int(randGen.uniform() * 100)
            q_pos, logL, dlogL, Accept, a, proba_acc = bht.AnalyticalGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, fit_method, oluts_fit, traj_index, sampler)
            atr += 1
            traj_type = traj_type_analytical
        	# Compute the acceptance rate along with the local acceptance rate
            if Accept == 1:
                traj_status = "** ACCEPTED **"
                stuck_count = 0
                count_eps += 1
                acc_eps = count_eps / atr
                hybrid_stuck = 0
            elif Accept == 0:
                traj_status = "   rejected   "
                stuck_count +=1
                acc_eps = count_eps / atr
                # acc_eps = count_eps / traj_index
                if traj_type == traj_type_hybrid:
                    hybrid_stuck += 1
        else:
            # if False:
            if hybrid_stuck == 0: # Try first a hybrid trajectory with fewer steps
                epsilon = epsilon_hyb_num
                length_traj = length_traj_stuck_min +  int(randGen.uniform() * 80)
                q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.HybridGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, fit_method, traj_index, sampler)
                traj_type = traj_type_hybrid
            else: # if hybrid trajectory did not work, switch to full  numericaltrajectory
                epsilon = epsilon_hyb_num
                length_traj = length_traj_stuck_min +  int(randGen.uniform() * 80)
                q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.NumericalGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, traj_index, sampler)
                traj_type = traj_type_numerical
            # Compute the acceptance rate and check whether the trajector was hybrid or numerical
            if Accept == 1:
                traj_status = "** ACCEPTED **"
                stuck_count = 0
                hybrid_stuck = 0
                # count_eps += 1
                # acc_eps = count_eps / traj_index
                if fit_method.nick_name == 'cubic':
                    oluts_fit.update_oluts(qpos_from_traj, dlogL_from_traj)
            elif Accept == 0:
                traj_status = "   rejected   "
                stuck_count += 1
                hybrid_stuck += 1
                # acc_eps = count_eps / traj_index

        if a < proba_acc:
            sup_or_inf = '<'
        else:
            sup_or_inf = '>'
        cut.logger.info("{:6} - {} - eps = {:5.2e} - Acc-ana = {:.1%} - {:11} traj - {:1.5f} {} {:1.5f} - len = {}".format(traj_index, traj_status, epsilon, acc_eps, traj_type, a, sup_or_inf, proba_acc, length_traj))

    stop = time.time()
    time_per_traj = (stop - start) / n_traj_for_cost
    if acc_eps == 0: # just to avoid a potential division by zero
        acc_eps = 0.001
    else:
        cost_eps = 1 /(acc_eps * loc_eps)
        cost_adhoc_eps = 1 /(acc_eps * (loc_eps - 8*loc_eps**2))
    cut.logger.info(f'Results with epsilon = {loc_eps:.2e}:\n  Acc = {acc_eps:.2%}\n  Cost_adhoc = {cost_adhoc_eps:.2f}\n  Cost = {cost_eps:.2f}\n  Average time per traj = {time_per_traj:.2f}')

    return cost_adhoc_eps, acc_eps
