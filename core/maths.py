import numpy as np
import itertools as it
import functools as fct

def get_combinations_for_vector(vector_x, exponent):
    """
    Returns all the possible combinations one has to make between
    the items of `vector_x` to produce the terms of order `exponent`.

    Parameters:
    ----------
    vector_x: array-like of size n
    exponent: power of the terms

    Returns:
    --------
    array: size = (n+e-1)! / e! / (n-1)! where e = exponent
        python list of exponent-uplets

    Examples:
    --------
    >> vector_x = ['q0', 'q1', 'q2']
    >> get_combinations_for_vector(vector_x, 2)
    [('q0', 'q0'),
     ('q0', 'q1'),
     ('q0', 'q2'),
     ('q1', 'q1'),
     ('q1', 'q2'),
     ('q2', 'q2')]

    >> vector_x = ['q0', 'q1']
    >> get_combinations_for_vector(vector_x, 3)
    [('q0', 'q0', 'q0'),
     ('q0', 'q0', 'q1'),
     ('q0', 'q1', 'q1'),
     ('q1', 'q1', 'q1')]
    """
    return list(it.combinations_with_replacement(vector_x, exponent))

def cubic_jacobian(vector_x):
    """
    Returns the list of all the terms part of the full cubic jacobian for vector_x.

    Parameters:
    ----------
    vector_x: array like of size n

    Returns:
    --------
    full jacobian of size 1 + n + n*(n+1)/2 + n*(n+1)*(n+2)/6

    Example:
    --------
    Imagining `a` and `b` are real numbers, the vector [a, b] of size
    2 will yield a cubic jacobian of size 10:
    >>> vector_x = [a, b]
    >>> cubic_jacobian(vector_x)
    [a**3, a**2 * b, a * b**2, b**3, a**2, a * b, b**2, a, b, 1]

    """
    # Creating all combinations of 2-uplets from vector_x
    comb_quadratic = np.asarray(get_combinations_for_vector(vector_x, 2))
    # Multiplying elements of each 2-uplets together produces all the
    # quadratic terms
    quadratic_terms = comb_quadratic[:, 0] * comb_quadratic[:, 1]

    # Similarly for the cubic terms using 3-uplets
    comb_cubic = np.asarray(get_combinations_for_vector(vector_x, 3))
    cubic_terms = comb_cubic[:, 0] * comb_cubic[:, 1] * comb_cubic[:, 2]

    # Combine everything to get the full cubic jacobian
    return np.concatenate((cubic_terms, quadratic_terms, vector_x , [1]))



######################################################################
# OTHER FUNCTIONS TRIED TO OPTIMIZE THE CODE OR MAKE IT MORE READABLE
# BUT NOT USED IN THE END
######################################################################

#
# def get_jacobian_terms_for_vector(vector_x, exponent):
#     combinations = np.asarray(get_combinations_for_vector(vector_x, exponent))
#     # jacobian_terms = combinations[:, 0]
#     # for e in range(1, exponent):
#     #     jacobian_terms *= combinations[:, e]
#     # return jacobian_terms
#     return fct.reduce(lambda a, b: a*b, combinations.T)
#
# def get_cubic_jacobian_for_vector(vector_x):
#     cubic_terms = get_jacobian_terms_for_vector(vector_x, 3)
#     quadratic_terms = get_jacobian_terms_for_vector(vector_x, 2)
#     all_terms = np.concatenate((cubic_terms, quadratic_terms, vector_x, [1]))
#     return all_terms
#
# def get_combinations_for_matrix(matrix, exponent):
#     return [get_combinations_for_vector(row, exponent) for row in matrix]
#
# def get_jacobian_terms_for_matrix(matrix, exponent):
#     combinations = np.asarray(get_combinations_for_matrix(matrix, exponent))
#     jacobian_terms = combinations[:, :, 0]
#     for e in range(1, combinations.shape[-1]):
#         jacobian_terms *= combinations[:, :, e]
#     return jacobian_terms
#     # iterable = [combinations[:, :, i] for i in range(combinations.shape[2])]
#     # return fct.reduce(lambda a, b: a*b, iterable)
#
# def get_cubic_jacobian_for_matrix(matrix):
#     cubic_terms = get_jacobian_terms_for_matrix(matrix, 3)
#     quadratic_terms = get_jacobian_terms_for_matrix(matrix, 2)
#     all_terms = np.concatenate((cubic_terms, quadratic_terms, matrix, np.ones((len(matrix), 1))), axis=1)
#     return all_terms




#
# def get_all_jacobian_terms_for_vector(vector, exponent):
#     pass
#
# def get_quadratic_jacobian_terms(vector):
#     quadratic_terms = get_jacobian_terms_for_vector(vector_x, 2)
#     return quadratic_terms
#
# def get_jacobian_terms_for_vector(vector_x, exponent):
#     if exponent == 0:
#         return [1]
#     cubic_terms = []
#     for i in range(len(vector)):
#         cubic_terms.append(vector[i] * get_jacobian_terms_for_vector(vector_x[i:], exponent - 1))
#     return
#
# def get_all_jacobian_terms(vector, exponent):
#     if exponent == 0:
#         return [1]
#     else:
#         res = [np.concatenate((vector[i] * get_all_jacobian_terms(vector[i:], exponent - 1))) for i in range(len(vector))]
#     return res
#
#
#
# def concat_for_reduce(arr1, arr2):
#     return np.concatenate((arr1, arr2))
#
#
# def get_cubic_jacobian(vector_x):
#     quadratic_terms = get_jacobian_terms_for_vector(vector_x, 2)
#     cubic_terms = []
#     # for i in range(len(vector_x)):
#     #     i_terms = vector_x[i] * get_jacobian_terms_for_vector(vector_x[i:], 2)
#     #     cubic_terms = np.concatenate((cubic_terms, i_terms))
#     cubic_terms = [vector_x[i] * get_jacobian_terms_for_vector(vector_x[i:], 2) for i in range(len(vector_x))]
#
#     return np.concatenate((cubic_terms, quadratic_terms, vector_x, [1]))
#
# def get_jacobian(vector_x, exponent):
#     if exponent == 1:
#         return vector_x
#
#     return
