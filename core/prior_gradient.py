import numpy as np
import bilby.core.prior as bcp
import bilby.gw.prior as bgp
import bilby.gw.conversion as bconv
import Library.param_utils as paru
import gw.prior as prior
import core.utils as cut

def analytical_gradient_exists(prior_class, traj_func):
    cond_1 = prior_class == bgp.AlignedSpin and traj_func == prior.identity
    return cond_1

def log_prior_gradient_analytical_aligned_spin(val, chi_max):
    return 1 / (val * np.log(abs(val / chi_max)))

def log_prior_gradient_analytical_aligned_spin_modified(val, chi_max):
    """
    Aligned spin priors defined with the "zprior" have a log gradient
    which diverge at chi = 0 (derive equation  (A7) of https://arxiv.org/abs/1805.10457).
    Hence we connect the two diverging branches by a smooth sinusoid taking
    effect between [-chi_low, chi_low].
    The sinusoid is set such that its extrema occur at +/- 0.9 * chi_low.
    The divergences at the boundaries is not handled here but by
    setting the boundary at 99% of the maximum value such that trajectory will bounce before the gradient diverges.
    """
    chi_low = 1e-3
    chi_low_max = 0.9 * chi_low
    if abs(val) <= chi_low:
        # val = (-1)**(val < 0) * chi_low
        norm = abs(log_prior_gradient_analytical_aligned_spin(chi_low, chi_max) / np.sin(np.pi/2 * chi_low / chi_low_max))
        res = -norm * np.sin((np.pi / 2) * val / chi_low_max)
    else:
        res = log_prior_gradient_analytical_aligned_spin(val, chi_max)
    return res

def log_prior_gradient_analytical_aligned_spin_tanh(val):
    """
    Another try to smooth out the diverging log-gradient of aligned spins.
    The sinusoid defined above gives a better acceptance rate.
    """
    chi_low = 2e-2
    chi_low_max = 0.9 * chi_low
    if abs(val) <= chi_low:
        res = -55.59 * np.tanh(117*val)
    else:
        res = log_prior_gradient_analytical_aligned_spin(val)
    return res


# FUNCTIONS FOR THE ANALYTICAL FORMULA OF THE GRADIENT OF THE LOG-JACOBIAN
# FOR MASS PARAMETERS, NOT USED AT THE MOMENT
def dlnJ_dlneta(eta):
    return -3/5 + 0.5 * eta / (0.25 - eta)

def dlnJ_dlnMc(mc, mu):
    eta = paru.Mc_mu_to_eta(mc, mu)
    return -5/2 * dlnJ_dlneta(eta)

def dlnJ_dlnmu(mc, mu):
    eta = paru.Mc_mu_to_eta(mc, mu)
    return 5/2 * dlnJ_dlneta(eta)

def log_traj_prior_gradient_mc_analytical(eta):
    """
    Formula only correct if uniform priors on component masses and if using the logarithm as trajectory function on chirp-mass and reduced-mass.
    """
    return -5/2 * dlnJ_dlneta(eta) + 1

def log_traj_prior_gradient_mu_analytical(eta):
    """
    Formula only correct if uniform priors on component masses and if using the logarithm as trajectory function on chirp-mass and reduced-mass.
    """
    return 5/2 * dlnJ_dlneta(eta) + 1


class TrajPriorGradient(object):
    def __init__(self, traj_prior, offset=1e-7):
        self.traj_prior = traj_prior
        self.offset = offset
        self.traj_func = traj_prior.traj_func
        self.traj_inv_func = prior.map_func_to_inv_func(self.traj_func)
        self.prior_class = type(self.traj_prior.prior)
        self.traj_prior_is_uniform_in_traj_func = self.set_traj_prior_is_uniform_in_traj_func()
        self.analytical_gradient_exists = analytical_gradient_exists(self.prior_class, self.traj_func)

    @property
    def name(self):
        return self.traj_prior.name

    def set_traj_prior_is_uniform_in_traj_func(self):
        # If the prior asked on theta was Sine, that's equivalent to a prior Uniform in cos(theta); hence if the trajectory function used is cosine we know directly that the gradient of it's uniform prior is zero. Likewise for declination if prior on declination asked was in Cosine, meaning uniform in sin(dec).
        possibility_1 = self.traj_func == np.cos and self.prior_class == bcp.analytical.Sine
        possibility_2 = self.traj_func == np.sin and self.prior_class == bcp.analytical.Cosine
        possibility_3 = self.traj_func == prior.identity and self.prior_class == bcp.analytical.Uniform
        return possibility_1 or possibility_2 or possibility_3

    def log_prior_gradient_analytical(self, val):
        res = None
        if self.prior_class == bgp.AlignedSpin and self.traj_func == prior.identity:
            res = log_prior_gradient_analytical_aligned_spin_modified(val, self.traj_prior.prior.maximum)
        return res


    def get_val_plus(self, val):
        return self.traj_inv_func(self.traj_func(val) + self.offset)

    def get_val_minus(self, val):
        return self.traj_inv_func(self.traj_func(val) - self.offset)

    def log_traj_prior_gradient_num(self, val):
        val_plus = self.get_val_plus(val)
        val_minus = self.get_val_minus(val)
        if np.isnan(val_minus) or val_minus < self.traj_prior.prior.minimum:
            log_prior_gradient = self.log_traj_prior_gradient_forward_diff(val, val_plus)
        elif np.isnan(val_plus) or val_plus > self.traj_prior.prior.maximum:
            log_prior_gradient = self.log_traj_prior_gradient_backward_diff(val, val_minus)
        else:
            log_prior_gradient = self.log_traj_prior_gradient_central_diff(val_minus, val_plus)
        return log_prior_gradient

    def log_traj_prior_gradient_central_diff(self, val_minus, val_plus):
        log_prior_gradient = (self.traj_prior._ln_prob(val_plus) - self.traj_prior._ln_prob(val_minus)) / (2*self.offset)
        return log_prior_gradient

    def log_traj_prior_gradient_forward_diff(self, val, val_plus):
        log_prior_gradient = (self.traj_prior._ln_prob(val_plus) - self.traj_prior._ln_prob(val)) / self.offset
        return log_prior_gradient

    def log_traj_prior_gradient_backward_diff(self, val, val_minus):
        log_prior_gradient = (self.traj_prior._ln_prob(val) - self.traj_prior._ln_prob(val_minus)) / self.offset
        return log_prior_gradient

    def log_traj_prior_gradient(self, params_dict):
        if self.traj_prior_is_uniform_in_traj_func:
            res = 0
        else:
            val = params_dict[self.traj_prior.prior.name]
            if self.analytical_gradient_exists:
                res = self.log_prior_gradient_analytical(val)
            else:
                res = self.log_traj_prior_gradient_num(val)
        return res

class TrajPriorGradientForMasses(object):
    def __init__(self, name, traj_combined_prior, lalsim_key, offset=1e-7):
        self.lalsim_key = lalsim_key
        self.name = name
        self.traj_combined_prior = traj_combined_prior
        if lalsim_key == 'chirp_mass':
            self.traj_func = self.traj_combined_prior.traj_func_mc
            self.prior_minimum = self.traj_combined_prior.mc_mu_combined_prior.prior_mc.minimum
            self.prior_maximum = self.traj_combined_prior.mc_mu_combined_prior.prior_mc.maximum
        elif lalsim_key == 'reduced_mass':
            self.traj_func = self.traj_combined_prior.traj_func_mu
            self.prior_minimum = self.traj_combined_prior.mc_mu_combined_prior.prior_mu.minimum
            self.prior_maximum = self.traj_combined_prior.mc_mu_combined_prior.prior_mu.maximum
        else:
            raise ValueError(f'TrajPriorGradientForMasses not coded yet for parameter {lalsim_key}')
        self.traj_inv_func = prior.map_func_to_inv_func(self.traj_func)
        self.offset = offset

    def get_val_plus(self, val):
        return self.traj_inv_func(self.traj_func(val) + self.offset)

    def get_val_minus(self, val):
        return self.traj_inv_func(self.traj_func(val) - self.offset)

    def log_traj_prior_gradient_num(self, mass_param_tovary, mass_param_cst):
        mptv_plus = self.get_val_plus(mass_param_tovary)
        mptv_minus = self.get_val_minus(mass_param_tovary)
        cond_forward = cond_backward = False
        if self.lalsim_key == 'chirp_mass':
            # If eta = 0.25: a backward step in chirp_mass will give eta > 0.25 which is unphysical, hence we only use forward difference
            _, eta, _, _ = paru.Mc_and_mu_to_M_eta_m1_and_m2(mptv_minus, mass_param_cst)
            cond_forward = eta > 0.25
        else:
            # If eta = 0.25: a forward step in reduced mass will give eta > 0.25 which is unphysical, hence we only use backward difference
            _, eta, _, _ = paru.Mc_and_mu_to_M_eta_m1_and_m2(mass_param_cst, mptv_plus)
            cond_backward = eta > 0.25
        if np.isnan(mptv_minus) or mptv_minus < self.prior_minimum or cond_forward:
            log_traj_prior_gradient = self.log_traj_prior_gradient_forward_diff(mass_param_tovary, mptv_plus, mass_param_cst)
        elif np.isnan(mptv_plus) or mptv_plus > self.prior_maximum or cond_backward:
            log_traj_prior_gradient = self.log_traj_prior_gradient_backward_diff(mass_param_tovary, mptv_minus, mass_param_cst)
        else:
            log_traj_prior_gradient = self.log_traj_prior_gradient_central_diff(mptv_minus, mptv_plus, mass_param_cst)
        return log_traj_prior_gradient

    def log_traj_prior_gradient_central_diff(self, mptv_minus, mptv_plus, mass_param_cst):
        mc_mu_plus = self.get_dict_from_massvarying_masscst(mptv_plus, mass_param_cst)
        mc_mu_minus = self.get_dict_from_massvarying_masscst(mptv_minus, mass_param_cst)
        log_traj_prior_gradient = (self.traj_combined_prior.ln_prob(mc_mu_plus) - self.traj_combined_prior.ln_prob(mc_mu_minus)) / (2*self.offset)
        return log_traj_prior_gradient

    def log_traj_prior_gradient_forward_diff(self, mass_param_tovary, mptv_plus, mass_param_cst):
        mc_mu_plus = self.get_dict_from_massvarying_masscst(mptv_plus, mass_param_cst)
        mc_mu = self.get_dict_from_massvarying_masscst(mass_param_tovary, mass_param_cst)
        log_traj_prior_gradient = (self.traj_combined_prior.ln_prob(mc_mu_plus) - self.traj_combined_prior.ln_prob(mc_mu)) / self.offset
        return log_traj_prior_gradient

    def log_traj_prior_gradient_backward_diff(self, mass_param_tovary, mptv_minus, mass_param_cst):
        mc_mu_minus = self.get_dict_from_massvarying_masscst(mptv_minus, mass_param_cst)
        mc_mu = self.get_dict_from_massvarying_masscst(mass_param_tovary, mass_param_cst)
        log_traj_prior_gradient = (self.traj_combined_prior.ln_prob(mc_mu) - self.traj_combined_prior.ln_prob(mc_mu_minus)) / self.offset
        return log_traj_prior_gradient

    def get_dict_from_massvarying_masscst(self, mass_varying, mass_param_cst):
        if self.lalsim_key == 'chirp_mass':
            dict = {'chirp_mass': mass_varying, 'reduced_mass': mass_param_cst}
        else:
            dict = {'chirp_mass': mass_param_cst, 'reduced_mass': mass_varying}
        return dict

    def log_traj_prior_gradient(self, mass_params_dict):
        chirp_mass = mass_params_dict['chirp_mass']
        reduced_mass = mass_params_dict['reduced_mass']

        # NUMERICAL COMPUTATION OF THE GRADIENT
        # chirp_mass, reduced_mass = self.limit_m2_value(mass_params_dict['chirp_mass'], mass_params_dict['reduced_mass'])
        # if self.lalsim_key == 'chirp_mass':
        #     mass_param_tovary = chirp_mass
        #     mass_param_cst = reduced_mass
        # elif self.lalsim_key == 'reduced_mass':
        #     mass_param_tovary = reduced_mass
        #     mass_param_cst = chirp_mass
        #
        # log_traj_pi_grad = self.log_traj_prior_gradient_num(mass_param_tovary, mass_param_cst)
        # if limit_mass_2:
        #     print(f'                         LIMITING VALUE OF m2 to {thresh}m1. dlnPi/dln({self.lalsim_key})={log_traj_pi_grad:.2f}')

        # ANALYTICAL COMPUTATION OF THE GRADIENT: works only if uniform priors on component masses and log as trajectory function for mc and mu
        eta = paru.Mc_mu_to_eta(chirp_mass, reduced_mass)
        # with q = 0.96, eta_max = 0.24989587671803418
        eta_max = bconv.component_masses_to_symmetric_mass_ratio(1, 0.96)
        eta = min(eta_max, eta)
        if self.lalsim_key == 'chirp_mass':
            log_traj_pi_grad = log_traj_prior_gradient_mc_analytical(eta)
        elif self.lalsim_key == 'reduced_mass':
            log_traj_pi_grad = log_traj_prior_gradient_mu_analytical(eta)
        # if eta == eta_max:
        #     cut.logger.info(f'                                 Limiting eta to {eta_max:.4f} for gradient computation. dlnPi/dln({self.lalsim_key})={log_traj_pi_grad:.2f}')
        return log_traj_pi_grad
        # return self.log_traj_prior_gradient_num(mass_param_tovary, mass_param_cst)

    def limit_m2_value(self, chirp_mass, reduced_mass):
        """
        The Jacobian of the transformation (mc, mu) -> (m1, m2) diverges on the equal mass boundary, thus so does the gradient of the prior on (mc, mu) when defined from prior on (m1, m2). Hence if getting to close to the m1=m2 line, we limit the value of m2 to 96% that of m1 in order to cap the gradient value.

        PROBLEM WHEN USING THIS METHOD: the new chirp_mass computed from `mass2=thres*mass_1` might be out of the prior boundaries set on chirp_mass, thus when computing numerically this gradient, a `(-inf - (-inf))/offset = nan` will be returned and the trajectory will be rejected.
        """
        mass_1, mass_2 = paru.Mc_and_mu_to_m1_and_m2(chirp_mass, reduced_mass)
        thresh = 0.96
        if mass_2 > thresh * mass_1:
            print(f'         LIMITING VALUE OF m2 to {thresh}m1')
            mass_2 = thresh * mass_1
            chirp_mass = bconv.component_masses_to_chirp_mass(mass_1, mass_2)
            reduced_mass = paru.component_masses_to_reduced_mass(mass_1, mass_2)
        return chirp_mass, reduced_mass

class GWTrajPriorGradients(dict):
    def __init__(self, traj_priors, dict_parameters_offsets, offset=1e-7):
        self.traj_priors = traj_priors
        self.search_parameter_keys = traj_priors.search_parameter_keys
        self.search_trajectory_parameters_keys = traj_priors.search_trajectory_parameters_keys
        # self.dict_trajectory_parameters_keys = dict_trajectory_parameters_keys
        self.dict_parameters_offsets = dict_parameters_offsets
        # self.start_time = traj_priors.start_time
        # self.traj_prior_gradients = {}
        for idx, traj_key in enumerate(self.search_trajectory_parameters_keys):
            if traj_key in self.traj_priors.combined_key:
                traj_prior = self.traj_priors[self.traj_priors.combined_key]
                lalsim_key = self.search_parameter_keys[idx]
                self[traj_key] = TrajPriorGradientForMasses(traj_key, traj_prior, lalsim_key)
            else:
                traj_prior = self.traj_priors[traj_key]
                self[traj_key] = TrajPriorGradient(traj_prior)

    def log_traj_prior_gradients(self, parameters_dict):
        log_traj_prior_gradients = {}
        for traj_key in self:
            # if traj_key in ['chi_1', 'chi_2']:
            #     log_traj_prior_gradients[traj_key] = 0
            # else:
            #     log_traj_prior_gradients[traj_key] = self[traj_key].log_traj_prior_gradient(parameters_dict)
            log_traj_prior_gradients[traj_key] = self[traj_key].log_traj_prior_gradient(parameters_dict)
        return log_traj_prior_gradients

    def convert_dlogPi_dict_to_np_array(self, dlogTrajPi_dict):
        dict_keys = dlogTrajPi_dict.keys()
        dlogPi_np = np.zeros(len(dict_keys))
        for i, key in enumerate(dict_keys):
            dlogPi_np[i] = dlogTrajPi_dict[key]

        return dlogPi_np

    def calculate_dlogPi_np(self, parameters_dict):
        dlogTrajPi_dict = self.log_traj_prior_gradients(parameters_dict)
        return self.convert_dlogPi_dict_to_np_array(dlogTrajPi_dict)




def dJ_deta(eta):
    return -5/4 * eta**(-3/5) * (1/4 - eta)**(-1/2) * (3/5 * eta**-1 + 1/2 * (1/4 - eta)**-1)

def dJ_deta_2(eta):
    return 5/4 * (-3/5) * eta**(-8/5) * (1/4 - eta)**(-1/2) + 5/4 * eta**(-3/5) * (-1/2) * (1/4 - eta)**(-3/2)

def dJ_deta_num(eta):
    from gw.prior import jacobian_mc_mu_to_m1_m2_from_eta
    offset = 0.00001
    eta_p = eta + offset
    eta_m = eta - offset
    J_plus = jacobian_mc_mu_to_m1_m2_from_eta(eta_p)
    J_minus = jacobian_mc_mu_to_m1_m2_from_eta(eta_m)
    return (J_plus - J_minus) / (2*offset)

def deta_dmc(eta):
    return -5/2 * eta**(-7/2)

def deta_dmu(eta):
    return 5/2 * eta**(3/2)


# qpos_dict = sampler.q_pos_to_parameters_dict(q_pos)
# qpos_dict.pop('total_mass')
# qpos_dict.pop('symmetric_mass_ratio')
# priors.ln_prob(qpos_dict)
