import numpy as np
import core.utils as cut


with_lalinf = True
with_emcee = False
with_homemade = False
try:
    import lalinference.bayespputils as lalinfbut
except ModuleNotFoundError:
    with_lalinf = False
    try:
        import emcee
        with_emcee = True
        print('WARNING: could not find `lalinference.bayespputils` module, thus using `emcee` module to compute the autocorrelation.')
    except ModuleNotFoundError:
        with_homemade = True
        print('WARNING: could not find `lalinference.bayespputils` nor `emcee` modules, thus using homemade functions to compute the autocorrelation.')

# from lalinference.bayespputils import autocorrelation, autocorrelation_length_estimate

if with_lalinf:
    def autocorrelation(chain):
        return lalinfbut.autocorrelation(chain)

    def autocorrelation_length_estimate(chain, acf=None, M=5, K=2):
        try:
            L = lalinfbut.autocorrelation_length_estimate(chain, acf=acf, M=M, K=K)
        except lalinfbut.ACLError as e:
            cut.logger.warning(f'Could not compute autocorrelation length on the chain for because of the following error:\n{e}\nHence returning L={len(chain)}')
            L = len(chain)
        return L
elif with_emcee:
    def autocorrelation(chain):
        return emcee.autocorr.function_1d(chain)

    def autocorrelation_length_estimate(chain):
        length = 1
        rho = autocorrelation(chain)
        # rho[0] corresponds to rho[lag=1]
        for lag_minus_one in range(len(rho)):
            previous_length = length
            length += 2 * rho[lag_minus_one]
            if 1 - previous_length/length < 0.01 and rho[lag_minus_one] < 0.001:
                return length
                # return length, lag_minus_one + 1
        raise ValueError('The integrated autocorrelation length could not converge to a maximum lags')
else:
    def homemade_autocorrelation_for_lag(array, lag):
        """
        "Home-made" version of lalinfbut.autocorrelation() or
        emcee.autocorr.function_1d(), DRASTICALLY slower than those two
        functions but giving the same result.
        I leave it here for a better understanding of how autocorrelation
        is computed.
        """
        if lag == 0:
            return 1
        else:
            mean = array.mean()
            array_minus_mean = array - mean
            array_numerator = array_minus_mean[:-lag] * array_minus_mean[lag:]
            array_denominator = array_minus_mean**2
            num = array_numerator.sum()
            den = array_denominator.sum()
            return num / den

    def autocorrelation(chain):
        cut.logger.warning('Computing autocorrelation without lalinf or emcee package, this might be very very slow...')
        rho = []
        for lag in range(len(chain)):
            rho.append(homemade_autocorrelation_for_lag(chain, lag))
        rho = np.asarray(rho)
        # remove the first value, equal to 1, corresponding to no lag
        return rho[1:]


def get_maximum_integrated_time(samples):
    acl = []
    for chain in samples.T:
        acl.append(autocorrelation_length_estimate(chain))
    L = int(max(acl)) + 1
    return L

def get_sis(samples):
    L = get_maximum_integrated_time(samples)
    return samples[::L]
