import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess
import pandas as pd

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf
import Library.initialization as init
import Library.psd_utils as psd_utils

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as CdlogL
import Codes.FIM as FIM

import gw.detector.networks as gwdn
import core.sampler
import core.utils as cut

if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--inj_file", default='../examples/injection_files/GW170817.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--event", default=None, action="store", type="string", help="""Event label part of the catalog GWTC-1""")
    parser.add_option("--trigger_file", default=None, action="store", type="string", help="""File .ini describing the parameters where to start the chain. If left to None and --event used, a default file corresponding to the event will be used.""")
    parser.add_option("--trigger_time", default=None, action="store", type="string", help="""Trigger GPS time.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""Print out information of interest during the run.""",dest='verbose')
    parser.add_option("--plot_traj", default=False, action="store_true", help="""Will save positions, momentas, hamiltonian values etc of every point along every trajectory in phase to be able to plot them with `plot_trajectories.py` script.""")
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")
    parser.add_option("--fit_method", default='cubic', action="store", type="string", help="""Method used to fit the numerical gradients of the log likelihood accumulated in phase1. """)
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")
    parser.add_option("--on_CIT", default=False, action="store_true", help="""Set this to True if running this script on Caltech's cluster. Will set the right path for the ROQ basis.""")

    # randGen = np.random.RandomState(seed=2)

    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()

    opts_dict_formatted = cut.dictionary_to_formatted_string(opts.__dict__)


    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)

    RUN_CONST.plot_traj = opts.plot_traj
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    # Check whether the run is on an injection, an event from the catalog or a trigger time
    if opts.event is not None:
        data_name = opts.event + 'real'
    elif opts.inj_file is not None:
        inj_file_name = opts.inj_file.split('/')[-1]
        data_name = inj_file_name.split('.')[0] + 'inj'
    elif opts.trigger_time is not None:
        data_name = opts.trigger_time
    else:
        raise ValueError('You must specify at least one of the 3 options: --event, --inj_file, --trigger_time')


    # Parse the inputs relative to the sampler and the search
    # sampler_dict = init.get_sampler_dict(config_dict)
    sampler_dict = pu.json_to_dict(config_dict['hmc']['config_hmc_parameters_file'])
    sampler_dict_formatted = cut.dictionary_to_formatted_string(sampler_dict)



    if opts.event is not None:

        if opts.trigger_file is None:
            trigger_file = '../examples/trigger_files/' + opts.event + '.ini'
        else:
            trigger_file = opts.trigger_file
        trigger_parameters = set_inj.ini_file_to_dict(trigger_file)
        # geocent_time = bilby.gw.utils.get_event_time(opts.event)
        # trigger_parameters['geocent_time'] = geocent_time
        ext_analysis_dict = set_inj.compute_extended_analysis_dict(trigger_parameters['mass_1'], trigger_parameters['mass_2'], trigger_parameters['geocent_time'], trigger_parameters['chirp_mass'], **config_dict['analysis'])

        trig_dict_formatted = cut.dictionary_to_formatted_string(trigger_parameters)
        ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)

        start_time = ext_analysis_dict['start_time']
        duration = ext_analysis_dict['duration']

        hdf5_file_path_prefix = '../__input_data/' + opts.event
        if not(os.path.exists(hdf5_file_path_prefix)):
            cut.logger.error(f'The folder given to look for hdf5 strain files of the event: {hdf5_file_path_prefix}, does not exist.')
            sys.exit(1)

        GWTC1_event_PSDs_file = f'../__input_data/{opts.event}/GWTC1_{opts.event}_PSDs.dat'
        GWTC1_event_PSDs = np.loadtxt(GWTC1_event_PSDs_file)

        # INITIALIZE THE THREE INTERFEROMETERS
        # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
        interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])

        # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
        for i, ifo in enumerate(interferometers):
            ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
            ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']

            # Should look like: '../__input_data/GW170817/H-H1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
            if opts.event == 'GW150914':
                hdf5_file_path = f'{hdf5_file_path_prefix}/{ifo.name[0]}-{ifo.name}_GWOSC_4KHZ_R1-1126259447-32.hdf5'
            if opts.event == 'GW170817':
                hdf5_file_path = f'{hdf5_file_path_prefix}/{ifo.name[0]}-{ifo.name}_LOSC_CLN_4_V1-1187007040-2048.hdf5'

            hdf5_strain, hdf5_start_time, hdf5_duration, sampling_frequency, hdf5_number_of_samples = psd_utils.get_strain_from_hdf5(hdf5_file_path)

            if sampling_frequency != ext_analysis_dict['sampling_frequency']:
                cut.logger.error('Sampling_frequency conflict between hdf5 file = {} and that in the config file = {}'.format(sampling_frequency, ext_analysis_dict['sampling_frequency']))
                sys.exit(1)

            idx_segment_start = int((start_time - hdf5_start_time) * sampling_frequency) + 1
            idx_segment_end = int((start_time + duration - hdf5_start_time) * sampling_frequency)
            desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end+1]
            strain = bilby.gw.detector.strain_data.InterferometerStrainData(minimum_frequency=ifo.minimum_frequency)
            strain.set_from_time_domain_strain(time_domain_strain=desired_hdf5_strain, sampling_frequency=sampling_frequency, duration=duration)
            # strain.low_pass_filter(filter_freq=ifo.minimum_frequency)

            ifo.strain_data = strain

            ifo.strain_data.start_time = start_time

            # SET THE PSDs
            ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_array=GWTC1_event_PSDs[:, i+1], frequency_array=GWTC1_event_PSDs[:, 0])

            ifo.psd_array = ifo.power_spectral_density_array
            ifo.inverse_psd_array = 1 / ifo.psd_array

        # WAVEFORM GENERATION
        waveform_arguments = {}
        waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
        waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
        waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
        waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']

        starting_point_parameters = trigger_parameters.copy()

        del hdf5_strain, GWTC1_event_PSDs

    elif opts.inj_file is not None:
        # SET THE INJECTION PARAMETERS AND THE ANALYSIS PARAMETERS
        injection_parameters, ext_analysis_dict = set_inj.get_inj_parameters_and_analysis_dict(opts.inj_file, **config_dict['analysis'])
        inj_dict_formatted = cut.dictionary_to_formatted_string(injection_parameters)
        cut.logger.info(f'Injected parameters derived from {opts.inj_file} are: {inj_dict_formatted}')
        ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)
        cut.logger.info(f'Analysis parameters derived from the component masses are: {ext_anal_dict_formatted}')

        start_time = ext_analysis_dict['start_time']

        # INITIALIZE THE THREE INTERFEROMETERS
        # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
        interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])
        # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
        for ifo in interferometers:
            ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
            ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']
            # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
            # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
            # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
            ifo.strain_data.duration = ext_analysis_dict['duration']
            ifo.strain_data.sampling_frequency = ext_analysis_dict['sampling_frequency']
            ifo.strain_data.start_time = start_time


        # SET THEIR POWER SPECTRAL DENSITIES
        # This function does not interpolate the psd
        set_psds.main(interferometers, opt_psd=config_dict['analysis']['psd'], sampling_frequency=ext_analysis_dict['sampling_frequency'], duration=ext_analysis_dict['duration'], start_time=start_time)


        # SET NOISE STRAIN DATA FROM PSD
        interferometers.set_strain_data_from_power_spectral_densities(
            sampling_frequency=ext_analysis_dict['sampling_frequency'],
            duration=ext_analysis_dict['duration'],
            start_time=start_time)

        # WAVEFORM GENERATION
        waveform_arguments = {}
        waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
        waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
        waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
        waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']
        injection_waveform_generator = bilby.gw.WaveformGenerator(
            duration=interferometers.duration,
            sampling_frequency=interferometers.sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
            waveform_arguments=waveform_arguments
            )

        # INJECT TEMPLATE INTO NOISE STRAIN DATA
        # Add the template signal to each detector's strain data
        # for i in range(len(interferometers)):
            # interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
            # interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain
        interferometers.inject_signal(parameters=injection_parameters, waveform_generator=injection_waveform_generator)

        # # COMPUTE AND PRINT SNR
        interferometers.optimal_snr()
        interferometers.matched_filter_snr()
        interferometers.log_likelihood()

        starting_point_parameters = injection_parameters.copy()
    elif opts.trigger_time is not None:
        pass


    for ifo in interferometers:
        ifo.fd_strain = ifo.strain_data.frequency_domain_strain


    # SET THE LIKELIHOOD OBJECT, two different cases whether ROQ basis is used or not
    if False:
        pass
    else:
        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        # priors = None
        priors = bilby.gw.prior.BNSPriorDict()

        frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
        if opts.event == 'GW150914':
            frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
            priors = bilby.gw.prior.BBHPriorDict()
        # make waveform generator
        # waveform_arguments['waveform_approximant'] = 'IMRPhenomPv2_NRTidal'
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=frequency_domain_source_model,
            waveform_arguments=waveform_arguments,
            parameter_conversion=parameter_conversion)

        likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
            interferometers=interferometers,
            waveform_generator=search_waveform_generator,
            priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])



    geocent_times = np.arange(1187008882.42, 1187008882.52, 0.0001)

    mf_snr_geocent_times = []
    mf_snr_phm_geocent_times = []
    for ii, geocent_time in enumerate(geocent_times):
        # Params from the 'maxL' column of the following run: https://ldas-jobs.ligo.caltech.edu/~soichiro.morisaki/O2/G298048/C02_2/lalinferencemcmc/IMRPhenomPv2_NRTidal/1187008882.45-0/V1H1L1/posplots.html
        # params_maxL = dict(chirp_mass=1.19755068423, mass_ratio=0.679399681207, luminosity_distance=19.8252382813, theta_jn=1.98489360594, psi=0.0617925754849, phase=2.34555197732, geocent_time=geocent_time, ra=3.44616, dec=-0.408084, a_1=0.045465883951, a_2=0.00339714665984, tilt_1=0.855949618025, tilt_2=1.46893080078, phi_12=0.80747942947, phi_jl=1.87728113831, lambda_tilde=157.389402093, delta_lambda_tilde=-14.8307681662)

        # Using chirp_mass and mass_ratio derived from `m1_source` and `m2_source`
        # params_maxL = dict(chirp_mass=1.1922157304867484, mass_ratio=0.6793996812058465, luminosity_distance=19.8252382813, theta_jn=1.98489360594, psi=0.0617925754849, phase=2.34555197732, geocent_time=geocent_time, ra=3.44616, dec=-0.408084, a_1=0.045465883951, a_2=0.00339714665984, tilt_1=0.855949618025, tilt_2=1.46893080078, phi_12=0.80747942947, phi_jl=1.87728113831, lambda_tilde=157.389402093, delta_lambda_tilde=-14.8307681662)

        # likelihood.parameters.update(params_maxL)


        # Using values reported in papers
        likelihood.parameters = starting_point_parameters.copy()
        likelihood.parameters.update(dict(geocent_time=geocent_time))


        # COMPUTE AND PRINT SNR
        waveform_polarizations = likelihood.waveform_generator.frequency_domain_strain(likelihood.parameters)
        snr_dict = {}
        d_inner_h_network = 0
        h_inner_h_network = 0
        mf_snr_network_sqrd = 0

        for ifo in interferometers:
            per_detector_snr = likelihood.calculate_snrs(waveform_polarizations, ifo)
            snr_dict[ifo.name] = {}
            snr_dict[ifo.name]['d_inner_h'] = per_detector_snr.d_inner_h
            snr_dict[ifo.name]['d_inner_h_phm'] = likelihood._bessel_function_interped(abs(per_detector_snr.d_inner_h))
            snr_dict[ifo.name]['h_inner_h'] = np.real(per_detector_snr.optimal_snr_squared)
            snr_dict[ifo.name]['logL'] = np.real(per_detector_snr.d_inner_h) - 0.5 * np.real(per_detector_snr.optimal_snr_squared)
            snr_dict[ifo.name]['complex_matched_filter_snr'] = per_detector_snr.complex_matched_filter_snr
            snr_dict[ifo.name]['matched_filter_snr_phm'] = snr_dict[ifo.name]['d_inner_h_phm'] / snr_dict[ifo.name]['h_inner_h']**0.5

            d_inner_h_network += per_detector_snr.d_inner_h
            h_inner_h_network += np.real(per_detector_snr.optimal_snr_squared)
            mf_snr_network_sqrd += np.real(per_detector_snr.complex_matched_filter_snr)**2

        snr_dict['network'] = {}
        snr_dict['network']['logL_ratio'] = np.real(d_inner_h_network) - 0.5 * h_inner_h_network
        snr_dict['network']['mf_snr'] = mf_snr_network_sqrd**0.5
        snr_dict['network']['mf_snr_phm'] = likelihood._bessel_function_interped(abs(d_inner_h_network)) / h_inner_h_network**0.5
        snr_dict['network']['opt_snr'] = h_inner_h_network**0.5
        mf_snr_geocent_times.append(snr_dict['network']['mf_snr'])
        mf_snr_phm_geocent_times.append(snr_dict['network']['mf_snr_phm'])
        # if ii == 45 or ii == 52:
        #     # 45      : geocent_time = 1187008882.4294953
        #     # 52      : geocent_time = 1187008882.4301946
        #     for k in snr_dict.keys():
        #         print(f'{k}')
        #         for kk in snr_dict[k].keys():
        #             print(f'  {kk} = {snr_dict[k][kk]}')
        #     import IPython; IPython.embed();

    mf_snr_geocent_times = np.asarray(mf_snr_geocent_times)
    mf_snr_phm_geocent_times = np.asarray(mf_snr_phm_geocent_times)
    print(f'mf_snr_geocent_times.max() = {mf_snr_geocent_times.max()} at index {mf_snr_geocent_times.argmax()}')
    print(f'mf_snr_phm_geocent_times.max() = {mf_snr_phm_geocent_times.max()} at index {mf_snr_phm_geocent_times.argmax()}')


        # snr_dict_formatted = cut.dictionary_to_formatted_string(snr_dict)
        # cut.logger.info(f'SNR at starting point: {snr_dict_formatted}')
    import IPython; IPython.embed(); sys.exit()
