import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess
import pandas as pd

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf
import Library.initialization as init
import Library.psd_utils as psd_utils

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as CdlogL
import Codes.FIM as FIM

import gw.detector.networks as gwdn
import core.sampler
import core.utils as cut

def q_pos_to_parameters_dict(q_pos):
    """
    Converts the array q_pos containing the position values of a point in the search parameter space to the corresponding dictionary. The correspondance between a key and the index is known through sampler.search_parameter_keys
    """
    search_parameter_keys = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'dec', 'ra', 'geocent_duration', 'chi_1', 'chi_2', 'lambda_1', 'lambda_2']
    start_time = 1187008820.43
    # search_parameter_keys = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'mass_ratio', 'dec', 'ra', 'geocent_time', 'chi_1', 'chi_2', 'lambda_1', 'lambda_2']
    # search_parameter_keys = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'mass_ratio', 'symmetric_mass_ratio', 'mass_1', 'mass_2', 'dec', 'ra', 'geocent_time', 'chi_1', 'chi_2', 'chi_eff', 'lambda_1', 'lambda_2', 'matched_filter_snr', 'logL', 'redshift', 'comoving_distance', 'mass_1_source', 'mass_2_source', 'chirp_mass_source', 'reduced_mass_source', 'lambda_tilde', 'delta_lambda_tilde']
    # search_parameter_keys = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'mass_ratio', 'symmetric_mass_ratio', 'mass_1', 'mass_2', 'dec', 'ra', 'geocent_time', 'chi_1', 'chi_2', 'chi_eff', 'lambda_1', 'lambda_2', 'matched_filter_snr', 'logL', 'redshift', 'comoving_distance', 'mass_1_source', 'mass_2_source', 'chirp_mass_source', 'reduced_mass_source']
    parameters_dict = {}
    for idx, key in enumerate(search_parameter_keys):
        parameters_dict[key] = q_pos[idx]

    # Since we actually sample the 'geocent_duration' (for numerical precision reasons) we need to add the start_time of the strain_data to recover the gps time: 'geocent_time' needed as an input in lalinference
    # if 'geocent_time' in search_parameter_keys:
    #     parameters_dict['geocent_duration'] = parameters_dict['geocent_time']
    #     parameters_dict['geocent_time'] += self.start_time
    if 'geocent_duration' in search_parameter_keys:
        parameters_dict['geocent_time'] = parameters_dict['geocent_duration']
        parameters_dict['geocent_time'] += start_time

    if 'chirp_mass' in search_parameter_keys or 'reduced_mass' in search_parameter_keys:
        # mass_1, mass_2 = paru.Mc_and_q_to_m1_and_m2(parameters_dict['chirp_mass'], parameters_dict['mass_ratio'])
        total_mass, symmetric_mass_ratio, mass_1, mass_2 = paru.Mc_and_mu_to_M_eta_m1_and_m2(parameters_dict['chirp_mass'], parameters_dict['reduced_mass'])
        # parameters_dict['total_mass'] = total_mass
        # parameters_dict['symmetric_mass_ratio'] = symmetric_mass_ratio
        parameters_dict['mass_1'] = mass_1
        parameters_dict['mass_2'] = mass_2

    return parameters_dict

if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--inj_file", default='../examples/injection_files/GW170817.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--event", default=None, action="store", type="string", help="""Event label part of the catalog GWTC-1""")
    parser.add_option("--trigger_file", default=None, action="store", type="string", help="""File .ini describing the parameters where to start the chain. If left to None and --event used, a default file corresponding to the event will be used.""")
    parser.add_option("--trigger_time", default=None, action="store", type="string", help="""Trigger GPS time.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""Print out information of interest during the run.""",dest='verbose')
    parser.add_option("--plot_traj", default=False, action="store_true", help="""Will save positions, momentas, hamiltonian values etc of every point along every trajectory in phase to be able to plot them with `plot_trajectories.py` script.""")
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")
    parser.add_option("--fit_method", default='cubic', action="store", type="string", help="""Method used to fit the numerical gradients of the log likelihood accumulated in phase1. """)
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")
    parser.add_option("--on_CIT", default=False, action="store_true", help="""Set this to True if running this script on Caltech's cluster. Will set the right path for the ROQ basis.""")

    # randGen = np.random.RandomState(seed=2)

    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()

    # opts_dict_formatted = cut.dictionary_to_formatted_string(opts.__dict__)


    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    # config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)

    RUN_CONST.plot_traj = opts.plot_traj
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    # Check whether the run is on an injection, an event from the catalog or a trigger time
    if opts.event is not None:
        data_name = opts.event + 'real'
    elif opts.inj_file is not None:
        inj_file_name = opts.inj_file.split('/')[-1]
        data_name = inj_file_name.split('.')[0] + 'inj'
    elif opts.trigger_time is not None:
        data_name = opts.trigger_time
    else:
        raise ValueError('You must specify at least one of the 3 options: --event, --inj_file, --trigger_time')


    # Parse the inputs relative to the sampler and the search
    # sampler_dict = init.get_sampler_dict(config_dict)
    sampler_dict = pu.json_to_dict(config_dict['hmc']['config_hmc_parameters_file'])
    # sampler_dict_formatted = cut.dictionary_to_formatted_string(sampler_dict)



    if opts.event is not None:

        if opts.trigger_file is None:
            trigger_file = '../examples/trigger_files/' + opts.event + '.ini'
        else:
            trigger_file = opts.trigger_file
        trigger_parameters = set_inj.ini_file_to_dict(trigger_file)
        # geocent_time = bilby.gw.utils.get_event_time(opts.event)
        # trigger_parameters['geocent_time'] = geocent_time
        ext_analysis_dict = set_inj.compute_extended_analysis_dict(trigger_parameters['mass_1'], trigger_parameters['mass_2'], trigger_parameters['geocent_time'], trigger_parameters['chirp_mass'], **config_dict['analysis'])

        # trig_dict_formatted = cut.dictionary_to_formatted_string(trigger_parameters)
        # ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)

        start_time = ext_analysis_dict['start_time']
        duration = ext_analysis_dict['duration']

        hdf5_file_path_prefix = '../__input_data/' + opts.event
        if not(os.path.exists(hdf5_file_path_prefix)):
            cut.logger.error(f'The folder given to look for hdf5 strain files of the event: {hdf5_file_path_prefix}, does not exist.')
            sys.exit(1)

        GWTC1_event_PSDs_file = f'../__input_data/{opts.event}/GWTC1_{opts.event}_PSDs.dat'
        GWTC1_event_PSDs = np.loadtxt(GWTC1_event_PSDs_file)

        # INITIALIZE THE THREE INTERFEROMETERS
        # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
        interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])

        # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
        for i, ifo in enumerate(interferometers):
            ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
            ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']

            # Should look like: '../__input_data/GW170817/H-H1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
            if opts.event == 'GW150914':
                hdf5_file_path = f'{hdf5_file_path_prefix}/{ifo.name[0]}-{ifo.name}_GWOSC_4KHZ_R1-1126259447-32.hdf5'
            if opts.event == 'GW170817':
                hdf5_file_path = f'{hdf5_file_path_prefix}/LOSC/{ifo.name[0]}-{ifo.name}_LOSC_CLN_4_V1-1187007040-2048.hdf5'

            hdf5_strain, hdf5_start_time, hdf5_duration, sampling_frequency, hdf5_number_of_samples = psd_utils.get_strain_from_hdf5(hdf5_file_path)

            if sampling_frequency != ext_analysis_dict['sampling_frequency']:
                cut.logger.error('Sampling_frequency conflict between hdf5 file = {} and that in the config file = {}'.format(sampling_frequency, ext_analysis_dict['sampling_frequency']))
                sys.exit(1)

            idx_segment_start_float = (start_time - hdf5_start_time) * sampling_frequency
            if idx_segment_start_float == int(idx_segment_start_float):
                idx_segment_start = int(idx_segment_start_float)
            else:
                idx_segment_start = int(idx_segment_start_float) + 1
                start_time_offset = (idx_segment_start - idx_segment_start_float) / sampling_frequency
                start_time +=start_time_offset
                cut.logger.info(f'Due to numerical precison when selecting the segment, the start_time has been offset by +{start_time_offset:.4e} to equal {start_time}')
            idx_segment_end_float = (start_time + duration - hdf5_start_time) * sampling_frequency
            idx_segment_end = int(idx_segment_end_float)
            if idx_segment_end_float != idx_segment_end:
                duration_real = (idx_segment_end_float - idx_segment_start) / sampling_frequency
                cut.logger.warning(f'Due to numerical precision when selecting the segment, the last sample chosen corresponds to a duration of {duration_real} and not {duration} but we keep the latter value as we need an integer...')
            #
            idx_segment_end = int((start_time + duration - hdf5_start_time) * sampling_frequency)
            # By convention due to fft routines used, `idx_segment_end` is not included
            # in the selected time segment; meaning the segment we select is said to have
            # lasted a duration = `duration` but its corresponding time_domain_series will give:
            # `tds[-1] - tds[0] = duration - 1/sampling_frequency`
            desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end]
            # desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end+1]
            strain = bilby.gw.detector.strain_data.InterferometerStrainData(minimum_frequency=ifo.minimum_frequency)
            strain.set_from_time_domain_strain(time_domain_strain=desired_hdf5_strain, sampling_frequency=sampling_frequency, duration=duration)
            # strain.low_pass_filter(filter_freq=ifo.minimum_frequency)

            ifo.strain_data = strain

            ifo.strain_data.start_time = start_time

            # SET THE PSDs
            ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_array=GWTC1_event_PSDs[:, i+1], frequency_array=GWTC1_event_PSDs[:, 0])

            ifo.psd_array = ifo.power_spectral_density_array
            ifo.inverse_psd_array = 1 / ifo.psd_array

        # WAVEFORM GENERATION
        waveform_arguments = {}
        waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
        waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
        waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
        waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']

        starting_point_parameters = trigger_parameters.copy()

        del hdf5_strain, GWTC1_event_PSDs

    elif opts.inj_file is not None:
        # SET THE INJECTION PARAMETERS AND THE ANALYSIS PARAMETERS
        injection_parameters, ext_analysis_dict = set_inj.get_inj_parameters_and_analysis_dict(opts.inj_file, **config_dict['analysis'])
        # inj_dict_formatted = cut.dictionary_to_formatted_string(injection_parameters)
        cut.logger.info(f'Injected parameters derived from {opts.inj_file} are: {inj_dict_formatted}')
        # ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)
        cut.logger.info(f'Analysis parameters derived from the component masses are: {ext_anal_dict_formatted}')

        start_time = ext_analysis_dict['start_time']

        # INITIALIZE THE THREE INTERFEROMETERS
        # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
        interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])
        # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
        for ifo in interferometers:
            ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
            ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']
            # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
            # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
            # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
            ifo.strain_data.duration = ext_analysis_dict['duration']
            ifo.strain_data.sampling_frequency = ext_analysis_dict['sampling_frequency']
            ifo.strain_data.start_time = start_time


        # SET THEIR POWER SPECTRAL DENSITIES
        # This function does not interpolate the psd
        set_psds.main(interferometers, opt_psd=config_dict['analysis']['psd'], sampling_frequency=ext_analysis_dict['sampling_frequency'], duration=ext_analysis_dict['duration'], start_time=start_time)


        # SET NOISE STRAIN DATA FROM PSD
        interferometers.set_strain_data_from_power_spectral_densities(
            sampling_frequency=ext_analysis_dict['sampling_frequency'],
            duration=ext_analysis_dict['duration'],
            start_time=start_time)

        # WAVEFORM GENERATION
        waveform_arguments = {}
        waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
        waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
        waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
        waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']
        injection_waveform_generator = bilby.gw.WaveformGenerator(
            duration=interferometers.duration,
            sampling_frequency=interferometers.sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
            waveform_arguments=waveform_arguments
            )

        # INJECT TEMPLATE INTO NOISE STRAIN DATA
        # Add the template signal to each detector's strain data
        # for i in range(len(interferometers)):
            # interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
            # interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain
        interferometers.inject_signal(parameters=injection_parameters, waveform_generator=injection_waveform_generator)

        # # COMPUTE AND PRINT SNR
        interferometers.optimal_snr()
        interferometers.matched_filter_snr()
        interferometers.log_likelihood()

        starting_point_parameters = injection_parameters.copy()
    elif opts.trigger_time is not None:
        pass


    for ifo in interferometers:
        ifo.fd_strain = ifo.strain_data.frequency_domain_strain

    # SET THE LIKELIHOOD OBJECT, two different cases whether ROQ basis is used or not
    if False:
        pass
    else:
        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        # priors = None
        priors = bilby.gw.prior.BNSPriorDict()

        frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
        if opts.event == 'GW150914':
            frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
            priors = bilby.gw.prior.BBHPriorDict()
        # make waveform generator
        # waveform_arguments['waveform_approximant'] = 'IMRPhenomPv2_NRTidal'
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=frequency_domain_source_model,
            waveform_arguments=waveform_arguments,
            parameter_conversion=parameter_conversion)

        import gw.likelihood as gwlh
        likelihood = gwlh.Likelihood(
            interferometers=interferometers,
            waveform_generator=search_waveform_generator,
            priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])

    if opts.trigger_file is None:
        # Params from the 'maxL' column of the following run: https://ldas-jobs.ligo.caltech.edu/~soichiro.morisaki/O2/G298048/C02_2/lalinferencemcmc/IMRPhenomPv2_NRTidal/1187008882.45-0/V1H1L1/posplots.html
        params_maxL = dict(chirp_mass=1.19755068423, mass_ratio=0.679399681207, luminosity_distance=19.8252382813, theta_jn=1.98489360594, psi=0.0617925754849, phase=2.34555197732, geocent_time=1187008882.4297464, ra=3.44616, dec=-0.408084, a_1=0.045465883951, a_2=0.00339714665984, tilt_1=0.855949618025, tilt_2=1.46893080078, phi_12=0.80747942947, phi_jl=1.87728113831, lambda_tilde=157.389402093, delta_lambda_tilde=-14.8307681662)

        likelihood.parameters.update(params_maxL)
    else:
        likelihood.parameters.update(starting_point_parameters)



    snr_dict = likelihood.calculate_network_snrs()

    snr_dict_formatted = cut.dictionary_to_formatted_string(snr_dict, decimal_format=2)
    print(snr_dict_formatted)

    # samples_lal_sis_file = '/Users/marcarene/projects/phd_manuscript/5e1f2f775f0e8800015c297c/chap9_comparisonLAL/fig/__samples_dnn_e60_b128_30Hz_64sec/nsrt5/samples_lal_sis_5000_12.dat'
    # samples_lal_sis_file = '/Users/marcarene/projects/phd_manuscript/5e1f2f775f0e8800015c297c/chap9_comparisonLAL/fig/__samples_dnn_e60_b128_30Hz_64sec/nsrt5/samples_lal_sis_5000_pesummary.dat'
    samples_lal_sis_file = '/Users/marcarene/projects/phd_manuscript/5e1f2f775f0e8800015c297c/chap9_comparisonLAL/fig/__samples_dnn_e60_b128_30Hz_64sec/nsrt5/samples.dat'
    samples_lal_sis = np.loadtxt(samples_lal_sis_file)
    # samples_lal_sis = samples_lal_sis[:,:-2] # remove the two last columns corresponding to lambda_tilde and delta_lambda_tilde because some conversion taking place in bilby seems erroneous.
    # import IPython; IPython.embed(); sys.exit()
    # samples_lal_sis[:, -1] += start_time_offset
    mf_snr_list = []
    logL2_list = []
    for i, q_pos in enumerate(samples_lal_sis[:20,:]):
        # print(i)
        # if i == 11:
        #     breakpoint()
        qpos_dict_final = q_pos_to_parameters_dict(q_pos)
        likelihood.parameters.update(qpos_dict_final)
        # import IPython; IPython.embed(); sys.exit()
        # qpos_dict_formatted = cut.dictionary_to_formatted_string(likelihood.parameters, decimal_format=2)
        # cut.logger.info(f'likelihood.parameters : {likelihood.parameters}')


        snr_dict = likelihood.calculate_network_snrs()
        mf_snr = snr_dict['network']['matched_filter_snr_phasemarg']
        logL2 = snr_dict['network']['log_l_ratio'] # equal to logL?
        mf_snr_list.append(mf_snr)
        logL2_list.append(logL2)

    snr_logl_lal_sis = np.asarray([mf_snr_list, logL2_list]).T

    import IPython; IPython.embed(); sys.exit()
    snr_dict_formatted = cut.dictionary_to_formatted_string(snr_dict, decimal_format=2)
    cut.logger.info(f'SNR at starting point: {snr_dict_formatted}')
