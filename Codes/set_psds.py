import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess
import pandas as pd

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.psd_utils as psd_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf
import Library.initialization as init

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as CdlogL
import Codes.FIM as FIM

import gw.detector.networks as gwdn
import core.sampler
import core.utils as cut



def psd_file_used(interferometer):
    psd_file = interferometer.power_spectral_density.psd_file
    psd_file = psd_file.split(sep='/').pop()
    return psd_file

def print_psd_file_used(interferometers):
    for ifo in interferometers:
        psd_file = psd_file_used(ifo)
        cut.logger.info(f"The PSD file used for {ifo.name} is: {psd_file}")

def main(interferometers, opt_psd=1, sampling_frequency=None, duration=None, start_time=None):
    # Those two methods are going to store psd into ifo.power_spectral_density.psd_array and the corresponding frequencies in ifo.power_spectral_density.frequency_array
    if opt_psd==1:
        GWTC1_GW170817_PSDs_file = '../__input_data/GW170817/GWTC1_GW170817_PSDs.dat'
        GWTC1_GW170817_PSDs = np.loadtxt(GWTC1_GW170817_PSDs_file)
        for i, ifo in enumerate(interferometers):
            ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_array=GWTC1_GW170817_PSDs[:, i+1], frequency_array=GWTC1_GW170817_PSDs[:, 0])
    elif opt_psd==2:
        hdf5_file_path_H1 = '../__input_data/GW170817/H-H1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
        hdf5_file_path_L1 = '../__input_data/GW170817/L-L1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
        hdf5_file_path_V1 = '../__input_data/GW170817/V-V1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
        hdf5_file_path_list = [hdf5_file_path_H1, hdf5_file_path_L1, hdf5_file_path_V1]

        # offset in seconds added to the time of coalescence to get to start_time of the strain which will be used to estimate the psd. This strain_psd should not overlap with the gw signal into the strain data otherwise the snr will be less than real.
        # psd_offset = -1024
        psd_offset = -2048
        # duration of the strain which will be used to estimate the psd. The longer this duration, the smoother the psd will be but that shouldn't change the snr much
        # psd_duration = 100
        psd_duration = 1800
        filter_freq = None
        # NFFT input description from `mlab.psd(.., NFFT = )`: The number of data points used in each block for the FFT. A power 2 is most efficient.  The default value is 256. This should *NOT* be used to get zero padding, or the scaling of the result will be incorrect.
        # NFFT description in gwosc script `lotsofplots.py`: number of sample for the fast fourier transform
        NFFT = int(4 * sampling_frequency)
        for i, ifo in enumerate(interferometers):
            psd_utils.set_interferometer_psd_from_hdf5_file(interferometer=ifo, hdf5_file_path=hdf5_file_path_list[i], signal_start_time=start_time, signal_duration=duration, psd_duration=psd_duration, psd_offset=psd_offset, filter_freq=filter_freq, NFFT=NFFT)
    elif opt_psd==3:
        # Change LIGO psd files if wanted:
        path = bilby.__path__[0] + '/gw/detector/noise_curves/'
        # new_LIGO_psd_file_name = 'aLIGO_early_high_psd.txt'
        # new_LIGO_psd_file_name = 'aLIGO_mid_psd.txt'
        # new_LIGO_psd_file_name = 'LIGO_srd_psd.txt'
        # new_LIGO_psd_file_name = 'aLIGO_late_psd.txt'
        new_LIGO_psd_file_name = 'aLIGO_ZERO_DET_high_P_psd.txt'
        interferometers[0].power_spectral_density.psd_file = path + new_LIGO_psd_file_name
        interferometers[1].power_spectral_density.psd_file = path + new_LIGO_psd_file_name
    else:
        cut.logger.warning("Error: the psd option you have set must be either 1, 2 or 3. Now it is set to {}".format(opt_psd))
        sys.exit(1)

    for ifo in interferometers:
        ifo.psd_array = ifo.power_spectral_density_array
        ifo.inverse_psd_array = 1 / ifo.psd_array

    if opt_psd==1:
        cut.logger.info("PSDs estimated from the official PSDs published with GWTC1 which file is locally stored here: \n {}".format(GWTC1_GW170817_PSDs_file))
        title = GWTC1_GW170817_PSDs_file.split('/').pop()
    elif opt_psd==2:
        cut.logger.info("PSDs estimated using the Welch methods from the following strain files:")
        for i in range(len(interferometers)):
            hdf5_file_name = hdf5_file_path_list[i].split(sep='/').pop()
            cut.logger.info("- {}".format(hdf5_file_name))
        title = 'Welch methods on strain data files'
    elif opt_psd==3:
        print_psd_file_used(interferometers)
        title = psd_file_used(interferometers[0]) + ' and ' + psd_file_used(interferometers[2])

    return title







if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--inj_file", default='../examples/injection_files/GW170817.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--event", default='GW170817', action="store", type="string", help="""Event label part of the catalog GWTC-1""")
    parser.add_option("--trigger_time", default=None, action="store", type="string", help="""Trigger GPS time.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""Print out information of interest during the run.""",dest='verbose')
    parser.add_option("--plot_traj", default=False, action="store_true", help="""Will save positions, momentas, hamiltonian values etc of every point along every trajectory in phase to be able to plot them with `plot_trajectories.py` script.""")
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")
    parser.add_option("--fit_method", default='cubic', action="store", type="string", help="""Method used to fit the numerical gradients of the log likelihood accumulated in phase1. """)
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")
    parser.add_option("--on_CIT", default=False, action="store_true", help="""Set this to True if running this script on Caltech's cluster. Will set the right path for the ROQ basis.""")

    # randGen = np.random.RandomState(seed=2)

    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()

    opts_dict_formatted = cut.dictionary_to_formatted_string(opts.__dict__)
    cut.logger.info(f'Options from the command line are: {opts_dict_formatted}')

    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)
    cut.logger.info(f'Options from the configuration file {opts.config_file} are: {config_dict_formatted}')

    if opts.on_CIT and config_dict['analysis']['roq']:
        config_dict['analysis']['roq_b_matrix_directory'] = '/home/cbc/ROQ_data/IMRPhenomPv2/'
        cut.logger.info('Setting the roq_b_matrix_directory to "/home/cbc/ROQ_data/IMRPhenomPv2/"')

    RUN_CONST.plot_traj = opts.plot_traj
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    # Check whether the run is on an injection, an event from the catalog or a trigger time
    if opts.event is not None:
        data_name = opts.event + 'real'
    elif opts.inj_file is not None:
        inj_file_name = opts.inj_file.split('/')[-1]
        data_name = inj_file_name.split('.')[0] + 'inj'
    elif opts.trigger_time is not None:
        data_name = opts.trigger_time
    else:
        raise ValueError('You must specify at least one of the 3 options: --event, --inj_file, --trigger_time')


    # SET THE INJECTION PARAMETERS AND THE ANALYSIS PARAMETERS
    injection_parameters, ext_analysis_dict = set_inj.get_inj_parameters_and_analysis_dict(opts.inj_file, **config_dict['analysis'])

    start_time = ext_analysis_dict['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
    interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])
    # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
    for ifo in interferometers:
        ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
        ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = ext_analysis_dict['duration']
        ifo.strain_data.sampling_frequency = ext_analysis_dict['sampling_frequency']
        ifo.strain_data.start_time = start_time



    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    plot_title = main(interferometers, opt_psd=2, sampling_frequency=ext_analysis_dict['sampling_frequency'], duration=ext_analysis_dict['duration'], start_time=start_time)
    # plot_title = main(interferometers, opt_psd=config_dict['analysis']['psd'], sampling_frequency=ext_analysis_dict['sampling_frequency'], duration=ext_analysis_dict['duration'], start_time=start_time)

    # import IPython; IPython.embed()


    # PLOTTING THE PSDS
    import numpy as np
    import matplotlib.pyplot as plt

    from matplotlib import rc
    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    ## for Palatino and other serif fonts use:
    # rc('font',**{'family':'serif','serif':['Palatino']})
    rc('text', usetex=True)
    plt.rc('text', usetex=False)
    plt.rc('font', family='serif')
    plt.rc('font', size=20)
    plt.rc('font', weight='bold')

    ifo_colors = ['red', 'green', 'purple']

    # psd_LIGO = np.loadtxt(interferometers[0].power_spectral_density.psd_file)
    # psd_Virgo = np.loadtxt(interferometers[2].power_spectral_density.psd_file)
    #
    # plt.figure(figsize=(10,6))
    # plt.loglog(psd_LIGO.T[0], psd_LIGO.T[1], label=psd_file_used(interferometers[0]))
    # plt.loglog(psd_Virgo.T[0], psd_Virgo.T[1], label=psd_file_used(interferometers[2]))
    # plt.xlabel("frequency (Hz)")
    # plt.ylabel("PSD")
    # plt.title("PSDs stored in files")
    # plt.legend()
    # plt.show()

    f_low = ext_analysis_dict['minimum_frequency']
    f_high = ext_analysis_dict['maximum_frequency_ifo']
    # indices = np.where((f_low<interferometers.frequency_array) & (interferometers.frequency_array<f_high))[0]

    indices_file_array_ifos = []
    for ifo in interferometers:
        indices_file_array = np.where((f_low<ifo.power_spectral_density.frequency_array) & (ifo.power_spectral_density.frequency_array<f_high))[0]

        indices_file_array_ifos.append(indices_file_array)

    plt.figure(figsize=(10,6))
    for ifo_index, ifo in enumerate(interferometers):
        # plt.loglog(ifo.frequency_array[indices], ifo.psd_array[indices], label=ifo.name, color=ifo_colors[ifo_index])
        plt.loglog(ifo.frequency_array, ifo.power_spectral_density_array, label=ifo.name,  color=ifo_colors[ifo_index])
        # plt.loglog(ifo.power_spectral_density.frequency_array[indices_file_array_ifos[ifo_index]], ifo.power_spectral_density.psd_array[indices_file_array_ifos[ifo_index]], label=ifo.name+'-file',  color=ifo_colors[ifo_index])
    plt.xlabel("frequency (Hz)")
    plt.ylabel("PSD")
    plt.title(plot_title)
    plt.legend()

    plt.show()

    sys.exit()
