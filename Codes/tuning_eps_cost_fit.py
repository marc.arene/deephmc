# Code snippet I had implemented in Phase 2 to fit the costs measured after the benchmark and infer the optimal stepsize value
# This snippet can be removed on the long term

        if False:
            # Finding the optimal step-size and corresponding acceptance rate
            # assuming in that regime the acceptance is a decreasing affine function
            # of the step-size.
            # If so, since cost = 1/(acc * eps) and acc = -alpha*eps + beta, the optimal
            # cost is found for eps_opt = beta / (2 * alpha) and acc_opt = beta / 2
            cut.logger.info(f'Trying linear fit for acceptance as a function of the step-size.')
            coeffs_acc_eps = np.polyfit(locs_eps, accs_eps, 1)
            acc_poly = np.poly1d(coeffs_acc_eps)
            accs_fitted = acc_poly(loc_eps)
            ss_res = ((accs_eps - accs_fitted)**2).sum()
            ss_tot = ((accs_eps - accs_eps.mean())**2).sum()
            R_squared = 1 - ss_res / ss_tot
            if R_squared >= 0.98:
                loc_eps_opt = coeffs_acc_eps[1] / (-2 * coeffs_acc_eps[0])
                acc_expected = coeffs_acc_eps[1] / 2
                cut.logger.info(f'Linear fit gave R**2 = {R_squared} > 0.98, hence using coeffs from this fit to derive the optimal step-size.')
            else:
                cut.logger.info(f'Linear fit gave R**2 = {R_squared} < 0.98 => rejecting the fit.')
                cut.logger.info(f'Trying quadratic fit of the costs as a function of the log10(step-size).')
                # Finding the optimal step-size with a quadratic fit on log10(epsilon)
                log10_epsilons = np.log10(locs_eps)
                costs_for_fit = np.asarray(costs_adhoc_eps_norm)
                coeffs_costs = np.polyfit(log10_epsilons, costs_for_fit, 2)
                cost_poly = np.poly1d(coeffs_costs)
                costs_fitted = cost_poly(log10_epsilons)
                ss_res = ((costs_for_fit - costs_fitted)**2).sum()
                ss_tot = ((costs_for_fit - costs_for_fit.mean())**2).sum()
                R_squared = 1 - ss_res / ss_tot
                # We use the result of the fit only if the fitted parabolla has a minimum and not a maximum.
                # Indeed if we only use three costs points which are almost aligned, the fit can get it wrong.
                # In that case we only use the value of epsilon giving the minimum cost computed
                if R_squared >= 0.98 and coeffs_costs[0] > 0:
                    cut.logger.info(f'Fit gave R**2 = {R_squared} > 0.98, hence using coeffs from this fit to derive the optimal step-size.')
                    loc_eps_opt = 10**(-coeffs_costs[1]/(2 * coeffs_costs[0]))
                    idx_closest_eps = np.abs(locs_eps - loc_eps_opt).argmin()
                    acc_expected = accs_eps[idx_closest_eps]
                else:
                    if coeffs_costs[0] <= 0:
                        cut.logger.info(f'Fit cannot be trusted because found a maximum instead of a minimum.')
                    else:
                        cut.logger.info(f'Fit gave R**2 = {R_squared} < 0.98 => rejecting the fit.')
                    idx_opt = np.array(costs_for_fit).argmin()
                    loc_eps_opt = locs_eps[idx_opt]
                    acc_expected = accs_eps[idx_opt]
                    cut.logger.info(f'Using the step-size tried earlier which gave the mimimum cost: {loc_eps_opt}')

                # if True:
                if False:
                    # Just a plot to understand how the optimal step-size is found via the fit
                    import matplotlib.pyplot as plt
                    polyfit = np.poly1d(coeffs_costs)
                    eps_logspace = np.logspace(-3, -1)
                    costs_logspace = polyfit(np.log10(eps_logspace))
                    plt.plot(locs_eps, costs_for_fit, '.')
                    plt.plot(eps_logspace, costs_logspace)
                    plt.vlines(loc_eps_opt, 0, costs_logspace.max())
                    plt.xscale('log')
                    plt.xlabel(r'epsilon')
                    plt.ylabel(r'cost')
                    plt.savefig(outdir + 'plots/phase2/cost_vs_eps.png')
