import sys
sys.path.append('../')
import os
import sys
import subprocess
import time
from optparse import OptionParser
from configparser import ConfigParser

import Library.python_utils as pu
import Library.initialization as init



# MAIN_PY_SCRIPT_DIR is relative to where this `main_with_condor.py`
# script is located
MAIN_PY_SCRIPT_DIR = '.'
MAIN_PY_SCRIPT_NAME = 'main.py'
SUB_FILE_NAME = 'condor_main.sub'
BASH_FILE_NAME = 'condor_sub_runme.sh'

SUB_FILE_PATH = f'{MAIN_PY_SCRIPT_DIR}/{SUB_FILE_NAME}'
BASH_FILE_PATH = f'{MAIN_PY_SCRIPT_DIR}/{BASH_FILE_NAME}'

def create_sub_file(outdir_condor):
    sub_file_content = f"""
    universe       = vanilla
    request_memory = 4000
    getenv         = True
    accounting_group_user = marc.arene
    accounting_group      = ligo.dev.o3.cbc.pe.lalinference
    notify_user = marc.arene@apc.in2p3.fr
    executable = {BASH_FILE_PATH}

    output = {outdir_condor}/main_with_args-$(Process)-$(Cluster)-$(Node).out
    error  = {outdir_condor}/main_with_args-$(Process)-$(Cluster)-$(Node).err
    log    = {outdir_condor}/main_with_args-$(Process)-$(Cluster)-$(Node).log

    nice_user = False
    queue
    """
    with open(SUB_FILE_PATH, "w") as sub_file:
        print(sub_file_content, file=sub_file)
    sub_file.close()

def create_sh_file(outdir_opt, other_options):
    # other_options = ' '.join(sys_argv[1:])
    sh_file_content = f'python {MAIN_PY_SCRIPT_DIR}/{MAIN_PY_SCRIPT_NAME} --outdir={outdir_opt} {other_options}'

    with open(BASH_FILE_PATH, "w") as sh_file:
        print(sh_file_content, file=sh_file)
    sh_file.close()
    # run chmod +x sh_file_path
    subprocess.run('chmod +x ' + BASH_FILE_PATH, shell=True)


if __name__ == '__main__':
    parser=OptionParser()

    parser.add_option("--inj_file", default='../examples/injection_files/GW170817.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")
    parser.add_option("--condor_submit", action="store_true", help="""Will run `$ condor_submit main.sub directly`""")
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")
    parser.add_option("--on_CIT", default=False, action="store_true", help="""Set this to True if running this script on Caltech's cluster. Will set the right path for the ROQ basis.""")

    (opts, args) = parser.parse_args()

    # PARSE CONFIG_FILE
    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    # Parse the inputs relative to the sampler and the search
    sampler_dict = init.get_sampler_dict(config_dict)

    # PARSE INJECTION NAME
    inj_file_name = opts.inj_file.split('/')[-1]
    inj_name = inj_file_name.split('.')[0]

    # RETRIEVE OUTDIR PATH AND CREATE IT
    outdir = init.get_output_dir(inj_name, sampler_dict['search_parameter_indices'], config_dict, opts.sub_dir)
    outdir_condor = outdir + '/condor_files'
    pu.mkdirs(outdir_condor)
    outdir_condor_abspath = os.path.abspath(outdir_condor)

    # RECREATE THE STRING OF ALL THE OPTIONS PASSED TO THIS SCRIPT
    # SO THEY CAN BE PASSED TO main.py IN THE BASH SCRIPT
    other_options_list = []
    for key in opts.__dict__.keys():
        if key not in ['condor_submit']:
            if type(opts.__dict__[key]) == bool:
                other_options_list.append(f'--{key}')
            else:
                other_options_list.append(f'--{key}={opts.__dict__[key]}')
    other_options = ' '.join(other_options_list)

    # CREATE THE .sub and .sh FILES
    create_sub_file(outdir_condor_abspath)
    create_sh_file(outdir, other_options)

    if opts.condor_submit:
        print(f'Submitting condor job with:\n $ condor_submit {SUB_FILE_PATH}')
        subprocess.run(f'condor_submit {SUB_FILE_PATH}', shell=True)
    else:
        print('Submit job by running:')
        print(f'  $ condor_submit {SUB_FILE_PATH}')

    # COPY THE .sub AND .sh FILES IN THE OUTDIR OF THE RUN
    subprocess.run(f'cp {SUB_FILE_PATH} {outdir_condor}/', shell=True)
    subprocess.run(f'cp {BASH_FILE_PATH} {outdir_condor}/', shell=True)

    print(f'Output directory for this run is: \n {outdir}')
