# python main.py --event=GW170817 --trigger_file=../examples/trigger_files/GW170817_IMRPhenomD_NRTidal.ini --config_file=../examples/config_files/config_GW170817_IMRPhenomD_NRTidal.ini --fit_method=dnn

import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
import datetime
from optparse import OptionParser
from configparser import ConfigParser
import subprocess
import pandas as pd

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf
import Library.initialization as init
import Library.psd_utils as psd_utils

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds

import gw.detector.networks as gwdn
import core.sampler
import core.utils as cut
import core.autocorrelation as cautocorr
import core.prior_gradient as pg
import gw.prior as prior
import gw.likelihood as gwlh
import core.stepsize_tuning_phase2 as stp




if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--inj_file", default=None, action="store", type="string", help="""[!BROKEN!] Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--event", default=None, action="store", type="string", help="""Event label part of the catalog GWTC-1""")
    parser.add_option("--trigger_file", default=None, action="store", type="string", help="""File .ini describing the parameter values where to start the chain. If left to None and --event used, a default file corresponding to the event will be used.""")
    parser.add_option("--trigger_time", default=None, action="store", type="string", help="""[!BROKEN!] Trigger GPS time.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them. Note that this config file must contain the path to a `prior_file` and to a `config_hmc_parameters_file.json`. The latter essentially defines the `search_parameter_keys` of the run.""")
    parser.add_option("--outdir", default=None, action="store", type="string", help="""[!BROKEN!] Output directory where results and plots will be stored. If not specified, it will be created automatically in the current working directory under ./outdir""")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""[!BROKEN!] Print out information of interest during the run.""",dest='verbose')
    parser.add_option("--plot_traj", default=False, action="store_true", help="""Will save positions, momentas, hamiltonian values etc of every point along every trajectory to be able to plot them with `plot_trajectories.py` script.""")
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the end to the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")
    parser.add_option("--fit_method", default='dnn', action="store", type="string", help="""Method used to fit the numerical gradients of the log likelihood accumulated in phase1. """)
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")
    parser.add_option("--on_CIT", default=False, action="store_true", help="""[!BROKEN!] Set this to True if running this script on Caltech's cluster. Will set the right path for the ROQ basis.""")



    #**************************************************************************************************#
    #********************************** INITIALIZATION OF THE RUN  ************************************#
    #**************************************************************************************************#


    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()

    opts_dict_formatted = cut.dictionary_to_formatted_string(opts.__dict__)
    cut.logger.info(f'Options from the command line are: {opts_dict_formatted}')

    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)
    cut.logger.info(f'Options from the configuration file {opts.config_file} are: {config_dict_formatted}')

    # This is not going to work on a generic CIT server...
    if opts.on_CIT and config_dict['analysis']['roq']:
        config_dict['analysis']['roq_b_matrix_directory'] = '/home/cbc/ROQ_data/IMRPhenomPv2/'
        cut.logger.info('Setting the roq_b_matrix_directory to "/home/cbc/ROQ_data/IMRPhenomPv2/"')

    # Set this boolean in RUN_CONST accordingly to the input option,
    # then `RUN_CONST.plot_traj` can be called anywhere in the code.
    RUN_CONST.plot_traj = opts.plot_traj
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    # Check whether the run is done on an injection, an event from the catalog or a trigger time.
    # WARNING: the only option really tested for the moment is `opts.event`,
    # the other two require adaptation of the code.
    if opts.event is not None:
        data_name = opts.event + 'real'
    elif opts.inj_file is not None:
        inj_file_name = opts.inj_file.split('/')[-1]
        data_name = inj_file_name.split('.')[0] + 'inj'
    elif opts.trigger_time is not None:
        data_name = opts.trigger_time
    else:
        raise ValueError('You must specify at least one of the 3 options: --event, --inj_file, --trigger_time')


    # Parse the inputs relative to the sampler and the search
    sampler_dict = pu.json_to_dict(config_dict['hmc']['config_hmc_parameters_file'])
    sampler_dict = init.sampler_dict_update_geocent_time_by_duration(sampler_dict)
    sampler_dict_formatted = cut.dictionary_to_formatted_string(sampler_dict)
    cut.logger.info(f'Options for the sampler are: {sampler_dict_formatted}')

    # SET THE OUTPUT DIRECTORY WHERE OUTPUT FILES WILL BE SAVED
    # The typical the structure is set as `'../__output_data/[data_name]/[search_parameter_nbs]/[psd_option]/[config_options]/[approximant_name]/`; cf doc string of the function.
    if opts.outdir is None:
        outdir = init.get_output_dir(data_name, sampler_dict['search_parameter_keys'], config_dict, opts.sub_dir)
    else:
        outdir = opts.outdir

    cut.setup_logger(cut.logger, outdir=outdir, label='logger', log_level='INFO')
    today = datetime.datetime.today()
    today_fmt = '{:%d/%m/%y at %Hh%M}'.format(today)
    cut.logger.info(f'\n\n\n\n\nToday is the {today_fmt}.')
    cut.logger.info(f'Output directory is: {outdir}')
    # Save the different input files in the output directory.
    if opts.inj_file is not None:
        subprocess.run('cp ' + opts.inj_file + ' ' + outdir + 'injection.ini', shell=True)
    if opts.trigger_file is not None:
        subprocess.run('cp ' + opts.trigger_file + ' ' + outdir + 'trigger.ini', shell=True)
    subprocess.run('cp ' + opts.config_file + ' ' + outdir + 'config.ini', shell=True)
    subprocess.run('cp ' + config_dict['hmc']['config_hmc_parameters_file'] + ' ' + outdir + 'config_hmc_parameters.json', shell=True)
    subprocess.run('cp ' + config_dict['parameters']['prior_file'] + ' ' + outdir + 'priors.prior', shell=True)


    # Set the frequency_source_model and priors according to BBH or BNS case.
    prior_file = config_dict['parameters']['prior_file']
    if config_dict['analysis'].get('frequency_domain_source_model', 'None') == 'lal_binary_black_hole':
        frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
        priors = bilby.gw.prior.BBHPriorDict(filename=prior_file)
    else:
        frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
        priors = bilby.gw.prior.BNSPriorDict(filename=prior_file)


    # WARNING: the`opt.inj_file` branch is certainly broken.
    if opts.event is not None:
        if opts.trigger_file is None:
            trigger_file = '../examples/trigger_files/' + opts.event + '.ini'
        else:
            trigger_file = opts.trigger_file
        trigger_parameters = set_inj.ini_file_to_dict(trigger_file)
        # geocent_time = bilby.gw.utils.get_event_time(opts.event)
        # trigger_parameters['geocent_time'] = geocent_time
        ext_analysis_dict = set_inj.compute_extended_analysis_dict(trigger_parameters['mass_1'], trigger_parameters['mass_2'], trigger_parameters['geocent_time'], trigger_parameters['chirp_mass'], priors, **config_dict['analysis'])

        subprocess.run('cp ' + trigger_file + ' ' + outdir + 'trigger.ini', shell=True)
        trig_dict_formatted = cut.dictionary_to_formatted_string(trigger_parameters)
        cut.logger.info(f'Triggered parameters derived from {trigger_file} are: {trig_dict_formatted}')
        pu.save_dict_to_json(ext_analysis_dict, outdir + 'ext_analysis_dict.json')
        ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)
        cut.logger.info(f'Analysis parameters derived from the component masses are: {ext_anal_dict_formatted}')

        start_time = ext_analysis_dict['start_time']
        duration = ext_analysis_dict['duration']

        # import IPython; IPython.embed(); sys.exit()

        # Retrieve the PSDs of the ifos from the psd_file given in the config_file.
        # For GWTC1 events, PSDs are available on DCC and formated for each event as a `GWTC1_{opts.event}_PSDs.dat` file, hence the single_file possibility.
        # [TO BE MODIFIED]: so I made a shortcut if they are stored correctly under f'../__input_data/{opts.event}/GWTC1_{opts.event}_PSDs.dat' but this should be simply removed and `gwtc1_psds_file` should not exist.
        psds_single_file = config_dict['analysis'].get('psds_single_file', '/dir/not/filled')
        gwtc1_psds_file = f'../__input_data/{opts.event}/GWTC1_{opts.event}_PSDs.dat'
        psd_dict = cut.convert_string_to_dict(config_dict['analysis'].get('psd_dict', '{}'))
        if os.path.exists(psds_single_file):
            set_psd_from_single_file = True
            psds_file_path = psds_single_file
            psds_event = np.loadtxt(psds_file_path)
        elif os.path.exists(gwtc1_psds_file):
            set_psd_from_single_file = True
            psds_file_path = gwtc1_psds_file
            psds_event = np.loadtxt(psds_file_path)
        else:
            set_psd_from_single_file = False
            if psd_dict == {}:
                cut.logger.info(f'No PSD path filled in options `psds_single_file` or `psd_dict` or could not be found. PSDs will therefore be computed from the strain files.')


        # Retrieve the names of the .hdf5 files in the input directory of the event.
        # [TO BE MODIFIED]: path of input strain files should be set in the configuration files and not half hard-coded as they are bellow...
        hdf5_file_path_prefix = '../__input_data/' + opts.event
        if not(os.path.exists(hdf5_file_path_prefix)):
            cut.logger.error(f'The folder given to look for hdf5 strain files of the event: {hdf5_file_path_prefix}, does not exist.')
            sys.exit(1)
        files_in_event_dir = os.listdir(hdf5_file_path_prefix)

        # INITIALIZE THE THREE INTERFEROMETERS
        # We use a home-made encapsulation of the standard Interferometer Bilby object
        # because we have added some optimized ifo methods for the HMC.
        # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
        interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])

        # For each ifo we will extract the time domain strain segment, attach it
        # to the ifo and then set its PSD.
        for i, ifo in enumerate(interferometers):
            # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
            ifo.minimum_frequency = ext_analysis_dict['minimum_frequency_ifos'][ifo.name]
            ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']

            # Should look like: '../__input_data/GW170817/H-H1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
            # [TO BE MODIFIED]: there should not be a hard-code for GW170817.
            if opts.event == 'GW170817':
                hdf5_file_path = f'{hdf5_file_path_prefix}/LOSC/{ifo.name[0]}-{ifo.name}_LOSC_CLN_4_V1-1187007040-2048.hdf5'
                # hdf5_file_path = f'{hdf5_file_path_prefix}/{ifo.name[0]}-{ifo.name}_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
            else:
                ifo_prefix = f'{ifo.name[0]}-{ifo.name}'
                hdf5_file_name = list(filter(lambda file: ifo_prefix in file, files_in_event_dir))[0]
                hdf5_file_path = f'{hdf5_file_path_prefix}/{hdf5_file_name}'

            # Extract the strain and other information from the hdf5 file
            hdf5_strain, hdf5_start_time, hdf5_duration, sampling_frequency, hdf5_number_of_samples = psd_utils.get_strain_from_hdf5(hdf5_file_path)
            if ext_analysis_dict['sampling_frequency'] > sampling_frequency:
                cut.logger.error('The sampling frequency required for this analysis: {}Hz, is higher than the sampling frequency of the input strain file: {}Hz'.format(ext_analysis_dict['sampling_frequency'], sampling_frequency))
                sys.exit(1)
            if ext_analysis_dict['sampling_frequency'] < sampling_frequency:
                downsampling_factor = int(sampling_frequency / ext_analysis_dict['sampling_frequency'])
                cut.logger.info('The sampling frequency required for this analysis: {}Hz is lower than the sampling frequency of the input strain file: {}Hz'.format(ext_analysis_dict['sampling_frequency'], sampling_frequency, downsampling_factor))
                cut.logger.info(f'=> Downsampling time-domain strain file by a factor {downsampling_factor}.')
                hdf5_strain = hdf5_strain[::downsampling_factor]
                sampling_frequency = ext_analysis_dict['sampling_frequency']

            # Find the index of the first sample of the segment which will be used for the analysis.
            idx_segment_start_float = (start_time - hdf5_start_time) * sampling_frequency
            if idx_segment_start_float == int(idx_segment_start_float):
                idx_segment_start = int(idx_segment_start_float)
            # If the `start_time` does not fall exactly on a sample (which is generally the case),
            # we shift its value to be that of the sample.
            else:
                idx_segment_start = int(idx_segment_start_float) + 1
                start_time_offset = (idx_segment_start - idx_segment_start_float) / sampling_frequency
                start_time +=start_time_offset
                cut.logger.info(f'Due to numerical precison when selecting the segment, the start_time has been offset by +{start_time_offset:.4e} to equal {start_time}')
            # Normally everything should be fine for the end of the segment if the
            # sampling_frequency and duration are both integer values
            idx_segment_end_float = (start_time + duration - hdf5_start_time) * sampling_frequency
            idx_segment_end = int(idx_segment_end_float)
            if idx_segment_end_float != idx_segment_end:
                duration_real = (idx_segment_end_float - idx_segment_start) / sampling_frequency
                cut.logger.warning(f'Due to numerical precision when selecting the segment, the last sample chosen corresponds to a duration of {duration_real} and not {duration} but we keep the latter value as we need an integer...')
            idx_segment_end = int((start_time + duration - hdf5_start_time) * sampling_frequency)

            # By convention due to fft routines used, `idx_segment_end` is not included
            # in the selected time segment; meaning the segment we select is said to have
            # lasted a duration = `duration` but its corresponding time_domain_series will give:
            # `tds[-1] - tds[0] = duration - 1/sampling_frequency`.
            desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end]

            # Create the bilby strain object, set it from the time domain strain
            # we selected from the hdf5 file and attach it to the ifo.
            strain = bilby.gw.detector.strain_data.InterferometerStrainData(minimum_frequency=ifo.minimum_frequency)
            strain.set_from_time_domain_strain(time_domain_strain=desired_hdf5_strain, sampling_frequency=sampling_frequency, duration=duration)
            # strain.low_pass_filter(filter_freq=ifo.minimum_frequency)
            ifo.strain_data = strain
            ifo.strain_data.start_time = start_time


            # SET THE PSDs
            # If a single file containing the, per say 3 PSDs, the following
            # order is assumed column-wise: freq - H1 - L1 - V1.
            if set_psd_from_single_file:
                cut.logger.info(f'Setting PSD of {ifo.name} from {psds_file_path}')
                ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_array=psds_event[:, i+1], frequency_array=psds_event[:, 0])
            # If a path for each individual PSD has been given in the config.ini file,
            # ie `psd_dict` is not an empty dict, we retrieve the path of the file.
            elif psd_dict != {}:
                psd_file_path = psd_dict[f'{ifo.name}']
                freq_and_psd_array_ifo = np.loadtxt(psd_file_path)
                cut.logger.info(f'Setting PSD of {ifo.name} from {psd_file_path}')
                ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_array=freq_and_psd_array_ifo[:, 1], frequency_array=freq_and_psd_array_ifo[:, 0])
            # Otherwise we compute the PSD from the time-domain strain file using the Welch method.
            else:
                cut.logger.info(f'Computing the PSD of {ifo.name} from the strain data in {hdf5_file_path}')
                # offset in seconds added to the time of coalescence to get to start_time of the strain which will be used to estimate the psd. This strain_psd should not overlap with the gw signal into the strain data otherwise the snr will be less than real.
                # psd_offset = -1024
                psd_offset = -2048
                # duration of the strain which will be used to estimate the psd. The longer this duration, the smoother the psd will be but that shouldn't change the snr much
                # psd_duration = 100
                psd_duration = 1800
                filter_freq = None
                # NFFT input description from `mlab.psd(.., NFFT = )`: The number of data points used in each block for the FFT. A power 2 is most efficient.  The default value is 256. This should *NOT* be used to get zero padding, or the scaling of the result will be incorrect.
                # NFFT description in gwosc script `lotsofplots.py`: number of sample for the fast fourier transform
                NFFT = int(4 * sampling_frequency)
                psd_utils.set_interferometer_psd_from_hdf5_file(interferometer=ifo, hdf5_file_path=hdf5_file_path, signal_start_time=start_time, signal_duration=duration, psd_duration=psd_duration, psd_offset=psd_offset, filter_freq=filter_freq, NFFT=NFFT)

            # I used to use a copy of `ifo.power_spectral_density_array` because
            # at some point in Bilby the latter was hidding a function which was
            # recomputing the PSD each time... it's been fixed by now i think.
            # [TO BE MODIFIED]: Hence the two following could/should be removed
            # from the entire code.
            ifo.psd_array = ifo.power_spectral_density_array
            ifo.inverse_psd_array = 1 / ifo.psd_array


        # Set some waveform args which will be used when setting the
        # waveform_generator
        waveform_arguments = {}
        waveform_arguments['minimum_frequency'] = min(ext_analysis_dict['minimum_frequency_ifos'].values())
        waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
        waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
        waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']


        # It's essentially a renaming of the variable here.
        starting_point_parameters = trigger_parameters.copy()

        del hdf5_strain, desired_hdf5_strain

    # WARNING: this `inj_file` branch is certainly broken.
    elif opts.inj_file is not None:
        # SET THE INJECTION PARAMETERS AND THE ANALYSIS PARAMETERS
        injection_parameters, ext_analysis_dict = set_inj.get_inj_parameters_and_analysis_dict(opts.inj_file, **config_dict['analysis'])
        inj_dict_formatted = cut.dictionary_to_formatted_string(injection_parameters)
        cut.logger.info(f'Injected parameters derived from {opts.inj_file} are: {inj_dict_formatted}')
        pu.save_dict_to_json(ext_analysis_dict, outdir + 'ext_analysis_dict.json')
        ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)
        cut.logger.info(f'Analysis parameters derived from the component masses are: {ext_anal_dict_formatted}')

        start_time = ext_analysis_dict['start_time']

        # INITIALIZE THE THREE INTERFEROMETERS
        # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
        interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])
        # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
        for ifo in interferometers:
            ifo.minimum_frequency = ifos_min_freqs_dict[ifo]
            ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']
            # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
            # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
            # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
            ifo.strain_data.duration = duration
            ifo.strain_data.sampling_frequency = ext_analysis_dict['sampling_frequency']
            ifo.strain_data.start_time = start_time


        # SET THEIR POWER SPECTRAL DENSITIES
        # This function does not interpolate the psd
        set_psds.main(interferometers, opt_psd=config_dict['analysis']['psd'], sampling_frequency=ext_analysis_dict['sampling_frequency'], duration=duration, start_time=start_time)


        # SET NOISE STRAIN DATA FROM PSD
        interferometers.set_strain_data_from_power_spectral_densities(
            sampling_frequency=ext_analysis_dict['sampling_frequency'],
            duration=duration,
            start_time=start_time)

        # WAVEFORM GENERATION
        waveform_arguments = {}
        waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
        waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
        waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
        waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']
        injection_waveform_generator = bilby.gw.WaveformGenerator(
            duration=interferometers.duration,
            sampling_frequency=interferometers.sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
            waveform_arguments=waveform_arguments
            )

        # INJECT TEMPLATE INTO NOISE STRAIN DATA
        # Add the template signal to each detector's strain data
        # for i in range(len(interferometers)):
            # interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
            # interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain
        interferometers.inject_signal(parameters=injection_parameters, waveform_generator=injection_waveform_generator)

        # # COMPUTE AND PRINT SNR
        interferometers.optimal_snr()
        interferometers.matched_filter_snr()
        interferometers.log_likelihood()

        starting_point_parameters = injection_parameters.copy()
    elif opts.trigger_time is not None:
        pass

    # Set the geocent_duration value of the starting_point.
    starting_point_parameters['geocent_duration'] = starting_point_parameters['geocent_time'] - start_time

    # Note: Calling the `ifo.strain_data.frequency_domain_strain` for the first time sets the `ifo.strain_data.window_factor` to a value potentially != 1.
    # Similar to ifo.psd_array, I used a copy of the `.frequency_domain_strain`
    # to avoid some potentially unecessary back-end call that bilby was doing.
    # [TO BE MODIFIED]: It should be checked if this is still necessary and if not
    # remove `ifo.fd_strain` from the entire code.
    for ifo in interferometers:
        ifo.fd_strain = ifo.strain_data.frequency_domain_strain



    # SET THE LIKELIHOOD AND PRIOR OBJECTS
    # Two different cases whether ROQ basis is used or not.
    # The ROQ case is probably broken by now but could be fixed quickly I reckon,
    # and it treats some tricky steps.
    if config_dict['analysis']['roq']:
        if ext_analysis_dict['approximant'] != 'IMRPhenomPv2':
            raise ValueError("Need to set approximant to IMRPhenomPv2 to work with the ROQ basis, approximant here is {}".format(ext_analysis_dict['approximant']))

        scale_factor = ext_analysis_dict['roq']['scale_factor']
        roq_directory = ext_analysis_dict['roq']['directory']
        # Load in the pieces for the linear part of the ROQ. Note you will need to
        freq_nodes_linear = np.load(roq_directory + "fnodes_linear.npy") * scale_factor
        # Load in the pieces for the quadratic part of the ROQ
        freq_nodes_quadratic = np.load(roq_directory + "fnodes_quadratic.npy") * scale_factor

        # Load the parameters describing the valid parameters for the basis.
        params = ext_analysis_dict['roq']['rescaled_params'].copy()

        outdir_psd = init.get_outdir_psd(config_dict['analysis']['psd'])
        roq_outdir = '../__ROQ_weights/' + data_name + '/' + outdir_psd
        pu.mkdirs(roq_outdir)
        weights_file_path_no_format = roq_outdir + '{:.0f}Hz_{:.0f}Hz_{:.0f}Hz_{:.0f}s_weights'.format(params['flow'], ext_analysis_dict['maximum_frequency_ifo'], ext_analysis_dict['maximum_frequency_injected_waveform'], params['seglen'])
        weight_format = 'npz'
        weights_file_path = weights_file_path_no_format + f'.{weight_format}'

        if config_dict['analysis'].get('frequency_domain_source_model', 'None') == 'lal_binary_black_hole':
            frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
        else:
            frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
        # make waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=duration, sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=frequency_domain_source_model,
            waveform_arguments=dict(
                frequency_nodes_linear=freq_nodes_linear,
                frequency_nodes_quadratic=freq_nodes_quadratic,
                reference_frequency=ext_analysis_dict['reference_frequency'], waveform_approximant=ext_analysis_dict['approximant']),
            parameter_conversion=parameter_conversion)

        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        priors = bilby.gw.prior.BNSPriorDict()
        # for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'phi_12', 'phi_jl', 'luminosity_distance']:
        for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'luminosity_distance']:
            priors[key] = starting_point_parameters[key]
        for key in ['mass_1', 'mass_2']:
            priors[key].minimum = max(priors[key].minimum, params['compmin'])
        priors['chirp_mass'] = bilby.core.prior.Constraint(name='chirp_mass', minimum=float(params['chirpmassmin']), maximum=float(params['chirpmassmax']))
        priors['mass_ratio'] = bilby.core.prior.Constraint(0.125, 1, name='mass_ratio')
        priors['geocent_time'] = bilby.core.prior.Uniform(starting_point_parameters['geocent_time'] - 0.1, starting_point_parameters['geocent_time'] + 0.1, latex_label='$t_c$', unit='s')

        # Update user's parameters boundaries with the roq boundaries
        # if they are more restrictive
        from bilby.gw.conversion import component_masses_to_chirp_mass, chirp_mass_and_mass_ratio_to_total_mass
        new_bounds = sampler_dict['parameters_boundaries'].copy()
        if 'chirp_mass' in sampler_dict['search_parameter_keys']:
            # If mass boundaries set by user with mass_1 and mass_2
            # convert these into chirp boundaries
            if 'chirp_mass' not in new_bounds.keys():
                chirp_mass_min_user = component_masses_to_chirp_mass(new_bounds['mass_1'][0], new_bounds['mass_2'][0])
                chirp_mass_max_user = component_masses_to_chirp_mass(new_bounds['mass_1'][1], new_bounds['mass_2'][1])
            # Then take the most restrictive between user bounds and ROQ
                chirp_mass_min = max(chirp_mass_min_user, params['chirpmassmin'])
                chirp_mass_max = min(chirp_mass_max_user, params['chirpmassmax'])
                new_bounds['chirp_mass'] = [chirp_mass_min, chirp_mass_max]
                if chirp_mass_min_user != chirp_mass_min or chirp_mass_max != chirp_mass_max_user:
                    cut.logger.info(f'Chirp mass boundaries from user was: [{chirp_mass_min_user}, {chirp_mass_max_user}], with the ROQ constraints it is now: [{chirp_mass_min}, {chirp_mass_max}]')
        # Now we want to know the new bounds for mass_1 and mass_2 as they will
        # change the bounds for the reduced_mass. For that we will first
        # derive the bounds for the mass ratio:
        if 'mass_ratio' not in new_bounds.keys():
            mass_ratio_max_user = new_bounds['mass_1'][1] / new_bounds['mass_2'][0]
            new_bounds['mass_ratio'] = [1, min(mass_ratio_max_user, params['qmax'])]
            compmassmin, _ = paru.Mc_and_q_to_m1_and_m2(chirp_mass_min, 1)
            _, compmassmax = paru.Mc_and_q_to_m1_and_m2(chirp_mass_max, 1)
            compmassmin = max(compmassmin, params['compmin'])
            new_bounds['mass_1'] = [compmassmin, compmassmax]
            new_bounds['mass_2'] = [compmassmin, compmassmax]
            cut.logger.info(f'New bounds for component masses are: [{compmassmin}, {compmassmax}]')

        sampler_dict['parameters_boundaries'] = new_bounds.copy()

        if os.access(weights_file_path, os.W_OK):
            # load the weights from the file
            likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                weights=weights_file_path, priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])
        else:
            # Load in the pieces for the linear part of the ROQ. Note you will need to
            basis_matrix_linear = np.load(roq_directory + "B_linear.npy").T
            # Load in the pieces for the quadratic part of the ROQ
            basis_matrix_quadratic = np.load(roq_directory + "B_quadratic.npy").T

            # Redoing here a bit what's already being done in set_inj because `bilby.gw.likelihood.ROQGravitationalWaveTransient()` does not accept dictionnary for roq_params...
            roq_params_ndarray = np.genfromtxt(ext_analysis_dict['roq']['params_path'], names=True)
            roq_params_ndarray = roqu.rescale_params(roq_params_ndarray, scale_factor)
            # roq_params_ndarray = None

            likelihood = gwlh.ROQLikelihood(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic,
                priors=priors, roq_params=roq_params_ndarray)

            # remove the basis matrices as these are big for longer bases
            del basis_matrix_linear, basis_matrix_quadratic

            # write the weights to file so they can be loaded multiple times
            likelihood.save_weights(weights_file_path_no_format, format=weight_format)
    else:
        # # Set the frequency_source_model and priors according to BBH or BNS case.
        # prior_file = config_dict['parameters']['prior_file']
        # if config_dict['analysis'].get('frequency_domain_source_model', 'None') == 'lal_binary_black_hole':
        #     frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
        #     parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
        #     priors = bilby.gw.prior.BBHPriorDict(filename=prior_file)
        # else:
        #     frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
        #     parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
        #     priors = bilby.gw.prior.BNSPriorDict(filename=prior_file)
        # Set the waveform generator which is needed for the likelihood object
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=duration, sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=frequency_domain_source_model,
            waveform_arguments=waveform_arguments,
            parameter_conversion=parameter_conversion)



        # SET THE PRIORS AND THEIR GRADIENTS
        # `fill_in_input_priors` sets the min and max for chirp and reduced masses,
        # sets the prior for geocent_time and geocent_duration around start_time and
        # shifts the boundaries if aligned spins and zprior is used.
        priors = prior.fill_in_input_priors(priors, starting_point_parameters['geocent_time'], start_time)
        # Create the corresponding priors for the parametrisation in which the HMC moves,
        # eg from chirp_mass to log(chirp_mass) etc...
        traj_priors = prior.GWTrajPriors(
            priors=priors,
            search_parameter_keys=sampler_dict['search_parameter_keys'],
            dict_trajectory_functions_str=sampler_dict['trajectory_functions']
            )
        traj_prior_gradients = pg.GWTrajPriorGradients(
            traj_priors=traj_priors,
            dict_parameters_offsets=sampler_dict['parameters_offsets']
            )

        # Finally, set the likelihood object.
        # Note that we use an encapsulation of the standard Bilby Likelihood object
        # so that we have handy methods for our needs.
        likelihood = gwlh.Likelihood(
            interferometers=interferometers,
            waveform_generator=search_waveform_generator,
            priors=priors,
            phase_marginalization=config_dict['analysis']['phase_marginalization'])

    # Initialize the parameters values at the starting point of the analysis.
    likelihood.parameters.update(starting_point_parameters)



    # COMPUTE AND PRINT SNR
    snr_dict = likelihood.calculate_network_snrs()
    snr_dict_formatted = cut.dictionary_to_formatted_string(snr_dict, decimal_format=2)
    cut.logger.info(f'SNR at starting point: {snr_dict_formatted}')
    # Set the first value of the snr of the chain.
    if likelihood.phase_marginalization:
        mf_snr = snr_dict['network']['matched_filter_snr_phasemarg']
    else:
        mf_snr = snr_dict['network']['matched_filter_snr']



    # SET THE LIKELIHOOD GRADIENT OBJECT AND COMPUTE THE GRADIENT AT POINT OF INJECTION
    # [TO BE MODIFIED]: not mandatory but the `dict_diff_method` could be set in the config.ini
    # file rather than hard coded here.
    likelihood_gradient = lg.GWTransientLikelihoodGradient(
        likelihood=likelihood,
        traj_priors=traj_priors,
        dict_trajectory_functions_str=sampler_dict['trajectory_functions'],
        dict_parameters_offsets=sampler_dict['parameters_offsets'],
        dict_diff_method={'chi_1': 'central', 'chi_2': 'central', 'lambda_1': 'forward', 'lambda_2': 'forward'}
        )
    dict_diff_method_str = cut.dictionary_to_formatted_string(likelihood_gradient.dict_diff_method)
    cut.logger.info(f'Differencing method used for gradients of the log-likelihood are: {dict_diff_method_str}')

    # SET THE HMC SAMPLER
    # This sampler object is the central object of the algorithm
    # and gathers (too) many methods.
    sampler = core.sampler.GWHMCSampler(
        starting_point_parameters=starting_point_parameters,
        outdir=outdir,
        likelihood_gradient=likelihood_gradient,
        traj_prior_gradients=traj_prior_gradients,
        search_parameter_keys_local_fit=sampler_dict['search_parameter_keys_local_fit'],
        n_traj_hmc_tot = config_dict['hmc']['n_traj_hmc_tot'],
        n_traj_fit = config_dict['hmc']['n_traj_fit'],
        n_traj_for_this_run = config_dict['hmc']['n_traj_for_this_run'],
        n_fit_1 = config_dict['hmc']['n_fit_1'],
        n_fit_2 = config_dict['hmc']['n_fit_2'],
        length_num_traj = config_dict['hmc']['length_num_traj'],
        epsilon0 = config_dict['hmc']['epsilon0'],
        n_sis = config_dict['hmc']['n_sis']
        )

    # SET THE STATE OF THE SAMPLER
    # Either to what it was saved if the run had been checkpointed,
    # or initialize it if this is the first time these settings are run.
    count, n_traj_already_run, duration1, Acc, q_pos, dlogL, logL, randGen, qpos_traj_phase1, dlogL_traj_phase1, stuck_seq_and_traj_list, count_phase3, duration2, duration3, atr, htr, ntr, atr_time, htr_time, ntr_time, stuck_count, hybrid_stuck, next_traj_to_check_sis  = sampler.get_state()

    # Check whether Phase 3 had already started and if so we should skip phase 1 and 2.
    # `n_traj_fit` is the nb of numerical trajectories that should be run in phase 1.
    phase3_ongoing = n_traj_already_run > sampler.n_traj_fit

    # Just a handy checkpoint to have access to the initialized state
    # of the sampler for debug purposes.
    # if True:
    if False:
        import IPython; IPython.embed(); sys.exit()

    # If the plot_traj option was asked in the command line, we create a
    # sub-directory and initialize a dictionary of array variables which
    # will be used to stored each trajectory such that they can be plotted
    # and scrutinized with the `gwhmc/Tests/plot_trajectory.py` script.
    if RUN_CONST.plot_traj:
        sampler.plot_traj_dir = outdir + 'plot_traj/'
        pu.mkdirs(sampler.plot_traj_dir)
        sampler.dict_of_trajectories = dict(
            H_p_logL_logP=[],
            pmom_trajs=[],
            qpos_traj=[],
            dlogL=[],
            dlogTrajPi_traj=[],
            lengths=[]
            )







    #**************************************************************************************************#
    #********************************** Phase 1 :  numericalHMC   *************************************#
    #**************************************************************************************************#
    cut.logger.info('****************************************************************************')
    cut.logger.info('PHASE 1 : NUMERICAL GRADIENT')


    l_start = n_traj_already_run + 1
    l_end = min(l_start + sampler.n_traj_for_this_run, sampler.n_traj_fit + 1)

    if l_start < l_end:
        start = time.time()
        if l_start == 1:
            cut.logger.info("Starting phase 1")
        else:
            cut.logger.info(f'Continuing phase 1 from traj nb {n_traj_already_run} for {l_end - l_start} more trajectories.')
        try:
            for traj_index in range(l_start, l_end):
                # Generation of epsilon according to a Gaussian law with limitations between 0.001 and 0.01
                rannum = randGen.normal(loc=0, scale=1.5)
                epsilon = sampler.epsilon0 + rannum * 1.0e-3
                if epsilon <= 0.001: epsilon = 0.001
                if epsilon >= 0.01: epsilon = 0.01

                # Run a numerical trajectory and retrieve the data.
                q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.NumericalGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, sampler.length_num_traj, traj_index, sampler)

                # Evaluation of acceptante rate
                traj_status = "   rejected   "
                if Accept == 1:
                    count += 1
                    traj_status = "** ACCEPTED **"
                    # Update the training set with the new data points.
                    fnr.update_train_data(qpos_traj_phase1, dlogL_traj_phase1, qpos_from_traj, dlogL_from_traj)

                    # Update the `mf_snr` variable with the value that was computed at the end of the trajectory.
                    # Otherwise (ie `if Accept == 0`), `mf_snr` keeps the same value as before.
                    # However this assumes that the network_snr is indeed updated at the end of each trajectory
                    # which I can't find in the code at the moment...
                    if sampler.likelihood.phase_marginalization:
                        mf_snr = sampler.likelihood.snr_dict['network']['matched_filter_snr_phasemarg']
                    else:
                        mf_snr = sampler.likelihood.snr_dict['network']['matched_filter_snr']
                Acc = count/traj_index

        		# Print the position of the chain
                sampler.samples.append(q_pos.tolist())
                sampler.chain_metrics.append([mf_snr, logL, Acc, Accept])
                if 'chirp_mass' in sampler.search_parameter_keys and 'reduced_mass' in sampler.search_parameter_keys:
                    idx_Mc = sampler.search_parameter_keys.index('chirp_mass')
                    idx_mu = sampler.search_parameter_keys.index('reduced_mass')
                    eta = paru.Mc_mu_to_eta(q_pos[idx_Mc], q_pos[idx_mu])
                elif 'mass_1' in sampler.search_parameter_keys and 'mass_2' in sampler.search_parameter_keys:
                    idx_m1 = sampler.search_parameter_keys.index('mass_1')
                    idx_m2 = sampler.search_parameter_keys.index('mass_2')
                    eta = bilby.gw.conversion.component_masses_to_symmetric_mass_ratio(q_pos[idx_m1], q_pos[idx_m2])

                if a < proba_acc:
                    sup_or_inf = '<'
                else:
                    sup_or_inf = '>'
                flag_strange_proba = ''
                if abs(proba_acc - 1) > 0.35:
                    flag_strange_proba = ' - ??'
                cut.logger.info(f'{traj_index:3} - {traj_status} - Acc = {Acc:5.1%} - {a:1.5f} {sup_or_inf} {proba_acc:1.5f}{flag_strange_proba} - mf_snr = {mf_snr:1.3f} - eta = {eta:.6f}')

                if False:
                # if sampler.n_traj_fit >= 100 and traj_index%int(sampler.n_traj_fit/4)==0:
                    # Make also a corner plot of all the points visited during phase1's accepted trajectories which hence will be used during phase2 for fitting
                    cut.logger.info(f'Making corner plots of {len(qpos_traj_phase1)} accumulated points.')
                    starting_point_parameters_traj = sampler.parameters_dict_to_parameters_traj_dict(starting_point_parameters)
                    plots.plot_walkers_and_corner_from_samples(np.asarray(qpos_traj_phase1), starting_point_parameters_traj, sampler.search_trajectory_parameters_keys, save=True, label='{}_samples'.format(len(qpos_traj_phase1)), outdir=outdir + 'plots/phase1')

                if traj_index % 25 == 0:
                    # Checkpoint at the end of the loop
                    duration1_new = duration1 + time.time() - start
                    sampler.save_state(
                        count, traj_index, duration1_new, Acc, q_pos, dlogL, logL, randGen,
                        qpos_traj_phase1=qpos_traj_phase1,
                        dlogL_traj_phase1=dlogL_traj_phase1
                        )
        except KeyboardInterrupt:
            traj_index -= 1
            save_state = input('Do you want to save the state of the run?\n=> ')
            if save_state.lower() not in ['y', 'yes', 'oui', 'ya', 'da', 'si', 'yep']:
                cut.logger.info('Quitting here without saving.')
                sys.exit()
        stop = time.time()
        duration1 += stop - start

        # Checkpoint at the end of the loop
        cut.logger.info("Saving sampler state...")
        sampler.save_state(
            count, traj_index, duration1, Acc, q_pos, dlogL, logL, randGen,
            qpos_traj_phase1=qpos_traj_phase1,
            dlogL_traj_phase1=dlogL_traj_phase1
            )
        if RUN_CONST.plot_traj:
            np.savetxt(sampler.plot_traj_dir + 'H_p_logL_logP.dat', sampler.dict_of_trajectories['H_p_logL_logP'])
            np.savetxt(sampler.plot_traj_dir + 'pmom_trajs.dat', sampler.dict_of_trajectories['pmom_trajs'])
            np.savetxt(sampler.plot_traj_dir + 'qpos_traj.dat', sampler.dict_of_trajectories['qpos_traj'])
            np.savetxt(sampler.plot_traj_dir + 'dlogL.dat', sampler.dict_of_trajectories['dlogL'])
            np.savetxt(sampler.plot_traj_dir + 'dlogTrajPi_traj.dat', sampler.dict_of_trajectories['dlogTrajPi_traj'])
            np.savetxt(sampler.plot_traj_dir + 'lengths.dat', sampler.dict_of_trajectories['lengths'], fmt='%.0f')

        # Make a walker and corner plots of the chain in phase1
        sampler.plot_corner(sis=False, reweighted=False, priors=True, save=True, outdir=outdir + 'plots/phase1/', in_comp_mass=False)
        sampler.plot_walkers(outdir=outdir + 'plots/phase1/')

        cut.logger.info("Total time for phase1: {:.3} hours".format(duration1/3600))
        cut.logger.info(f'Average time to compute 1 numerical trajectory: {duration1 / l_end:.2f} sec')
        cut.logger.info(f'Average time to get 1 accepted trajectory     : {duration1 / l_end / Acc:.2f} sec')
        cut.logger.info("Average time to compute one numerical gradient dlogL = {}ms".format(int(1000 * duration1/(l_end * sampler.length_num_traj))))
        # If there remains trajectories to run in phase1 we quit here
        if l_end < sampler.n_traj_fit + 1:
            cut.logger.info(f'Quitting here after running {sampler.n_traj_for_this_run} trajectories; there remain {sampler.n_traj_fit + 1 - l_end} traj to run in phase1.')
            sys.exit()
        if l_end == sampler.n_traj_fit + 1:
            # Make a savings of phase1 data in a separate directory
            sampler.save_state(
                count, l_end - 1, duration1, Acc, q_pos, dlogL, logL, randGen,
                qpos_traj_phase1=qpos_traj_phase1,
                dlogL_traj_phase1=dlogL_traj_phase1,
                state_dir_suffix='phase1/'
                )
            sampler.plot_hist_dlogL_phase1(dlogL_traj_phase1)

    else:
        cut.logger.info(" There are no trajectories left to run in phase1.")
    # sys.exit()

    #************************************************************************************************/
    #***************************** Phase 2 : Fitting routines   *************************************/
    #************************************************************************************************/
    cut.logger.info("****************************************************************************")
    cut.logger.info("PHASE 2.1 : FITTING")

    cut.logger.info(f'Method chosen for the global fit is: {opts.fit_method}')

    # INITIALIZE THE GLOBAL_FIT_METHOD CLASS
    if opts.fit_method == 'cubic':
        fit_method = fnr.CubicFitRegressor()
    elif opts.fit_method == 'dnn':
        epochs = 60
        batch_size = 128
        fit_method = fnr.DNNRegressorMultipleGradients(
            n_dim=sampler.n_dim,
            batch_size=batch_size,
            epochs=epochs
        )
    else:
        cut.logger.error(f'Unknown fit method asked {opts.fit_method}.')
        sys.exit()

    if not(phase3_ongoing):
        start_phase2 = time.time()
        # SET THE TRAINING SET AND VALIDATION SET FROM PHASE1 POINTS
        # validation_split = 0.5
        # train_prop = 0.5019635844341307 # corresponds to 281200 when 560200 total
        # val_prop = 1 - train_prop
        # train_prop = 0.4803234501347709 # corresponds to 142560 when 1500 num traj
        # val_prop = 0.4803234501347709
        # train_prop = 0.3941176470588235
        train_prop = 1
        val_prop = 0
        x_data = np.asarray(qpos_traj_phase1)
        y_data = np.asarray(dlogL_traj_phase1)

        # # REMOVE OUTLIERS
        # cut.logger.info(f'Total data set size from phase1 BEFORE removing outliers = {len(x_data)}.')
        # import scipy
        # z = abs(scipy.stats.zscore(y_data, axis=0))
        # sample_to_keep_mask = (z < 4).all(axis=1)
        # x_data = x_data[sample_to_keep_mask]
        # y_data = y_data[sample_to_keep_mask]
        # cut.logger.info(f'Total data set size from phase1 AFTER removing outliers = {len(x_data)}.')
        cut.logger.info(f'Total data set size from phase1 = {len(x_data)}.')
        cut.logger.info(f'Discarding {val_prop:.1%} of phase1 data as validation set of the fit.')
        if val_prop != 0:
            idx_train = int(x_data.shape[0] * train_prop)
            idx_val = int(x_data.shape[0] * val_prop)
            # cut.logger.info(f'SWAPPING FROM USUAL: VAL = 1500 FIRST AND TRAIN = 1500 LAST')
            x_train = x_data[:idx_train]
            y_train = y_data[:idx_train]
            x_val = x_data[-idx_val:]
            y_val = y_data[-idx_val:]
            # x_train = x_data[-idx_val:]
            # y_train = y_data[-idx_val:]
            # x_val = x_data[:idx_train]
            # y_val = y_data[:idx_train]

            validation_data = (x_val, y_val)
            cut.logger.info(f'Validation set size = {len(x_val)}.')
            # import IPython; IPython.embed(); sys.exit()
            filename = outdir + f'plots/phase2/dlogL_vs_qpos_{len(x_train)}train_{len(x_val)}val.png'
            if os.path.exists(filename):
                pass
            else:
                cut.logger.info(f'Making plots of dlogL vs qpos, validation set over training set...')
                plots.plot_dlogL_vs_qpos(x_train, y_train, x_val, y_val, sampler.search_trajectory_parameters_keys, sampler.outdir + 'plots/phase2')

            filename = outdir + f'plots/phase2/{len(x_train)}x_train_vs_{len(x_val)}x_val.png'
            # if True:
            if os.path.exists(filename):
                pass
            else:
                cut.logger.info(f'Making corner plots of validation set over training set...')
                latex_labels_traj = CONST.get_latex_labels(sampler.search_trajectory_parameters_keys)
                result_train = bilby.core.result.Result(
                    label='x-train',
                    search_parameter_keys=sampler.search_trajectory_parameters_keys,
                    parameter_labels=latex_labels_traj,
                    posterior=pd.DataFrame(x_train, columns=sampler.search_trajectory_parameters_keys),
                    meta_data={}
                    )
                result_val = bilby.core.result.Result(
                    label='x-val',
                    search_parameter_keys=sampler.search_trajectory_parameters_keys,
                    parameter_labels=latex_labels_traj,
                    posterior=pd.DataFrame(x_val, columns=sampler.search_trajectory_parameters_keys),
                    meta_data={}
                    )
                from bilby.core.result import plot_multiple
                plot_multiple([result_train, result_val], filename=filename, corner_labels=latex_labels_traj)
        else:
            x_train = x_data.copy()
            y_train = y_data.copy()
            x_val, y_val = None, None
            validation_data = None
        # cut.logger.info(f'Setting validation data to None to check how long really takes the DNN training.')
        # x_val, y_val = None, None
        # validation_data = None
        cut.logger.info(f'Training set size = {len(x_train)}, which corresponds to data gathered by {len(x_train)/200:.0f} numerical trajectories accepted in Phase I.')

        # LOAD THE FIT IF IT WAS SAVED PREVIOUSLY AT THE END OF PHASE 1, MAKE THE FIT OTHERWISE IF PHASE 1 JUST FINISHED. NOTE THAT THE LOADING OF THE FIT IF PHASE 3 WAS ON-GOING IS NOT DONE HERE.
        global_fit_dir = sampler.outdir + f'sampler_state/global_fit/train_size_{len(x_train)}'
        global_fit_dir_specific = global_fit_dir + f'/{fit_method.save_dir_suffix}'
        if os.path.exists(global_fit_dir_specific):
            cut.logger.info(f'Loading the {fit_method.nick_name} fit from saved directory {global_fit_dir_specific}...')
            fit_method.load(global_fit_dir)
        else:
            if opts.fit_method == 'cubic':
                fit_method.fit(x_train, y_train)
            elif opts.fit_method == 'dnn':
                cut.logger.info(f'Training the DNN model...')
                kwargs = dict(
                    batch_size=batch_size,
                    epochs=epochs,
                    validation_data=validation_data
                    )
                hist = fit_method.fit(x_train, y_train, **kwargs)
                cut.check_directory_exists_and_if_not_mkdir(global_fit_dir_specific)
                pu.save_dict_to_json(hist.history, global_fit_dir_specific + '/training_hist.json')
                hist_file_path = global_fit_dir_specific + f'/dnn_mg_model_hist.png'
                plots.plot_training_history(hist.history, hist_file_path)
            else:
                cut.logger.info(f'Unknown fit method asked {opts.fit_method}.')
                sys.exit()


            # if x_val is not None:
            #     fit_method.new_x_for_train = x_val.tolist()
            #     fit_method.new_y_for_train = y_val.tolist()

            # stop = time.time()
            # duration2 += stop - start
            cut.logger.info(f"Total time for the fitting part of the algorithm : {(time.time()-start_phase2)/60:.0f}min.")

            cut.logger.info('Saving global fit data for future load...')
            fit_method.save(global_fit_dir)


        oluts_fit = sampler.set_oluts_regressor()
        oluts_fit_dir = sampler.outdir + 'sampler_state/oluts_fit'
        # if False:
        if os.path.exists(oluts_fit_dir):
            cut.logger.info('Loading the OLUTs from saved directory...')
            oluts_fit.load(oluts_fit_dir)
        else:
            cut.logger.info('Creating the OLUTs from train data...')
            oluts_fit.set_oluts(np.asarray(x_train), np.asarray(y_train))


        # Make regression plots on the phase1 training set to see how good
        # the fit did
        # if False:
        if True:
            # import IPython; IPython.embed();sys.exit()
            # fit_method.plot_color = 'grey'
            outdir_plots_phase2 = outdir + 'plots/phase2'
            dlogL_latex_labels = CONST.get_dlogL_latex_labels(sampler.search_trajectory_parameters_keys)
            plots.make_regression_plots(fit_method, x_train, y_train, outdir_plots_phase2, validation_data=validation_data, y_labels=dlogL_latex_labels)

        if False:
        # if True:
            outdir_plots_phase2 = outdir + 'plots/phase2'
            dlogL_latex_labels = CONST.get_dlogL_latex_labels(sampler.search_trajectory_parameters_keys)
            oluts_fit_for_plots = sampler.set_oluts_regressor()
            oluts_fit_for_plots.set_oluts(x_train, y_train)
            plots.make_regression_plots(oluts_fit_for_plots, x_train, y_train, outdir_plots_phase2, validation_data=validation_data, y_labels=dlogL_latex_labels)

        del x_data, y_data
    else:
        cut.logger.info(f'Phase 3 was on-going.')
        global_fit_dir = sampler.outdir + f'sampler_state/global_fit'
        global_fit_dir_specific = global_fit_dir + f'/{fit_method.save_dir_suffix}'
        if os.path.exists(global_fit_dir_specific):
            cut.logger.info(f'Loading the {fit_method.nick_name} fit from saved directory {global_fit_dir_specific}...')
            fit_method.load(global_fit_dir)
        else:
            cut.logger.warning(f'Directory: {global_fit_dir_specific} does not exist, cannot load the fit_method. Exiting here.')
            sys.exit()

        oluts_fit = sampler.set_oluts_regressor()
        oluts_fit_dir = sampler.outdir + 'sampler_state/oluts_fit'
        # if False:
        if os.path.exists(oluts_fit_dir):
            cut.logger.info(f'Loading the OLUTs from saved directory {oluts_fit_dir}...')
            oluts_fit.load(oluts_fit_dir)
        else:
            cut.logger.warning(f'Directory: {oluts_fit_dir} does not exist, cannot load the OLUTs as they were in phase 3.')


    # DELETE GATHERED DATA FROM PHASE1
    # They are now loaded in the fit_method and oluts_fit already hence
    # we can save up a bit of memory
    del qpos_traj_phase1, dlogL_traj_phase1
    # import IPython; IPython.embed();sys.exit()
    # sys.exit()



    #*************************************************************************************************/
    #******************************** Phase 3.0 : Tuning epsilon *************************************/
    #*************************************************************************************************/
    cut.logger.info("----------------------------------------------------------------------------")
    cut.logger.info("PHASE 2.2 : 1/ TUNING EPSILON")
    tuning_epsilon = True
    tuning_nsrt = False
    nsrt = 6
    traj_type_analytical = f'analy-{opts.fit_method}'
    traj_type_hybrid = 'hybrid'
    traj_type_numerical = 'numerical'
    epsilon_hyb_num = 5e-3
    length_traj_min = 50
    length_traj_stuck_min = 20
    acc_threshold = 0.4
    if tuning_epsilon and not(phase3_ongoing):
        start_tune_eps = time.time()
        locs_eps = []
        accs_eps = []
        costs_adhoc_eps = []
        time_per_traj_eps = []
        costs_eps = []
        costs_timed_eps = []

        eps_low = 5e-3
        eps_high = 6e-2

        loc_eps = 5e-3
        # loc_eps = 3e-2
        cost_adhoc_eps, acc_eps = stp.evaluate_cost_and_acc_for_eps(loc_eps, sampler, fit_method, oluts_fit, nsrt)
        locs_eps.append(loc_eps)
        accs_eps.append(acc_eps)
        costs_adhoc_eps.append(cost_adhoc_eps)

        # Step 1: make sure that the stepsize with which we will start Newton's method produces an acceptance rate > 40%
        while acc_eps < acc_threshold:
            cut.logger.info(f'As the acceptance rate for eps = {loc_eps} was less than {acc_threshold:.0%}, trying with a new eps = {loc_eps/2}')
            loc_eps /= 2
            # loc_eps = min(sampler.epsilon0, loc_eps-5e-3)
            cost_adhoc_eps, acc_eps = stp.evaluate_cost_and_acc_for_eps(loc_eps, sampler, fit_method, oluts_fit, nsrt)
            locs_eps.append(loc_eps)
            accs_eps.append(acc_eps)
            costs_adhoc_eps.append(cost_adhoc_eps)

        # Step 2: Newton's method
        cut.logger.info(f"Starting Newton's method with eps = {loc_eps:.2e}")
        for it_newton in range(3):
            loc_eps = loc_eps + loc_eps * (1 - 8 * loc_eps)/(1 - 16 * loc_eps)
            cut.logger.info(f"Newton's method proposes a stepsize: {loc_eps:.2e}.")
            if loc_eps < eps_low or eps_high < loc_eps:
                cut.logger.info(f"This stepsize is outside of [{eps_low},{eps_high}]; thus setting the upper bound for the biscetion method directly to {eps_high}.")
                loc_eps = eps_high
                locs_eps.append(loc_eps)
                accs_eps.append(-1)
                costs_adhoc_eps.append(np.inf)
                break
            else:
                cost_adhoc_eps, acc_eps = stp.evaluate_cost_and_acc_for_eps(loc_eps, sampler, fit_method, oluts_fit, nsrt)
                locs_eps.append(loc_eps)
                accs_eps.append(acc_eps)
                costs_adhoc_eps.append(cost_adhoc_eps)
                if acc_eps < acc_threshold:
                    cut.logger.info(f"The acceptance rate for eps = {loc_eps:.2e} was {acc_eps:.2%} < {acc_threshold:.0%}, stopping Newton's method here.")
                    break

        # Step 3: Bisection method
        eps_bisec_low = locs_eps[-2]
        eps_bisec_high = locs_eps[-1]
        cost_eps_bisec_low = costs_adhoc_eps[-2]
        cost_eps_bisec_high = costs_adhoc_eps[-1]
        acc_eps_bisec_low = accs_eps[-2]
        acc_eps_bisec_high = accs_eps[-1]
        for it_bisec in range(3):
            cut.logger.info(f'Boundaries for bisection set to [{eps_bisec_low:.2e},{eps_bisec_high:.2e}].')
            # Set the new stepsize to test in the logarithmic middle of the two boundaries
            loc_eps = 10**(np.log10(eps_bisec_low) + (np.log10(eps_bisec_high) - np.log10(eps_bisec_low)) / 2)
            cut.logger.info(f'Testing the (logarithmic) middle stepsize of {loc_eps:.2e}')
            cost_adhoc_eps, acc_eps = stp.evaluate_cost_and_acc_for_eps(loc_eps, sampler, fit_method, oluts_fit, nsrt)
            locs_eps.append(loc_eps)
            accs_eps.append(acc_eps)
            costs_adhoc_eps.append(cost_adhoc_eps)
            got_smaller_cost = cost_adhoc_eps < max(cost_eps_bisec_low, cost_eps_bisec_high)
            if acc_eps > acc_threshold and got_smaller_cost and cost_eps_bisec_low > cost_eps_bisec_high:
                cut.logger.info(f'The acceptance rate ({acc_eps:.1%}) is higher than {acc_threshold:.1%}, the cost is smaller than that of the boundaries: {cost_adhoc_eps:.2f} vs [{cost_eps_bisec_low:.2f},{cost_eps_bisec_high:.2f}], and the cost for the lower boundary is greater than that of the upper boundary.')
                cut.logger.info(f'Therefore setting the mid-point as the new lower boundary.')
                eps_bisec_low = loc_eps
                cost_eps_bisec_low = cost_adhoc_eps
                acc_eps_bisec_low = acc_eps
            else:
                if cost_eps_bisec_low <= cost_eps_bisec_high:
                    cut.logger.info(f'The low boundary had produced a cost smaller than that of the upper boundary ({cost_eps_bisec_low:.2f} vs {cost_eps_bisec_high:.2f}).')
                elif not(got_smaller_cost):
                    cut.logger.info(f'The new cost obtained for {loc_eps:.2e} was bigger than the two previous ones: {cost_adhoc_eps:.2f} vs [{cost_eps_bisec_low:.2f},{cost_eps_bisec_high:.2f}].')
                else:
                    cut.logger.info(f'The acceptance rate for {loc_eps:.2e} ({acc_eps:.2%}) is less than {acc_threshold:.0%}.')
                cut.logger.info(f'Therefore setting the mid-point as the new upper boundary.')
                eps_bisec_high = loc_eps
                cost_eps_bisec_high = cost_adhoc_eps
                acc_eps_bisec_high = acc_eps

        locs_eps = np.asarray(locs_eps)
        costs_adhoc_eps = np.asarray(costs_adhoc_eps)
        accs_eps = np.asarray(accs_eps)
        mask_acc_high_enough = np.where(accs_eps >= acc_threshold)[0]
        idx_opt = np.argmin(costs_adhoc_eps[mask_acc_high_enough])
        eps_opt = locs_eps[mask_acc_high_enough][idx_opt]
        acc_opt = accs_eps[mask_acc_high_enough][idx_opt]

        df_costs = pd.DataFrame([accs_eps, costs_adhoc_eps], columns=[f'{eps:.2e}' for eps in locs_eps], index=['Acc', 'C = 1/(a*(eps-8*eps^2)'])
        cut.logger.info(f'Summary of the stepsize optimization:\n {df_costs}')

        # time_per_traj_opt = time_per_traj_eps[idx_opt]
        cut.logger.info(f'Setting the stepsize for Phase III to epsilon = {eps_opt:.2e}.')
        duration_tun_eps = time.time() - start_tune_eps
        cut.logger.info(f'Stepsize optimization lasted {duration_tun_eps/60:.0f}min.')
        # sys.exit()
    else:
        eps_opt = 2.37e-02
        acc_opt = 0.403
        time_per_traj_opt = 1.063490
        cut.logger.info(f'')
        cut.logger.info(f'No tuning of stepsize, setting directly epsilon to {eps_opt} and expected acc to {acc_opt:.1%}')
    if tuning_nsrt and not(phase3_ongoing):
        cut.logger.info("----------------------------------------------------------------------------")
        cut.logger.info("PHASE 2.2 : 2/ TUNING n_srt")
        # Create the table of nsrt which will be used to compute the timed costs.
        # Note that `nsrt_to_test` contains the previous value used in the tuning of epsilon
        # and on which we won't rerun as we already have the result.
        # nsrt_to_test =  np.ones(nb_nsrt_to_test) * nsrt
        # inc_step = 1
        # increments = np.arange(nb_nsrt_to_test) * inc_step
        # for i, inc in enumerate(increments):
        #     nsrt_to_test[i] += inc
        # nsrt_to_test = nsrt_to_test.T.astype(int)
        nb_nsrt_to_test = 5
        nsrt_max = nsrt + nb_nsrt_to_test
        nsrt_max = 18
        nsrt_to_test = np.arange(nsrt, nsrt_max + 1, step=3)
        nsrt_opt = nsrt
        accs_nsrt = [acc_opt]
        time_per_traj_nsrt = [time_per_traj_opt]
        acc_expected = acc_opt
        n_traj_for_cost = 400
        for j, nsrt in enumerate(nsrt_to_test[1:]):
            count, n_traj_already_run, duration1, Acc, q_pos, dlogL, logL, randGen, _, _, _, count_phase3, duration2, duration3, atr, htr, ntr, atr_time, htr_time, ntr_time, stuck_count, hybrid_stuck, next_traj_to_check_sis  = sampler.get_state()
            sampler.set_scales_from_covariance_matrix()
            if opts.fit_method == 'cubic':
                oluts_fit = sampler.set_oluts_regressor()
                cut.logger.info('Creating the OLUTs from phase1 data...')
                oluts_fit.set_oluts(np.asarray(x_train), np.asarray(y_train))

            # n_traj_for_cost = sampler.n_dim * 5
            cut.logger.info(f'Running {n_traj_for_cost} with epsilon = {eps_opt}, n_srt = {nsrt}')

            start = time.time()
            acc_nsrt = 1
            count_nsrt = 0
            stuck_count = 0
            hybrid_stuck = 0
            length_traj_min = 50
            length_traj_stuck_min = 20

            # HMC using analytical fitting values for the gradients of the log-likelihood
            for traj_index in range(1, n_traj_for_cost + 1):
                # Draw epsilon according to a Gaussian law limited between 0.001 and 0.01
                # rannum = randGen.normal(loc=0, scale=1.5)
                # epsilon = sampler.epsilon0 + rannum * 1.0e-3
                scale_eps = 0.3 * eps_opt
                epsilon = randGen.normal(loc=eps_opt, scale=scale_eps)
                # epsilon = randGen.normal(loc=sampler.epsilon0, scale=0.3 * sampler.epsilon0) * 1.0e-3
                if epsilon < eps_opt - 2 * scale_eps:
                    epsilon = eps_opt - 2 * scale_eps
                if epsilon > eps_opt + 2 * scale_eps:
                    epsilon = eps_opt + 2 * scale_eps

                if stuck_count < nsrt:
                    length_traj = length_traj_min + int(randGen.uniform() * 100)
                    q_pos, logL, dlogL, Accept, a, proba_acc = bht.AnalyticalGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, fit_method, oluts_fit, traj_index, sampler)
                    atr += 1
                    traj_type = traj_type_analytical
                	# Compute the acceptance rate along with the local acceptance rate
                    if Accept == 1:
                        traj_status = "** ACCEPTED **"
                        stuck_count = 0
                        count_nsrt += 1
                        acc_nsrt = count_nsrt / atr
                        hybrid_stuck = 0
                    elif Accept == 0:
                        traj_status = "   rejected   "
                        stuck_count +=1
                        acc_nsrt = count_nsrt / atr
                        # acc_nsrt = count_nsrt / traj_index
                        if traj_type == traj_type_hybrid:
                            hybrid_stuck += 1
                else:
                    # if False:
                    if hybrid_stuck == 0: # Try first a hybrid trajectory with fewer steps
                        epsilon_stuck = eps_opt / 2
                        length_traj = length_traj_stuck_min +  int(randGen.uniform() * 80)
                        q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.HybridGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon_stuck, length_traj, fit_method, traj_index, sampler)
                        traj_type = traj_type_hybrid
                    else: # if hybrid trajectory did not work, switch to full  numericaltrajectory
                        epsilon_stuck = eps_opt / 2
                        length_traj = length_traj_stuck_min +  int(randGen.uniform() * 80)
                        q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.NumericalGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon_stuck, length_traj, traj_index, sampler)
                        traj_type = traj_type_numerical
                    # Compute the acceptance rate and check whether the trajector was hybrid or numerical
                    if Accept == 1:
                        traj_status = "** ACCEPTED **"
                        stuck_count = 0
                        hybrid_stuck = 0
                        # count_nsrt += 1
                        # acc_nsrt = count_nsrt / traj_index
                        if opts.fit_method == 'cubic':
                            oluts_fit.update_oluts(qpos_from_traj, dlogL_from_traj)
                    elif Accept == 0:
                        traj_status = "   rejected   "
                        stuck_count += 1
                        hybrid_stuck += 1
                        # acc_nsrt = count_nsrt / traj_index

                if a < proba_acc:
                    sup_or_inf = '<'
                else:
                    sup_or_inf = '>'
                cut.logger.info("{:6} - {} - (eps, n_srt) = ({:5.2e}, {}) - Acc-ana = {:.1%} - {:11} traj - {:1.5f} {} {:1.5f}".format(traj_index, traj_status, eps_opt, nsrt, acc_nsrt, traj_type, a, sup_or_inf, proba_acc))

                duration_so_far = time.time() - start
                if duration_so_far > 5 * 60:
                    cut.logger.info(f'More than 5min spent on this case. Going to the next.')
                    break

            stop = time.time()
            time_per_traj = (stop - start) / traj_index
            cut.logger.info(f'Time / traj = {time_per_traj:.2f} s/traj')

            accs_nsrt.append(acc_nsrt)
            time_per_traj_nsrt.append(time_per_traj)
            if acc_nsrt >= 0.8 * acc_opt:
                cut.logger.info(f'The acceptance rate is {acc_nsrt:.1%} > 0.8 * {acc_opt:.1%}, so the new optimal n_srt is set to {nsrt}.')
                nsrt_opt = nsrt
                acc_expected = acc_nsrt
            else:
                cut.logger.info(f'The acceptance rate for n_srt={nsrt} is {acc_nsrt:.1%} < 0.8 * {acc_opt:.1%}, so we keep {nsrt_opt} as optimal n_srt and break here.')
                break
        df_nsrts = pd.DataFrame([accs_nsrt, time_per_traj_nsrt], columns=[f'{nsrt}' for nsrt in nsrt_to_test], index=['Acc', 'time / traj'])
        cut.logger.info(f'Benchmark of n_srt gave:\n {df_nsrts}')
        cut.logger.info(f'Hence the stepsize and successive-rejection-threshold are set to (epsilon, n_srt) = ({eps_opt:.2e}, {nsrt_opt}), producing an expected acceptance rate of {acc_expected:.1%}.')
        sampler.epsilon0 = eps_opt
    else:
        sampler.epsilon0 = eps_opt
        nsrt_opt = 6
        nsrt = nsrt_opt
        acc_expected = acc_opt
        cut.logger.info(f'No tuning of n_srt, setting it directly to {nsrt_opt} and expected acc to {acc_expected:.1%}')

    # sys.exit()


    if not(phase3_ongoing):
        # Check whether user only wants to run Phases I and II,
        # in which case we don't discard Phase I samples yet otherwise
        # an empty file will be saved and the future load will see
        # sampler.samples = [] which will bug the derivations of scales
        # from the covariance matrix
        if sampler.n_traj_hmc_tot == sampler.n_traj_fit:
            cut.logger.info(f"Only Phase I and II were asked to be run as n_traj_hmc_tot = {sampler.n_traj_hmc_tot}. Quitting here.")
            sys.exit()
        cut.logger.info("----------------------------------------------------------------------------")
        cut.logger.info("PHASE 2.3 : SCALES")
        old_scales = sampler.scales.copy()
        sampler.set_scales_from_covariance_matrix()
        if oluts_fit.__class__ == fnr.OLUTsRegressor:
            oluts_fit.squared_scales = sampler.scales**2
        cut.logger.info("Discarding samples from Phase I.")
        sampler.samples = []
        sampler.chain_metrics = []

        duration2 += time.time() - start_phase2
        cut.logger.info(f'Phase II lasted a total of {duration2/60:.0f}min.')


    #*************************************************************************************************/
    #******************************** Phase 3 : Analytical HMC   *************************************/
    #*************************************************************************************************/
    cut.logger.info("****************************************************************************")
    cut.logger.info("PHASE 3: ANALYTICAL PART")


    # Update scales every 1e4 trajectories with the covariance matrix
    keep_updating_scales = True
    traj_type_analytical = f'analy-{opts.fit_method}'
    traj_type_hybrid = 'hybrid'
    traj_type_numerical = 'numerical'
    n_consec_nsrt_rej = 0
    n_additional_num_to_run_in_row = 0
    n_consec_num_rej = 0

    idx_start_phase3 = max(sampler.n_traj_fit + 1, n_traj_already_run + 1)
    idx_end_phase3_asked = min(sampler.n_traj_hmc_tot, l_start + sampler.n_traj_for_this_run -1)
    traj_index_phase3 = idx_start_phase3 - sampler.n_traj_fit - 1
    break_before_enough_sis = True
    n_traj_left_to_run_a_priori = idx_end_phase3_asked
    if idx_start_phase3 <= idx_end_phase3_asked:
        start = time.time()
        stuck_seq_count_dict = {}
        cut.logger.info(f'Stuck count before running hybrid traj set to: {nsrt}')
        acc_low_1 = 0.75 * acc_expected
        # acc_low_1 = 0.52 * acc_expected
        acc_low_2 = 0.5 * acc_expected
        cut.logger.info(f'Will run hybrid trajectories instead of analytical if acc gets < {acc_low_1:.1%}'
                        f' and numerical only if < {acc_low_2:.1%}%.')

        Acc_phase3 = 1

        length_traj_min = 50
        length_traj_stuck_min = 20

        # HMC using analytical fitting values for the gradients of the log-likelihood
        try:
            for traj_index in range(idx_start_phase3, idx_end_phase3_asked + 1):
                # Every 1e4 trajectories set the scales of each parameter
                # to its standard dev derived from the covariance matrix
                # if that accounts for more than 10% difference with the
                # previous scale value
                if (traj_index % 1e4 == 0 and keep_updating_scales):
                    old_scales = sampler.scales.copy()
                    sampler.set_scales_from_covariance_matrix()
                    if oluts_fit.__class__ == fnr.OLUTsRegressor:
                        oluts_fit.squared_scales = sampler.scales**2
                    at_least_ten_percent_change = np.abs((old_scales - sampler.scales)/old_scales > 0.1)
                    keep_updating_scales = np.any(at_least_ten_percent_change)


                traj_index_phase3 = traj_index - sampler.n_traj_fit
                # Draw epsilon according to a Gaussian law limited between 0.001 and 0.01
                # rannum = randGen.normal(loc=0, scale=1.5)
                # epsilon = sampler.epsilon0 + rannum * 1.0e-3
                loc_eps = sampler.epsilon0
                scale_eps = 0.3 * loc_eps
                epsilon = randGen.normal(loc=loc_eps, scale=scale_eps)
                # epsilon = randGen.normal(loc=sampler.epsilon0, scale=0.3 * sampler.epsilon0) * 1.0e-3
                if epsilon < loc_eps - 2 * scale_eps:
                    epsilon = loc_eps - 2 * scale_eps
                if epsilon > loc_eps + 2 * scale_eps:
                    epsilon = loc_eps + 2 * scale_eps

                # The `run_add_num` boolean enables running as many numerical traj in a row as there were "stuck sequences" before. This will free the chain faster hence tempering the harm done by successive `nsrt + 1` rejections sequences on the autocorrelation.
                run_add_num = n_additional_num_to_run_in_row > 0
                run_add_num = False


                # Check if the code was stuck for `nsrt` consecutive trajectories
                if stuck_count < nsrt and not(run_add_num):
                	# Main HMC routines. Depending on the value of acceptance rate, we either use the full analytcal trajectories,
                	# the hybrid trajectories or the full  numericaltrajectories.
                    # if True:
                    if acc_low_1 <= Acc_phase3 or traj_index_phase3 < sampler.n_dim * 50:
                        length_traj = length_traj_min + int(randGen.uniform() * 100)
                        # length_traj = length_traj_min
                        # if traj_index == 69573: breakpoint()
                        # import IPython; IPython.embed();sys.exit()
                        atr_start = time.time()
                        q_pos, logL, dlogL, Accept, a, proba_acc = bht.AnalyticalGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, fit_method, oluts_fit, traj_index, sampler)
                        atr += 1
                        atr_time += time.time() - atr_start
                        traj_type = traj_type_analytical
                    elif False:
                    # elif (acc_low_2 <= Acc_phase3) and (Acc_phase3 < acc_low_1) and (hybrid_stuck < 2):
                        length_traj = length_traj_min +  int(randGen.uniform() * 50)
                        epsilon = epsilon_hyb_num
                        htr_start = time.time()
                        q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.HybridGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, fit_method, traj_index, sampler)
                        htr += 1
                        htr_time += time.time() - htr_start
                        traj_type = traj_type_hybrid
                    else:
                        length_traj = length_traj_min +  int(randGen.uniform() * 50)
                        epsilon = epsilon_hyb_num
                        ntr_start = time.time()
                        q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.NumericalGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, traj_index, sampler)
                        ntr += 1
                        ntr_time += time.time() - ntr_start
                        traj_type = traj_type_numerical
                	# Compute the acceptance rate along with the local acceptance rate
                    if Accept == 1:
                        traj_status = "** ACCEPTED **"
                        count += 1
                        Acc = count/traj_index
                        if stuck_count != 0:
                            stuck_seq_and_traj_list.append([traj_index, stuck_count])
                        stuck_count = 0
                        n_consec_nsrt_rej = 0
                        n_additional_num_to_run_in_row = 0
                        count_phase3 += 1
                        Acc_phase3 = count_phase3 / traj_index_phase3
                        hybrid_stuck = 0
                        n_consec_num_rej = 0
                    elif Accept == 0:
                        traj_status = "   rejected   "
                        Acc = count/traj_index
                        stuck_count +=1
                        Acc_phase3 = count_phase3 / traj_index_phase3
                        if traj_type == traj_type_hybrid:
                            hybrid_stuck += 1
                        n_consec_num_rej = 0
                else:
                    # if False:
                    if hybrid_stuck == 0 and not(run_add_num): # Try first a hybrid trajectory with fewer steps
                        # epsilon = sampler.epsilon0 / 2
                        epsilon = epsilon_hyb_num
                        length_traj = length_traj_stuck_min +  int(randGen.uniform() * 80)
                        htr_start = time.time()
                        q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.HybridGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, fit_method, traj_index, sampler)
                        traj_type = traj_type_hybrid
                        htr += 1
                        htr_time += time.time() - htr_start
                    else: # if hybrid trajectory did not work, switch to full  numericaltrajectory
                        # epsilon = (sampler.epsilon0 / 2) / (1 + n_consec_num_rej)
                        # length_traj = (length_traj_stuck_min + int(randGen.uniform() * 80)) * (1 + n_consec_num_rej)
                        # epsilon = sampler.epsilon0 / 2
                        epsilon = epsilon_hyb_num
                        length_traj = length_traj_stuck_min + int(randGen.uniform() * 80)
                        ntr_start = time.time()
                        q_pos, logL, dlogL, qpos_from_traj, dlogL_from_traj, Accept, a, proba_acc = bht.NumericalGradientLeapfrog_ThreeDetectors(q_pos, logL, dlogL, randGen, epsilon, length_traj, traj_index, sampler)
                        ntr += 1
                        ntr_time += time.time() - ntr_start
                        traj_type = traj_type_numerical
                    # Compute the acceptance rate and check whether the trajector was hybrid or numerical
                    if Accept == 1:
                        traj_status = "** ACCEPTED **"
                        count += 1
                        Acc = count/traj_index
                        if stuck_count != 0:
                            stuck_seq_and_traj_list.append([traj_index, stuck_count])
                        # If `stuck_count >= nsrt, it means that this accepted traj we just run has unstuck the chain from a rej sequence
                        if stuck_count >= nsrt:
                            n_consec_nsrt_rej +=1
                            # -1 because we just ran 1 non-analytical traj which was accepted
                            n_additional_num_to_run_in_row = n_consec_nsrt_rej - 1
                        else:
                            n_additional_num_to_run_in_row -= 1
                        stuck_count = 0
                        hybrid_stuck = 0
                        count_phase3 += 1
                        Acc_phase3 = count_phase3 / traj_index_phase3
                        n_consec_num_rej = 0
                    elif Accept == 0:
                        traj_status = "   rejected   "
                        Acc = count/traj_index
                        stuck_count += 1
                        hybrid_stuck += 1
                        Acc_phase3 = count_phase3 / traj_index_phase3
                        # Count  the number of consecutive numerical trajectories being rejected
                        if traj_type == traj_type_numerical:
                            n_consec_num_rej += 1

        		# Record the position of the chain
                sampler.samples.append(q_pos.tolist())
                # import IPython; IPython.embed();sys.exit()
                if Accept == 1:
                    if sampler.likelihood.phase_marginalization:
                        mf_snr = sampler.likelihood.snr_dict['network']['matched_filter_snr_phasemarg']
                    else:
                        mf_snr = sampler.likelihood.snr_dict['network']['matched_filter_snr']
                sampler.chain_metrics.append([mf_snr, logL, Acc_phase3, Accept])

                # Print position of the chain
                # if traj_index%int(sampler.n_traj_for_this_run/20000)==0:
                # if traj_index%int(sampler.n_traj_for_this_run/200)==0:
                if True:
                    if a < proba_acc:
                        sup_or_inf = '<'
                    else:
                        sup_or_inf = '>'
                    cut.logger.info("{:6} - {} - AccP3 = {:.1%} - {:11} traj - {:1.5f} {} {:1.5f} - len = {:3} - eps = {:.2e}".format(traj_index, traj_status, Acc_phase3, traj_type, a, sup_or_inf, proba_acc, length_traj, epsilon))

                # UPDATE TABLES FOR THE CUBIC FIT AND OLUTS IF PERTINENT
                # It is important NOT to convert to numpy arrays here, outside the
                # `if flag_conduct_new_fit` of the new_fit() function
                # but inside because x_data and y_data being large arrays
                # their transformations to np.array takes ~200ms each; since
                # .new_fit() is called at the end of each traj here, that would
                # double the run time (1 anal traj is also ~200ms)
                if traj_type in [traj_type_hybrid, traj_type_numerical] and Accept:
                    if opts.fit_method == 'cubic':
                        oluts_fit.update_oluts(qpos_from_traj, dlogL_from_traj)
                    if traj_type == traj_type_numerical:
                        fit_method.propose_new_fit(qpos_from_traj, dlogL_from_traj)


                # Check the number of SIS accumulated.
                # Only start this check if traj_index > CONST.N_TRAJ_TO_CHECK_SIS_DEFAULT to
                # leave enough length for the integrated autocorr time `ACLmax`
                # to converge. Do the check at the end of run anyway also.
                if (traj_index >= CONST.N_TRAJ_TO_CHECK_SIS_DEFAULT and traj_index == next_traj_to_check_sis) or traj_index == idx_end_phase3_asked:
                    cut.logger.info(f'Checking SIS on Phase III samples only...')
                    ACLmax = cautocorr.get_maximum_integrated_time(np.asarray(sampler.samples))
                    n_sis_accumulated = int(len(sampler.samples) / ACLmax)
                    n_sis_to_get = sampler.n_sis - n_sis_accumulated
                    cut.logger.info(f'~{n_sis_accumulated} SIS out of {len(sampler.samples)} samples since ACLmax={ACLmax}.')
                    if n_sis_to_get <= 0:
                        cut.logger.info(f'{sampler.n_sis} were asked hence breaking loop here.')
                        break_before_enough_sis = False
                        break
                    else:
                        n_traj_left_to_run_a_priori = sampler.n_sis * ACLmax - len(sampler.samples)
                        n_traj_to_check_sis = min(CONST.N_TRAJ_TO_CHECK_SIS_DEFAULT, n_traj_left_to_run_a_priori)
                        next_traj_to_check_sis = traj_index + n_traj_to_check_sis
                        cut.logger.info(f' Missing {n_sis_to_get} SIS => continuing phase3 for ~{n_traj_left_to_run_a_priori}. Next check in {n_traj_to_check_sis} traj at trajectory #{next_traj_to_check_sis}.')


                if traj_index % CONST.N_TRAJ_TO_SAVE_STATE == 0 and traj_index != idx_start_phase3:
                    sampler.save_state(
                        count, traj_index, duration1, Acc, q_pos, dlogL, logL, randGen,
                        fit_method=fit_method,
                        oluts_fit=oluts_fit,
                        stuck_seq_and_traj_list= np.asarray(stuck_seq_and_traj_list),
                        count_phase3=count_phase3,
                        duration2=duration2,
                        duration3=duration3 +  time.time() - start,
                        atr=atr,
                        htr=htr,
                        ntr=ntr,
                        atr_time=atr_time,
                        htr_time=htr_time,
                        ntr_time=ntr_time,
                        stuck_count=stuck_count,
                        hybrid_stuck=hybrid_stuck,
                        next_traj_to_check_sis=next_traj_to_check_sis
                        )

                # END OF PHASE 3 LOOP !
        except KeyboardInterrupt:
            traj_index -= 1
            save_state = input('Do you want to save the state of the run?\n=> ')
            if save_state.lower() not in ['y', 'yes', 'oui', 'ya', 'da', 'si', 'yep']:
                cut.logger.info('Quitting here without saving.')
                sys.exit()
        # Check the final time of the routine
        stop = time.time()
        duration3 += stop - start

        stuck_seq_and_traj_list = np.asarray(stuck_seq_and_traj_list)
        if idx_end_phase3_asked <= sampler.n_traj_hmc_tot:
            # cut.logger.info("Quitting here, there remains {} traj to run in phase3.".format(sampler.n_traj_hmc_tot - idx_end_phase3_asked))
            sampler.save_state(
                count, traj_index, duration1, Acc, q_pos, dlogL, logL, randGen,
                fit_method=fit_method,
                oluts_fit=oluts_fit,
                stuck_seq_and_traj_list=stuck_seq_and_traj_list,
                count_phase3=count_phase3,
                duration2=duration2,
                duration3=duration3,
                atr=atr,
                htr=htr,
                ntr=ntr,
                atr_time=atr_time,
                htr_time=htr_time,
                ntr_time=ntr_time,
                stuck_count=stuck_count,
                hybrid_stuck=hybrid_stuck,
                next_traj_to_check_sis=next_traj_to_check_sis
                )

        if RUN_CONST.plot_traj:
            np.savetxt(sampler.plot_traj_dir + 'H_p_logL_logP.dat', sampler.dict_of_trajectories['H_p_logL_logP'])
            np.savetxt(sampler.plot_traj_dir + 'pmom_trajs.dat', sampler.dict_of_trajectories['pmom_trajs'])
            np.savetxt(sampler.plot_traj_dir + 'qpos_traj.dat', sampler.dict_of_trajectories['qpos_traj'])
            np.savetxt(sampler.plot_traj_dir + 'dlogL.dat', sampler.dict_of_trajectories['dlogL'])
            np.savetxt(sampler.plot_traj_dir + 'dlogTrajPi_traj.dat', sampler.dict_of_trajectories['dlogTrajPi_traj'])
            np.savetxt(sampler.plot_traj_dir + 'lengths.dat', sampler.dict_of_trajectories['lengths'], fmt='%.0f')

        sampler.plot_all_corners()
        sampler.plot_autocorrelations_of_parameters()
        sampler.plot_walkers()


        cut.logger.info(f"Duration of phase3 code : {duration3 / 3600:.3} hours")
        cut.logger.info('Average time to compute 1 phase3 trajectory: {:.2f} sec'.format(duration3 / traj_index_phase3))
        if Acc_phase3 != 0:
            cut.logger.info('Average time to get 1 accepted trajectory  : {:.2f} sec'.format(duration3 / (traj_index_phase3 * Acc_phase3)))
            sis_samples = sampler.get_posterior(sis=True)
            cut.logger.info('Average time to get 1 SIS in phase3        : {:.2f} sec'.format(duration3 / len(sis_samples)))
            samples_ph3 = np.asarray(sampler.samples)
            acl_max = cautocorr.get_maximum_integrated_time(samples_ph3)
            nsis_ph3 = len(samples_ph3)/acl_max
            eff_ph3 = duration3 / nsis_ph3
            acl_mean = 0
            for i, chain in enumerate(samples_ph3.T):
                acl = cautocorr.autocorrelation_length_estimate(chain)
                acl = int(acl) + 1
                cut.logger.info(f'{i}: acl = {acl}')
                acl_mean += acl
            acl_mean /= samples_ph3.shape[1]
            ph3_5000sis_duration = eff_ph3 * 5000 / 3600
            cut.logger.info(f'acl_max = {acl_max}')
            cut.logger.info(f'acl_mean = {acl_mean}')
            cut.logger.info(f'eff_ph3 = {eff_ph3:.3} sec/SIS')
            cut.logger.info(f'ph3_5000sis_duration = {ph3_5000sis_duration:.3}h')
            cut.logger.info(f'time / analytical traj = {atr_time/atr*1000:.1f}ms')

        cut.logger.info("****************************************************************************")
        cut.logger.info(f"Out of {traj_index_phase3} phase3 trajectories : \n - {atr} were analytical ({atr/traj_index_phase3:.3%} - {atr_time/3600:.2f}h) \n - {htr} were hybrid ({htr/traj_index_phase3:.3%} - {htr_time/3600:.2f}h), \n - {ntr} were numerical  ({ntr/traj_index_phase3:.3%} - {ntr_time/3600:.2f}h)")
        if break_before_enough_sis and Acc_phase3 != 0:
            estimated_duration_to_end = sampler.n_sis * duration3 / len(sis_samples) + duration1
            cut.logger.info(f'{len(sis_samples)} SIS have been gathered out of {sampler.n_sis} asked. To get there, a total of ~{traj_index_phase3 + n_traj_left_to_run_a_priori} would be needed meaning the run would last ~{estimated_duration_to_end / 3600:.3} hours')
        cut.logger.info("Hence a total of {} cubic fit points and {} look-up table points".format(len(fit_method.x_trained), len(oluts_fit[0].olut_x)))
        cut.logger.info("The total runtime is : {:.3} hours".format((duration1 + duration2 + duration3) / 3600))


        # PLOT A HISTOGRAM OF THE PROPORTIONS OF K REJECTIONS IN A ROW
        stuck_seq_and_traj_list = np.asarray(stuck_seq_and_traj_list)
        # np.savetxt(outdir + 'sampler_state/rejection_sequences.dat', stuck_seq_and_traj_list)
        stuck_seq_list = stuck_seq_and_traj_list[:, 1]
        # Counts the nb of times an integer appears in the list. We exclude 0 then.
        seq_count_np = np.bincount(stuck_seq_list)[1:]
        bins = range(1, stuck_seq_list.max() + 1)
        import matplotlib.pyplot as plt
        plt.figure(figsize=(15,10))
        # plt.bar(bins, seq_count_np, width=0.2, align='center', log=True)
        plt.bar(bins, seq_count_np/seq_count_np.sum(), width=0.2, align='center', log=True)
        # plt.plot(kseq, Nkfit, color='r', label=r'$N_k^{fit} = ' + rf'{factor:.0f} \times {qfit:.1f}^k$')
        # plt.plot(bins, seq_count_np, '.', markersize=12)
        # plt.ylim(bottom=0.5)
        plt.yscale('log')
        plt.xlabel(r'$k$, number of rejections in a row')
        plt.ylabel(r'$P_k$, proportion of k rejections in a row')
        # plt.legend()
        # plt.title(rf'$P_k$ vs $k$, after {atr} analytical traj, $\epsilon={sampler.epsilon0}$, $Acc={Acc_phase3:.1%}$')
        for i in range(len(bins)):
            plt.text(x=bins[i]-0.1, y=seq_count_np[i]/seq_count_np.sum() * 1.1, s=f'{seq_count_np[i]/seq_count_np.sum():.3%}', size=12)
        plt.savefig(outdir + f'plots/{traj_index_phase3}_rejections_sequences_distribution_prop.png')

        if idx_end_phase3_asked <= sampler.n_traj_hmc_tot:
            cut.logger.info("Quitting here, there remains {} traj to run in phase3.".format(sampler.n_traj_hmc_tot - idx_end_phase3_asked))
            sys.exit()

    else:
        cut.logger.info('No phase3 trajectory to be run since n_traj_hmc_tot = n_traj_fit.')
        sampler.save_state(
            count, traj_index_phase3 + sampler.n_traj_fit, duration1, Acc, q_pos, dlogL, logL, randGen,
            fit_method=fit_method,
            oluts_fit=oluts_fit,
            stuck_seq_and_traj_list=stuck_seq_and_traj_list,
            count_phase3=count_phase3,
            duration2=duration2,
            duration3=duration3
            )
        sys.exit()

    cut.logger.info('ENDING THE RUN HERE !')
    sys.exit()
