import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import numpy as np

import bilby
import Library.bilby_utils as bilby_utils
from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.bilby_waveform as bilby_wv
import Library.python_utils as pu

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds




def main(template_ifos, interferometers, do_prints):

    logLBest, mf_snr_best, opt_snr_Best = bilby_utils.loglikelihood_snr(template_ifos, interferometers)

    if do_prints:
        print("")
        print("Optimal logL = {:.4f}".format(logLBest))
        print("Matched filter SNR = {:.4f}".format(mf_snr_best))
        print("Optimal SNR = {:.4f}".format(opt_snr_Best))
        print("")

    return logLBest, mf_snr_best, opt_snr_Best





if __name__=='__main__':
    from optparse import OptionParser
    usage = """%prog [options]
    Plots the three waveforms in the time domain"""
    parser=OptionParser(usage)
    parser.add_option("--psd",default=1,action="store",type="int",help="""1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.""",metavar="INT from {1,2,3}")
    parser.add_option("--ifos", default='H1,L1,V1', action="store", type="string", help=""" Interferometers to run the analysis on. Default is 'H1,L1,V1' """)

    (opts,args)=parser.parse_args()

    # Set up a random seed for result reproducibility.  This is optional!
    np.random.seed(88170235)

    # SET THE INJECTION PARAMETERS
    injection_parameters = set_inj.ini_file_to_dict()
    pu.print_dict(injection_parameters)
    minimum_frequency = injection_parameters['meta']['minimum_frequency']
    maximum_frequency_ifo = injection_parameters['meta']['maximum_frequency_ifo']
    duration = injection_parameters['meta']['duration']
    sampling_frequency = injection_parameters['meta']['sampling_frequency']
    f_high = injection_parameters['meta']['f_high']
    start_time = injection_parameters['meta']['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    ifos_possible = ['H1', 'L1', 'V1']
    ifos_chosen = opts.ifos.split(',')
    if set.intersection(set(ifos_possible), set(ifos_chosen)) != set(ifos_chosen):
        raise ValueError("the '--ifos' option was wrongly set, you must choose between {}. Example: '--ifos=H1,V1'. ".format(ifos_possible))
    interferometers = bilby.gw.detector.InterferometerList(ifos_chosen)
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        ifo.maximum_frequency = maximum_frequency_ifo
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=opts.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)

    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=start_time)

    # WAVEFORM GENERATION
    injection_parameters['meta']['maximum_frequency_generated_waveform'] = injection_parameters['meta']['maximum_frequency_injected_waveform']
    template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    for i in range(len(interferometers)):
        interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain

    # COMPUTE  AND PRINT SNR
    # Generate a template up to frequency of analysis
    injection_parameters['meta']['maximum_frequency_generated_waveform'] = injection_parameters['meta']['maximum_frequency_search_waveform']
    template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)
    logLBest, mf_snr_best, opt_snr_Best = main(template_ifos, interferometers, True)
