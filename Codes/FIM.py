import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import numpy as np
from scipy.linalg import lu
import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.bilby_waveform as bilby_wv
import Library.param_utils as paru
import Library.CONST as CONST

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds



def main(injection_parameters, start_time, interferometers, waveform_arguments, **sampler_dict):

    ######################################
    # FISHER MATRICES #

    ###################
    # classic decomp
    # Prepare scales	used in HMC equations using the Fisher matrix depending on option taken:
    #	option = 1 : full inversion of Fisher matrix
    #	other option : inverse of square root of diagonal FIM terms

    # Compute the Fisher Matrix
    FisherMatrix = bilby_wv.FisherMatrix_ThreeDetectors(injection_parameters, start_time, interferometers, waveform_arguments, **sampler_dict)

    # If I want to run the hmc on less than 9 dimensions, my 9x9 FIM will be really singular, hence I should remove the rows and columns of the parameters which I keep fixed to get a non-singular sub_FIM which I can invert, otherwise just run the last commented two lines.
    ixgrid = np.ix_(sampler_dict['search_parameter_indices'], sampler_dict['search_parameter_indices'])
    if len(sampler_dict['search_parameter_indices'])<9:
        sub_FIM = FisherMatrix[ixgrid]
        inverse_sub_FIM = np.linalg.inv(sub_FIM)
        sub_scale = np.sqrt(inverse_sub_FIM.diagonal())
        scale = np.zeros(9)
        scale[sampler_dict['search_parameter_indices']] = sub_scale
    else:
        InverseFisherMatrix = np.linalg.inv(FisherMatrix)
        scale = np.sqrt(InverseFisherMatrix.diagonal())

    # re-adjust scales if FIM is incorrect
    # if scale[0] >1.0: scale[0] = 1.0   		# assume 50% error in cosinc
    # if scale[1] > PI: scale[1] = PI   		# assume 50% error in phi_c
    # if scale[2]>PIb2: scale[2] = PIb2     # assume 50% error in psi
    # if scale[3]> 0.5: scale[3] = 0.5   		# assume 50% error in lnDL
    # classic decomp
    ###################

    ###################
    # SVD decomp of FIM

    # If I want to run the hmc on less than 9 dimensions, my 9x9 FIM will be really singular, hence I should remove the rows and columns of the parameters which I keep fixed to get a non-singular sub_FIM which I can invert, otherwise just run the last commented four lines.
    if len(sampler_dict['search_parameter_indices'])<9:
        u, s, vh = np.linalg.svd(sub_FIM)
        inverse_sub_FIM_SVD = vh.T * 1/s @ u.T
        sub_scale_SVD = np.sqrt(inverse_sub_FIM_SVD.diagonal())
        scale_SVD = np.zeros(9)
        scale_SVD[sampler_dict['search_parameter_indices']] = sub_scale_SVD
    else:
        # u * s @ vh = u @ np.diag(s) @ vh = FisherMatrix
        u, s, vh = np.linalg.svd(FisherMatrix)
        InverseFisherMatrix_SVD = vh.T * 1/s @ u.T
        scale_SVD = np.sqrt(InverseFisherMatrix_SVD.diagonal())

    # re-adjust scales if FIM is incorrect
    # if scale_SVD[0] >1.0: scale_SVD[0] = 1.0   		# assume 50% error in cosinc
    # if scale_SVD[1] > PI: scale_SVD[1] = PI   		# assume 50% error in phi_c
    # if scale_SVD[2]>PIb2: scale_SVD[2] = PIb2     # assume 50% error in psi
    # if scale_SVD[3]> 0.5: scale_SVD[3] = 0.5   		# assume 50% error in lnDL
    # SVD decomp of FIM
    ###################

    ###################
    # LU decomp of FIM

    # If I want to run the hmc on less than 9 dimensions, my 9x9 FIM will be really singular, hence I should remove the rows and columns of the parameters which I keep fixed to get a non-singular sub_FIM which I can invert, otherwise just run the last commented four lines.
    if len(sampler_dict['search_parameter_indices'])<9:
        p, l, u = lu(sub_FIM)
        inverse_sub_FIM_LU = np.linalg.inv(u) @ np.linalg.inv(l) @ np.linalg.inv(p)
        sub_scale_LU = np.sqrt(inverse_sub_FIM_LU.diagonal())
        scale_LU = np.zeros(9)
        scale_LU[sampler_dict['search_parameter_indices']] = sub_scale_LU
    else:
        p, l, u = lu(FisherMatrix)
        InverseFisherMatrix_LU = np.linalg.inv(u) @ np.linalg.inv(l) @ np.linalg.inv(p)
        scale_LU = np.sqrt(InverseFisherMatrix_LU.diagonal())

    # re-adjust scales if FIM is incorrect
    # if scale_LU[0] >1.0: scale_LU[0] = 1.0   		# assume 50% error in cosinc
    # if scale_LU[1] > PI: scale_LU[1] = PI   		# assume 50% error in phi_c
    # if scale_LU[2]>PIb2: scale_LU[2] = PIb2     # assume 50% error in psi
    # if scale_LU[3]> 0.5: scale_LU[3] = 0.5   		# assume 50% error in lnDL
    # LU decomp of FIM
    ###################
    return FisherMatrix, scale, scale_SVD




if __name__=='__main__':
    from optparse import OptionParser
    usage = """%prog [options]
    Plots the three waveforms in the time domain"""
    parser=OptionParser(usage)
    parser.add_option("--psd",default=1,action="store",type="int",help="""1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.""",metavar="INT from {1,2,3}")
    parser.add_option("--ifos", default='H1,L1,V1', action="store", type="string", help=""" Interferometers to run the analysis on. Default is 'H1,L1,V1' """)

    (opts,args)=parser.parse_args()


    # SET THE INJECTION PARAMETERS
    injection_parameters = set_inj.ini_file_to_dict()
    minimum_frequency = injection_parameters['meta']['minimum_frequency']
    duration = injection_parameters['meta']['duration']
    sampling_frequency = injection_parameters['meta']['sampling_frequency']
    f_high = injection_parameters['meta']['f_high']
    start_time = injection_parameters['meta']['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    ifos_possible = ['H1', 'L1', 'V1']
    ifos_chosen = opts.ifos.split(',')
    if set.intersection(set(ifos_possible), set(ifos_chosen)) != set(ifos_chosen):
        raise ValueError("the '--ifos' option was wrongly set, you must choose between {}. Example: '--ifos=H1,V1'. ".format(ifos_possible))
    interferometers = bilby.gw.detector.InterferometerList(ifos_chosen)
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd. But the interpolation is done automatically if `ifo.power_spectral_density_array` is called at some point.
    set_psds.main(interferometers, opt_psd=opts.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)


    # COMPUTE THE FISHER MATRICES
    FisherMatrix, scale, scale_SVD = main(injection_parameters, start_time, interferometers, waveform_arguments)


    print("\nValues of FIM:")
    print("FIM from Bilby")
    for i in range(sampler_dict['n_dim']):
        for j in range(sampler_dict['n_dim']):
            print('{:12.4e}'.format(FisherMatrix[i][j]), end=' ')
        print('')
    #
    # print("FIM from TaylorF2_Waveform_Threedetectors()")
    # for i in range(sampler_dict['n_dim']):
    #     for j in range(sampler_dict['n_dim']):
    #         print('{:12.4e}'.format(FisherMatrix_hmc[i][j]), end=' ')
    #     print('')

    ndscale = np.zeros(9)
    ndscale[0] = 1.705314e+00
    ndscale[1] = 3.188156e-01
    ndscale[2] = 1.027065e+02
    ndscale[3] = 1.561083e+00
    ndscale[4] = 2.813453e-05
    ndscale[5] = 9.993015e-04
    ndscale[6] = 3.007305e-03
    ndscale[7] = 8.457038e-03
    ndscale[8] = 1.403338e-06

    print("\nValue of scales")
    print("{:20} | {:20} | {:20} | {:20} | {:20} | {:20}".format("param name", "bilby linalg.svd()", "hmc in C", "bilby / hmc_C", "offset python", "param name"))
    for i in range(sampler_dict['n_dim']):
        print("{:20} | {:20.6e} | {:20.6e} | {:20.2f} | {:20.0e} | {:20}\t".format(CONST.TRAJ_PARAMETERS_KEYS[i], scale_SVD[i],  ndscale[i], scale_SVD[i]/ndscale[i], sampler_dict['parameter_offsets'][i], CONST.TRAJ_PARAMETERS_KEYS[i]))
