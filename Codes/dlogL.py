import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import numpy as np

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.bilby_waveform as bilby_wv
import Library.param_utils as paru
import Library.CONST as CONST
import Library.python_utils as pu

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr


def main(injection_parameters, start_time, interferometers, waveform_arguments, do_prints=False, **sampler_dict):

    dlogL, logL = bilby_wv.dlogL_ThreeDetectors(injection_parameters, start_time, interferometers, waveform_arguments, **sampler_dict)
    prefix = '\ndlogL with bilby '

    if do_prints:
        if sampler_dict['dlogL'] == 'dlogL2':
            info = prefix + 'using: dlogL = logL(p+offset) - logL(p-offset)'
        else:
            info = prefix + 'using: dlogL = <s|dh> - <h|dh>'
        print(info)
        print("{:15} | {:15}".format("     dlogL", "offsets"))
        for i in range(sampler_dict['n_dim']):
            print("{:15f} | {:6.0e}\t".format(dlogL[i], sampler_dict['parameter_offsets'][i]))

    return dlogL





if __name__=='__main__':
    from optparse import OptionParser
    usage = """%prog [options]
    Plots the three waveforms in the time domain"""
    parser=OptionParser(usage)
    parser.add_option("--psd",default=1,action="store",type="int",help="""1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.""",metavar="INT from {1,2,3}")
    parser.add_option("--dlogL", default='dlogL1', action="store", type="string", help=""" Method to use to compute the gradient of the log-likelihood. Use `dlogL1` to use  dlogL = <s|dh> - <h|dh>, `dlogL2` for dlogL = logL(p+offset) - logL(p-offset).""")
    parser.add_option("--ifos", default='H1,L1,V1', action="store", type="string", help=""" Interferometers to run the analysis on. Default is 'H1,L1,V1' """)
    parser.add_option("--parameter_offsets", default='707077777', action="store", type="string", help=""" -log10() of the offset used for central differencing on each parameter. Default is '707077777' meaning sampler_dict['parameter_offsets'] = [1e-07, 0, 1e-07, 0, 1e-07, 1e-07, 1e-07, 1e-07, 1e-07]. 0 Means analytical formula is used if available.""")


    (opts,args)=parser.parse_args()

    if len(opts.parameter_offsets)!=9:
        raise ValueError('You must define 9 offsets for argument `parameter_offsets` which is here of length {}'.format(len(opts.parameter_offsets)))
        sys.exit(1)

    exponents = [int(e) for e in list(opts.parameter_offsets)]
    # sampler_dict['parameter_offsets'] = [10**(-e) for e in exponents]
    sampler_dict['parameter_offsets'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    idx_nonzero = np.nonzero(exponents)[0]
    for i in idx_nonzero:
        sampler_dict['parameter_offsets'][i] = 10**(-exponents[i])
    sampler_dict['dlogL'] = opts.dlogL


    # Set up a random seed for result reproducibility.  This is optional!
    np.random.seed(88170235)

    # SET THE INJECTION PARAMETERS
    injection_parameters = set_inj.ini_file_to_dict()
    pu.print_dict(injection_parameters)
    minimum_frequency = injection_parameters['meta']['minimum_frequency']
    maximum_frequency = 2048
    # maximum_frequency_ifo = injection_parameters['meta']['maximum_frequency_ifo']
    duration = injection_parameters['meta']['duration']
    sampling_frequency = injection_parameters['meta']['sampling_frequency']
    f_high = injection_parameters['meta']['f_high']
    start_time = injection_parameters['meta']['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    ifos_possible = ['H1', 'L1', 'V1']
    ifos_chosen = opts.ifos.split(',')
    if set.intersection(set(ifos_possible), set(ifos_chosen)) != set(ifos_chosen):
        raise ValueError("the '--ifos' option was wrongly set, you must choose between {}. Example: '--ifos=H1,V1'. ".format(ifos_possible))
    interferometers = bilby.gw.detector.InterferometerList(ifos_chosen)
    for ifo in interferometers:
        # ifo.__class__ = bilby_detector.Interferometer
        ifo.minimum_frequency = minimum_frequency
        ifo.maximum_frequency = maximum_frequency_ifo
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=opts.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)

    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=start_time)

    # WAVEFORM GENERATION
    injection_parameters['meta']['maximum_frequency_generated_waveform'] = injection_parameters['meta']['maximum_frequency_injected_waveform']
    template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, interferometers, waveform_arguments)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    for i in range(len(interferometers)):
        interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain

    # COMPUTE  AND PRINT SNR
    # Generate a template up to frequency of analysis
    injection_parameters['meta']['maximum_frequency_generated_waveform'] = injection_parameters['meta']['maximum_frequency_search_waveform']
    template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, interferometers, waveform_arguments)
    logLBest, mf_snr_best, opt_snr_Best = main(template_ifos, interferometers, True)
