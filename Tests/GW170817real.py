# How to import real data of GW170817 and create the interferometer, psd and strain_data structures correctly

import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
# import time
# from optparse import OptionParser
# from configparser import ConfigParser
# import subprocess
# import pandas as pd
#
import bilby
#
# from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
# import Library.python_utils as pu
# import Library.param_utils as paru
# import Library.roq_utils as roqu
# import Library.BNS_HMC_Tools as bht
# import Library.Fit_NR as fnr
# import Library.CONST as CONST
# import Library.RUN_CONST as RUN_CONST
# import Library.bilby_waveform as bilby_wv
# import Library.bilby_utils as bilby_utils
# import Library.plots as plots
# import Library.likelihood_gradient as lg
# import Library.sklearn_fit as sklf
# import Library.initialization as init
#
# import Codes.set_injection_parameters as set_inj
# import Codes.set_psds as set_psds
# import Codes.logL_snr as logL_snr
# import Codes.dlogL as CdlogL
# import Codes.FIM as FIM
#
import gw.detector.networks as gwdn
# import core.sampler
# import core.utils as cut

import matplotlib.pyplot as plt
import Library.psd_utils as psd_utils
from bilby.gw.detector.strain_data import InterferometerStrainData

if __name__ == '__main__':


    hdf5_file_path_H1 = '../__input_data/GW170817/H-H1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
    hdf5_file_path_L1 = '../__input_data/GW170817/L-L1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
    hdf5_file_path_V1 = '../__input_data/GW170817/V-V1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
    hdf5_file_path_list = [hdf5_file_path_H1, hdf5_file_path_L1, hdf5_file_path_V1]

    hdf5_file_path = hdf5_file_path_H1
    hdf5_strain, hdf5_strain_start_time, duration, sampling_frequency, number_of_samples = psd_utils.get_strain_from_hdf5(hdf5_file_path)

    geocent_time=1187008882.43
    tc_3p5PN = 56.9
    flow_start_time = int(geocent_time - tc_3p5PN)
    segment_duration = int(tc_3p5PN) + 1 + 2
    segment_duration2 = int(geocent_time - flow_start_time + 2)

    idx_segment_start = int((flow_start_time - hdf5_strain_start_time) * sampling_frequency) + 1
    idx_segment_end = int((flow_start_time + segment_duration - hdf5_strain_start_time) * sampling_frequency)
    # idx_segment_end = int((flow_start_time + segment_duration - hdf5_strain_start_time) * sampling_frequency) + 1

    seg_strain = hdf5_strain[idx_segment_start:idx_segment_end+1]

    bilby_strain = InterferometerStrainData(minimum_frequency=30)
    bilby_strain.set_from_time_domain_strain(time_domain_strain=seg_strain, sampling_frequency=sampling_frequency, duration=segment_duration)




    H1 = gwdn.InterferometerList(['H1'])[0]
    H1.strain_data = bilby_strain

    GWTC1_GW170817_PSDs_file = '../__input_data/GW170817/GWTC1_GW170817_PSDs.dat'
    GWTC1_GW170817_PSDs = np.loadtxt(GWTC1_GW170817_PSDs_file)

    H1.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_array=GWTC1_GW170817_PSDs[:, 1], frequency_array=GWTC1_GW170817_PSDs[:, 0])
    # H1.plot_time_domain_data(bandpass_frequencies=None)



    import IPython; IPython.embed(); sys.exit()
    breakpoint()
