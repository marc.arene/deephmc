import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../')

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff

from scipy.special import i0e
from scipy.interpolate import interp1d

# def _setup_phase_marginalization(self):
#     self._bessel_function_interped = interp1d(
#         np.logspace(-5, 10, int(1e6)), np.logspace(-5, 10, int(1e6)) +
#         np.log([i0e(snr) for snr in np.logspace(-5, 10, int(1e6))]),
#         bounds_error=False, fill_value=(0, np.nan))
#
# nb_points = 100
# nb_points = int(1e6)
# arr1 = np.logspace(-5, 10, nb_points)
# # arr2 = np.logspace(-5, 10, nb_points) + np.log([i0e(snr) for snr in np.logspace(-5, 10, nb_points)])
# arr2 = np.log([i0e(snr) for snr in np.logspace(-5, 10, nb_points)])
#
# plt.plot(arr1, arr2)
# plt.show()


def autocorrelation(array, lag):
    numerator = 0
    mean = array.mean()
    for i in range(len(array) - lag):
        numerator += (array[i] - mean) * (array[i+lag] - mean)

    denominator = 0
    for i in range(len(array)):
        denominator += (array[i] - mean)**2

    return numerator / denominator

def autocorrelation_2(array, lag):
    numerator = 0
    mean = array.mean()
    for i in range(len(array) - lag):
        numerator += (array[i] - mean) * (array[i+lag] - mean)

    # denominator = 0
    # for i in range(len(array)):
    #     denominator += (array[i] - mean)**2

    return numerator

def autocorrelation_np(array, lag):
    if lag == 0:
        return 1
    else:
        mean = array.mean()
        array_minus_mean = array - mean
        array_numerator = array_minus_mean[:-lag] * array_minus_mean[lag:]
        array_denominator = array_minus_mean**2
        num = array_numerator.sum()
        den = array_denominator.sum()
        return num / den

def autocorrelation_length(array):
    length = 1
    for lag in range(1, len(array)):
        previous_length = length
        length += 2 * autocorrelation_np(array, lag)
        if previous_length/length < 0.05:
            return length, lag

    raise ValueError('The integrated autocorrelation length could not converge to a maximum lags')

def ratio_hybrid_analytical(acc, rej_thresh=3):
    rej = 1 - acc
    N_max = 100 * rej_thresh
    ratio = 0
    for l in range(1, N_max):
        for k in range(l*rej_thresh, l*N_max):
            ratio += rej**k / k

    return (1 - rej) * ratio


# def ratio_hybrid_analytical(acc, rej_thresh=3):
#     rej = 1 - acc
#     N_max = 10 * rej_thresh
#     ratio = 0
#     while
#     for l in range(1, N_max):
#         for k in range(l*rej_thresh, N_max):
#             ratio += rej**k / k
#
#     return (1 - rej) * ratio

import IPython; IPython.embed();



rand = np.random.uniform(size=100000)
idx = np.where(rand <= 0.6)[0]
rand[idx] = 0
idxone = np.where(rand > 0)[0]
rand[idxone] = 1
stuckcount = 0
stucklist = []

for traj in rand:
    if traj == 1:
        if stuckcount != 0:
            stucklist.append(stuckcount)
        stuckcount =0
    if traj == 0:
        stuckcount += 1

stucklist = np.asarray(stucklist)
seqcount = np.bincount(stucklist)[1:]

sys.exit()
