import sys
sys.path.append('../')
import numpy as np
import matplotlib.pyplot as plt
import os

import Library.python_utils as pu
import Library.CONST as CONST

from optparse import OptionParser
usage = """%prog [options]
Plots one hmc trajectory for each parameter involved."""
parser=OptionParser(usage)
parser.add_option("--search_parameter_indices", default='012345678', action="store", type="string", help=""" Indices of the parameters to run the hmc on. Other are kept fixed. Default are values already stored in /opts_dict.npy file.""")
parser.add_option("--traj_nb", default=1, action="store", type="int", help=""" Which trajectory to plot. Default is 1. """,metavar="")

(opts,args)=parser.parse_args()

# output_path = sys.argv[1]
output_path = '../__output_data/GW170817/bilbyoriented/SUB-D_012345678/PSD_1/1020_1000_2000_200_200_0.005_dlogL1/'

# trajectory_number = 1
# if len(sys.argv)==3: trajectory_number = int(sys.argv[2])


# Intersect the default search_parameter_indices to plot on w.r.t those asked in input
# parameter_indices_input = [int(i) for i in list(opts.search_parameter_indices)]
# search_parameter_indices = list(set.intersection(set(parameter_indices_input), set(search_parameter_indices)))

dlogL_num_file = output_path + 'dlogL_num_single_traj_1000_phase3.dat'
dlogL_ana_file = output_path + 'dlogL_ana_single_traj_1000_phase3.dat'


dlogL_num = np.loadtxt(dlogL_num_file).T
dlogL_ana = np.loadtxt(dlogL_ana_file).T

search_parameter_indices = np.arange(dlogL_num.shape[0])

steps = np.arange(len(dlogL_num[0]))


import IPython; IPython.embed();


from matplotlib import rc
# rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
# ## for Palatino and other serif fonts use:
# # rc('font',**{'family':'serif','serif':['Palatino']})
# rc('text', usetex=True)
#
# plt.rc('text', usetex=False)
# plt.rc('font', family='serif')
# plt.rc('font', size=10)
# plt.rc('font', weight='bold')

# axes_fontdict = {'fontsize': 20,
#         'fontweight': 'bold',
#         'verticalalignment': 'baseline',
#         'horizontalalignment': 'center'}
# axes_titleweight = 'bold'

plt.rcParams['text.usetex'] = False

plt.rcParams['figure.titlesize'] = 18
plt.rcParams['figure.titleweight'] = 'bold'

plt.rcParams['axes.titlesize'] = 15
plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titleweight'] = 'normal'

plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12
plt.rcParams['font.size'] = 10


# import IPython; IPython.embed()

fig, axs = plt.subplots(nrows=dlogL_num.shape[0], figsize=(15, 3 * dlogL_num.shape[0]))
# fig, axs = plt.subplots(dlogL_num.shape[0], 1, figsize=(16,8.2))
for param_index in range(dlogL_num.shape[0]):
    axs[param_index].plot(steps, dlogL_num[param_index], label='numerical')
    axs[param_index].plot(steps, dlogL_ana[param_index], label='analytical')
    axs[param_index].set_xlabel("steps", color='black')
    axs[param_index].set_ylabel("{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]), color='black')
    axs[param_index].legend()

# fig.suptitle("Trajectory {} on {}, offset = {:.0e}: Acc={:.3f}".format(trajectory_number, CONST.TRAJ_PARAMETERS_KEYS[param_index], parameter_offsets[param_index], acc_proba))
fig.tight_layout()
# plt.show()

file_name = output_path + '/plots/dlogL_num_vs_ana.png'
fig.savefig(file_name)

sys.exit()
