import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess
import pandas as pd

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf
import Library.initialization as init

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as CdlogL
import Codes.FIM as FIM

import gw.detector.networks as gwdn
import core.sampler
import core.utils as cut

if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--inj_file", default='../__output_data/GW170817/0_2_3_4_5_6_7_8_9_10_11_12_13_14/PSD_3/20_20_2000_200_200_0.005/phase_marginalization/IMRPhenomPv2_ROQ/injection.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--config_file", default='../__output_data/GW170817/0_2_3_4_5_6_7_8_9_10_11_12_13_14/PSD_3/20_20_2000_200_200_0.005/phase_marginalization/IMRPhenomPv2_ROQ/config.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")
    parser.add_option("--outdir", default='../Tests/.logL_vs_lnmu/', action="store", type="string", help="""Output directory where results and plots will be stored. If not specified, it will be created automatically in the current working directory under ./outdir""")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""Print out information of interest during the run.""",dest='verbose')
    parser.add_option("-d", "--debug", default=False, action="store_true", help="""Print out heavy information on trajectories to debug them.""")
    parser.add_option("--stdout_file", default=False, action="store_true", help="""All printed outputs will be written in the file `print_out.txt` instead of the terminal""")
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")
    parser.add_option("--fit_method", default='cubic', action="store", type="string", help="""Method used to fit the numerical gradients of the log likelihood accumulated in phase1. """)
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")
    parser.add_option("--on_CIT", default=False, action="store_true", help="""Set this to True if running this script on Caltech's cluster. Will set the right path for the ROQ basis.""")

    # randGen = np.random.RandomState(seed=2)
    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()
    outdir = opts.outdir

    opts_dict_formatted = cut.dictionary_to_formatted_string(opts.__dict__)
    cut.logger.info(f'Options from the command line are: {opts_dict_formatted}')

    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)
    cut.logger.info(f'Options from the configuration file {opts.config_file} are: {config_dict_formatted}')

    if opts.on_CIT and config_dict['analysis']['roq']:
        config_dict['analysis']['roq_b_matrix_directory'] = '/home/cbc/ROQ_data/IMRPhenomPv2/'
        cut.logger.info('Setting the roq_b_matrix_directory to "/home/cbc/ROQ_data/IMRPhenomPv2/"')

    RUN_CONST.debug = opts.debug
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    inj_file_name = opts.inj_file.split('/')[-1]
    inj_name = inj_file_name.split('.')[0]
    inj_name = 'GW170817'


    # Parse the inputs relative to the sampler and the search
    sampler_dict = init.get_sampler_dict(config_dict)
    sampler_dict_formatted = cut.dictionary_to_formatted_string(sampler_dict)
    cut.logger.info(f'Options for the sampler are: {sampler_dict_formatted}')

    # SET THE OUTPUT DIRECTORY WHERE OUTPUT FILES WILL BE SAVED

    cut.setup_logger(cut.logger, outdir=outdir, label='logger', log_level='INFO')
    cut.logger.info(f'Output directory is: {outdir}')





    # # INITIAL PRINTS, CREATE OUTPUT FILES
    # if opts.stdout_file:
    #     stdout_terminal = sys.stdout # Keep in memory what the terminal output is for stuff we still want to output in the terminal to follow script process
    #     stdout_file_path = outdir + 'print_out_{}_{}.txt'.format(config_dict['hmc']['n_traj_hmc_tot'], config_dict['hmc']['n_traj_fit'])
    #     print("\nStandard output is now redirected to file:\n{}".format(stdout_file_path))
    #     sys.stdout = open(stdout_file_path, 'w')
    #
    #     print("Options from the command line are:")
    #     pu.print_dict(opts.__dict__)
    #     print("\nOptions from the configuration file {} are:".format(opts.config_file))
    #     pu.print_dict(config_dict)

    samples_file_path = "{}samples.dat".format(outdir)						# chain file





    # SET THE INJECTION PARAMETERS AND THE ANALYSIS PARAMETERS
    injection_parameters, ext_analysis_dict = set_inj.get_inj_parameters_and_analysis_dict(opts.inj_file, **config_dict['analysis'])
    inj_dict_formatted = cut.dictionary_to_formatted_string(injection_parameters)
    cut.logger.info(f'Injected parameters derived from {opts.inj_file} are: {inj_dict_formatted}')
    ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)
    cut.logger.info(f'Analysis parameters derived from the component masses are: {ext_anal_dict_formatted}')

    start_time = ext_analysis_dict['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
    interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])
    # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
    for ifo in interferometers:
        ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
        ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = ext_analysis_dict['duration']
        ifo.strain_data.sampling_frequency = ext_analysis_dict['sampling_frequency']
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=config_dict['analysis']['psd'], sampling_frequency=ext_analysis_dict['sampling_frequency'], duration=ext_analysis_dict['duration'], start_time=start_time)


    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=ext_analysis_dict['sampling_frequency'],
        duration=ext_analysis_dict['duration'],
        start_time=start_time)

    # WAVEFORM GENERATION
    waveform_arguments = {}
    waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
    waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
    waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
    waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']
    injection_waveform_generator = bilby.gw.WaveformGenerator(
        duration=interferometers.duration,
        sampling_frequency=interferometers.sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
        waveform_arguments=waveform_arguments
        )
    # template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, interferometers, waveform_arguments)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    # for i in range(len(interferometers)):
        # interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        # interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain
    interferometers.inject_signal(parameters=injection_parameters, waveform_generator=injection_waveform_generator)
    for ifo in interferometers:
        ifo.fd_strain = ifo.strain_data.frequency_domain_strain

    # # COMPUTE AND PRINT SNR
    interferometers.optimal_snr()
    interferometers.matched_filter_snr()
    interferometers.log_likelihood()
    # waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_search_waveform']
    # template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, interferometers, waveform_arguments)
    # logL, Snr, opt_snr_Best = logL_snr.main(template_ifos, interferometers, True)
    #
    # # COMPUTE dlogL and print it to check with the plots
    # dlogL = CdlogL.main(injection_parameters, start_time, interferometers, waveform_arguments, True, **sampler_dict)


    # SET THE LIKELIHOOD OBJECT, two different cases whether ROQ basis is used or not
    if config_dict['analysis']['roq']:
        if ext_analysis_dict['approximant'] != 'IMRPhenomPv2':
            raise ValueError("Need to set approximant to IMRPhenomPv2 to work with the ROQ basis, approximant here is {}".format(ext_analysis_dict['approximant']))

        scale_factor = ext_analysis_dict['roq']['scale_factor']
        roq_directory = ext_analysis_dict['roq']['directory']
        # Load in the pieces for the linear part of the ROQ. Note you will need to
        freq_nodes_linear = np.load(roq_directory + "fnodes_linear.npy") * scale_factor
        # Load in the pieces for the quadratic part of the ROQ
        freq_nodes_quadratic = np.load(roq_directory + "fnodes_quadratic.npy") * scale_factor

        # Load the parameters describing the valid parameters for the basis.
        params = ext_analysis_dict['roq']['rescaled_params'].copy()

        outdir_psd = init.get_outdir_psd(config_dict['analysis']['psd'])
        roq_outdir = '../__ROQ_weights/' + inj_name + '/' + outdir_psd
        pu.mkdirs(roq_outdir)
        weights_file_path_no_format = roq_outdir + '{:.0f}Hz_{:.0f}Hz_{:.0f}Hz_{:.0f}s_weights'.format(params['flow'], ext_analysis_dict['maximum_frequency_ifo'], ext_analysis_dict['maximum_frequency_injected_waveform'], params['seglen'])
        weight_format = 'npz'
        weights_file_path = weights_file_path_no_format + f'.{weight_format}'

        # make waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=bilby.gw.source.binary_neutron_star_roq,
            waveform_arguments=dict(
                frequency_nodes_linear=freq_nodes_linear,
                frequency_nodes_quadratic=freq_nodes_quadratic,
                reference_frequency=ext_analysis_dict['reference_frequency'], waveform_approximant=ext_analysis_dict['approximant']),
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters)

        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        priors = bilby.gw.prior.BNSPriorDict()
        # for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'phi_12', 'phi_jl', 'luminosity_distance']:
        for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'luminosity_distance']:
            priors[key] = injection_parameters[key]
        for key in ['mass_1', 'mass_2']:
            priors[key].minimum = max(priors[key].minimum, params['compmin'])
        priors['chirp_mass'] = bilby.core.prior.Constraint(name='chirp_mass', minimum=float(params['chirpmassmin']), maximum=float(params['chirpmassmax']))
        priors['mass_ratio'] = bilby.core.prior.Constraint(0.125, 1, name='mass_ratio')
        priors['geocent_time'] = bilby.core.prior.Uniform(injection_parameters['geocent_time'] - 0.1, injection_parameters['geocent_time'] + 0.1, latex_label='$t_c$', unit='s')


        if os.access(weights_file_path, os.W_OK):
            # load the weights from the file
            likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                weights=weights_file_path, priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])
        else:
            # Load in the pieces for the linear part of the ROQ. Note you will need to
            basis_matrix_linear = np.load(roq_directory + "B_linear.npy").T
            # Load in the pieces for the quadratic part of the ROQ
            basis_matrix_quadratic = np.load(roq_directory + "B_quadratic.npy").T

            # Redoing here a bit what's already being done in set_inj because `bilby.gw.likelihood.ROQGravitationalWaveTransient()` does not accept dictionnary for roq_params...
            roq_params_ndarray = np.genfromtxt(ext_analysis_dict['roq']['params_path'], names=True)
            roq_params_ndarray = roqu.rescale_params(roq_params_ndarray, scale_factor)
            # roq_params_ndarray = None

            likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic,
                priors=priors, roq_params=roq_params_ndarray)

            # remove the basis matrices as these are big for longer bases
            del basis_matrix_linear, basis_matrix_quadratic

            # write the weights to file so they can be loaded multiple times
            likelihood.save_weights(weights_file_path_no_format, format=weight_format)
    else:
        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        # priors = None
        priors = bilby.gw.prior.BNSPriorDict()

        # make waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
            waveform_arguments=waveform_arguments,
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters)

        likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
            interferometers=interferometers,
            waveform_generator=search_waveform_generator,
            priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])

    # # Initialize all the required parameters to zero s.t. the dictionary has a value for all keys even if the injection_parameters doesn't specify some of them
    for key in likelihood.waveform_generator.source_parameter_keys:
        likelihood.parameters[key] = 0

    # SET THE LIKELIHOOD GRADIENT OBJECT AND COMPUTE THE GRADIENT AT POINT OF INJECTION
    likelihood_gradient = lg.GWTransientLikelihoodGradient(
        likelihood=likelihood,
        search_parameter_keys=sampler_dict['search_parameter_keys'],
        dict_trajectory_functions_str=sampler_dict['trajectory_functions'],
        dict_trajectory_parameters_keys=sampler_dict['trajectory_parameters_keys'],
        dict_parameters_offsets=sampler_dict['parameters_offsets']
        )

    # SET THE HMC SAMPLER
    sampler = core.sampler.GWHMCSampler(
        injection_parameters=injection_parameters,
        outdir=outdir,
        likelihood_gradient=likelihood_gradient,
        parameters_boundaries=sampler_dict['parameters_boundaries'],
        search_parameter_keys_local_fit=sampler_dict['search_parameter_keys_local_fit'],
        n_traj_hmc_tot = config_dict['hmc']['n_traj_hmc_tot'],
        n_traj_fit = config_dict['hmc']['n_traj_fit'],
        n_traj_for_this_run = config_dict['hmc']['n_traj_for_this_run'],
        n_fit_1 = config_dict['hmc']['n_fit_1'],
        n_fit_2 = config_dict['hmc']['n_fit_2'],
        length_num_traj = config_dict['hmc']['length_num_traj'],
        epsilon0 = config_dict['hmc']['epsilon0'],
        n_sis = config_dict['hmc']['n_sis']
        )

    # SET THE STATE OF THE SAMPLER TO WHAT IT WAS SAVED OR INITIALIZE IT IF THIS IS THE FIRST TIME THESE SETTINGS ARE RUN
    state_dir = 'toto' # A none existing state_dir will initialize the sampler
    count, n_traj_already_run, duration1, Acc, q_pos, dlogL, logL, qpos_global_fit, dlogL_global_fit, qpos_olut_fit, dlogL_olut_fit, oluts_dict, randGen, fit_coeff_global, count_phase3, duration2, duration3, atr, htr, ntr, stuck_count, hybrid_stuck  = sampler.get_state(state_dir=state_dir)

    q_pos_traj_where_dlogL_bumped = np.loadtxt(outdir + 'q_pos_traj.dat')
    sampler.update_likelihood_parameters_from_q_pos_traj(q_pos_traj_where_dlogL_bumped)

    lnmu_idx = sampler.dict_search_parameter_indices['reduced_mass']
    lnmu_central_value = q_pos_traj_where_dlogL_bumped[lnmu_idx]
    offset = 1e-8
    half_nb_pts_to_plot = 20
    lnmu_list = np.arange(lnmu_central_value - half_nb_pts_to_plot*offset, lnmu_central_value + half_nb_pts_to_plot*offset, offset)

    q_pos_traj = q_pos_traj_where_dlogL_bumped.copy()
    logL_list = []
    dlogL_list = []
    for lnmu in lnmu_list:
        q_pos_traj[lnmu_idx] = lnmu
        sampler.update_likelihood_parameters_from_q_pos_traj(q_pos_traj)
        dlogL_pos = sampler.likelihood_gradient.calculate_dlogL_np()
        logL = sampler.likelihood_gradient.log_likelihood_at_point
        logL_list.append(logL)
        dlogL_list.append(dlogL_pos[lnmu_idx])

    import matplotlib.pyplot as plt


    plt.plot(lnmu_list, logL_list, label='logL')
    plt.xlabel(r'$ln(\mu)$')
    plt.ylabel(r'$logL$')
    plt.title('logL behavior around bumpy point in trajectory')
    plt.legend()
    # plt.savefig(outdir + f'{inj_name}_logL_vs_iota_psi_contour.png')
    plt.show()

    plt.plot(lnmu_list, dlogL_list, label='dlogL')
    plt.xlabel(r'$ln(\mu)$')
    plt.ylabel(r'$dlogL$')
    plt.title('dlogL behavior around bumpy point in trajectory')
    plt.legend()
    # plt.savefig(outdir + f'{inj_name}_logL_vs_iota_psi_contour.png')
    plt.show()

    plt.close()

    if False: import IPython; IPython.embed(); sys.exit()
