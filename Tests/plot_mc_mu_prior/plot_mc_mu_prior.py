import numpy as np

import bilby
import bilby.gw.conversion as conv
import sys
sys.path.append('../../')
import gw.prior as prior
import core.prior_gradient as pg
import Library.param_utils as paru

def mc_mu_to_eta(mc, mu):
    return (mu/mc)**(5/2)

def dlnJ_dlneta(eta):
    return -3/5 + 0.5 * eta / (0.25 - eta)

def dlnJ_dlnMc(mc, mu):
    eta = mc_mu_to_eta(mc, mu)
    return -5/2 * dlnJ_dlneta(eta)

def dlnJ_dlnmu(mc, mu):
    eta = mc_mu_to_eta(mc, mu)
    return 5/2 * dlnJ_dlneta(eta)

priors = bilby.gw.prior.BNSPriorDict(filename='../../gw/prior_files/GW170817_LALInf_IMRPhenomD.prior')
# geocent_time_start = 1187008882.43
# start_time = 1187008826.227964

# SET THE PRIORS AND THEIR GRADIENTS
# priors = prior.fill_in_input_priors(priors, geocent_time_start, start_time)
prior.set_chirp_mass_min_max(priors)
prior.set_reduced_mass_min_max(priors)

traj_priors = prior.GWTrajPriors(
    priors=priors,
    search_parameter_keys=['chirp_mass', 'reduced_mass'],
    dict_trajectory_functions_str=dict(chirp_mass='log', reduced_mass='log')
    )
traj_prior_gradients = pg.GWTrajPriorGradients(
    traj_priors=traj_priors,
    dict_parameters_offsets={}
    )



pi_mc_mu = traj_priors['log_chirp_mass_and_log_reduced_mass'].mc_mu_combined_prior
tpg_mc = traj_prior_gradients['log_chirp_mass']
tpg_mu = traj_prior_gradients['log_reduced_mass']
m_min = priors['mass_1'].minimum + 0.01
m_max = priors['mass_1'].maximum - 0.01
npts = 100
deltam = (m_max - m_min) / npts
mc_mu_list = []
pi_mc_mu_prob_list = []
pi_mc_mu_ln_prob_list = []
tpg_mc_ln_prob_list = []
tpg_mu_ln_prob_list = []
dlnJ_dlnMC_ana_list = []
dlnJ_dlnmu_ana_list = []
for m2 in np.linspace(m_min, m_max - deltam, npts):
    for m1 in np.linspace(m2 + deltam, m_max, npts):
        mc = conv.component_masses_to_chirp_mass(m1, m2)
        mu = paru.component_masses_to_reduced_mass(m1, m2)
        pi_mc_mu_prob = pi_mc_mu._prob(mc, mu)
        pi_mc_mu_ln_prob = pi_mc_mu._ln_prob(mc, mu)
        tpg_mc_ln_prob = tpg_mc.log_traj_prior_gradient_num(mc, mu)
        tpg_mu_ln_prob = tpg_mu.log_traj_prior_gradient_num(mu, mc)

        dlnJ_dlnMc_ana = dlnJ_dlnMc(mc, mu)
        dlnJ_dlnmu_ana = dlnJ_dlnmu(mc, mu)

        mc_mu_list.append([mc, mu])
        pi_mc_mu_prob_list.append(pi_mc_mu_prob)
        pi_mc_mu_ln_prob_list.append(pi_mc_mu_ln_prob)
        tpg_mc_ln_prob_list.append(tpg_mc_ln_prob)
        tpg_mu_ln_prob_list.append(tpg_mu_ln_prob)

        dlnJ_dlnMC_ana_list.append(dlnJ_dlnMc_ana)
        dlnJ_dlnmu_ana_list.append(dlnJ_dlnmu_ana)


mc_mu_list = np.asarray(mc_mu_list)
pi_mc_mu_prob_list = np.asarray(pi_mc_mu_prob_list)
pi_mc_mu_ln_prob_list = np.asarray(pi_mc_mu_ln_prob_list)
tpg_mc_ln_prob_list = np.asarray(tpg_mc_ln_prob_list)
tpg_mu_ln_prob_list = np.asarray(tpg_mu_ln_prob_list)

dlnJ_dlnMC_ana_list = np.asarray(dlnJ_dlnMC_ana_list)
dlnJ_dlnmu_ana_list = np.asarray(dlnJ_dlnmu_ana_list)







import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
myfontsize = 12
fig = plt.figure(figsize=(10,10))

import IPython; IPython.embed()
# idx = pi_mc_mu_prob_list.argsort()
# x, y, z = mc_mu_list[:,0][idx], mc_mu_list[:,1][idx], pi_mc_mu_prob_list[idx]
# ax.scatter(x, y, c=z, s=50, edgecolor='')

ax = Axes3D(fig)
ax.scatter(mc_mu_list[:,0], mc_mu_list[:,1], pi_mc_mu_prob_list, c='r', marker='o')
# ax.set_xlim(left=xmin, right=xmax)
# ax.set_ylim(bottom=ymin, top=ymax)
ax.set_xlabel(r"$\mathcal{M}$", fontsize=myfontsize)
ax.set_ylabel(r"$\mu$", fontsize=myfontsize)
ax.set_zlabel(r"$\pi^{(\mathcal{M}, \mu)}$", fontsize=myfontsize)
ax.set_title(r'Joint prior on $\pi^{(\mathcal{M}, \mu)}$ when uniform on component masses', fontsize=myfontsize)
ax.xaxis.labelpad = 20
ax.yaxis.labelpad = 30
ax.zaxis.labelpad = 30
# plt.savefig('pi_mc_mu_prob.png')


fig = plt.figure(figsize=(10,10))
ax = Axes3D(fig)
ax.scatter(mc_mu_list[:,0], mc_mu_list[:,1], pi_mc_mu_ln_prob_list, c='r', marker='o')
# ax.set_xlim(left=xmin, right=xmax)
# ax.set_ylim(bottom=ymin, top=ymax)
ax.set_xlabel(r"$\mathcal{M}$", fontsize=myfontsize)
ax.set_ylabel(r"$\mu$", fontsize=myfontsize)
ax.set_zlabel(r"$ln\pi^{(\mathcal{M}, \mu)}$", fontsize=myfontsize)
ax.set_title(r'Log of joint prior $ln\pi^{(\mathcal{M}, \mu)}$ when uniform on component masses', fontsize=myfontsize)
ax.xaxis.labelpad = 20
ax.yaxis.labelpad = 30
ax.zaxis.labelpad = 30
# plt.savefig('pi_mc_mu_ln_prob.png')


fig = plt.figure(figsize=(10,10))
ax = Axes3D(fig)
ax.scatter(mc_mu_list[:,0], mc_mu_list[:,1], tpg_mc_ln_prob_list, c='r', marker='o')
# ax.set_xlim(left=xmin, right=xmax)
# ax.set_ylim(bottom=ymin, top=ymax)
ax.set_xlabel(r"$\mathcal{M}$", fontsize=myfontsize)
ax.set_ylabel(r"$\mu$", fontsize=myfontsize)
ax.set_zlabel(r"$\partial_{ln\mathcal{M}} ln\pi^{(ln\mathcal{M}, ln\mu)}$", fontsize=myfontsize)
ax.set_title(r'$\frac{\partial ln\pi^{(ln\mathcal{M}, ln\mu)}}{ln\mathcal{M}}$ when uniform on component masses', fontsize=myfontsize)
ax.xaxis.labelpad = 20
ax.yaxis.labelpad = 30
ax.zaxis.labelpad = 30
# plt.savefig('tpg_mc_ln_prob.png')


fig = plt.figure(figsize=(10,10))
ax = Axes3D(fig)
ax.scatter(mc_mu_list[:,0], mc_mu_list[:,1], tpg_mu_ln_prob_list, c='r', marker='o')
# ax.set_xlim(left=xmin, right=xmax)
# ax.set_ylim(bottom=ymin, top=ymax)
ax.set_xlabel(r"$\mathcal{M}$", fontsize=myfontsize)
ax.set_ylabel(r"$\mu$", fontsize=myfontsize)
ax.set_zlabel(r"$\partial_{ln\mu} ln\pi^{(ln\mathcal{M}, ln\mu)}$", fontsize=myfontsize)
ax.set_title(r'$\frac{\partial ln\pi^{(ln\mathcal{M}, ln\mu)}}{ln\mu}$ when uniform on component masses', fontsize=myfontsize)
ax.xaxis.labelpad = 20
ax.yaxis.labelpad = 30
ax.zaxis.labelpad = 30
# plt.savefig('tpg_mu_ln_prob.png')

plt.show()
