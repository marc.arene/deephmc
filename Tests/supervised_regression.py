# get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
# import seaborn as sns; sns.set()
import numpy as np
import pandas as pd

from sklearn import neighbors, svm, ensemble, linear_model, metrics, model_selection, preprocessing

import IPython as ip
import sys
sys.path.append('../')
import Library.CONST as CONST

def plot_regression(data_predicted, data_true, ax=None, label='', data_label='data'):
    """Summary plot of regression performance.
    """
    ax = ax or plt.gca()
    ax.scatter(data_true, data_predicted, lw=0, alpha=0.5, s=10)
    ax.set_xlabel('True ' + data_label)
    ax.set_ylabel('Predicted ' + data_label)
    x_min = data_true.min()
    x_max = data_true.max()
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(x_min, x_max)
    ax.plot([x_min, x_max], [x_min, x_max], 'r--')
    R2 = metrics.r2_score(data_true, data_predicted)
    ax.text(0.05, 0.9, f'{label}$R^2 = {R2:.2f}$', transform=ax.transAxes, fontsize=16, color='r')

def test_sk_regression(method, x_train_scaled, x_test_scaled, y_train_scaled, y_train, y_test, y_scaler, ax, y_label):
    # Fit normalized training dlogL.
    fit = method.fit(x_train_scaled, y_train_scaled)
    # Get predicted dlogL for training qpos.
    y_train_predicted = y_scaler.inverse_transform(fit.predict(x_train_scaled))
    # Get predicted shear for test images.
    y_test_predicted = y_scaler.inverse_transform(fit.predict(x_test_scaled))
    # y_train = y_scaler.inverse_transform(y_train_scaled)
    # Plot results.
    # y_label=f'dlogL[{idx_dlogL_to_predict}]'
    # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
    plot_regression(y_train_predicted, y_train, label='train: ', data_label=y_label, ax=ax[0])
    plot_regression(y_test_predicted, y_test, label='test: ', data_label=y_label, ax=ax[1])
    plt.tight_layout()


if __name__=='__main__':
    search_parameter_indices = [0,1,3,4,5,6,7,8]
    outdir_subD = 'SUB-D_'
    for i in range(sampler_dict['n_dim']):
        if i in search_parameter_indices:
            outdir_subD+='{}'.format(i)
        else:
            outdir_subD += '#'
    output_data_dir = '../__output_data/GW170817'
    directory = output_data_dir + f'/{outdir_subD}/PSD_1/501_500_2000_200_200_0.005_dlogL1/IMRPhenomPv2_ROQ/phase1'

    # method = neighbors.KNeighborsRegressor()
    # suffix = 'KN'
    method = ensemble.RandomForestRegressor(random_state=123)
    suffix = 'RF'
    # method = ensemble.GradientBoostingRegressor(random_state=123)
    # suffix = 'GB'



    q_pos = np.loadtxt(directory + '/pt_fit_phase1.dat')
    dlogL = np.loadtxt(directory + '/dlogL_fit_phase1.dat')
    df_qpos = pd.DataFrame(q_pos[:, search_parameter_indices])

    # dlogL_to_predict = search_parameter_indices.copy()
    dlogL_to_predict = [0,1,3,4]
    dlogL_names = ['dlogL/d' + fpname for fpname in [CONST.TRAJ_PARAMETERS_KEYS[j] for j in dlogL_to_predict]]
    nb_dlogL_to_predict = len(dlogL_to_predict)
    fig, ax = plt.subplots(nb_dlogL_to_predict, 2, sharex=False, sharey=False, figsize=(10, 4 * nb_dlogL_to_predict))
    if nb_dlogL_to_predict==1:
        ax = ax.reshape(1, -1)


    for ii, idx_dlogL_to_predict in enumerate(dlogL_to_predict):
    # idx_dlogL_to_predict = 4
        df_dlogL = pd.DataFrame(dlogL[:, idx_dlogL_to_predict])

        df_qpos_train, df_qpos_test, df_dlogL_train, df_dlogL_test = model_selection.train_test_split(df_qpos, df_dlogL, test_size=0.9, random_state=123)

        # nb_of_traj_to_test = 125
        # df_qpos_train = pd.DataFrame(df_qpos.values[:-200*nb_of_traj_to_test, :])
        # df_dlogL_train = pd.DataFrame(df_dlogL.values[:-200*nb_of_traj_to_test, :])
        # df_qpos_test = pd.DataFrame(df_qpos.values[-200*nb_of_traj_to_test:, :])
        # df_dlogL_test = pd.DataFrame(df_dlogL.values[-200*nb_of_traj_to_test:, :])

        qpos_scaler = preprocessing.StandardScaler().fit(df_qpos_train)
        qpos_train_scaled = qpos_scaler.transform(df_qpos_train)
        qpos_test_scaled = qpos_scaler.transform(df_qpos_test)

        if False:
            param_to_plot = 4
            plt.hist(df_qpos_train.values[:, param_to_plot], bins=35, label='Train')
            plt.hist(df_qpos_test.values[:, param_to_plot], bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Distribution of qpos[{param_to_plot}]')
            plt.show()

            plt.hist(qpos_train_scaled[:, param_to_plot], bins=35, label='Train')
            plt.hist(qpos_test_scaled[:, param_to_plot], bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Distribution of scaled qpos[{param_to_plot}]')
            plt.show()

        dlogL_scaler = preprocessing.StandardScaler().fit(df_dlogL_train.values.reshape(-1, 1))
        dlogL_train_scaled = dlogL_scaler.transform(df_dlogL_train.values.reshape(-1, 1)).reshape(-1)
        dlogL_test_scaled = dlogL_scaler.transform(df_dlogL_test.values.reshape(-1, 1)).reshape(-1)
        dlogL_scaled = dlogL_scaler.transform(df_dlogL.values.reshape(-1, 1)).reshape(-1)

        if False:
            plt.hist(df_dlogL_train.values, bins=35, label='Train')
            plt.hist(df_dlogL_test.values, bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'True dlogL[{idx_dlogL_to_predict}] magnitude')
            plt.show()

            plt.hist(dlogL_train_scaled, bins=35, label='Train')
            plt.hist(dlogL_test_scaled, bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Scaled true dlogL[{idx_dlogL_to_predict}] magnitude')
            plt.show()

        # method = neighbors.KNeighborsRegressor()
        # train_data=qpos_train_scaled
        # test_data=qpos_test_scaled
        # fit = method.fit(train_data, dlogL_train_scaled)
        # dlogL_train_predicted = dlogL_scaler.inverse_transform(fit.predict(train_data))
        # dlogL_test_predicted = dlogL_scaler.inverse_transform(fit.predict(test_data))
        #
        # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
        # data_label=f'dlogL[{idx_dlogL_to_predict}]'
        # plot_regression(dlogL_train_predicted, df_dlogL_train.values, label='train: ', data_label=data_label,  ax=ax[0])
        # plot_regression(dlogL_test_predicted, df_dlogL_test.values, label='test: ', data_label=data_label, ax=ax[1])
        # plt.tight_layout()

        test_sk_regression(method=method, x_train_scaled=qpos_train_scaled, x_test_scaled=qpos_test_scaled, y_train_scaled=dlogL_train_scaled, y_train=df_dlogL_train.values, y_test=df_dlogL_test.values, y_scaler=dlogL_scaler, ax=ax[ii,:], y_label=dlogL_names[ii])

    if False:
        filename = '/plots/supervised_reg_dlogL' + ''.join([str(idx) for idx in dlogL_to_predict]) + f'_{suffix}.png'
        fig.savefig(directory + filename)
    else:
        plt.show()


    # ip_shell = ip.get_ipython()
    # ip_shell.run_line_magic('time', 'test_sk_regression(method=method, x_train_scaled=qpos_train_scaled, x_test_scaled=qpos_test_scaled, y_train_scaled=dlogL_train_scaled, y_train=df_dlogL_train.values, y_test=df_dlogL_test.values, y_scaler=dlogL_scaler, ax=ax[ii,:], y_label=dlogL_names[ii])')
