#!/usr/bin/env python
"""
Example of how to use the Reduced Order Quadrature method (see Smith et al.,
(2016) Phys. Rev. D 94, 044031) for a Binary Black hole simulated signal in
Gaussian noise.

This requires files specifying the appropriate basis weights.
These aren't shipped with Bilby, but are available on LDG clusters and
from the public repository https://git.ligo.org/lscsoft/ROQ_data.
"""
from __future__ import division, print_function

import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import os

import numpy as np

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.bilby_waveform as bilby_wv
# import Library.param_utils as paru
import Library.CONST as CONST
import Library.likelihood_gradient as lg
import Library.python_utils as pu
import Library.roq_utils as roqu

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds

if __name__=='__main__':
    from optparse import OptionParser
    usage = """%prog [options]
    Plots the three waveforms in the time domain"""
    parser=OptionParser(usage)
    parser.add_option("--psd",default=1,action="store",type="int",help="""1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.""",metavar="INT from {1,2,3}")
    parser.add_option("--dlogL", default='dlogL1', action="store", type="string", help=""" Method to use to compute the gradient of the log-likelihood. Use `dlogL1` to use  dlogL = <s|dh> - <h|dh>, `dlogL2` for dlogL = logL(p+offset) - logL(p-offset).""")
    parser.add_option("--ifos", default='H1,L1,V1', action="store", type="string", help=""" Interferometers to run the analysis on. Default is 'H1,L1,V1' """)
    parser.add_option("--parameter_offsets", default='707077777', action="store", type="string", help=""" -log10() of the offset used for central differencing on each parameter. Default is '707077777' meaning sampler_dict['parameter_offsets'] = [1e-07, 0, 1e-07, 0, 1e-07, 1e-07, 1e-07, 1e-07, 1e-07]. 0 Means analytical formula is used if available.""")
    parser.add_option("--search_parameter_indices", default='012345678', action="store", type="string", help=""" Indices of the parameters to run the hmc on. Other are kept fixed. Default are values already stored in config.py file.""")
    parser.add_option("--inj_file", default='../examples/massive_GW170817.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")


    (opts,args)=parser.parse_args()

    if len(opts.parameter_offsets)!=9:
        raise ValueError('You must define 9 offsets for argument `parameter_offsets` which is here of length {}'.format(len(opts.parameter_offsets)))
        sys.exit(1)

    exponents = [int(e) for e in list(opts.parameter_offsets)]
    # sampler_dict['parameter_offsets'] = [10**(-e) for e in exponents]
    sampler_dict['parameter_offsets'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    idx_nonzero = np.nonzero(exponents)[0]
    for i in idx_nonzero:
        sampler_dict['parameter_offsets'][i] = 10**(-exponents[i])
    sampler_dict['dlogL'] = opts.dlogL

    sampler_dict['search_parameter_indices'] = [int(i) for i in list(opts.search_parameter_indices)]

    inj_file_name = opts.inj_file.split('/')[-1]
    inj_name = inj_file_name.split('.')[0]

    outdir = '../__ROQ_weights/' + inj_name + '/'

    # Set up a random seed for result reproducibility.  This is optional!
    np.random.seed(88170235)

    # SET THE INJECTION PARAMETERS
    injection_parameters = set_inj.ini_file_to_dict(opts.inj_file)
    pu.print_dict(injection_parameters)
    minimum_frequency = injection_parameters['meta']['minimum_frequency']
    maximum_frequency_ifo = injection_parameters['meta']['maximum_frequency_ifo']
    duration = injection_parameters['meta']['duration']
    sampling_frequency = injection_parameters['meta']['sampling_frequency']
    f_high = injection_parameters['meta']['f_high']
    start_time = injection_parameters['meta']['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    ifos_possible = ['H1', 'L1', 'V1']
    ifos_chosen = opts.ifos.split(',')
    if set.intersection(set(ifos_possible), set(ifos_chosen)) != set(ifos_chosen):
        raise ValueError("the '--ifos' option was wrongly set, you must choose between {}. Example: '--ifos=H1,V1'. ".format(ifos_possible))

    interferometers = bilby.gw.detector.InterferometerList(ifos_chosen)
    for ifo in interferometers:
        # ifo.__class__ = bilby_detector.Interferometer
        ifo.minimum_frequency = minimum_frequency
        ifo.maximum_frequency = maximum_frequency_ifo
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time

    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=opts.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)

    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=start_time)

    # WAVEFORM GENERATION
    injection_parameters['meta']['maximum_frequency_generated_waveform'] = injection_parameters['meta']['maximum_frequency_injected_waveform']
    template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    for i in range(len(interferometers)):
        interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain








    if injection_parameters['meta']['approximant'] != 'IMRPhenomPv2':
        raise ValueError("Need to set approximant to IMRPhenomPv2 to work with the ROQ basis, approximant here is {}".format(injection_parameters['meta']['approximant']))

    scale_factor = injection_parameters['roq']['scale_factor']
    roq_directory = injection_parameters['roq']['directory']
    # Load in the pieces for the linear part of the ROQ. Note you will need to
    freq_nodes_linear = np.load(roq_directory + "fnodes_linear.npy") * scale_factor
    # Load in the pieces for the quadratic part of the ROQ
    freq_nodes_quadratic = np.load(roq_directory + "fnodes_quadratic.npy") * scale_factor

    # Load the parameters describing the valid parameters for the basis.
    params = injection_parameters['roq']['rescaled_params'].copy()

    weights_file_path = outdir + '{:.0f}Hz_{:.0f}Hz_{:.0f}Hz_{:.0f}s_weights.json'.format(params['flow'], injection_parameters['meta']['maximum_frequency_ifo'], injection_parameters['meta']['maximum_frequency_injected_waveform'], params['seglen'])

    # make ROQ waveform generator
    search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
        duration=duration, sampling_frequency=sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.binary_neutron_star_roq,
        waveform_arguments=dict(
            frequency_nodes_linear=freq_nodes_linear,
            frequency_nodes_quadratic=freq_nodes_quadratic,
            reference_frequency=injection_parameters['meta']['reference_frequency'], waveform_approximant=injection_parameters['meta']['approximant']),
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters)

    # Here we add constraints on chirp mass and mass ratio to the prior, these are
    # determined by the domain of validity of the ROQ basis.
    priors = bilby.gw.prior.BNSPriorDict()
    # for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'phi_12', 'phi_jl', 'luminosity_distance']:
    for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'luminosity_distance']:
        priors[key] = injection_parameters[key]
    for key in ['mass_1', 'mass_2']:
        priors[key].minimum = max(priors[key].minimum, params['compmin'])
    priors['chirp_mass'] = bilby.core.prior.Constraint(name='chirp_mass', minimum=float(params['chirpmassmin']), maximum=float(params['chirpmassmax']))
    priors['mass_ratio'] = bilby.core.prior.Constraint(0.125, 1, name='mass_ratio')
    priors['geocent_time'] = bilby.core.prior.Uniform(injection_parameters['geocent_time'] - 0.1, injection_parameters['geocent_time'] + 0.1, latex_label='$t_c$', unit='s')

    pu.mkdirs(outdir)


    if os.access(weights_file_path, os.W_OK):
        # load the weights from the file
        likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
            interferometers=interferometers, waveform_generator=search_waveform_generator,
            weights=weights_file_path, priors=priors)
    else:
        # Load in the pieces for the linear part of the ROQ. Note you will need to
        basis_matrix_linear = np.load(roq_directory + "B_linear.npy").T
        # Load in the pieces for the quadratic part of the ROQ
        basis_matrix_quadratic = np.load(roq_directory + "B_quadratic.npy").T

        # Redoing here a bit what's already being done in set_inj because `bilby.gw.likelihood.ROQGravitationalWaveTransient()` does not accept dictionnary for roq_params...
        roq_params_ndarray = np.genfromtxt(injection_parameters['roq']['params_path'], names=True)
        roq_params_ndarray = roqu.rescale_params(roq_params_ndarray, scale_factor)
        # roq_params_ndarray = None

        likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
            interferometers=interferometers, waveform_generator=search_waveform_generator,
            linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic,
            priors=priors, roq_params=roq_params_ndarray)

        # remove the basis matrices as these are big for longer bases
        del basis_matrix_linear, basis_matrix_quadratic

        # write the weights to file so they can be loaded multiple times
        likelihood.save_weights(weights_file_path)


    search_parameter_keys = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
    likelihood.parameters = injection_parameters
    print("")
    print("likelihood.log_likelihood_ratio() = {}".format(likelihood.log_likelihood_ratio()))

    likelihood_gradient = lg.GWTransientLikelihoodGradient(likelihood, search_parameter_keys)
    dlogL_dict = likelihood_gradient.calculate_log_likelihood_gradient()
    pu.print_dict({"dlogL_dict": dlogL_dict})

    dlogL_np = likelihood_gradient.calculate_dlogL_np()
    print("dlogL_np = {}".format(dlogL_np))

    import IPython; IPython.embed()
