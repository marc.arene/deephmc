import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np
import matplotlib.pyplot as plt
from optparse import OptionParser
from configparser import ConfigParser

import core.sampler

if __name__ == '__main__':
    parser=OptionParser(usage)

    parser.add_option("--dir", default=None, action="store", type="string", help="""  Directory where the state of the sampler was saved""")

    (opts, args) = parser.parse_args()
