import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=False)

import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as Cdlo

if __name__=='__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--inj_file", default='../examples/injection_files/GW170817.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""Print out information of interest during the run.""",dest='verbose')
    parser.add_option("-d", "--debug", default=False, action="store_true", help="""Print out heavy information on trajectories to debug them.""")
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")



    # PARSE INPUT ARGUMENTS
    (opts,args)=parser.parse_args()

    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)

    inj_file_name = opts.inj_file.split('/')[-1]
    inj_name = inj_file_name.split('.')[0]
    RUN_CONST.debug = opts.debug
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    n_traj_hmc_tot = config_dict['hmc']['n_traj_hmc_tot']
    n_traj_fit = config_dict['hmc']['n_traj_fit']
    n_traj_for_this_run = config_dict['hmc']['n_traj_for_this_run']
    n_fit_1 = config_dict['hmc']['n_fit_1']
    n_fit_2 = config_dict['hmc']['n_fit_2']
    length_num_traj = config_dict['hmc']['length_num_traj']
    epsilon0 = config_dict['hmc']['epsilon0']

    ifos_possible = ['H1', 'L1', 'V1']
    ifo_chosen = config_dict['analysis']['ifos'].split(',')
    if set.intersection(set(ifos_possible), set(ifo_chosen)) != set(ifo_chosen):
        raise ValueError("the '--ifos' option was wrongly set, you must choose between {}. Example: '--ifos=H1,V1'. ".format(ifos_possible))
    CONST.approximant = config_dict['analysis']['approx']
    CONST.minimum_frequency = config_dict['analysis']['minimum_frequency']
    CONST.maximum_frequency = config_dict['analysis']['maximum_frequency']
    CONST.reference_frequency = config_dict['analysis']['reference_frequency']
    CONST.roq = config_dict['analysis']['roq']
    if CONST.roq:
        CONST.roq_b_matrix_directory = config_dict['analysis']['roq_b_matrix_directory']
    CONST.psd = config_dict['analysis']['psd']
    sampler_dict['dlogL'] = config_dict['analysis']['dlogl']
    sampler_dict['search_parameter_indices'] = [int(i) for i in list(config_dict['analysis']['search_parameter_indices'].split(','))]
    exponents = [int(e) for e in list(config_dict['analysis']['parameter_offsets'].split(','))]
    sampler_dict['parameter_offsets'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    idx_nonzero = np.nonzero(exponents)[0]
    for i in idx_nonzero:
        sampler_dict['parameter_offsets'][i] = 10**(-exponents[i])

    search_parameter_keys = [CONST.PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
    search_traj_parameter_keys = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
    sampler_dict['search_parameter_indices_local_fit'] = np.intersect1d(sampler_dict['search_parameter_indices'], sampler_dict['search_parameter_indices_local_fit']).tolist()
    sampler_dict['search_traj_parameter_keys_local_fit'] = np.intersect1d(search_traj_parameter_keys, CONST.PARAMETERS_KEYS_LOCAL_FIT).tolist()

    # need this line in case for instance: `search_parameter_indices_local_fit_full = 2` where it's gonna be converted into an int and hence won't be able to be split()
    parameter_indices_local_fit_full_str = str(config_dict['analysis']['search_parameter_indices_local_fit_full'])
    search_parameter_indices_local_fit_full = [int(i) for i in list(parameter_indices_local_fit_full_str.split(','))]
    sampler_dict['search_parameter_indices_local_fit_full'] = np.intersect1d(sampler_dict['search_parameter_indices'], search_parameter_indices_local_fit_full).tolist()
    sampler_dict['search_traj_parameter_keys_local_fit_full'] = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices_local_fit_full']]

    n_param = 9        # Number of parameters used
    randGen = np.random.RandomState(seed=2)


    outdir_psd = 'PSD_{}/'.format(CONST.psd)


    # SET THE INJECTION PARAMETERS
    injection_parameters = set_inj.ini_file_to_dict(opts.inj_file)

    print("\nInjected parameters derived from {} are:".format(opts.inj_file))
    minimum_frequency = CONST.minimum_frequency
    maximum_frequency_ifo = CONST.maximum_frequency_ifo
    duration = CONST.duration
    sampling_frequency = CONST.sampling_frequency
    start_time = CONST.start_time

    # import IPython; IPython.embed(); sys.exit()

    # INITIALIZE THE THREE INTERFEROMETERS
    interferometers = bilby.gw.detector.InterferometerList(ifo_chosen)
    # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        ifo.maximum_frequency = maximum_frequency_ifo
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=CONST.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)


    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=start_time)

    # WAVEFORM GENERATION
    CONST.waveform_arguments['maximum_frequency'] = CONST.maximum_frequency_injected_waveform
    template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    for i in range(len(interferometers)):
        interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain

    # COMPUTE AND PRINT SNR
    CONST.waveform_arguments['maximum_frequency'] = CONST.maximum_frequency_search_waveform
    template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)
    logL, Snr, opt_snr_Best = logL_snr.main(template_ifos, interferometers, True)



    # ROQ Basis used?
    if CONST.roq:
        if injection_parameters['meta']['approximant'] != 'IMRPhenomPv2':
            raise ValueError("Need to set approximant to IMRPhenomPv2 to work with the ROQ basis, approximant here is {}".format(injection_parameters['meta']['approximant']))

        scale_factor = injection_parameters['roq']['scale_factor']
        roq_directory = injection_parameters['roq']['directory']
        # Load in the pieces for the linear part of the ROQ. Note you will need to
        freq_nodes_linear = np.load(roq_directory + "fnodes_linear.npy") * scale_factor
        # Load in the pieces for the quadratic part of the ROQ
        freq_nodes_quadratic = np.load(roq_directory + "fnodes_quadratic.npy") * scale_factor

        # Load the parameters describing the valid parameters for the basis.
        params = injection_parameters['roq']['rescaled_params'].copy()

        roq_outdir = '../__ROQ_weights/' + inj_name + '/' + outdir_psd

        weights_file_path_no_format = roq_outdir + '{:.0f}Hz_{:.0f}Hz_{:.0f}Hz_{:.0f}s_weights'.format(params['flow'], injection_parameters['meta']['maximum_frequency_ifo'], injection_parameters['meta']['maximum_frequency_injected_waveform'], params['seglen'])
        weight_format = 'npz'
        weights_file_path = weights_file_path_no_format + f'.{weight_format}'

        # make ROQ waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=duration, sampling_frequency=sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.binary_neutron_star_roq,
            waveform_arguments=dict(
                frequency_nodes_linear=freq_nodes_linear,
                frequency_nodes_quadratic=freq_nodes_quadratic,
                reference_frequency=injection_parameters['meta']['reference_frequency'], waveform_approximant=injection_parameters['meta']['approximant']),
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters)

        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        priors = bilby.gw.prior.BNSPriorDict()
        # for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'phi_12', 'phi_jl', 'luminosity_distance']:
        for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'luminosity_distance']:
            priors[key] = injection_parameters[key]
        for key in ['mass_1', 'mass_2']:
            priors[key].minimum = max(priors[key].minimum, params['compmin'])
        priors['chirp_mass'] = bilby.core.prior.Constraint(name='chirp_mass', minimum=float(params['chirpmassmin']), maximum=float(params['chirpmassmax']))
        priors['mass_ratio'] = bilby.core.prior.Constraint(0.125, 1, name='mass_ratio')
        priors['geocent_time'] = bilby.core.prior.Uniform(injection_parameters['geocent_time'] - 0.1, injection_parameters['geocent_time'] + 0.1, latex_label='$t_c$', unit='s')

    # INITIALIZATION OF THE LIKELIHOOD OBJECT

    likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
        interferometers=interferometers, waveform_generator=search_waveform_generator,
        weights=weights_file_path, priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])

    likelihood.parameters = injection_parameters.copy()
    print("")
    print("likelihood.log_likelihood_ratio() = {}".format(likelihood.log_likelihood_ratio()))


    search_parameter_keys = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in [1,2]]
    likelihood_gradient = lg.GWTransientLikelihoodGradient(likelihood, search_parameter_keys)
    dlogL_dict = likelihood_gradient.calculate_log_likelihood_gradient()






    # import IPython; IPython.embed(); sys.exit()
    nb_points_per_axis = 50

    phic_list = np.linspace(0, 2*np.pi, nb_points_per_axis)
    psi_list = np.linspace(0, np.pi, nb_points_per_axis)
    logL_list = []
    dlogL_dict_list = {}
    for key in search_parameter_keys:
        dlogL_dict_list[key] = []

    print('Starting the loop on the grid...')
    for phic in phic_list:
        for psi in psi_list:
            likelihood_gradient.likelihood.parameters['phase'] = phic / 2
            likelihood_gradient.likelihood.parameters['psi'] = psi
            logL_list.append(likelihood_gradient.likelihood.log_likelihood_ratio())
            dlogL_dict = likelihood_gradient.calculate_log_likelihood_gradient()
            for key in search_parameter_keys:
                dlogL_dict_list[key].append(dlogL_dict[key])

    print('logL computation on the grid finished.')


    # breakpoint()


    # PLOTTING PART
    phic_mg, psi_mg = np.meshgrid(phic_list, psi_list, sparse=True)
    logL_3D = np.asarray(logL_list).reshape((len(psi_list), len(phic_list)))
    dlogL_dict_3D = {}
    for key in search_parameter_keys:
        dlogL_dict_3D[key] = np.asarray(dlogL_dict_list[key]).reshape((len(psi_list), len(phic_list)))

    # breakpoint()

    for key in search_parameter_keys:
        fig = plt.figure(figsize=(10,10))
        ax = Axes3D(fig)
        ax.plot_surface(phic_mg, psi_mg, dlogL_dict_3D[key], rstride=1, cstride=1, cmap=cm.viridis, shade=True)
        # ax.plot_wireframe(phic_mg, psi_mg, dlogL_dict_3D[key], rstride=3, cstride=3)
        ax.set_xlabel(r'$\phi_c$', fontsize=25)
        ax.set_ylabel(r'$\psi$', fontsize=25)
        ax.set_zlabel(r'$dlnL/d\{}$'.format(key), fontsize=25)
        ax.set_title(r'$dlnL/d\{}$'.format(key), fontsize=25)


    fig = plt.figure(figsize=(10,10))
    ax = Axes3D(fig)
    ax.plot_surface(phic_mg, psi_mg, logL_3D, rstride=1, cstride=1, cmap=cm.viridis, shade=True)
    # ax.plot_wireframe(phic_mg, psi_mg, logL_3D, rstride=3, cstride=3)
    ax.set_xlabel('phi_c', fontsize=25)
    ax.set_ylabel('psi', fontsize=25)
    ax.set_zlabel('logL', fontsize=25)
    ax.set_title('logL', fontsize=25)



    plt.show()
