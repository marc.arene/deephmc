import sys
sys.path.append('../')

import Library.CONST as CONST
import modify_config_2


offsets = [1e-4, 1e-5]

for offset in offsets:
    sampler_dict['parameter_offsets'][0] = offset
    modify_config_2.main()
    print("In ipythonw.py, sampler_dict['parameter_offsets'][0] = {}".format(sampler_dict['parameter_offsets'][0]))

sys.exit()
