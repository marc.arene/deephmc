# Script meant to monitor what is happening at the steps of the numerical trajectories where dlogL goes suddenly from 60 to 1e4 from one step to the other. Hence if inst_index is the step index where we have the instability, we will plot the waveforms at inst_index-1 and inst_index+1 also
import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../')

# import Library.CONST as CONST


instability_analysis_directory = '/Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/newPythonCode/__output_data/GW170817/bilbyoriented/SUB-D_######6##/logL920.44/0e+00_1_2000_200_34_0.005_dlogL2_1e-07/instability_analysis/'

frequency_array = np.loadtxt(instability_analysis_directory + 'frequency_array.dat')

# step_sub_dir_list = ['step_30', 'step_31', 'step_32']
step_sub_dir_list = ['step_30', 'step_31', 'step_32']
h_wave_minus_list = []
h_wave_plus_list = []
h_wave_list = []
diff_list = []
values_list = []
step_number_list = []
for dir in step_sub_dir_list:
    path = instability_analysis_directory + dir
    h_wave_minus = np.loadtxt(path + '/h_wave_1_minus.dat').view(complex)
    h_wave_plus = np.loadtxt(path + '/h_wave_1_plus.dat').view(complex)
    h_wave = np.loadtxt(path + '/h_wave_1.dat').view(complex)
    diff = h_wave_plus - h_wave_minus
    values = np.load(path + '/values.npy')
    step_number = int(dir.split('_')[1])

    h_wave_minus_list.append(h_wave_minus)
    h_wave_plus_list.append(h_wave_plus)
    h_wave_list.append(h_wave)
    diff_list.append(diff)
    values_list.append(values.item())
    step_number_list.append(step_number)

offset = values_list[0]['offset']

f_min = 30
# f_max =36
f_max = frequency_array[-1]
# f_min = 800
# f_max = 804
# f_min = 1500
# f_max = 1504
indices = np.where((f_min<frequency_array) & (frequency_array<f_max))[0]


frequency_array = frequency_array[indices]



# import IPython; IPython.embed()
# sys.exit()
#


from matplotlib import rc

plt.rcParams['text.usetex'] = False

plt.rcParams['figure.titlesize'] = 18
plt.rcParams['figure.titleweight'] = 'bold'

plt.rcParams['axes.titlesize'] = 15
plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titleweight'] = 'normal'

plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12
plt.rcParams['font.size'] = 10


for step_index in range(len(step_sub_dir_list)):

    h_wave_minus_list[step_index] = h_wave_minus_list[step_index][indices]
    h_wave_list[step_index] = h_wave_list[step_index][indices]
    h_wave_plus_list[step_index] = h_wave_plus_list[step_index][indices]
    diff_list[step_index] = diff_list[step_index][indices]


    fig, axs = plt.subplots(2, 2, figsize=(16,8), gridspec_kw = {'height_ratios': [1, 1]})
    # fig, axs = plt.subplots(3, 1, figsize=(15.7,16.16), gridspec_kw = {'height_ratios': [2, 1, 1]}, dpi=[0])
    # https://stackoverflow.com/questions/47633546/relationship-between-dpi-and-figure-size
    # default dpi = 100, size_in_pixel = w*dpi x h*dpi
    # fig, axs = plt.subplots(3, 1, figsize=(9.6,10.2), gridspec_kw = {'height_ratios': [2, 1, 1]}, dpi=160)

    # fig, axs = plt.subplots(3, 1)
    fig.suptitle("Step {}, offset = {:.0e}".format(step_number_list[step_index], offset))
    # fig.text(0.5, 0.8, "Trajectory {} on {}, offset = {:.1e}".format(trajectory_number, CONST.TRAJ_PARAMETERS_KEYS[0][param_index], parameter_offsets[0][param_index]), fontsize=16)
    axs[0][0].plot(frequency_array, h_wave_minus_list[step_index].real, linewidth=1, color='red', label='h_minus')
    axs[0][0].plot(frequency_array, h_wave_list[step_index].real, linewidth=1, color='black', label='h')
    axs[0][0].plot(frequency_array, h_wave_minus_list[step_index].real, linewidth=1, color='green', label='h_plus')
    axs[0][0].set_xlabel("frequencies (Hz)")
    axs[0][0].set_ylabel("Strain")
    axs[0][0].set_title("Real parts")
    axs[0][0].legend()

    axs[1][0].plot(frequency_array, h_wave_minus_list[step_index].imag, linewidth=1, color='red', label='h_minus')
    axs[1][0].plot(frequency_array, h_wave_list[step_index].imag, linewidth=1, color='black', label='h')
    axs[1][0].plot(frequency_array, h_wave_minus_list[step_index].imag, linewidth=1, color='green', label='h_plus')
    axs[1][0].set_xlabel("frequencies (Hz)")
    axs[1][0].set_ylabel("Strain")
    axs[1][0].set_title("Imaginary parts ")
    axs[1][0].legend()

    # Plotting the diff
    axs[0][1].plot(frequency_array, diff_list[step_index].real, linewidth=1, color='black', label='diff')
    axs[0][1].set_xlabel("frequencies (Hz)")
    axs[0][1].set_ylabel("diff")
    axs[0][1].set_title("Real part")
    axs[0][1].legend()

    axs[1][1].plot(frequency_array, diff_list[step_index].imag, linewidth=1, color='black', label='diff')
    axs[1][1].set_xlabel("frequencies (Hz)")
    axs[1][1].set_ylabel("diff")
    axs[1][1].set_title("Imaginary part")
    axs[1][1].legend()

    fig.tight_layout()
    # import IPython; IPython.embed();
    fig.text(x=0.4, y=0.85, s='logL_plus    = {:.6f}\nlogL_minus = {:.6f}\ndlogL = {:.1f}\nsin(dec) = {}'.format(values_list[step_index]['logL_plus'], values_list[step_index]['logL_minus'], values_list[step_index]['dlogL'], values_list[step_index]['sin_dec']))
    fig.subplots_adjust(top=0.80) # Needed so that fig.title and ax.title don't overlap

    file_name = instability_analysis_directory + step_sub_dir_list[step_index] + '/instability_plots_{}.png'.format(step_number_list[step_index])
    fig.savefig(file_name)

plt.show()
