import numpy as np


search_parameter_keys = ['theta_jn', 'phase', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'dec', 'ra', 'geocent_time']

trajectory_functions = {
    'theta_jn': np.cos,
    'phase': None,
    'psi': None,
    'luminosity_distance': np.log,
    'chirp_mass': np.log,
    'reduced_mass': np.log,
    'dec': np.sin,
    'ra': None,
    'geocent_time': np.log
}

trajectory_parameter_keys = {
    'theta_jn': 'cos_theta_jn',
    'phase': None,
    'psi': None,
    'luminosity_distance': 'ln(D)',
    'chirp_mass': 'ln(Mc)',
    'reduced_mass': 'ln(mu)',
    'dec': 'sin(dec)',
    'ra': None,
    'geocent_time': 'ln(tc)'
}

priors = {
    'theta_jn': [-np.pi/2, np.pi/2],
    'phase': [0, 2*np.pi],
    'psi': [0, 2*np.pi],
    'luminosity_distance': [1e-6, 70]
}
