# This script is meant to shed the light on a steppy behavior of the logL as a function of declination and right-ascension at small scales.
# Running this script with `--stepsize=1e-6` shows this.
# Note that this behavior does not appear when varying the other parameters.
import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess
import matplotlib.pyplot as plt

import bilby
from scipy.interpolate import interp1d


from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as CdlogL
import Codes.FIM as FIM

def main(parameters, likelihood_gradient, nb_points, stepsize, pkey):
    """
    Computes the reduced log-likelihood varying parameter[pkey] around the injected value.

    Parameters:
    -----------
    parameters: dict
        Parameters to evaluate the waveform for.
    interferometers:
        List of interferometers to run the analysis on.
    waveform_generator:
        bilby's object needed to generate the source frame polarizations
    nb_points: int
        number of points to compute the logL on, around the injected value.
    stepsize: float
        stepsize between computed points.
    pkey: string
        key of the dictionary `parameters` relative to the parameter we want to vary. This script is meant to work with `key = dec` or `key = ra`.

    Returns:
    --------
    param_linspace: list
        list of parameter values values on which logL has been computed
    logL_linspace: array of shape (nb_points, len(interferometers))
        Contains the values of logL for each ifo for each points in param_linspace
    """
    likelihood_gradient.likelihood.parameters = parameters.copy()

    logL_linspace = []
    d_inner_h_linspace = []
    optimal_snr_squared_linspace = []
    complex_matched_filter_snr_linspace = []
    f_plus_ifos_linspace = []
    f_cross_ifos_linspace = []

    if pkey in ['chirp_mass', 'reduced_mass']:
        param_inj = np.log(parameters[pkey])

        param_max = param_inj + nb_points/2 * stepsize
        param_min = param_inj - nb_points/2 * stepsize

        param_linspace = np.linspace(param_min, param_max, nb_points)

        for param in param_linspace:
            parameters.update({pkey: np.exp(param)})
            pkeys_to_update = ['total_mass', 'symmetric_mass_ratio', 'mass_1', 'mass_2']
            parameters['total_mass'], parameters['symmetric_mass_ratio'], parameters['mass_1'], parameters['mass_2'] = paru.Mc_and_mu_to_M_eta_m1_and_m2(parameters['chirp_mass'], parameters['reduced_mass'])
            for pkey_to_update in pkeys_to_update:
                likelihood_gradient.likelihood.parameters[pkey_to_update] = parameters[pkey_to_update]

            logL, d_inner_h, optimal_snr_squared, complex_matched_filter_snr, f_plus_ifos, f_cross_ifos = log_likelihood_ratio(likelihood_gradient.likelihood)

            logL_linspace.append(logL)
            d_inner_h_linspace.append(d_inner_h)
            optimal_snr_squared_linspace.append(optimal_snr_squared)
            complex_matched_filter_snr_linspace.append(complex_matched_filter_snr)
            f_plus_ifos_linspace.append(f_plus_ifos)
            f_cross_ifos_linspace.append(f_cross_ifos)
    else:
        param_inj = parameters[pkey]

        param_max = param_inj + nb_points/2 * stepsize
        param_min = param_inj - nb_points/2 * stepsize

        param_linspace = np.linspace(param_min, param_max, nb_points)

        for param in param_linspace:
            likelihood_gradient.likelihood.parameters[pkey] = param
            logL, d_inner_h, optimal_snr_squared, complex_matched_filter_snr, f_plus_ifos, f_cross_ifos = log_likelihood_ratio(likelihood_gradient.likelihood)

            logL_linspace.append(logL)
            d_inner_h_linspace.append(d_inner_h)
            optimal_snr_squared_linspace.append(optimal_snr_squared)
            complex_matched_filter_snr_linspace.append(complex_matched_filter_snr)
            f_plus_ifos_linspace.append(f_plus_ifos)
            f_cross_ifos_linspace.append(f_cross_ifos)

    return param_linspace, param_inj, logL_linspace, d_inner_h_linspace, optimal_snr_squared_linspace, complex_matched_filter_snr_linspace, f_plus_ifos_linspace, f_cross_ifos_linspace

def log_likelihood_ratio(likelihood):
    waveform_polarizations =\
        likelihood.waveform_generator.frequency_domain_strain(likelihood.parameters)

    if waveform_polarizations is None:
        return np.nan_to_num(-np.inf)

    d_inner_h = 0.
    optimal_snr_squared = 0.
    complex_matched_filter_snr = 0.
    f_plus_ifos = 0.
    f_cross_ifos = 0.
    if likelihood.time_marginalization:
        d_inner_h_tc_array = np.zeros(
            likelihood.interferometers.frequency_array[0:-1].shape,
            dtype=np.complex128)

    for interferometer in likelihood.interferometers:
        per_detector_snr, f_plus, f_cross = calculate_snrs(likelihood,
            waveform_polarizations=waveform_polarizations,
            interferometer=interferometer)

        d_inner_h += per_detector_snr.d_inner_h
        optimal_snr_squared += np.real(per_detector_snr.optimal_snr_squared)
        complex_matched_filter_snr += per_detector_snr.complex_matched_filter_snr
        f_plus_ifos += f_plus
        f_cross_ifos += f_cross

        if likelihood.time_marginalization:
            d_inner_h_tc_array += per_detector_snr.d_inner_h_squared_tc_array

    if likelihood.time_marginalization:
        log_l = likelihood.time_marginalized_likelihood(
            d_inner_h_tc_array=d_inner_h_tc_array,
            h_inner_h=optimal_snr_squared)

    elif likelihood.distance_marginalization:
        log_l = likelihood.distance_marginalized_likelihood(
            d_inner_h=d_inner_h, h_inner_h=optimal_snr_squared)

    elif likelihood.phase_marginalization:
        log_l = likelihood.phase_marginalized_likelihood(
            d_inner_h=d_inner_h, h_inner_h=optimal_snr_squared)

    else:
        log_l = np.real(d_inner_h) - optimal_snr_squared / 2

    logL = float(log_l.real)

    return logL, d_inner_h, optimal_snr_squared, complex_matched_filter_snr, f_plus_ifos, f_cross_ifos


def calculate_snrs(likelihood, waveform_polarizations, interferometer):
    """
    Compute the snrs for ROQ

    Parameters
    ----------
    waveform_polarizations: waveform
    interferometer: bilby.gw.detector.Interferometer

    """

    f_plus = interferometer.antenna_response(
        likelihood.parameters['ra'], likelihood.parameters['dec'],
        likelihood.parameters['geocent_time'], likelihood.parameters['psi'], 'plus')
    f_cross = interferometer.antenna_response(
        likelihood.parameters['ra'], likelihood.parameters['dec'],
        likelihood.parameters['geocent_time'], likelihood.parameters['psi'], 'cross')

    dt = interferometer.time_delay_from_geocenter(
        likelihood.parameters['ra'], likelihood.parameters['dec'],
        likelihood.parameters['geocent_time'])
    # It is important to split the computation of `ifo_time` with those two lines !
    # Indeed since `parameters['geocent_time']` and `likelihood.strain_data.start_time` are ~1e9, their difference needs to be computed first before adding `time_shift` (which is ~1e-2). Otherwise variations in `time_shift` of the order of 1e-6 are not taken into account in `dt`
    dt_geocent = likelihood.parameters['geocent_time'] - interferometer.strain_data.start_time
    ifo_time = dt_geocent + dt
    # ifo_time = likelihood.parameters['geocent_time'] + dt - \
    #     interferometer.strain_data.start_time

    calib_linear = interferometer.calibration_model.get_calibration_factor(
        likelihood.frequency_nodes_linear,
        prefix='recalib_{}_'.format(interferometer.name), **likelihood.parameters)
    calib_quadratic = interferometer.calibration_model.get_calibration_factor(
        likelihood.frequency_nodes_quadratic,
        prefix='recalib_{}_'.format(interferometer.name), **likelihood.parameters)

    h_plus_linear = f_plus * waveform_polarizations['linear']['plus'] * calib_linear
    h_cross_linear = f_cross * waveform_polarizations['linear']['cross'] * calib_linear
    h_plus_quadratic = (
        f_plus * waveform_polarizations['quadratic']['plus'] * calib_quadratic)
    h_cross_quadratic = (
        f_cross * waveform_polarizations['quadratic']['cross'] * calib_quadratic)

    indices, in_bounds = likelihood._closest_time_indices(
        ifo_time, likelihood.weights['time_samples'])
    if not in_bounds:
        logger.debug("SNR calculation error: requested time at edge of ROQ time samples")
        return likelihood._CalculatedSNRs(
            d_inner_h=np.nan_to_num(-np.inf), optimal_snr_squared=0,
            complex_matched_filter_snr=np.nan_to_num(-np.inf),
            d_inner_h_squared_tc_array=None)

    d_inner_h_tc_array = np.einsum(
        'i,ji->j', np.conjugate(h_plus_linear + h_cross_linear),
        likelihood.weights[interferometer.name + '_linear'][indices])

    d_inner_h = interp1d(
        likelihood.weights['time_samples'][indices],
        d_inner_h_tc_array, kind='cubic', assume_sorted=True)(ifo_time)

    optimal_snr_squared = \
        np.vdot(np.abs(h_plus_quadratic + h_cross_quadratic)**2,
                likelihood.weights[interferometer.name + '_quadratic'])

    complex_matched_filter_snr = d_inner_h / (optimal_snr_squared**0.5)
    d_inner_h_squared_tc_array = None

    return likelihood._CalculatedSNRs(
        d_inner_h=d_inner_h, optimal_snr_squared=optimal_snr_squared,
        complex_matched_filter_snr=complex_matched_filter_snr,
        d_inner_h_squared_tc_array=d_inner_h_squared_tc_array), f_plus, f_cross


if __name__=='__main__':

    # previously was : run BNS_HMC_9D.py input_file #n_traj_hmc_tot #n_traj_fit #n_fit_1 #n_fit_2  #source_id SkipPhase1=False
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)
    parser.add_option("--stepsize", default=1e-7, action="store", type="float", help="""Stepsize used between plotted points around the injeceted value.""")
    parser.add_option("-n", "--nb_points", default=200, action="store", type="int", help="""Number of points to plot. Default is 200""")
    parser.add_option("--pkey", default='reduced_mass', action="store", help="""Key of the parameter in the dictionary. Can either be 'dec' or 'ra'. Default is 'dec'. """)

    parser.add_option("--inj_file", default='../examples/injection_files/GW170817.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will overwrite them.""")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""Print out information of interest during the run.""",dest='verbose')
    parser.add_option("-d", "--debug", default=False, action="store_true", help="""Print out heavy information on trajectories to debug them.""")
    parser.add_option("--stdout_file", default=False, action="store_true", help="""All printed outputs will be written in the file `print_out.txt` instead of the terminal""")
    parser.add_option("--skip_phase1", default=False, action="store_true", help="""Not available at the moment, meant to skip phase 1 once its trajectories will be stored into file...""",dest='SkipPhase1')
    parser.add_option("--continue_phase1", default=False, action="store_true", help="""Not available at the moment, meant to skip phase 1 once its trajectories will be stored into file...""",dest='continue_phase1')
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")

    # parser.add_option("--psd", default=1, action="store", type="int", help="""1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.""", metavar="INT from {1,2,3}")
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")

    # parser.add_option("--search_parameter_indices", default='012345678', action="store", type="string", help=""" Indices of the parameters to run the hmc on. Other are kept fixed. Default are values already stored in config.py file.""")
    # parser.add_option("--parameter_offsets", default='707077777', action="store", type="string", help=""" -log10() of the offset used for central differencing on each parameter. Default is '707077777' meaning sampler_dict['parameter_offsets'] = [1e-07, 0, 1e-07, 0, 1e-07, 1e-07, 1e-07, 1e-07, 1e-07]. 0 Means analytical formula is used if available.""")
    # parser.add_option("--dlogL", default='dlogL1', action="store", type="string", help=""" Method to use to compute the gradient of the log-likelihood. Use `dlogL1` to use  dlogL = <s|dh> - <h|dh>, `dlogL2` for dlogL = logL(p+offset) - logL(p-offset).""")
    # parser.add_option("--ifos", default='H1,L1,V1', action="store", type="string", help=""" Interferometers to run the analysis on. Default iss 'H1,L1,V1' """)



    # PARSE INPUT ARGUMENTS
    (opts,args)=parser.parse_args()
    print("_____________________________")
    print("_____________________________")
    print("Options from the command line are:")
    pu.print_dict(opts.__dict__)

    inj_file_name = opts.inj_file.split('/')[-1]
    inj_name = inj_file_name.split('.')[0]
    SkipPhase1 = opts.SkipPhase1
    verbose = opts.verbose
    RUN_CONST.debug = opts.debug
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    print("\nOptions from the configuration file {} are:".format(opts.config_file))
    pu.print_dict(config_dict)



    n_traj_hmc_tot = config_dict['hmc']['n_traj_hmc_tot']
    n_traj_fit = config_dict['hmc']['n_traj_fit']
    n_traj_for_this_run = config_dict['hmc']['n_traj_for_this_run']
    n_fit_1 = config_dict['hmc']['n_fit_1']
    n_fit_2 = config_dict['hmc']['n_fit_2']
    length_num_traj = config_dict['hmc']['length_num_traj']
    epsilon0 = config_dict['hmc']['epsilon0']

    ifos_possible = ['H1', 'L1', 'V1']
    ifo_chosen = config_dict['analysis']['ifos'].split(',')
    if set.intersection(set(ifos_possible), set(ifo_chosen)) != set(ifo_chosen):
        raise ValueError("the '--ifos' option was wrongly set, you must choose between {}. Example: '--ifos=H1,V1'. ".format(ifos_possible))
    CONST.approximant = config_dict['analysis']['approx']
    CONST.minimum_frequency = config_dict['analysis']['minimum_frequency']
    CONST.maximum_frequency = config_dict['analysis']['maximum_frequency']
    CONST.reference_frequency = config_dict['analysis']['reference_frequency']
    CONST.roq = config_dict['analysis']['roq']
    if CONST.roq:
        CONST.roq_b_matrix_directory = config_dict['analysis']['roq_b_matrix_directory']
    CONST.psd = config_dict['analysis']['psd']
    sampler_dict['dlogL'] = config_dict['analysis']['dlogl']
    sampler_dict['search_parameter_indices'] = [int(i) for i in list(config_dict['analysis']['search_parameter_indices'].split(','))]
    exponents = [int(e) for e in list(config_dict['analysis']['parameter_offsets'].split(','))]
    sampler_dict['parameter_offsets'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    idx_nonzero = np.nonzero(exponents)[0]
    for i in idx_nonzero:
        sampler_dict['parameter_offsets'][i] = 10**(-exponents[i])



    n_param = 9        # Number of parameters used
    randGen = np.random.RandomState(seed=2)




    # SET THE OUTPUT DIRECTORY WHERE OUTPUT FILES WILL BE STORED
    # example: outdir = '../__output_data/GW170817/bilbyoriented/'
    outdir = '../__output_data/' + inj_name + '/'

    # example: outdir = '../__output_data/GW170817/bilbyoriented/SUB-D_######6##/'
    outdir_subD = 'SUB-D_'
    for i in range(sampler_dict['n_dim']):
        if i in sampler_dict['search_parameter_indices']:
            outdir_subD+='{}'.format(i)
        else:
            outdir_subD += '#'
    outdir += outdir_subD + '/'

    # example: outdir = '../__output_data/GW170817/SUB-D_######6##/PSD_1/'
    outdir_psd = 'PSD_{}/'.format(CONST.psd)
    outdir += outdir_psd

    # example: outdir = '../__output_data/GW170817/SUB-D_######6##/PSD_1/NO_SEED/'
    if opts.no_seed: outdir += 'NO_SEED/'

    # example: outdir = '../__output_data/GW170817/SUB-D_######6##/logL920.44/0e+00_1_2000_200_25_0.005_dlogL2_1e-06'
    offset_suffix = ''
    if len(sampler_dict['search_parameter_indices']) == 1 and (sampler_dict['search_parameter_indices'][0]!=1 or sampler_dict['search_parameter_indices'][0]!=3):
        offset_suffix = '_{:.0e}'.format(sampler_dict['parameter_offsets'][sampler_dict['search_parameter_indices'][0]])
    outdir_opts = "{}_{}_{}_{}_{}_{}_{}{}/".format(n_traj_hmc_tot, n_traj_fit, n_fit_1, n_fit_2, length_num_traj, epsilon0/1000, sampler_dict['dlogL'], offset_suffix)
    outdir += outdir_opts

    approx_suffix = CONST.approximant
    if CONST.roq and CONST.approximant == 'IMRPhenomPv2':
        approx_suffix += '_ROQ'
    outdir += approx_suffix + '/'

    if opts.sub_dir is not None: outdir += opts.sub_dir + '/'

    pu.mkdirs(outdir)
    print("\nOutput directory is:\n{}".format(outdir))

    # Save the config file dictionary



    # INITIAL PRINTS, CREATE OUTPUT FILES
    if opts.stdout_file:
        stdout_terminal = sys.stdout # Keep in memory what the terminal output is for stuff we still want to output in the terminal to follow script process
        stdout_file_path = outdir + 'print_out_{}_{}.txt'.format(n_traj_hmc_tot, n_traj_fit)
        print("\nStandard output is now redirected to file:\n{}".format(stdout_file_path))
        sys.stdout = open(stdout_file_path, 'w')

        print("Options from the command line are:")
        pu.print_dict(opts.__dict__)
        print("\nOptions from the configuration file {} are:".format(opts.config_file))
        pu.print_dict(config_dict)

    # Create output files: chain file, info file, file containing the coefficients of global cubic fit, file containing the positions and gradients of points used for fits
    samples_file_path = "{}samples.dat".format(outdir)						# chain file
    stderr_path = "{}BNS_HMC_Information.dat".format(outdir)			            # info file
    fileOutCoeffGood_path = "{}BNS_HMC_CoeffFit.dat".format(outdir)					# fit coeffiients file
    fileOutPointsFit_path = "{}BNS_HMC_PointsFit.dat".format(outdir)				# positions and values of gradients at fit points





    # SET THE INJECTION PARAMETERS
    injection_parameters = set_inj.ini_file_to_dict(opts.inj_file)
    print("\nInjected parameters derived from {} are:".format(opts.inj_file))
    pu.print_dict(injection_parameters)
    minimum_frequency = CONST.minimum_frequency
    maximum_frequency_ifo = CONST.maximum_frequency_ifo
    duration = CONST.duration
    sampling_frequency = CONST.sampling_frequency
    start_time = CONST.start_time

    # import IPython; IPython.embed(); sys.exit()

    # INITIALIZE THE THREE INTERFEROMETERS
    interferometers = bilby.gw.detector.InterferometerList(ifo_chosen)
    # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        ifo.maximum_frequency = maximum_frequency_ifo
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=CONST.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)


    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=start_time)

    # WAVEFORM GENERATION
    CONST.waveform_arguments['maximum_frequency'] = CONST.maximum_frequency_injected_waveform
    template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    for i in range(len(interferometers)):
        interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain

    # COMPUTE AND PRINT SNR
    CONST.waveform_arguments['maximum_frequency'] = CONST.maximum_frequency_search_waveform
    template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)
    logL, Snr, opt_snr_Best = logL_snr.main(template_ifos, interferometers, True)


    # COMPUTE dlogL and print it to check with the plots
    dlogL = CdlogL.main(injection_parameters, start_time, minimum_frequency, interferometers, True)

    # DEFINE BOUNDARY CONDITIONS FOR HMC
    # The boundary conditions are given both for masses m1, m2 (solar masses) and for the distance (meters)
    boundaries = dict(
        m1min = 1,
        m1max = 2.6,
        m2min = 1,
        m2max = 2.6,
        Dmin = 1e-6,
        Dmax = 70
    )

    injection_parameters['boundaries'] = boundaries

    boundary=np.array([[boundaries['m1min'],boundaries['m1max']],[boundaries['m2min'],boundaries['m2max']],[boundaries['Dmin'] * MPC, boundaries['Dmax'] * MPC]])

    print("\nAstrophysical boundaries :")
    print("m1min = {}, m1max = {}".format(boundary[0][0], boundary[0][1]))
    print("m2min = {}, m2max = {}".format(boundary[1][0], boundary[1][1]))
    print("Dmin = {}, Dmax = {}".format(boundary[2][0]/MPC, boundary[2][1]/MPC))



    # ROQ Basis used?
    if CONST.roq:
        if injection_parameters['meta']['approximant'] != 'IMRPhenomPv2':
            raise ValueError("Need to set approximant to IMRPhenomPv2 to work with the ROQ basis, approximant here is {}".format(injection_parameters['meta']['approximant']))

        scale_factor = injection_parameters['roq']['scale_factor']
        roq_directory = injection_parameters['roq']['directory']
        # Load in the pieces for the linear part of the ROQ. Note you will need to
        freq_nodes_linear = np.load(roq_directory + "fnodes_linear.npy") * scale_factor
        # Load in the pieces for the quadratic part of the ROQ
        freq_nodes_quadratic = np.load(roq_directory + "fnodes_quadratic.npy") * scale_factor

        # Load the parameters describing the valid parameters for the basis.
        params = injection_parameters['roq']['rescaled_params'].copy()

        roq_outdir = '../__ROQ_weights/' + inj_name + '/'
        weights_file_path = roq_outdir + '{:.0f}Hz_{:.0f}Hz_{:.0f}Hz_{:.0f}s_weights.json'.format(params['flow'], injection_parameters['meta']['maximum_frequency_ifo'], injection_parameters['meta']['maximum_frequency_injected_waveform'], params['seglen'])

        # make ROQ waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=duration, sampling_frequency=sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.binary_neutron_star_roq,
            waveform_arguments=dict(
                frequency_nodes_linear=freq_nodes_linear,
                frequency_nodes_quadratic=freq_nodes_quadratic,
                reference_frequency=injection_parameters['meta']['reference_frequency'], waveform_approximant=injection_parameters['meta']['approximant']),
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters)

        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        priors = bilby.gw.prior.BNSPriorDict()
        # for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'phi_12', 'phi_jl', 'luminosity_distance']:
        for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'luminosity_distance']:
            priors[key] = injection_parameters[key]
        for key in ['mass_1', 'mass_2']:
            priors[key].minimum = max(priors[key].minimum, params['compmin'])
        priors['chirp_mass'] = bilby.core.prior.Constraint(name='chirp_mass', minimum=float(params['chirpmassmin']), maximum=float(params['chirpmassmax']))
        priors['mass_ratio'] = bilby.core.prior.Constraint(0.125, 1, name='mass_ratio')
        priors['geocent_time'] = bilby.core.prior.Uniform(injection_parameters['geocent_time'] - 0.1, injection_parameters['geocent_time'] + 0.1, latex_label='$t_c$', unit='s')

        pu.mkdirs(outdir)


        if os.access(weights_file_path, os.W_OK):
            # load the weights from the file
            likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                weights=weights_file_path, priors=priors)
        else:
            # Load in the pieces for the linear part of the ROQ. Note you will need to
            basis_matrix_linear = np.load(roq_directory + "B_linear.npy").T
            # Load in the pieces for the quadratic part of the ROQ
            basis_matrix_quadratic = np.load(roq_directory + "B_quadratic.npy").T

            # Redoing here a bit what's already being done in set_inj because `bilby.gw.likelihood.ROQGravitationalWaveTransient()` does not accept dictionnary for roq_params...
            roq_params_ndarray = np.genfromtxt(injection_parameters['roq']['params_path'], names=True)
            roq_params_ndarray = roqu.rescale_params(roq_params_ndarray, scale_factor)
            # roq_params_ndarray = None

            likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic,
                priors=priors, roq_params=roq_params_ndarray)

            # remove the basis matrices as these are big for longer bases
            del basis_matrix_linear, basis_matrix_quadratic

            # write the weights to file so they can be loaded multiple times
            likelihood.save_weights(weights_file_path)


        search_parameter_keys = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
        likelihood.parameters = injection_parameters

        logL = likelihood.log_likelihood_ratio()
        print("")
        print("likelihood.log_likelihood_ratio() = {}".format(logL))

        likelihood_gradient = lg.GWTransientLikelihoodGradient(likelihood, search_parameter_keys)
        dlogL_dict = likelihood_gradient.calculate_log_likelihood_gradient()
        pu.print_dict({"dlogL_ROQ": dlogL_dict})
        dlogL = likelihood_gradient.convert_dlogL_dict_to_np_array(dlogL_dict)
    else:
        likelihood_gradient = None

    # point of interest for the peak appearing at trajectory nb 466 in dlogL/dln(mu)
    point_of_interest = {'iota': 2.2551332663739028, 'theta_jn': 2.2551332663739028, 'phase': 2.356194490192345, 'psi': 1.8869028282434492, 'luminosity_distance': 26.06694524598723, 'chirp_mass': 1.1889187477865588, 'reduced_mass': 0.6828544847608546, 'dec': -0.42784129377440505, 'ra': 3.4500951273835714, 'geocent_time': 1187008882.4299889, 'mass_1': 1.3660799433474224, 'mass_2': 1.3653381971252285, 'total_mass': 2.731418140472651, 'symmetric_mass_ratio': 0.24999998156367667, 'chi_1': 0, 'chi_2': 0}


    # point_of_interest = {'iota': 2.412387, 'theta_jn': 2.412387, 'phase': 2.356194490192345, 'psi': 2.062567, 'luminosity_distance': 30.791833, 'chirp_mass': 1.188849320847, 'reduced_mass': 0.6818916671875, 'dec': -0.440832, 'ra': 3.464403, 'geocent_time': 1187008882.43 + 40.78308901887, 'mass_1': 1.4479096490086982, 'mass_2': 1.2888960154069415, 'total_mass': 2.7368056644156398, 'symmetric_mass_ratio': 0.2491560420433056, 'chi_1': 0, 'chi_2': 0}
    # point of interest when I had a steppy behavior on 'ra' because only 5 time samples where used for interpoloation
    # point_of_interest = {'iota': 2.81, 'theta_jn': 2.81, 'phase': 2.356194490192345, 'psi': 2.21, 'luminosity_distance': 39.999999999999886, 'chirp_mass': 1.1888375686890265, 'reduced_mass': 0.6813503649635037, 'dec': -0.4201637618271667, 'ra': 3.4412760, 'geocent_time': 1187008882.43, 'mass_1': 1.4700000000000013, 'mass_2': 1.269999999999999, 'total_mass': 2.74, 'symmetric_mass_ratio': 0.2486680164100378, 'chi_1': 0, 'chi_2': 0}





    # COMPUTE THE LOGL AROUND THE INJECTED VALUE OF SIN(DEC)
    param_linspace, param_inj, logL_linspace, d_inner_h_linspace, optimal_snr_squared_linspace, complex_matched_filter_snr_linspace, f_plus_ifos_linspace, f_cross_ifos_linspace = main(point_of_interest, likelihood_gradient, opts.nb_points, opts.stepsize, opts.pkey)




    # PLOT
    from matplotlib import rc

    plt.rcParams['text.usetex'] = False

    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'

    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'

    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10

    # stuff_to_plot = [logL_linspace, d_inner_h_linspace, optimal_snr_squared_linspace, complex_matched_filter_snr_linspace, f_plus_ifos_linspace, f_cross_ifos_linspace]
    # stuff_to_plot_names = ["logL", "d_inner_h", "optimal_snr_squared", "complex_matched_filter_snr", "f_plus_ifos_linspace", "f_cross_ifos_linspace"]


    stuff_to_plot = [logL_linspace]
    stuff_to_plot_names = ["logL"]

    for i in range(len(stuff_to_plot)):
        fig= plt.figure(figsize=(16,8))
        ax0 = plt.subplot(111)
        ax0.plot(param_linspace, stuff_to_plot[i], linewidth=1, marker='o', markersize=3, label='logL')
        ax0.axvline(x=param_inj, linewidth=1, color='black', label='injected value')
        xlabel = opts.pkey
        ax0.set_xlabel(xlabel)
        ax0.set_ylabel(stuff_to_plot_names[i])
        ax0.set_title('{}. Step size between plotted points =  {:.0e}'.format(stuff_to_plot_names[i], opts.stepsize))
        ax0.legend(loc=(0.85,0.7))

        fig.tight_layout()


    # ifo_colors = ['red', 'green', 'purple']
    # ax0 = plt.subplot(111)
    # for i, ifo in enumerate(interferometers):
    #     ax0.plot(param_linspace, dt_ifos_steppy[:, i], linewidth=1, marker='o', markersize=3, label='dt_{} = parameters[\'geocent_time\'] + time_shift - start_time'.format(ifo.name).format(ifo.name))
    #     ax0.plot(param_linspace, dt_ifos_smooth[:, i], linewidth=1, marker='o', markersize=3, label='dt_{} = parameters[\'geocent_time\'] - start_time + time_shift'.format(ifo.name).format(ifo.name))
    #     ax0.axvline(x=param_inj, linewidth=0.5, color='black', label='injected value')
    #     xlabel = opts.pkey
    #     ax0.set_xlabel(xlabel)
    #     ax0.set_ylabel('dt')
    #     ax0.set_title('Two different results for dt')
    #     ax0.legend()
        # ax0.legend(loc=(0.85,0.7))

    # # UNCOMMENT THIS SECTION TO PLOT THE 3 LOGL INDEPENDENTLY
    # ifo_colors = ['red', 'green', 'purple']
    # for i, ifo in enumerate(interferometers):
    #     ax = ax0.twinx()
    #     ax.plot(param_linspace, logL_ifos_linspace[:, i], linewidth=0.5, label='logL_{}'.format(ifo.name), color=ifo_colors[i])
    #     # ax00.set_ylabel('logL_{}'.format(ifo.name))
    #     ax.tick_params('y', colors=ifo_colors[i])
    #     y_legend = 0.7 - (i+1)*0.05
    #     ax.legend(loc=(0.85,y_legend))

    # Plotting the timeshifts of each ifo independently
    # ifo_colors = ['red', 'green', 'purple']
    # for i, ifo in enumerate(interferometers):
    #     ax = ax0.twinx()
    #     ax.plot(param_linspace, dt_ifos_smooth[:, i], linewidth=0.5, marker='x', markersize=1, label='dt_{} = '.format(ifo.name), color=ifo_colors[i])
    #     ax.set_ylabel('time_shift_{}'.format(ifo.name), color=ifo_colors[i])
    #     ax.tick_params('y', colors=ifo_colors[i])
    #     y_legend = 0.9 - (i+1)*0.05
    #     ax.legend(loc=(0.70,y_legend))


    file_name = './.logL_vs_param_standalone/logL_vs_{}_standalone_{}.png'.format(opts.pkey, opts.stepsize)
    # fig.savefig(file_name)

    plt.show()
