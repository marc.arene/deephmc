import sys
sys.path.append('../')
import numpy as np
import matplotlib.pyplot as plt
import os

import Library.python_utils as pu
import Library.CONST as CONST

from optparse import OptionParser
from configparser import ConfigParser

usage = """%prog [options]
Plots one hmc trajectory for each parameter involved."""
parser=OptionParser(usage)
parser.add_option("--search_parameter_indices", default='012345678', action="store", type="string", help=""" Indices of the parameters to run the hmc on. Other are kept fixed. Default are values already stored in /opts_dict.npy file.""")
parser.add_option("--traj_nb", default=1, action="store", type="int", help=""" Which trajectory to plot. Default is 1. """,metavar="")

(opts,args)=parser.parse_args()

output_path = sys.argv[1]
output_path_phase1 = output_path + '/phase1'

trajectory_number = opts.traj_nb
# trajectory_number = 1
# if len(sys.argv)==3: trajectory_number = int(sys.argv[2])

trajectory_index = trajectory_number - 1

if os.path.isfile(output_path + '/conf_dict.npy'):
    conf_dict_file = output_path + '/conf_dict.npy'
    conf_dict = np.load(conf_dict_file).item()
    search_parameter_indices = conf_dict['search_parameter_indices']
    parameter_offsets = conf_dict['parameter_offsets']
    lengthT = conf_dict['length_num_traj']
elif os.path.isfile(output_path + '/opts_dict.npy'):
    conf_dict_file = output_path + '/opts_dict.npy'
    conf_dict = np.load(conf_dict_file).item()
    search_parameter_indices = [int(i) for i in list(conf_dict['search_parameter_indices'])]
    exponents = [int(e) for e in list(conf_dict['parameter_offsets'])]
    parameter_offsets = [10**(-e) for e in exponents]
    lengthT = conf_dict['length_num_traj']
elif os.path.isfile(output_path + '/config.json'):
    conf_dict_file = output_path + '/config.json'
    conf_dict = pu.json_to_dict(conf_dict_file)
    search_parameter_indices = [int(i) for i in list(conf_dict['analysis']['search_parameter_indices'].split(','))]
    exponents = [int(e) for e in list(conf_dict['analysis']['parameter_offsets'].split(','))]
    parameter_offsets = [10**(-e) for e in exponents]
    lengthT = conf_dict['hmc']['length_num_traj']
elif os.path.isfile(output_path + '/config.ini'):
    config_file_path = output_path + '/config.ini'
    config_parser = ConfigParser()
    config_parser.read(config_file_path)
    conf_dict = pu.config_parser_to_dict(config_parser)
    search_parameter_indices = [int(i) for i in list(conf_dict['analysis']['search_parameter_indices'].split(','))]
    exponents = [int(e) for e in list(conf_dict['analysis']['parameter_offsets'].split(','))]
    parameter_offsets = [10**(-e) for e in exponents]
    lengthT = conf_dict['hmc']['length_num_traj']
elif os.path.isfile(output_path + '/opts.json'):
    conf_dict_file = output_path + '/opts.json'
    conf_dict = pu.json_to_dict(conf_dict_file)
    search_parameter_indices = [int(i) for i in list(conf_dict['search_parameter_indices'])]
    exponents = [int(e) for e in list(conf_dict['parameter_offsets'])]
    parameter_offsets = [10**(-e) for e in exponents]
    lengthT = conf_dict['length_num_traj']
else:
    print("ERROR: COULD NOT FIND ANY SORT OF CONFIGURATION FILE")
    sys.exit()

# Intersect the default search_parameter_indices to plot on w.r.t those asked in input
parameter_indices_input = [int(i) for i in list(opts.search_parameter_indices)]
search_parameter_indices = list(set.intersection(set(parameter_indices_input), set(search_parameter_indices)))
search_parameter_indices.sort()

pt_fit_traj_file = output_path_phase1 + '/pt_fit_phase1.dat'
dlogL_file = output_path_phase1 + '/dlogL_fit_phase1.dat'
H_p_logL_logP_file = output_path_phase1 + '/H_p_logL_logP.dat'
pmom_trajs_file = output_path_phase1 + '/pmom_trajs.dat'
scale_file = output_path + '/scale.dat'

# # Pb with np.loadtxt(): when there is only one index in the file, the resulting array has a shape = (), but size still equal to 1. Reshaping it puts it with shape = (1,) and hence it can be normally manipulated then
# if search_parameter_indices.size == 1:
#     search_parameter_indices = search_parameter_indices.reshape(1)
# # Need the indices to be integers
# search_parameter_indices = search_parameter_indices.astype(np.int64)
pt_fit_traj = np.loadtxt(pt_fit_traj_file).T
dlogL = np.loadtxt(dlogL_file).T
H_p_logL_logP = np.loadtxt(H_p_logL_logP_file).T
pmom_trajs = np.loadtxt(pmom_trajs_file).T
try:
    scale = np.loadtxt(scale_file).T
except FileNotFoundError:
    print("No scale.dat file found")
    scale = [None for i in range(sampler_dict['n_dim'])]

# q_pos = pt_fit_traj[search_parameter_indices]
# dlogL = dlogL_all[search_parameter_indices]

pt_fit_traj = pt_fit_traj[:, trajectory_index * lengthT: (trajectory_index +1) * lengthT]
dlogL = dlogL[:, trajectory_index * lengthT: (trajectory_index +1) * lengthT]
H_p_logL_logP = H_p_logL_logP[:, trajectory_index * lengthT: (trajectory_index +1) * lengthT]
pmom_trajs = pmom_trajs[:, trajectory_index * lengthT: (trajectory_index +1) * lengthT]


steps = np.arange(len(pt_fit_traj[0]))


# import IPython; IPython.embed();


from matplotlib import rc
# rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
# ## for Palatino and other serif fonts use:
# # rc('font',**{'family':'serif','serif':['Palatino']})
# rc('text', usetex=True)
#
# plt.rc('text', usetex=False)
# plt.rc('font', family='serif')
# plt.rc('font', size=10)
# plt.rc('font', weight='bold')

# axes_fontdict = {'fontsize': 20,
#         'fontweight': 'bold',
#         'verticalalignment': 'baseline',
#         'horizontalalignment': 'center'}
# axes_titleweight = 'bold'

plt.rcParams['text.usetex'] = False

plt.rcParams['figure.titlesize'] = 18
plt.rcParams['figure.titleweight'] = 'bold'

plt.rcParams['axes.titlesize'] = 15
plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titleweight'] = 'normal'

plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12
plt.rcParams['font.size'] = 10

for param_index in search_parameter_indices:
    H_start = H_p_logL_logP[0, 0]
    H_end = H_p_logL_logP[0, -1]
    acc_proba = np.exp(-(H_end-H_start))
    # plt.figure(figsize=(15,8))
    # plt.subplot(211)
    # plt.plot(steps, pt_fit_traj[param_index], linewidth=1, color='black', label="{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    # plt.xlabel('steps')
    # plt.ylabel("{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index])
    # plt.title("{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    # plt.subplot(212)
    # plt.plot(steps, dlogL[param_index], linewidth=1, color='red', label="dlogL/d{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    # plt.xlabel('steps')
    # plt.ylabel("dlogL/d{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index])
    # plt.title("dlogL/d{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    # plt.tight_layout()

    # import IPython; IPython.embed()
    # sys.exit()

    fig, axs = plt.subplots(2, 2, figsize=(16,8.2), gridspec_kw = {'height_ratios': [1, 1]})
    # fig, axs = plt.subplots(3, 1, figsize=(15.7,16.16), gridspec_kw = {'height_ratios': [2, 1, 1]}, dpi=[0])
    # https://stackoverflow.com/questions/47633546/relationship-between-dpi-and-figure-size
    # default dpi = 100, size_in_pixel = w*dpi x h*dpi
    # fig, axs = plt.subplots(3, 1, figsize=(9.6,10.2), gridspec_kw = {'height_ratios': [2, 1, 1]}, dpi=160)

    # fig, axs = plt.subplots(3, 1)
    fig.suptitle("Trajectory {} on {}, offset = {:.0e}: Acc={:.5f}".format(trajectory_number, CONST.TRAJ_PARAMETERS_KEYS[param_index], parameter_offsets[param_index], acc_proba))
    # fig.text(0.5, 0.8, "Trajectory {} on {}, offset = {:.1e}".format(trajectory_number, CONST.TRAJ_PARAMETERS_KEYS[param_index], parameter_offsets[param_index]), fontsize=16)
    axs[0][0].plot(pt_fit_traj[param_index], pmom_trajs[param_index], linewidth=1, color='black', label='Trajectory')
    axs[0][0].plot(pt_fit_traj[param_index][0], pmom_trajs[param_index][0], marker='o', markersize=9, color='green', label='Start')
    axs[0][0].plot(pt_fit_traj[param_index][-1], pmom_trajs[param_index][-1], marker='o', markersize=9, color='red', label='End')
    axs[0][0].set_xlabel("{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]), color='black')
    axs[0][0].set_ylabel("p({})".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]), color='black')
    axs[0][0].set_title("Trajectory in phase space")
    axs[0][0].legend()


    axs[1][0].plot(pt_fit_traj[param_index], H_p_logL_logP[2], linewidth=1, color='green', label="logL")
    axs[1][0].set_xlabel(CONST.TRAJ_PARAMETERS_KEYS[param_index])
    axs[1][0].set_ylabel('logL')
    axs[1][0].legend(loc=(0.8,0.9))

    ax101 = axs[1][0].twinx()
    ax101.plot(pt_fit_traj[param_index], dlogL[param_index], linewidth=1, color='red', label="dlogL")
    # ax11.set_ylabel("dlogL/d{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]), color='red')
    ax101.tick_params('y', colors='red')
    ax101.legend(loc=(0.8,0.8))



    # fig = plt.figure(figsize=(15,8))
    # import IPython; IPython.embed();
    # ax1 = fig.add_subplot(211)
    axs[0][1].plot(steps, pt_fit_traj[param_index], linewidth=1, color='black', label="{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    axs[0][1].set_xlabel('steps')
    axs[0][1].set_ylabel("{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]), color='black')
    axs[0][1].tick_params('y', colors='black')
    axs[0][1].set_title("Step evolution of {}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))


    ax011 = axs[0][1].twinx()
    ax011.plot(steps, dlogL[param_index], linewidth=1, color='red', label="dlogL/d{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    ax011.set_ylabel("dlogL/d{}".format(CONST.TRAJ_PARAMETERS_KEYS[param_index]), color='red')
    ax011.tick_params('y', colors='red')

    # axs[2] = fig.add_subplot(212)
    axs[1][1].plot(steps, H_p_logL_logP[0], linewidth=1, color='black', label="H")
    # axs[1][1].plot(steps, H_p_logL_logP[1], linewidth=1, color='red', label="0.5*p**2")
    axs[1][1].plot(steps, -H_p_logL_logP[2], linewidth=1, color='green', label="-logL")
    axs[1][1].set_xlabel('steps')
    axs[1][1].legend()
    axs[1][1].set_title("Step evolution of H's components".format(trajectory_number))

    ax111 = axs[1][1].twinx()
    ax111.plot(steps, H_p_logL_logP[1], linewidth=0.5, color='blue', label="0.5*p^2")
    ax111.plot(steps, 0.5 * pmom_trajs[param_index]**2, linewidth=0.8, color='orange', label="0.5*p_param^2")
    ax111.tick_params('y', colors='blue')
    ax111.legend(loc=(0.8,0.7))

    fig.tight_layout()
    # import IPython; IPython.embed();
    fig.text(x=0.4, y=0.93, s='scale = {:.2e}'.format(scale[param_index]))
    fig.subplots_adjust(top=0.88) # Needed so that fig.title and ax.title don't overlap

    file_name = output_path_phase1 + '/plots/traj_{}_{}-{}_{:.0e}.png'.format(trajectory_number, param_index,  CONST.TRAJ_PARAMETERS_KEYS[param_index], parameter_offsets[param_index])
    # fig.savefig(file_name)

# fig = plt.figure(figsize=(15,8))
# plt.plot(steps, H_p_logL_logP[0], linewidth=1, color='black', label="H")
# plt.plot(steps, H_p_logL_logP[1], linewidth=1, color='red', label="0.5*p^2")
# plt.plot(steps, H_p_logL_logP[2], linewidth=1, color='green', label="logL")
# plt.xlabel('steps')
# plt.legend()
# plt.title("Trajectory evolution of H's components")

plt.show()
