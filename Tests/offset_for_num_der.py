import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import numpy as np
import matplotlib.pyplot as plt


import bilby
import Library.bilby_waveform as bilby_wv
from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Codes.set_injection_parameters as set_inj


def numerical_derivative(parameters, start_time, minimum_frequency, interferometers, offset):
    """
    Computes the numerical derivative using central differencing for the two parameters: phic and log(luminosity_distance)

    Return
    ------
        dtemplate_ifos
    """


    template_ifos = bilby_wv.WaveForm_ThreeDetectors(parameters, minimum_frequency, interferometers)

    number_of_parameters = 3
    waveform_length = len(h_wave_1)

    dh_Wave_1 = np.empty((number_of_parameters,waveform_length), dtype=np.cfloat)
    dh_Wave_2 = np.empty((number_of_parameters,waveform_length), dtype=np.cfloat)
    dh_Wave_3 = np.empty((number_of_parameters,waveform_length), dtype=np.cfloat)


    # Deriving w.r.t. phi_c = parameters['phase'] * 2
    parameters_minus = parameters.copy()
    parameters_minus['phase'] = parameters['phase'] - offset/2

    parameters_plus = parameters.copy()
    parameters_plus['phase'] = parameters['phase'] + offset/2

    template_ifos_minus = bilby_wv.WaveForm_ThreeDetectors(parameters_minus, minimum_frequency, interferometers)
    template_ifos_plus = bilby_wv.WaveForm_ThreeDetectors(parameters_plus, minimum_frequency, interferometers)

    dh_Wave_1[0] = (h_wave_1_plus-h_wave_1_minus)/(2*offset)
    dh_Wave_2[0] = (h_wave_2_plus-h_wave_2_minus)/(2*offset)
    dh_Wave_3[0] = (h_wave_3_plus-h_wave_3_minus)/(2*offset)


    # Deriving w.r.t. ln(D) where D = parameters['luminosity_distance']
    # Here since h ~ constant/D, dh/dD = -h/D. Hence dh/dlnD = D*dh/dD = -h
    parameters_minus = parameters.copy()
    parameters_minus['luminosity_distance'] = np.exp(np.log(parameters['luminosity_distance']) - offset)

    parameters_plus = parameters.copy()
    parameters_plus['luminosity_distance'] = np.exp(np.log(parameters['luminosity_distance']) + offset)

    template_ifos_minus = bilby_wv.WaveForm_ThreeDetectors(parameters_minus, minimum_frequency, interferometers)
    template_ifos_plus = bilby_wv.WaveForm_ThreeDetectors(parameters_plus, minimum_frequency, interferometers)

    dh_Wave_1[1] = (h_wave_1_plus-h_wave_1_minus)/(2*offset)
    dh_Wave_2[1] = (h_wave_2_plus-h_wave_2_minus)/(2*offset)
    dh_Wave_3[1] = (h_wave_3_plus-h_wave_3_minus)/(2*offset)



    # Deriving w.r.t. ln(tc)
    parameters_minus = parameters.copy()
    parameters_minus['geocent_time'] = np.exp(np.log(parameters['geocent_time'] - start_time) - offset) + start_time
    # parameters_minus['geocent_time'] = (parameters['geocent_time'] - start_time)*np.exp(-offset) + start_time

    parameters_plus = parameters.copy()
    parameters_plus['geocent_time'] = np.exp(np.log(parameters['geocent_time'] - start_time) + offset) + start_time
    # parameters_plus['geocent_time'] = (parameters['geocent_time'] - start_time)*np.exp(offset) + start_time

    template_ifos_minus = bilby_wv.WaveForm_ThreeDetectors(parameters_minus, minimum_frequency, interferometers)
    template_ifos_plus = bilby_wv.WaveForm_ThreeDetectors(parameters_plus, minimum_frequency, interferometers)

    dh_Wave_1[2] = (h_wave_1_plus-h_wave_1_minus)/(2*offset)
    dh_Wave_2[2] = (h_wave_2_plus-h_wave_2_minus)/(2*offset)
    dh_Wave_3[2] = (h_wave_3_plus-h_wave_3_minus)/(2*offset)

    return dtemplate_ifos


def analytical_derivative(parameters, start_time, minimum_frequency, interferometers):
    """
    Computes the analytical derivative using for the two parameters: phic and log(luminosity_distance).

    Return
    ------
        dtemplate_ifos
    """


    template_ifos = bilby_wv.WaveForm_ThreeDetectors(parameters, minimum_frequency, interferometers)

    number_of_parameters = 3
    waveform_length = len(h_wave_1)

    dh_Wave_1 = np.empty((number_of_parameters,waveform_length), dtype=np.cfloat)
    dh_Wave_2 = np.empty((number_of_parameters,waveform_length), dtype=np.cfloat)
    dh_Wave_3 = np.empty((number_of_parameters,waveform_length), dtype=np.cfloat)


    # Deriving w.r.t. phi_c = parameters['phase'] * 2
    # h(f) = A * np.exp()
    # dh/dphic    = (h(phic+offset)-h(phic-offset))/(2*offset)
    #             = (h(phi_c)*np.exp(i*offset)-h(phi_c)*np.exp(-i*offset))/2*offset # if -1j convention used
    #             = i*sin(offset)*h(phi_ref)/offset
    #             = i * h # since offset is really small
    dh_Wave_1[0] = 1j * h_wave_1
    dh_Wave_2[0] = 1j * h_wave_2
    dh_Wave_3[0] = 1j * h_wave_3


    # Deriving w.r.t. ln(D) where D = parameters['luminosity_distance']
    # Here since h ~ constant/D, dh/dD = -h/D. Hence dh/dlnD = D*dh/dD = -h
    dh_Wave_1[1] = -h_wave_1
    dh_Wave_2[1] = -h_wave_2
    dh_Wave_3[1] = -h_wave_3

    # Deriving w.r.t. ln(tc) where tc = parameters['meta']['tc_3p5PN']
    two_pi_f_tc = -1j * 2*np.pi*interferometers.frequency_array*(parameters['geocent_time']-start_time)
    dh_Wave_1[2] = two_pi_f_tc * h_wave_1
    dh_Wave_2[2] = two_pi_f_tc * h_wave_2
    dh_Wave_3[2] = two_pi_f_tc * h_wave_3

    return dtemplate_ifos

# ## Overlap the results
def scalarProduct(a, b):
    return (a.real*b.real + a.imag*b.imag).sum()

def overlap(a, b):
    aa = scalarProduct(a, a)
    bb = scalarProduct(b, b)
    ab = scalarProduct(a, b)
    norm = np.sqrt(aa * bb)
    return ab / norm

def print_overlaps(offsets, overlaps, interferometers, ifo_index):

    print("\nOverlaps values for {}".format(interferometers[ifo_index].name))
    print("{:8} | {:20} | {:20} | {:20}".format("offsets","phic","ln(D)","ln(tc)"))
    for i in range(len(offsets)):
        print("{:6.2e} | {:20} | {:20} | {:20}".format(offsets[i], overlaps[ifo_index][0][i], overlaps[ifo_index][1][i], overlaps[ifo_index][2][i]))
    print("")

#################################
# INJECTION PARAMETERS SETTINGS #
#-------------------------------#
# Params for GW170817
injection_parameters = dict(
    mass_1=1.47,
    mass_2=1.27,
    chi_1=0.0,
    chi_2=0.0,
    luminosity_distance=40,
    # iota=146*np.pi/180,
    iota=2.81,
    psi=2.21,
    phase=5.497787143782138,
    geocent_time=1187008882.43,
    ra=3.44616, # value from fixed skyposition run
    dec=-0.408084, # value from fixed skyposition run
    lambda_1=0.,
    lambda_2=0.
    )

injection_parameters = bilby.gw.conversion.generate_mass_parameters(injection_parameters)
injection_parameters['reduced_mass'] = (injection_parameters['mass_1'] * injection_parameters['mass_2']) / (injection_parameters['mass_1'] + injection_parameters['mass_2'])
# Set the duration and sampling frequency of the data segment that we're going
# to inject the signal into. For the
# TaylorF2 waveform, we cut the signal close to the isco frequency
minimum_frequency=30.0
m1 = injection_parameters['mass_1']
m2 = injection_parameters['mass_2']
tc_3p5PN = set_inj.ComputeChirpTime3p5PN(minimum_frequency, m1, m2)
# We integrate the signal up to the frequency of the "Innermost stable circular orbit (ISCO)"
R_isco = 6.      # Orbital separation at ISCO, in geometric units. 6M for PN ISCO; 2.8M for EOB
f_high = set_inj.Compute_fISCO(R_isco, m1, m2)

# import IPython; IPython.embed();
duration = tc_3p5PN + 2
# duration = tc_3p5PN * 1.1
sampling_frequency = 4096
# sampling_frequency = 2 * f_high
start_time = injection_parameters['geocent_time'] - (duration - 2)
# start_time = injection_parameters['geocent_time'] - tc_3p5PN

meta_params = dict(
    minimum_frequency = minimum_frequency,
    tc_3p5PN = tc_3p5PN,
    f_high = f_high,
    duration = duration,
    sampling_frequency = sampling_frequency
)

injection_parameters['meta'] = meta_params
#-------------------------------#
# INJECTION PARAMETERS SETTINGS #
#################################


################
# PSD SETTINGS #
#--------------#
interferometers = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
for interferometer in interferometers:
    interferometer.minimum_frequency = minimum_frequency

# Change LIGO psd files if wanted:
path = bilby.__path__[0] + '/gw/noise_curves/'
new_LIGO_psd_file_name = 'aLIGO_early_high_psd.txt'
# new_LIGO_psd_file_name = 'aLIGO_mid_psd.txt'
# new_LIGO_psd_file_name = 'aLIGO_ZERO_DET_high_P_psd.txt'
interferometers[0].power_spectral_density.psd_file = path + new_LIGO_psd_file_name
interferometers[1].power_spectral_density.psd_file = path + new_LIGO_psd_file_name

interferometers.set_strain_data_from_power_spectral_densities(
    sampling_frequency=sampling_frequency,
    duration=duration,
    start_time=start_time)
#--------------#
# PSD SETTINGS #
################


#########################
# COMPUTING DERIVATIVES #
#-----------------------#
dh_Wave_1_ana, dh_Wave_2_ana, dh_Wave_3_ana = analytical_derivative(injection_parameters, start_time, minimum_frequency, interferometers)

# Removing indices/frequencies for which the waveform is zero since it's not of interest for the script
indices = np.where(dh_Wave_1_ana!=0)[1]
dh_Wave_1_ana = dh_Wave_1_ana[:, indices]
dh_Wave_2_ana = dh_Wave_2_ana[:, indices]
dh_Wave_3_ana = dh_Wave_3_ana[:, indices]

dh_Wave_ana = [dh_Wave_1_ana, dh_Wave_2_ana, dh_Wave_3_ana]

# offsets = np.array([1e-8, 1e-7, 1e-6, 1e-5, 5e-5, 1e-4, 5e-4, 1e-3])
offsets = np.logspace(-8, -4, 9)
overlaps = np.zeros((len(dh_Wave_ana), 3, len(offsets))) # 2 is to store the err for phic and log(d)

for i, offset in enumerate(offsets):
    dh_Wave_1_num, dh_Wave_2_num, dh_Wave_3_num = numerical_derivative(injection_parameters, start_time, minimum_frequency, interferometers, offset)

    # Removing indices/frequencies for which the waveform is zero since it's not of interest for the script
    dh_Wave_1_num = dh_Wave_1_num[:, indices]
    dh_Wave_2_num = dh_Wave_2_num[:, indices]
    dh_Wave_3_num = dh_Wave_3_num[:, indices]

    dh_Wave_num = [dh_Wave_1_num, dh_Wave_2_num, dh_Wave_3_num]

    for k in range(len(interferometers)):
        for l in range(3):
            overlaps[k][l][i] = overlap(dh_Wave_num[k][l], dh_Wave_ana[k][l])

    # import IPython; IPython.embed();
    # sys.exit()

# import IPython; IPython.embed();
for ifo_index in range(len(interferometers)):
    print_overlaps(offsets, overlaps, interferometers, ifo_index)
#-----------------------#
# COMPUTING DERIVATIVES #
#########################




#########
# PLOTS #
#-------#
plot_overlaps = False
plot_dh_w = True


param_names = [r"\phi_c", r"ln(D)", r"ln(t_c)"]
param_names_eq = ['$'+param_name+'$' for param_name in param_names]
ifo_colors = ['red', 'green', 'purple']

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)


plt.rc('text', usetex=False)
plt.rc('font', family='serif')
plt.rc('font', size=20)
plt.rc('font', weight='bold')


if plot_overlaps:
    plt.figure(figsize=(15,8))

    plt.subplot(311)
    for i, ifo in enumerate(interferometers):
        plt.semilogx(offsets, overlaps[i][0], linewidth=1, color=ifo_colors[i], label="{}".format(ifo.name))
    plt.xlabel("offset")
    plt.ylabel("overlap")
    plt.legend(loc='upper right')
    plt.title("Overlap between numerical and analytical dh/d"+param_names_eq[0])
    plt.tight_layout()

    plt.subplot(312)
    for i, ifo in enumerate(interferometers):
        plt.semilogx(offsets, overlaps[i][1], linewidth=1, color=ifo_colors[i], label="{}".format(ifo.name))
    plt.xlabel("offset")
    plt.ylabel("overlap")
    plt.legend(loc='upper right')
    plt.title("Overlap between numerical and analytical dh/d"+param_names_eq[1])
    plt.tight_layout()

    plt.subplot(313)
    for i, ifo in enumerate(interferometers):
        plt.semilogx(offsets, overlaps[i][2], linewidth=1, color=ifo_colors[i], label="{}".format(ifo.name))
    plt.xlabel("offset")
    plt.ylabel("overlap")
    plt.legend(loc='upper right')
    plt.title("Overlap between numerical and analytical dh/d"+param_names_eq[2])
    plt.tight_layout()

    plt.show()


# PLOT WAVEFORM DERIVATIVES

if plot_dh_w:
    # Select the interferometer on which you would like to make the comparison
    ifo_number = 1
    ifo_index = ifo_number - 1
    # Create frequency array suited for the plot
    freqs_for_plot = interferometers.frequency_array
    f_min = 30
    f_max = 3000
    f_min = max(f_min, minimum_frequency)
    f_max = min(f_max, freqs_for_plot[-1])
    indices = np.where((f_min<freqs_for_plot) & (freqs_for_plot<f_max))[0]
    freqs_for_plot = freqs_for_plot[indices]

    # Recompute analytical derivatives and slice arrays with the new wanted indices
    dh_Wave_1_ana, dh_Wave_2_ana, dh_Wave_3_ana = analytical_derivative(injection_parameters, start_time, minimum_frequency, interferometers)
    dh_Wave_1_ana = dh_Wave_1_ana[:, indices]
    dh_Wave_2_ana = dh_Wave_2_ana[:, indices]
    dh_Wave_3_ana = dh_Wave_3_ana[:, indices]
    dh_Wave_ana = [dh_Wave_1_ana, dh_Wave_2_ana, dh_Wave_3_ana]

    # Compute the numerical derivatives for a particular offset:
    offset = 1e-6
    dh_Wave_1_num, dh_Wave_2_num, dh_Wave_3_num = numerical_derivative(injection_parameters, start_time, minimum_frequency, interferometers, offset)
    dh_Wave_1_num = dh_Wave_1_num[:, indices]
    dh_Wave_2_num = dh_Wave_2_num[:, indices]
    dh_Wave_3_num = dh_Wave_3_num[:, indices]
    dh_Wave_num = [dh_Wave_1_num, dh_Wave_2_num, dh_Wave_3_num]


    for i in range(3):
        plt.figure(figsize=(15,8))

        plt.subplot(211)
        plt.plot(freqs_for_plot, dh_Wave_ana[ifo_index][i].real, linewidth=1, color='black', label="analytical")
        plt.plot(freqs_for_plot, dh_Wave_num[ifo_index][i].real, linewidth=1, color='red', label="numerical, offset={:.1e}".format(offset))
        plt.legend(loc='upper right')
        plt.title(r'dh_wave_{}['.format(ifo_number)+param_names_eq[i]+r'].real')
        plt.tight_layout()

        # plt.figure(figsize=(15,8))
        plt.subplot(212)
        plt.plot(freqs_for_plot, dh_Wave_ana[ifo_index][i].imag, linewidth=1, color='black', label="analytical")
        plt.plot(freqs_for_plot, dh_Wave_num[ifo_index][i].imag, linewidth=1, color='red', label="numerical, offset={:.1e}".format(offset))
        plt.legend(loc='upper right')
        plt.title(r'dh_wave_{}['.format(ifo_number)+param_names_eq[i]+r'].imag')
        plt.tight_layout()

    plt.show()


# import IPython; IPython.embed();
# sys.exit()
#-------#
# PLOTS #
#########



sys.exit()
