import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams["text.usetex"] = True
rcParams["font.serif"] = "Computer Modern Roman"
rcParams["font.family"] = "serif"
rcParams['text.latex.preamble'] = r'\providecommand{\mathdefault}[1][]{}'

import sys
sys.path.append('../..')
import Library.CONST as CONST



outdir_central = '../../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/1300_1300_2000_200_200_0.005/phase_marginalization/IMRPhenomD_NRTidal/sampler_state'
outdir_forward = '../../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/1300_1300_2000_200_200_0.005_forward/phase_marginalization/IMRPhenomD_NRTidal/sampler_state'

if False:
    # PLOT SUPER-IMPOSED WALKERS
    search_trajectory_parameters_keys = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'dec', 'ra', 'geocent_duration', 'chi_1', 'chi_2', 'lambda_1', 'lambda_2']
    latex_labels = CONST.get_latex_labels(search_trajectory_parameters_keys)

    samples_central = np.loadtxt(outdir_central + '/samples.dat')
    samples_forward = np.loadtxt(outdir_forward + '/samples.dat')

    samples_central = samples_central[:20]
    samples_forward = samples_forward[:20]

    ndim = len(search_trajectory_parameters_keys)
    nsteps = samples_central.shape[0]
    idxs = np.arange(nsteps)
    fig, axes = plt.subplots(nrows=ndim, figsize=(15, 3 * ndim))
    for i, ax in enumerate(axes):
        ax.plot(idxs, samples_central[:, i], lw=1, color='C0', label='central')
        ax.plot(idxs, samples_forward[:, i], lw=1, color='C1', label='forward')
        ax.set_ylabel(latex_labels[i])
        ax.legend(loc='upper right')

    fig.tight_layout()
    fig.savefig(f'walkers_central_vs_forward_{nsteps}.png')



if True:
    # PLOT 1ST TRAJECTORY
    qpos_traj_traj1_central = np.loadtxt(outdir_central + '/qpos_traj_phase1.dat')[:200]
    qpos_traj_traj1_forward = np.loadtxt(outdir_forward + '/qpos_traj_phase1.dat')[:200]

    search_trajectory_parameters_keys = ['cos_theta_jn', 'psi', 'log_luminosity_distance', 'log_chirp_mass', 'log_reduced_mass', 'sin_dec', 'ra', 'log_geocent_duration', 'chi_1', 'chi_2', 'lambda_1', 'lambda_2']
    latex_labels = CONST.get_latex_labels(search_trajectory_parameters_keys)

    ndim = len(search_trajectory_parameters_keys)
    nsteps = qpos_traj_traj1_central.shape[0]
    idxs = np.arange(nsteps)
    fig, axes = plt.subplots(nrows=ndim, figsize=(15, 3 * ndim))
    for i, ax in enumerate(axes):
        ax.plot(idxs, qpos_traj_traj1_central[:, i], lw=1, color='C0', label='central')
        ax.plot(idxs, qpos_traj_traj1_forward[:, i], lw=1, color='C1', label='forward')
        ax.set_ylabel(latex_labels[i])
        ax.legend(loc='upper right')

    fig.tight_layout()
    fig.savefig(f'first_trajpath_central_vs_forward.png')
