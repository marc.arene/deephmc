import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
# import subprocess
# import pandas as pd

import Library.python_utils as pu
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.plots as plots

import Codes.set_injection_parameters as set_inj

# import core.sampler
import core.utils as cut

def get_dict_trajectory_parameters_keys(search_parameter_keys, dict_trajectory_parameters_keys):
    """
    Fills the gaps of the inputed dictionary: dict_trajectory_parameters_keys, associating a search_parameter to its 'trajectory-parameter-name'. Indeed when a search_parameter is its own trajectory-parameter we allow the user to say 'none' or not mention it.
    """
    dict_traj_param_keys_full = dict_trajectory_parameters_keys.copy()
    for key in search_parameter_keys:
        if key not in dict_trajectory_parameters_keys.keys() or dict_trajectory_parameters_keys[key].lower() == 'none':
            dict_traj_param_keys_full[key] = key
    return dict_traj_param_keys_full

def get_trajectory_parameters_keys(search_parameter_keys, dict_trajectory_parameters_keys):
    search_trajectory_parameters_keys = []
    for key in search_parameter_keys:
        traj_param_key = dict_trajectory_parameters_keys[key]
        if traj_param_key == 'None':
            traj_param_key = key
        search_trajectory_parameters_keys.append(traj_param_key)
    return search_trajectory_parameters_keys

def q_pos_traj_to_parameters_traj_dict(search_trajectory_parameters_keys, q_pos_traj):
    parameters_traj = {}
    for i, traj_key in enumerate(search_trajectory_parameters_keys):
        parameters_traj[traj_key] = q_pos_traj[i]
    return parameters_traj

if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--rundir", default='../__output_data/GW170817inj/SUB-D_0#2345678/PSD_3/100000_1500_2000_200_200_0.005/phase_marginalization/IMRPhenomPv2_ROQ', action="store", type="string", help="""Directory were the run configuration files and data were stored.""")
    parser.add_option("--outdir", default=None, action="store", type="string", help="""Directory to store the images for the movie. By default will create a rundir/plots/png_for_movie directory. """)

    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()

    # SET THE STATE OF THE SAMPLER TO WHAT IT WAS SAVED OR INITIALIZE IT IF THIS IS THE FIRST TIME THESE SETTINGS ARE RUN
    # rundir = '../__output_data/GW170817inj/SUB-D_0#2345678/PSD_3/100000_1500_2000_200_200_0.005/phase_marginalization/IMRPhenomPv2_ROQ'
    # run_dir = '../__output_data/GW170817inj/SUB-D_0#2345678/PSD_3/1000000_1500_2000_200_200_0.005_mass_scales_high/phase_marginalization/IMRPhenomPv2_ROQ/sampler_state/'
    # rundir = '../__output_data/GW170817inj/SUB-D_0#2345678/PSD_3/1000000_1500_2000_200_200_0.005_scales_from_phase1_samples/phase_marginalization/IMRPhenomPv2_ROQ/sampler_state/'
    # rundir = '../__output_data/GW170817inj/SUB-D_0#2345678/PSD_3/1000000_1500_2000_200_200_0.005_mass_scales_smallish/phase_marginalization/IMRPhenomPv2_ROQ/sampler_state/'
    rundir = opts.rundir
    statedir = rundir + '/sampler_state'

    config_file = rundir + '/config.ini'
    inj_file = rundir + '/injection.ini'
    hmc_params_file = rundir + '/config_hmc_parameters.json'

    config_parser = ConfigParser()
    config_parser.read(config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)
    # cut.logger.info(f'Options from the configuration file {config_file} were: {config_dict_formatted}')

    # Parse the inputs relative to the sampler and the search
    sampler_dict = pu.json_to_dict(hmc_params_file)
    sampler_dict_formatted = cut.dictionary_to_formatted_string(sampler_dict)
    cut.logger.info(f'Options for the sampler are: {sampler_dict_formatted}')


    # Set up a random seed for result reproducibility.  This is optional!
    np.random.seed(88170235)

    # SET THE OUTPUT DIRECTORY WHERE OUTPUT FILES WILL BE SAVED
    if opts.outdir is None:
        outdir = rundir + '/plots/png_for_movie'
    else:
        outdir = opts.outdir

    cut.setup_logger(cut.logger, outdir=outdir, label='logger', log_level='INFO')
    cut.logger.info(f'Output directory is: {outdir}')

    # SET THE INJECTION PARAMETERS AND THE ANALYSIS PARAMETERS
    injection_parameters, ext_analysis_dict = set_inj.get_inj_parameters_and_analysis_dict(inj_file, **config_dict['analysis'])

    search_parameter_keys = sampler_dict['search_parameter_keys']
    dict_trajectory_parameters_keys = get_dict_trajectory_parameters_keys(search_parameter_keys, sampler_dict['trajectory_parameters_keys'])
    search_trajectory_parameters_keys = get_trajectory_parameters_keys(search_parameter_keys, dict_trajectory_parameters_keys)

    qpos_global_fit = np.loadtxt(statedir + '/phase1/qpos_global_fit.dat')
    end_run_path = '../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16/PSD_3/150000_800_2000_200_200_0.005/phase_marginalization/IMRPhenomD/sampler_state'
    trajquants68 = np.loadtxt(end_run_path + '/trajquants68.dat')
    trajquants90 = np.loadtxt(end_run_path + '/trajquants90.dat')

    traj_params_keys_to_plot = ['cos_theta_jn', 'psi']
    # traj_params_keys_to_plot = search_trajectory_parameters_keys
    traj_params_idx_to_plot = []
    for k in traj_params_keys_to_plot:
        traj_params_idx_to_plot.append(search_trajectory_parameters_keys.index(k))
    traj_param_idx_str = ''.join([str(i) for i in traj_params_idx_to_plot])
    # if True: import IPython; IPython.embed(); sys.exit()
    for starting_traj in range(12, 13):
        # starting_traj = 3
        idx_start = starting_traj * config_dict['hmc']['n_fit_2']
        nb_of_traj_for_movie = 1
        idx_final = idx_start + nb_of_traj_for_movie * config_dict['hmc']['n_fit_2']

        qpos_traj = qpos_global_fit[idx_start:idx_final, :]

        nb_of_images_per_traj = 40
        # nb_of_images_per_traj = 20
        nb_of_images = nb_of_traj_for_movie * nb_of_images_per_traj
        n_step_in_traj = int(config_dict['hmc']['n_fit_2'] / nb_of_images_per_traj)

        ranges = []
        if False: # set ranges from the 90% quantiles of visited pts in phase1
            quantiles=[16, 84]
            quants = np.percentile(qpos_global_fit, quantiles, axis=0)
            ranges = quants.T
        elif True:
            ranges = trajquants68[:, traj_params_idx_to_plot].T
        else: # set ranges from the min and max in the traj we are plotting
            for idx in range(qpos_traj.shape[1]):
                min = qpos_traj[:, idx].min()
                max = qpos_traj[:, idx].max()
                range_real = max - min
                min -= 0.1 * range_real
                max += 0.1 * range_real
                ranges.append((min, max))

        hist2d_kwargs = {'plot_datapoints': True, 'label_kwargs': dict(fontsize=18)}

        for idx_image in range(nb_of_images):
            idx_qpos_end = (idx_image + 1) * n_step_in_traj - 1
            qpos_traj_sub = qpos_traj[:idx_qpos_end + 1, :]
            # # Set the truth value to be plotted to the starting point of the trajectory
            # idx_qpos_traj_start = int(idx_qpos_end/ config_dict['hmc']['n_fit_2']) * config_dict['hmc']['n_fit_2']
            # qpos_traj_start = qpos_traj[idx_qpos_traj_start, :]
            # truth_parameters = sampler.q_pos_traj_to_parameters_traj_dict(qpos_traj_start)
            truth_parameters = q_pos_traj_to_parameters_traj_dict(search_trajectory_parameters_keys, qpos_traj_sub[-1])
            # if starting_traj not in [4,5]:
            # breakpoint()
            try:
                # kwargs = hist2d_kwargs.copy()
                # kwargs['truth'] = qpos_traj_sub[-1, traj_params_idx_to_plot]
                # plots.plot_corner_simple(qpos_traj_sub[:, traj_params_idx_to_plot], path=outdir + f'/traj{starting_traj}_{idx_image+1:02}', **kwargs)
                plots.plot_walkers_and_corner_from_samples(qpos_traj_sub[:, traj_params_idx_to_plot], truth_parameters, traj_params_keys_to_plot, save=True, label=f'traj{starting_traj}_{traj_param_idx_str}_{idx_image+1:02}', outdir=outdir, range=np.asarray(ranges)[traj_params_idx_to_plot], **hist2d_kwargs)
            except ValueError as e:
                cut.logger.info(f'Could not plot corner since ValueError was raised: {e}')

            # plots.plot_walkers_and_corner_from_samples(qpos_traj_sub, truth_parameters, sampler.search_trajectory_parameters_keys, save=True, label=f'traj_000', outdir=sampler.outdir, range=ranges, **hist2d_kwargs)

    if False: import IPython; IPython.embed();
    sys.exit()
    # command to create the movie:
    # https://trac.ffmpeg.org/wiki/Slideshow
    # $ ffmpeg -start_number 0 -f image2 -r 5 -i traj_%02d_corner.png -vcodec mpeg4 -y movie.mp4
    # $ ffmpeg -start_number 0 -f image2 -r 5 -i traj12_01_%02d_corner.png -vcodec mpeg4 -y movie.mp4
    # ' '.join([f'traj_{n}_corner.png' for n in range(21, 41)])

    import matplotlib.pyplot as plt
    import Library.CONST as CONST
    plt.figure(figsize=(15,10))
    for key in dict_integrated_times_estimates.keys():
        traj_key = sampler.likelihood_gradient.dict_trajectory_parameters_keys[key]
        latex_label = CONST.LATEX_LABELS[key] + rf', $\sigma =$ {sampler.scales_dict[traj_key]:.1e}'
        plt.plot(nb_of_samples_to_integrate_on, dict_integrated_times_estimates[key], label=latex_label)
    plt.xlabel(r'number of samples, N')
    plt.ylabel(r'$L_{estimated}$')
    plt.title(r'Convergence of $L$, discarding phase1 samples')
    plt.legend()
    plt.xscale('log')
    plt.savefig(outdir + 'L_convergence.png')
    plt.show()

    if True: import IPython; IPython.embed(); sys.exit()
