# This script is meant to shed the light on a steppy behavior of the logL as a function of declination and right-ascension at small scales.
# Running this script with `--stepsize=1e-6` shows this.
# Note that this behavior does not appear when varying the other parameters.

import numpy as np
import matplotlib.pyplot as plt
import bilby
from bilby.core import utils

import sys
sys.path.append('../')
import Library.python_utils as pu
import Library.param_utils as paru
import Library.bilby_waveform as bilby_wv

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds



SOL = 2.99792458e8                                       # Speed of light / m/s
MSOLAR = 1.9884e30                                       # Mass of the sun / kg
G = 6.67428e-11                                          # Newtons Constant
GEOM = (G * MSOLAR / (SOL*SOL*SOL))                      # mass to seconds conversion


def Compute_fISCO(Rmin, m1, m2):
    """
    Compute the frequency at the Innermost Stable Circular Orbit
    """

    vISCO = np.sqrt(1.0 / Rmin)
    Mt = m1 + m2
    fmax = vISCO**3 / (np.pi * GEOM * Mt)      # fmax signal
    return fmax

def set_parameters():
    """
    Set the parameters for the run
    """
    # Values found for GW170817
    # injection_parameters = dict(
    #     mass_1=1.47,
    #     mass_2=1.27,
    #     chi_1=0.0,
    #     chi_2=0.0,
    #     luminosity_distance=40,
    #     # iota=2.81,
    #     theta_jn=2.81,
    #     psi=2.21,
    #     phase=5.497787143782138,
    #     geocent_time=1187008882.43,
    #     ra=3.44616, # value from fixed skyposition run
    #     dec=-0.408084, # value from fixed skyposition run
    #     lambda_1=0.,
    #     lambda_2=0.
    #     )


    injection_parameters = pu.json_to_dict('../Codes/parameters_dlogL_dlnmu_investigation.json')

    injection_parameters = bilby.gw.conversion.generate_mass_parameters(injection_parameters)
    injection_parameters['reduced_mass'] = (injection_parameters['mass_1'] * injection_parameters['mass_2']) / (injection_parameters['mass_1'] + injection_parameters['mass_2'])

    m1 = injection_parameters['mass_1']
    m2 = injection_parameters['mass_2']

    # I would normally compute the chirp time associated to these parameters with a function involving  PN coefficients, but since I want this code to be standalone I will hard code the chirp time for those GW170817 parameters:
    # tc_3p5PN = ComputeChirpTime3p5PN(minimum_frequency, m1, m2)
    tc_3p5PN = 56.91538269948342

    # We integrate the signal up to the frequency of the "Innermost stable circular orbit (ISCO)"
    R_isco = 6.      # Orbital separation at ISCO, in geometric units. 6M for PN ISCO; 2.8M for EOB

    fISCO = Compute_fISCO(R_isco, m1, m2)

    duration = tc_3p5PN + 2
    sampling_frequency = 2 * fISCO
    start_time = injection_parameters['geocent_time'] - tc_3p5PN

    return injection_parameters, start_time, sampling_frequency, duration

def main(parameters, interferometers, waveform_generator, nb_points, stepsize, pkey):
    """
    Computes the reduced log-likelihood varying parameter[pkey] around the injected value.

    Parameters:
    -----------
    parameters: dict
        Parameters to evaluate the waveform for.
    interferometers:
        List of interferometers to run the analysis on.
    waveform_generator:
        bilby's object needed to generate the source frame polarizations
    nb_points: int
        number of points to compute the logL on, around the injected value.
    stepsize: float
        stepsize between computed points.
    pkey: string
        key of the dictionary `parameters` relative to the parameter we want to vary. This script is meant to work with `key = dec` or `key = ra`.

    Returns:
    --------
    param_linspace: list
        list of parameter values values on which logL has been computed
    logL_ifos_linspace: array of shape (nb_points, len(interferometers))
        Contains the values of logL for each ifo for each points in param_linspace
    """

    param_inj = parameters[pkey]

    func = np.log
    func_inv = np.exp

    fparam_inj = func(param_inj)
    fparam_max = func(param_inj) + nb_points/2 * stepsize
    fparam_min = func(param_inj) - nb_points/2 * stepsize


    fparam_linspace = np.linspace(fparam_min, fparam_max, nb_points)

    logL_ifos_linspace = []
    time_shifts_ifos = []
    mass_1_list = []

    for fparam in fparam_linspace:
        # update the parameter dictionary with the new value of sin(dec)
        parameters.update({pkey: func_inv(fparam)})
        if pkey in ['chirp_mass', 'reduced_mass']:
            parameters['total_mass'], parameters['symmetric_mass_ratio'], parameters['mass_1'], parameters['mass_2'] = paru.Mc_and_mu_to_M_eta_m1_and_m2(parameters['chirp_mass'], parameters['reduced_mass'])
            mass_1_list.append(parameters['mass_1']* utils.solar_mass)
        # pu.print_dict(parameters)
        # Compute the template in the source frame
        # print("_______________")
        # print('{:20} = {}'.format('mass_1 in hmc', parameters['mass_1']))
        source_frame_polarizations = waveform_generator.frequency_domain_strain(parameters)
        # breakpoint()
        time_shift_ifos = []
        logL_ifos = []
        for i, ifo in enumerate(interferometers):
            # Project the source frame waveform in detector frame
            # h = ifo.get_detector_response(source_frame_polarizations, parameters)
            h = bilby_wv.WaveForm_ThreeDetectors(parameters, minimum_frequency, interferometers, source_frame_polarizations=source_frame_polarizations)

            # Compute <s|h>
            sh = bilby.gw.utils.noise_weighted_inner_product(
                aa=h,
                bb=ifo.frequency_domain_strain,
                power_spectral_density=ifo.power_spectral_density_array,
                duration= ifo.strain_data.duration).real
            # Compute <h|h>
            hh = bilby.gw.utils.noise_weighted_inner_product(
                aa=h,
                bb=h,
                power_spectral_density=ifo.power_spectral_density_array,
                duration=ifo.strain_data.duration).real

            hsource_hsource = bilby.gw.utils.noise_weighted_inner_product(
                aa=source_frame_polarizations['plus'],
                bb=source_frame_polarizations['plus'],
                power_spectral_density=ifo.power_spectral_density_array,
                duration=ifo.strain_data.duration).real

            # Actually only the <s|h> part of logL has the steppy behavior
            # logL_ifo = source_frame_polarizations['plus'].real.sum()
            # logL_ifo = hsource_hsource
            logL_ifo = sh - 0.5 * hh

            time_shift_ifo = ifo.time_delay_from_geocenter(parameters['ra'], parameters['dec'], parameters['geocent_time'])

            logL_ifos.append(logL_ifo)
            time_shift_ifos.append(time_shift_ifo)
        # breakpoint()
        logL_ifos_linspace.append(logL_ifos)
        time_shifts_ifos.append(time_shift_ifos)

    return fparam_linspace, logL_ifos_linspace, fparam_inj, time_shifts_ifos, mass_1_list


if __name__=='__main__':

    from optparse import OptionParser
    usage = """%prog [options]
    Plots the logL as a function of the parameter defined by `pkey`, keeping the other constant."""
    parser=OptionParser(usage)
    parser.add_option("--stepsize", default=1e-7, action="store", type="float", help="""Stepsize used between plotted points around the injeceted value.""")
    parser.add_option("-n", "--nb_points", default=200, action="store", type="int", help="""Number of points to plot. Default is 200""")
    parser.add_option("--minimum_frequency", default=30.0, action="store", type="float", help=""" Low frequency cut-off""")
    parser.add_option("--pkey", default='reduced_mass', action="store", help="""Key of the parameter in the dictionary. Can either be 'dec' or 'ra'. Default is 'dec'. """)

    (opts,args)=parser.parse_args()

    minimum_frequency = opts.minimum_frequency

    np.random.seed(88170235)

    # SET THE INJECTION PARAMETERS
    # injection_parameters, start_time, sampling_frequency, duration = set_parameters()
    # injection_parameters = pu.json_to_dict('../__output_data/parameters_dlogL_dlnmu_investigation.json')
    injection_parameters = set_inj.ini_file_to_dict('../examples/GW170817.ini')
    # pu.save_dict_to_json(injection_parameters, outdir + 'injection_parameters.json')
    # print("\nInjection parameters dictionary:")
    # pu.print_dict(injection_parameters)
    minimum_frequency = injection_parameters['meta']['minimum_frequency']
    duration = injection_parameters['meta']['duration']
    sampling_frequency = injection_parameters['meta']['sampling_frequency']
    f_high = injection_parameters['meta']['f_high']
    start_time = injection_parameters['meta']['start_time']


    # INITIALIZE THE THREE INTERFEROMETERS
    ifos_list = ['H1']
    # ifos_list = ['H1', 'L1', 'V1']
    interferometers = bilby.gw.detector.InterferometerList(ifos_list)
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time

    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=1, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)


    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=start_time)

    # CREATE THE WAVEFORM GENERATOR NEEDED FOR BILBY
    # Fixed arguments passed into the source model. The analysis starts at 40 Hz.
    approximant = 'TaylorF2'
    # approximant = 'SpinTaylorF2'
    # approximant = 'IMRPhenomPv2'
    waveform_arguments = dict(waveform_approximant=approximant, reference_frequency=0, minimum_frequency=minimum_frequency, maximum_frequency=0)

    # Create the waveform_generator using a LAL Binary Neutron Star source function
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=interferometers.duration,
        sampling_frequency=interferometers.sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
        waveform_arguments=waveform_arguments
        )

    # INJECTION THE GW170817 LIKE SIGNAL
    # interferometers.inject_signal(parameters=injection_parameters, waveform_generator=waveform_generator)
    source_frame_polarizations = waveform_generator.frequency_domain_strain(injection_parameters)
    for ifo in interferometers:
        signal_ifo = ifo.get_detector_response(source_frame_polarizations, injection_parameters)
        ifo.strain_data.frequency_domain_strain += signal_ifo
        # ifo.set_strain_data_from_frequency_domain_strain(
        #         signal_ifo,
        #         sampling_frequency=ifo.strain_data.sampling_frequency,
        #         duration=ifo.strain_data.duration,
        #         start_time=ifo.strain_data.start_time)


    #
    # # WAVEFORM GENERATION
    # template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)
    #
    # # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # # Add the template signal to each detector's strain data
    # for i in range(len(interferometers)):
    #     interferometers[i].strain_data.frequency_domain_strain += template_ifos[i]
    #     interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain




    # COMPUTE THE LOGL AROUND THE INJECTED VALUE OF SIN(DEC)
    param_linspace, logL_ifos_linspace, param_inj, time_shifts_ifos, mass_1_list =  main(injection_parameters, interferometers, waveform_generator, opts.nb_points, opts.stepsize, opts.pkey)

    logL_ifos_linspace = np.array(logL_ifos_linspace)
    time_shifts_ifos = np.array(time_shifts_ifos)

    dt_ifos_steppy = injection_parameters['geocent_time'] + time_shifts_ifos - start_time

    # tc = injection_parameters['geocent_time'] - start_time
    # dt_ifos_smooth = tc + time_shifts_ifos
    dt_ifos_smooth = injection_parameters['geocent_time'] - start_time + time_shifts_ifos




    # PLOT
    from matplotlib import rc

    plt.rcParams['text.usetex'] = False

    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'

    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'

    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10


    fig= plt.figure(figsize=(16,8))
    ifo_colors = ['red', 'green', 'purple']
    ax0 = plt.subplot(111)
    # fig, axs = plt.subplots(2, 1, figsize=(16,8), gridspec_kw = {'height_ratios': [10, 1]})

    # fig.suptitle('logL and dlogL computed with a step size of {:.0e}'.format(opts.stepsize))
    ax0.plot(param_linspace, logL_ifos_linspace.sum(axis=1), linewidth=1, marker='o', markersize=3, label='logL_{}.sum'.format(''.join(ifos_list)))
    # ax0.plot(param_linspace, mass_1_list, linewidth=1, marker='o', markersize=3, label='mass_1')
    ax0.axvline(x=param_inj, linewidth=0.5, color='black', label='injected value')
    xlabel = 'ln(mu)'
    # xlabel = opts.pkey
    ax0.set_xlabel(xlabel)
    ax0.set_ylabel('logL')
    ax0.set_title('logL. Step size between plotted points =  {:.0e}'.format(opts.stepsize))
    ax0.legend(loc=(0.85,0.7))

    # for i, ifo in enumerate(interferometers):
    #     ax0.plot(param_linspace, dt_ifos_steppy[:, i], linewidth=1, marker='o', markersize=3, label='dt_{} = parameters[\'geocent_time\'] + time_shift - start_time'.format(ifo.name).format(ifo.name))
    #     ax0.plot(param_linspace, dt_ifos_smooth[:, i], linewidth=1, marker='o', markersize=3, label='dt_{} = parameters[\'geocent_time\'] - start_time + time_shift'.format(ifo.name).format(ifo.name))
    #     ax0.axvline(x=param_inj, linewidth=0.5, color='black', label='injected value')
    #     xlabel = opts.pkey
    #     ax0.set_xlabel(xlabel)
    #     ax0.set_ylabel('dt')
    #     ax0.set_title('Two different results for dt')
    #     ax0.legend()
        # ax0.legend(loc=(0.85,0.7))

    # UNCOMMENT THIS SECTION TO PLOT THE 3 LOGL INDEPENDENTLY
    # for i, ifo in enumerate(interferometers):
    #     ax = ax0.twinx()
    #     ax.plot(param_linspace, logL_ifos_linspace[:, i], linewidth=0.5, label='logL_{}'.format(ifo.name), color=ifo_colors[i])
    #     # ax00.set_ylabel('logL_{}'.format(ifo.name))
    #     ax.tick_params('y', colors=ifo_colors[i])
    #     y_legend = 0.7 - (i+1)*0.05
    #     ax.legend(loc=(0.85,y_legend))

    # Plotting the timeshifts of each ifo independently
    # ifo_colors = ['red', 'green', 'purple']
    # for i, ifo in enumerate(interferometers):
    #     ax = ax0.twinx()
    #     ax.plot(param_linspace, dt_ifos_smooth[:, i], linewidth=0.5, marker='x', markersize=1, label='dt_{} = '.format(ifo.name), color=ifo_colors[i])
    #     ax.set_ylabel('time_shift_{}'.format(ifo.name), color=ifo_colors[i])
    #     ax.tick_params('y', colors=ifo_colors[i])
    #     y_legend = 0.9 - (i+1)*0.05
    #     ax.legend(loc=(0.70,y_legend))

    fig.tight_layout()

    file_name = './.logL_vs_param_standalone/logL_vs_{}_standalone_{}.png'.format(opts.pkey, opts.stepsize)
    # fig.savefig(file_name)

    plt.show()
