import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
sys.path.append('../../')

import numpy as np
import matplotlib.pyplot as plt
import Library.plots as plots
import Library.CONST as CONST



input_dir = './.data/'
lengths = ['20', '50', '100', '150', '_uniform_100']
search_parameter_keys = ['theta_jn', 'psi', 'luminosity_distance', 'chirp_mass', 'reduced_mass', 'dec', 'ra', 'geocent_time', 'chi_1', 'chi_2']
search_parameter_labels = []
for key in search_parameter_keys:
    search_parameter_labels.append(CONST.LATEX_LABELS[key])
ndim = len(search_parameter_keys)
start_time = 1187008826.227964
# ranges taken from the 80 000 traj run:
# /GW170817real/0_2_3_4_5_6_7_8_15_16/PSD_3/100000_800_2000_200_200_0.005/phase_marginalization/IMRPhenomD/sampler_state/samples.dat
ranges = [[1.7884451177476561, 3.140119511498079], [5.157776513321571e-06, 3.1414539168222557], [9.90953509834938, 51.58333067695794], [1.1972488310467464, 1.1979142880523075], [0.6627971115456294, 0.6878464894229931], [-0.4774958477838727, -0.22687592162777812], [3.3220810159464693, 3.491773618791785], [1187008882.4289122, 1187008882.4323082], [-0.04989132530176791, 0.04999782532609715], [-0.049991789771876266, 0.049999681581933464]]
quants_68pct = [[1.9768224037517694,2.6350269251869323], [0.25003161851177275,2.661576418120939], [18.638554344942083,39.90797619131743], [1.1974903759085709,1.197683075600725], [0.6701411137080957,0.6825839437757302], [-0.39199379466468215,-0.318098127563762], [3.3915010684732,3.437268202798471], [1187008882.4299474,1187008882.4308753], [0.0022426200194746487,0.04453527594588478], [-0.01365548759559741,0.041385378495445296]]


if __name__ == '__main__':
    samples_phase3_all_legnths = []
    for len in lengths:
        samples_phase3 = np.loadtxt('./.data/samples_len' + len + '.dat')
        samples_phase3 = samples_phase3[800:, :]
        geocent_time_index = search_parameter_keys.index('geocent_time')
        samples_phase3[:, geocent_time_index] += start_time
        samples_phase3_all_legnths.append(samples_phase3)


    fig, axes = plt.subplots(nrows=ndim, figsize=(15, 3 * ndim))
    idxs = np.arange(100)
    for i, ax in enumerate(axes):
        for j, len in enumerate(lengths):
            ax.plot(idxs, samples_phase3_all_legnths[j][:, i], lw=1, label='traj length = ' + len)
        ax.axhline(y=ranges[i][0], label='min/max value of full run', color= 'black', linestyle='-', linewidth=1)
        ax.axhline(y=ranges[i][1], color= 'black', linestyle='-', linewidth=1)
        ax.axhline(y=quants_68pct[i][0], label='68% quantiles of full run', color= 'grey', linestyle='--', linewidth=2)
        ax.axhline(y=quants_68pct[i][1], color= 'grey', linestyle='--', linewidth=2)
        ax.set_ylabel(search_parameter_labels[i])
        if i == 0: ax.legend()

    # import IPython; IPython.embed();

    fig.suptitle('Trajectory length influence on walk behaviour, looking at 100 phase3 purely analytical trajectories')
    fig.text(x=0.4, y=0.95, s='Acceptances: 20: 90%, 50: 85%, 100: 77%, 150: 72%')
    fig.tight_layout()
    fig.subplots_adjust(top=0.92) # Needed so that fig.title and ax.title don't overlap
    fig.savefig('./.plots/phase3_len_compare_full_2.png')
    plt.close(fig)
