import sys
sys.path.append('../')
import numpy as np
import Library.Fit_NR as fnr
import Library.plots as plots
import Library.CONST as CONST


if __name__ == '__main__':

    global_fit_dir = '../__output_data/GW190412real/0_1_2_3_4_5_6_7_8_15_16/PSD_2/501500_1500_2000_200_200_0.0123/IMRPhenomHM/sampler_state/global_fit'

    train_size = 286200
    train_size = 504297
    train_size_max = 543185
    input_dir = global_fit_dir + f'/train_size_{train_size}'
    # input_dir = global_fit_dir + '/train_size_286200'
    # input_dir = global_fit_dir + '/train_size_424623'
    # input_dir = global_fit_dir + '/train_size_504297'
    # input_dir = global_fit_dir + '/train_size_543185'
    outdir_plots = input_dir + '/plots'


    x_train = np.loadtxt(input_dir + '/qpos_trained.dat')
    y_train = np.loadtxt(input_dir + '/dlogL_trained.dat')

    if train_size == train_size_max:
    # if True:
        x_val = None
        y_val = None
        validation_data = None
    else:
        x_val = np.loadtxt(global_fit_dir + f'/train_size_{train_size_max}/qpos_trained.dat')
        y_val = np.loadtxt(global_fit_dir + f'/train_size_{train_size_max}/dlogL_trained.dat')
        val_size = train_size_max - 504297
        x_val = x_val[-val_size:]
        y_val = y_val[-val_size:]
        validation_data = (x_val, y_val)


    epochs = 60
    batch_size = 128
    fit_method = fnr.DNNRegressorMultipleGradients(
        n_dim=x_train.shape[1],
        batch_size=batch_size,
        epochs=epochs
    )

    print(f'Loading the {fit_method.nick_name} fit from saved directory {input_dir}...')
    fit_method.load(input_dir)

    search_trajectory_parameters_keys = ['cos_theta_jn', 'phase', 'psi', 'log_luminosity_distance', 'log_chirp_mass', 'log_reduced_mass', 'sin_dec', 'ra', 'log_geocent_duration', 'chi_1', 'chi_2']
    dlogL_latex_labels = CONST.get_dlogL_latex_labels(search_trajectory_parameters_keys)
    plots.make_regression_plots(fit_method, x_train, y_train, outdir_plots, validation_data=validation_data, y_labels=dlogL_latex_labels)
