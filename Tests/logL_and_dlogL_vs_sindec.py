# This script is meant to compare the influence of the offset used in the numerical derivative on trajectories that have revealed some instability.

import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../')
import bilby
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.CONST as CONST
# import Codes.set_injection_parameters as set_inj

import GW170817_parameters
import GW170817_set_psds
import GW170817_waveform_three_detectors
import GW170817_logL_snr
import GW170817_set_strain_data_from_psd


def main(parameters, interferometers, template_ifos, start_time, minimum_frequency):
    nb_points = 100
    offset = sampler_dict['parameter_offsets'][6]

    # Values taken from the trajectory with offset = 1e-3
    # Cf file `pt_debug.dat` here: /Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/newPythonCode/__output_data/GW170817/bilbyoriented/SUB-D_######6##/logL920.44/0e+00_1_2000_200_200_0.005_dlogL2_1e-03
    # sin_dec_max = -3.968513939046793837e-01
    # # sin_dec_min = -4.008370449495952670e-01
    # sin_dec_min = sin_dec_max - nb_points * offset

    sin_dec_inj = np.sin(parameters['dec'])
    sin_dec_max = sin_dec_inj + nb_points/2 * offset
    sin_dec_min = sin_dec_inj - nb_points/2 * offset


    sin_dec_list = np.linspace(sin_dec_min, sin_dec_max, nb_points)
    logL_list = []
    dlogL_list = []


    for sin_dec in sin_dec_list:
        parameters.update({'dec': np.arcsin(sin_dec)})

        template_ifos = bilby_wv.WaveForm_ThreeDetectors(parameters, minimum_frequency, interferometers)
        logL = bilby_utils.loglikelihood(template_ifos, interferometers)
        dlogL, logL = bilby_wv.dlogL_ThreeDetectors(template_ifos, parameters, start_time, minimum_frequency, interferometers)

        logL_list.append(logL)
        dlogL_list.append(dlogL[6])

    return sin_dec_list, logL_list, dlogL_list, sin_dec_inj


if __name__=='__main__':
    from optparse import OptionParser
    usage = """%prog [options]
    Plots the three waveforms in the time domain"""
    parser=OptionParser(usage)
    parser.add_option("--psd", default=1, action="store", type="int", help="""1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.""", metavar="INT from {1,2,3}")
    parser.add_option("--hmc", default=True, action="store_false", help="""Waveforms are generated using Ed and Yann's TF2 code, default is to generate them using bilby.lalsim""", dest='generate_from_bilby')
    # parser.add_option("--offset", default=None,action="store", type="float", help="""offset used to compute dlogL but also stepsize between plotted points around the injeceted value.""")
    parser.add_option("--new", default=False, action="store_true", help="""New noise realisation from PSDs is generated""", dest='new_noise_realisation_from_psd')
    (opts,args)=parser.parse_args()

    # import IPython; IPython.embed(); sys.exit(1)
    # SET THE INJECTION PARAMETERS
    injection_parameters = GW170817_parameters.main()
    minimum_frequency = injection_parameters['meta']['minimum_frequency']
    duration = injection_parameters['meta']['duration']
    sampling_frequency = injection_parameters['meta']['sampling_frequency']
    f_high = injection_parameters['meta']['f_high']
    start_time = injection_parameters['meta']['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    interferometers = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    GW170817_set_psds.main(interferometers, opt_psd=opts.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)

    # SET NOISE STRAIN DATA FROM PSD
    GW170817_set_strain_data_from_psd.main(interferometers, injection_parameters, sampling_frequency, duration, start_time, opts.psd, opts.new_noise_realisation_from_psd)

    # WAVEFORM GENERATION
    template_ifos = GW170817_waveform_three_detectors.main(injection_parameters, start_time, minimum_frequency, interferometers, opts.generate_from_bilby)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    for i in range(len(interferometers)):
        interferometers[i].strain_data.frequency_domain_strain += template_ifos[i]
        interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain

    # COMPUTE  AND PRINT SNR
    logLBest, mf_snr_best, opt_snr_Best = GW170817_logL_snr.main(template_ifos, interferometers, True)

    # This copy is here just because for the moment I will keep manipulating 3 separate variables as originally done in the hmc
    signal1 = interferometers[0].strain_data.frequency_domain_strain.copy()
    signal2 = interferometers[1].strain_data.frequency_domain_strain.copy()
    signal3 = interferometers[2].strain_data.frequency_domain_strain.copy()

    sin_dec_list, logL_list, dlogL_list, sin_dec_inj =  main(injection_parameters, interferometers, template_ifos, start_time, minimum_frequency)




    # PLOT
    offset = sampler_dict['parameter_offsets'][6]

    from matplotlib import rc

    plt.rcParams['text.usetex'] = False

    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'

    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'

    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10

    fig= plt.figure(figsize=(16,8))
    # fig, axs = plt.subplots(2, 1, figsize=(16,8), gridspec_kw = {'height_ratios': [10, 1]})

    # fig.suptitle('logL and dlogL computed with a step size of {:.0e}'.format(offset))
    ax0 = plt.subplot(111)
    ax0.plot(sin_dec_list, logL_list, linewidth=0.5, marker='o', markersize=3, color='green', label='logL')
    # import IPython; IPython.embed()
    ax0.axvline(x=sin_dec_inj, linewidth=0.5, color='black', label='injected value')
    ax0.set_xlabel('sin(dec)')
    ax0.set_ylabel('logL')
    ax0.set_title('logL and dlogL. Step size between plotted points = offset for dlogL =  {:.0e}'.format(offset))
    ax0.legend(loc=(0.85,0.9))

    ax1 = ax0.twinx()
    ax1.plot(sin_dec_list, dlogL_list, linewidth=0.5, marker='o', markersize=3, color='red', label='dlogL')
    ax1.set_ylabel('dlogL', color='red')
    ax1.tick_params('y', colors='red')
    ax1.legend(loc=(0.85,0.85))

    # axs[0].plot(sin_dec_list, logL_list, linewidth=1, color='green', label='logL')
    # axs[0].axvline(x=sin_dec_inj, linewidth=2, color='black')
    # axs[0].set_xlabel('sin(dec)')
    # axs[0].set_ylabel('logL')
    # axs[0].set_title('logL and dlogL computed with a step size of {:.0e}'.format(offset))
    #
    # ax01 = axs[0].twinx()
    # ax01.plot(sin_dec_list, dlogL_list, linewidth=1, color='red', label='dlogL')
    # ax01.set_ylabel('dlogL', color='red')
    # ax01.tick_params('y', colors='red')


    # axs[1].plot(sin_dec_list, dlogL_list, linewidth=1, color='red', label='dlogL')
    # axs[1].set_xlabel('sin(dec)')
    # axs[1].set_title('dlogL computed with a step size of {:.0e}'.format(offset))

    fig.tight_layout()

    file_name = './instability_offsets_comparison/logL_and_dlogL_vs_sindec_{:.0e}'.format(offset)
    # fig.savefig(file_name)

    plt.show()
