import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as CdlogL
import Codes.FIM as FIM


if __name__=='__main__':
    usage = """
    Script meant to check what is the acceptance rate during phase1's accepted trajectories but run using analytical fit of the gradient instead of numerical
    """
    parser=OptionParser(usage)

    parser.add_option("--outdir", default='../__output_data/GW170817/SUB-D_012345678/PSD_1/1500_1500_2000_200_200_0.005_dlogL1/IMRPhenomPv2_ROQ', action="store", type="string", help="""Output directory of the run we are interested in.""")
    parser.add_option("--fit_method", default='cubic', action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")

    ##################################################################
    # BIG COPY PASTE OF THE FIRST PART OF THE CODE IN BNS_HMC_9D.py
    ##################################################################

    # PARSE INPUT ARGUMENTS
    (opts,args)=parser.parse_args()

    np.random.seed(88170235)

    config_parser = ConfigParser()
    config_file_path = opts.outdir + '/config.ini'
    config_parser.read(config_file_path)
    config_dict = pu.config_parser_to_dict(config_parser)
    # print("\nOptions from the configuration file {} are:".format(opts.config_file))
    # pu.print_dict(config_dict)



    n_traj_hmc_tot = config_dict['hmc']['n_traj_hmc_tot']
    n_traj_fit = config_dict['hmc']['n_traj_fit']
    n_traj_for_this_run = config_dict['hmc']['n_traj_for_this_run']
    n_fit_1 = config_dict['hmc']['n_fit_1']
    n_fit_2 = config_dict['hmc']['n_fit_2']
    length_num_traj = config_dict['hmc']['length_num_traj']
    epsilon0 = config_dict['hmc']['epsilon0']

    ifos_possible = ['H1', 'L1', 'V1']
    ifo_chosen = config_dict['analysis']['ifos'].split(',')
    if set.intersection(set(ifos_possible), set(ifo_chosen)) != set(ifo_chosen):
        raise ValueError("the '--ifos' option was wrongly set, you must choose between {}. Example: '--ifos=H1,V1'. ".format(ifos_possible))
    CONST.approximant = config_dict['analysis']['approx']
    CONST.minimum_frequency = config_dict['analysis']['minimum_frequency']
    CONST.maximum_frequency = config_dict['analysis']['maximum_frequency']
    CONST.reference_frequency = config_dict['analysis']['reference_frequency']
    CONST.roq = config_dict['analysis']['roq']
    if CONST.roq:
        CONST.roq_b_matrix_directory = config_dict['analysis']['roq_b_matrix_directory']
    CONST.psd = config_dict['analysis']['psd']
    sampler_dict['dlogL'] = config_dict['analysis']['dlogl']
    sampler_dict['search_parameter_indices'] = [int(i) for i in list(config_dict['analysis']['search_parameter_indices'].split(','))]
    exponents = [int(e) for e in list(config_dict['analysis']['parameter_offsets'].split(','))]
    sampler_dict['parameter_offsets'] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    idx_nonzero = np.nonzero(exponents)[0]
    for i in idx_nonzero:
        sampler_dict['parameter_offsets'][i] = 10**(-exponents[i])

    search_traj_parameter_keys = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
    sampler_dict['search_parameter_indices_local_fit'] = np.intersect1d(sampler_dict['search_parameter_indices'], sampler_dict['search_parameter_indices_local_fit']).tolist()
    sampler_dict['search_traj_parameter_keys_local_fit'] = np.intersect1d(search_traj_parameter_keys, CONST.PARAMETERS_KEYS_LOCAL_FIT).tolist()

    # need this line in case for instance: `search_parameter_indices_local_fit_full = 2` where it's gonna be converted into an int and hence won't be able to be split()
    parameter_indices_local_fit_full_str = str(config_dict['analysis']['search_parameter_indices_local_fit_full'])
    search_parameter_indices_local_fit_full = [int(i) for i in list(parameter_indices_local_fit_full_str.split(','))]
    sampler_dict['search_parameter_indices_local_fit_full'] = np.intersect1d(sampler_dict['search_parameter_indices'], search_parameter_indices_local_fit_full).tolist()
    sampler_dict['search_traj_parameter_keys_local_fit_full'] = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices_local_fit_full']]

    n_param = 9        # Number of parameters used
    randGen = np.random.RandomState(seed=2)







    # SET THE INJECTION PARAMETERS
    injection_parameters = set_inj.ini_file_to_dict(opts.outdir + '/injection.ini')
    # print("\nInjected parameters derived from {} are:".format(opts.inj_file))
    # pu.print_dict(injection_parameters)
    minimum_frequency = CONST.minimum_frequency
    maximum_frequency_ifo = CONST.maximum_frequency_ifo
    duration = CONST.duration
    sampling_frequency = CONST.sampling_frequency
    start_time = CONST.start_time


    # INITIALIZE THE THREE INTERFEROMETERS
    interferometers = bilby.gw.detector.InterferometerList(ifo_chosen)
    # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        ifo.maximum_frequency = maximum_frequency_ifo
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=CONST.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)


    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=start_time)

    # WAVEFORM GENERATION
    CONST.waveform_arguments['maximum_frequency'] = CONST.maximum_frequency_injected_waveform
    template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    for i in range(len(interferometers)):
        interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain

    # COMPUTE AND PRINT SNR
    CONST.waveform_arguments['maximum_frequency'] = CONST.maximum_frequency_search_waveform
    template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)
    logL, Snr, opt_snr_Best = logL_snr.main(template_ifos, interferometers, True)


    # COMPUTE dlogL and print it to check with the plots
    dlogL = CdlogL.main(injection_parameters, start_time, minimum_frequency, interferometers, True)

    # DEFINE BOUNDARY CONDITIONS FOR HMC
    # The boundary conditions are given both for masses m1, m2 (solar masses) and for the distance (meters)
    boundaries = dict(
        m1min = config_dict['priors']['comp-min'],
        m1max = config_dict['priors']['comp-max'],
        m2min = config_dict['priors']['comp-min'],
        m2max = config_dict['priors']['comp-max'],
        Dmin = config_dict['priors']['distance-min'],
        Dmax = config_dict['priors']['distance-max']
    )

    injection_parameters['boundaries'] = boundaries

    boundary=np.array([[boundaries['m1min'],boundaries['m1max']],[boundaries['m2min'],boundaries['m2max']],[boundaries['Dmin'] * MPC, boundaries['Dmax'] * MPC]])

    # print("\nAstrophysical boundaries :")
    # print("m1min = {}, m1max = {}".format(boundary[0][0], boundary[0][1]))
    # print("m2min = {}, m2max = {}".format(boundary[1][0], boundary[1][1]))
    # print("Dmin = {}, Dmax = {}".format(boundary[2][0]/MPC, boundary[2][1]/MPC))

    FisherMatrix, scale, scale_SVD = FIM.main(injection_parameters, start_time, minimum_frequency, interferometers, **sampler_dict)


    # # re-adjust scales if FIM is incorrect
    # if scale[0] >1.0: scale[0] = 1.0           # assume 50% error in cosinc
    # if scale[1] > PI: scale[1] = PI           # assume 50% error in phi_c
    # if scale[2]>PIb2: scale[2] = PIb2     # assume 50% error in psi
    # if scale[3]> 0.5: scale[3] = 0.5           # assume 50% error in lnDL

    # re-adjust scales if FIM is incorrect
    if scale[0] >1.0: scale[0] = 1.0/2           # assume 50% error in cosinc
    if scale[1] > PI: scale[1] = PI/2           # assume 50% error in phi_c
    if scale[2]>PIb2: scale[2] = PIb2/2     # assume 50% error in psi
    if scale[3]> 0.5: scale[3] = 0.5/2           # assume 50% error in lnDL


    # ROQ Basis used?
    if CONST.roq:
        if injection_parameters['meta']['approximant'] != 'IMRPhenomPv2':
            raise ValueError("Need to set approximant to IMRPhenomPv2 to work with the ROQ basis, approximant here is {}".format(injection_parameters['meta']['approximant']))

        scale_factor = injection_parameters['roq']['scale_factor']
        roq_directory = injection_parameters['roq']['directory']
        # Load in the pieces for the linear part of the ROQ. Note you will need to
        freq_nodes_linear = np.load(roq_directory + "fnodes_linear.npy") * scale_factor
        # Load in the pieces for the quadratic part of the ROQ
        freq_nodes_quadratic = np.load(roq_directory + "fnodes_quadratic.npy") * scale_factor

        # Load the parameters describing the valid parameters for the basis.
        params = injection_parameters['roq']['rescaled_params'].copy()

        outdir_psd = 'PSD_{}/'.format(CONST.psd)
        roq_outdir = '../__ROQ_weights/' + 'GW170817' + '/' + outdir_psd
        weights_file_path = roq_outdir + '{:.0f}Hz_{:.0f}Hz_{:.0f}Hz_{:.0f}s_weights.json'.format(params['flow'], injection_parameters['meta']['maximum_frequency_ifo'], injection_parameters['meta']['maximum_frequency_injected_waveform'], params['seglen'])

        # make ROQ waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=duration, sampling_frequency=sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.binary_neutron_star_roq,
            waveform_arguments=dict(
                frequency_nodes_linear=freq_nodes_linear,
                frequency_nodes_quadratic=freq_nodes_quadratic,
                reference_frequency=injection_parameters['meta']['reference_frequency'], waveform_approximant=injection_parameters['meta']['approximant']),
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters)

        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        priors = bilby.gw.prior.BNSPriorDict()
        # for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'phi_12', 'phi_jl', 'luminosity_distance']:
        for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'luminosity_distance']:
            priors[key] = injection_parameters[key]
        for key in ['mass_1', 'mass_2']:
            priors[key].minimum = max(priors[key].minimum, params['compmin'])
        priors['chirp_mass'] = bilby.core.prior.Constraint(name='chirp_mass', minimum=float(params['chirpmassmin']), maximum=float(params['chirpmassmax']))
        priors['mass_ratio'] = bilby.core.prior.Constraint(0.125, 1, name='mass_ratio')
        priors['geocent_time'] = bilby.core.prior.Uniform(injection_parameters['geocent_time'] - 0.1, injection_parameters['geocent_time'] + 0.1, latex_label='$t_c$', unit='s')


        if os.access(weights_file_path, os.W_OK):
            # load the weights from the file
            likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                weights=weights_file_path, priors=priors)
        else:
            # Load in the pieces for the linear part of the ROQ. Note you will need to
            basis_matrix_linear = np.load(roq_directory + "B_linear.npy").T
            # Load in the pieces for the quadratic part of the ROQ
            basis_matrix_quadratic = np.load(roq_directory + "B_quadratic.npy").T

            # Redoing here a bit what's already being done in set_inj because `bilby.gw.likelihood.ROQGravitationalWaveTransient()` does not accept dictionnary for roq_params...
            roq_params_ndarray = np.genfromtxt(injection_parameters['roq']['params_path'], names=True)
            roq_params_ndarray = roqu.rescale_params(roq_params_ndarray, scale_factor)
            # roq_params_ndarray = None

            likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic,
                priors=priors, roq_params=roq_params_ndarray)

            # remove the basis matrices as these are big for longer bases
            del basis_matrix_linear, basis_matrix_quadratic

            # write the weights to file so they can be loaded multiple times
            # likelihood.save_weights(weights_file_path)


        search_parameter_keys = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
        likelihood.parameters = injection_parameters

        logL = likelihood.log_likelihood_ratio()
        print("")
        print("likelihood.log_likelihood_ratio() = {}".format(logL))

        likelihood_gradient = lg.GWTransientLikelihoodGradient(likelihood, search_parameter_keys)
        dlogL_dict = likelihood_gradient.calculate_log_likelihood_gradient()
        # pu.print_dict({"dlogL_ROQ": dlogL_dict})
        dlogL = likelihood_gradient.convert_dlogL_dict_to_np_array(dlogL_dict)
    else:
        likelihood_gradient = None

    count = 0
    ##################################################################
    # END OF COPY PASTE
    ##################################################################











    # The idea is to load all the data necessary to reproduce the "same" phase1 from the output directory where it has been stored
    print("\n\nLoading qpos_global_fit/bad and dlogL_global_fit/bad from files pt/dlogL_fit_phase1.dat")
    outdir_phase1 = opts.outdir + '/phase1'
    q_pos_traj_phase1 = np.loadtxt(outdir_phase1 + '/pt_fit_phase1.dat', dtype=np.longdouble)
    p_mom_phase1 = np.loadtxt(outdir_phase1 + '/pmom_trajs.dat', dtype=np.longdouble)
    dlogL_fit_phase1 = np.loadtxt(outdir_phase1 + '/dlogL_fit_phase1.dat', dtype=np.longdouble)

    oluts_dict = fnr.get_oluts_dict(q_pos_traj_phase1, dlogL_fit_phase1, sampler_dict['search_parameter_indices_local_fit'])
    file_name_fit_coeff_global = f'fit_coeff_global_{len(q_pos_traj_phase1)}.dat'
    fit_coeff_global = np.loadtxt(opts.outdir + '/' + file_name_fit_coeff_global)


    q_pos_traj_phase1_start = q_pos_traj_phase1[::length_num_traj]
    p_mom_phase1_start = p_mom_phase1[::length_num_traj]
    dlogL_phase1_start = dlogL_fit_phase1[::length_num_traj]




    for traj_index in range(len(q_pos_traj_phase1_start)):
        q_pos_traj_0 = q_pos_traj_phase1_start[traj_index]
        q_pos_0 = paru.q_pos_traj_to_q_pos(q_pos_traj_0)
        p_mom_0 = p_mom_phase1_start[traj_index]
        dlogL_0 = dlogL_phase1_start[traj_index]

        # Compute logL0
        parameters_start_of_traj = paru.q_pos_to_dictionary(q_pos_0, start_time)
        if likelihood_gradient is None:
            template_ifos = bilby_wv.WaveForm_ThreeDetectors(parameters_start_of_traj, minimum_frequency, interferometers)
            # Calculate Hamiltonian at final position, and evaluate Hastings ratio with appropriate priors
            logL_0 = bilby_utils.loglikelihood(template_ifos, interferometers)
        else:
            likelihood_gradient.likelihood.parameters = parameters_start_of_traj
            logL_0 = likelihood_gradient.likelihood.log_likelihood_ratio()

        #    Hamiltonian at the start of the trajectory
        H_0 = bht.computeHamiltonian(p_mom_0, logL_0)

        rannum = randGen.normal(loc=0, scale=1.5)
        epsilon = (epsilon0 + rannum) * 1.0e-3
        if epsilon <= 0.001: epsilon = 0.001
        if epsilon >= 0.01: epsilon = 0.01
        length_num_traj = 50 + int(randGen.uniform() * 100)
        q_pos_traj, p_mom, dlogL_pos, BreakIndic = bht.AnalyticalGradientLeapfrog_ThreeDetectors_Trajectory(q_pos_0, p_mom_0, dlogL_0, epsilon, length_num_traj, scale, fit_coeff_global, oluts_dict, boundary, n_param, len(q_pos_traj_phase1), n_fit_1, n_fit_2, scale**2, traj_index, None, None, None)

        if BreakIndic==0:
            q_pos = paru.q_pos_traj_to_q_pos(q_pos_traj)
            parameters_end_of_traj = paru.q_pos_to_dictionary(q_pos, start_time)
            if likelihood_gradient is None:
                template_ifos = bilby_wv.WaveForm_ThreeDetectors(parameters_end_of_traj, minimum_frequency, interferometers)
                # Calculate Hamiltonian at final position, and evaluate Hastings ratio with appropriate priors
                logL_final = bilby_utils.loglikelihood(template_ifos, interferometers)
            else:
                likelihood_gradient.likelihood.parameters = parameters_end_of_traj
                logL_final = likelihood_gradient.likelihood.log_likelihood_ratio()

            H = bht.computeHamiltonian(p_mom, logL_final)

            a = randGen.uniform(low=0, high=1)    # draw random number
            proba_Acc = np.exp(-(H-H_0))
            if a < proba_Acc or H <= H_0:    # Accept position
                Accept = 1

            else:    # Reject position
                Accept = 0
        else:    # Reject position because of divergence and/or NaN
            Accept = 0

        if Accept == 1:
            count += 1
            traj_status = "** ACCEPTED **"
            MH_ratio_sup_or_inf = '<'
        else:
            traj_status = "   rejected   "
            MH_ratio_sup_or_inf = '>'

        Acc = count/(traj_index + 1)

        print(f'{traj_index+1:3} - {traj_status} - Acc = {Acc:5.1%} - {a:1.5f} {MH_ratio_sup_or_inf} {proba_Acc:1.5f}')

    sys.exit()
