# This script is meant to compare the influence of the offset used in the numerical derivative on trajectories that have revealed some instability.

import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../')
import bilby
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.CONST as CONST
import Codes.set_injection_parameters as set_inj

injection_parameters = set_inj.ini_file_to_dict()
parameters = injection_parameters.copy()


nb_points = 100
offset = sampler_dict['parameter_offsets'][6]

# Values taken from the trajectory with offset = 1e-3
# Cf file `pt_debug.dat` here: /Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/newPythonCode/__output_data/GW170817/bilbyoriented/SUB-D_######6##/logL920.44/0e+00_1_2000_200_200_0.005_dlogL2_1e-03
# sin_dec_max = -3.968513939046793837e-01
# # sin_dec_min = -4.008370449495952670e-01
# sin_dec_min = sin_dec_max - nb_points * offset

sin_dec_inj = np.sin(injection_parameters['dec'])
sin_dec_max = sin_dec_inj + nb_points/2 * offset
sin_dec_min = sin_dec_inj - nb_points/2 * offset


sin_dec_list = np.linspace(sin_dec_min, sin_dec_max, nb_points)
logL_list = []
dlogL_list = []

interferometers = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
# The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
for ifo in interferometers:
    ifo.minimum_frequency = set_inj.minimum_frequency
            ifo.strain_data._set_time_and_frequency_array_parameters(set_inj.duration, set_inj.sampling_frequency, set_inj.start_time)

# import IPython; IPython.embed()
# sys.exit()

# Change LIGO psd files if wanted:
path = bilby.__path__[0] + '/gw/noise_curves/'
new_LIGO_psd_file_name = 'aLIGO_early_high_psd.txt'
# new_LIGO_psd_file_name = 'aLIGO_mid_psd.txt'
# new_LIGO_psd_file_name = 'aLIGO_ZERO_DET_high_P_psd.txt'
interferometers[0].power_spectral_density.psd_file = path + new_LIGO_psd_file_name
interferometers[1].power_spectral_density.psd_file = path + new_LIGO_psd_file_name

new_noise_realisation_from_psd = False
if new_noise_realisation_from_psd:
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=set_inj.sampling_frequency,
        duration=set_inj.duration,
        start_time=set_inj.start_time)
else:
    noise_realisation_full_path = '/Users/marcarene/Documents/APC/Code/myPython/YannsCtoPython/newPythonCode/__input_data/noise_realisations/aLIGO_early_high_psd/logL920.44'
    H1_fds = np.loadtxt(noise_realisation_full_path + '/H1_fds.dat').view(complex)
    L1_fds = np.loadtxt(noise_realisation_full_path + '/L1_fds.dat').view(complex)
    V1_fds = np.loadtxt(noise_realisation_full_path + '/V1_fds_from_AdV_psd.dat').view(complex)

    interferometers[0].set_strain_data_from_frequency_domain_strain(
        frequency_domain_strain=H1_fds,
        sampling_frequency=set_inj.sampling_frequency,
        duration=set_inj.duration,
        start_time=set_inj.start_time
        )
    interferometers[1].set_strain_data_from_frequency_domain_strain(
        frequency_domain_strain=L1_fds,
        sampling_frequency=set_inj.sampling_frequency,
        duration=set_inj.duration,
        start_time=set_inj.start_time
        )
    interferometers[2].set_strain_data_from_frequency_domain_strain(
        frequency_domain_strain=V1_fds,
        sampling_frequency=set_inj.sampling_frequency,
        duration=set_inj.duration,
        start_time=set_inj.start_time
        )
# Generate the template to inject in each detector
template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, set_inj.start_time, set_inj.minimum_frequency, interferometers)

# Add the template signal to each detector's strain data
interferometers[0].strain_data.frequency_domain_strain += h_wave_1
interferometers[1].strain_data.frequency_domain_strain += h_wave_2
interferometers[2].strain_data.frequency_domain_strain += h_wave_3

# This copy is here just because for the moment I will keep manipulating 3 separate variables as originally done in the hmc
signal1 = interferometers[0].strain_data.frequency_domain_strain.copy()
signal2 = interferometers[1].strain_data.frequency_domain_strain.copy()
signal3 = interferometers[2].strain_data.frequency_domain_strain.copy()

for sin_dec in sin_dec_list:
    parameters.update({'dec': np.arcsin(sin_dec)})

    template_ifos = bilby_wv.WaveForm_ThreeDetectors(parameters, set_inj.start_time, set_inj.minimum_frequency, interferometers)
    logL = bilby_utils.loglikelihood(template_ifos, interferometers)
    dlogL, logL = bilby_wv.dlogL_ThreeDetectors(template_ifos, parameters, set_inj.start_time, set_inj.minimum_frequency, interferometers)

    logL_list.append(logL)
    dlogL_list.append(dlogL[6])








from matplotlib import rc

plt.rcParams['text.usetex'] = False

plt.rcParams['figure.titlesize'] = 18
plt.rcParams['figure.titleweight'] = 'bold'

plt.rcParams['axes.titlesize'] = 15
plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titleweight'] = 'normal'

plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12
plt.rcParams['font.size'] = 10

fig= plt.figure(figsize=(16,8))
# fig, axs = plt.subplots(2, 1, figsize=(16,8), gridspec_kw = {'height_ratios': [10, 1]})

# fig.suptitle('logL and dlogL computed with a step size of {:.0e}'.format(offset))
ax0 = plt.subplot(111)
ax0.plot(sin_dec_list, logL_list, linewidth=0.5, marker='o', markersize=3, color='green', label='logL')
# import IPython; IPython.embed()
ax0.axvline(x=sin_dec_inj, linewidth=0.5, color='black', label='injected value')
ax0.set_xlabel('sin(dec)')
ax0.set_ylabel('logL')
ax0.set_title('logL and dlogL. Step size between plotted points = offset for dlogL =  {:.0e}'.format(offset))
ax0.legend(loc=(0.85,0.9))

ax1 = ax0.twinx()
ax1.plot(sin_dec_list, dlogL_list, linewidth=0.5, marker='o', markersize=3, color='red', label='dlogL')
ax1.set_ylabel('dlogL', color='red')
ax1.tick_params('y', colors='red')
ax1.legend(loc=(0.85,0.85))

# axs[0].plot(sin_dec_list, logL_list, linewidth=1, color='green', label='logL')
# axs[0].axvline(x=sin_dec_inj, linewidth=2, color='black')
# axs[0].set_xlabel('sin(dec)')
# axs[0].set_ylabel('logL')
# axs[0].set_title('logL and dlogL computed with a step size of {:.0e}'.format(offset))
#
# ax01 = axs[0].twinx()
# ax01.plot(sin_dec_list, dlogL_list, linewidth=1, color='red', label='dlogL')
# ax01.set_ylabel('dlogL', color='red')
# ax01.tick_params('y', colors='red')


# axs[1].plot(sin_dec_list, dlogL_list, linewidth=1, color='red', label='dlogL')
# axs[1].set_xlabel('sin(dec)')
# axs[1].set_title('dlogL computed with a step size of {:.0e}'.format(offset))

fig.tight_layout()

file_name = './instability_offsets_comparison/logL_and_dlogL_vs_sindec_{:.0e}'.format(offset)
# fig.savefig(file_name)

plt.show()
