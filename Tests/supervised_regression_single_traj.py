# get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
# import seaborn as sns; sns.set()
import numpy as np
import pandas as pd

from sklearn import neighbors, svm, ensemble, linear_model, metrics, model_selection, preprocessing

# import IPython as ip
import sys
sys.path.append('../')
import Library.CONST as CONST
import Library.python_utils as pu


def plot_regression(data_predicted, data_true, ax=None, label='', data_label='data'):
    """Summary plot of regression performance.
    """
    ax = ax or plt.gca()
    ax.scatter(data_true, data_predicted, lw=0, alpha=0.5, s=10)
    ax.set_xlabel('True ' + data_label)
    ax.set_ylabel('Predicted ' + data_label)
    x_min = data_true.min()
    x_max = data_true.max()
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(x_min, x_max)
    ax.plot([x_min, x_max], [x_min, x_max], 'r--')
    R2 = metrics.r2_score(data_true, data_predicted)
    ax.text(0.05, 0.9, f'{label}$R^2 = {R2:.2f}$', transform=ax.transAxes, fontsize=16, color='r')

def test_sk_regression(method, x_train_scaled, x_test_scaled, y_train_scaled, y_train, y_test, y_scaler, ax, y_label):
    # Fit normalized training dlogL.
    fit = method.fit(x_train_scaled, y_train_scaled)
    # Get predicted dlogL for training qpos.
    y_train_predicted = y_scaler.inverse_transform(fit.predict(x_train_scaled))
    # Get predicted shear for test images.
    y_test_predicted = y_scaler.inverse_transform(fit.predict(x_test_scaled))
    # y_train = y_scaler.inverse_transform(y_train_scaled)
    # Plot results.
    # y_label=f'dlogL[{idx_to_plot}]'
    # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
    plot_regression(y_train_predicted, y_train, label='train: ', data_label=y_label, ax=ax[0])
    plot_regression(y_test_predicted, y_test, label='test: ', data_label=y_label, ax=ax[1])
    plt.tight_layout()

def plot_num_and_ana_gradients(dlogL_num, dlogL_ana, opts_dict=None, save=True, show_plot=False, file_name=None, outdir='./plots'):
    from matplotlib import rc
    plt.rcParams['text.usetex'] = False
    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'
    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'
    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10

    steps = np.arange(len(dlogL_num[0]))

    # params_to_plot = [0]
    params_to_plot = list(range(dlogL_num.shape[0]))

    fig, axs = plt.subplots(nrows=len(params_to_plot), figsize=(15, 3 * dlogL_num.shape[0]))
    # fig, axs = plt.subplots(nrows=dlogL_num.shape[0], figsize=(15, 3 * dlogL_num.shape[0]))
    # fig, axs = plt.subplots(dlogL_num.shape[0], 1, figsize=(16,8.2))
    gradient_names = ['dlogL/d' + fpname for fpname in CONST.TRAJ_PARAMETERS_KEYS]

    title = ''
    file_name_default = ''
    if 'method' in opts_dict.keys():
        title += opts_dict['method'] + ' - '
        file_name_default += opts_dict['method'] + '_'
    if 'training' in opts_dict.keys():
        title = 'train_nph1 = {} - '.format(opts_dict['training']) + title
        file_name_default = 'train_{}_'.format(opts_dict['training']) + file_name_default
    if 'testing' in opts_dict.keys():
        title += ' - traj end PH1-{} - '.format(opts_dict['testing'])
        file_name_default += '_TrajEndPh1_{}'.format(opts_dict['testing'])

    for i, param_index in enumerate(params_to_plot):
    # for param_index in range(dlogL_num.shape[0]):
        axs[i].plot(steps, dlogL_num[param_index], label='numerical')
        axs[param_index].plot(steps, dlogL_ana[param_index], label='analytical along num trajectory')
        axs[i].set_xlabel("steps", color='black')
        axs[i].set_ylabel("{}".format(gradient_names[param_index]), color='black')
        axs[i].legend()


    fig.suptitle(title)
    fig.tight_layout()
    fig.subplots_adjust(top=0.95)

    if show_plot:
        plt.show()

    if file_name is None:
        file_name = file_name_default
    file_name = outdir + '/' + file_name + '.png'
    if save:
        pu.mkdirs(outdir)
        fig.savefig(file_name)


if __name__=='__main__':
    from optparse import OptionParser
    usage = """Script meant to test and improve the function `Table_GradientFit()`, ie the local OLUTs fit."""
    parser=OptionParser(usage)
    parser.add_option("--search_parameter_indices", default='02345678', action="store", type="string", help=""" Indices of the parameters to run the hmc on. Other are kept fixed. Default are values already stored in optsig.py file.""")
    parser.add_option("--training", default='1500', action="store", type="string", help=""" Number of trajectories in phase 1.""")
    parser.add_option("--testing", default='1500', action="store", type="string", help=""" Selects the testing set. It corresponds to the 1000 step trajectory that was run at the end of phase1.""")

    (opts,args)=parser.parse_args()

    sampler_dict['search_parameter_indices'] = [int(i) for i in list(opts.search_parameter_indices)]

    directory = '../Tests/.supervised_regression_single_traj/SUB' + opts.search_parameter_indices + '/'
    input_dir = directory + 'input_data/'

    testing_dir = input_dir + 'testing_data/'
    training_dir = input_dir + 'training_data/'
    qpos_train_tot = np.loadtxt(training_dir + opts.training + '_training_q_pos_traj.dat')
    dlogL_train_tot = np.loadtxt(training_dir + opts.training + '_training_dlogL.dat')

    qpos_test = np.loadtxt(testing_dir + opts.testing + '_test_q_pos_traj.dat')
    dlogL_test = np.loadtxt(testing_dir + opts.testing + '_test_dlogL.dat')
    # breakpoint()
    qpos_train = qpos_train_tot[:-200, :]
    dlogL_train = dlogL_train_tot[:-200, :]
    # qpos_train = qpos_train_tot
    # dlogL_train = dlogL_train_tot
    # qpos_test = qpos_train_tot[-200:, :]
    # dlogL_test = dlogL_train[-200:, :]


    # search_parameter_indices = [0,1,3,4,5,6,7,8]
    # outdir_subD = 'SUB-D_'
    # for i in range(sampler_dict['n_dim']):
    #     if i in search_parameter_indices:
    #         outdir_subD+='{}'.format(i)
    #     else:
    #         outdir_subD += '#'
    # output_data_dir = '../__output_data/GW170817'
    # directory = output_data_dir + f'/{outdir_subD}/PSD_1/501_500_2000_200_200_0.005_dlogL1/IMRPhenomPv2_ROQ/phase1'

    # method = neighbors.KNeighborsRegressor()
    # method_str = 'KN'
    method = ensemble.RandomForestRegressor(random_state=123)
    method_str = 'RF'
    # method = ensemble.GradientBoostingRegressor(random_state=123)
    # method_str = 'GB'
    opts_fit = dict(training = opts.training, testing = opts.testing, method=method_str)


    df_qpos_train = pd.DataFrame(qpos_train[:, sampler_dict['search_parameter_indices']])
    # df_qpos_train_tot = pd.DataFrame(qpos_train_tot[:, sampler_dict['search_parameter_indices']])
    # df_dlogL_train_tot = pd.DataFrame(dlogL_train_tot[:, sampler_dict['search_parameter_indices']])
    # df_qpos_train, df_qpos_test_unused, df_dlogL_train, df_dlogL_test_unused = model_selection.train_test_split(df_qpos_train_tot, df_dlogL_train_tot, test_size=0.25, random_state=123)
    # dlogL_train = df_dlogL_train.values

    df_qpos_test = pd.DataFrame(qpos_test[:, sampler_dict['search_parameter_indices']])

    qpos_scaler = preprocessing.StandardScaler().fit(df_qpos_train)
    qpos_train_scaled = qpos_scaler.transform(df_qpos_train)
    qpos_test_scaled = qpos_scaler.transform(df_qpos_test)

    parameter_indices_to_predict = sampler_dict['search_parameter_indices'].copy()
    # parameter_indices_to_predict = [0]
    # dlogL_names = ['dlogL/d' + fpname for fpname in [CONST.TRAJ_PARAMETERS_KEYS[j] for j in parameter_indices_to_predict]]
    # nb_dlogL_to_predict = len(parameter_indices_to_predict)
    # fig, ax = plt.subplots(nb_dlogL_to_predict, 2, sharex=False, sharey=False, figsize=(10, 4 * nb_dlogL_to_predict))
    # if nb_dlogL_to_predict==1:
        # ax = ax.reshape(1, -1)
    dlogL_test_predicted = np.zeros((len(qpos_test), 9))

    # Train all the gradients
    for ii, idx_to_plot in enumerate(parameter_indices_to_predict):
        print(f'\nidx_to_plot = {idx_to_plot}')
        dlogL_train_idx = dlogL_train[:, idx_to_plot]
        dlogL_test_idx = dlogL_test[:, idx_to_plot]

        dlogL_scaler = preprocessing.StandardScaler().fit(dlogL_train_idx.reshape(-1, 1))
        dlogL_train_scaled = dlogL_scaler.transform(dlogL_train_idx.reshape(-1, 1)).reshape(-1)
        print('Training the model...')
        fit = method.fit(qpos_train_scaled, dlogL_train_scaled)
        # Get predicted shear for test images.
        print('Predicting dlogL on test values...')
        dlogL_test_predicted[:, idx_to_plot] = dlogL_scaler.inverse_transform(fit.predict(qpos_test_scaled))

        # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
        # dlogL_train_predicted = dlogL_scaler.inverse_transform(fit.predict(qpos_train_scaled))
        # dlogL_label = f'dlogL[{idx_to_plot}]'
        # plot_regression(dlogL_train_predicted, dlogL_train_idx, label='train: ', data_label=dlogL_label, ax=ax[0])
        # plot_regression(dlogL_test_predicted[:, idx_to_plot], dlogL_test_idx, label='test: ', data_label=dlogL_label, ax=ax[1])
        # plt.show()

    plot_num_and_ana_gradients(dlogL_test.T, dlogL_test_predicted.T, save=True, opts_dict=opts_fit, show_plot=False, outdir=directory + 'plots_num_vs_ana')


    # ip_shell = ip.get_ipython()
    # ip_shell.run_line_magic('time', 'test_sk_regression(method=method, x_train_scaled=qpos_train_scaled, x_test_scaled=qpos_test_scaled, y_train_scaled=dlogL_train_scaled, y_train=df_dlogL_train.values, y_test=df_dlogL_test.values, y_scaler=dlogL_scaler, ax=ax[ii,:], y_label=dlogL_names[ii])')
