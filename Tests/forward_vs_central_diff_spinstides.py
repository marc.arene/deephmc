# python forward_vs_central_diff_spinstides.py --event=GW170817 --trigger_file=../examples/trigger_files/GW170817_IMRPhenomD_NRTidal.ini --config_file=../examples/config_files/config_GW170817_IMRPhenomD_NRTidal.ini --fit_method=cubic

import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess
import pandas as pd

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf
import Library.initialization as init
import Library.psd_utils as psd_utils

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds

import gw.detector.networks as gwdn
import core.sampler
import core.utils as cut
import core.autocorrelation as cautocorr
import core.prior_gradient as pg
import gw.prior as prior
import gw.likelihood as gwlh



if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--inj_file", default='../examples/injection_files/GW170817.ini', action="store", type="string", help="""Injection file path leading to the `.ini` file specifying the parameters of the injection.""")
    parser.add_option("--event", default=None, action="store", type="string", help="""Event label part of the catalog GWTC-1""")
    parser.add_option("--trigger_file", default=None, action="store", type="string", help="""File .ini describing the parameters where to start the chain. If left to None and --event used, a default file corresponding to the event will be used.""")
    parser.add_option("--trigger_time", default=None, action="store", type="string", help="""Trigger GPS time.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")
    parser.add_option("--outdir", default=None, action="store", type="string", help="""Output directory where results and plots will be stored. If not specified, it will be created automatically in the current working directory under ./outdir""")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""Print out information of interest during the run.""",dest='verbose')
    parser.add_option("--plot_traj", default=False, action="store_true", help="""Will save positions, momentas, hamiltonian values etc of every point along every trajectory in phase to be able to plot them with `plot_trajectories.py` script.""")
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")
    parser.add_option("--fit_method", default='dnn', action="store", type="string", help="""Method used to fit the numerical gradients of the log likelihood accumulated in phase1. """)
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")
    parser.add_option("--on_CIT", default=False, action="store_true", help="""Set this to True if running this script on Caltech's cluster. Will set the right path for the ROQ basis.""")

    # randGen = np.random.RandomState(seed=2)

    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()

    opts_dict_formatted = cut.dictionary_to_formatted_string(opts.__dict__)
    cut.logger.info(f'Options from the command line are: {opts_dict_formatted}')

    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)
    cut.logger.info(f'Options from the configuration file {opts.config_file} are: {config_dict_formatted}')

    if opts.on_CIT and config_dict['analysis']['roq']:
        config_dict['analysis']['roq_b_matrix_directory'] = '/home/cbc/ROQ_data/IMRPhenomPv2/'
        cut.logger.info('Setting the roq_b_matrix_directory to "/home/cbc/ROQ_data/IMRPhenomPv2/"')

    RUN_CONST.plot_traj = opts.plot_traj
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    # Check whether the run is on an injection, an event from the catalog or a trigger time
    if opts.event is not None:
        data_name = opts.event + 'real'
    elif opts.inj_file is not None:
        inj_file_name = opts.inj_file.split('/')[-1]
        data_name = inj_file_name.split('.')[0] + 'inj'
    elif opts.trigger_time is not None:
        data_name = opts.trigger_time
    else:
        raise ValueError('You must specify at least one of the 3 options: --event, --inj_file, --trigger_time')


    # Parse the inputs relative to the sampler and the search
    # sampler_dict = init.get_sampler_dict(config_dict)
    sampler_dict = pu.json_to_dict(config_dict['hmc']['config_hmc_parameters_file'])
    sampler_dict = init.sampler_dict_update_geocent_time_by_duration(sampler_dict)
    sampler_dict_formatted = cut.dictionary_to_formatted_string(sampler_dict)
    cut.logger.info(f'Options for the sampler are: {sampler_dict_formatted}')

    # SET THE OUTPUT DIRECTORY WHERE OUTPUT FILES WILL BE SAVED
    if opts.outdir is None:
        outdir = init.get_output_dir(data_name, sampler_dict['search_parameter_keys'], config_dict, opts.sub_dir)
    else:
        outdir = opts.outdir

    outdir = './.forward_vs_central_diff_spinstides/'
    cut.setup_logger(cut.logger, outdir=outdir, label='logger', log_level='INFO')
    cut.logger.info(f'Output directory is: {outdir}')


    if opts.event is not None:

        if opts.trigger_file is None:
            trigger_file = '../examples/trigger_files/' + opts.event + '.ini'
        else:
            trigger_file = opts.trigger_file
        trigger_parameters = set_inj.ini_file_to_dict(trigger_file)
        # geocent_time = bilby.gw.utils.get_event_time(opts.event)
        # trigger_parameters['geocent_time'] = geocent_time
        ext_analysis_dict = set_inj.compute_extended_analysis_dict(trigger_parameters['mass_1'], trigger_parameters['mass_2'], trigger_parameters['geocent_time'], trigger_parameters['chirp_mass'], **config_dict['analysis'])

        subprocess.run('cp ' + trigger_file + ' ' + outdir + 'trigger.ini', shell=True)
        trig_dict_formatted = cut.dictionary_to_formatted_string(trigger_parameters)
        cut.logger.info(f'Triggered parameters derived from {trigger_file} are: {trig_dict_formatted}')
        pu.save_dict_to_json(ext_analysis_dict, outdir + 'ext_analysis_dict.json')
        ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)
        cut.logger.info(f'Analysis parameters derived from the component masses are: {ext_anal_dict_formatted}')

        start_time = ext_analysis_dict['start_time']
        duration = ext_analysis_dict['duration']

        hdf5_file_path_prefix = '../__input_data/' + opts.event
        if not(os.path.exists(hdf5_file_path_prefix)):
            cut.logger.error(f'The folder given to look for hdf5 strain files of the event: {hdf5_file_path_prefix}, does not exist.')
            sys.exit(1)

        GWTC1_event_PSDs_file = f'../__input_data/{opts.event}/GWTC1_{opts.event}_PSDs.dat'
        GWTC1_event_PSDs = np.loadtxt(GWTC1_event_PSDs_file)

        # INITIALIZE THE THREE INTERFEROMETERS
        # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
        interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])

        # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
        for i, ifo in enumerate(interferometers):
            ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
            ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']

            # Should look like: '../__input_data/GW170817/H-H1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
            if opts.event == 'GW150914':
                hdf5_file_path = f'{hdf5_file_path_prefix}/{ifo.name[0]}-{ifo.name}_GWOSC_4KHZ_R1-1126259447-32.hdf5'
            if opts.event == 'GW170817':
                hdf5_file_path = f'{hdf5_file_path_prefix}/LOSC/{ifo.name[0]}-{ifo.name}_LOSC_CLN_4_V1-1187007040-2048.hdf5'
                # hdf5_file_path = f'{hdf5_file_path_prefix}/{ifo.name[0]}-{ifo.name}_GWOSC_4KHZ_R1-1187006835-4096.hdf5'

            hdf5_strain, hdf5_start_time, hdf5_duration, sampling_frequency, hdf5_number_of_samples = psd_utils.get_strain_from_hdf5(hdf5_file_path)

            if sampling_frequency != ext_analysis_dict['sampling_frequency']:
                cut.logger.error('Sampling_frequency conflict between hdf5 file = {} and that in the config file = {}'.format(sampling_frequency, ext_analysis_dict['sampling_frequency']))
                sys.exit(1)

            idx_segment_start_float = (start_time - hdf5_start_time) * sampling_frequency
            if idx_segment_start_float == int(idx_segment_start_float):
                idx_segment_start = int(idx_segment_start_float)
            else:
                idx_segment_start = int(idx_segment_start_float) + 1
                start_time_offset = (idx_segment_start - idx_segment_start_float) / sampling_frequency
                start_time +=start_time_offset
                cut.logger.info(f'Due to numerical precison when selecting the segment, the start_time has been offset by +{start_time_offset} to equal {start_time}')
            idx_segment_end_float = (start_time + duration - hdf5_start_time) * sampling_frequency
            idx_segment_end = int(idx_segment_end_float)
            if idx_segment_end_float != idx_segment_end:
                duration_real = (idx_segment_end_float - idx_segment_start) / sampling_frequency
                cut.logger.warning(f'Due to numerical precision when selecting the segment, the last sample chosen corresponds to a duration of {duration_real} and not {duration} but we keep the latter value as we need an integer...')
            #
            idx_segment_end = int((start_time + duration - hdf5_start_time) * sampling_frequency)
            # By convention due to fft routines used, `idx_segment_end` is not included
            # in the selected time segment; meaning the segment we select is said to have
            # last a time = `duration` but its corresponding time_domain_series will give:
            # `tds[-1] - tds[0] = duration - 1/sampling_frequency`
            desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end]
            # desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end+1]
            strain = bilby.gw.detector.strain_data.InterferometerStrainData(minimum_frequency=ifo.minimum_frequency)
            # import IPython; IPython.embed();sys.exit()
            strain.set_from_time_domain_strain(time_domain_strain=desired_hdf5_strain, sampling_frequency=sampling_frequency, duration=duration)
            # strain.low_pass_filter(filter_freq=ifo.minimum_frequency)
            ifo.strain_data = strain
            ifo.strain_data.start_time = start_time



            # SET THE PSDs
            ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_array=GWTC1_event_PSDs[:, i+1], frequency_array=GWTC1_event_PSDs[:, 0])

            ifo.psd_array = ifo.power_spectral_density_array
            ifo.inverse_psd_array = 1 / ifo.psd_array


        # WAVEFORM GENERATION
        waveform_arguments = {}
        waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
        waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
        waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
        waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']

        starting_point_parameters = trigger_parameters.copy()

        del hdf5_strain, desired_hdf5_strain, GWTC1_event_PSDs

    elif opts.inj_file is not None:
        # SET THE INJECTION PARAMETERS AND THE ANALYSIS PARAMETERS
        injection_parameters, ext_analysis_dict = set_inj.get_inj_parameters_and_analysis_dict(opts.inj_file, **config_dict['analysis'])
        inj_dict_formatted = cut.dictionary_to_formatted_string(injection_parameters)
        cut.logger.info(f'Injected parameters derived from {opts.inj_file} are: {inj_dict_formatted}')
        pu.save_dict_to_json(ext_analysis_dict, outdir + 'ext_analysis_dict.json')
        ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)
        cut.logger.info(f'Analysis parameters derived from the component masses are: {ext_anal_dict_formatted}')

        start_time = ext_analysis_dict['start_time']

        # INITIALIZE THE THREE INTERFEROMETERS
        # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
        interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])
        # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
        for ifo in interferometers:
            ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
            ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']
            # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
            # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
            # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
            ifo.strain_data.duration = ext_analysis_dict['duration']
            ifo.strain_data.sampling_frequency = ext_analysis_dict['sampling_frequency']
            ifo.strain_data.start_time = start_time


        # SET THEIR POWER SPECTRAL DENSITIES
        # This function does not interpolate the psd
        set_psds.main(interferometers, opt_psd=config_dict['analysis']['psd'], sampling_frequency=ext_analysis_dict['sampling_frequency'], duration=ext_analysis_dict['duration'], start_time=start_time)


        # SET NOISE STRAIN DATA FROM PSD
        interferometers.set_strain_data_from_power_spectral_densities(
            sampling_frequency=ext_analysis_dict['sampling_frequency'],
            duration=ext_analysis_dict['duration'],
            start_time=start_time)

        # WAVEFORM GENERATION
        waveform_arguments = {}
        waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
        waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
        waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
        waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']
        injection_waveform_generator = bilby.gw.WaveformGenerator(
            duration=interferometers.duration,
            sampling_frequency=interferometers.sampling_frequency,
            frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
            waveform_arguments=waveform_arguments
            )

        # INJECT TEMPLATE INTO NOISE STRAIN DATA
        # Add the template signal to each detector's strain data
        # for i in range(len(interferometers)):
            # interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
            # interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain
        interferometers.inject_signal(parameters=injection_parameters, waveform_generator=injection_waveform_generator)

        # # COMPUTE AND PRINT SNR
        interferometers.optimal_snr()
        interferometers.matched_filter_snr()
        interferometers.log_likelihood()

        starting_point_parameters = injection_parameters.copy()
    elif opts.trigger_time is not None:
        pass

    starting_point_parameters['geocent_duration'] = starting_point_parameters['geocent_time'] - start_time

    for ifo in interferometers:
        ifo.fd_strain = ifo.strain_data.frequency_domain_strain







    # SET THE LIKELIHOOD OBJECT, two different cases whether ROQ basis is used or not
    if config_dict['analysis']['roq']:
        if ext_analysis_dict['approximant'] != 'IMRPhenomPv2':
            raise ValueError("Need to set approximant to IMRPhenomPv2 to work with the ROQ basis, approximant here is {}".format(ext_analysis_dict['approximant']))

        scale_factor = ext_analysis_dict['roq']['scale_factor']
        roq_directory = ext_analysis_dict['roq']['directory']
        # Load in the pieces for the linear part of the ROQ. Note you will need to
        freq_nodes_linear = np.load(roq_directory + "fnodes_linear.npy") * scale_factor
        # Load in the pieces for the quadratic part of the ROQ
        freq_nodes_quadratic = np.load(roq_directory + "fnodes_quadratic.npy") * scale_factor

        # Load the parameters describing the valid parameters for the basis.
        params = ext_analysis_dict['roq']['rescaled_params'].copy()

        outdir_psd = init.get_outdir_psd(config_dict['analysis']['psd'])
        roq_outdir = '../__ROQ_weights/' + data_name + '/' + outdir_psd
        pu.mkdirs(roq_outdir)
        weights_file_path_no_format = roq_outdir + '{:.0f}Hz_{:.0f}Hz_{:.0f}Hz_{:.0f}s_weights'.format(params['flow'], ext_analysis_dict['maximum_frequency_ifo'], ext_analysis_dict['maximum_frequency_injected_waveform'], params['seglen'])
        weight_format = 'npz'
        weights_file_path = weights_file_path_no_format + f'.{weight_format}'

        frequency_domain_source_model = bilby.gw.source.binary_neutron_star_roq
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
        if opts.event == 'GW150914':
            frequency_domain_source_model = bilby.gw.source.binary_black_hole_roq
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
        # make waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=frequency_domain_source_model,
            waveform_arguments=dict(
                frequency_nodes_linear=freq_nodes_linear,
                frequency_nodes_quadratic=freq_nodes_quadratic,
                reference_frequency=ext_analysis_dict['reference_frequency'], waveform_approximant=ext_analysis_dict['approximant']),
            parameter_conversion=parameter_conversion)

        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        priors = bilby.gw.prior.BNSPriorDict()
        # for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'phi_12', 'phi_jl', 'luminosity_distance']:
        for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'luminosity_distance']:
            priors[key] = starting_point_parameters[key]
        for key in ['mass_1', 'mass_2']:
            priors[key].minimum = max(priors[key].minimum, params['compmin'])
        priors['chirp_mass'] = bilby.core.prior.Constraint(name='chirp_mass', minimum=float(params['chirpmassmin']), maximum=float(params['chirpmassmax']))
        priors['mass_ratio'] = bilby.core.prior.Constraint(0.125, 1, name='mass_ratio')
        priors['geocent_time'] = bilby.core.prior.Uniform(starting_point_parameters['geocent_time'] - 0.1, starting_point_parameters['geocent_time'] + 0.1, latex_label='$t_c$', unit='s')

        # Update user's parameters boundaries with the roq boundaries
        # if they are more restrictive
        from bilby.gw.conversion import component_masses_to_chirp_mass, chirp_mass_and_mass_ratio_to_total_mass
        new_bounds = sampler_dict['parameters_boundaries'].copy()
        if 'chirp_mass' in sampler_dict['search_parameter_keys']:
            # If mass boundaries set by user with mass_1 and mass_2
            # convert these into chirp boundaries
            if 'chirp_mass' not in new_bounds.keys():
                chirp_mass_min_user = component_masses_to_chirp_mass(new_bounds['mass_1'][0], new_bounds['mass_2'][0])
                chirp_mass_max_user = component_masses_to_chirp_mass(new_bounds['mass_1'][1], new_bounds['mass_2'][1])
            # Then take the most restrictive between user bounds and ROQ
                chirp_mass_min = max(chirp_mass_min_user, params['chirpmassmin'])
                chirp_mass_max = min(chirp_mass_max_user, params['chirpmassmax'])
                new_bounds['chirp_mass'] = [chirp_mass_min, chirp_mass_max]
                if chirp_mass_min_user != chirp_mass_min or chirp_mass_max != chirp_mass_max_user:
                    cut.logger.info(f'Chirp mass boundaries from user was: [{chirp_mass_min_user}, {chirp_mass_max_user}], with the ROQ constraints it is now: [{chirp_mass_min}, {chirp_mass_max}]')
        # Now we want to know the new bounds for mass_1 and mass_2 as they will
        # change the bounds for the reduced_mass. For that we will first
        # derive the bounds for the mass ratio:
        if 'mass_ratio' not in new_bounds.keys():
            mass_ratio_max_user = new_bounds['mass_1'][1] / new_bounds['mass_2'][0]
            new_bounds['mass_ratio'] = [1, min(mass_ratio_max_user, params['qmax'])]
            compmassmin, _ = paru.Mc_and_q_to_m1_and_m2(chirp_mass_min, 1)
            _, compmassmax = paru.Mc_and_q_to_m1_and_m2(chirp_mass_max, 1)
            compmassmin = max(compmassmin, params['compmin'])
            new_bounds['mass_1'] = [compmassmin, compmassmax]
            new_bounds['mass_2'] = [compmassmin, compmassmax]
            cut.logger.info(f'New bounds for component masses are: [{compmassmin}, {compmassmax}]')

        sampler_dict['parameters_boundaries'] = new_bounds.copy()

        if os.access(weights_file_path, os.W_OK):
            # load the weights from the file
            likelihood = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                weights=weights_file_path, priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])
        else:
            # Load in the pieces for the linear part of the ROQ. Note you will need to
            basis_matrix_linear = np.load(roq_directory + "B_linear.npy").T
            # Load in the pieces for the quadratic part of the ROQ
            basis_matrix_quadratic = np.load(roq_directory + "B_quadratic.npy").T

            # Redoing here a bit what's already being done in set_inj because `bilby.gw.likelihood.ROQGravitationalWaveTransient()` does not accept dictionnary for roq_params...
            roq_params_ndarray = np.genfromtxt(ext_analysis_dict['roq']['params_path'], names=True)
            roq_params_ndarray = roqu.rescale_params(roq_params_ndarray, scale_factor)
            # roq_params_ndarray = None

            likelihood = gwlh.ROQLikelihood(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic,
                priors=priors, roq_params=roq_params_ndarray)

            # remove the basis matrices as these are big for longer bases
            del basis_matrix_linear, basis_matrix_quadratic

            # write the weights to file so they can be loaded multiple times
            likelihood.save_weights(weights_file_path_no_format, format=weight_format)
    else:
        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        # priors = bilby.gw.prior.BNSPriorDict(filename='../gw/prior_files/GW170817_LALInf_IMRPhenomD.prior')
        prior_file = config_dict['parameters']['prior_file']
        priors = bilby.gw.prior.BNSPriorDict(filename=prior_file)

        frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
        if opts.event == 'GW150914':
            frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters
            # priors = bilby.gw.prior.BBHPriorDict()
            priors = bilby.gw.prior.BBHPriorDict(filename='GW150914.prior')
        # make waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=frequency_domain_source_model,
            waveform_arguments=waveform_arguments,
            parameter_conversion=parameter_conversion)



        # SET THE PRIORS AND THEIR GRADIENTS
        priors = prior.fill_in_input_priors(priors, starting_point_parameters['geocent_time'], start_time)
        traj_priors = prior.GWTrajPriors(
            priors=priors,
            search_parameter_keys=sampler_dict['search_parameter_keys'],
            dict_trajectory_functions_str=sampler_dict['trajectory_functions']
            )
        traj_prior_gradients = pg.GWTrajPriorGradients(
            traj_priors=traj_priors,
            dict_parameters_offsets=sampler_dict['parameters_offsets']
            )


        likelihood = gwlh.Likelihood(
            interferometers=interferometers,
            waveform_generator=search_waveform_generator,
            priors=priors,
            phase_marginalization=config_dict['analysis']['phase_marginalization'])

    likelihood.parameters.update(starting_point_parameters)



    # COMPUTE AND PRINT SNR
    snr_dict = likelihood.calculate_network_snrs()
    snr_dict_formatted = cut.dictionary_to_formatted_string(snr_dict, decimal_format=2)
    cut.logger.info(f'SNR at starting point: {snr_dict_formatted}')




    # SET THE LIKELIHOOD GRADIENT OBJECT AND COMPUTE THE GRADIENT AT POINT OF INJECTION
    likelihood_gradient = lg.GWTransientLikelihoodGradient(
        likelihood=likelihood,
        traj_priors=traj_priors,
        dict_trajectory_functions_str=sampler_dict['trajectory_functions'],
        dict_parameters_offsets=sampler_dict['parameters_offsets'],
        # dict_diff_method={'chi_1': 'forward', 'chi_2': 'forward', 'lambda_1': 'forward', 'lambda_2': 'forward'}
        dict_diff_method={'chi_1': 'forward', 'chi_2': 'forward', 'lambda_1': 'forward', 'lambda_2': 'forward'}
        )
    dict_diff_method_str = cut.dictionary_to_formatted_string(likelihood_gradient.dict_diff_method)
    cut.logger.info(f'Differencing method used for gradients of the log-likelihood are: {dict_diff_method_str}')

    # SET THE HMC SAMPLER
    sampler = core.sampler.GWHMCSampler(
        starting_point_parameters=starting_point_parameters,
        outdir=outdir,
        likelihood_gradient=likelihood_gradient,
        traj_prior_gradients=traj_prior_gradients,
        search_parameter_keys_local_fit=sampler_dict['search_parameter_keys_local_fit'],
        n_traj_hmc_tot = config_dict['hmc']['n_traj_hmc_tot'],
        n_traj_fit = config_dict['hmc']['n_traj_fit'],
        n_traj_for_this_run = config_dict['hmc']['n_traj_for_this_run'],
        n_fit_1 = config_dict['hmc']['n_fit_1'],
        n_fit_2 = config_dict['hmc']['n_fit_2'],
        length_num_traj = config_dict['hmc']['length_num_traj'],
        epsilon0 = config_dict['hmc']['epsilon0'],
        n_sis = config_dict['hmc']['n_sis']
        )


    dir_central_diff = '/Users/marcarene/projects/python/gwhmc/__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/_40Hz/3000_3000_2000_200_200_0.005_central/phase_marginalization/IMRPhenomD_NRTidal/sampler_state'
    cut.logger.info(f'Importing positions from central diff run...')
    x_data_central = np.loadtxt(dir_central_diff + '/qpos_traj_phase1.dat')
    cut.logger.info(f'Importing gradients from central diff run...')
    y_data_central = np.loadtxt(dir_central_diff + '/dlogL_traj_phase1.dat')
    idx_train_set = int(len(x_data_central)/2)
    # idx_train_set = 10
    x_train_central = x_data_central[:idx_train_set]
    y_train_central = y_data_central[:idx_train_set]
    x_val = x_data_central[-idx_train_set:]
    y_val = y_data_central[-idx_train_set:]
    validation_data = (x_val, y_val)


    parameters_to_analyse = ['chi_1', 'chi_2', 'lambda_1', 'lambda_2']


    file_path_cf = outdir + 'data' + f'/dlogL_num_central_forward_{len(y_train_central)}train_cc.dat'
    file_path_ff = outdir + 'data' + f'/dlogL_num_forward_forward_{len(y_train_central)}train_cc.dat'

    if os.path.exists(file_path_cf):
        y_train_central_forward = np.loadtxt(file_path_cf)
        y_train_forward_forward = np.loadtxt(file_path_ff)
    else:
        dlogL_pos_forward_all = []
        nb_points_tested = len(x_train_central)
        cut.logger.info(f"Starting to loop over the {nb_points_tested} qpos points...")
        # Loop over the selected points to compare forward gradient wrt central
        for idx, qpos_traj in enumerate(x_train_central):

            parameters = sampler.q_pos_traj_to_parameters_dict(qpos_traj)

            likelihood_gradient.likelihood.parameters.update(parameters)
            dlogL_pos_forward = likelihood_gradient.calculate_dlogL_np(search_parameter_keys=parameters_to_analyse)

            dlogL_pos_forward_all.append(dlogL_pos_forward.tolist())
            if idx % int(nb_points_tested/10) == 0 and idx != 0:
                cut.logger.info(f'{idx} gradients computed...')

        dlogL_pos_forward_all = np.asarray(dlogL_pos_forward_all)

        y_train_central_forward = y_train_central.copy()
        y_train_central_forward[:,-2] = dlogL_pos_forward_all[:,-2]
        y_train_central_forward[:,-1] = dlogL_pos_forward_all[:,-1]


        y_train_forward_forward = y_train_central_forward.copy()
        y_train_forward_forward[:,-4] = dlogL_pos_forward_all[:,-4]
        y_train_forward_forward[:,-3] = dlogL_pos_forward_all[:,-3]

        np.savetxt(outdir + 'data' + f'/dlogL_num_central_forward_{len(y_train_central_forward)}train_cc.dat', y_train_central_forward)
        np.savetxt(outdir + 'data' + f'/dlogL_num_forward_forward_{len(y_train_forward_forward)}train_cc.dat', y_train_forward_forward)

    suffixes = ['cc', 'cf', 'ff']
    y_train_list = [y_train_central, y_train_central_forward, y_train_forward_forward]
    for i, y_train in enumerate(y_train_list):
        cut.logger.info(f'\n\nCASE {suffixes[i]}')
        fit_method = fnr.CubicFitRegressor()
        outdir_method = outdir + suffixes[i]
        save_dir = outdir_method + f'/{fit_method.save_dir_suffix}'
        if os.path.exists(save_dir):
            cut.logger.info(f'Loading the {fit_method.nick_name} fit from saved directory...')
            fit_method.load(outdir_method)
        else:
            fit_method.fit(x_train_central, y_train)
            fit_method.save(outdir_method)
        dlogL_latex_labels = CONST.get_dlogL_latex_labels(sampler.search_trajectory_parameters_keys)
        plots.make_regression_plots(fit_method, x_train_central, y_train, outdir_method, validation_data=validation_data, y_labels=dlogL_latex_labels)

    import IPython; IPython.embed(); sys.exit()
