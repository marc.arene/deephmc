import numpy as np
import matplotlib.pyplot as plt
import sys
import corner

dir = '../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16_17_18/PSD_1/1300_1300_2000_200_200_0.005_forward/phase_marginalization/IMRPhenomD_NRTidal/sampler_state/global_fit'

dlogL_train = np.loadtxt(dir + '/dlogL_trained.dat')
qpos_train = np.loadtxt(dir + '/qpos_trained.dat')
dlogL_val = np.loadtxt(dir + '/new_dlogL_for_trained.dat')
qpos_val = np.loadtxt(dir + '/new_qpos_for_trained.dat')

label_fontsize = 64
tick_fontsize = 32
label_kwargs=dict(fontsize=label_fontsize)
defaults_kwargs = dict(
    bins=50, smooth=0.9, label_kwargs=label_kwargs,
    title_kwargs=label_kwargs,
    plot_density=False, plot_datapoints=True, fill_contours=True,
    max_n_ticks=3)

dlogL_train = dlogL_train[:, :3]
qpos_train = qpos_train[:, :3]
dlogL_val = dlogL_val[:, :3]
qpos_val = qpos_val[:, :3]

fig, axes = plt.subplots(dlogL_train.shape[1], dlogL_train.shape[1], figsize=(50,50))
# import IPython; IPython.embed(); sys.exit()
for i in range(axes.shape[0]):
    for j in range(axes.shape[1]):
        corner.hist2d(qpos_train[:, j], dlogL_train[:, i], ax=axes[i, j], color='C0', **defaults_kwargs)
        corner.hist2d(qpos_val[:, j], dlogL_val[:, i], ax=axes[i, j], color='C1', **defaults_kwargs)
        # import IPython; IPython.embed(); sys.exit()
        if i == axes.shape[0]-1:
            axes[i, j].set_xlabel(f'{j}', **label_kwargs)
            for tick in axes[i, j].xaxis.get_major_ticks():
                tick.label.set_fontsize(tick_fontsize)
            # import IPython; IPython.embed(); sys.exit()
        else:
            axes[i, j].get_xaxis().set_visible(False)
        if j == 0:
            axes[i, j].set_ylabel(f'{i}', **label_kwargs)
            axes[i, j].set_xlabel(f'{j}', **label_kwargs)
            for tick in axes[i, j].yaxis.get_major_ticks():
                tick.label.set_fontsize(tick_fontsize)
        else:
            axes[i, j].get_yaxis().set_visible(False)

fig.tight_layout()
fig.subplots_adjust(top=0.88) # Needed so that fig.title and ax.title don't overlap
fig.suptitle(f'dlogL vs qpos', fontsize=int(label_fontsize*1.2))
fig.text(x=0.5, y=0.92, s=f'train set = {len(qpos_train):,}', color='C0', horizontalalignment='center', fontsize=label_fontsize)
fig.text(x=0.5, y=0.90, s=f'val set = {len(qpos_val):,}', color='C1', horizontalalignment='center', fontsize=label_fontsize)

filepath = dir + f'dlogL_vs_qpos_{len(dlogL_train)}train_{len(dlogL_val)}val.png'
fig.savefig(filepath)
sys.exit()
# import IPython; IPython.embed(); sys.exit()

#
# plt.hist2d(qpos, dlogL, bins=100)
# cbar = plt.colorbar()
# cbar.ax.set_ylabel('Counts')
# plt.show()
