# get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
# import seaborn as sns; sns.set()
import numpy as np
import pandas as pd
from configparser import ConfigParser

from sklearn import neighbors, svm, ensemble, linear_model, metrics, model_selection, preprocessing

import IPython as ip
import sys
sys.path.append('../')
import Library.CONST as CONST
import Library.python_utils as pu

class CubicFitRegressor:

    def __init__(self, pseudo_inverse=None):
        self.pseudo_inverse = pseudo_inverse
        self.cubic_fit_coeff = None

    def nb_of_cubic_coeff(self, n):
        """
        Returns the number of coefficients needed to define a cubic function which has `n` variables / input parameters.

        Parameters:
        ----------
        n : int
            number of parameters
        """

        nb_of_fit_coeff = int(1 + n + n*(n+1)/2 + n*(n+1)*(n+2)/6)
        return nb_of_fit_coeff

    def cubic_jacobian(self, vector_x):
        """
        Expression of the cubic jacobian used in QR routine

        Parameters
        ----------
        x : array_like.
            Position array in parameter space.
        m : integer.
            Number of parameters.
        Returns
        -------
        cubic_jacobian_line : array_like.
            Vector containing the polynomial values of the cubic fit at position x. If x is of length m, then cubic_jacobian_line should be of length 1 + m + m(m+1)/2 + m(m+1)(m+2)/6. 220 for m = 9.
        """
        l = 0
        nb_dimensions = len(vector_x)
        nb_of_fit_coeff = self.nb_of_cubic_coeff(nb_dimensions)
        cubic_jacobian_line = [np.longdouble(0) for i in range(nb_of_fit_coeff)]
        for i in range(nb_dimensions):
            cubic_jacobian_line[l] = vector_x[i]
            l+=1
        for i in range(nb_dimensions):
            for j in range(i, nb_dimensions):
                cubic_jacobian_line[l] = vector_x[i] * vector_x[j]
                l+=1
        for i in range(nb_dimensions):
            for j in range(i, nb_dimensions):
                for k in range(j, nb_dimensions):
                    cubic_jacobian_line[l] = vector_x[i] * vector_x[j] * vector_x[k]
                    l+=1
        cubic_jacobian_line[l] = 1.0
        return cubic_jacobian_line

    def qr_decompose(self, x_data):
        n_pt_fit = x_data.shape[0]
        cubic_jacobian_matrix = []
        for i in range(n_pt_fit):
            cubic_jacobian_line = self.cubic_jacobian(x_data[i]) # has shape (220,)
            cubic_jacobian_matrix.append(cubic_jacobian_line)
        cubic_jacobian_matrix = np.asarray(cubic_jacobian_matrix, dtype=np.float64)
        q, r = np.linalg.qr(cubic_jacobian_matrix)
        return q, r

    def compute_pseudo_inverse(self, x_data):
        q, r = self.qr_decompose(x_data)
        self.pseudo_inverse = np.matmul(np.linalg.inv(r), q.T, dtype=np.longdouble)
        return self.pseudo_inverse

    def fit(self, x_data, y_data):
        self.cubic_fit_coeff = CubicFitCoeff()
        if self.pseudo_inverse is None:
            self.compute_pseudo_inverse(x_data)
        self.cubic_fit_coeff.coeff = np.matmul(self.pseudo_inverse, y_data, dtype=np.longdouble)
        # fit_coeff = pseudo_inverse @ y_data # has shape (220, 9)
        return self.cubic_fit_coeff

class CubicFitCoeff:

    def __init__(self, coeff=None):
        self.coeff = coeff

    def predict_single_value(self, vector_x):
        y_predicted = np.longdouble(0.0)
        l = 0
        n_param = len(vector_x)
        for i in range(n_param):
        	y_predicted += self.coeff[l] * vector_x[i]
        	l += 1
        for i in range(n_param):
        	for j in range(i,n_param):
        		y_predicted += self.coeff[l] * vector_x[i] * vector_x[j]
        		l += 1
        for i in range(n_param):
        	for j in range(i,n_param):
        		for k in range(j,n_param):
        			y_predicted += self.coeff[l] * vector_x[i] * vector_x[j] * vector_x[k]
        			l += 1
        y_predicted += self.coeff[l]
        return y_predicted

    def predict(self, x_data):
        y_predicted = []
        for i in range(x_data.shape[0]):
            y_predicted.append(self.predict_single_value(x_data[i]))
        return np.asarray(y_predicted)




def plot_regression(data_predicted, data_true, ax=None, label='', data_label='data'):
    """Summary plot of regression performance.
    """
    ax = ax or plt.gca()
    ax.scatter(data_true, data_predicted, lw=0, alpha=0.5, s=10)
    ax.set_xlabel('True ' + data_label)
    ax.set_ylabel('Predicted ' + data_label)
    x_min = data_true.min()
    x_max = data_true.max()
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(x_min, x_max)
    ax.plot([x_min, x_max], [x_min, x_max], 'r--')
    R2 = metrics.r2_score(data_true, data_predicted)
    ax.text(0.05, 0.9, f'{label}$R^2 = {R2:.2f}$', transform=ax.transAxes, fontsize=16, color='r')

def test_sk_regression(method, x_train_scaled, x_test_scaled, y_train_scaled, y_train, y_test, y_scaler, ax, y_label):
    # Fit normalized training dlogL.
    print('Fitting the model on train set...')
    fit = method.fit(x_train_scaled, y_train_scaled)
    # Get predicted dlogL for training qpos.
    print('Predicting train values...')
    breakpoint()
    y_train_predicted = y_scaler.inverse_transform(fit.predict(x_train_scaled))
    # Get predicted shear for test images.
    print('Predicting test values...')
    y_test_predicted = y_scaler.inverse_transform(fit.predict(x_test_scaled))
    # y_train = y_scaler.inverse_transform(y_train_scaled)
    # Plot results.
    # y_label=f'dlogL[{idx_dlogL_to_predict}]'
    # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
    plot_regression(y_train_predicted, y_train, label='train: ', data_label=y_label, ax=ax[0])
    plot_regression(y_test_predicted, y_test, label='test: ', data_label=y_label, ax=ax[1])
    plt.tight_layout()


if __name__=='__main__':

    from optparse import OptionParser
    # usage = """%prog [options]
    # Plots the logL as a function of the parameter defined by `pkey`, keeping the other constant."""
    parser=OptionParser()
    parser.add_option("-n", "--nb_traj_test", default=100, action="store", type="int", help="""Number of trajectories from phase1 to use for the test set.""")
    parser.add_option("--method", default='cubic', action="store", help="""Method of regression to use. RF, KN, cubic """)
    parser.add_option("--search_parameter_indices", default=None, action="store", help=""" Indices for which to plot dlogL. Default is all of those contained in the config.ini file. """)
    parser.add_option("--rescale", default=False, action="store_true", help=""" Wether to rescale the qpos and dlogL between -1 and -1 before doing the fit. """)
    parser.add_option("--outdir", default='../__output_data/GW170817/SUB-D_012345678/PSD_3/1501_1500_2000_200_200_0.005_dlogL1/IMRPhenomPv2_ROQ', action="store", type="string", help="""Output directory of the run we are interested in.""")
    (opts,args)=parser.parse_args()

    config_parser = ConfigParser()
    config_file_path = opts.outdir + '/config.ini'
    config_parser.read(config_file_path)
    config_dict = pu.config_parser_to_dict(config_parser)

    search_parameter_indices = [int(i) for i in list(config_dict['analysis']['search_parameter_indices'].split(','))]
    if opts.search_parameter_indices is None:
        parameter_indices_to_plot = search_parameter_indices.copy()
    else:
        parameter_indices_to_plot = opts.search_parameter_indices
    directory = opts.outdir + '/phase1'


    qpos_phase1_all = np.loadtxt(directory + '/pt_fit_phase1.dat')
    qpos_phase1 = qpos_phase1_all[:, search_parameter_indices]
    dlogL_phase1 = np.loadtxt(directory + '/dlogL_fit_phase1.dat')

    if False:
        # remove outliers of the data set
        print('Removing outliers from phase1 data...')
        from scipy import stats
        z = np.abs(stats.zscore(qpos_phase1))
        threshold = 2
        qpos_phase1 = qpos_phase1[(z < threshold).all(axis=1)]
        dlogL_phase1 = dlogL_phase1[(z < threshold).all(axis=1)]
        nb_outliers = np.where((z > threshold).any(axis=1))[0].shape[0]
        print(f'{nb_outliers} outliers removed.')

    if False:
        for idx_dlogL_to_predict in range(dlogL_phase1.shape[1]):
            plt.hist(dlogL_phase1[:, idx_dlogL_to_predict], bins=35)
            plt.legend(fontsize='large')
            plt.xlabel(f'dlogL[{idx_dlogL_to_predict}] magnitude')
            plt.show()
        sys.exit()

    # Create train-test sets on the position data
    qpos_train = qpos_phase1[:-200*opts.nb_traj_test, :]
    qpos_test = qpos_phase1[-200*opts.nb_traj_test:, :]

    if opts.rescale:
        qpos_scaler = preprocessing.StandardScaler().fit(qpos_train)
        qpos_train_scaled = qpos_scaler.transform(qpos_train)
        qpos_test_scaled = qpos_scaler.transform(qpos_test)

    # parameter_indices_to_plot = search_parameter_indices.copy()
    parameter_indices_to_plot = [0]
    dlogL_names = ['dlogL/d' + fpname for fpname in [CONST.TRAJ_PARAMETERS_KEYS[j] for j in parameter_indices_to_plot]]
    nb_dlogL_to_predict = len(parameter_indices_to_plot)

    fig, ax = plt.subplots(nb_dlogL_to_predict, 2, sharex=False, sharey=False, figsize=(10, 4 * nb_dlogL_to_predict))
    fig.text(x=0.4, y=0.95, s=f'{opts.method} fit - {search_parameter_indices} case', fontsize=15)
    fig.text(x=0.2, y=0.90, s=f'train set: {qpos_train.shape[0]} pts', fontsize=12)
    fig.text(x=0.7, y=0.90, s=f'test set: {qpos_test.shape[0]} pts', fontsize=12)

    if nb_dlogL_to_predict==1:
        ax = ax.reshape(1, -1)


    if opts.method == 'KN':
        method = neighbors.KNeighborsRegressor()
    if opts.method == 'RF':
        method = ensemble.RandomForestRegressor(random_state=123)
    if opts.method == 'GB':
        method = ensemble.GradientBoostingRegressor(random_state=123)
    if opts.method == 'cubic':
        print(f'Computing pseudo_inverse...')
        method_global = CubicFitRegressor()
        if opts.rescale:
            pseudo_inverse = method_global.compute_pseudo_inverse(qpos_train_scaled)
        else:
            pseudo_inverse = method_global.compute_pseudo_inverse(qpos_train)
    if opts.method == 'DNN':
        from keras.models import Sequential
        from keras.layers import Dense
        method = Sequential()
        # Informations about activation functions: https://missinglink.ai/guides/neural-network-concepts/7-types-neural-network-activation-functions-right/
        # For Keras: https://keras.io/activations/
        # from keras.activations import sigmoid
        # int(1 + n + n*(n+1)/2 + n*(n+1)*(n+2)/6)
        n = qpos_train.shape[1]
        method.add(Dense(units=n-1 , activation='relu', input_dim=n))
        method.add(Dense(units=n-1, activation='tanh'))
        # method.add(Dense(units=int(qpos_train.shape[1]/2), activation='relu'))
        method.add(Dense(units=1, activation='tanh'))
        # Compile the model/method
        # Cf https://keras.io/losses/ for loss functions available
        # from keras import losses
        # loss = keras.losses.logcosh(y_true, y_pred)
        loss = 'mse'
        # For a mean squared error regression problem
        method.compile(optimizer='rmsprop', loss=loss)


    for ii, idx_dlogL_to_predict in enumerate(parameter_indices_to_plot):
        print(f'\nidx_dlogL_to_predict = {idx_dlogL_to_predict}')
        # dlogL_param = dlogL_phase1[:, idx_dlogL_to_predict]

        # Create train-test set by randomly choosing amongst points in phase1
        # qpos_train, qpos_test, dlogL_train, dlogL_test = model_selection.train_test_split(qpos_phase1, dlogL_phase1, test_size=0.9, random_state=123)

        # Create train-test set by removing some of the last trajectories of phase1 for the test set
        dlogL_train = dlogL_phase1[:-200*opts.nb_traj_test, idx_dlogL_to_predict]
        dlogL_test = dlogL_phase1[-200*opts.nb_traj_test:, idx_dlogL_to_predict]

        if False:
            param_to_plot = 4
            plt.hist(qpos_train[:, param_to_plot], bins=35, label='Train')
            plt.hist(qpos_test[:, param_to_plot], bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Distribution of qpos[{param_to_plot}]')
            plt.show()

            plt.hist(qpos_train_scaled[:, param_to_plot], bins=35, label='Train')
            plt.hist(qpos_test_scaled[:, param_to_plot], bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Distribution of scaled qpos[{param_to_plot}]')
            plt.show()

        if opts.rescale:
            dlogL_train_scaler = preprocessing.StandardScaler().fit(dlogL_train.reshape(-1, 1))
            # the `.reshape(-1)` at the end is in order not to have a column vector but a flattened array as asked by the `method.fit()` function
            dlogL_train_scaled = dlogL_train_scaler.transform(dlogL_train.reshape(-1, 1)).reshape(-1)
            dlogL_test_scaled = dlogL_train_scaler.transform(dlogL_test.reshape(-1, 1)).reshape(-1)

        if False:
            plt.hist(dlogL_train, bins=35, label='Train')
            plt.hist(dlogL_test, bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'True dlogL[{idx_dlogL_to_predict}] magnitude')
            plt.show()

            plt.hist(dlogL_train_scaled, bins=35, label='Train')
            plt.hist(dlogL_test_scaled, bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Scaled true dlogL[{idx_dlogL_to_predict}] magnitude')
            plt.show()

        # method = neighbors.KNeighborsRegressor()
        # train_data=qpos_train_scaled
        # test_data=qpos_test_scaled
        # fit = method.fit(train_data, dlogL_train_scaled)
        # dlogL_train_predicted = dlogL_scaler.inverse_transform(fit.predict(train_data))
        # dlogL_test_predicted = dlogL_scaler.inverse_transform(fit.predict(test_data))
        #
        # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
        # data_label=f'dlogL[{idx_dlogL_to_predict}]'
        # plot_regression(dlogL_train_predicted, dlogL_train, label='train: ', data_label=data_label,  ax=ax[0])
        # plot_regression(dlogL_test_predicted, dlogL_test, label='test: ', data_label=data_label, ax=ax[1])
        # plt.tight_layout()
        if opts.method == 'cubic':
            method = CubicFitRegressor(pseudo_inverse)
        # test_sk_regression(method=method, x_train_scaled=qpos_train_scaled, x_test_scaled=qpos_test_scaled, y_train_scaled=dlogL_train_scaled, y_train=dlogL_train, y_test=dlogL_test, y_scaler=dlogL_train_scaler, ax=ax[ii,:], y_label=dlogL_names[ii])


        # Fit normalized training dlogL.
        if opts.rescale:
            print('Fitting the model on train set...')
            fit = method.fit(qpos_train_scaled, dlogL_train_scaled)
            # Get predicted dlogL for training qpos.
            print('Predicting train values...')
            if opts.method == 'DNN':
                dlogL_train_predicted_scaled = method.predict(qpos_train_scaled)
                dlogL_train_predicted = dlogL_train_scaler.inverse_transform(dlogL_train_predicted_scaled)
            else:
                dlogL_train_predicted_scaled = fit.predict(qpos_train_scaled)
                dlogL_train_predicted = dlogL_train_scaler.inverse_transform(dlogL_train_predicted_scaled)
            # Get predicted shear for test images.
            print('Predicting test values...')
            if opts.method == 'DNN':
                dlogL_test_predicted_scaled = method.predict(qpos_test_scaled)
                dlogL_test_predicted = dlogL_train_scaler.inverse_transform(dlogL_test_predicted_scaled)
            else:
                dlogL_test_predicted_scaled = fit.predict(qpos_test_scaled)
                dlogL_test_predicted = dlogL_train_scaler.inverse_transform(dlogL_test_predicted_scaled)
        else:
            print('Fitting the model on train set...')
            fit = method.fit(qpos_train, dlogL_train)
            # Get predicted dlogL for training qpos.
            print('Predicting train values...')
            if opts.method == 'DNN':
                dlogL_train_predicted = method.predict(qpos_train)
            else:
                dlogL_train_predicted = fit.predict(qpos_train)
            # Get predicted shear for test images.
            print('Predicting test values...')
            if opts.method == 'DNN':
                dlogL_test_predicted = method.predict(qpos_test)
            else:
                dlogL_test_predicted = fit.predict(qpos_test)

        plot_regression(dlogL_train_predicted, dlogL_train, label='train: ', data_label=dlogL_names[ii], ax=ax[ii, 0])
        plot_regression(dlogL_test_predicted, dlogL_test, label='test: ', data_label=dlogL_names[ii], ax=ax[ii, 1])
        plt.tight_layout()
        fig.subplots_adjust(top=0.85)
    if True:
        filename = f'/plots/{qpos_phase1.shape[0]}_regression_{opts.method}_TestedTrajs{opts.nb_traj_test}_dlogL' + ''.join([str(idx) for idx in parameter_indices_to_plot])
        fig.savefig(directory + filename)
    else:
        plt.show()


    # ip_shell = ip.get_ipython()
    # ip_shell.run_line_magic('time', 'test_sk_regression(method=method, x_train_scaled=qpos_train_scaled, x_test_scaled=qpos_test_scaled, y_train_scaled=dlogL_train_scaled, y_train=dlogL_train, y_test=dlogL_test, y_scaler=dlogL_scaler, ax=ax[ii,:], y_label=dlogL_names[ii])')
