# This script is meant to compare the influence of the offset used in the numerical derivative on trajectories that have revealed some instability.

import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../')
import bilby
import Library.bilby_utils as bilby_utils
import Library.CONST as CONST
import Library.sigpar_utils as su
import Library.param_utils as paru
from Library.param_utils import Mc_and_mu_to_M_eta_m1_and_m2
# import Codes.set_injection_parameters as set_inj

import GW170817_parameters
import GW170817_set_psds
import GW170817_waveform_three_detectors
import GW170817_logL_snr
import GW170817_set_strain_data_from_psd
import GW170817_dlogL

def fparam_ranges(parameters, param_index):
    """
    Returns the ranges for each parameter in which one can vary a parameter while keeping the other fixed at the position defined by the input `parameters`. When the bound is infinite we stop it at...
    """
    Mc_min =  parameters['reduced_mass'] / (0.2495**(2/5))
    Mc_max =  parameters['reduced_mass'] / (0.248**(2/5))
    mu_max = 0.25**(2/5) * parameters['chirp_mass']
    mu_min = 0.2435**(2/5) * parameters['chirp_mass']

    ranges = [(-1,1), (0, 2*np.pi), (0, 2*np.pi), (54.55, 59), (np.log(Mc_min), np.log(Mc_max)), (np.log(mu_min), np.log(mu_max)), (-1, 0), (2, 4.5), (4.0413, 4.0418)]

    return ranges[param_index]

def main(param_index, parameters, interferometers, template_ifos, start_time, minimum_frequency, generate_from_bilby, nb_points, whole_range, offsets):
    offset = sampler_dict['parameter_offsets'][param_index]
    step = offset
    if step == 0: # ie analytical derivative like for ln(D)
        step = 1e-5

    q_pos = paru.injection_parameters_to_q_pos(parameters, parameters['meta']['start_time'])

    f_q_pos_pindex_inj = CONST.PARAMETERS_FUNCTIONS[param_index](q_pos[param_index]) # does not work for phase and lntc...
    # if param_index in [1,8]:
    #     print("PROBLEM: can't do plots for phic and lntc for the moment because I need to write special code for them....")
    #     sys.exit(1)
    if whole_range:
        f_q_pos_pindex_min, f_q_pos_pindex_max = fparam_ranges(parameters, param_index)
    else:
        f_q_pos_pindex_max = f_q_pos_pindex_inj + nb_points/2 * step
        f_q_pos_pindex_min = f_q_pos_pindex_inj - nb_points/2 * step


    f_q_pos_pindex_linspace = np.linspace(f_q_pos_pindex_min, f_q_pos_pindex_max, nb_points)

    logL_list = []
    for f_q_pos_pindex in f_q_pos_pindex_linspace:
        q_pos[param_index] = CONST.PARAMETERS_INVERSE_FUNCTIONS[param_index](f_q_pos_pindex)
        parameters = paru.q_pos_to_dictionary(q_pos, start_time)
        if param_index in [4,5]:
            parameters['total_mass'], parameters['symmetric_mass_ratio'], parameters['mass_1'], parameters['mass_2'] = Mc_and_mu_to_M_eta_m1_and_m2(parameters['chirp_mass'], parameters['reduced_mass'])
        # parameters.update({CONST.param_dict_keys[param_index]: param_inverse_functions[param_index](f_q_pos_pindex)})

        template_ifos = GW170817_waveform_three_detectors.main(parameters, start_time, minimum_frequency, interferometers, generate_from_bilby, False)
        logL = bilby_utils.loglikelihood(template_ifos, interferometers)

        logL_list.append(logL)

    dlogL_list_offsets = []
    for offset in offsets:
        dlogL_list_offset = []
        for f_q_pos_pindex in f_q_pos_pindex_linspace:
            q_pos[param_index] = CONST.PARAMETERS_INVERSE_FUNCTIONS[param_index](f_q_pos_pindex)
            parameters = paru.q_pos_to_dictionary(q_pos, start_time)
            if param_index in [4,5]:
                parameters['total_mass'], parameters['symmetric_mass_ratio'], parameters['mass_1'], parameters['mass_2'] = Mc_and_mu_to_M_eta_m1_and_m2(parameters['chirp_mass'], parameters['reduced_mass'])
            sampler_dict['parameter_offsets'][param_index] = offset
            dlogL = GW170817_dlogL.main(template_ifos, parameters, start_time, minimum_frequency, interferometers, generate_from_bilby, False)
            dlogL_list_offset.append(dlogL[param_index])

        dlogL_list_offsets.append(dlogL_list_offset)


    return f_q_pos_pindex_linspace, logL_list, dlogL_list_offsets, f_q_pos_pindex_inj


if __name__=='__main__':

    from optparse import OptionParser
    usage = """%prog [options]
    Plots the logL and its derivative with respect to the chosen parameter."""
    parser=OptionParser(usage)
    parser.add_option("--psd", default=1, action="store", type="int", help="""1: sets the PSDs from official GWTC-1 open PSD data; 2: computes PSDs using Welch methods from gwosc open strain data; 3: uses bilby's analytical pre-stored PSD files.""", metavar="INT from {1,2,3}")
    parser.add_option("--hmc", default=True, action="store_false", help="""Waveforms are generated using Ed and Yann's TF2 code, default is to generate them using bilby.lalsim""", dest='generate_from_bilby')
    # parser.add_option("--offset", default=None,action="store", type="float", help="""offset used to compute dlogL but also stepsize between plotted points around the injeceted value.""")
    parser.add_option("--new", default=False, action="store_true", help="""New noise realisation from PSDs is generated""", dest='new_noise_realisation_from_psd')
    parser.add_option("--whole_range", default=False, action="store_true", help="""Plots on a predefined range that should give a good view of the logL shape overall""")
    parser.add_option("-n", "--nb_points", default=500, action="store", type="int", help="""Number of points to plot. Default is 100""", metavar="INT")
    parser.add_option("-i", "--p_index", default=None, action="store", type="int", help="""Index of the parameter to make the plot on. Default is the value stored in config.py""", metavar="INT")
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")
    # parser.add_option("-p", "--param_index", default=6, action="store", type="int", help="""Index of the parameter which will be plotted""", metavar="INT from {0,1,2,3,4,5,6,7,8}")
    (opts,args)=parser.parse_args()

    if opts.p_index is None:
        param_index = sampler_dict['search_parameter_indices'][0]
        sampler_dict['search_parameter_indices'] = [param_index]
    else:
        param_index = opts.p_index
        sampler_dict['search_parameter_indices'] = [param_index]

    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)


    # offsets = [1e-6]
    offsets = [1e-4, 1e-5, 1e-6, 1e-7]

    # import IPython; IPython.embed(); sys.exit(1)
    # SET THE INJECTION PARAMETERS
    injection_parameters = GW170817_parameters.main()
    minimum_frequency = injection_parameters['meta']['minimum_frequency']
    duration = injection_parameters['meta']['duration']
    sampling_frequency = injection_parameters['meta']['sampling_frequency']
    f_high = injection_parameters['meta']['f_high']
    start_time = injection_parameters['meta']['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    interferometers = bilby.gw.detector.InterferometerList(['H1', 'L1', 'V1'])
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    GW170817_set_psds.main(interferometers, opt_psd=opts.psd, sampling_frequency=sampling_frequency, duration=duration, start_time=start_time)

    # SET NOISE STRAIN DATA FROM PSD
    GW170817_set_strain_data_from_psd.main(interferometers, injection_parameters, sampling_frequency, duration, start_time, opts.psd, opts.new_noise_realisation_from_psd)

    # WAVEFORM GENERATION
    injection_parameters['meta']['maximum_frequency_generated_waveform'] = injection_parameters['meta']['maximum_frequency_injected_waveform']
    template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    for i in range(len(interferometers)):
        interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain

    # COMPUTE  AND PRINT SNR
    # Generate a template up to frequency of analysis
    injection_parameters['meta']['maximum_frequency_generated_waveform'] = injection_parameters['meta']['maximum_frequency_search_waveform']
    template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, minimum_frequency, interferometers)
    logLBest, mf_snr_best, opt_snr_Best = main(template_ifos, interferometers, True)

    # This copy is here just because for the moment I will keep manipulating 3 separate variables as originally done in the hmc
    signal1 = interferometers[0].strain_data.frequency_domain_strain.copy()
    signal2 = interferometers[1].strain_data.frequency_domain_strain.copy()
    signal3 = interferometers[2].strain_data.frequency_domain_strain.copy()

    # COMPUTE dlogL and print it to check with the plots
    GW170817_dlogL.main(template_ifos, injection_parameters, start_time, minimum_frequency, interferometers, opts.generate_from_bilby, True)

    f_q_pos_pindex_linspace, logL_list, dlogL_list_offsets, f_q_pos_pindex_inj =  main(param_index, injection_parameters, interferometers, template_ifos, start_time, minimum_frequency, opts.generate_from_bilby, opts.nb_points, opts.whole_range, offsets)


    # import IPython; IPython.embed();
    # PLOT
    # offset = sampler_dict['parameter_offsets'][param_index]

    from matplotlib import rc

    plt.rcParams['text.usetex'] = False

    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'

    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'

    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10

    if opts.generate_from_bilby:
        title_prefix = '(bilby) '
    else:
        title_prefix = '(hmc) '

    fig= plt.figure(figsize=(16,8))
    # fig, axs = plt.subplots(2, 1, figsize=(16,8), gridspec_kw = {'height_ratios': [10, 1]})

    # fig.suptitle('logL and dlogL computed with a step size of {:.0e}'.format(offset))
    ax0 = plt.subplot(211)
    ax0.plot(f_q_pos_pindex_linspace, logL_list, linewidth=0.5, color='green', label='logL')
    # import IPython; IPython.embed()
    ax0.axvline(x=f_q_pos_pindex_inj, linewidth=0.5, color='black', label='injected value')
    ax0.set_xlabel('{}'.format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    ax0.set_ylabel('logL')
    ax0.set_title(title_prefix + 'logL')
    # ax0.legend(loc=(0.85,0.9))
    ax0.legend()

    ax1 = plt.subplot(212)
    for i, offset in enumerate(offsets):
        ax1.plot(f_q_pos_pindex_linspace, dlogL_list_offsets[i], linewidth=0.5, label='offset = {}'.format(offset))
    ax1.axvline(x=f_q_pos_pindex_inj, linewidth=0.5, color='black', label='injected value')
    ax1.set_xlabel('{}'.format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    ax1.set_ylabel('dlogL/d{}'.format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    ax1.set_title(title_prefix + 'dlogL/d{}'.format(CONST.TRAJ_PARAMETERS_KEYS[param_index]))
    # ax1.tick_params('y', colors='red')
    # ax1.legend(loc=(0.85,0.85))
    ax1.legend()

    # axs[0].plot(f_q_pos_pindex_linspace, logL_list, linewidth=1, color='green', label='logL')
    # axs[0].axvline(x=f_q_pos_pindex_inj, linewidth=2, color='black')
    # axs[0].set_xlabel('sin(dec)')
    # axs[0].set_ylabel('logL')
    # axs[0].set_title('logL and dlogL computed with a step size of {:.0e}'.format(offset))
    #
    # ax01 = axs[0].twinx()
    # ax01.plot(f_q_pos_pindex_linspace, dlogL_list, linewidth=1, color='red', label='dlogL')
    # ax01.set_ylabel('dlogL', color='red')
    # ax01.tick_params('y', colors='red')


    # axs[1].plot(f_q_pos_pindex_linspace, dlogL_list, linewidth=1, color='red', label='dlogL')
    # axs[1].set_xlabel('sin(dec)')
    # axs[1].set_title('dlogL computed with a step size of {:.0e}'.format(offset))

    fig.tight_layout()

    outdir = './.logL_and_dlogL_at_inj/'
    if opts.generate_from_bilby:
        suffix = 'bilby'
    else:
        suffix = 'hmc'
    file_name = outdir + '{}_{}_{}'.format(param_index, CONST.TRAJ_PARAMETERS_KEYS[param_index], suffix)
    fig.savefig(file_name)

    # plt.show()
    sys.exit(0)
