# python bilby_event_tutorial.py --event=GW150914 --config_file=../examples/config_files/config_GW150914_IMRPhenomD.ini --template_file=../examples/trigger_files/GW150914_IMRPhenomD.ini

# python bilby_event_tutorial.py --event=GW170817 --config_file=../examples/config_files/config_GW170817_IMRPhenomD.ini --template_file=../examples/trigger_files/GW170817_IMRPhenomD.ini

# Standard python numerical analysis imports:
from scipy import signal
from scipy.interpolate import interp1d
from scipy.signal import butter, filtfilt, iirdesign, zpk2tf, freqz
import h5py
import json

# the IPython magic below must be commented out in the .py file, since it doesn't work there.
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

import sys
sys.path.append('../')


import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import pandas as pd

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf
import Library.initialization as init
import Library.psd_utils as psd_utils

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as CdlogL
import Codes.FIM as FIM

import gw.detector.networks as gwdn
import gw.likelihood as gwlh
import core.sampler
import core.utils as cut





make_plots = 1
plottype = "png"

fs = sampling_frequency = 4096
dt = 1 / fs
geocent_time=1187008882.43
tc_3p5PN = 56.9
start_time = int(geocent_time - tc_3p5PN)
duration = int(tc_3p5PN) + 1 + 2
fband = [20, 2048]

outdir = './.bilby_event_tutorial/'


# function to whiten data
def whiten_tutorial(strain, interp_psd, dt):
    Nt = len(strain)
    freqs = np.fft.rfftfreq(Nt, dt)
    freqs1 = np.linspace(0,2048.,Nt/2+1)

    # whitening: transform to freq domain, divide by asd, then transform back,
    # taking care to get normalization right.
    hf = np.fft.rfft(strain)
    norm = 1./np.sqrt(1./(dt*2))
    white_hf = hf / np.sqrt(interp_psd(freqs)) * norm
    white_ht = np.fft.irfft(white_hf, n=Nt)
    return white_ht

# function to whiten data
def whitened_time_domain_strain(ifo):
    # norm = np.sqrt(2 / ifo.strain_data.sampling_frequency)
    # hf = np.fft.rfft(ifo.strain_data.time_domain_strain)
    # # white_hf = hf / ifo.amplitude_spectral_density_array * norm
    # white_hf = hf / np.sqrt(ifo.power_spectral_density_array) * norm

    # When setting the ifo.frequency_domain_strain for the first time
    # bilby applies automatically a window function to the time domain
    # strain it uses before applying the FFT
    # white_hf = ifo.whitened_frequency_domain_strain * norm
    # adhoc_norm = np.sqrt(2 * ifo.strain_data.sampling_frequency)
    adhoc_norm = 1
    white_hf = ifo.whitened_frequency_domain_strain * adhoc_norm

    # white_ht = np.fft.irfft(white_hf, n=ifo.strain_data.time_domain_strain.shape[0]) * ifo.strain_data.sampling_frequency
    white_ht = np.fft.irfft(white_hf) * ifo.strain_data.sampling_frequency
    return white_ht

def bandpass_time_domain_strain(ifo, strain, fband=None):
    if fband is None:
        fband = [ifo.minimum_frequency, ifo.maximum_frequency]

    fs = ifo.strain_data.sampling_frequency

    if fband[0] * 2 / fs <= 0:
        fband[0] = 0.01 * fs / 2
    if fband[1] * 2 / fs >= 1:
        fband[1] = 0.99 * fs / 2

    bb, ab = butter(4, [fband[0] * 2 / fs, fband[1] * 2 / fs], btype='bandpass')

    normalization = np.sqrt((fband[1]-fband[0])/(fs/2))

    strain_bp = filtfilt(bb, ab, strain) / normalization
    return strain_bp


if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--event", default='GW170817', action="store", type="string", help="""Event label part of the catalog GWTC-1""")
    parser.add_option("--template_file", default=None, action="store", type="string", help="""File .ini describing the parameters where to start the chain. If left to None and --event used, a default file corresponding to the event will be used.""")
    parser.add_option("--trigger_time", default=None, action="store", type="string", help="""Trigger GPS time.""")
    parser.add_option("--config_file", default='../Tests/config_test.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")

    # randGen = np.random.RandomState(seed=2)

    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()

    config_parser = ConfigParser()
    config_parser.read(opts.config_file)
    config_dict = pu.config_parser_to_dict(config_parser)
    config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)




    geocent_time_gwosc = bilby.gw.utils.get_event_time(opts.event)
    # if opts.event == 'GW150914': geocent_time_gwosc = 1126259462.4 + 0.01611328125 # L: +0.015869140625
    if opts.event == 'GW150914': geocent_time_gwosc = 1126259462.4
    if opts.event == 'GW170817': geocent_time_gwosc = 1187008882.4
    # H: +0.039794921875, L: +0.07666015625, V: +25.518310546875
    if opts.template_file is None:
        template_file = '../examples/trigger_files/' + opts.event + '.ini'
    else:
        template_file = opts.template_file
    template_parameters = set_inj.ini_file_to_dict(template_file)
    template_parameters['geocent_time'] = geocent_time_gwosc
    ext_analysis_dict = set_inj.compute_extended_analysis_dict(template_parameters['mass_1'], template_parameters['mass_2'], template_parameters['geocent_time'], template_parameters['chirp_mass'], **config_dict['analysis'])

    start_time = ext_analysis_dict['start_time']
    duration = ext_analysis_dict['duration']

    hdf5_file_path_prefix = '../__input_data/' + opts.event
    if not(os.path.exists(hdf5_file_path_prefix)):
        cut.logger.error(f'The folder given to look for hdf5 strain files of the event: {hdf5_file_path_prefix}, does not exist.')
        sys.exit(1)

    GWTC1_event_PSDs_file = f'../__input_data/{opts.event}/GWTC1_{opts.event}_PSDs.dat'
    GWTC1_event_PSDs = np.loadtxt(GWTC1_event_PSDs_file)

    # INITIALIZE THE THREE INTERFEROMETERS
    # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
    interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])

    # SET WAVEFORM GENERATOR FOR SNR COMPUTATION
    frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
    if opts.event == 'GW150914':
        frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters

    waveform_arguments = {}
    waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
    waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
    waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
    waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']
    # make waveform generator
    waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
        duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
        frequency_domain_source_model=frequency_domain_source_model,
        waveform_arguments=waveform_arguments,
        parameter_conversion=parameter_conversion)

    waveform_polarizations = waveform_generator.frequency_domain_strain(template_parameters)

    SNRmax_ifos = []
    indmax_ifos = []
    phase_ifos = []
    # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
    for i, ifo in enumerate(interferometers):
        ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
        ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']

        # Should look like: '../__input_data/GW170817/H-H1_GWOSC_4KHZ_R1-1187006835-4096.hdf5'
        if opts.event == 'GW150914':
            hdf5_file_path = f'{hdf5_file_path_prefix}/{ifo.name[0]}-{ifo.name}_GWOSC_4KHZ_R1-1126259447-32.hdf5'
        if opts.event == 'GW170817':
            hdf5_file_path = f'{hdf5_file_path_prefix}/LOSC/{ifo.name[0]}-{ifo.name}_LOSC_CLN_4_V1-1187007040-2048.hdf5'

        hdf5_strain, hdf5_start_time, hdf5_duration, sampling_frequency, hdf5_number_of_samples = psd_utils.get_strain_from_hdf5(hdf5_file_path)

        if sampling_frequency != ext_analysis_dict['sampling_frequency']:
            cut.logger.error('Sampling_frequency conflict between hdf5 file = {} and that in the config file = {}'.format(sampling_frequency, ext_analysis_dict['sampling_frequency']))
            sys.exit(1)

        idx_segment_start = int((start_time - hdf5_start_time) * sampling_frequency) + 1
        idx_segment_end = int((start_time + duration - hdf5_start_time) * sampling_frequency)
        desired_hdf5_strain = hdf5_strain[idx_segment_start:idx_segment_end+1]

        strain = bilby.gw.detector.strain_data.InterferometerStrainData(minimum_frequency=ifo.minimum_frequency)
        strain.set_from_time_domain_strain(time_domain_strain=desired_hdf5_strain, sampling_frequency=sampling_frequency, duration=duration)
        # strain.low_pass_filter(filter_freq=ifo.minimum_frequency)

        ifo.strain_data = strain

        ifo.strain_data.start_time = start_time

        # SET THE PSDs
        ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_array=GWTC1_event_PSDs[:, i+1], frequency_array=GWTC1_event_PSDs[:, 0])

        ifo.psd_array = ifo.power_spectral_density_array
        ifo.inverse_psd_array = 1 / ifo.psd_array

        # WHITEN THE DATA
        alpha = 1/8 #  coefficient used in the Tukey window
        ifo.strain_data.roll_off = alpha * ifo.strain_data.duration / 2
        ifo.strain_data.time_domain_strain_whitened = whitened_time_domain_strain(ifo)

        # BANDPASS THE DATA
        # fband = None
        if opts.event == 'GW150914':
            fband = [43, 300] # for GW150914
        if opts.event == 'GW170817':
            fband = [30, 2048]
        ifo.strain_data.time_domain_strain_whitenedbp = bandpass_time_domain_strain(ifo, ifo.strain_data.time_domain_strain_whitened, fband=fband)

        # SNR COMPUTATION
        template_fft = ifo.get_detector_response(waveform_polarizations, template_parameters)
        optimal = ifo.strain_data.frequency_domain_strain * template_fft.conjugate() / ifo.psd_array
        optimal_time = np.fft.irfft(optimal) * ifo.strain_data.sampling_frequency
        template = np.fft.irfft(template_fft) * ifo.strain_data.sampling_frequency

        # -- Normalize the matched filter output:
        # Normalize the matched filter output so that we expect a value of 1 at times of just noise.
        # Then, the peak of the matched filter output will tell us the signal-to-noise ratio (SNR) of the signal.
        df = ifo.strain_data.frequency_array[1] - ifo.strain_data.frequency_array[0]
        sigmasq = 1*(template_fft * template_fft.conjugate() / ifo.psd_array).sum() * df
        sigma = np.sqrt(np.abs(sigmasq))
        # Reminder: SNR(t) = correlation between the data and the
        # the time-domain template slided by -t
        SNR_complex = optimal_time/sigma
        # SNR_complex = optimal_time
        SNR = abs(SNR_complex)

        # indmax is the offset, in number of array elements, by which
        # the natural time-domain template should be slided forward to
        # match at best the data.
        # Then the time of the peak of that template is taken to be
        # the found merger time, or triggered time, that corresponding
        # to SNR max.
        indmax = np.argmax(SNR)
        indmax_ifos.append(indmax)


        phase = np.angle(SNR_complex[indmax]) # Extract phase at peak
        template_phaseshifted = np.real(template * np.exp(1j * phase))    # phase shift the template
        template_rolled = np.roll(template_phaseshifted, indmax)  # Apply time offset
        phase_ifos.append(phase)
        # Since by definition the SNR peak-time tells us by how many seconds the template should be slided forward, it makes sens to slide the SNR time-series by `geocent_time_template - start_time` hence the new SNR time-series will peak at the found merger-time
        # Example: if SNR peaks at 0.017 sec after start_time and the template was generated with an input geocent_time = 1126259462.4, then the found geocent_time_merger = 1126259462.4 + 0.017
        ind_geocent_template = int((template_parameters['geocent_time'] - start_time) * ifo.strain_data.sampling_frequency)
        SNR_rolled = np.roll(SNR, ind_geocent_template)
        SNRmax = SNR[indmax] # equivalent would be: SNRmax= SNR_rolled[indmax_peak]
        SNRmax_ifos.append(SNRmax)
        # template_fft_2 = template_fft.copy()
        # for n in range(template_fft_2.size):
        #     template_fft_2[n] *= np.exp(-1j * 2 * np.pi * n * indmax / (2 * template_fft_2.size))
        # template_2 = np.fft.irfft(template_fft_2) * ifo.strain_data.sampling_frequency
        #
        # plt.figure(figsize=(15,10))
        # plt.plot(ifo.strain_data.time_array - template_parameters['geocent_time'], template, label='template')
        # plt.plot(ifo.strain_data.time_array - template_parameters['geocent_time'], template_rolled, label='template_rolled')
        # plt.plot(ifo.strain_data.time_array - template_parameters['geocent_time'], template_2, label='template_2')
        # plt.legend()
        # plt.xlim([-0.1, 0.02])
        # plt.show()
        # breakpoint()

        # Whiten and band-pass the template for plotting
        # template_whitened = whiten(template_rolled, interp1d(freqs, data_psd),dt)  # whiten the template
        # norm = np.sqrt(2 / ifo.strain_data.sampling_frequency)
        # template_rolled_fd = np.fft.rfft(template) / ifo.strain_data.sampling_frequency
        template_rolled_fd = np.fft.rfft(template_rolled) / ifo.strain_data.sampling_frequency
        template_rolled_fd_white = template_rolled_fd / ifo.amplitude_spectral_density_array
        template_rolled_whitened = np.fft.irfft(template_rolled_fd_white) * ifo.strain_data.sampling_frequency
        template_rolled_match = bandpass_time_domain_strain(ifo, template_rolled_whitened, fband)

        adhoc_norm = np.sqrt(2 / ifo.strain_data.sampling_frequency)
        ifo.strain_data.time_domain_strain_whitenedbp *= adhoc_norm
        template_rolled_match *= adhoc_norm



        template_fft = ifo.get_detector_response(waveform_polarizations, template_parameters)
        template_fft_white = template_fft / ifo.amplitude_spectral_density_array
        template_orig_whitened = np.fft.irfft(template_fft_white) * ifo.strain_data.sampling_frequency
        template_orig_match = bandpass_time_domain_strain(ifo, template_orig_whitened, fband)
        template_orig_match *= adhoc_norm







        # import IPython; IPython.embed(); sys.exit()

        # This way of computing the matched-filter snr, as we do it in PE,
        # gives the exact same answer as SNRmax
        s_inner_h_integrand = ifo.strain_data.frequency_domain_strain * template_rolled_fd.conjugate() / ifo.psd_array
        s_inner_h = 4 * s_inner_h_integrand.sum() / duration
        h_inner_h_integrand = template_rolled_fd * template_rolled_fd.conjugate() / ifo.psd_array
        h_inner_h = 4 * h_inner_h_integrand.sum() / duration
        mf_snr1 = s_inner_h / np.sqrt(h_inner_h)
        # breakpoint()

        # Now if we want to leave the template untouched and slide the data
        # stream instead, the windowing process will make the computed
        # matched-filter snr a tiny bit different from SNRmax
        time_domain_strain_rolled = np.roll(ifo.strain_data.time_domain_strain, -indmax)
        time_domain_strain_rolled = np.real(time_domain_strain_rolled * np.exp(1j * phase))
        window = ifo.strain_data.time_domain_window()
        frequency_domain_strain_rolled = np.fft.rfft(time_domain_strain_rolled * window) / ifo.strain_data.sampling_frequency
        s_inner_h_integrand = frequency_domain_strain_rolled * template_fft.conjugate() / ifo.psd_array
        s_inner_h = 4 * s_inner_h_integrand.sum() / duration
        h_inner_h_integrand = template_fft * template_fft.conjugate() / ifo.psd_array
        h_inner_h = 4 * h_inner_h_integrand.sum() / duration
        mf_snr = s_inner_h / np.sqrt(h_inner_h)
        # breakpoint()

        # PLOT
        time_array = ifo.strain_data.time_array - geocent_time_gwosc

        plt.figure(figsize=(15,10))
        plt.subplot(2,1,1)
        # plt.plot(time_array, SNR, label=ifo.name + ' SNR(t)')
        plt.plot(time_array, SNR_rolled, label=ifo.name + ' SNR(t)')
        plt.grid('on')
        plt.ylabel('SNR')
        plt.xlabel('Time since {0:.4f}'.format(geocent_time_gwosc))
        plt.legend(loc='upper left')
        plt.title(ifo.name+' matched filter SNR around event')

        # plot the strain
        plt.subplot(2,1,2)
        plt.plot(time_array, ifo.strain_data.time_domain_strain_whitenedbp, label='whitened + bp')
        plt.plot(time_array, template_rolled_match, label='Template(t) rolled')
        plt.plot(time_array, template_orig_match, label='Template(t) original')
        plt.xlabel(f'Time since {geocent_time_gwosc} (sec)')
        plt.ylabel('whitened strain (units of noise stdev)')
        plt.legend(loc='upper left')
        plt.title(ifo.name +' whitened data around event')
        plt.tight_layout()
        plt.savefig(outdir + opts.event + "_" + ifo.name + "_SNR." + plottype)


        # zoom in
        plt.figure(figsize=(15,10))
        if opts.event == 'GW150914':
            xlim = [-0.15,0.05]
            # pass
        if opts.event == 'GW170817':
            xlim = [-0.2, 0.2]
            xlim = [-0.05, 0.1]
            # pass
        plt.subplot(2,1,1)
        # plt.plot(time_array, SNR, label=ifo.name + ' SNR(t)')
        plt.plot(time_array, SNR_rolled, label=ifo.name + ' SNR(t)')
        plt.grid('on')
        plt.ylabel('SNR')
        plt.xlim(xlim)
        #plt.xlim([-0.3,+0.3])
        plt.grid('on')
        plt.xlabel('Time since {0:.4f}'.format(geocent_time_gwosc))
        plt.legend(loc='upper left')

        plt.subplot(2,1,2)
        plt.plot(time_array, ifo.strain_data.time_domain_strain_whitenedbp, label='whitened + bp')
        plt.plot(time_array, template_rolled_match, label='Template(t) rolled')
        plt.plot(time_array, template_orig_match, label='Template(t) original')
        plt.xlim(xlim)
        # plt.ylim([-10, 10])
        plt.xlabel(f'Time since {geocent_time_gwosc} (sec)')
        plt.ylabel('whitened strain (units of noise stdev)')
        plt.legend(loc='upper left')
        plt.title(ifo.name +' whitened data around event')

        plt.tight_layout()
        plt.savefig(outdir + opts.event + "_" + ifo.name + "_SNR_zoomed." + plottype)

        # breakpoint()

        # import IPython; IPython.embed(); sys.exit()


    # likelihood = bilby.gw.likelihood.GravitationalWaveTransient(
    #     interferometers, waveform_generator, time_marginalization=False,
    #     distance_marginalization=False, phase_marginalization=False)

    likelihood = gwlh.Likelihood(
        interferometers=interferometers,
        waveform_generator=waveform_generator,
        # priors=priors,
        time_marginalization=False,
        distance_marginalization=False,
        phase_marginalization=False)

    idx_ifo_best_snr = np.argmax(SNRmax_ifos)
    indmax_best = indmax_ifos[idx_ifo_best_snr]
    geocent_time_best = template_parameters['geocent_time'] + indmax_best / interferometers[idx_ifo_best_snr].strain_data.sampling_frequency
    orbital_phase_best = phase_ifos[idx_ifo_best_snr] / 2
    likelihood.parameters = template_parameters.copy()
    likelihood.parameters['geocent_time'] = geocent_time_best
    likelihood.parameters['phase'] += orbital_phase_best

    snr_dict = likelihood.calculate_network_snrs()
    snr_dict_formatted = cut.dictionary_to_formatted_string(snr_dict, decimal_format=2)
    cut.logger.info(f'SNR: {snr_dict_formatted}')

    # waveform_polarizations = waveform_generator.frequency_domain_strain(likelihood.parameters)
    # s_inner_h_network = 0
    # h_inner_h_network = 0
    # mf_snr_network = 0
    # for ifo in interferometers:
    #     per_detector_snr = likelihood.calculate_snrs(waveform_polarizations, ifo)
    #     s_inner_h_network += per_detector_snr.d_inner_h
    #     h_inner_h_network += np.real(per_detector_snr.optimal_snr_squared)
    #     mf_snr_network += per_detector_snr.complex_matched_filter_snr
    #     print(f'\nifo {ifo.name}')
    #     print(f's_inner_h = {per_detector_snr.d_inner_h}')
    #     print(f'h_inner_h = {np.real(per_detector_snr.optimal_snr_squared)}')
    #     print(f'logL = {per_detector_snr.d_inner_h - 0.5 * np.real(per_detector_snr.optimal_snr_squared)}')
    #     print(f'mf_snr = {per_detector_snr.d_inner_h / np.real(per_detector_snr.optimal_snr_squared)**0.5}')
    #
    # logL_ratio_network = np.real(s_inner_h_network) - 0.5 * h_inner_h_network
    # print(f'logL_ratio_network = {logL_ratio_network}')
    # print(f'mf_snr_network = {mf_snr_network}')

    # breakpoint()
