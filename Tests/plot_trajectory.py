import sys
sys.path.append('../')
import numpy as np
import matplotlib.pyplot as plt
import os

import Library.python_utils as pu
import Library.CONST as CONST

from optparse import OptionParser
from configparser import ConfigParser


def get_trajectory_parameters_keys(search_parameter_keys, search_trajectory_functions):
    from gw.prior import identity
    search_trajectory_parameters_keys = []
    for idx, key in enumerate(search_parameter_keys):
        traj_func = search_trajectory_functions[idx]
        if traj_func == identity:
            prefix = ''
        else:
            prefix = traj_func.__name__ + '_'
        search_trajectory_parameters_keys.append(f'{prefix}{key}')
    return search_trajectory_parameters_keys

def get_trajectory_functions(search_parameter_keys, dict_trajectory_functions_str):
    from gw.prior import TRAJECTORY_FUNCTIONS
    search_trajectory_functions = []
    for key in search_parameter_keys:
        if key not in dict_trajectory_functions_str.keys():
            func_name = 'none'
        else:
            func_name = dict_trajectory_functions_str[key]
        traj_func = TRAJECTORY_FUNCTIONS[func_name.lower()]
        search_trajectory_functions.append(traj_func)
    return search_trajectory_functions

def get_trajectory_inverse_functions(search_trajectory_functions):
    from gw.prior import map_func_to_inv_func
    search_trajectory_inv_functions = []
    for traj_func in search_trajectory_functions:
        traj_inv_func = map_func_to_inv_func(traj_func)
        search_trajectory_inv_functions.append(traj_inv_func)
    return search_trajectory_inv_functions


usage = """%prog [options]
Plots one hmc trajectory for each parameter involved."""
parser=OptionParser(usage)
parser.add_option("--keys_to_plot", default=None, action="store", type="string", help=""" List of keys to plot, separated by a comma, for instance: --keys_to_plot=mass_1,mass_2,luminosity_distance. By default all parameters will be plotted.""")
parser.add_option("--traj_nb", default=1, action="store", type="int", help=""" Which trajectory to plot. Default is 1. """,metavar="")

(opts,args)=parser.parse_args()

outdir = sys.argv[1]
outdir_plot_traj = outdir + 'plot_traj/'

trajectory_number = opts.traj_nb
# trajectory_number = 1
# if len(sys.argv)==3: trajectory_number = int(sys.argv[2])

trajectory_index = trajectory_number - 1

if os.path.isfile(outdir + 'config.ini'):
    config_file_path = outdir + 'config.ini'
    config_parser = ConfigParser()
    config_parser.read(config_file_path)
    config_dict = pu.config_parser_to_dict(config_parser)
    # lengthT = config_dict['hmc']['length_num_traj']
else:
    print("ERROR: COULD NOT FIND ANY SORT OF CONFIGURATION FILE")
    sys.exit()

sampler_dict = pu.json_to_dict(outdir + 'config_hmc_parameters.json')
search_parameter_keys = sampler_dict['search_parameter_keys']
search_trajectory_functions = get_trajectory_functions(search_parameter_keys, sampler_dict['trajectory_functions'])
search_trajectory_parameters_keys = get_trajectory_parameters_keys(search_parameter_keys, search_trajectory_functions)
search_trajectory_inv_functions = get_trajectory_inverse_functions(search_trajectory_functions)
# dict_trajectory_parameters_keys = get_dict_trajectory_parameters_keys(search_parameter_keys, sampler_dict['trajectory_parameters_keys'])
dict_parameters_offsets = sampler_dict['parameters_offsets']

# For a long time I used to save the positions of the trajectories under `qpos.dat` and then renamed for `qpos_traj.dat` because I am actually saving the "trajectory" parameters values.
if os.path.isfile(outdir_plot_traj + 'qpos.dat'):
    pt_fit_traj_file = outdir_plot_traj + 'qpos.dat'
else:
    pt_fit_traj_file = outdir_plot_traj + 'qpos_traj.dat'
dlogL_file = outdir_plot_traj + 'dlogL.dat'
H_p_logL_logP_file = outdir_plot_traj + 'H_p_logL_logP.dat'
pmom_trajs_file = outdir_plot_traj + 'pmom_trajs.dat'
dlogTrajPi_trajs_file = outdir_plot_traj + 'dlogTrajPi_traj.dat'
lengths_file = outdir_plot_traj + 'lengths.dat'

# # Pb with np.loadtxt(): when there is only one index in the file, the resulting array has a shape = (), but size still equal to 1. Reshaping it puts it with shape = (1,) and hence it can be normally manipulated then
# if search_parameter_indices.size == 1:
#     search_parameter_indices = search_parameter_indices.reshape(1)
# # Need the indices to be integers
# search_parameter_indices = search_parameter_indices.astype(np.int64)
pt_fit_traj = np.loadtxt(pt_fit_traj_file).T
dlogL = np.loadtxt(dlogL_file).T
H_p_logL_logP = np.loadtxt(H_p_logL_logP_file).T
pmom_trajs = np.loadtxt(pmom_trajs_file).T
dlogTrajPi_trajs = np.loadtxt(dlogTrajPi_trajs_file).T
lengths = np.loadtxt(lengths_file).T
if os.path.exists(outdir + 'sampler_state/phase1/scales.dat'):
    scale = np.loadtxt(outdir + 'sampler_state/phase1/scales.dat').T
elif os.path.exists(outdir + 'sampler_state/scales.dat'):
    scale = np.loadtxt(outdir + 'sampler_state/scales.dat').T
else:
    scale = [-1 for i in range(len(sampler_dict['search_parameter_keys']))]

# q_pos = pt_fit_traj[search_parameter_indices]
# dlogL = dlogL_all[search_parameter_indices]
# import IPython; IPython.embed();
lengthT = int(lengths[trajectory_index])
idx_start = trajectory_index * (lengthT + 1)
idx_end = (trajectory_index +1) * (lengthT + 1)
pt_fit_traj = pt_fit_traj[:, idx_start: idx_end]
dlogL = dlogL[:, idx_start: idx_end]
H_p_logL_logP = H_p_logL_logP[:, idx_start: idx_end]
pmom_trajs = pmom_trajs[:, idx_start: idx_end]
dlogTrajPi_trajs = dlogTrajPi_trajs[:, idx_start: idx_end]


steps = np.arange(len(pt_fit_traj[0]))




from matplotlib import rc
# rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
# ## for Palatino and other serif fonts use:
# # rc('font',**{'family':'serif','serif':['Palatino']})
# rc('text', usetex=True)
#
# plt.rc('text', usetex=False)
# plt.rc('font', family='serif')
# plt.rc('font', size=10)
# plt.rc('font', weight='bold')

# axes_fontdict = {'fontsize': 20,
#         'fontweight': 'bold',
#         'verticalalignment': 'baseline',
#         'horizontalalignment': 'center'}
# axes_titleweight = 'bold'

plt.rcParams['text.usetex'] = False

plt.rcParams['figure.titlesize'] = 18
plt.rcParams['figure.titleweight'] = 'bold'

plt.rcParams['axes.titlesize'] = 15
plt.rcParams['axes.labelsize'] = 12
plt.rcParams['axes.titleweight'] = 'normal'

plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12
plt.rcParams['font.size'] = 10

with_phase_space_traj = True




if opts.keys_to_plot is not None:
    keys_to_plot = opts.keys_to_plot.split(',')
else:
    keys_to_plot = search_parameter_keys

for param_index, param_key in enumerate(search_parameter_keys):
    if param_key in keys_to_plot:
        # traj_param_key = dict_trajectory_parameters_keys[param_key]
        traj_param_key = search_trajectory_parameters_keys[param_index]
        traj_func = search_trajectory_functions[param_index]
        traj_inv_func = search_trajectory_inv_functions[param_index]
        traj_param_latex_label = CONST.LATEX_LABELS[traj_param_key]
        param_latex_label = CONST.LATEX_LABELS[param_key]
        H_start = H_p_logL_logP[0, 0]
        H_end = H_p_logL_logP[0, -1]
        acc_proba = np.exp(-(H_end-H_start))
        # plt.figure(figsize=(15,8))
        # plt.subplot(211)
        # plt.plot(steps, pt_fit_traj[param_index], linewidth=1, color='black', label="{}".format(param_key))
        # plt.xlabel('steps')
        # plt.ylabel("{}".format(param_key)
        # plt.title("{}".format(param_key))
        # plt.subplot(212)
        # plt.plot(steps, dlogL[param_index], linewidth=1, color='red', label="dlogL/d{}".format(param_key))
        # plt.xlabel('steps')
        # plt.ylabel("dlogL/d{}".format(param_key)
        # plt.title("dlogL/d{}".format(param_key))
        # plt.tight_layout()

        # import IPython; IPython.embed()
        # sys.exit()

        if with_phase_space_traj:
            fig, axs = plt.subplots(2, 2, figsize=(16,8.2), gridspec_kw = {'height_ratios': [1, 1]})
            # fig, axs = plt.subplots(3, 1, figsize=(15.7,16.16), gridspec_kw = {'height_ratios': [2, 1, 1]}, dpi=[0])
            # https://stackoverflow.com/questions/47633546/relationship-between-dpi-and-figure-size
            # default dpi = 100, size_in_pixel = w*dpi x h*dpi
            # fig, axs = plt.subplots(3, 1, figsize=(9.6,10.2), gridspec_kw = {'height_ratios': [2, 1, 1]}, dpi=160)

            # fig, axs = plt.subplots(3, 1)
            fig.suptitle(r"Trajectory {} on {}: Acc={:.5f}".format(trajectory_number, traj_param_latex_label, acc_proba))
            # fig.text(0.5, 0.8, "Trajectory {} on {}, offset = {:.1e}".format(trajectory_number, param_key, dict_parameters_offsets[param_key]), fontsize=16)
            axs[0][0].plot(pt_fit_traj[param_index], pmom_trajs[param_index], linewidth=1, color='black', label='Trajectory')
            axs[0][0].plot(pt_fit_traj[param_index][0], pmom_trajs[param_index][0], marker='o', markersize=9, color='green', label='Start')
            axs[0][0].plot(pt_fit_traj[param_index][-1], pmom_trajs[param_index][-1], marker='o', markersize=9, color='red', label='End')
            axs[0][0].set_xlabel(r"{}".format(traj_param_latex_label), color='black')
            axs[0][0].set_ylabel(r"p({})".format(traj_param_latex_label), color='black')
            axs[0][0].set_title("Trajectory in phase space")
            axs[0][0].legend()

            if traj_inv_func.__name__ == 'signval_log_abs_val_inv':
                qpos_of_traj = []
                for qpos_traj_of_traj in pt_fit_traj[param_index]:
                    qpos_of_traj.append(traj_inv_func(qpos_traj_of_traj))
            else:
                qpos_of_traj = traj_inv_func(pt_fit_traj[param_index])
            axs[1][0].plot(qpos_of_traj, pmom_trajs[param_index], linewidth=1, color='black', label='Trajectory')
            axs[1][0].plot(traj_inv_func(pt_fit_traj[param_index][0]), pmom_trajs[param_index][0], marker='o', markersize=9, color='green', label='Start')
            axs[1][0].plot(traj_inv_func(pt_fit_traj[param_index][-1]), pmom_trajs[param_index][-1], marker='o', markersize=9, color='red', label='End')
            axs[1][0].set_xlabel(r"{}".format(param_latex_label), color='black')
            axs[1][0].set_ylabel(r"p({})".format(traj_param_latex_label), color='black')
            # axs[1][0].set_title("Trajectory in phase space")
            axs[1][0].legend()



            # fig = plt.figure(figsize=(15,8))
            # import IPython; IPython.embed();
            # ax1 = fig.add_subplot(211)
            axs[0][1].plot(steps, pt_fit_traj[param_index], linewidth=1, color='black', label=rf"{traj_param_latex_label}")
            axs[0][1].set_xlabel('steps')
            axs[0][1].set_ylabel(rf"{traj_param_latex_label}", color='black')
            axs[0][1].tick_params('y', colors='black')
            axs[0][1].set_title(rf"Step evolution of {traj_param_latex_label}")
            axs[0][1].legend(loc='upper left')


            ax011 = axs[0][1].twinx()
            ax011.plot(steps, dlogL[param_index], linewidth=1, color='green', label=rf"dlogL/d{traj_param_latex_label}")
            ax011.plot(steps, dlogTrajPi_trajs[param_index], linewidth=1, color='red', label=rf"dlogP/d{traj_param_latex_label}")
            ax011.set_ylabel(rf"dlogL/d{traj_param_latex_label} and dlogP/d{traj_param_latex_label}")
            ax011.legend(loc='upper right')
            # ax011.tick_params('y', colors='red')
            ax011.grid(False)

            # axs[2] = fig.add_subplot(212)
            axs[1][1].plot(steps, H_p_logL_logP[0], linewidth=1, color='black', label="H")
            # axs[1][1].plot(steps, H_p_logL_logP[1], linewidth=1, color='red', label="0.5*p**2")
            # axs[1][1].plot(steps, -H_p_logL_logP[2], linewidth=1, color='green', label="-logL")
            axs[1][1].plot(steps, -H_p_logL_logP[2] -H_p_logL_logP[3], linewidth=1, color='purple', label="-logL -logPi")
            axs[1][1].set_xlabel('steps')
            axs[1][1].set_ylabel('H and -logL')
            axs[1][1].legend(loc='upper left')
            axs[1][1].set_title("Step evolution of H's components".format(trajectory_number))

            ax111 = axs[1][1].twinx()
            ax111.plot(steps, H_p_logL_logP[1], linewidth=0.5, color='blue', label=r"$\frac{1}{2}p_{TOT}^2$")
            ax111.plot(steps, 0.5 * pmom_trajs[param_index]**2, linewidth=0.8, color='orange', label=r'$\frac{1}{2}p[$' + rf'{traj_param_latex_label}' + r'$]^2$')
            ax111.set_ylabel(r"$\frac{1}{2}p_{TOT}^2$ and " + r'$\frac{1}{2}p[$' + rf'{traj_param_latex_label}' + r'$]^2$')
            # ax111.plot(steps, -H_p_logL_logP[3], linewidth=0.8, color='red', label="-logP")
            # ax111.tick_params('y', colors='blue')
            ax111.legend(loc='upper right')
            ax111.grid(False)

            fig.tight_layout()
            # import IPython; IPython.embed();
            fig.text(x=0.4, y=0.93, s='scale = {:.2e}'.format(scale[param_index]))
            fig.subplots_adjust(top=0.88) # Needed so that fig.title and ax.title don't overlap

            file_name = outdir_plot_traj + '/plots/traj_{}_{}-{}.png'.format(trajectory_number, param_index,  param_key)
            # fig.savefig(file_name)
        else:
            # import IPython; IPython.embed()
            fig, axs = plt.subplots(2, 1, figsize=(16,8.2), gridspec_kw = {'height_ratios': [1, 1]})
            # fig, axs = plt.subplots(3, 1, figsize=(15.7,16.16), gridspec_kw = {'height_ratios': [2, 1, 1]}, dpi=[0])
            # https://stackoverflow.com/questions/47633546/relationship-between-dpi-and-figure-size
            # default dpi = 100, size_in_pixel = w*dpi x h*dpi
            # fig, axs = plt.subplots(3, 1, figsize=(9.6,10.2), gridspec_kw = {'height_ratios': [2, 1, 1]}, dpi=160)

            # fig, axs = plt.subplots(3, 1)
            fig.suptitle(r"Trajectory {} on {}: MH={:.5f}".format(trajectory_number, traj_param_latex_label, acc_proba))
            # fig = plt.figure(figsize=(15,8))
            # import IPython; IPython.embed();
            # ax1 = fig.add_subplot(211)
            axs[0].plot(steps, pt_fit_traj[param_index], linewidth=1, color='black', label=rf"{traj_param_latex_label}")
            axs[0].set_xlabel('steps')
            axs[0].set_ylabel(rf"{traj_param_latex_label}", color='black')
            axs[0].tick_params('y', colors='black')
            axs[0].set_title(rf"Step evolution of {traj_param_latex_label}")
            axs[0].legend(loc='upper left')


            ax011 = axs[0].twinx()
            ax011.plot(steps, dlogL[param_index], linewidth=1, color='green', label=rf"dlogL/d{traj_param_latex_label}")
            ax011.plot(steps, dlogTrajPi_trajs[param_index], linewidth=1, color='red', label=rf"dlogP/d{traj_param_latex_label}")
            ax011.set_ylabel(rf"dlogL/d{traj_param_latex_label} and dlogP/d{traj_param_latex_label}")
            ax011.legend(loc='upper right')
            # ax011.tick_params('y', colors='red')
            ax011.grid(False)

            # axs[2] = fig.add_subplot(212)
            axs[1].plot(steps, H_p_logL_logP[0], linewidth=1, color='black', label="H")
            # axs[1].plot(steps, H_p_logL_logP[1], linewidth=1, color='red', label="0.5*p**2")
            axs[1].plot(steps, -H_p_logL_logP[2], linewidth=1, color='green', label="-logL")
            axs[1].set_xlabel('steps')
            axs[1].set_ylabel('H and -logL')
            axs[1].legend(loc='upper left')
            axs[1].set_title("Step evolution of H's components".format(trajectory_number))

            ax111 = axs[1].twinx()
            ax111.plot(steps, H_p_logL_logP[1], linewidth=0.5, color='blue', label=r"$\frac{1}{2}p_{TOT}^2$")
            ax111.plot(steps, 0.5 * pmom_trajs[param_index]**2, linewidth=0.8, color='orange', label=r'$\frac{1}{2}p[$' + rf'{traj_param_latex_label}' + r'$]^2$')
            ax111.set_ylabel(r"$\frac{1}{2}p_{TOT}^2$ and " + r'$\frac{1}{2}p[$' + rf'{traj_param_latex_label}' + r'$]^2$')
            # ax111.plot(steps, -H_p_logL_logP[3], linewidth=0.8, color='red', label="-logP")
            # ax111.tick_params('y', colors='blue')
            ax111.legend(loc='upper right')
            ax111.grid(False)

            fig.tight_layout()
            # import IPython; IPython.embed();
            fig.text(x=0.4, y=0.93, s='scale = {:.2e}'.format(scale[param_index]))
            fig.subplots_adjust(top=0.88) # Needed so that fig.title and ax.title don't overlap

            file_name = outdir_plot_traj + '/plots/traj_{}_{}-{}.png'.format(trajectory_number, param_index,  param_key)
            # fig.savefig(file_name)

# fig = plt.figure(figsize=(15,8))
# plt.plot(steps, H_p_logL_logP[0], linewidth=1, color='black', label="H")
# plt.plot(steps, H_p_logL_logP[1], linewidth=1, color='red', label="0.5*p^2")
# plt.plot(steps, H_p_logL_logP[2], linewidth=1, color='green', label="logL")
# plt.xlabel('steps')
# plt.legend()
# plt.title("Trajectory evolution of H's components")

plt.show()


# if True:
if False:
    idx_mc = search_parameter_keys.index('chirp_mass')
    idx_mu = search_parameter_keys.index('reduced_mass')
    traj_inv_func_mc = search_trajectory_inv_functions[idx_mc]
    traj_inv_func_mu = search_trajectory_inv_functions[idx_mu]
    mc_traj = traj_inv_func_mc(pt_fit_traj[idx_mc])
    mu_traj = traj_inv_func_mc(pt_fit_traj[idx_mu])
    eta_traj = (mu_traj / mc_traj)**2.5

    fig, ax = plt.subplots(1, 1, figsize=(16,8.2))
    ax.plot(steps, eta_traj, linewidth=1, color='black', label=rf"$\eta$")
    ax.set_xlabel('steps')
    ax.set_ylabel(rf"$\eta$", color='black')
    ax.tick_params('y', colors='black')
    ax.set_title(rf"Step evolution of $\eta$")
    ax.legend(loc='upper left')

    fig, ax = plt.subplots(1, 1, figsize=(16,8.2))
    ax.plot(mc_traj, mu_traj, linewidth=1, color='black')
    ax.plot(mc_traj[0], mu_traj[0], marker='o', markersize=9, color='green', label='Start')
    ax.plot(mc_traj[-1], mu_traj[-1], marker='o', markersize=9, color='red', label='End')
    ax.set_xlabel(r"$\mathcal{M}$")
    ax.set_ylabel(rf"$\mu$", color='black')
    ax.tick_params('y', colors='black')

    plt.show()
    sys.exit()
