# This scripts computes the amplitude as computed by LAL for the TF2 waveform
# the final expression is at line 470 here: https://lscsoft.docs.ligo.org/lalsuite/lalsimulation/_l_a_l_sim_inspiral_taylor_f2_8c_source.html#l00470

import numpy as np
import matplotlib.pyplot as plt

import sys
sys.path.append('../')

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import bilby
import Codes.set_injection_parameters as set_inj

A0 = 2.2542206004661943e-21


LAL_MRSUN_SI = 1.476625061404649406193430731479084713e3
LAL_MTSUN_SI = 4.925491025543575903411922162094833998e-6
LAL_MSUN_SI = 1.988546954961461467461011951140572744e30
LAL_PI = 3.141592653589793238462643383279502884
LAL_PC_SI = 3.085677581491367278913937957796471611e16 # not used here
LAL_GAMMA = 0.577215664901532860606512090082402431

eta = set_inj.ini_file_to_dict()['symmetric_mass_ratio']
mass_1= set_inj.ini_file_to_dict()['mass_1']
mass_2 = set_inj.ini_file_to_dict()['mass_2']
mass_1 = mass_1 * bilby.core.utils.solar_mass
mass_2 = mass_2 * bilby.core.utils.solar_mass
m1 = mass_1 / LAL_MSUN_SI
m2 = mass_2 / LAL_MSUN_SI

luminosity_distance = set_inj.ini_file_to_dict()['luminosity_distance']
luminosity_distance = luminosity_distance * 1e6 * bilby.core.utils.parsec
r = luminosity_distance


amp0 = -4. * m1 * m2 / r * LAL_MRSUN_SI * LAL_MTSUN_SI * np.sqrt(LAL_PI/12)



m = m1 + m2
m_sec = m * LAL_MTSUN_SI
piM = LAL_PI * m_sec



v = piM**(1/3)
v2 = v * v
v3 = v2 * v
v4 = v3 * v
v5 = v4 * v
v6 = v5 * v
v7 = v6 * v
v10 = v**10

FTaN = 32.0 * eta*eta / 5.0
FTa2 = -(12.47/3.36 + 3.5/1.2 * eta)
FTa3 = 4.0 * LAL_PI
FTa4 = -(44.711/9.072 - 92.71/5.04 * eta - 6.5/1.8 * eta*eta)
FTa5 = -(81.91/6.72 + 58.3/2.4 * eta) * LAL_PI
FTl6 = -17.12/1.05
FTa6 = (664.3739519/6.9854400 + 16.0/3.0 * LAL_PI*LAL_PI - 17.12/1.05 * LAL_GAMMA - 17.12/1.05*np.log(4.) + (4.1/4.8 * LAL_PI*LAL_PI - 134.543/7.776) * eta - 94.403/3.024 * eta*eta - 7.75/3.24 * eta*eta*eta)
FTa7 = -(162.85/5.04 - 214.745/1.728 * eta - 193.385/3.024 * eta*eta) * LAL_PI


dETaN = 2 * (-eta / 2.0)
dETa1 = 2. * (-(0.75 + eta/12.0))
dETa2 = 3. * (-(27.0/8.0 - 19.0/8.0 * eta + 1./24.0 * eta*eta))
dETa3 = 4 * (-(67.5/6.4 - (344.45/5.76 - 20.5/9.6 * LAL_PI*LAL_PI) * eta + 15.5/9.6 * eta*eta + 3.5/518.4 * eta*eta*eta))


flux = 0
dEnergy = 0

flux += 1
dEnergy += 1

flux += FTa2 * v2
dEnergy += dETa1 * v2

flux += FTa3 * v3

flux += FTa4 * v4
dEnergy += dETa2 * v4

flux += FTa5 * v5

flux += (FTa6 + FTl6*np.log(v)) * v6
dEnergy += dETa3 * v6

flux += FTa7 * v7

flux *= FTaN * v10
dEnergy *= dETaN * v

amp = amp0 * np.sqrt(-dEnergy/flux) * v

# amp = -2.2576107639202769e-21
# A0_lal = amp0 * piM**(-7/6)


import IPython; IPython.embed();

sys.exit()
