import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import subprocess
import pandas as pd

import bilby

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.roq_utils as roqu
import Library.BNS_HMC_Tools as bht
import Library.Fit_NR as fnr
import Library.CONST as CONST
import Library.RUN_CONST as RUN_CONST
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.plots as plots
import Library.likelihood_gradient as lg
import Library.sklearn_fit as sklf
import Library.initialization as init

import Codes.set_injection_parameters as set_inj
import Codes.set_psds as set_psds
import Codes.logL_snr as logL_snr
import Codes.dlogL as CdlogL
import Codes.FIM as FIM

import gw.detector.networks as gwdn
import core.sampler
import core.utils as cut



if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--outdir", default='./.forward_vs_central_diff', action="store", type="string", help="""Output directory where results and plots will be stored. If not specified, it will be created automatically in the current working directory under ./outdir""")
    parser.add_option("--inputdir", default='../__output_data/GW170817/0_2_3_4_5_6_7_8_15_16/PSD_3/1500_1500_2000_200_200_0.005/phase_marginalization/IMRPhenomD', action="store", type="string", help="""Input directory from which points on where to calculate de gradients of logL will be read.""")
    parser.add_option("--param_keys", default='chi_1,chi_2', action="store", type="string", help="""Parameters for which to compare the gradients.""")
    parser.add_option("--offset", default=1e-7, action="store", type="float")
    parser.add_option("--squeeze", default=False, action="store_true")

    parser.add_option("-v", "--verbose", default=True, action="store_true", help="""cut.logger.info out information of interest during the run.""",dest='verbose')
    parser.add_option("--plot_traj", default=False, action="store_true", help="""Will save positions, momentas, hamiltonian values etc of every point along every trajectory in phase to be able to plot them with `plot_trajectories.py` script.""")
    parser.add_option("--sub_dir", default=None, action="store", type="string", help="""Optional sub-directory that will be appended at the endto the default output directory such that the final output directory is: '/default-output-dir/sub_dir/'""")
    parser.add_option("--fit_method", default='cubic', action="store", type="string", help="""Method used to fit the numerical gradients of the log likelihood accumulated in phase1. """)
    parser.add_option("--no_seed", default=False, action="store_true", help="""New noise realisation from PSDs is generated""")
    parser.add_option("--on_CIT", default=False, action="store_true", help="""Set this to True if running this script on Caltech's cluster. Will set the right path for the ROQ basis.""")

    # randGen = np.random.RandomState(seed=2)

    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()

    opts_dict_formatted = cut.dictionary_to_formatted_string(opts.__dict__)
    cut.logger.info(f'Options from the command line are: {opts_dict_formatted}')
    cut.logger.info(f'Loading data from run in: {opts.inputdir}')
    config_parser = ConfigParser()
    config_parser.read(opts.inputdir + '/config.ini')
    config_dict = pu.config_parser_to_dict(config_parser)
    config_dict_formatted = cut.dictionary_to_formatted_string(config_dict)
    cut.logger.info(f'Options from the configuration file were: {config_dict_formatted}')

    if opts.on_CIT and config_dict['analysis']['roq']:
        config_dict['analysis']['roq_b_matrix_directory'] = '/home/cbc/ROQ_data/IMRPhenomPv2/'
        cut.logger.info('Setting the roq_b_matrix_directory to "/home/cbc/ROQ_data/IMRPhenomPv2/"')

    RUN_CONST.plot_traj = opts.plot_traj
    if not(opts.no_seed):
        # Set up a random seed for result reproducibility.  This is optional!
        np.random.seed(88170235)

    inj_file = opts.inputdir + '/injection.ini'
    inj_file_name = inj_file.split('/')[-1]
    inj_name = inj_file_name.split('.')[0]


    # Parse the inputs relative to the sampler and the search
    # sampler_dict = init.get_sampler_dict(config_dict)
    sampler_dict = pu.json_to_dict(opts.inputdir + '/config_hmc_parameters.json')
    sampler_dict_formatted = cut.dictionary_to_formatted_string(sampler_dict)
    cut.logger.info(f'Options for the sampler are: {sampler_dict_formatted}')

    # SET THE OUTPUT DIRECTORY WHERE OUTPUT FILES WILL BE SAVED
    outdir = opts.outdir
    cut.setup_logger(cut.logger, outdir=outdir, label='logger', log_level='INFO')
    cut.logger.info(f'Output directory is: {outdir}')





    # SET THE INJECTION PARAMETERS AND THE ANALYSIS PARAMETERS
    injection_parameters, ext_analysis_dict = set_inj.get_inj_parameters_and_analysis_dict(inj_file, **config_dict['analysis'])
    inj_dict_formatted = cut.dictionary_to_formatted_string(injection_parameters)
    cut.logger.info(f'Injected parameters derived from {inj_file} are: {inj_dict_formatted}')
    ext_anal_dict_formatted = cut.dictionary_to_formatted_string(ext_analysis_dict)
    cut.logger.info(f'Analysis parameters derived from the component masses are: {ext_anal_dict_formatted}')

    start_time = ext_analysis_dict['start_time']

    # INITIALIZE THE THREE INTERFEROMETERS
    # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
    interferometers = gwdn.InterferometerList(ext_analysis_dict['ifos'])
    # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
    for ifo in interferometers:
        ifo.minimum_frequency = ext_analysis_dict['minimum_frequency']
        ifo.maximum_frequency = ext_analysis_dict['maximum_frequency_ifo']
        # The following line is called automatically when setting the PSDs, which we donnot do here hence the line.
        # Stangely however, in bilby, the `Interferometer` class does not have `duration` and `sampling_frequency` arguments when the `InterferometerStrainData` and the `InterferometerList` classes do. Hence one needs to set those arguments for the list of `InterferometerStrainData` so that the `InterferometerList` inherits from them. The values will be inherited from the first one in the list...
        # Why do `interferometers.duration` and `interferometers.sampling_frequency` need to be set ? => Because I use them when creating my waveform_generator in bilby_wavefor
        ifo.strain_data.duration = ext_analysis_dict['duration']
        ifo.strain_data.sampling_frequency = ext_analysis_dict['sampling_frequency']
        ifo.strain_data.start_time = start_time


    # SET THEIR POWER SPECTRAL DENSITIES
    # This function does not interpolate the psd
    set_psds.main(interferometers, opt_psd=config_dict['analysis']['psd'], sampling_frequency=ext_analysis_dict['sampling_frequency'], duration=ext_analysis_dict['duration'], start_time=start_time)


    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=ext_analysis_dict['sampling_frequency'],
        duration=ext_analysis_dict['duration'],
        start_time=start_time)

    # WAVEFORM GENERATION
    waveform_arguments = {}
    waveform_arguments['minimum_frequency'] = ext_analysis_dict['minimum_frequency']
    waveform_arguments['waveform_approximant'] = ext_analysis_dict['approximant']
    waveform_arguments['reference_frequency'] = ext_analysis_dict['reference_frequency']
    waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_injected_waveform']
    injection_waveform_generator = bilby.gw.WaveformGenerator(
        duration=interferometers.duration,
        sampling_frequency=interferometers.sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
        waveform_arguments=waveform_arguments
        )
    # template_ifos_injected = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, interferometers, waveform_arguments)

    # INJECT TEMPLATE INTO NOISE STRAIN DATA
    # Add the template signal to each detector's strain data
    # for i in range(len(interferometers)):
        # interferometers[i].strain_data.frequency_domain_strain += template_ifos_injected[i]
        # interferometers[i].fd_strain = interferometers[i].strain_data.frequency_domain_strain
    interferometers.inject_signal(parameters=injection_parameters, waveform_generator=injection_waveform_generator)
    for ifo in interferometers:
        ifo.fd_strain = ifo.strain_data.frequency_domain_strain

    # # COMPUTE AND cut.logger.info SNR
    interferometers.optimal_snr()
    interferometers.matched_filter_snr()
    interferometers.log_likelihood()
    # waveform_arguments['maximum_frequency'] = ext_analysis_dict['maximum_frequency_search_waveform']
    # template_ifos = bilby_wv.WaveForm_ThreeDetectors(injection_parameters, interferometers, waveform_arguments)
    # logL, Snr, opt_snr_Best = logL_snr.main(template_ifos, interferometers, True)
    #
    # # COMPUTE dlogL and cut.logger.info it to check with the plots
    # dlogL = CdlogL.main(injection_parameters, start_time, interferometers, waveform_arguments, True, **sampler_dict)


    # SET THE LIKELIHOOD OBJECT, two different cases whether ROQ basis is used or not
    if config_dict['analysis']['roq']:
        if ext_analysis_dict['approximant'] != 'IMRPhenomPv2':
            raise ValueError("Need to set approximant to IMRPhenomPv2 to work with the ROQ basis, approximant here is {}".format(ext_analysis_dict['approximant']))

        scale_factor = ext_analysis_dict['roq']['scale_factor']
        roq_directory = ext_analysis_dict['roq']['directory']
        # Load in the pieces for the linear part of the ROQ. Note you will need to
        freq_nodes_linear = np.load(roq_directory + "fnodes_linear.npy") * scale_factor
        # Load in the pieces for the quadratic part of the ROQ
        freq_nodes_quadratic = np.load(roq_directory + "fnodes_quadratic.npy") * scale_factor

        # Load the parameters describing the valid parameters for the basis.
        params = ext_analysis_dict['roq']['rescaled_params'].copy()

        outdir_psd = init.get_outdir_psd(config_dict['analysis']['psd'])
        roq_outdir = '../__ROQ_weights/' + inj_name + '/' + outdir_psd
        pu.mkdirs(roq_outdir)
        weights_file_path_no_format = roq_outdir + '{:.0f}Hz_{:.0f}Hz_{:.0f}Hz_{:.0f}s_weights'.format(params['flow'], ext_analysis_dict['maximum_frequency_ifo'], ext_analysis_dict['maximum_frequency_injected_waveform'], params['seglen'])
        weight_format = 'npz'
        weights_file_path = weights_file_path_no_format + f'.{weight_format}'

        # make waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=bilby.gw.source.binary_neutron_star_roq,
            waveform_arguments=dict(
                frequency_nodes_linear=freq_nodes_linear,
                frequency_nodes_quadratic=freq_nodes_quadratic,
                reference_frequency=ext_analysis_dict['reference_frequency'], waveform_approximant=ext_analysis_dict['approximant']),
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters)

        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        priors = bilby.gw.prior.BNSPriorDict()
        # for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'phi_12', 'phi_jl', 'luminosity_distance']:
        for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'theta_jn', 'phase', 'psi', 'ra', 'dec', 'luminosity_distance']:
            priors[key] = injection_parameters[key]
        for key in ['mass_1', 'mass_2']:
            priors[key].minimum = max(priors[key].minimum, params['compmin'])
        priors['chirp_mass'] = bilby.core.prior.Constraint(name='chirp_mass', minimum=float(params['chirpmassmin']), maximum=float(params['chirpmassmax']))
        priors['mass_ratio'] = bilby.core.prior.Constraint(0.125, 1, name='mass_ratio')
        priors['geocent_time'] = bilby.core.prior.Uniform(injection_parameters['geocent_time'] - 0.1, injection_parameters['geocent_time'] + 0.1, latex_label='$t_c$', unit='s')

        # Update user's parameters boundaries with the roq boundaries
        # if they are more restrictive
        from bilby.gw.conversion import component_masses_to_chirp_mass, chirp_mass_and_mass_ratio_to_total_mass
        new_bounds = sampler_dict['parameters_boundaries'].copy()
        if 'chirp_mass' in sampler_dict['search_parameter_keys']:
            # If mass boundaries set by user with mass_1 and mass_2
            # convert these into chirp boundaries
            if 'chirp_mass' not in new_bounds.keys():
                chirp_mass_min_user = component_masses_to_chirp_mass(new_bounds['mass_1'][0], new_bounds['mass_2'][0])
                chirp_mass_max_user = component_masses_to_chirp_mass(new_bounds['mass_1'][1], new_bounds['mass_2'][1])
            # Then take the most restrictive between user bounds and ROQ
                chirp_mass_min = max(chirp_mass_min_user, params['chirpmassmin'])
                chirp_mass_max = min(chirp_mass_max_user, params['chirpmassmax'])
                new_bounds['chirp_mass'] = [chirp_mass_min, chirp_mass_max]
                if chirp_mass_min_user != chirp_mass_min or chirp_mass_max != chirp_mass_max_user:
                    cut.logger.info(f'Chirp mass boundaries from user was: [{chirp_mass_min_user}, {chirp_mass_max_user}], with the ROQ constraints it is now: [{chirp_mass_min}, {chirp_mass_max}]')
        # Now we want to know the new bounds for mass_1 and mass_2 as they will
        # change the bounds for the reduced_mass. For that we will first
        # derive the bounds for the mass ratio:
        if 'mass_ratio' not in new_bounds.keys():
            mass_ratio_max_user = new_bounds['mass_1'][1] / new_bounds['mass_2'][0]
            new_bounds['mass_ratio'] = [1, min(mass_ratio_max_user, params['qmax'])]
            compmassmin, _ = paru.Mc_and_q_to_m1_and_m2(chirp_mass_min, 1)
            _, compmassmax = paru.Mc_and_q_to_m1_and_m2(chirp_mass_max, 1)
            compmassmin = max(compmassmin, params['compmin'])
            new_bounds['mass_1'] = [compmassmin, compmassmax]
            new_bounds['mass_2'] = [compmassmin, compmassmax]
            cut.logger.info(f'New bounds for component masses are: [{compmassmin}, {compmassmax}]')

        sampler_dict['parameters_boundaries'] = new_bounds.copy()

        if os.access(weights_file_path, os.W_OK):
            # load the weights from the file
            likelihood_central = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                weights=weights_file_path, priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])
            likelihood_forward = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                weights=weights_file_path, priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])
        else:
            # Load in the pieces for the linear part of the ROQ. Note you will need to
            basis_matrix_linear = np.load(roq_directory + "B_linear.npy").T
            # Load in the pieces for the quadratic part of the ROQ
            basis_matrix_quadratic = np.load(roq_directory + "B_quadratic.npy").T

            # Redoing here a bit what's already being done in set_inj because `bilby.gw.likelihood.ROQGravitationalWaveTransient()` does not accept dictionnary for roq_params...
            roq_params_ndarray = np.genfromtxt(ext_analysis_dict['roq']['params_path'], names=True)
            roq_params_ndarray = roqu.rescale_params(roq_params_ndarray, scale_factor)
            # roq_params_ndarray = None

            likelihood_central = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic,
                priors=priors, roq_params=roq_params_ndarray)
            likelihood_forward = bilby.gw.likelihood.ROQGravitationalWaveTransient(
                interferometers=interferometers, waveform_generator=search_waveform_generator,
                linear_matrix=basis_matrix_linear, quadratic_matrix=basis_matrix_quadratic,
                priors=priors, roq_params=roq_params_ndarray)

            # remove the basis matrices as these are big for longer bases
            del basis_matrix_linear, basis_matrix_quadratic

            # write the weights to file so they can be loaded multiple times
            likelihood_central.save_weights(weights_file_path_no_format, format=weight_format)
    else:
        # Here we add constraints on chirp mass and mass ratio to the prior, these are
        # determined by the domain of validity of the ROQ basis.
        priors = bilby.gw.prior.BNSPriorDict()
        # make waveform generator
        search_waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
            duration=ext_analysis_dict['duration'], sampling_frequency=ext_analysis_dict['sampling_frequency'],
            frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
            waveform_arguments=waveform_arguments,
            parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters)

        likelihood_central = bilby.gw.likelihood.GravitationalWaveTransient(
            interferometers=interferometers, waveform_generator=search_waveform_generator,
            priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])
        likelihood_forward = bilby.gw.likelihood.GravitationalWaveTransient(
            interferometers=interferometers, waveform_generator=search_waveform_generator,
            priors=priors, phase_marginalization=config_dict['analysis']['phase_marginalization'])

    # # Initialize all the required parameters to zero s.t. the dictionary has a value for all keys even if the injection_parameters doesn't specify some of them
    for likelihood in [likelihood_central, likelihood_forward]:
        for key in likelihood.waveform_generator.source_parameter_keys:
            likelihood.parameters[key] = 0


    likelihood_central.parameters.update(injection_parameters)
    likelihood_forward.parameters.update(injection_parameters)

    # SET THE LIKELIHOOD GRADIENT OBJECT AND COMPUTE THE GRADIENT AT POINT OF INJECTION
    likelihood_gradient_central = lg.GWTransientLikelihoodGradient(
        likelihood=likelihood_central,
        search_parameter_keys=sampler_dict['search_parameter_keys'],
        dict_trajectory_functions_str=sampler_dict['trajectory_functions'],
        dict_trajectory_parameters_keys=sampler_dict['trajectory_parameters_keys'],
        dict_parameters_offsets=sampler_dict['parameters_offsets'],
        dict_diff_method={'chi_1': 'central', 'chi_2': 'central', 'psi': 'central', 'ra': 'central'},
        offset=opts.offset
        )
    likelihood_gradient_forward = lg.GWTransientLikelihoodGradient(
        likelihood=likelihood_forward,
        search_parameter_keys=sampler_dict['search_parameter_keys'],
        dict_trajectory_functions_str=sampler_dict['trajectory_functions'],
        dict_trajectory_parameters_keys=sampler_dict['trajectory_parameters_keys'],
        dict_parameters_offsets=sampler_dict['parameters_offsets'],
        dict_diff_method={'chi_1': 'forward', 'chi_2': 'forward', 'psi': 'forward', 'ra': 'forward'},
        offset=opts.offset
        )

    # SET THE HMC SAMPLER JUST TO HAVE ACCESS TO SOME HANDY METHODS...
    sampler = core.sampler.GWHMCSampler(
        injection_parameters=injection_parameters,
        outdir=outdir,
        likelihood_gradient=likelihood_gradient_central,
        parameters_boundaries=sampler_dict['parameters_boundaries'],
        search_parameter_keys_local_fit=sampler_dict['search_parameter_keys_local_fit'],
        n_traj_hmc_tot = config_dict['hmc']['n_traj_hmc_tot'],
        n_traj_fit = config_dict['hmc']['n_traj_fit'],
        n_traj_for_this_run = config_dict['hmc']['n_traj_for_this_run'],
        n_fit_1 = config_dict['hmc']['n_fit_1'],
        n_fit_2 = config_dict['hmc']['n_fit_2'],
        length_num_traj = config_dict['hmc']['length_num_traj'],
        epsilon0 = config_dict['hmc']['epsilon0'],
        n_sis = config_dict['hmc']['n_sis']
        )


    logL = likelihood_central.log_likelihood_ratio()
    cut.logger.info(f'likelihood.log_likelihood_ratio() = {logL}')


    parameters_to_analyse = opts.param_keys.split(',')

    dlogL_central = likelihood_gradient_central.calculate_dlogL_np(search_parameter_keys=parameters_to_analyse)
    dlogL_forward = likelihood_gradient_forward.calculate_dlogL_np(search_parameter_keys=parameters_to_analyse)
    df_dlogL = pd.DataFrame([dlogL_central, dlogL_forward], columns=parameters_to_analyse, index=['central', 'forward'])
    cut.logger.info(f'dlogL at injection:\n {df_dlogL}')




    # if True: import IPython; IPython.embed(); sys.exit()


    if False:
        data_prefix = '/data/100_15_16_1e-07'
        err_rel = np.loadtxt(outdir + f'{data_prefix}_err_rel.dat')
        err_abs = np.loadtxt(outdir + f'{data_prefix}_err_abs.dat')
        dlogL_pos_central_all = np.loadtxt(outdir + f'{data_prefix}_dlogL_central.dat')
        dlogL_pos_forward_all = np.loadtxt(outdir + f'{data_prefix}_dlogL_forward.dat')
    else:
        cut.logger.info("Loading qpos_global_fit")
        qpos_global_fit_all = np.loadtxt(opts.inputdir + '/sampler_state/qpos_global_fit.dat')

        # SUB-SELECT ~10 000 pts from phase1 from all trajectories
        n_pt_total_wished = 100
        nb_pt_phase1 = len(qpos_global_fit_all)
        ratio = int(nb_pt_phase1 / n_pt_total_wished)
        # length_num_traj = config_dict['hmc']['length_num_traj']
        # nb_acc_traj_phase1 = int(nb_pt_phase1/length_num_traj)
        qpos_traj_for_comparison = qpos_global_fit_all[::ratio, :]


        dlogL_pos_central_all = []
        dlogL_pos_forward_all = []
        err_rel = []
        err_abs = []
        nb_points_tested = len(qpos_traj_for_comparison)
        cut.logger.info(f"Starting to loop over the {nb_points_tested} qpos points...")
        # Loop over the selected points to compare forward gradient wrt central
        for idx, qpos_traj in enumerate(qpos_traj_for_comparison):

            parameters = sampler.q_pos_traj_to_parameters_dict(qpos_traj)

            likelihood_gradient_central.likelihood.parameters.update(parameters)
            dlogL_pos_central = likelihood_gradient_central.calculate_dlogL_np(search_parameter_keys=parameters_to_analyse)

            likelihood_gradient_forward.likelihood.parameters.update(parameters)
            dlogL_pos_forward = likelihood_gradient_forward.calculate_dlogL_np(search_parameter_keys=parameters_to_analyse)

            err_rel.append(np.abs((dlogL_pos_central - dlogL_pos_forward) / dlogL_pos_central).tolist())
            err_abs.append(np.abs((dlogL_pos_central - dlogL_pos_forward)).tolist())
            # if err_rel[idx][0] > 0.5 or err_rel[idx][1] > 0.5: breakpoint()
            dlogL_pos_central_all.append(dlogL_pos_central.tolist())
            dlogL_pos_forward_all.append(dlogL_pos_forward.tolist())
            if idx % int(nb_points_tested/10) == 0 and idx != 0:
                cut.logger.info(f'{idx} points compared')


        # Introduce scales ?
        err_rel = np.asarray(err_rel)
        err_rel *=100
        err_abs = np.asarray(err_abs)
        dlogL_pos_central_all = np.asarray(dlogL_pos_central_all)
        dlogL_pos_forward_all = np.asarray(dlogL_pos_forward_all)

    if False:
        historical_param_indices = []
        for key in parameters_to_analyse:
            historical_param_indices.append(CONST.HISTORICAL_PARAMETERS_INDICES[key])
        historical_param_indices.sort()
        hist_idx_str = '_'.join([str(idx) for idx in historical_param_indices])
        header = ' | '.join(parameters_to_analyse)
        np.savetxt(outdir + f'/data/{err_rel.shape[0]}_{hist_idx_str}_{opts.offset}_err_rel.dat', err_rel, header=header)
        np.savetxt(outdir + f'/data/{err_rel.shape[0]}_{hist_idx_str}_{opts.offset}_err_abs.dat', err_abs, header=header)
        np.savetxt(outdir + f'/data/{err_rel.shape[0]}_{hist_idx_str}_{opts.offset}_dlogL_central.dat', dlogL_pos_central_all, header=header)
        np.savetxt(outdir + f'/data/{err_rel.shape[0]}_{hist_idx_str}_{opts.offset}_dlogL_forward.dat', dlogL_pos_forward_all, header=header)
    # cut.logger.info(f"\nMean of relative errors for an offset of {opts.offset} are:")
    # cut.logger.info(err_rel.mean(axis=0))

    # PLOT HISTOGRAMS OF THE RELATIVE ERRORS
    show_plots = False
    import matplotlib.pyplot as plt
    color = 'blue'
    if opts.offset == 1e-5:
        color = 'red'
    if opts.offset == 1e-6:
        color = 'orange'
    if opts.offset == 1e-7:
        color = 'green'
    if opts.offset == 1e-8:
        color = 'blue'
    # breakpoint()
    # fig, axes = plt.subplots(err_rel.shape[1], 1, figsize=(10, 10*err_rel.shape[1]))
    fig, axes = plt.subplots(err_rel.shape[1], 1, figsize=(10,4*err_rel.shape[1]))
    fig.text(x=0.5, y=0.98, s='Central vs forward differencing in $\\partial ln\\mathcal{L}$', fontsize=22, horizontalalignment='center')
    fig.text(x=0.5, y=0.94, s=f'Number of points tested: {err_rel.shape[0]}', fontsize=18, horizontalalignment='center')
    fig.text(x=0.5, y=0.92, s=f'offset = {opts.offset}', fontsize=18, horizontalalignment='center', color=color)
    for param_index, param_key in enumerate(parameters_to_analyse):
        err_p_copy = err_rel[:, param_index].copy()
        err_max = max(err_p_copy.max(), 0.1)
        if opts.squeeze and err_max > 10:
            err_max = 10
            err_p_copy = np.where(err_p_copy < err_max, err_p_copy, err_max)
        axes[param_index].hist(err_p_copy, bins=np.logspace(np.log10(err_p_copy.min()), np.log10(err_max), 50), color=color)
        # axes[param_index].hist(err_p_copy, bins=400, color=color)
        # axes[param_index].set_xlim(0, err_max)
        axes[param_index].set_xscale('log')
        # axes[param_index].set_ylim(0, err_rel.shape[0])
        axes[param_index].set_xlabel('Relative error (%)')
        latex_label_dlogLd = CONST.LATEX_LABELS['dlogL/d']
        latex_label = CONST.LATEX_LABELS[param_key]
        # axes[param_index].set_title(latex_label)
        axes[param_index].set_title(latex_label_dlogLd + latex_label)
    plt.tight_layout()
    fig.subplots_adjust(top=0.90)
    if show_plots:
        filepath = opts.outdir + f'/forward_vs_central_diff_offset{opts.offset}_rel'
        if opts.squeeze:
            filepath += '_squeezed'
        filepath += '.png'
        fig.savefig(filepath)
        cut.logger.info(f'Saving figure in {filepath}')
    else:
        plt.show()





    fig, axes = plt.subplots(err_abs.shape[1], 1, figsize=(10,4*err_abs.shape[1]))
    fig.text(x=0.5, y=0.98, s='Central vs forward differencing in $\\partial ln\\mathcal{L}$', fontsize=22, horizontalalignment='center')
    fig.text(x=0.5, y=0.94, s=f'Number of points tested: {err_rel.shape[0]}', fontsize=18, horizontalalignment='center')
    fig.text(x=0.5, y=0.92, s=f'offset = {opts.offset}', fontsize=18, horizontalalignment='center', color=color)
    for param_index, param_key in enumerate(parameters_to_analyse):
        err_p_copy = err_abs[:, param_index].copy()
        err_max = max(err_p_copy.max(), 0.1)
        if False and err_max > 10:
            err_max = 10
            err_p_copy = np.where(err_p_copy < err_max, err_p_copy, err_max)
        axes[param_index].hist(err_p_copy, bins=50, color=color)
        # axes[param_index].set_xlim(0, err_max)
        # axes[param_index].set_ylim(0, err_rel.shape[0])
        axes[param_index].set_xlabel('Absolute error')
        latex_label_dlogLd = CONST.LATEX_LABELS['dlogL/d']
        latex_label = CONST.LATEX_LABELS[param_key]
        # axes[param_index].set_title(latex_label)
        axes[param_index].set_title(latex_label_dlogLd + latex_label)
    plt.tight_layout()
    fig.subplots_adjust(top=0.90)
    if show_plots:
        filepath = opts.outdir + f'/forward_vs_central_diff_offset{opts.offset}_abs'
        if False:
            filepath += '_squeezed'
        filepath += '.png'
        fig.savefig(filepath)
        cut.logger.info(f'Saving figure in {filepath}')
    else:
        plt.show()
