import numpy as np

low = 248345
high = 746315
digits = np.arange(10)
count = 0

list = []
for i in digits:
    for j in digits[i:]:
        for k in digits[j:]:
            for l in digits[k:]:
                for m in digits[l:]:
                    for n in digits[m:]:
                        # nb_str = ''.join([i,j,k,l,m,n])
                        nb_str = str(i) + str(j) + str(k) + str(l) + str(m) + str(n)
                        nb = int(nb_str)
                        intervalOK = low <= nb and nb <= high
                        consec = i==j or j==k or k==l or l==m or m==n
                        if consec and intervalOK:
                            mset = set([i,j,k,l,m,n])
                            list_of_occurrences = []
                            for item in mset:
                                nb_of_occurences = len(np.where(np.asarray([i,j,k,l,m,n]) == item)[0])
                                list_of_occurrences.append(nb_of_occurences)
                            if 2 in list_of_occurrences:
                                count +=1
                                list.append(nb)


255666
print(count)
