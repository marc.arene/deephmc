# Script meant to test the function `def Table_GradientFit(q_pos_traj, PointFit_Sort, n_pt_fit, n_param, n_fit_1, n_fit_2, SquaredScale, param_index, i_debug=-1)`.

# The numerical value of dlogL/dcosinc in the SUB02345678 case at step=65 of the phase3 trajectory is : -15.025623270048527. In the case of SUB0345678 it is = -32.7835897451223
import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import numpy as np
from sklearn import neighbors
import matplotlib.pyplot as plt

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.python_utils as pu
import Library.param_utils as paru
import Library.bilby_waveform as bilby_wv
import Library.bilby_utils as bilby_utils
import Library.CONST as CONST
import Library.python_utils as pu

import Library.sklearn_fit as sklf

def FindClosestPoints(q_pos_traj, PointFit_Sort, n_pt_fit, n_param, n_fit_1, param_index):
    """
    Returns the n_fit_1 closest points of the sorted table PointFit_Sort to the value q_pos_traj[param_index], where PointFit_Sort is already sorted wrt to PointFit_Sort[param_index].

    Parameter
    ---------
    q_pos_traj: array_like, shape = (n_param,)
        The parametrised vector position
    PointFit_Sort: array_like, shape = (n_param+1, n_pt_fit)
        The table of recorded points along numerical trajectories which we want to use for the fit; sorted wrt PointFit_Sort[param_index]. Its last line: PointFit_Sort[n_param] (hence the n_param+1's line) contains the values of the gradients of the points.
    n_pt_fit: int
        Number of fitting points
    n_param: int
        Number of parameters used in our model (9 or 15)
    n_fit_1: int
        Number of closest points selected. We actually keep n_fit_1 + 1 points.
    param_index: int
        Index of the line with respect to which PointFit_Sort is sorted. Note that PointFit_Sort should already be sorted that way

    Return
    ------
    Matrix_ClosestPoint: array_like, shape = (n_param, n_fit_1+1)
        Contains the n_fit_1+1 closest points.
    Vector_GradientClosestPoint: array_like, shape = (n_fit_1+1,)
        Contains the n_fit_1+1 corresponding values of the gradients

    Note
    ----
    If q_pos_traj[param_index] falls out close to the borders of PointFit_Sort[param_index], we can't select n_fit_1+1 points symmetrically left and right wrt its position. If so the indices which would go out of bounds select circularly on the other edge of the vector.

    """

    # Find the closest index for the searched parameter
    IndexClosest = np.searchsorted(PointFit_Sort[param_index], q_pos_traj[param_index]) - 1

    # Allocate vector and matrices for closest points in cos(inc) around IndexClosest
    limLeft = int(IndexClosest-(n_fit_1/2))         # left limit index
    limRight = int(IndexClosest+(n_fit_1/2))        # right limit index

    # import IPython; IPython.embed()
    # Select the n_fit_1 closest points. In python slicing is more natural.
    if 0 <= limLeft and limRight < n_pt_fit:
        Matrix_ClosestPoint = PointFit_Sort[0:n_param, limLeft:limRight+1]
        Vector_GradientClosestPoint = PointFit_Sort[n_param, limLeft:limRight+1]
    elif limLeft < 0:
        Matrix_ClosestPoint = np.concatenate((PointFit_Sort[0:n_param, n_pt_fit+limLeft:n_pt_fit], PointFit_Sort[0:n_param, 0:limRight+1]), axis=1)
        Vector_GradientClosestPoint = np.concatenate((PointFit_Sort[n_param, n_pt_fit+limLeft:n_pt_fit], PointFit_Sort[n_param, 0:limRight+1]))
    elif n_pt_fit <= limRight:
        Matrix_ClosestPoint = np.concatenate((PointFit_Sort[0:n_param, limLeft:n_pt_fit], PointFit_Sort[0:n_param, 0:limRight-n_pt_fit+1]), axis=1)
        Vector_GradientClosestPoint = np.concatenate((PointFit_Sort[n_param, limLeft:n_pt_fit], PointFit_Sort[n_param, 0:limRight-n_pt_fit+1]))

    return Matrix_ClosestPoint, Vector_GradientClosestPoint


def jac_quadratic(q_pos_traj):
    """
    Expression of the 2nd order jacobian

    Parameters
    ----------
    q_pos_traj : array_like.
        Position array in parameter space.
    Returns
    -------
    afunc : array_like.
        Vector containing the polynomial values of the cubic fit at position x. If x is of length m, then afunc should be of length 1 + m + m(m+1)/2 + m(m+1)(m+2)/6. 220 for m = 9.
    """

    l = 0
    n_param = len(q_pos_traj)
    m_fit = int(1 + n_param + n_param * (n_param + 1) / 2)

    afunc = [np.longdouble(0) for i in range(m_fit)]

    for i in range(n_param):
        afunc[l] = q_pos_traj[i]
        l+=1
    for i in range(n_param):
        for j in range(i, n_param):
            afunc[l] = q_pos_traj[i] * q_pos_traj[j]
            l+=1
    afunc[l] = 1.0
    return afunc

def jac_cubic(q_pos_traj):
    """
    Expression of the 2nd order jacobian

    Parameters
    ----------
    q_pos_traj : array_like.
        Position array in parameter space.
    Returns
    -------
    afunc : array_like.
        Vector containing the polynomial values of the cubic fit at position x. If x is of length m, then afunc should be of length 1 + m + m(m+1)/2 + m(m+1)(m+2)/6. 220 for m = 9.
    """

    l = 0
    n_param = len(q_pos_traj)
    m_fit = int(1 + n_param + n_param * (n_param + 1) / 2 +  n_param * (n_param + 1) * (n_param + 2) / 6)

    afunc = [np.longdouble(0) for i in range(m_fit)]

    for i in range(n_param):
        afunc[l] = q_pos_traj[i]
        l+=1
    for i in range(n_param):
        for j in range(i, n_param):
            afunc[l] = q_pos_traj[i] * q_pos_traj[j]
            l+=1
    for i in range(n_param):
        for j in range(i, n_param):
            for k in range(j, n_param):
                afunc[l] = q_pos_traj[i] * q_pos_traj[j] * q_pos_traj[k]
                l+=1
    afunc[l] = 1.0
    return afunc

def plot_num_and_ana_gradients(dlogL_num, dlogL_ana, opts_dict=None, save=True, show_plot=False, file_name=None, outdir='./plots'):
    from matplotlib import rc
    plt.rcParams['text.usetex'] = False
    plt.rcParams['figure.titlesize'] = 18
    plt.rcParams['figure.titleweight'] = 'bold'
    plt.rcParams['axes.titlesize'] = 15
    plt.rcParams['axes.labelsize'] = 12
    plt.rcParams['axes.titleweight'] = 'normal'
    plt.rcParams['xtick.labelsize'] = 12
    plt.rcParams['ytick.labelsize'] = 12
    plt.rcParams['font.size'] = 10

    steps = np.arange(len(dlogL_num[0]))

    # params_to_plot = [0]
    params_to_plot = list(range(dlogL_num.shape[0]))

    fig, axs = plt.subplots(nrows=len(params_to_plot), figsize=(15, 3 * dlogL_num.shape[0]))
    # fig, axs = plt.subplots(nrows=dlogL_num.shape[0], figsize=(15, 3 * dlogL_num.shape[0]))
    # fig, axs = plt.subplots(dlogL_num.shape[0], 1, figsize=(16,8.2))
    gradient_names = ['dlogL/d' + fpname for fpname in ['cos(inc)', 'psi', 'lnD']]

    title = ''
    file_name_default = ''
    if 'n1' in opts_dict.keys():
        title += 'n1 = {} - n2 = {}'.format(opts_dict['n1'], opts_dict['n2'])
        file_name_default += 'n1_{}_n2_{}'.format(opts_dict['n1'], opts_dict['n2'])
    if 'training' in opts_dict.keys():
        title = 'train_nph1 = {} - '.format(opts_dict['training']) + title
        file_name_default = 'train_{}_'.format(opts_dict['training']) + file_name_default
    if 'testing' in opts_dict.keys():
        title += ' - traj end PH1-{} - '.format(opts_dict['testing'])
        file_name_default += '_TrajEndPh1_{}'.format(opts_dict['testing'])

    for i, param_index in enumerate(params_to_plot):
    # for param_index in range(dlogL_num.shape[0]):
        axs[i].plot(steps, dlogL_num[param_index], label='numerical')
        axs[param_index].plot(steps, dlogL_ana[param_index], label='analytical along num trajectory')
        axs[i].set_xlabel("steps", color='black')
        axs[i].set_ylabel("{}".format(gradient_names[param_index]), color='black')
        axs[i].legend()


    fig.suptitle(title)
    fig.tight_layout()
    fig.subplots_adjust(top=0.95)

    if show_plot:
        plt.show()

    if file_name is None:
        file_name = file_name_default
    file_name = outdir + '/' + file_name + '.png'
    if save:
        pu.mkdirs(outdir)
        fig.savefig(file_name)

def get_OLUT(training_q_pos_traj, training_dlogL, param_index):
    # Sort tables according to cos(inc) using  numericalRecipes quicksort routine
    Sort_Array_Index = np.argsort(training_q_pos_traj.T[param_index]) # returns the indices of the array in the sorted order relative to cos(inc)
    PointFit_Sort = training_q_pos_traj[Sort_Array_Index].T # returns the values of the recorded pointed sorted by cos(inc) values. The transpose is just there to follow Yann's code.
    dlogL_olut_fit_Sort = training_dlogL.T[param_index][Sort_Array_Index].reshape(1, len(training_q_pos_traj)) # reshape is necessary for the concatenation below
    PointFit_Sort = np.concatenate((PointFit_Sort, dlogL_olut_fit_Sort)) # add as the last line the values of the gradient wrt to cos(inc)
    return PointFit_Sort





if __name__=='__main__':
    from optparse import OptionParser
    usage = """Script meant to test and improve the function `Table_GradientFit()`, ie the local OLUTs fit."""
    parser=OptionParser(usage)
    parser.add_option("--search_parameter_indices", default='02345678', action="store", type="string", help=""" Indices of the parameters to run the hmc on. Other are kept fixed. Default are values already stored in optsig.py file.""")
    parser.add_option("--training", default='1500', action="store", type="string", help=""" Number of trajectories in phase 1.""")
    parser.add_option("--testing", default='1500', action="store", type="string", help=""" Selects the testing set. It corresponds to the 1000 step trajectory that was run at the end of phase1.""")

    q_pos_traj_inj = [ -9.455251e-01, 4.712389, 2.21, 5.547252e+01, 1.729760e-01, -3.836786e-01, -3.968514e-01, 3.446160e+00, 3.708274e+00]


    (opts,args)=parser.parse_args()

    sampler_dict['search_parameter_indices'] = [int(i) for i in list(opts.search_parameter_indices)]

    directory = '../Tests/.OLUTs_test/SUB' + opts.search_parameter_indices + '/'
    input_dir = directory + 'input_data/'
    q_pos_traj = np.loadtxt(input_dir + 'q_pos_traj.dat')
    testing_dir = input_dir + 'testing_data/'
    training_dir = input_dir + 'training_data/'
    test_q_pos_traj = np.loadtxt(testing_dir + opts.testing + '_test_q_pos_traj.dat')
    test_dlogL = np.loadtxt(testing_dir + opts.testing + '_test_dlogL.dat')
    # PointFit_Sort = np.loadtxt(input_dir + 'PointFit_Sort.dat')
    training_q_pos_traj = np.loadtxt(training_dir + opts.training + '_training_q_pos_traj.dat')
    training_dlogL = np.loadtxt(training_dir + opts.training + '_training_dlogL.dat')
    SquaredScale = np.loadtxt(input_dir + 'SquaredScale.dat')
    scales = SquaredScale**0.5
    # scales = np.ones(SquaredScale.shape)
    Grad_true = np.loadtxt(input_dir + 'dlogL_dcosinc_true_i65.dat')
    n_pt_fit = training_q_pos_traj.shape[0]
    parameter_indices_to_plot = [0, 2, 3]
    # parameter_indices_to_plot = [0, 2, 3]
    # n_fit_1 = 2000
    # n_fit_2 = 200
    # n_fit_1 = 2000 * 10
    # n_fit_2 = 200 * 10
    n_fit_1_list = [2000, 10000, 50000]
    # n_fit_2_list = [50, 100, 150, 200]
    # n_fit_1_list = [2000, 5000, 10000, 20000, 50000, 100000, 290000]
    # n_fit_1_list = [500, 1000, 2000, 5000, 10000, 20000]
    # n_fit_2_list = np.linspace(n_fit_1/40, n_fit_1/10, 4)
    n_param = len(q_pos_traj)

    true_nearest_neighbors = False
    distance_wrt_all_params = False
    use_KNN = False
    higher_order_fit = False
    print("distance_wrt_all_params = {}".format(distance_wrt_all_params))
    print("use_KNN = {}".format(use_KNN))

    if False:
    # if i_debug >= 65 and param_index==0:
        import corner
        # bilby default corner kwargs. Overwritten by anything passed to kwargs
        defaults_kwargs = {}
        # defaults_kwargs = dict(bins=25, smooth=0.9, label_kwargs=dict(fontsize=16), title_kwargs=dict(fontsize=16), color='#0072C1', truth_color='tab:orange', plot_density=False, plot_datapoints=True, fill_contours=False, max_n_ticks=3)
        kwargs = {}
        kwargs['labels'] = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
        kwargs['plot_contours'] = False

        # kwargs['truths'] = q_pos_traj[sampler_dict['search_parameter_indices']]

        defaults_kwargs.update(kwargs)
        kwargs = defaults_kwargs

        # samples_train = training_q_pos_traj[:, sampler_dict['search_parameter_indices']]
        # figure_n1 = corner.corner(samples_train, **kwargs)
        # figure_n1.savefig(training_dir + opts.training + '_corner.png', dpi=300)
        #
        # samples_test = test_q_pos_traj[:, sampler_dict['search_parameter_indices']]
        # figure_n1 = corner.corner(samples_test, **kwargs)
        # figure_n1.savefig(testing_dir + opts.testing + '_corner.png', dpi=300)
        samples_train = training_q_pos_traj[:, sampler_dict['search_parameter_indices']]
        # figure_train = corner.corner(samples_train, **kwargs)
        figure_train = corner.corner(samples_train, **kwargs)
        # extract the range of each parameter
        axes = np.array(figure_train.axes).reshape((samples_train.shape[1],samples_train.shape[1]))
        range = []
        for ax in axes.diagonal():
            range.append((ax.dataLim.xmin, ax.dataLim.xmax))

        samples_test = test_q_pos_traj[:, sampler_dict['search_parameter_indices']]
        figure_combine = corner.corner(samples_test, range=range, fig=figure_train, color='red', no_fill_contours=True, **kwargs)
        figure_combine.savefig(testing_dir + '{}_corner_train_{}.png'.format(opts.testing, opts.training), dpi=300)

        sys.exit()

    print("\nCreating OLUTs...")
    PointFit_Sort_CosInc = get_OLUT(training_q_pos_traj, training_dlogL, 0)
    PointFit_Sort_Psi = get_OLUT(training_q_pos_traj, training_dlogL, 2)
    PointFit_Sort_lnD = get_OLUT(training_q_pos_traj, training_dlogL, 3)

    PointFit_Sort = np.array([PointFit_Sort_CosInc, PointFit_Sort_Psi, PointFit_Sort_lnD])

    opts_fit = dict(training = opts.training, testing = opts.testing)
    for n_fit_1 in n_fit_1_list:
        print("\nn_fit_1 = {}".format(n_fit_1))
        n_fit_2_list = np.linspace(n_fit_1/40, n_fit_1/10, 4)
        for n_fit_2 in n_fit_2_list:
            n_fit_2 = int(n_fit_2)
            print(" n_fit_2 = {}".format(n_fit_2))
            opts_fit['n1'] = n_fit_1
            opts_fit['n2'] = n_fit_2
            dlogL_traj_predicted = np.zeros((len(parameter_indices_to_plot), len(test_q_pos_traj)))
            for ii, param_index in enumerate(parameter_indices_to_plot):
                for step, q_pos_traj in enumerate(test_q_pos_traj):
                    if true_nearest_neighbors:
                        distance_all_params = np.zeros(PointFit_Sort[:-1,:].shape)
                        for i in sampler_dict['search_parameter_indices']:
                            if i != param_index:
                                distance_all_params[i] = (PointFit_Sort[i] - q_pos_traj[i])**2/SquaredScale[i]
                        distance = distance_all_params.sum(axis=0)
                        distance_closest_index_n1 = np.argpartition(distance, n_fit_1)[:n_fit_1]

                        Matrix_ClosestPoint = PointFit_Sort[:9, distance_closest_index_n1]
                        Vector_GradientClosestPoint = PointFit_Sort[9, distance_closest_index_n1]

                        distance_closest_index_n2 = np.argpartition(distance, n_fit_2)[:n_fit_2]

                        pt_Y = PointFit_Sort[:9, distance_closest_index_n2].T
                        dlogL_Y = PointFit_Sort[9, distance_closest_index_n2]

                    else:
                        # n_fit_1 = 20000
                        Matrix_ClosestPoint, Vector_GradientClosestPoint = FindClosestPoints(q_pos_traj, PointFit_Sort[ii], n_pt_fit, n_param, n_fit_1, param_index)

                        # So now we have the n_fit_1 closest points wrt q_pos_traj[param_index]. Instead of using them directly for the fit, we are going to select out of them a n_fit_2 subset of the closest points in terms of their euclidian distance wrt to the other two parameters which are correlated either:
                        # - (psi, logD)         if param = cos(inc),     ie param_index = 0
                        # - (cos(inc), logD)     if param = psi,         ie param_index = 2
                        # - (cos(inc), psi)     if param = logD,         ie param_index = 3
                        if distance_wrt_all_params:
                            distance_all_params = np.zeros(Matrix_ClosestPoint.shape)

                            distance_all_params_unscaled = np.zeros(Matrix_ClosestPoint.shape)
                            local_scales_sqrd = np.zeros(SquaredScale.shape)
                            distance_all_params_2 = np.zeros(Matrix_ClosestPoint.shape)
                            for i in sampler_dict['search_parameter_indices']:
                                # It's actually useless to compute the distance in the dimension the OLUT is sorted with since they will be very small compared to the distances in the other dimensions
                                if i != param_index:
                                    distance_all_params_unscaled[i] = (Matrix_ClosestPoint[i] - q_pos_traj[i])**2
                                    local_scales_sqrd[i] = distance_all_params_unscaled[i].mean()
                                    distance_all_params_2[i] = distance_all_params_unscaled[i] / local_scales_sqrd[i]

                                    distance_all_params[i] = (Matrix_ClosestPoint[i] - q_pos_traj[i])**2/SquaredScale[i]

                            distance_closest = np.sqrt(distance_all_params.sum(axis=0))
                            distance_closest_2 = np.sqrt(distance_all_params_2.sum(axis=0))
                        elif use_KNN:
                            # n_neighbors = n_fit_2
                            # Matrix_ClosestPoint = PointFit_Sort[:9]
                            # Vector_GradientClosestPoint = PointFit_Sort[9]
                            # for n_neighbors in [200]:
                            # n_neighbors = n_fit_2
                            # weights = 'distance'
                            # # X = Matrix_ClosestPoint.T[:, sampler_dict['search_parameter_indices']] / scales[sampler_dict['search_parameter_indices']]
                            # X = Matrix_ClosestPoint.T[:, sampler_dict['search_parameter_indices']]
                            # Y = Vector_GradientClosestPoint
                            # # knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
                            # knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights, algorithm='brute')
                            # knn_fit = knn.fit(X, Y)
                            # Grad_knn = knn_fit.predict([q_pos_traj[sampler_dict['search_parameter_indices']]])[0]


                            weights = 'uniform'
                            params_to_consider = sampler_dict['search_parameter_indices']
                            # params_to_consider = [2,3]
                            X = Matrix_ClosestPoint.T[:, params_to_consider]
                            Y = Vector_GradientClosestPoint
                            # knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
                            for n_neighbors in np.arange(10, 2000, 50):
                                knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights, algorithm='auto')
                                knn_fit = knn.fit(X, Y)
                                Grad_knn = knn_fit.predict([q_pos_traj[params_to_consider]])[0]
                                print("    {:4}: Grad_knn = {:.3}".format(n_neighbors, Grad_knn))



                            # import IPython; IPython.embed(); sys.exit()
                            # %timeit knn_fit = knn.fit(X, Y)
                            # print("weights = {}".format(weights))
                            # print("n_neighbors = {}".format(n_neighbors))
                            # err = abs((Grad_knn - Grad_true)/Grad_true)
                            # print("{}: Grad_knn = {}".format(n_neighbors, Grad_knn))
                            # print("    {:4}: err = {:6.1%}".format(n_fit_2, err))

                            # neigh = neighbors.NearestNeighbors(n_neighbors=n_neighbors)
                            # neigh.fit(X)
                            # dist_nn, idx_nn = neigh.kneighbors([q_pos_traj[sampler_dict['search_parameter_indices']] / scales[sampler_dict['search_parameter_indices']]])
                            breakpoint()
                            continue
                        else:
                            if param_index==0: # cos(inc)
                                other_param_index_1 = 2 # psi
                                other_param_index_2 = 3 # logD
                            elif param_index==2: # psi
                                other_param_index_1 = 0 # cos(inc)
                                other_param_index_2 = 3 # logD
                            elif param_index==3: # logD
                                other_param_index_1 = 0 # cos(inc)
                                other_param_index_2 = 2 # psi

                            if other_param_index_1 in sampler_dict['search_parameter_indices']:
                                d2_other_param_1 = (Matrix_ClosestPoint[other_param_index_1] - q_pos_traj[other_param_index_1])**2/SquaredScale[other_param_index_1]
                            else:
                                d2_other_param_1 = np.zeros_like(Matrix_ClosestPoint[0])

                            if other_param_index_2 in sampler_dict['search_parameter_indices']:
                                d2_other_param_2 = (Matrix_ClosestPoint[other_param_index_2] - q_pos_traj[other_param_index_2])**2/SquaredScale[other_param_index_2]
                            else:
                                d2_other_param_2 = np.zeros_like(Matrix_ClosestPoint[0])

                            distance_closest = np.sqrt(d2_other_param_1 + d2_other_param_2)
                        # n_fit_2 = 20
                        # if i_debug >= 65 and param_index==0: breakpoint()
                        distance_closest_index_n2 = np.argpartition(distance_closest, n_fit_2)[:n_fit_2]


                        # Get the n_fit_2 closest points in terms of distance
                        pt_Y = Matrix_ClosestPoint[:, distance_closest_index_n2].T # shape=(n_fit_2, n_param)
                        dlogL_Y = Vector_GradientClosestPoint[distance_closest_index_n2]

                    # Define the number of coefficients used for the fit depending on the option
                    m_fit_global = 10

                    # Barycenter method
                    if False:
                        distance_closest_n2 = distance_closest[distance_closest_index_n2]
                        weights_not_normed = 1 / distance_closest_n2
                        weights = weights_not_normed / weights_not_normed.sum()
                        Grad_bary = (dlogL_Y * weights).sum()
                        return Grad_bary

                    # KNeighborRegressor trials
                    if False:
                        # n_neighbors = n_fit_2
                        # Matrix_ClosestPoint = PointFit_Sort[:9]
                        # Vector_GradientClosestPoint = PointFit_Sort[9]
                        # for n_neighbors in [200]:
                        for n_neighbors in np.arange(50,300,5):
                            n_neighbors = n_fit_2
                            weights = 'distance'
                            X = Matrix_ClosestPoint.T
                            Y = Vector_GradientClosestPoint
                            # knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
                            knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights, algorithm='brute')
                            knn_fit = knn.fit(X, Y)
                            Grad_knn = knn_fit.predict([q_pos_traj])[0]
                            import IPython; IPython.embed(); sys.exit()
                            # %timeit knn_fit = knn.fit(X, Y)
                            # print("weights = {}".format(weights))
                            # print("n_neighbors = {}".format(n_neighbors))
                            res = abs(Grad_knn - Grad_true)
                            # print("{}: Grad_knn = {}".format(n_neighbors, Grad_knn))
                            print("{}: res = {}".format(n_neighbors, res))
                        # breakpoint()
                        sys.exit()
                        np.intersect1d(distance_closest_index_n2, knn.kneighbors()[1]).shape
                    if False:
                        n_neighbors = n_fit_2
                        weights = 'uniform'
                        X = pt_Y
                        Y = dlogL_Y
                        knn = neighbors.KNeighborsRegressor(n_neighbors, weights=weights)
                        knn_fit = knn.fit(X, Y)
                        Grad_knn = knn_fit.predict([q_pos_traj])[0]
                        print("weights = {}".format(weights))
                        print("n_neighbors = {}".format(n_neighbors))
                        print("Grad_knn = {}".format(Grad_knn))
                    # print("higher_order_fit = {}".format(higher_order_fit))
                    if higher_order_fit:
                        order = 3
                        print("order = {}".format(order))
                        if order == 2:
                            jac_func = jac_quadratic
                        else:
                            jac_func = jac_cubic

                        jac_pt_Y = []

                        for point in pt_Y[:, sampler_dict['search_parameter_indices']]:
                            jacobian = jac_func(point)
                            # jacobian = jac_cubic(point)
                            jac_pt_Y.append(jacobian)
                        A = np.asarray(jac_pt_Y, dtype=np.float64)
                        jac_q_pos_traj = jac_func(q_pos_traj[sampler_dict['search_parameter_indices']])

                    ########################################################################################################
                    # MY FIRST VERSION
                    # # Do the linear fit
                    # Since we will do a linear fit, we add a column of ones; hence the set of equations we aim to solve is A @ x = dlogL_Y
                    else:
                        A = np.append(pt_Y[:, sampler_dict['search_parameter_indices']], np.ones((pt_Y.shape[0],1)), axis=1)
                        A = np.asarray(A, dtype=np.float64)
                        jac_q_pos_traj = np.append(q_pos_traj[sampler_dict['search_parameter_indices']],1)
                    # Now do the singular value decomposition on A. Since we are only interested in the pseudo_inverse, we don't need to compute all components of u, hence we save so time using the option full_matrices=False. This returns a matrice u of shape (n_fit_2, m_fit_global) instead of (n_fit_2, n_fit_2).
                    u, s, vh = np.linalg.svd(A, full_matrices=False)

                    # Retreive the indices of s where its values are too small, corresponding to a degeneracy in the columns of A (meaning some columns can be equated to others by linear combinations). See Numerical Recipes>SVD for detailed explanations. Basically those small values mean A has a nullspace (Ker{A}), and trying to invert them in s would throw off very large numbers with important round-off errors. These big numbers would pull off the searched solution: fit_coeff towards the nullspace and in the end the chi-squared merit result would not be the smallest possible. Equating those to zero in the inverse matrix basically unables us to get rid of those very linear combinations which make A degenerate.
                    # The criteria for being too small, named formally the "condition number of a matrix", is for the relative precision of the number: s/s.max to be smaller than the machine floating-point precision. For python, we here deal with float64 number, which precision is given (and explained here: https://docs.python.org/3/tutorial/floatingpoint.html, https://fr.wikipedia.org/wiki/Epsilon_d%27une_machine#cite_ref-b_2-0) to be 2**-52 ~ 2.22e-16. We take here 1.0e-15
                    TOL = 1.0e-15
                    idx_singular = np.where(s < s.max() * TOL)[0] #[0] is added because np.where() throws a weird structure I want to get rid of.
                    s_inv = 1/s
                    # Now set those values to zero in the inverse matrix:
                    s_inv[idx_singular] = np.zeros(len(idx_singular))
                    vhT_sinv = np.matmul(vh.T, np.diag(s_inv), dtype=np.longdouble)
                    pseudo_inverse = np.matmul(vhT_sinv, u.T, dtype=np.longdouble)
                    # pseudo_inverse = np.matmul(vh.T * s_inv, u.T, dtype=np.longdouble)
                    fit_coeff = np.matmul(pseudo_inverse, dlogL_Y, dtype=np.longdouble)
                    Grad = np.matmul(fit_coeff, jac_q_pos_traj, dtype=np.longdouble)
                    dlogL_traj_predicted[ii, step] = Grad
                    # res_pts = (A @ fit_coeff - dlogL_Y)**2
                    # res_sum = res_pts.sum()
                    # res = np.sqrt(res_sum)/res_pts.shape[0]
                    # ss = ((dlogL_Y - dlogL_Y.mean())**2).sum()
                    # R2 = 1 - res_sum / ss
                    # # print("Grad = {}".format(Grad))
                    # # print("R2 = {}".format(R2))
                    # # print("res = {}".format(res))
                    # err = abs((Grad - Grad_true)/Grad_true)

                    # print("    {:4}: err = {:6.1%} - R2 = {:.4f}".format(n_fit_2, err, R2))
                    # pseudo_inverse = vh.T * s_inv @ u.T
                    # fit_coeff = pseudo_inverse @ dlogL_Y
                    # Grad = fit_coeff @ np.append(q_pos_traj,1)
                    ########################################################################################################
                    if False:
                    # if i_debug >= 65 and param_index==0:
                        import corner
                        # bilby default corner kwargs. Overwritten by anything passed to kwargs
                        defaults_kwargs = {}
                        # defaults_kwargs = dict(bins=25, smooth=0.9, label_kwargs=dict(fontsize=16), title_kwargs=dict(fontsize=16), color='#0072C1', truth_color='tab:orange', plot_density=False, plot_datapoints=True, fill_contours=False, max_n_ticks=3)
                        kwargs = {}
                        kwargs['labels'] = [CONST.TRAJ_PARAMETERS_KEYS[i] for i in sampler_dict['search_parameter_indices']]
                        kwargs['plot_contours'] = False

                        kwargs['truths'] = q_pos_traj[sampler_dict['search_parameter_indices']]

                        defaults_kwargs.update(kwargs)
                        kwargs = defaults_kwargs

                        samples_n1 = Matrix_ClosestPoint[sampler_dict['search_parameter_indices'], :].T
                        figure_n1 = corner.corner(samples_n1, **kwargs)
                        figure_n1.savefig(directory + 'i{}_{}pts.png'.format(i_debug, samples_n1.shape[0]), dpi=300)

                        samples_n2 = pt_Y[:, sampler_dict['search_parameter_indices']]
                        figure_n2 = corner.corner(samples_n2, **kwargs)
                        figure_n2.savefig(directory + 'i{}_{}pts.png'.format(i_debug, samples_n2.shape[0]), dpi=300)

                    # if step==0: breakpoint()
            plot_num_and_ana_gradients(test_dlogL.T[parameter_indices_to_plot,:], dlogL_traj_predicted, save=True, opts_dict=opts_fit, show_plot=False, outdir=directory + 'plots_num_vs_ana')
