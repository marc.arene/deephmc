# python time_waveform_gen.py --event=GW170817 --trigger_file=../examples/trigger_files/GW170817_IMRPhenomD_NRTidal.ini

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from matplotlib import rcParams
rcParams["text.usetex"] = True
rcParams["font.serif"] = "Computer Modern Roman"
rcParams["font.family"] = "serif"
rcParams['text.latex.preamble'] = r'\providecommand{\mathdefault}[1][]{}'

# Standard python numerical analysis imports:
from scipy import signal
from scipy.interpolate import interp1d
from scipy.signal import butter, filtfilt, iirdesign, zpk2tf, freqz
import h5py
import json

# the IPython magic below must be commented out in the .py file, since it doesn't work there.
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

import sys
sys.path.append('/Users/marcarene/projects/python') # Necessary to import gwhmc
sys.path.append('/Users/marcarene/projects/python/gwhmc') # Necessary because of the `import Library` in gwhmc modules ...
sys.path.append('/Users/marcarene/projects/python/gwhmc/__ROM_data') # Necessary because of the `import Library` in gwhmc modules ...

import os
import numpy as np
import time
from optparse import OptionParser
from configparser import ConfigParser
import pandas as pd

import bilby
import Library.python_utils as pu
import Codes.set_injection_parameters as set_inj
import gw.detector.networks as gwdn
import core.utils as cut
from bilby.gw.utils import noise_weighted_inner_product as nwip


def timing_dlogL():
    # IMRPhenomPv2-NRTidal, 128sec
    n_ifos = 3
    swg = 120
    fh = 3.7
    exp2pifdt = 8.4
    stimesexp = 1.4
    nwip = 2
    # 12-D
    n_swg = 20
    n_fh = 21 * n_ifos
    n_exp2pifdt = 1 * n_ifos
    n_stimesexp = 21 * n_ifos
    n_nwip = 34 * n_ifos


    # IMRPhenomPv2-NRTidal, 512sec
    n_ifos = 3
    swg = 481
    fh = 13.3
    exp2pifdt = 38.5
    stimesexp = 7.8
    nwip = 9.8
    # 12-D
    n_swg = 20
    n_fh = 21 * n_ifos
    n_exp2pifdt = 1 * n_ifos
    n_stimesexp = 21 * n_ifos
    n_nwip = 34 * n_ifos
    # 16-D
    n_swg = 20
    n_fh = 26 * n_ifos
    n_exp2pifdt = 5 * n_ifos
    n_stimesexp = 26 * n_ifos
    n_nwip = 43 * n_ifos



    dlogL_timing = n_swg*swg + n_fh*fh + n_exp2pifdt*exp2pifdt + n_stimesexp*stimesexp + n_nwip*nwip
    traj_timing = dlogL_timing * 200 / 1000 / 60
    phase1_timing = traj_timing * 1500 / 60 / 24


if __name__ == '__main__':
    usage = """%prog [options]
    To be written ..."""
    parser=OptionParser(usage)

    parser.add_option("--event", default='GW170817', action="store", type="string", help="""Event label part of the catalog GWTC-1""")
    parser.add_option("--trigger_file", default=None, action="store", type="string", help="""File .ini describing the parameters where to start the chain. If left to None and --event used, a default file corresponding to the event will be used.""")
    parser.add_option("--trigger_time", default='../examples/trigger_files/GW170817_IMRPhenomD.ini', action="store", type="string", help="""Trigger GPS time.""")
    parser.add_option("--config_file", default='../Codes/config_default.ini', action="store", type="string", help="""Configuration file path leading to the `.ini` file specifying some key options to run the HMC. The default file is '../Codes/config_default.ini'. Options set in the command line which are redundant with that of the config.ini will (should...) overwrite them.""")

    # randGen = np.random.RandomState(seed=2)

    # PARSE INPUT ARGUMENTS
    (opts, args) = parser.parse_args()
    trigger_file = opts.trigger_file
    trigger_parameters = set_inj.ini_file_to_dict(trigger_file)
    geocent_time = trigger_parameters['geocent_time']



    # INPUT PARAMS
    # to get the list of approximant names available, do:
    # import lalsimulation as lalsim
    # for i in range(100):
    #   print(lalsim.GetStringFromApproximant(i))
    approximant = 'TaylorF2'
    approximant = 'IMRPhenomD'
    approximant = 'IMRPhenomD_NRTidal'
    # approximant = 'IMRPhenomPv2'
    # approximant = 'IMRPhenomPv2_NRTidal'
    # approximant = 'SEOBNRv4'
    # ROM file can be found here:https://git.ligo.org/lscsoft/lalsuite-extra/-/tree/master/data/lalsimulation
    # approximant = 'SEOBNRv4_ROM'
    ifos = ['H1', 'L1', 'V1']
    sampling_frequency = 4096
    maximum_frequency = sampling_frequency/2
    reference_frequency = 20
    minimum_frequency = 30
    mass_1 = trigger_parameters['mass_1']
    mass_2 = trigger_parameters['mass_2']
    tc_3p5PN = set_inj.ComputeChirpTime3p5PN(minimum_frequency, mass_1, mass_2)
    t_end_minus_tc = 2
    # duration = int(tc_3p5PN) + 1 + t_end_minus_tc
    duration = set_inj.next_power_of_two(tc_3p5PN + t_end_minus_tc)
    start_time = geocent_time + t_end_minus_tc - duration

    dict_toprint = dict(approximant=approximant, sampling_frequency=sampling_frequency, maximum_frequency=maximum_frequency, reference_frequency=reference_frequency, minimum_frequency=minimum_frequency, duration=duration, start_time=start_time, geocent_time=geocent_time)
    dict_toprint_fmt = cut.dictionary_to_formatted_string(dict_toprint)
    print(dict_toprint_fmt)










    # INITIALIZE THE THREE INTERFEROMETERS
    # interferometers = bilby.gw.detector.InterferometerList(ext_analysis_dict['ifos'])
    interferometers = gwdn.InterferometerList(ifos)

    # SET WAVEFORM GENERATOR FOR SNR COMPUTATION
    frequency_domain_source_model = bilby.gw.source.lal_binary_neutron_star
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters
    if opts.event == 'GW150914':
        frequency_domain_source_model = bilby.gw.source.lal_binary_black_hole
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters

    waveform_arguments = {}
    waveform_arguments['minimum_frequency'] = minimum_frequency
    waveform_arguments['waveform_approximant'] = approximant
    waveform_arguments['reference_frequency'] = reference_frequency
    waveform_arguments['maximum_frequency'] = maximum_frequency
    # waveform_arguments['maximum_frequency'] = 4096
    # make waveform generator
    waveform_generator = bilby.gw.waveform_generator.WaveformGenerator(
        duration=duration,
        sampling_frequency=sampling_frequency,
        frequency_domain_source_model=frequency_domain_source_model,
        waveform_arguments=waveform_arguments,
        parameter_conversion=parameter_conversion)

    waveform_polarizations = waveform_generator.frequency_domain_strain(trigger_parameters)
    hplus = waveform_polarizations['plus']
    hcross = waveform_polarizations['cross']
    hplus.shape[0] / duration

    for i, ifo in enumerate(interferometers):
        ifo.minimum_frequency = minimum_frequency
        ifo.maximum_frequency = maximum_frequency


        # SNR COMPUTATION
        ifo.set_strain_data_from_zero_noise(
            sampling_frequency=sampling_frequency,
            duration=duration,
            start_time=start_time
            )

    # ifo = interferometers[0]
    template_fft = ifo.get_detector_response(waveform_polarizations, trigger_parameters)
    fake_psd = np.random.uniform(size=template_fft.size) * 1e-43
    nwip(template_fft, template_fft, fake_psd, duration)

    import IPython; IPython.embed(); sys.exit()












    # The minimun frequency for each interferometer needs to be set if we want the strain derived from the psd to start being non zero at that frequency and not at the default one which is 20Hz
    fig, ax = plt.subplots(1,1, figsize=(14, 7))
    kwargs = dict(
        fontsize_label=22,
        fontsize_tick=15,
        fontsize_legend=15,
        color='black',
        max_n_ticks=8,
        linewidth=3
    )
    ifo_realnames = ['Handford', 'Livingston', 'Virgo']
    for i, ifo in enumerate(interferometers):
        # ifo.minimum_frequency = minimum_frequency
        # ifo.maximum_frequency = maximum_frequency
        #
        #
        # # SNR COMPUTATION
        # ifo.set_strain_data_from_zero_noise(
        #     sampling_frequency=sampling_frequency,
        #     duration=duration,
        #     start_time=start_time
        #     )
        template_fft = ifo.get_detector_response(waveform_polarizations, trigger_parameters)
        template = np.fft.irfft(template_fft) * ifo.strain_data.sampling_frequency
        time_array = np.linspace(start_time, start_time + duration, template.size)
        ax.plot(time_array - geocent_time, template, linewidth=kwargs['linewidth'], label=f'{ifo_realnames[i]}')

    xlabel = f'Time since $t_c$ = {geocent_time} (sec)'
    ylabel = 'Strain'
    fontsize_label = kwargs['fontsize_label']
    ax.set_xlabel(xlabel, fontsize=fontsize_label, labelpad=int(0.5*fontsize_label))
    ax.set_ylabel(ylabel, fontsize=fontsize_label, labelpad=int(0.5*fontsize_label))
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(kwargs['fontsize_tick'])
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(kwargs['fontsize_tick'])
    offset_x = ax.xaxis.get_offset_text()
    offset_x.set_size(kwargs['fontsize_tick'])
    offset_y = ax.yaxis.get_offset_text()
    offset_y.set_size(kwargs['fontsize_tick'])
    ax.xaxis.set_major_locator(MaxNLocator(kwargs['max_n_ticks'], prune="lower"))
    ax.yaxis.set_major_locator(MaxNLocator(kwargs['max_n_ticks'], prune="lower"))
    plt.legend(fontsize=kwargs['fontsize_legend'])
    plt.tight_layout()
    if opts.event == 'GW170817':
        xlim = [-0.015, 0.020]
        # xlim = [-56.9, 0.02]
        # xlim = [-2, 0.02]
        # xlim = [-2, -0.015]
    if opts.event == 'GW150914':
        xlim = [-0.05, 0.03]
    plt.xlim(xlim)
    # plt.xlim([-0.4, 0.3])
    # plt.xlim([-0.02, 0.015])
    # plt.show()
    filename = f'./{opts.event}_td_only_{approximant}.png'
    # filename = f'../images/{opts.event}_td_only.pdf'
    plt.savefig(filename)
