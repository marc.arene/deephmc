# This script is meant to shed the light on a steppy behavior of the logL as a function of declination and right-ascension at small scales.
# Running this script with `--stepsize=1e-6` shows this.
# Note that this behavior does not appear when varying the other parameters.

import numpy as np
import matplotlib.pyplot as plt
import bilby


from scipy import signal
import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
plt.rc('text', usetex=False)
plt.rc('font', family='serif')
plt.rc('font', size=20)
plt.rc('font', weight='bold')


SOL = 2.99792458e8                                       # Speed of light / m/s
MSOLAR = 1.9884e30                                       # Mass of the sun / kg
G = 6.67428e-11                                          # Newtons Constant
GEOM = (G * MSOLAR / (SOL*SOL*SOL))                      # mass to seconds conversion

ifo_colors = ['red', 'green', 'purple']


def Compute_fISCO(Rmin, m1, m2):
    """
    Compute the frequency at the Innermost Stable Circular Orbit
    """

    vISCO = np.sqrt(1.0 / Rmin)
    Mt = m1 + m2
    fmax = vISCO**3 / (np.pi * GEOM * Mt)      # fmax signal
    return fmax

def set_parameters():
    """
    Set the parameters for the run
    """
    # Values found for GW170817
    injection_parameters = dict(
        mass_1=1.47,
        mass_2=1.27,
        chi_1=0.0,
        chi_2=0.0,
        luminosity_distance=40,
        # iota=2.81,
        theta_jn=2.81,
        psi=2.21,
        phase=5.497787143782138,
        geocent_time=1187008882.43,
        ra=3.44616, # value from fixed skyposition run
        dec=-0.408084, # value from fixed skyposition run
        lambda_1=0.,
        lambda_2=0.
        )

    injection_parameters = bilby.gw.conversion.generate_mass_parameters(injection_parameters)

    m1 = injection_parameters['mass_1']
    m2 = injection_parameters['mass_2']

    # I would normally compute the chirp time associated to these parameters with a function involving  PN coefficients, but since I want this code to be standalone I will hard code the chirp time for those GW170817 parameters:
    # tc_3p5PN = ComputeChirpTime3p5PN(minimum_frequency, m1, m2)
    tc_3p5PN = 56.91538269948342

    # We integrate the signal up to the frequency of the "Innermost stable circular orbit (ISCO)"
    R_isco = 6.      # Orbital separation at ISCO, in geometric units. 6M for PN ISCO; 2.8M for EOB

    fISCO = Compute_fISCO(R_isco, m1, m2)

    duration = tc_3p5PN + 2
    sampling_frequency = 2 * fISCO
    start_time = injection_parameters['geocent_time'] - tc_3p5PN

    return injection_parameters, start_time, sampling_frequency, duration

def plot_time_domain_strain(interferometers):

    plt.figure(figsize=(15,5))

    for ifo_index, ifo in enumerate(interferometers):
        h_fd = ifo.frequency_domain_strain
        dwindow = signal.tukey(h_fd.size, alpha=1./8)
        h_td = np.fft.irfft(h_fd * dwindow)
        # h_td = np.fft.irfft(h_fd)
        # h_td_unwindowed = h_td / dwindow

        number_of_samples = len(h_td)
        duration_td = number_of_samples / interferometers.sampling_frequency

        time = np.linspace(start=0, stop=duration_td, num=number_of_samples)

        plt.plot(time, h_td, linewidth=1, color=ifo_colors[ifo_index], label=ifo.name)

    plt.xlabel("Time (sec)")
    plt.ylabel("strain")
    plt.legend()
    plt.title('Time domain strain data with noise in each interferometer')
    # plt.show()

def plot_psds(interferometers, minimum_frequency):

    plt.figure(figsize=(10,6))
    f_low = minimum_frequency
    f_high = interferometers.frequency_array[-1]

    indices_file_array_ifos = []
    for ifo_index, ifo in enumerate(interferometers):
        indices_file_array = np.where((f_low<ifo.power_spectral_density.frequency_array) & (ifo.power_spectral_density.frequency_array<f_high))[0]

        indices_file_array_ifos.append(indices_file_array)
        # plt.loglog(ifo.frequency_array[indices], ifo.psd_array[indices], label=ifo.name, color=ifo_colors[ifo_index])
        plt.loglog(ifo.power_spectral_density.frequency_array[indices_file_array_ifos[ifo_index]], ifo.power_spectral_density.psd_array[indices_file_array_ifos[ifo_index]], label=ifo.name,  color=ifo_colors[ifo_index])
    plt.xlabel("frequency (Hz)")
    plt.ylabel("PSD")
    plt.title('Power Spectral Densities used for each interferometers')
    plt.legend()
    # plt.show()

def plot_time_domain_injected_waveform(interferometers, source_frame_polarizations):

    ifo_colors = ['red', 'green', 'purple']
    plt.figure(figsize=(15,5))

    for ifo_index, ifo in enumerate(interferometers):
        h_fd = ifo.get_detector_response(source_frame_polarizations, injection_parameters)
        dwindow = signal.tukey(h_fd.size, alpha=1./8)
        h_td = np.fft.irfft(h_fd * dwindow)
        # h_td = np.fft.irfft(h_fd)
        # h_td_unwindowed = h_td / dwindow

        number_of_samples = len(h_td)
        duration_td = number_of_samples / sampling_frequency

        time = np.linspace(start=0, stop=duration_td, num=number_of_samples)

        plt.plot(time, h_td, linewidth=1, color=ifo_colors[ifo_index], label=ifo.name)

    plt.xlabel("Time (sec)")
    plt.ylabel("strain")
    plt.legend()
    plt.title('Injected waveforms projected onto each interferometers')
    # plt.show()


if __name__=='__main__':

    from optparse import OptionParser
    usage = """%prog [options]
    """
    parser=OptionParser(usage)
    parser.add_option("--minimum_frequency", default=30.0, action="store", type="float", help=""" Low frequency cut-off""")
    # parser.add_option("--ifos", default='123', action="store", type="string", help=""" List of interferometers to make the plot on.""")

    (opts,args)=parser.parse_args()

    minimum_frequency = opts.minimum_frequency

    np.random.seed(88170235)

    # SET THE INJECTION PARAMETERS
    injection_parameters, start_time, sampling_frequency, duration = set_parameters()

    # INITIALIZE THE THREE INTERFEROMETERS
    # ifos_list = ['H1']
    ifos_list = ['H1', 'L1', 'V1']
    interferometers = bilby.gw.detector.InterferometerList(ifos_list)
    for ifo in interferometers:
        ifo.minimum_frequency = minimum_frequency
        ifo.strain_data.duration = duration
        ifo.strain_data.sampling_frequency = sampling_frequency
        ifo.strain_data.start_time = start_time


    # SET NOISE STRAIN DATA FROM PSD
    interferometers.set_strain_data_from_power_spectral_densities(
        sampling_frequency=sampling_frequency,
        duration=duration,
        start_time=start_time)

    # CREATE THE WAVEFORM GENERATOR NEEDED FOR BILBY
    # Fixed arguments passed into the source model.
    waveform_arguments = dict(waveform_approximant='TaylorF2',
                              reference_frequency=0, minimum_frequency=minimum_frequency, f_max=0)
    # Create the waveform_generator using a LAL Binary Neutron Star source function
    waveform_generator = bilby.gw.WaveformGenerator(
        duration=interferometers.duration,
        sampling_frequency=interferometers.sampling_frequency,
        frequency_domain_source_model=bilby.gw.source.lal_binary_neutron_star,
        parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_neutron_star_parameters,
        waveform_arguments=waveform_arguments
        )

    # INJECTION THE GW170817 LIKE SIGNAL
    interferometers.inject_signal(parameters=injection_parameters, waveform_generator=waveform_generator)


    # CALL THE PLOT FUNCTIONS
    plot_time_domain_strain(interferometers)

    plot_psds(interferometers, minimum_frequency)

    # We need to regenerate the waveforms for this plot
    source_frame_polarizations = waveform_generator.frequency_domain_strain(injection_parameters)
    plot_time_domain_injected_waveform(interferometers, source_frame_polarizations)


    # SHOW THE PLOTS
    plt.show()
