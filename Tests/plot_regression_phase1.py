# get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
# import seaborn as sns; sns.set()
import numpy as np
import pandas as pd
from configparser import ConfigParser

from sklearn import neighbors, svm, ensemble, linear_model, metrics, model_selection, preprocessing

import IPython as ip
import sys
sys.path.append('../')
import Library.CONST as CONST
import Library.python_utils as pu

import Library.Fit_NR as fnr
import Library.plots as plots


def get_dict_trajectory_parameters_keys(search_parameter_keys, dict_trajectory_parameters_keys):
    """
    Fills the gaps of the inputed dictionary: dict_trajectory_parameters_keys, associating a search_parameter to its 'trajectory-parameter-name'. Indeed when a search_parameter is its own trajectory-parameter we allow the user to say 'none' or not mention it.
    """
    dict_traj_param_keys_full = dict_trajectory_parameters_keys.copy()
    for key in search_parameter_keys:
        if key not in dict_trajectory_parameters_keys.keys() or dict_trajectory_parameters_keys[key].lower() == 'none':
            dict_traj_param_keys_full[key] = key
    return dict_traj_param_keys_full

def plot_regression(data_predicted, data_true, ax=None, label='', data_label='data'):
    """Summary plot of regression performance.
    """
    ax = ax or plt.gca()
    ax.scatter(data_true, data_predicted, lw=0, alpha=0.5, s=10)
    ax.set_xlabel('True ' + data_label)
    ax.set_ylabel('Predicted ' + data_label)
    x_min = data_true.min()
    x_max = data_true.max()
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(x_min, x_max)
    ax.plot([x_min, x_max], [x_min, x_max], 'r--')
    R2 = metrics.r2_score(data_true, data_predicted)
    ax.text(0.05, 0.9, f'{label}$R^2 = {R2:.2f}$', transform=ax.transAxes, fontsize=16, color='r')

def test_sk_regression(method, x_train_scaled, x_test_scaled, y_train_scaled, y_train, y_test, y_scaler, ax, y_label):
    # Fit normalized training dlogL.
    print('Fitting the model on train set...')
    fit = method.fit(x_train_scaled, y_train_scaled)
    # Get predicted dlogL for training qpos.
    print('Predicting train values...')
    y_train_predicted = y_scaler.inverse_transform(fit.predict(x_train_scaled))
    # Get predicted shear for test images.
    print('Predicting test values...')
    y_test_predicted = y_scaler.inverse_transform(fit.predict(x_test_scaled))
    # y_train = y_scaler.inverse_transform(y_train_scaled)
    # Plot results.
    # y_label=f'dlogL[{idx_dlogL_to_predict}]'
    # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
    plot_regression(y_train_predicted, y_train, label='train: ', data_label=y_label, ax=ax[0])
    plot_regression(y_test_predicted, y_test, label='test: ', data_label=y_label, ax=ax[1])
    plt.tight_layout()


if __name__=='__main__':

    from optparse import OptionParser
    # usage = """%prog [options]
    # Plots the logL as a function of the parameter defined by `pkey`, keeping the other constant."""
    parser=OptionParser()
    parser.add_option("-n", "--nb_pts_test", default=20000, action="store", type="int", help="""Number of trajectories from phase1 to use for the test set.""")
    parser.add_option("-m" ,"--nb_pts_train", default=None, action="store", type="int", help="""Number of points in qpos_phase1 to use for the fit. If left to None it will be set to the max possible given the nb of test points required.""")
    # parser.add_option("--train_frac", default=1, action="store", type="float", help="""Once the test points removed from qpos_phase1, fraction of the remaining points to use for the fit.""")
    parser.add_option("--method", default='cubic', action="store", help="""Method of regression to use. RF, KN, cubic """)
    parser.add_option("--parameters_to_plot", default=None, action="store", help=""" Indices for which to plot dlogL. Default is all of those contained in the config.ini file. """)
    parser.add_option("--rescale", default=False, action="store_true", help=""" Whether to rescale the qpos and dlogL between -1 and -1 before doing the fit. """)
    parser.add_option("--outdir", default='../__output_data/GW170817real/0_2_3_4_5_6_7_8_15_16/PSD_3/150000_800_2000_200_200_0.005/phase_marginalization/IMRPhenomD', action="store", type="string", help="""Output directory of the run we are interested in.""")
    (opts,args)=parser.parse_args()

    config_parser = ConfigParser()
    config_file_path = opts.outdir + '/config.ini'
    config_parser.read(config_file_path)
    config_dict = pu.config_parser_to_dict(config_parser)
    sampler_dict = pu.json_to_dict(opts.outdir + '/config_hmc_parameters.json')
    search_parameter_keys = sampler_dict['search_parameter_keys']
    dict_trajectory_parameters_keys = get_dict_trajectory_parameters_keys(search_parameter_keys, sampler_dict['trajectory_parameters_keys'])


    if opts.parameters_to_plot is not None:
        parameters_to_plot = list(opts.parameters_to_plot.split(','))
    else:
        parameters_to_plot = search_parameter_keys

    directory = opts.outdir + '/sampler_state/phase1'
    qpos_phase1 = np.loadtxt(directory + '/qpos_global_fit.dat')
    dlogL_phase1 = np.loadtxt(directory + '/dlogL_global_fit.dat')

    if False:
        # remove outliers of the data set
        print('Removing outliers from phase1 data...')
        from scipy import stats
        z = np.abs(stats.zscore(qpos_phase1))
        threshold = 2
        qpos_phase1 = qpos_phase1[(z < threshold).all(axis=1)]
        dlogL_phase1 = dlogL_phase1[(z < threshold).all(axis=1)]
        nb_outliers = np.where((z > threshold).any(axis=1))[0].shape[0]
        print(f'{nb_outliers} outliers removed.')

    if False:
        for idx_dlogL_to_predict in range(dlogL_phase1.shape[1]):
            plt.hist(dlogL_phase1[:, idx_dlogL_to_predict], bins=35)
            plt.legend(fontsize='large')
            plt.xlabel(f'dlogL[{idx_dlogL_to_predict}] magnitude')
            plt.show()
        sys.exit()

    # Create train-test sets on the position data
    length_num_traj = config_dict['hmc']['length_num_traj']
    nb_points_test = opts.nb_pts_test
    nb_points_train_max = qpos_phase1.shape[0] - nb_points_test
    if opts.nb_pts_train is None:
        nb_points_train = nb_points_train_max
    else:
        nb_points_train = min(opts.nb_pts_train, nb_points_train_max)


    print(f'Using the last  {nb_points_test:,} phase1 points for test out of {qpos_phase1.shape[0]:,} available')
    print(f'Using the first {nb_points_train:,} phase1 points for train set.')
    qpos_train = qpos_phase1[:nb_points_train, :]
    qpos_test = qpos_phase1[-nb_points_test:, :]

    if opts.rescale:
        qpos_scaler = preprocessing.StandardScaler().fit(qpos_train)
        qpos_train_scaled = qpos_scaler.transform(qpos_train)
        qpos_test_scaled = qpos_scaler.transform(qpos_test)
        plots.plot_walkers_and_corner_from_samples(qpos_train_scaled, None, search_parameter_keys, save=True, label='{}_samples_scaled'.format(len(qpos_train_scaled)), outdir=opts.outdir + '/plots/phase1')
        sys.exit()

    nb_dlogL_to_predict = len(parameters_to_plot)
    fig, ax = plt.subplots(nb_dlogL_to_predict, 2, sharex=False, sharey=False, figsize=(10, 4 * nb_dlogL_to_predict))
    title = f'{opts.method} fit'
    if config_dict['analysis']['phase_marginalization']:
        title += ' - marginalizing over phase'
    fig.text(x=0.4, y=0.95, s=title, fontsize=15)
    fig.text(x=0.2, y=0.90, s=f'train set: {qpos_train.shape[0]} pts', fontsize=12)
    fig.text(x=0.7, y=0.90, s=f'test set: {qpos_test.shape[0]} pts', fontsize=12)

    if nb_dlogL_to_predict==1:
        ax = ax.reshape(1, -1)


    if opts.method == 'KN':
        method = neighbors.KNeighborsRegressor()
    if opts.method == 'RF':
        method = ensemble.RandomForestRegressor(random_state=123)
    if opts.method == 'GB':
        method = ensemble.GradientBoostingRegressor(random_state=123)
    if opts.method == 'cubic':
        print(f'Computing pseudo_inverse...')
        method_global = fnr.CubicFitRegressor()
        if opts.rescale:
            pseudo_inverse = method_global.compute_pseudo_inverse(qpos_train_scaled)
        else:
            pseudo_inverse = method_global.compute_pseudo_inverse(qpos_train)
    if opts.method == 'DNN':
        from keras.models import Sequential
        from keras.layers import Dense
        method = Sequential()
        # Informations about activation functions: https://missinglink.ai/guides/neural-network-concepts/7-types-neural-network-activation-functions-right/
        # For Keras: https://keras.io/activations/
        # from keras.activations import sigmoid
        # int(1 + n + n*(n+1)/2 + n*(n+1)*(n+2)/6)
        n_dim = qpos_train.shape[1]
        # method.add(Dense(units=n-1 , activation='relu', input_dim=n))
        # method.add(Dense(units=n-1, activation='tanh'))
        # # method.add(Dense(units=int(qpos_train.shape[1]/2), activation='relu'))
        # method.add(Dense(units=1, activation='tanh'))
        method.add(Dense(units=n_dim * 10, input_dim=n_dim, kernel_initializer='normal', activation='relu'))
        method.add(Dense(units=n_dim * 100, kernel_initializer='normal', activation='relu'))
        method.add(Dense(units=n_dim * 10, kernel_initializer='normal', activation='relu'))
        method.add(Dense(units=1))
        # Compile the model/method
        # Cf https://keras.io/losses/ for loss functions available
        # from keras import losses
        # loss = keras.losses.logcosh(y_true, y_pred)
        # loss = 'mse'
        loss = 'mean_absolute_error'
        # optimizer='rmsprop'
        optimizer='adam'
        # For a mean squared error regression problem
        method.compile(optimizer=optimizer, loss=loss)

        n_iterations_per_epochs = 100
        batch_size = int(qpos_train.shape[0]/n_iterations_per_epochs)
        # epochs = batch_size * 2
        epochs = 50


    parameter_indices_to_plot = []
    for ii, param_key in enumerate(parameters_to_plot):
        idx_dlogL_to_predict = search_parameter_keys.index(param_key)
        parameter_indices_to_plot.append(idx_dlogL_to_predict)
        traj_param_key = dict_trajectory_parameters_keys[param_key]
        traj_param_latex_label = CONST.LATEX_LABELS[traj_param_key]
        dlogL_name = r'$dlogL/d$' + traj_param_latex_label
        print(f'\ntraj_param_key = {traj_param_key}')
        # dlogL_param = dlogL_phase1[:, idx_dlogL_to_predict]

        # Create train-test set by randomly choosing amongst points in phase1
        # qpos_train, qpos_test, dlogL_train, dlogL_test = model_selection.train_test_split(qpos_phase1, dlogL_phase1, test_size=0.9, random_state=123)

        # Create train-test set by removing some of the last trajectories of phase1 for the test set
        dlogL_train = dlogL_phase1[:nb_points_train, idx_dlogL_to_predict]
        dlogL_test = dlogL_phase1[-nb_points_test:, idx_dlogL_to_predict]

        if False:
            param_to_plot = 4
            plt.hist(qpos_train[:, param_to_plot], bins=35, label='Train')
            plt.hist(qpos_test[:, param_to_plot], bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Distribution of qpos[{param_to_plot}]')
            plt.show()

            plt.hist(qpos_train_scaled[:, param_to_plot], bins=35, label='Train')
            plt.hist(qpos_test_scaled[:, param_to_plot], bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Distribution of scaled qpos[{param_to_plot}]')
            plt.show()

        if opts.rescale:
            dlogL_train_scaler = preprocessing.StandardScaler().fit(dlogL_train.reshape(-1, 1))
            # the `.reshape(-1)` at the end is in order not to have a column vector but a flattened array as asked by the `method.fit()` function
            dlogL_train_scaled = dlogL_train_scaler.transform(dlogL_train.reshape(-1, 1)).reshape(-1)
            dlogL_test_scaled = dlogL_train_scaler.transform(dlogL_test.reshape(-1, 1)).reshape(-1)


        if False:
            plt.hist(dlogL_train, bins=35, label='Train')
            plt.hist(dlogL_test, bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'True dlogL[{idx_dlogL_to_predict}] magnitude')
            plt.show()

            plt.hist(dlogL_train_scaled, bins=35, label='Train')
            plt.hist(dlogL_test_scaled, bins=35, label='Test')
            plt.legend(fontsize='large')
            plt.xlabel(f'Scaled true dlogL[{idx_dlogL_to_predict}] magnitude')
            plt.show()

        # method = neighbors.KNeighborsRegressor()
        # train_data=qpos_train_scaled
        # test_data=qpos_test_scaled
        # fit = method.fit(train_data, dlogL_train_scaled)
        # dlogL_train_predicted = dlogL_scaler.inverse_transform(fit.predict(train_data))
        # dlogL_test_predicted = dlogL_scaler.inverse_transform(fit.predict(test_data))
        #
        # fig, ax = plt.subplots(1, 2, sharex=True, sharey=True, figsize=(10, 4))
        # data_label=f'dlogL[{idx_dlogL_to_predict}]'
        # plot_regression(dlogL_train_predicted, dlogL_train, label='train: ', data_label=data_label,  ax=ax[0])
        # plot_regression(dlogL_test_predicted, dlogL_test, label='test: ', data_label=data_label, ax=ax[1])
        # plt.tight_layout()
        if opts.method == 'cubic':
            method = fnr.CubicFitRegressor(pseudo_inverse)
        # test_sk_regression(method=method, x_train_scaled=qpos_train_scaled, x_test_scaled=qpos_test_scaled, y_train_scaled=dlogL_train_scaled, y_train=dlogL_train, y_test=dlogL_test, y_scaler=dlogL_train_scaler, ax=ax[ii,:], y_label=dlogL_name)


        # Fit normalized training dlogL.
        if opts.rescale:
            print('Fitting the model on train set...')
            # Get predicted dlogL for training qpos.
            print('Predicting train values...')
            if opts.method == 'DNN':
                fit = method.fit(qpos_train_scaled, dlogL_train_scaled, batch_size=batch_size, epochs=epochs, verbose=2)
                dlogL_train_predicted_scaled = method.predict(qpos_train_scaled)
                dlogL_train_predicted = dlogL_train_scaler.inverse_transform(dlogL_train_predicted_scaled)
            else:
                fit = method.fit(qpos_train_scaled, dlogL_train_scaled)
                dlogL_train_predicted_scaled = fit.predict(qpos_train_scaled)
                dlogL_train_predicted = dlogL_train_scaler.inverse_transform(dlogL_train_predicted_scaled)
            # Get predicted shear for test images.
            print('Predicting test values...')
            if opts.method == 'DNN':
                dlogL_test_predicted_scaled = method.predict(qpos_test_scaled)
                dlogL_test_predicted = dlogL_train_scaler.inverse_transform(dlogL_test_predicted_scaled)
            else:
                dlogL_test_predicted_scaled = fit.predict(qpos_test_scaled)
                dlogL_test_predicted = dlogL_train_scaler.inverse_transform(dlogL_test_predicted_scaled)
        else:
            print('Fitting the model on train set...')
            # Get predicted dlogL for training qpos.
            print('Predicting train values...')
            if opts.method == 'DNN':
                fit = method.fit(qpos_train, dlogL_train, batch_size=batch_size, epochs=epochs, verbose=2)
                dlogL_train_predicted = method.predict(qpos_train)
            else:
                fit = method.fit(qpos_train, dlogL_train)
                import IPython; IPython.embed();
                dlogL_train_predicted = fit.predict(qpos_train)
            # Get predicted shear for test images.
            print('Predicting test values...')
            if opts.method == 'DNN':
                dlogL_test_predicted = method.predict(qpos_test)
            else:
                dlogL_test_predicted = fit.predict(qpos_test)

        plot_regression(dlogL_train_predicted, dlogL_train, label='train: ', data_label=dlogL_name, ax=ax[ii, 0])
        plot_regression(dlogL_test_predicted, dlogL_test, label='test: ', data_label=dlogL_name, ax=ax[ii, 1])
        plt.tight_layout()
        fig.subplots_adjust(top=0.85)
    if True:
        filename = f'/plots/{opts.method}_regression_{nb_points_train}train_{nb_points_test}test_dlogL' + ''.join([str(idx) for idx in parameter_indices_to_plot])
        fig.savefig(opts.outdir + filename)
    else:
        plt.show()


    # ip_shell = ip.get_ipython()
    # ip_shell.run_line_magic('time', 'test_sk_regression(method=method, x_train_scaled=qpos_train_scaled, x_test_scaled=qpos_test_scaled, y_train_scaled=dlogL_train_scaled, y_train=dlogL_train, y_test=dlogL_test, y_scaler=dlogL_scaler, ax=ax[ii,:], y_label=dlogL_name)')
