import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

from matplotlib import rc

offset = 1e-5
nb_pts = 200
# x = np.arange(0,8,0.1)
# y = np.arange(0,10,0.1)
xmin, xmax = 0, 20
ymin, ymax = 0, 100
x = np.linspace(xmin, xmax, nb_pts)
y = np.linspace(ymin, ymax, nb_pts)

sigma_x_1 = (xmax - xmin) / 12
sigma_y_1 = (ymax - ymin) / 8
sigma_x_2 = (xmax - xmin) / 14
sigma_y_2 = (ymax - ymin) / 18
C_x_1 = 1/np.sqrt(2*np.pi*sigma_x_1**2)
C_y_1 = 1/np.sqrt(2*np.pi*sigma_y_1**2)
C_x_2 = 1/np.sqrt(2*np.pi*sigma_x_2**2)
C_y_2 = 1/np.sqrt(2*np.pi*sigma_y_2**2)
x_0_1 = 5
y_0_1 = 25
x_0_2 = 15
y_0_2 = 75

def loglikelihood(x, y):
    x_r_1 = (x-x_0_1)/sigma_x_1
    y_r_1 = (y-y_0_1)/sigma_y_1
    x_r_2 = (x-x_0_2)/sigma_x_2
    y_r_2 = (y-y_0_2)/sigma_y_2
    logL = 0.5*(C_x_1*C_y_1*np.exp(-0.5*(x_r_1**2 + y_r_1**2)) + C_x_2*C_y_2*np.exp(-0.5*(x_r_2**2 + y_r_2**2)))
    return logL


# Meshgrid method
xx, yy = np.meshgrid(x, y, sparse=True)
logL_3D = loglikelihood(xx, yy)

logL_3D_xplus = loglikelihood(xx + offset, yy)
logL_3D_xminus = loglikelihood(xx - offset, yy)
dlogL_dx_3D = (logL_3D_xplus - logL_3D_xminus) / (2 * offset)

logL_3D_yplus = loglikelihood(xx, yy + offset)
logL_3D_yminus = loglikelihood(xx, yy - offset)
dlogL_dy_3D = (logL_3D_yplus - logL_3D_yminus) / (2 * offset)

if False:
    # Loop method
    logL_list = []
    dlogL_dx_list = []
    for yit in y:
        for xit in x:
            logL = loglikelihood(xit, yit)
            logL_list.append(logL)
            logL_plus = loglikelihood(xit + offset, yit)
            logL_minus = loglikelihood(xit - offset, yit)
            dlogL_dx = (logL_plus - logL_minus) / (2 * offset)
            dlogL_dx_list.append(dlogL_dx)

    logL_3D = np.asarray(logL_list).reshape((len(y), len(x)))
    dlogL_dx_3D = np.asarray(dlogL_dx_list).reshape((len(y), len(x)))




fig = plt.figure(figsize=(20,15))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=16)
plt.rc('font', weight='bold')
ax = Axes3D(fig)
ax.plot_surface(xx, yy, dlogL_dx_3D, rstride=3, cstride=3, cmap=cm.viridis, shade=True, color='w')
# ax.plot_wireframe(xx, yy, dlogL_dx_3D, rstride=3, cstride=3)
ax.set_xlim(left=xmin, right=xmax)
ax.set_ylim(bottom=ymin, top=ymax)
ax.set_xlabel(r"\textbf{q1}", fontsize=25)
ax.set_ylabel(r"\textbf{q2}", fontsize=25)
ax.set_zlabel(r"\boldmath$dln\mathcal{L}/d\textbf{q1}$", fontsize=25)
ax.set_title(r"\boldmath$dln\mathcal{L}/d\textbf{q1}$", fontsize=25)



fig = plt.figure(figsize=(20,15))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=16)
plt.rc('font', weight='bold')
ax = Axes3D(fig)
ax.plot_surface(xx, yy, dlogL_dy_3D, rstride=3, cstride=3, cmap=cm.viridis, shade=True, color='w')
# ax.plot_wireframe(xx, yy, dlogL_dx_3D, rstride=3, cstride=3)
ax.set_xlim(left=xmin, right=xmax)
ax.set_ylim(bottom=ymin, top=ymax)
ax.set_xlabel(r"\textbf{q1}", fontsize=25)
ax.set_ylabel(r"\textbf{q2}", fontsize=25)
ax.set_zlabel(r"\boldmath$dln\mathcal{L}/d\textbf{q2}$", fontsize=25)
ax.set_title(r"\boldmath$dln\mathcal{L}/d\textbf{q2}$", fontsize=25)




fig = plt.figure(figsize=(20,15))
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rc('font', size=16)
plt.rc('font', weight='bold')
ax = Axes3D(fig)
ax.plot_surface(xx, yy, logL_3D, rstride=3, cstride=3, cmap=cm.viridis, shade=True, color='w')
# ax.plot_wireframe(xx, yy, logL_3D, rstride=3, cstride=3)
ax.set_xlim(left=xmin, right=xmax)
ax.set_ylim(bottom=ymin, top=ymax)
ax.set_xlabel(r"\textbf{q1}", fontsize=25)
ax.set_ylabel(r"\textbf{q2}", fontsize=25)
ax.set_zlabel(r"\boldmath$\mathcal{N}(\mu_A,\Sigma_A) + \mathcal{N}(\mu_B,\Sigma_B)$", fontsize=25)
ax.set_title(r"\textbf{Bimodal posterior distribution}", fontsize=25)

# plt.savefig('../../../General theory/presentations/Mes présentations/')
plt.show()






# import sys
# # cf https:#docs.python.org/3/tutorial/modules.html
# sys.path.append('../')
#
# import Library.Fit_NR as fnr
#
# method_global = fnr.CubicFitRegressor()
# pseudo_inverse = method_global.compute_pseudo_inverse(qpos_train)

# fit_coeff_global = fnr.QR_FittingMethod_Gradients(qpos_global_fit, dlogL_global_fit)
