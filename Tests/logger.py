import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

import logging
import os
import Library.python_utils as pu


logger = logging.getLogger('gwhmc')

def check_directory_exists_and_if_not_mkdir(directory):
    """ Checks if the given directory exists and creates it if it does not exist

    Parameters
    ----------
    directory: str
        Name of the directory

    """
    if not os.path.exists(directory):
        os.makedirs(directory)
        logger.debug('Making directory {}'.format(directory))
    else:
        logger.debug('Directory {} exists'.format(directory))

def setup_logger(outdir=None, label=None, log_level='INFO'):
    """ Setup logging output: call at the start of the script to use

    Parameters
    ----------
    outdir, label: str
        If supplied, write the logging output to outdir/label.log
    log_level: str, optional
        ['debug', 'info', 'warning']
        Either a string from the list above, or an integer as specified
        in https://docs.python.org/2/library/logging.html#logging-levels
    print_version: bool
        If true, print version information
    """

    if type(log_level) is str:
        try:
            level = getattr(logging, log_level.upper())
        except AttributeError:
            raise ValueError('log_level {} not understood'.format(log_level))
    else:
        level = int(log_level)

    logger = logging.getLogger('gwhmc')
    logger.propagate = False
    logger.setLevel(level)
    breakpoint()
    if any([type(h) == logging.StreamHandler for h in logger.handlers]) is False:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(name)s %(levelname)-8s: %(message)s', datefmt='%H:%M'))
        stream_handler.setLevel(level)
        logger.addHandler(stream_handler)

    if any([type(h) == logging.FileHandler for h in logger.handlers]) is False:
        if label:
            if outdir:
                check_directory_exists_and_if_not_mkdir(outdir)
            else:
                outdir = '.'
            log_file = '{}/{}.log'.format(outdir, label)
            file_handler = logging.FileHandler(log_file)
            file_handler.setFormatter(logging.Formatter(
                '%(asctime)s %(levelname)-8s: %(message)s', datefmt='%H:%M'))

            file_handler.setLevel(level)
            logger.addHandler(file_handler)

    for handler in logger.handlers:
        handler.setLevel(level)


def format_dict_for_print(dictionary, indent=3, align_keys=True):
    """
    Formats a dictionary such that the outputted string will be printed with the nested structure.

    Parameters:
        indent: int, default=3
            number of spaces used to indent the nested dictionaries
        align_keys: boolean, default=True
            aligns vertically the printed values
    """
    string_to_print = ''
    indent_align_key_str = ''
    # We need to know the biggest length of all keys in order to align the prints
    if align_keys:
        max_length_key = 0
        for key in dictionary.keys():
            max_length_key = max(max_length_key, len(key))

    # Multiply a space string by the number of indent wished to get the string which will be appended before printing each key
    indent_str = ' ' * indent

    # Now iterate over the dictionary
    for key, value in dictionary.items():
        if align_keys:
            indent_align_key = max(max_length_key - len(key), 0)
            indent_align_key_str = ' ' * indent_align_key
        # If the key contains itself a dictionary, we recursively call this `format_dict_for_print()` function but multiplying the indent by 2 such that the nested structure appears well on the print
        if type(value) == dict:
            string_to_print += '\n' + indent_str + key + indent_align_key_str + ':'
            string_to_print += format_dict_for_print(value, indent*2, align_keys)
        else:
            string_to_print += '\n' + indent_str + key + indent_align_key_str + ': {}'.format(value)

    return string_to_print

if __name__ == '__main__':

    setup_logger(log_level='INFO')

    logger.info("Testing the info method")
    logger.warning("Testing the warning method")

    simple_dict = {'tutu': 999}
    nested_dict = {'toto': 123, 'person': {'firstName': "Marc", 'lastName': "Arène"}}
    formatted_sdict = format_dict_for_print(simple_dict)
    formatted_ndict = format_dict_for_print(nested_dict)
    import IPython; IPython.embed(); sys.exit()
    logger.warning(f"Testing the warning method {string_with_multiple_lines}")
