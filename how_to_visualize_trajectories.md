# How to use visualize trajectories in Phase 1 and 3

## Use the `--plot_traj` option
- Run with the `--plot-traj` option in the command line
```
$ python main.py --event=GW170817 --trigger_file=../examples/trigger_files/GW170817_IMRPhenomD_NRTidal.ini --config_file=../examples/config_files/config_GW170817_IMRPhenomD_NRTidal_compmass.ini --fit_method=dnn --outdir=../__output_data/GW170817real/compmass/ --plot_traj
```
This will create in the `outdir` you defined a sub-directory named `/plot_traj` where data files about each trajectory will be saved.

- If for instance all trajectories in Phase 1 are rejected and you want to scrutinize what's happening, don't forget to set the total number of trajectory in Phase 1 in the `config.ini` file to a small number, like `~20`. Note that to avoid any potential bug, it's safe to have this number at least as big as the dimensionality of the analysis.
```ini
[hmc]
# Total number of trajectories of HMC
n_traj_hmc_tot = 13
# Number of trajectories using  numericalgradients in phase 1.
n_traj_fit = 13
```

## Use the script `/Tests/plot_trajectory.py`
- Go to the directory `/gwhmc/Tests`
- If `[outdir]` points to the main directory (ending with a slash `/`) of the run from which you want to have some plots about the 4th numerical trajectory, then simply run the following:
```
$ python plot_trajectory.py [outdir] --traj_nb=2
```
So in the example above it would give
```
$ python plot_trajectory.py /Users/marcarene/projects/python/gwhmc/__output_data/GW170817real/compmass/ --traj_nb=2
```

- Note that the acceptance rate displayed at the top, `Acc=...`, does not corresponds exactly to that computed during the run because the data points recorded to plot the trajectory end a step before the MH ratio is computed.
