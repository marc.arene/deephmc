# How to setup and install DeepHMC source code

DeepHMC heavily relies on the [Bilby library](https://arxiv.org/abs/2006.00714), but for stability reasons as Bilby was evolving rapidly, at the moment one has to use my (i.e. Marc Arène's) fork of Bilby for DeepHMC to run.

## Preliminary (optional)
- Conda: where is anaconda installed ? i.e in `/Anaconda` or in `~/anaconda3` ? It's just that at some point my Mac OS update moved everything from the former to the latter and it was quite a pain then to fix everything...
- Modify your `.bash_profile`:
  - Add the following line:
    `export BILBY_MATHDEFAULT=1;`
    which will export this bilby environment variable. It will set the following latex command in bilby: `'\providecommand{\mathdefault}[1][]{}'` and avoid some errors when making some plots (see function `bilby.core.utils.latex_plot_format()`).
  - Do you have a `.bash_prompt` sourced automatically ? ie some lines in your `.bash_profile` which look like
  ```
  # Load the shell dotfiles, and then some:
  # * ~/.path can be used to extend `$PATH`.
  # * ~/.extra can be used for other settings you don’t want to commit.
  for file in ~/.{path,bash_prompt,exports,aliases,functions,extra,profile}; do
      [ -r "$file" ] && [ -f "$file" ] && source "$file";
  done;
  unset file;
  ```
  if not I would advise to add them and create and `~/.bash_prompt` file.
- Modify your `~/.condarc` file to
  ```
  channels:
  - conda-forge
  - defaults
  ssl_verify: true
  changeps1: true
  env_prompt: '\n({default_env}) '
  channel_priority: strict
  ```
- Modify your `.bash_prompt` to essentially mine which I copy-paste bellow. The first part will automatically display and colour on your terminal the git branch you're on. The last part sets some colors for the title of the prompt and the last line: `echo $CONDA_DEFAULT_ENV`, displays the active conda environement.
  ```bash
  #!/usr/bin/env bash

  # Shell prompt based on the Solarized Dark theme.
  # Screenshot: http://i.imgur.com/EkEtphC.png
  # Heavily inspired by @necolas’s prompt: https://github.com/necolas/dotfiles
  # iTerm → Profiles → Text → use 13pt Monaco with 1.1 vertical spacing.

  if [[ $COLORTERM = gnome-* && $TERM = xterm ]] && infocmp gnome-256color >/dev/null 2>&1; then
  	export TERM='gnome-256color';
  elif infocmp xterm-256color >/dev/null 2>&1; then
  	export TERM='xterm-256color';
  fi;

  prompt_git() {
  	local s='';
  	local branchName='';

  	# Check if the current directory is in a Git repository.
  	if [ $(git rev-parse --is-inside-work-tree &>/dev/null; echo "${?}") == '0' ]; then

  		# check if the current directory is in .git before running git checks
  		if [ "$(git rev-parse --is-inside-git-dir 2> /dev/null)" == 'false' ]; then

  			# Ensure the index is up to date.
  			git update-index --really-refresh -q &>/dev/null;

  			# Check for uncommitted changes in the index.
  			if ! $(git diff --quiet --ignore-submodules --cached); then
  				s+='+';
  			fi;

  			# Check for unstaged changes.
  			if ! $(git diff-files --quiet --ignore-submodules --); then
  				s+='!';
  			fi;

  			# Check for untracked files.
  			if [ -n "$(git ls-files --others --exclude-standard)" ]; then
  				s+='?';
  			fi;

  			# Check for stashed files.
  			if $(git rev-parse --verify refs/stash &>/dev/null); then
  				s+='$';
  			fi;

  		fi;

  		# Get the short symbolic ref.
  		# If HEAD isn’t a symbolic ref, get the short SHA for the latest commit
  		# Otherwise, just give up.
  		branchName="$(git symbolic-ref --quiet --short HEAD 2> /dev/null || \
  			git rev-parse --short HEAD 2> /dev/null || \
  			echo '(unknown)')";

  		[ -n "${s}" ] && s=" [${s}]";

  		echo -e "${1}${branchName}${2}${s}";
  	else
  		return;
  	fi;
  }

  if tput setaf 1 &> /dev/null; then
  	tput sgr0; # reset colors
  	bold=$(tput bold);
  	reset=$(tput sgr0);
  	# Solarized colors, taken from http://git.io/solarized-colors.
  	black=$(tput setaf 0);
  	blue=$(tput setaf 33);
  	cyan=$(tput setaf 37);
  	green=$(tput setaf 64);
  	orange=$(tput setaf 166);
  	purple=$(tput setaf 125);
  	red=$(tput setaf 124);
  	violet=$(tput setaf 61);
  	white=$(tput setaf 15);
  	yellow=$(tput setaf 136);
  else
  	bold='';
  	reset="\e[0m";
  	black="\e[1;30m";
  	blue="\e[1;34m";
  	cyan="\e[1;36m";
  	green="\e[1;32m";
  	orange="\e[1;33m";
  	purple="\e[1;35m";
  	red="\e[1;31m";
  	violet="\e[1;35m";
  	white="\e[1;37m";
  	yellow="\e[1;33m";
  fi;

  # Highlight the user name when logged in as root.
  if [[ "${USER}" == "root" ]]; then
  	userStyle="${red}";
  else
  	userStyle="${orange}";
  fi;

  # Highlight the hostname when connected via SSH.
  if [[ "${SSH_TTY}" ]]; then
  	hostStyle="${bold}${red}";
  else
  	hostStyle="${yellow}";
  fi;

  # Set the terminal title and prompt.
  # To act on the conda environment name which is appended before this, modify the ~/.condarc file and cf https://conda.io/projects/conda/en/latest/configuration.html
  PS1="\[\033]0;\W\007\]"; # working directory base name
  # PS1+="\[${bold}\]\n"; # newline
  PS1="\[${bold}\]"; # newline
  PS1+="\[${userStyle}\]\u"; # username
  # PS1+="\[${white}\] at ";
  # PS1+="\[${hostStyle}\]\h"; # host
  PS1+="\[${white}\] in ";
  PS1+="\[${green}\]\w"; # working directory full path
  PS1+="\$(prompt_git \"\[${white}\] on \[${violet}\]\" \"\[${blue}\]\")"; # Git repository details
  PS1+="\n";
  PS1+="\[${white}\]\$ \[${reset}\]"; # `$` (and reset color)
  export PS1;

  PS2="\[${yellow}\]→ \[${reset}\]";
  export PS2;

  echo $CONDA_DEFAULT_ENV
  ```


## Create conda environment and activate it
- Conda commands on environments can be reviewed [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html).
- Create a conda environment which we name `bilby_fork_marc`, with python version `3.7.3`
  ```
  $ conda create -n bilby_fork_marc python=3.7.3
  ```
- Activate the environment and check which python is run
  ```
  $ conda activate bilby_fork_marc
  (bilby_fork_marc) $  which python
  /Users/marcarene/anaconda3/envs/bilby_fork_marc/bin/python
  ```
  Note that your conda env is only activated in your terminal session.
- To deactivate the environment, simply run
  ```
  (bilby_fork_marc) $ conda deactivate
  (base) $
  ```

## Clone and install my fork of bilby
- Clone my fork of bilby in, for this example, a folder `~/projects/python`
  ```
  $ cd
  $ mkdir projects
  $ cd projects
  $ mkdir python
  $ cd python
  $ git clone git@git.ligo.org:marc.arene/bilby.git
  ```
- Going into the directory of the fork: `~/projects/python/bilby`, install first the non-optional requirements, starting specifically with this version of `h5py` because otherwise we will get a bug
  ```
  (bilby_fork_marc) $ conda install h5py=2.10.0
  (bilby_fork_marc) $ conda install --file requirements.txt
  ```
- Then set-up this fork of bilby as the package to be used in the environment `bilby_fork_marc` whenever some python file will run `import bilby`. That can be done with the `setup.py` script:
  ```
  (bilby_fork_marc) $ python setup.py develop
  ```
- Then install the "optional requirements" even though they are necessary to most people as it contains the packages `astropy, lalsuite, gwpy, theano, plotly`, and also install `ipython` and `ipdb` which is so useful to debug the code.
  ```
  (bilby_fork_marc) $ conda install --file optional_requirements.txt
  (bilby_fork_marc) $ conda install ipython
  (bilby_fork_marc) $ conda install ipdb
  ```

## Clone DeepHMC's repo and finish installing
- Let's clone this conda environment so that you have a "check-pointed" environment with my fork of bilby set-up. Let us name the clone `deephmc`
  ```
  (bilby_fork_marc) $ conda create --name deephmc --clone bilby_fork_marc
  (bilby_fork_marc) $ conda activate deephmc
  (deephmc) $ conda env list
  ```
- Clone our repo
 ```
 $ cd ~/projects/python
 $ git clone git@gitlab.in2p3.fr:marc.arene/gwhmc.git
 ```
- Install some packages needed for DeepHMC (most of them should already be installed from our install of bilby). For keras the reason I need this specific version is because it installs the `tensorflow` with the version `v1.14` which is optimized for my CPU settings contrary to TF `v2.0`, I hope it's the same for yours...
  ```
  (deephmc) $ conda install scikit-learn
  (deephmc) $ conda install keras=2.3.1
  (deephmc) $ conda install pydot
  ```

- Create your git branch
```
$ git checkout -b branch_ed
```
Note that the branch you are on does not depend on your terminal session.
- What's your code editor ? It probably provides short-cuts to run the usual git commands, like commit etc.


## Set-up some folders and input files to run DeepHMC
- In your DeepHMC (gwhmc) home directory, which should be `~/projects/python/gwhmc`, create the following folders:
  ```
  $ mkdir __input_data
  $ mkdir __output_data
  ```
  The double underscores is set in the `.git_ignore` to be ignored by git, so that's where I store the data.


## Try running DeepHMC on GW190412
- Download the "4096sec • 4KHz" strain files of every detector from [GWOSC](https://www.gw-openscience.org/eventapi/html/GWTC-2/GW190412/v3/) in the `__input_data`
  ```
  $ mkdir __input_data/GW190412
  ```
- Dowload the PSDs from the [discovery paper repo](https://git.ligo.org/publications/gw190412/gw190412-discovery/-/tree/master/pe/PSDs) in a sub-directory
  ```
  $ mkdir __input_data/GW190412/discovery_repo
  ```
- Go to the directory where the `main.py` file is
  ```
  $ cd ~/projects/python/gwhmc/Codes
  ```
- The main command must be run from there and in the correct conda env, it goes as
  ```
  (deephmc) ed.porter in ~/projects/python/gwhmc/Codes on branch_ed [$]
  $ python main.py --event=GW190412 --config_file=../examples/config_files/config_GW190412_IMRPhenomHM.ini --trigger_file=../examples/trigger_files/GW190412_IMRPhenomHM_mfsnr20.ini --fit_method=dnn
  ```


## Get latest code version on your `branch_ed`
- So when I make changes to the code, fix bugs etc, at some point I push them on this repo on the `master` branch. To get them on your local `branch_ed` you have 1: go to your local `master` branch, 2: pull the changes from the remote repo, 3: go to your branch, 4: merge your newly updated local `master` branch into your `branch_ed`, i.e. :
  ```
  $ git checkout master
  $ git pull origin master
  $ git checkout branch_ed
  $ git merge master
  ```
  If there are some conflicts between the changes you pulled and those on your `branch_ed`, you need to resolve them manually !
